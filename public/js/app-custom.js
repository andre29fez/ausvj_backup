let form = document.getElementById("formQty");
let qty = document.getElementById("qty");
let foodId = document.getElementById("foodId");
let foodPrice = document.getElementById("foodPrice");
let foodName = document.getElementById("foodName");
let foodCatgory = document.getElementById("foodCatgory");
let msg = document.getElementById("msg");
let cart = document.getElementById("cart");
let data = JSON.parse(localStorage.getItem("data")) ? JSON.parse(localStorage.getItem("data")) : [];
let totalOrder = 0;

function addFood(id, name, price, category){
    $('#foodId').val(id)
    $('#foodName').val(name)
    $('#foodPrice').val(price)
    $('#foodCatgory').val(category)
    $('#qty').val(1)
    msg.innerHTML = "";
    $("#extras").modal('show');

}

function storeQty(){
    formValidation();
}

let formValidation = () => {
    let food = data.filter((x=>{
        if (x) {
            return foodId.value == x.foodId && foodCatgory.value == x.foodCatgory
        }
    }))
    // return

    if (qty.value == 0) {
        msg.innerHTML = "Qty cannot be 0";
    } else if(food.length == 1)
        msg.innerHTML = "Article already added";
    else {
        msg.innerHTML = "";
        acceptData();

        $("#extras").modal('hide');
    }
};


let acceptData = () => {
    data.push({
        qty: qty.value,
        foodId: foodId.value,
        foodName: foodName.value,
        foodPrice: foodPrice.value,
        foodCatgory: foodCatgory.value,

        totalPrice: parseFloat(foodPrice.value)*parseFloat(qty.value)
    });

    localStorage.setItem("data", JSON.stringify(data));

    createCart();
};

let createCart = () => {
    itemTotal()

    cart.innerHTML = "";
    data.map((x, y) => {
        // return;

        if (x != null && x.qty > 0) {

            return (cart.innerHTML += `
                    <div class="gold-members d-flex align-items-center justify-content-between px-3 py-2 border-bottom">
                            <div class="media align-items-center">
                                <div class="mr-2 text-danger">&middot;</div>
                                <div class="media-body">
                                    <p class="m-0">${x.foodName}</p>
                                </div>
                                </div>
                            <div class="d-flex align-items-center">
                                <span class="count-number float-right"><button onclick="updateQty('sub','${x.qty}', '${x.foodId}', '${x.foodCatgory}')" type="button" class="btn-sm left dec btn btn-outline-secondary"> <i class="feather-minus"></i>
                                    </button><input class="count-number-input" type="text" value="${x.qty}"><button onclick="updateQty('add', '${x.qty}', '${x.foodId}', '${x.foodCatgory}')" type="button" class="btn-sm right inc btn btn-outline-secondary"> <i class="feather-plus"></i>
                                        </button></span>
                                        <p class="text-gray mb-0 float-right ml-2 text-muted small">${(Math.round(x.totalPrice * 100) / 100).toFixed(2)}</p>
                                        </div>
                                        </div>
                                        `)
        }else{

        }
    });
};

let itemTotal = () => {
    let total = 0;
    data.map((x, y) => {
        if (x != null && x.qty > 0){
            total += x.totalPrice}
    });
    let totalFormat = (Math.round(total * 100) / 100).toFixed(2);
    totalOrder = totalFormat;
    $('#itemTotal').text(totalFormat)
    $('#toPay').text(totalFormat)
    $('#pay').text(totalFormat)

}

function updateQty(act, qty, foodId, foodCatgory){
    let dataSub = data.map((x, y) => {
        let addCount = 0
        let updateTotal = 0
        if (x != null && x.foodId == foodId && x.foodCatgory == foodCatgory) {
            addCount = act == 'sub' ? parseInt(x.qty) - 1 : parseInt(x.qty) + 1
            updateTotal = parseFloat(x.foodPrice) * parseInt(addCount)
            x.qty = addCount
            x.totalPrice = updateTotal
        }

        return x
    });

    let datas = dataSub.filter(function(value){
        return value.qty > 0
    });

    localStorage.setItem("data", JSON.stringify(datas));
    data = datas;

    createCart()
}

function extrasQty(act){
    let currentQty = parseInt($('#qty').val())
    if (act == 'sub') {
        currentQty -= 1
    }else{
        currentQty += 1
    }

    $('#qty').val(parseInt(currentQty))
}


function confirmOrder() {
    let patien = JSON.parse(localStorage.getItem('patient'))
    // console.log(data.length, 'dataddd');


    // return;
    if (!patien) {
        $("#confirmOrder").modal('show');
    }else{
        window.location.href = '/checkout';
    }
}

function doOrder() {
    let requestData = {
        'patient_bed' : $("#patientBed").val(),
        'patient_floor' :  $("#patientFloor").val(),
        'patient_name' : $("#patientName").val(),
        'patient_phone' : $("#patientPhone").val(),
        'order_data' : data,
        'total_price_order': totalOrder
    }

    if ($('#userId').val() != 0) {
        requestData.user_id = $('#userId').val()
    }

    if(requestData.patient_bed == '') return alert('Patient Bed cannot be empty');
    if(requestData.patient_floor == '') return alert('Patient Floor cannot be empty');
    if(requestData.patient_name == '') return alert('Patient Name cannot be empty');
    if(requestData.patient_phone == '') return alert('Patient Phone cannot be empty');

    localStorage.setItem('patient', JSON.stringify(requestData))

    // console.log(requestData.patient_bed);

    window.location.href = '/checkout';
}

function order(){
    let patien = JSON.parse(localStorage.getItem('patient'))
    if (data.length<=0) {
        alert('item is empty')
        return;
    }

    $.ajax({
        type: "POST",
        url: '/order/create',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data : patien,
        success: function (response) {
            if (response.code === 0) {
                window.location.href = '/order-proof';
                localStorage.setItem('orderNumber', response.data.data)
                localStorage.removeItem('data')
            } else {
                console.log("get data error ")
            }
        },
        error: function (request, status, error) {
            console.log(request)
            alert(request.responseJSON.message)
        }
    })
}
