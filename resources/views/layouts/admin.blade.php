<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <script src="{{ asset('js/app.js') }}" defer></script>
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/fonts/circular-std/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/fonts/fontawesome/css/fontawesome-all.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/charts/chartist-bundle/chartist.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/charts/morris-bundle/morris.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/fonts/material-design-iconic-font/css/materialdesignicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/charts/c3charts/c3.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/fonts/flag-icon-css/flag-icon.min.css') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Ausvj - Admin</title>
    @yield('css')
    <script src="https://cdn.tiny.cloud/1/8866op4d2agb54tf1mecjkc5pl13buy0jsqxp5w0ph0yktni/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
</head>
<body>
    <!-- ============================================================== -->
    <!-- main wrapper -->
    <!-- ============================================================== -->
    <div class="dashboard-main-wrapper">
        @include('layouts.navbar')
        @include('layouts.sidebar')
        <!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        <div class="dashboard-wrapper">

            <div class="dashboard-ecommerce">
                <div class="container-fluid dashboard-content">
                @include('layouts.breadcrumb')
                @yield('content')

                </div>
            </div>
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <div class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                             Copyright © 2021 Concept. All rights reserved. Dashboard by <a href="https://xenren.co/">Xenren</a>.
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="text-md-right footer-links d-none d-sm-block">
                                <a href="javascript: void(0);">About</a>
                                <a href="javascript: void(0);">Support</a>
                                <a href="javascript: void(0);">Contact Us</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- end wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- end main wrapper  -->
    <!-- ============================================================== -->
    <!-- Optional JavaScript -->
    <!-- jquery 3.3.1 -->
    <script src="{{ asset('vendor/jquery/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
    <!-- bootstap bundle js -->
    <!-- <script src="assets/vendor/bootstrap/js/bootstrap.bundle.js"></script> -->
    <!-- slimscroll js -->
    <script src="{{ asset('vendor/slimscroll/jquery.slimscroll.js') }}"></script>
    <!-- main js -->
    <script src="{{ asset('js/admin.js') }}"></script>
    <!-- chart chartist js -->
    <!-- <script src="{{ asset('vendor/charts/chartist-bundle/chartist.min.js') }}"></script> -->
    <!-- sparkline js -->
    <!-- <script src="{{ asset('vendor/charts/sparkline/jquery.sparkline.js') }}"></script> -->
    <!-- morris js -->
    <!-- <script src="{{ asset('vendor/charts/morris-bundle/raphael.min.js') }}"></script> -->
    <!-- <script src="{{ asset('vendor/charts/morris-bundle/morris.js') }}"></script> -->
    <!-- chart c3 js -->
    <!-- <script src="{{ asset('vendor/charts/c3charts/c3.min.js') }}"></script>
    <script src="{{ asset('vendor/charts/c3charts/d3-5.4.0.min.js') }}"></script>
    <script src="{{ asset('vendor/charts/c3charts/C3chartjs.js') }}"></script>
    <script src="{{ asset('js/dashboard.js') }}"></script> -->
<!--    <script type="text/javascript" src="{{ asset('js/vue.min.js') }}"></script>-->
<!--    <script type="text/javascript" src="{{ asset('js/vue-resource.min.js') }}"></script>-->
    <script type="text/javascript" src="{{ asset('js/sweetalert.min.js') }}"></script>

    @yield('js')
</body>

</html>
