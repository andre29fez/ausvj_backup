<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }} - Best trusted gaming agency</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <link rel="stylesheet" href="{{ asset('css/mixing.min.css') }}" rel="stylesheet">
  <link rel="icon" type="image/png"  href="favicon.png">
    <script src="{{ asset('js/app.js') }}" defer></script>
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Baskervville:400,400i|Carme&display=swap" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        .float{
            position:fixed;
            width:60px;
            height:60px;

            background-color:#25d366;
            color:#FFF;
            border-radius:50px;
            text-align:center;
            font-size:30px;
            box-shadow: 2px 2px 3px #999;
            z-index:100;
        }
        .telegram-icon{
            position:fixed;
            width:60px;
            height:60px;
            z-index:100;
        }
        @media only screen and (max-width: 900px) {
            /* For mobile phones: */
            .float{
                right:10px;
                bottom:135px;
            }
            .telegram-icon{
                right:10px;
                bottom:60px;
            }
        }
        @media only screen and (min-width: 900px) {
            /* For mobile phones: */
            .float{
                left:30px;
                bottom:140px;
            }
            .telegram-icon{
                left:30px;
                bottom:50px;
            }
        }


        .my-float{
            margin-top:16px;
        }
    </style>
    @yield('css')
    @stack('scripts')
</head>
<body class="homepage">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<a href="http://wa.me/60143969676" class="float" target="_blank">
    <i class="fa fa-whatsapp my-float"></i>
</a>

<a href="https://t.me/Alyssa_2000wong" target="_blank" class="telegram-icon">
    <img style="width: 60px" src="/images/telegram-logo.png">

</a>
    @yield('content')
</body>
</html>
