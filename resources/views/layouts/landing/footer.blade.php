<!-- Footer -->
<div class="osahan-menu-fotter fixed-bottom bg-white px-3 py-2 text-center d-none">
    <div class="row">
        <div class="col">
            <a href="home.html" class="text-danger small font-weight-bold text-decoration-none">
                <p class="h4 m-0"><i class="feather-home text-danger"></i></p>
                Home
            </a>
        </div>
        {{-- <div class="col">
            <a href="most_popular.html" class="text-dark small font-weight-bold text-decoration-none">
                <p class="h4 m-0"><i class="feather-map-pin"></i></p>
                Trending
            </a>
        </div> --}}
        <div class="col bg-white rounded-circle mt-n4 px-3 py-2">
            <div class="bg-danger rounded-circle mt-n0 shadow">
                <a href="checkout.html" class="text-white small font-weight-bold text-decoration-none">
                    <i class="feather-shopping-cart"></i>
                </a>
            </div>
        </div>
        {{-- <div class="col">
            <a href="favorites.html" class="text-dark small font-weight-bold text-decoration-none">
                <p class="h4 m-0"><i class="feather-heart"></i></p>
                Favorites
            </a>
        </div> --}}
        <div class="col">
            <a href="profile.html" class="text-dark small font-weight-bold text-decoration-none">
                <p class="h4 m-0"><i class="feather-user"></i></p>
                Profile
            </a>
        </div>
    </div>
</div>
<!-- footer -->
<footer class="section-footer border-top bg-dark">

    <div class="container">
        <section class="footer-top padding-y py-5">
            <div class="row">
                <aside class="col-md-4 footer-about">
                    <article class="d-flex pb-3">
                        <div><img alt="#" src="{{ asset('/images/fresco-logo.png') }}" class="logo-footer mr-3"></div>
                        <div>
                            <h6 class="title text-white">About Us</h6>
                            <p class="text-muted">Some short text about company like You might remember the Dell computer commercials in which a youth reports.</p>
                            <div class="d-flex align-items-center">
                                <a class="btn btn-icon btn-outline-light mr-1 btn-sm" title="Facebook" target="_blank" href="#"><i class="feather-facebook"></i></a>
                                <a class="btn btn-icon btn-outline-light mr-1 btn-sm" title="Instagram" target="_blank" href="#"><i class="feather-instagram"></i></a>
                                <a class="btn btn-icon btn-outline-light mr-1 btn-sm" title="Youtube" target="_blank" href="#"><i class="feather-youtube"></i></a>
                                <a class="btn btn-icon btn-outline-light mr-1 btn-sm" title="Twitter" target="_blank" href="#"><i class="feather-twitter"></i></a>
                            </div>
                        </div>
                    </article>
                </aside>
                <aside class="col-sm-3 col-md-2 text-white">
                    <h6 class="title">Error Pages</h6>
                    <ul class="list-unstyled hov_footer">
                        <li> <a href="not-found.html" class="text-muted">Not found</a></li>
                        <li> <a href="maintence.html" class="text-muted">Maintence</a></li>
                        <li> <a href="coming-soon.html" class="text-muted">Coming Soon</a></li>
                    </ul>
                </aside>
                <aside class="col-sm-3 col-md-2 text-white">
                    <h6 class="title">Services</h6>
                    <ul class="list-unstyled hov_footer">
                        <li> <a href="faq.html" class="text-muted">Delivery Support</a></li>
                        <li> <a href="contact-us.html" class="text-muted">Contact Us</a></li>
                        <li> <a href="terms.html" class="text-muted">Terms of use</a></li>
                        <li> <a href="privacy.html" class="text-muted">Privacy policy</a></li>
                    </ul>
                </aside>
                <aside class="col-sm-3  col-md-2 text-white">
                    <h6 class="title">For users</h6>
                    <ul class="list-unstyled hov_footer">
                        <li> <a href="login.html" class="text-muted"> User Login </a></li>
                        <li> <a href="signup.html" class="text-muted"> User register </a></li>
                        <li> <a href="forgot_password.html" class="text-muted"> Forgot Password </a></li>
                        <li> <a href="profile.html" class="text-muted"> Account Setting </a></li>
                    </ul>
                </aside>
                <aside class="col-sm-3  col-md-2 text-white">
                    <h6 class="title">More Pages</h6>
                    <ul class="list-unstyled hov_footer">
                        <li> <a href="trending.html" class="text-muted"> Trending </a></li>
                        <li> <a href="most_popular.html" class="text-muted"> Most popular </a></li>
                        <li> <a href="javascript:void(0)" class="text-muted"> Restaurant Details </a></li>
                        <li> <a href="favorites.html" class="text-muted"> Favorites </a></li>
                    </ul>
                </aside>
            </div>
            <!-- row.// -->
        </section>
        <!-- footer-top.// -->
        <section class="footer-center border-top padding-y py-5">
            <h6 class="title text-white">Countries</h6>
            <div class="row">
                <aside class="col-sm-2 col-md-2 text-white">
                    <ul class="list-unstyled hov_footer">
                        <li> <a href="#" class="text-muted">India</a></li>
                        <li> <a href="#" class="text-muted">Indonesia</a></li>
                        <li> <a href="#" class="text-muted">Ireland</a></li>
                        <li> <a href="#" class="text-muted">Italy</a></li>
                        <li> <a href="#" class="text-muted">Lebanon</a></li>
                    </ul>
                </aside>
                <aside class="col-sm-2 col-md-2 text-white">
                    <ul class="list-unstyled hov_footer">
                        <li> <a href="#" class="text-muted">Malaysia</a></li>
                        <li> <a href="#" class="text-muted">New Zealand</a></li>
                        <li> <a href="#" class="text-muted">Philippines</a></li>
                        <li> <a href="#" class="text-muted">Poland</a></li>
                        <li> <a href="#" class="text-muted">Portugal</a></li>
                    </ul>
                </aside>
                <aside class="col-sm-2 col-md-2 text-white">
                    <ul class="list-unstyled hov_footer">
                        <li> <a href="#" class="text-muted">Australia</a></li>
                        <li> <a href="#" class="text-muted">Brasil</a></li>
                        <li> <a href="#" class="text-muted">Canada</a></li>
                        <li> <a href="#" class="text-muted">Chile</a></li>
                        <li> <a href="#" class="text-muted">Czech Republic</a></li>
                    </ul>
                </aside>
                <aside class="col-sm-2 col-md-2 text-white">
                    <ul class="list-unstyled hov_footer">
                        <li> <a href="#" class="text-muted">Turkey</a></li>
                        <li> <a href="#" class="text-muted">UAE</a></li>
                        <li> <a href="#" class="text-muted">United Kingdom</a></li>
                        <li> <a href="#" class="text-muted">United States</a></li>
                        <li> <a href="#" class="text-muted">Sri Lanka</a></li>
                    </ul>
                </aside>
                <aside class="col-sm-2 col-md-2 text-white">
                    <ul class="list-unstyled hov_footer">
                        <li> <a href="#" class="text-muted">Qatar</a></li>
                        <li> <a href="#" class="text-muted">Singapore</a></li>
                        <li> <a href="#" class="text-muted">Slovakia</a></li>
                        <li> <a href="#" class="text-muted">South Africa</a></li>
                        <li> <a href="#" class="text-muted">Green Land</a></li>
                    </ul>
                </aside>
                <aside class="col-sm-2 col-md-2 text-white">
                    <ul class="list-unstyled hov_footer">
                        <li> <a href="#" class="text-muted">Pakistan</a></li>
                        <li> <a href="#" class="text-muted">Bangladesh</a></li>
                        <li> <a href="#" class="text-muted">Bhutaan</a></li>
                        <li> <a href="#" class="text-muted">Nepal</a></li>
                    </ul>
                </aside>
            </div>
            <!-- row.// -->
        </section>
    </div>
    <!-- //container -->
    <section class="footer-copyright border-top py-3 bg-light">
        <div class="container d-flex align-items-center">
            <p class="mb-0"> © 2020 Company All rights reserved </p>
            <p class="text-muted mb-0 ml-auto d-flex align-items-center">
                <a href="#" class="d-block"><img alt="#" src="{{ asset('/themes/img/appstore.png') }}" height="40"></a>
                <a href="#" class="d-block ml-3"><img alt="#" src="{{ asset('/themes/img/playmarket.png') }}" height="40"></a>
            </p>
        </div>
    </section>

</footer>
