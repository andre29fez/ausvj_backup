<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Askbootstrap">
    <meta name="author" content="Askbootstrap">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" href="{{ asset('/themes/img/fav.png') }}">
    <title>AUSVJ Login</title>
    <!-- Slick Slider -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/themes/vendor/slick/slick.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('/themes/vendor/slick/slick-theme.min.css') }}" />
    <!-- Feather Icon-->
    <link href="{{ asset('/themes/vendor/icons/feather.css') }}" rel="stylesheet" type="text/css">
    <!-- Bootstrap core CSS -->
    <link href="{{ asset('/themes/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="{{ asset('/themes/css/style.css') }}" rel="stylesheet">
    <!-- Sidebar CSS -->
    <link href="{{ asset('/themes/vendor/sidebar/demo.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/magnific-popup.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/magnific-popup.min.css') }}" rel="stylesheet">
    <script type="text/javascript">
        function googleTranslateElementInit() {
            new google.translate.TranslateElement(
                {pageLanguage: 'en'},
                'google_translate_element'
            );
        }
    </script>
    <style>
        .tab {
            overflow: hidden;
            border: 1px solid #ccc;
            background-color: #f1f1f1;
            float: right;
            border-radius: 20px;
            margin-left: 50px;
        }
        .tab button {
            background-color: inherit;
            float: left;
            border: none;

            outline: none;
            cursor: pointer;
            padding: 14px 16px;
            transition: 0.3s;
            font-size: 13px;
        }

        /* Change background color of buttons on hover */
        .tab button:hover {
            background-color: #ddd;
        }

        /* Create an active/current tablink class */
        .tab button.active {
            background-color: #ccc;
        }

        /* Style the tab content */
        .tabcontent {
            display: none;
            padding: 6px 12px;
            border: 1px solid #ccc;
            border-top: none;
        }
    </style>

    <script type="text/javascript"
            src=
            "https://translate.google.com/translate_a/element.js?
cb=googleTranslateElementInit">
    </script>
</head>

<body class="fixed-bottom-bar">

    @yield('content')

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Filter</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body p-0">
                    <div class="osahan-filter">
                        <div class="filter">
                            <!-- SORT BY -->
                            <div class="p-3 bg-light border-bottom">
                                <h6 class="m-0">SORT BY</h6>
                            </div>
                            <div class="custom-control border-bottom px-0  custom-radio">
                                <input type="radio" id="customRadio1f" name="location" class="custom-control-input" checked>
                                <label class="custom-control-label py-3 w-100 px-3" for="customRadio1f">Top Rated</label>
                            </div>
                            <div class="custom-control border-bottom px-0  custom-radio">
                                <input type="radio" id="customRadio2f" name="location" class="custom-control-input">
                                <label class="custom-control-label py-3 w-100 px-3" for="customRadio2f">Nearest Me</label>
                            </div>
                            <div class="custom-control border-bottom px-0  custom-radio">
                                <input type="radio" id="customRadio3f" name="location" class="custom-control-input">
                                <label class="custom-control-label py-3 w-100 px-3" for="customRadio3f">Cost High to Low</label>
                            </div>
                            <div class="custom-control border-bottom px-0  custom-radio">
                                <input type="radio" id="customRadio4f" name="location" class="custom-control-input">
                                <label class="custom-control-label py-3 w-100 px-3" for="customRadio4f">Cost Low to High</label>
                            </div>
                            <div class="custom-control border-bottom px-0  custom-radio">
                                <input type="radio" id="customRadio5f" name="location" class="custom-control-input">
                                <label class="custom-control-label py-3 w-100 px-3" for="customRadio5f">Most Popular</label>
                            </div>
                            <!-- Filter -->
                            <div class="p-3 bg-light border-bottom">
                                <h6 class="m-0">FILTER</h6>
                            </div>
                            <div class="custom-control border-bottom px-0  custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="defaultCheck1" checked>
                                <label class="custom-control-label py-3 w-100 px-3" for="defaultCheck1">Open Now</label>
                            </div>
                            <div class="custom-control border-bottom px-0  custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="defaultCheck2">
                                <label class="custom-control-label py-3 w-100 px-3" for="defaultCheck2">Credit Cards</label>
                            </div>
                            <div class="custom-control border-bottom px-0  custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="defaultCheck3">
                                <label class="custom-control-label py-3 w-100 px-3" for="defaultCheck3">Alcohol Served</label>
                            </div>
                            <!-- Filter -->
                            <div class="p-3 bg-light border-bottom">
                                <h6 class="m-0">ADDITIONAL FILTERS</h6>
                            </div>
                            <div class="px-3 pt-3">
                                <input type="range" class="custom-range" min="0" max="100" name="minmax">
                                <div class="form-row">
                                    <div class="form-group col-6">
                                        <label>Min</label>
                                        <input class="form-control" placeholder="$0" type="number">
                                    </div>
                                    <div class="form-group text-right col-6">
                                        <label>Max</label>
                                        <input class="form-control" placeholder="$1,0000" type="number">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer p-0 border-0">
                    <div class="col-6 m-0 p-0">
                        <button type="button" class="btn border-top btn-lg btn-block" data-dismiss="modal">Close</button>
                    </div>
                    <div class="col-6 m-0 p-0">
                        <button type="button" class="btn btn-primary btn-lg btn-block">Apply</button>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- extras modal -->
    <div class="modal fade" id="extras" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Extras</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                    <form id="formQty">
                        <!-- extras body -->
                        <div class="recepie-body">
                            {{-- <div class="custom-control custom-radio border-bottom py-2">
                                <input type="radio" id="customRadio1f" name="location" class="custom-control-input" checked>
                                <label class="custom-control-label" for="customRadio1f">Tuna <span class="text-muted">+$35.00</span></label>
                            </div> --}}
                            <input type="hidden" id="foodId" name="foodId" value="">
                            <input type="hidden" id="foodName" name="foodName" value="">
                            <input type="hidden" id="foodPrice" name="foodPrice" value="">
                            <input type="hidden" id="foodCatgory" name="foodCatgory" value="">
                            <h6 class="font-weight-bold mt-4">QUANTITY</h6>
                            <div class="d-flex align-items-center">
                                <p class="m-0">1 Item</p>
                                <div class="ml-auto">
                                    <span class="count-number"><button onclick="extrasQty('sub')" type="button" id="sub" class="btn-sm left dec btn btn-outline-secondary"> <i class="feather-minus"></i> </button><input class="count-number-input" id="qty" type="text" value="1"><button onclick="extrasQty('add')" type="button" id="add" class="btn-sm right inc btn btn-outline-secondary"> <i class="feather-plus"></i> </button></span>
                                    <div id="msg"></div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer p-0 border-0">
                    <div class="col-6 m-0 p-0">
                        <button type="button" class="btn border-top btn-lg btn-block" data-dismiss="modal">Close</button>
                    </div>
                    <div class="col-6 m-0 p-0">
                        <button type="button" onclick="storeQty()" class="btn btn-primary btn-lg btn-block">Apply</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Confrim order modal -->
    <div class="modal fade" id="confirmOrder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Confirm Order</h5>
                    <div class="tab">
                        <button class="tablinks" onclick="openCity(event, 'eng')">English</button>
                        <button class="tablinks" onclick="openCity(event, 'cn')">Chinese</button>
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                        <!-- extras body -->


                    @if (Auth::check())
                    <input type="hidden" id="userId" name="userId" value="{{ Auth::user()->id }}">
                    @else
                    <input type="hidden" id="userId" name="userId" value="0">
                    @endif
                    <div class="form-group">
                        <label for="patientBed">Patient Bed</label>
                        <input type="number" id="patientBed" name="patientBed" class="form-control"  required>
                    </div>
                    <div class="form-group">
                        <label for="patientFloor">Patient Floor</label>
                        <input type="number" id="patientFloor" name="patientFloor" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="patientName">Patient Name</label>
                        <input type="text" id="patientName" name="patientName" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="patientPhone">Patient Phone</label>
                        <input type="number" id="patientPhone" name="patientPhone" class="form-control" required>
                    </div>

                </div>
                <div class="modal-footer p-0 border-0">

                    <div class="col-6 m-0 p-0">
                        <button type="button" class="btn border-top btn-lg btn-block" data-dismiss="modal" id="closeBtn">Close</button>
                    </div>
                    <div class="col-6 m-0 p-0">
                        <button type="button" onclick="doOrder()" class="btn btn-primary btn-lg btn-block" id="checkoutBtn">Checkout</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="{{ asset('/themes/vendor/jquery/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/themes/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- slick Slider JS-->
    <script type="text/javascript" src="{{ asset('/themes/vendor/slick/slick.min.js') }}"></script>
    <!-- Sidebar JS-->
    <script type="text/javascript" src="{{ asset('/themes/vendor/sidebar/hc-offcanvas-nav.js') }}"></script>
    <!-- Custom scripts for all pages-->
    <script type="text/javascript" src="{{ asset('/themes/js/osahan.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.magnific-popup.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.magnific-popup.js') }}"></script>
    <script type="text/javascript" src="/js/app-custom.js"></script>
    <script>
        createCart();
        function openCity(evt, language) {
            console.log(language)
            if (language == "cn") {
                $("label[for='patientBed']").text("病床")
                $("label[for='patientFloor']").text("病房楼层")
                $("label[for='patientName']").text("患者姓名")
                $("label[for='patientPhone']").text("病人电话")
                $("#checkoutBtn").text("查看")
                $("#closeBtn").text("关")
            }
            else {
                $("label[for='patientBed']").text("Patient Bed")
                $("label[for='patientFloor']").text("Patient Floor")
                $("label[for='patientName']").text("Patient Name")
                $("label[for='patientPhone']").text("Patient Phone")
                $("#checkoutBtn").text("Checkout")
                $("#closeBtn").text("Close")
            }
        }
    </script>

    @stack('js')
</body>

</html>
