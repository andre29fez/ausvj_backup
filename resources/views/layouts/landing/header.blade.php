<header class="section-header">
    <section class="header-main shadow-sm bg-white">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-1">
                    <a href="{{ url('/') }}" class="brand-wrap mb-0">
                        <img alt="#" class="img-fluid" src="{{ asset('/images/fresco-logo.png') }}">
                    </a>
                    <!-- brand-wrap.// -->
                </div>
                <div class="col-3 d-flex align-items-center m-none">

                </div>
                <!-- col.// -->
                <div class="col-8">
                    <div class="d-flex align-items-center justify-content-end pr-5">
                        <!-- signin -->
                        @if (Auth::check() == false)
                        <a href="{{ route('login') }}" class="widget-header mr-4 text-dark m-none">
                            <div class="icon d-flex align-items-center">
                                <i class="feather-user h6 mr-2 mb-0"></i> <span>Sign in </span>
                            </div>
                        </a>
                        @endif
                        <!-- my account -->
                        <div class="dropdown mr-4 m-none">
                            @if (Auth::check())
                                <a href="#" class="dropdown-toggle text-dark py-3 d-block" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img alt="#" src="{{ asset('/themes/img/user/1.jpg') }}" class="img-fluid rounded-circle header-user mr-2 header-user"> Hi {{ Auth::user()->name }}
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="profile.html">My account</a>
                                    <a class="dropdown-item" href="faq.html">Delivery support</a>
                                    <a class="dropdown-item" href="contact-us.html">Contant us</a>
                                    <a class="dropdown-item" href="terms.html">Term of use</a>
                                    <a class="dropdown-item" href="privacy.html">Privacy policy</a>
                                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            @else
                                <a href="#" class="text-dark py-3 d-block" id="dropdownMenuButton" >
                                    <img alt="#" src="{{ asset('/themes/img/user/1.jpg') }}" class="img-fluid rounded-circle header-user mr-2 header-user"> Hi
                                </a>
                            @endif
                        </div>
                        <!-- signin -->
                        <a href="checkout.html" class="widget-header mr-4 text-dark">
                            <div class="icon d-flex align-items-center">
                                <i class="feather-shopping-cart h6 mr-2 mb-0"></i> <span>Cart</span>
                            </div>
                        </a>
                        <a class="toggle" href="#">
                            <span></span>
                        </a>
                    </div>
                    <!-- widgets-wrap.// -->
                </div>
                <!-- col.// -->
            </div>
            <!-- row.// -->
        </div>
        <!-- container.// -->
    </section>
    <!-- header-main .// -->
</header>
