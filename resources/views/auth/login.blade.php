@extends('layouts.landing.app')


@section('content')
    <div class="login-page vh-100">
        <div class="d-flex align-items-center justify-content-center vh-100">
            <div class="px-5 col-md-12 ml-auto">
                <div class="px-5 col-12 mx-auto">
                    <h2 class="text-dark my-0">Welcome Back</h2>
                    <p class="text-50">Sign in to continue</p>
                    <form class="mt-5 mb-4" method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group">
                            <label for="exampleInputEmail1" class="text-dark">Email</label>
                            <input type="email" name="email" placeholder="Enter Email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1" class="text-dark">Password</label>
                            <input type="password" name="password" placeholder="Enter Password" class="form-control" id="exampleInputPassword1">
                        </div>
                        <button type="submit" class="btn btn-primary btn-lg btn-block">SIGN IN</button>
                    </form>
                    @if (Route::has('password.request'))
                        <a class="text-decoration-none" href="{{ route('password.request') }}">
                            <p class="text-center">Forgot your password?</p>
                        </a>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
