<!DOCTYPE html>
<!-- saved from url=(0037)index.html -->
<html lang="zh" class="enable-scroll">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>

    <link rel="icon" sizes="192x192" href="icon.png">
    <link rel="shortcut icon" href="icon.png" type="image/x-icon">
    <link rel="apple-touch-icon" href="icon.png" type="image/x-icon">
    5<!-- Safari Pinned Tab Icon -->
    <!-- <link rel="mask-icon" href="icon.png"> -->

    <!-- Legacy Polyfills -->
    <script type="text/javascript" async="" src="/index_files/pinit_main.js"></script>
    <script src="/index_files/bundle.min.js"></script><script nomodule="" src="/index_files/minified.js"></script>
    <script nomodule="" src="/index_files/focus-within-polyfill.js"></script>
    <script nomodule="" src="/index_files/polyfill.min(1).js"></script>

    <!-- Performance API Polyfills -->
    <script>
        (function () {
            var noop = function noop() {};
            if ("performance" in window === false) {
                window.performance = {};
            }
            window.performance.mark = performance.mark || noop;
            window.performance.measure = performance.measure || noop;
            if ("now" in window.performance === false) {
                var nowOffset = Date.now();
                if (performance.timing && performance.timing.navigationStart) {
                    nowOffset = performance.timing.navigationStart;
                }
                window.performance.now = function now() {
                    return Date.now() - nowOffset;
                };
            }
        })();
    </script>

    <!-- Globals Definitions -->
    <script>
        (function () {
            var now = Date.now()
            window.initialTimestamps = {
                initialTimestamp: now,
                initialRequestTimestamp: Math.round(performance.timeOrigin ? performance.timeOrigin : now - performance.now())
            }

            window.thunderboltTag = "libs-releases-GA-local"
            window.thunderboltVersion = "1.10991.0"
        })();
    </script>


    <!-- Old Browsers Deprecation -->
    <script data-url="https://static.parastorage.com/services/wix-thunderbolt/dist/webpack-runtime.61e868e1.bundle.min.js">!function(){"use strict";var e,a,n,t,r,o,i,d={},s={};function c(e){var a=s[e];if(void 0!==a)return a.exports;var n=s[e]={id:e,loaded:!1,exports:{}};return d[e].call(n.exports,n,n.exports,c),n.loaded=!0,n.exports}c.m=d,e=[],c.O=function(a,n,t,r){if(!n){var o=1/0;for(s=0;s<e.length;s++){n=e[s][0],t=e[s][1],r=e[s][2];for(var i=!0,d=0;d<n.length;d++)(!1&r||o>=r)&&Object.keys(c.O).every((function(e){return c.O[e](n[d])}))?n.splice(d--,1):(i=!1,r<o&&(o=r));i&&(e.splice(s--,1),a=t())}return a}r=r||0;for(var s=e.length;s>0&&e[s-1][2]>r;s--)e[s]=e[s-1];e[s]=[n,t,r]},c.n=function(e){var a=e&&e.__esModule?function(){return e.default}:function(){return e};return c.d(a,{a:a}),a},n=Object.getPrototypeOf?function(e){return Object.getPrototypeOf(e)}:function(e){return e.__proto__},c.t=function(e,t){if(1&t&&(e=this(e)),8&t)return e;if("object"==typeof e&&e){if(4&t&&e.__esModule)return e;if(16&t&&"function"==typeof e.then)return e}var r=Object.create(null);c.r(r);var o={};a=a||[null,n({}),n([]),n(n)];for(var i=2&t&&e;"object"==typeof i&&!~a.indexOf(i);i=n(i))Object.getOwnPropertyNames(i).forEach((function(a){o[a]=function(){return e[a]}}));return o.default=function(){return e},c.d(r,o),r},c.d=function(e,a){for(var n in a)c.o(a,n)&&!c.o(e,n)&&Object.defineProperty(e,n,{enumerable:!0,get:a[n]})},c.f={},c.e=function(e){return Promise.all(Object.keys(c.f).reduce((function(a,n){return c.f[n](e,a),a}),[]))},c.u=function(e){return 4767===e?"bootstrap-features.2fbcde07.bundle.min.js":2081===e?"render-indicator.inline.8a3b699a.bundle.min.js":({66:"protectedPages",73:"santa-langs-ko",117:"santa-langs-ru",263:"widget",356:"autoDisplayLightbox",438:"santa-langs-hi",506:"browserNotifications",515:"gift-card-index",638:"groups-post-index",679:"events-page-structured-data-index",687:"triggersAndReactions",763:"bookings-service-index",783:"video-component-index",788:"sosp",819:"richTextBox",872:"componentsqaapi",879:"santa-langs-he",964:"popups",1140:"ghostRefComp",1193:"addressInput",1218:"wixDomSanitizer",1220:"santa-langs-en",1256:"pageTransitions",1344:"Repeater_FluidColumns",1386:"santa-langs-sl",1398:"editorWixCodeSdk",1407:"TPAModal",1475:"tpaWidgetNativeDeadComp",1477:"santa-langs-pt",1711:"debug",1954:"santa-langs-sv",2015:"santa-langs-th",2044:"tpaModuleProvider",2084:"santa-langs-sk",2144:"module-executor",2192:"ooiTpaSharedConfig",2220:"socialUrl",2262:"TPAPopup",2300:"multilingual",2348:"chat",2351:"dashboardWixCodeSdk",2355:"widgetWixCodeSdk",2395:"ContentReflowBanner",2519:"santa-langs-de",2553:"pageAnchors",2646:"coBranding",2700:"loginSocialBar",2750:"headerContainer",2896:"fileUploader",2945:"bookings-form-index",3015:"santa-langs-bg",3048:"seoTpa",3071:"portfolio-projects-index",3080:"santa-langs-no",3133:"static-page-v2-schema-presets-index",3184:"tinyMenu",3198:"quickActionBar",3272:"santa-langs-es",3275:"santa-langs-tl",3366:"passwordProtectedPage",3392:"repeaters",3408:"menuContainer",3639:"businessManager",3679:"cookiesManager",3742:"blog-hashtags-index",3749:"ooi",3756:"static-page-index",3843:"santa-langs-da",3955:"TPAWorker",3973:"Repeater_FixedColumns",3987:"schedule-page-index",4102:"AppPart",4152:"santa-langs-hu",4157:"santa-langs-nl",4222:"challenges-page-index",4281:"qaApi",4471:"activePopup",4522:"siteMembers",4698:"headerPlaceholderHeight",4766:"animations-vendors",4792:"safari-browser-notifications",4794:"portfolio-collections-index",4813:"wixCustomElementComponent",4919:"stores-product-schema-presets-index",4932:"santa-langs-vi",5060:"santa-platform-utils",5170:"events-page-index",5246:"TPAPreloaderOverlay",5262:"wixapps",5535:"contentReflow",5553:"santa-langs-zh",5573:"currentUrl",5776:"tpaCommons",5805:"hoverBox",5810:"presence-lazy",5836:"santa-langs-el",5864:"santa-langs-tr",5880:"scrollVar",5921:"reporter-api",6127:"containerSlider",6173:"santa-langs-fi",6209:"languageSelector",6211:"custom-elements-polyfill",6428:"santa-langs-ar",6464:"SEO_DEFAULT",6496:"santa-langs-ca",6499:"blog-tags-index",6506:"santa-langs-pl",6537:"pro-gallery-item-index",6736:"datePicker",6743:"sliderGallery",6790:"santa-langs-ja",6805:"editorElementsDynamicTheme",7047:"wix-code-sdk-providers",7122:"seo-api-converters",7212:"siteMembersWixCodeSdk",7290:"breadcrumbs-component-index",7294:"intersection-observer-polyfill",7296:"santa-langs-uk",7361:"imageZoom",7482:"url-mapper-utils",7555:"loginButton",7573:"page-features",7597:"tpaWorkerFeature",7607:"platformPubsub",7675:"santa-langs-ms",7678:"fcm-browser-notifications",7698:"SiteStyles",7718:"santa-langs-lt",7738:"backgroundScrub",7745:"platform",7802:"restaurants-order-page-index",7858:"members-area-profile-index",7880:"stores-product-index",7905:"captcha",7935:"santa-langs-cs",7955:"tpa",7971:"wix-resize-observer-polyfill",8007:"customUrlMapper",8094:"blog-post-index",8138:"AppPart2",8148:"santa-langs-id",8179:"testApi",8200:"seo-api",8219:"groups-page-index",8308:"santa-langs-ro",8317:"events-page-calculated-index",8389:"TPAUnavailableMessageOverlay",8391:"dynamicPages",8392:"blog-archive-index",8450:"screenIn",8559:"forum-category-index",8681:"environmentWixCodeSdk",8693:"search-page-index",8794:"searchBox",8834:"santa-langs-fr",8945:"renderIndicator",9030:"authenticationWixCodeSdk",9040:"static-page-v2-index",9110:"forum-post-index",9210:"breadcrumbs",9227:"welcomeScreen",9279:"FontRulersContainer",9292:"mobileActionsMenu",9385:"codeEmbed",9487:"bookings-calendar-index",9540:"blog-category-index",9836:"santa-langs-it",9941:"TPABaseComponent"}[e]||e)+"."+{66:"9a93f372",73:"fc6ad401",117:"5a0cbeb7",263:"309988f6",356:"16e475c3",438:"25c099a9",506:"2e6d8f4f",515:"c042d22f",638:"6d94192a",679:"b61cc005",687:"c9386721",763:"cbcf54b5",783:"9247cbf3",788:"5e15677d",819:"7d293a95",872:"b672034c",879:"e9777357",964:"5098a5a3",1140:"a3adbb84",1193:"d7511170",1218:"57a58b25",1220:"cde5975b",1256:"aa3db23e",1344:"1ea6eb49",1386:"bef88d7d",1398:"47367b7b",1407:"dd01683d",1475:"87f15e08",1477:"09f9ea67",1602:"95e8b027",1711:"6bd3b1af",1954:"7f55767c",1969:"a30cae8a",2015:"bda14823",2044:"0b4ac04d",2084:"5e9341a3",2144:"18a25011",2192:"9087e72f",2220:"48ca4d0c",2262:"340122e6",2300:"852ee276",2348:"f549a2c2",2351:"a45a1f89",2355:"bba33a22",2395:"b65d4571",2519:"d464051a",2553:"fd1f203f",2646:"db66c641",2700:"f0038327",2750:"5695c75c",2896:"8ba1989b",2945:"18365e69",3015:"cbb9af80",3048:"7a8c8755",3071:"4b5eaf3e",3080:"fe16de42",3133:"9d0025bb",3184:"c0f87171",3198:"ffb26fff",3272:"93342caa",3275:"255d5240",3366:"a737eff5",3392:"be021c59",3408:"e49ae7d8",3639:"3ea5197c",3679:"6723c46e",3742:"a7e2b365",3749:"353be548",3756:"d411be6a",3843:"fc311dc7",3955:"c1ef46ed",3973:"04351e29",3987:"3fda2d59",4102:"57e398e5",4152:"9d8b5d34",4157:"fdcde2f4",4222:"673006d1",4281:"32b708fb",4471:"ce7a5657",4522:"d4af4f47",4698:"df1b5a63",4766:"dab4fc5c",4792:"eaabc49f",4794:"0f3022d9",4813:"39902bc6",4919:"e7857637",4932:"4cb861af",5060:"aa0d748a",5170:"2393d58b",5246:"77600aa5",5262:"8481917f",5535:"3f89cfd8",5553:"4b4e52a7",5573:"4c72a489",5776:"3ed36768",5805:"4fc48d6a",5810:"9e293824",5836:"a9a42aac",5864:"1a64db9d",5880:"6d40d651",5921:"2b092af5",6127:"5722b210",6173:"026d7528",6209:"f8862827",6211:"b7b34e46",6307:"2896f830",6428:"42f0d755",6464:"3ebad75a",6496:"91ef91e1",6499:"75d1fce7",6506:"69e90c00",6537:"be1d91d4",6736:"88274f29",6743:"dcf98111",6790:"3efc8d0a",6805:"b084f93b",7047:"c091d2d5",7122:"7b641001",7212:"f77d635e",7290:"26f53ff9",7294:"ec3fb8a2",7296:"4cff13c2",7361:"17ddf2e3",7482:"a7af8b48",7555:"df5b7a6e",7573:"508fbd14",7597:"ab859427",7607:"e7ced280",7675:"6479e773",7678:"53fd76eb",7698:"dbb2f29c",7718:"2e6e1f04",7738:"5b9f91fc",7745:"429e9eb2",7802:"ce758721",7858:"2b132725",7880:"5a33e975",7905:"ff763fa8",7935:"9b9419ee",7955:"c02d88e0",7971:"56f8c1c1",8007:"21fb11c4",8094:"69220556",8138:"c39bf5dc",8148:"a97cb713",8179:"409aab02",8200:"9792f1e7",8219:"1cf4894b",8308:"4288f442",8317:"040967dd",8389:"d2009b2a",8391:"ea883e38",8392:"bde30f65",8450:"50291cf0",8559:"c81875cc",8681:"e050a1e8",8693:"4f513f95",8794:"360a016e",8834:"ebda78a5",8945:"11d9e2b1",9030:"aee50ff8",9040:"24a9234f",9110:"5f38a481",9210:"89291e3c",9227:"72e61a44",9279:"e6e6359e",9292:"6399f382",9385:"d53a03f1",9487:"c442504a",9540:"22b81a00",9836:"415d3a1a",9941:"811264a5"}[e]+".chunk.min.js"},c.miniCssF=function(e){return 2081===e?"render-indicator.inline.110b367c.min.css":{1344:"Repeater_FluidColumns",1407:"TPAModal",1475:"tpaWidgetNativeDeadComp",2262:"TPAPopup",2395:"ContentReflowBanner",3749:"ooi",3973:"Repeater_FixedColumns",4102:"AppPart",5246:"TPAPreloaderOverlay",8138:"AppPart2",8389:"TPAUnavailableMessageOverlay",9279:"FontRulersContainer",9941:"TPABaseComponent"}[e]+"."+{1344:"6a3d85df",1407:"aa0fdb42",1475:"ff659636",2262:"680b0cf2",2395:"f7ab19eb",3749:"abdee6a1",3973:"7652ad2e",4102:"2026c7c9",5246:"62c76388",8138:"9da20422",8389:"52d6fb93",9279:"32ec7a2f",9941:"fdd66901"}[e]+".chunk.min.css"},c.g=function(){if("object"==typeof globalThis)return globalThis;try{return this||new Function("return this")()}catch(e){if("object"==typeof window)return window}}(),c.o=function(e,a){return Object.prototype.hasOwnProperty.call(e,a)},t={},r="_wix_thunderbolt_app:",c.l=function(e,a,n,o){if(t[e])t[e].push(a);else{var i,d;if(void 0!==n)for(var s=document.getElementsByTagName("script"),f=0;f<s.length;f++){var l=s[f];if(l.getAttribute("src")==e||l.getAttribute("data-webpack")==r+n){i=l;break}}i||(d=!0,(i=document.createElement("script")).charset="utf-8",i.timeout=120,c.nc&&i.setAttribute("nonce",c.nc),i.setAttribute("data-webpack",r+n),i.src=e,0!==i.src.indexOf(window.location.origin+"/")&&(i.crossOrigin="anonymous")),t[e]=[a];var u=function(a,n){i.onerror=i.onload=null,clearTimeout(p);var r=t[e];if(delete t[e],i.parentNode&&i.parentNode.removeChild(i),r&&r.forEach((function(e){return e(n)})),a)return a(n)},p=setTimeout(u.bind(null,void 0,{type:"timeout",target:i}),12e4);i.onerror=u.bind(null,i.onerror),i.onload=u.bind(null,i.onload),d&&document.head.appendChild(i)}},c.r=function(e){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(e,"__esModule",{value:!0})},c.nmd=function(e){return e.paths=[],e.children||(e.children=[]),e},c.p="https://static.parastorage.com/services/wix-thunderbolt/dist/",o=function(e){return new Promise((function(a,n){var t=c.miniCssF(e),r=c.p+t;if(function(e,a){for(var n=document.getElementsByTagName("link"),t=0;t<n.length;t++){var r=(i=n[t]).getAttribute("data-href")||i.getAttribute("href");if("stylesheet"===i.rel&&(r===e||r===a))return i}var o=document.getElementsByTagName("style");for(t=0;t<o.length;t++){var i;if((r=(i=o[t]).getAttribute("data-href"))===e||r===a)return i}}(t,r))return a();!function(e,a,n,t){var r=document.createElement("link");r.rel="stylesheet",r.type="text/css",r.onerror=r.onload=function(o){if(r.onerror=r.onload=null,"load"===o.type)n();else{var i=o&&("load"===o.type?"missing":o.type),d=o&&o.target&&o.target.href||a,s=new Error("Loading CSS chunk "+e+" failed.\n("+d+")");s.code="CSS_CHUNK_LOAD_FAILED",s.type=i,s.request=d,r.parentNode.removeChild(r),t(s)}},r.href=a,0!==r.href.indexOf(window.location.origin+"/")&&(r.crossOrigin="anonymous"),document.head.appendChild(r)}(e,r,a,n)}))},i={6658:0},c.f.miniCss=function(e,a){i[e]?a.push(i[e]):0!==i[e]&&{1344:1,1407:1,1475:1,2081:1,2262:1,2395:1,3749:1,3973:1,4102:1,5246:1,8138:1,8389:1,9279:1,9941:1}[e]&&a.push(i[e]=o(e).then((function(){i[e]=0}),(function(a){throw delete i[e],a})))},function(){var e={6658:0};c.f.j=function(a,n){var t=c.o(e,a)?e[a]:void 0;if(0!==t)if(t)n.push(t[2]);else if(6658!=a){var r=new Promise((function(n,r){t=e[a]=[n,r]}));n.push(t[2]=r);var o=c.p+c.u(a),i=new Error;c.l(o,(function(n){if(c.o(e,a)&&(0!==(t=e[a])&&(e[a]=void 0),t)){var r=n&&("load"===n.type?"missing":n.type),o=n&&n.target&&n.target.src;i.message="Loading chunk "+a+" failed.\n("+r+": "+o+")",i.name="ChunkLoadError",i.type=r,i.request=o,t[1](i)}}),"chunk-"+a,a)}else e[a]=0},c.O.j=function(a){return 0===e[a]};var a=function(a,n){var t,r,o=n[0],i=n[1],d=n[2],s=0;for(t in i)c.o(i,t)&&(c.m[t]=i[t]);if(d)var f=d(c);for(a&&a(n);s<o.length;s++)r=o[s],c.o(e,r)&&e[r]&&e[r][0](),e[o[s]]=0;return c.O(f)},n=self.webpackJsonp__wix_thunderbolt_app=self.webpackJsonp__wix_thunderbolt_app||[];n.forEach(a.bind(null,0)),n.push=a.bind(null,n.push.bind(n))}()}();
        //# sourceMappingURL=https://static.parastorage.com/services/wix-thunderbolt/dist/webpack-runtime.61e868e1.bundle.min.js.map</script>
    <script data-url="https://static.parastorage.com/services/wix-thunderbolt/dist/browser-deprecation.inline.d8b320ba.bundle.min.js">(self.webpackJsonp__wix_thunderbolt_app=self.webpackJsonp__wix_thunderbolt_app||[]).push([[785],{70073:function(e,t,n){"use strict";n(1901)},1901:function(e,t,n){"use strict";var o,i,r,a;Object.defineProperty(t,"__esModule",{value:!0});var s,c,d=n(48337),u=window,l=(null===(r=null===(i=null===(o=u.navigator)||void 0===o?void 0:o.userAgent)||void 0===i?void 0:i.toLowerCase)||void 0===r?void 0:r.call(i))||"",p=!!(null===(a=u.document)||void 0===a?void 0:a.documentMode),m=!(!l.match(/msie\s([\d.]+)/)&&!l.match(/trident\/[\d](?=[^\?]+).*rv:([0-9.].)/)),v=p||m,x=!(function(){var e,t,n=document.createElement("style");n.innerHTML=":root { --tmp-var: bold; }",document.head.appendChild(n);var o=!!(u.CSS&&u.CSS.supports&&u.CSS.supports("font-weight","var(--tmp-var)"));return null===(t=null===(e=n.parentNode)||void 0===e?void 0:e.removeChild)||void 0===t||t.call(e,n),o}()&&"string"==typeof document.createElement("div").style.grid&&function(){try{new Function("let x = 1"),new Function("const x = `1`"),new Function("class X {}"),new Function("const x = (a = 0, ...b) => a"),new Function("const x = {...Object}"),new Function("const y = 1; const x = {y}"),new Function("const x = (function*() { yield 1; })().next().value === 1"),new Function("const x = async () => await new Promise(res => res(true))"),new Function("const objWithTrailingComma = {a: 1, b: 2,}"),new Function("const arrWithTrailingComma = [1,2,3,]"),Object.entries({}),Object.values({}),"x".padStart(3,"A").padEnd(5,"B"),Object.getOwnPropertyDescriptor({a:1,b:2},"a"),Object.fromEntries([["a",1]])}catch(e){return!1}return!0}());function b(){var e,t,n;(e=document.getElementById("SITE_CONTAINER"))&&(e.innerHTML=""),t=document.createElement("iframe"),n=function(){var e,t=(null===(e=u.viewerModel)||void 0===e?void 0:e.language.userLanguage)||"en";return"https://static.parastorage.com/services/wix-thunderbolt/dist/deprecation-".concat({pt:1,fr:1,es:1,de:1,ja:1}[t]?t:"en",".").concat("v5",".html")}(),t.setAttribute("src",n),t.setAttribute("style","position: fixed; top: 0; left: 0; width: 100%; height: 100%"),t.onload=function(){document.body.style.visibility="visible"},document.body.appendChild(t),(0,d.reportPhaseStarted)("browser_not_supported")}(v||x)&&(u.__browser_deprecation__=!0,s=document.head||document.getElementsByTagName("head")[0],(c=document.createElement("style")).setAttribute("type","text/css"),c.appendChild(document.createTextNode("body { visibility: hidden; }")),s.appendChild(c),u.Sentry={mute:!0},"complete"===document.readyState?b():document.addEventListener("readystatechange",(function(){"complete"===document.readyState&&b()})))},48337:function(e,t){"use strict";Object.defineProperty(t,"__esModule",{value:!0}),t.reportPhaseStarted=void 0;var n=window;function o(e,t){void 0===t&&(t="");var o=n.fedops.data,i=o.site,r=o.rollout,a=o.fleetConfig,s=o.requestUrl,c=o.frogOnUserDomain;if(!s.includes("suppressbi=true")){var d=i.isResponsive?"thunderbolt-responsive":"thunderbolt",u=r.isDACRollout?1:0,l=r.siteAssetsVersionsRollout?1:0,p=0===a.code||1===a.code?a.code:null,m=document.visibilityState,v={WixSite:1,UGC:2,Template:3}[i.siteType]||0,x=(c?i.externalBaseUrl.replace(/^https?:\/\//,"")+"/_frog":"//frog.wix.com")+"/bolt-performance?src=72&evid="+e+"&appName="+d+"&is_rollout="+p+"&is_sav_rollout="+l+"&is_dac_rollout="+u+"&dc="+i.dc+"&msid="+i.metaSiteId+"&session_id="+i.sessionId+"&vsi="+"xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g,(function(e){var t=16*Math.random()|0;return("x"===e?t:3&t|8).toString(16)}))+"&pv="+m+"&v="+n.thunderboltVersion+"&url="+s+"&st="+v+t;(new Image).src=x}}t.reportPhaseStarted=function(e){var t=Date.now()-n.initialTimestamps.initialTimestamp,i=Date.now()-t;o(28,"&name=".concat(e,"&duration=").concat(i))}}},function(e){"use strict";var t;t=70073,e(e.s=t)}]);
        //# sourceMappingURL=https://static.parastorage.com/services/wix-thunderbolt/dist/browser-deprecation.inline.d8b320ba.bundle.min.js.map</script>


    <!-- registry runtime -->
    <script>
        window.componentsRegistry = {};
        window.componentsRegistry.runtimeReady = new Promise((resolve) => { window.componentsRegistry.manifestsLoadedResolve = resolve });
    </script>


    <!-- Sentry -->

    <script id="sentry">(function(c,t,u,n,p,l,y,z,v){if(c[l] && c[l].mute) {return;}function e(b){if(!w){w=!0;var d=t.getElementsByTagName(u)[0],a=t.createElement(u);a.src=z;a.crossorigin="anonymous";a.addEventListener("load",function(){try{c[n]=q;c[p]=r;var a=c[l],d=a.init;a.init=function(a){for(var b in a)Object.prototype.hasOwnProperty.call(a,b)&&(v[b]=a[b]);d(v)};B(b,a)}catch(A){console.error(A)}});d.parentNode.insertBefore(a,d)}}function B(b,d){try{for(var a=0;a<b.length;a++)if("function"===typeof b[a])b[a]();var f=m.data,g=!1,h=!1;for(a=0;a<f.length;a++)if(f[a].f){h=
            !0;var e=f[a];!1===g&&"init"!==e.f&&d.init();g=!0;d[e.f].apply(d,e.a)}!1===h&&d.init();var k=c[n],l=c[p];for(a=0;a<f.length;a++)f[a].e&&k?k.apply(c,f[a].e):f[a].p&&l&&l.apply(c,[f[a].p])}catch(C){console.error(C)}}for(var g=!0,x=!1,k=0;k<document.scripts.length;k++)if(-1<document.scripts[k].src.indexOf(y)){g="no"!==document.scripts[k].getAttribute("data-lazy");break}var w=!1,h=[],m=function(b){(b.e||b.p||b.f&&-1<b.f.indexOf("capture")||b.f&&-1<b.f.indexOf("showReportDialog"))&&g&&e(h);m.data.push(b)};
            m.data=[];c[l]={onLoad:function(b){h.push(b);g&&!x||e(h)},forceLoad:function(){x=!0;g&&setTimeout(function(){e(h)})}};"init addBreadcrumb captureMessage captureException captureEvent configureScope withScope showReportDialog".split(" ").forEach(function(b){c[l][b]=function(){m({f:b,a:arguments})}});var q=c[n];c[n]=function(b,d,a,f,e){m({e:[].slice.call(arguments)});q&&q.apply(c,arguments)};var r=c[p];c[p]=function(b){m({p:b.reason});r&&r.apply(c,arguments)};g||setTimeout(function(){e(h)})})(window,document,
            "script","onerror","onunhandledrejection","Sentry","605a7baede844d278b89dc95ae0a9123","https://browser.sentry-cdn.com/6.18.2/bundle.min.js",{"dsn":"https://605a7baede844d278b89dc95ae0a9123@sentry-next.wixpress.com/68"});</script>




    <!-- sendFedopsLoadStarted.inline -->
    <script type="application/json" id="wix-fedops">{"data":{"site":{"metaSiteId":"a231884f-e942-45d8-998a-334660ffb56b","userId":"4da0f370-cd96-4f9b-93e3-8b54352029c5","siteId":"efee9b1b-f085-466c-8316-095656a4ebef","externalBaseUrl":"https:\/\/ausvj2002.wixsite.com\/my-site","siteRevision":22,"siteType":"UGC","dc":"ae1","isResponsive":false,"sessionId":"18140576-ac0a-4bf7-bc1f-189f028038e2"},"rollout":{"siteAssetsVersionsRollout":false,"isDACRollout":0,"isTBRollout":false},"fleetConfig":{"fleetName":"thunderbolt-renderer-light","type":"GA","code":0},"requestUrl":"https:\/\/ausvj2002.wixsite.com\/my-site","isInSEO":false}}</script>
    <script>window.fedops = JSON.parse(document.getElementById('wix-fedops').textContent)</script>
    <script data-url="https://static.parastorage.com/services/wix-thunderbolt/dist/bi-common.inline.059bd271.bundle.min.js">(self.webpackJsonp__wix_thunderbolt_app=self.webpackJsonp__wix_thunderbolt_app||[]).push([[142],{87931:function(e,i,n){"use strict";n.d(i,{e:function(){return v}});var t=n(80459),s=n(74751);const o=null;function r([e,i]){return i!==o&&`${e}=${i}`}function a(){const e=document.cookie.match(/_wixCIDX=([^;]*)/);return e&&e[1]}function c(e){if(!e)return o;const i=new URL(decodeURIComponent(e));return i.search="?",encodeURIComponent(i.href)}var d=function(e,{eventType:i,ts:n,tts:t,extra:s=""},d,u){var l;const p=function(e){const i=e.split("&").reduce(((e,i)=>{const[n,t]=i.split("=");return Object.assign(Object.assign({},e),{[n]:t})}),{});return(e,n)=>void 0!==i[e]?i[e]:n}(s),m=(w=d,e=>void 0===w[e]?o:w[e]);var w;let v=!0;const f=null===window||void 0===window?void 0:window.consentPolicyManager;if(f){const e=f.getCurrentConsentPolicy();if(e){const{policy:i}=e;v=!(i.functional&&i.analytics)}}const g=m("requestUrl"),h={src:"29",evid:"3",viewer_name:m("viewerName"),caching:m("caching"),client_id:v?o:a(),dc:m("dc"),microPop:m("microPop"),et:i,event_name:e?encodeURIComponent(e):o,is_cached:m("isCached"),is_platform_loaded:m("is_platform_loaded"),is_rollout:m("is_rollout"),ism:m("isMesh"),isp:0,isjp:m("isjp"),iss:m("isServerSide"),ssr_fb:m("fallbackReason"),ita:p("ita",d.checkVisibility()?"1":"0"),mid:v?o:(null==u?void 0:u.siteMemberId)||o,msid:m("msId"),pid:p("pid",o),pn:p("pn","1"),ref:document.referrer&&!v?encodeURIComponent(document.referrer):o,sar:v?o:p("sar",screen.availWidth?`${screen.availWidth}x${screen.availHeight}`:o),sessionId:v&&f?o:m("sessionId"),siterev:d.siteRevision||d.siteCacheRevision?`${d.siteRevision}-${d.siteCacheRevision}`:o,sr:v?o:p("sr",screen.width?`${screen.width}x${screen.height}`:o),st:m("st"),ts:n,tts:t,url:v?c(g):g,v:(null===window||void 0===window?void 0:window.thunderboltVersion)||"0.0.0",vid:v?o:(null==u?void 0:u.visitorId)||o,bsi:v?o:(null==u?void 0:u.bsi)||o,vsi:m("viewerSessionId"),wor:v||!window.outerWidth?o:`${window.outerWidth}x${window.outerHeight}`,wr:v?o:p("wr",window.innerWidth?`${window.innerWidth}x${window.innerHeight}`:o),_brandId:(null===(l=d.commonConfig)||void 0===l?void 0:l.brand)||o,nt:p("nt",o)};return`https://frog.wix.com/bt?${Object.entries(h).map(r).filter(Boolean).join("&")}`},u=n(62865),l=n(32652),p=n(51173),m=n(94936);const w={WixSite:1,UGC:2,Template:3};const v=function(){const e=(()=>{const{fedops:e,viewerModel:{siteFeaturesConfigs:i,requestUrl:n,site:t,fleetConfig:s,commonConfig:o}}=window,r=(0,u.Q)(window)||(0,p.f)()||(0,l.d)()||(({seo:e})=>(null==e?void 0:e.isInSEO)?"seo":"")(i);return Object.assign(Object.assign({suppressbi:n.includes("suppressbi=true"),initialTimestamp:window.initialTimestamps.initialTimestamp,initialRequestTimestamp:window.initialTimestamps.initialRequestTimestamp,viewerSessionId:e.vsi,viewerName:t.isResponsive?"thunderbolt-responsive":"thunderbolt",siteRevision:String(t.siteRevision),msId:t.metaSiteId,is_rollout:0===s.code||1===s.code?s.code:null,is_platform_loaded:0,requestUrl:encodeURIComponent(n),sessionId:String(t.sessionId),btype:r,isjp:!!r,dc:t.dc,siteCacheRevision:"__siteCacheRevision__",checkVisibility:(()=>{let e=!0;function i(){e=e&&!0!==document.hidden}return document.addEventListener("visibilitychange",i,{passive:!0}),i(),()=>(i(),e)})()},(0,m._)()),{isMesh:1,st:w[t.siteType]||0,commonConfig:o})})(),i={};let n=1;const o=(t,o,r={})=>{const a=Date.now(),c=a-e.initialRequestTimestamp,u=a-e.initialTimestamp;if(function(e,i){if(i&&performance.mark){const n=`${i} (beat ${e})`;performance.mark(n)}}(t,o),e.suppressbi||window.__browser_deprecation__)return;const{pageId:l,pageNumber:p=n,navigationType:m}=r;let w=`&pn=${p}`;l&&(w+=`&pid=${l}`),m&&(w+=`&nt=${m}`);const v=d(o,{eventType:t,ts:u,tts:c,extra:w},e,i);(0,s.Z)(v)};return{sendBeat:o,reportBI:function(e,i){!function(e,i){const n=i?`${e} - ${i}`:e,t="end"===i?`${e} - start`:null;performance.mark(n),performance.measure&&t&&performance.measure(`\u2b50${e}`,t,n)}(e,i)},wixBiSession:e,sendBeacon:s.Z,setDynamicSessionData:({visitorId:e,siteMemberId:n,bsi:t})=>{i.visitorId=e||i.visitorId,i.siteMemberId=n||i.siteMemberId,i.bsi=t||i.bsi},reportPageNavigation:function(e){n+=1,o(t.sT.PAGE_NAVIGATION,"page navigation start",{pageId:e,pageNumber:n})},reportPageNavigationDone:function(e,i){o(t.sT.PAGE_NAVIGATION_DONE,"page navigation complete",{pageId:e,pageNumber:n,navigationType:i}),i!==t.$7.DYNAMIC_REDIRECT&&i!==t.$7.NAVIGATION_ERROR&&i!==t.$7.CANCELED||(n-=1)}}}();window.bi=v,v.sendBeat(1,"Init")},39256:function(e,i,n){"use strict";var t=n(62865),s=n(51173),o=n(32652),r=n(94936),a=n(74751);!function(){const{site:e,rollout:i,fleetConfig:n,requestUrl:c,isInSEO:d,frogOnUserDomain:u,sentryPreload:l}=window.fedops.data,p=(0,t.Q)(window)||(0,s.f)()||(0,o.d)()||(d?"seo":""),m=!!p,{isCached:w,caching:v,microPop:f}=(0,r._)(),g={WixSite:1,UGC:2,Template:3}[e.siteType]||0,h=e.isResponsive?"thunderbolt-responsive":"thunderbolt",{isDACRollout:b,siteAssetsVersionsRollout:_}=i,x=b?1:0,I=_?1:0,$=0===n.code||1===n.code?n.code:null,T=Date.now()-window.initialTimestamps.initialTimestamp,y=Date.now()-window.initialTimestamps.initialRequestTimestamp,{visibilityState:R}=document,C=R,{fedops:S,addEventListener:O,Sentry:E,thunderboltVersion:A}=window;S.apps=S.apps||{},S.apps[h]={startLoadTime:y},S.sessionId=e.sessionId,S.vsi="xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g,(e=>{const i=16*Math.random()|0;return("x"===e?i:3&i|8).toString(16)})),S.is_cached=w,S.phaseStarted=k(28),S.phaseEnded=k(22),E&&l&&!w&&(E.onLoad((()=>{var i;E.init({dsn:"https://831126cb46b74583bf6f72c5061cba9d@sentry-viewer.wixpress.com/4",release:`${A}`.startsWith("1")?A:null,environment:(i=n.code,0===i?"production":1===i?"rollout":"canary"),integrations:E.BrowserTracing?[new E.BrowserTracing]:[],tracesSampleRate:1,initialScope:{tags:{responsive:e.isResponsive,is_dac_rollout:x,is_sav_rollout:I,siteType:g},user:{id:window.fedops.vsi}}})})),E.forceLoad()),S.reportError=e=>{const i=(null==e?void 0:e.reason)||(null==e?void 0:e.message);i?P(26,`&errorInfo=${i}&errorType=load`):e.preventDefault()},O("error",S.reportError),O("unhandledrejection",S.reportError);let N=!1;function P(i,n=""){if(c.includes("suppressbi=true"))return;const t=(u?e.externalBaseUrl.replace(/^https?:\/\//,"")+"/_frog":"//frog.wix.com")+"/bolt-performance?src=72&evid="+i+"&appName="+h+"&is_rollout="+$+"&is_sav_rollout="+I+"&is_dac_rollout="+x+"&dc="+e.dc+(f?"&microPop="+f:"")+"&is_cached="+w+"&msid="+e.metaSiteId+"&session_id="+window.fedops.sessionId+"&ish="+m+"&isb="+m+(m?"&isbr="+p:"")+"&vsi="+window.fedops.vsi+"&caching="+v+(N?",browser_cache":"")+"&pv="+C+"&pn=1&v="+A+"&url="+encodeURIComponent(c)+"&st="+g+`&ts=${T}&tsn=${y}`+n;(0,a.Z)(t)}function k(e){return(i,n)=>{const t=`&name=${i}&duration=${Date.now()-T}`,s=n&&n.paramsOverrides?Object.keys(n.paramsOverrides).map((e=>e+"="+n.paramsOverrides[e])).join("&"):"";P(e,s?`${t}&${s}`:t)}}O("pageshow",(({persisted:e})=>{e&&(N||(N=!0,S.is_cached=!0))}),!0),window.__browser_deprecation__||P(21)}()},94936:function(e,i,n){"use strict";n.d(i,{_:function(){return t}});const t=()=>{let e,i="none",n=document.cookie.match(/ssr-caching="?cache[,#]\s*desc=([\w-]+)(?:[,#]\s*varnish=(\w+))?(?:[,#]\s*dc[,#]\s*desc=([\w-]+))?(?:"|;|$)/);if(!n&&window.PerformanceServerTiming){const i=(()=>{let e,i;try{e=performance.getEntriesByType("navigation")[0].serverTiming||[]}catch(i){e=[]}const n=[];return e.forEach((e=>{switch(e.name){case"cache":n[1]=e.description;break;case"varnish":n[2]=e.description;break;case"dc":i=e.description}})),{microPop:i,matches:n}})();e=i.microPop,n=i.matches}if(n&&n.length&&(i=`${n[1]},${n[2]||"none"}`,e||(e=n[3])),"none"===i){const e=performance.timing;e&&e.responseStart-e.requestStart==0&&(i="browser")}return Object.assign({caching:i,isCached:0===i.indexOf("hit")},e?{microPop:e}:{})}},62865:function(e,i,n){"use strict";n.d(i,{Q:function(){return t}});const t=e=>{const{userAgent:i}=e.navigator;return/instagram.+google\/google/i.test(i)?"":/bot|google(?!play)|phantom|crawl|spider|headless|slurp|facebookexternal|Lighthouse|PTST|^mozilla\/4\.0$|^\s*$/i.test(i)?"ua":""}},51173:function(e,i,n){"use strict";n.d(i,{f:function(){return t}});const t=()=>{try{if(window.self===window.top)return""}catch(e){}return"iframe"}},32652:function(e,i,n){"use strict";n.d(i,{d:function(){return t}});const t=()=>{var e;if(!Function.prototype.bind)return"bind";const{document:i,navigator:n}=window;if(!i||!n)return"document";const{webdriver:t,userAgent:s,plugins:o,languages:r}=n;if(t)return"webdriver";if(!o||Array.isArray(o))return"plugins";if(null===(e=Object.getOwnPropertyDescriptor(o,"0"))||void 0===e?void 0:e.writable)return"plugins-extra";if(!s)return"userAgent";if(s.indexOf("Snapchat")>0&&i.hidden)return"Snapchat";if(!r||0===r.length||!Object.isFrozen(r))return"languages";try{throw Error()}catch(e){if(e instanceof Error){const{stack:i}=e;if(i&&/ (\(internal\/)|(\(?file:\/)/.test(i))return"stack"}}return""}},74751:function(e,i,n){"use strict";n.d(i,{Z:function(){return t}});const t=e=>{var i,n;let t=!1;if(!((null===(i=window.viewerModel)||void 0===i?void 0:i.experiments["specs.thunderbolt.useImgNotBeacon"])||(null===(n=window.viewerModel)||void 0===n?void 0:n.experiments["specs.thunderbolt.checkIOSToAvoidBeacon"])&&(()=>{var e;return/\(iP(hone|ad|od);/i.test(null===(e=null===window||void 0===window?void 0:window.navigator)||void 0===e?void 0:e.userAgent)})()))try{t=navigator.sendBeacon(e)}catch(e){}t||((new Image).src=e)}}}]);
        //# sourceMappingURL=https://static.parastorage.com/services/wix-thunderbolt/dist/bi-common.inline.059bd271.bundle.min.js.map</script>
    <script data-url="https://static.parastorage.com/services/wix-thunderbolt/dist/sendFedopsLoadStarted.inline.9cb06959.bundle.min.js">(self.webpackJsonp__wix_thunderbolt_app=self.webpackJsonp__wix_thunderbolt_app||[]).push([[5987],{},function(n){"use strict";n.O(0,[142],(function(){return p=39256,n(n.s=p);var p}));n.O()}]);</script>

    <!-- Polyfills check -->
    <script>
        if (
            typeof Promise === 'undefined' ||
            typeof Set === 'undefined' ||
            typeof Object.assign === 'undefined' ||
            typeof Array.from === 'undefined' ||
            typeof Symbol === 'undefined'
        ) {
            // send bi in order to detect the browsers in which polyfills are not working
            window.fedops.phaseStarted('missing_polyfills')
        }
    </script>

    <!-- Viewer Model -->
    <script>
        window.viewerModel = JSON.parse(document.getElementById('wix-viewer-model').textContent)
        window.fetchDynamicModel = () => (window.viewerModel.siteFeaturesConfigs.sessionManager.isRunningInDifferentSiteContext ?  Promise.resolve({}) : fetch(window.viewerModel.dynamicModelUrl, { credentials: 'same-origin' }).then(function(r){if(!r.ok)throw new Error(`[${r.status}]${r.statusText}`);return r.json()}))
        window.dynamicModelPromise = window.fetchDynamicModel()
        window.commonConfig = viewerModel.commonConfig
    </script>

    <script data-url="https://static.parastorage.com/services/wix-thunderbolt/dist/externals-registry.inline.f2844f8f.bundle.min.js">(self.webpackJsonp__wix_thunderbolt_app=self.webpackJsonp__wix_thunderbolt_app||[]).push([[58],{47510:function(){"use strict";window.__imageClientApi__={sdk:{}};const{lodash:e,react:o,reactDOM:a,imageClientApi:n}=window.externalsRegistry={lodash:{},react:{},reactDOM:{},imageClientApi:{}};n.loaded=new Promise((e=>{n.onload=e})),e.loaded=new Promise((o=>{e.onload=o})),window.reactDOMReference=window.ReactDOM={loading:!0},a.loaded=new Promise((e=>{a.onload=()=>{Object.assign(window.reactDOMReference,window.ReactDOM,{loading:!1}),e()}})),window.reactReference=window.React={loading:!0},o.loaded=new Promise((e=>{o.onload=()=>{Object.assign(window.reactReference,window.React,{loading:!1}),e()}})),window.reactAndReactDOMLoaded=Promise.all([o.loaded,a.loaded])}},function(e){"use strict";var o;o=47510,e(e.s=o)}]);
        //# sourceMappingURL=https://static.parastorage.com/services/wix-thunderbolt/dist/externals-registry.inline.f2844f8f.bundle.min.js.map</script>

    <!-- bi -->
    <script data-url="https://static.parastorage.com/services/wix-thunderbolt/dist/bi.inline.c2c89c4a.bundle.min.js">(self.webpackJsonp__wix_thunderbolt_app=self.webpackJsonp__wix_thunderbolt_app||[]).push([[2892],{80459:function(n,I,N){"use strict";var t,A;N.d(I,{sT:function(){return t},$7:function(){return A},i7:function(){return E}}),function(n){n[n.START=1]="START",n[n.VISIBLE=2]="VISIBLE",n[n.PAGE_FINISH=33]="PAGE_FINISH",n[n.FIRST_CDN_RESPONSE=4]="FIRST_CDN_RESPONSE",n[n.TBD=-1]="TBD",n[n.PAGE_NAVIGATION=101]="PAGE_NAVIGATION",n[n.PAGE_NAVIGATION_DONE=103]="PAGE_NAVIGATION_DONE"}(t||(t={})),function(n){n[n.NAVIGATION=1]="NAVIGATION",n[n.DYNAMIC_REDIRECT=2]="DYNAMIC_REDIRECT",n[n.INNER_ROUTE=3]="INNER_ROUTE",n[n.NAVIGATION_ERROR=4]="NAVIGATION_ERROR",n[n.CANCELED=5]="CANCELED"}(A||(A={}));const E={1:"page-navigation",2:"page-navigation-redirect",3:"page-navigation-inner-route",4:"navigation-error",5:"navigation-canceled"}}},function(n){"use strict";n.O(0,[142],(function(){return I=87931,n(n.s=I);var I}));n.O()}]);
        //# sourceMappingURL=https://static.parastorage.com/services/wix-thunderbolt/dist/bi.inline.c2c89c4a.bundle.min.js.map</script>


    <!-- preloading pre-scripts -->
    <!-- renderIndicator -->


    <!-- Business Manager -->


    <!-- initial scripts -->
    <script data-url="https://static.parastorage.com/services/wix-thunderbolt/dist/createPlatformWorker.inline.c3466bce.bundle.min.js">(self.webpackJsonp__wix_thunderbolt_app=self.webpackJsonp__wix_thunderbolt_app||[]).push([[5457],{47770:function(e,r,t){"use strict";t.r(r),t.d(r,{platformWorkerPromise:function(){return i}});const{siteAssets:{clientTopology:s},siteFeatures:o,siteFeaturesConfigs:{platform:a},site:{externalBaseUrl:p}}=window.viewerModel,c=Worker&&o.includes("platform"),i=c?(async()=>{const e="platform_create-worker started";performance.mark(e);const r=a.clientWorkerUrl,t=r.startsWith("http://localhost:4200/")||r.startsWith("https://bo.wix.com/suricate/")||document.baseURI!==location.href?(e=>{const r=new Blob([`importScripts('${e}');`],{type:"application/javascript"});return URL.createObjectURL(r)})(a.clientWorkerUrl):r.replace(s.fileRepoUrl,`${p}/_partials`),o=new Worker(t),c=Object.keys(a.appsScripts.urls).filter((e=>{var r;return!(null===(r=a.bootstrapData.appsSpecData[e])||void 0===r?void 0:r.isModuleFederated)})).reduce(((e,r)=>(e[r]=a.appsScripts.urls[r],e)),{});o.postMessage({type:"platformScriptsToPreload",appScriptsUrls:c});const i="platform_create-worker ended";return performance.mark(i),performance.measure("Create Platform Web Worker",e,i),o})():Promise.resolve()}},function(e){"use strict";var r;r=47770,e(e.s=r)}]);
        //# sourceMappingURL=https://static.parastorage.com/services/wix-thunderbolt/dist/createPlatformWorker.inline.c3466bce.bundle.min.js.map</script>
    <script data-url="https://static.parastorage.com/services/wix-thunderbolt/dist/windowMessageRegister.inline.8c9e8df6.bundle.min.js">(self.webpackJsonp__wix_thunderbolt_app=self.webpackJsonp__wix_thunderbolt_app||[]).push([[9439],{64277:function(){"use strict";!function(n){const e=new Set,t=[],a=n=>{const t=[];e.forEach((e=>{n.canHandleEvent(e)&&t.push(e)})),t.forEach((t=>{e.delete(t),n.handleEvent(t)}))};n.addEventListener("message",(n=>{const s={source:n.source,data:n.data,origin:n.origin},d=t.find((n=>n.canHandleEvent(s)));d?(a(d),d.handleEvent(s)):e.add(s)})),n._addWindowMessageHandler=n=>{t.push(n),a(n)}}(window)}},function(n){"use strict";var e;e=64277,n(n.s=e)}]);
        //# sourceMappingURL=https://static.parastorage.com/services/wix-thunderbolt/dist/windowMessageRegister.inline.8c9e8df6.bundle.min.js.map</script>
    <script async="" src="/index_files/bootstrap-features.2fbcde07.bundle.min.js"></script>
    <script data-url="https://static.parastorage.com/services/wix-thunderbolt/dist/tslib.inline.ef0662b7.bundle.min.js">(self.webpackJsonp__wix_thunderbolt_app=self.webpackJsonp__wix_thunderbolt_app||[]).push([[8050],{31191:function(t,n,r){"use strict";r.d(n,{ZT:function(){return o},pi:function(){return c},_T:function(){return u},mG:function(){return i},Jh:function(){return l},XA:function(){return a},CR:function(){return f},fl:function(){return p},ev:function(){return y}});
                /*! *****************************************************************************
                Copyright (c) Microsoft Corporation.

                Permission to use, copy, modify, and/or distribute this software for any
                purpose with or without fee is hereby granted.

                THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
                REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
                AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
                INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
                LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
                OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
                PERFORMANCE OF THIS SOFTWARE.
                ***************************************************************************** */
                var e=function(t,n){return e=Object.setPrototypeOf||{__proto__:[]}instanceof Array&&function(t,n){t.__proto__=n}||function(t,n){for(var r in n)Object.prototype.hasOwnProperty.call(n,r)&&(t[r]=n[r])},e(t,n)};function o(t,n){if("function"!=typeof n&&null!==n)throw new TypeError("Class extends value "+String(n)+" is not a constructor or null");function r(){this.constructor=t}e(t,n),t.prototype=null===n?Object.create(n):(r.prototype=n.prototype,new r)}var c=function(){return c=Object.assign||function(t){for(var n,r=1,e=arguments.length;r<e;r++)for(var o in n=arguments[r])Object.prototype.hasOwnProperty.call(n,o)&&(t[o]=n[o]);return t},c.apply(this,arguments)};function u(t,n){var r={};for(var e in t)Object.prototype.hasOwnProperty.call(t,e)&&n.indexOf(e)<0&&(r[e]=t[e]);if(null!=t&&"function"==typeof Object.getOwnPropertySymbols){var o=0;for(e=Object.getOwnPropertySymbols(t);o<e.length;o++)n.indexOf(e[o])<0&&Object.prototype.propertyIsEnumerable.call(t,e[o])&&(r[e[o]]=t[e[o]])}return r}function i(t,n,r,e){return new(r||(r=Promise))((function(o,c){function u(t){try{l(e.next(t))}catch(t){c(t)}}function i(t){try{l(e.throw(t))}catch(t){c(t)}}function l(t){var n;t.done?o(t.value):(n=t.value,n instanceof r?n:new r((function(t){t(n)}))).then(u,i)}l((e=e.apply(t,n||[])).next())}))}function l(t,n){var r,e,o,c,u={label:0,sent:function(){if(1&o[0])throw o[1];return o[1]},trys:[],ops:[]};return c={next:i(0),throw:i(1),return:i(2)},"function"==typeof Symbol&&(c[Symbol.iterator]=function(){return this}),c;function i(c){return function(i){return function(c){if(r)throw new TypeError("Generator is already executing.");for(;u;)try{if(r=1,e&&(o=2&c[0]?e.return:c[0]?e.throw||((o=e.return)&&o.call(e),0):e.next)&&!(o=o.call(e,c[1])).done)return o;switch(e=0,o&&(c=[2&c[0],o.value]),c[0]){case 0:case 1:o=c;break;case 4:return u.label++,{value:c[1],done:!1};case 5:u.label++,e=c[1],c=[0];continue;case 7:c=u.ops.pop(),u.trys.pop();continue;default:if(!(o=u.trys,(o=o.length>0&&o[o.length-1])||6!==c[0]&&2!==c[0])){u=0;continue}if(3===c[0]&&(!o||c[1]>o[0]&&c[1]<o[3])){u.label=c[1];break}if(6===c[0]&&u.label<o[1]){u.label=o[1],o=c;break}if(o&&u.label<o[2]){u.label=o[2],u.ops.push(c);break}o[2]&&u.ops.pop(),u.trys.pop();continue}c=n.call(t,u)}catch(t){c=[6,t],e=0}finally{r=o=0}if(5&c[0])throw c[1];return{value:c[0]?c[1]:void 0,done:!0}}([c,i])}}}Object.create;function a(t){var n="function"==typeof Symbol&&Symbol.iterator,r=n&&t[n],e=0;if(r)return r.call(t);if(t&&"number"==typeof t.length)return{next:function(){return t&&e>=t.length&&(t=void 0),{value:t&&t[e++],done:!t}}};throw new TypeError(n?"Object is not iterable.":"Symbol.iterator is not defined.")}function f(t,n){var r="function"==typeof Symbol&&t[Symbol.iterator];if(!r)return t;var e,o,c=r.call(t),u=[];try{for(;(void 0===n||n-- >0)&&!(e=c.next()).done;)u.push(e.value)}catch(t){o={error:t}}finally{try{e&&!e.done&&(r=c.return)&&r.call(c)}finally{if(o)throw o.error}}return u}function p(){for(var t=[],n=0;n<arguments.length;n++)t=t.concat(f(arguments[n]));return t}function y(t,n,r){if(r||2===arguments.length)for(var e,o=0,c=n.length;o<c;o++)!e&&o in n||(e||(e=Array.prototype.slice.call(n,0,o)),e[o]=n[o]);return t.concat(e||Array.prototype.slice.call(n))}Object.create}}]);
        //# sourceMappingURL=https://static.parastorage.com/services/wix-thunderbolt/dist/tslib.inline.ef0662b7.bundle.min.js.map</script>
    <script data-url="https://static.parastorage.com/services/wix-thunderbolt/dist/openPerformanceTool.inline.dbde83bd.bundle.min.js">(self.webpackJsonp__wix_thunderbolt_app=self.webpackJsonp__wix_thunderbolt_app||[]).push([[2759,5457],{10580:function(e,t,r){"use strict";var a=r(47770);const{fleetConfig:o}=window.viewerModel,n=new URLSearchParams(window.location.search),s=n.has("localPerformanceTool"),i=n.get("localPerformanceTool"),p=s?`http://localhost:${""===i?"3000":i}/`:"https://apps.wix.com/tb-performance-tool",c=(e,t)=>{window.addEventListener("message",(r=>{"opened"===r.data&&e.postMessage({type:"performanceData",data:JSON.parse(JSON.stringify(t))},"*")}))};if("Canary"===o.type||window.location.search.includes("performanceTool=true")){const e=[];new PerformanceObserver((function(t){const r=t.getEntries();e.push(...r)})).observe({entryTypes:["longtask"]}),window.openPerformanceTool=async()=>{var t,r;const{ssrEvents:o}=JSON.parse((null===(t=document.getElementById("wix-warmup-data"))||void 0===t?void 0:t.textContent)||"{}"),n=JSON.parse((null===(r=document.getElementById("wix-head-performance-data"))||void 0===r?void 0:r.textContent)||"[]"),s=await a.platformWorkerPromise,i=window.open(p,"_blank"),l=window.performance.getEntries(),m=e.map((e=>({duration:e.duration,startTime:e.startTime,entryType:e.entryType,name:`long task: ${e.attribution[0].containerId}, ${e.attribution[0].containerName}, ${e.attribution[0].containerSrc}`})));if(l.push(...m),o||n){const e=[...n,...o],t=window.performance.timeOrigin,r=e=>e-t;l.push(...e.map((e=>JSON.parse(JSON.stringify({name:e.name,entryType:"SSR",startTime:r(e.startTime),duration:0})))))}s?(s.addEventListener("message",(e=>{var t;if("workerPerformanceData"===(null===(t=e.data)||void 0===t?void 0:t.type)){const t=e.data.data.performanceEntries,r=e.data.data.workerStartTime-window.performance.timeOrigin;t.forEach((e=>{e.startTime=e.startTime+r,e.worker="WORKER"})),l.push(...t),c(i,l)}})),s.postMessage({type:"PerformanceTool"})):c(i,l)}}},47770:function(e,t,r){"use strict";r.r(t),r.d(t,{platformWorkerPromise:function(){return p}});const{siteAssets:{clientTopology:a},siteFeatures:o,siteFeaturesConfigs:{platform:n},site:{externalBaseUrl:s}}=window.viewerModel,i=Worker&&o.includes("platform"),p=i?(async()=>{const e="platform_create-worker started";performance.mark(e);const t=n.clientWorkerUrl,r=t.startsWith("http://localhost:4200/")||t.startsWith("https://bo.wix.com/suricate/")||document.baseURI!==location.href?(e=>{const t=new Blob([`importScripts('${e}');`],{type:"application/javascript"});return URL.createObjectURL(t)})(n.clientWorkerUrl):t.replace(a.fileRepoUrl,`${s}/_partials`),o=new Worker(r),i=Object.keys(n.appsScripts.urls).filter((e=>{var t;return!(null===(t=n.bootstrapData.appsSpecData[e])||void 0===t?void 0:t.isModuleFederated)})).reduce(((e,t)=>(e[t]=n.appsScripts.urls[t],e)),{});o.postMessage({type:"platformScriptsToPreload",appScriptsUrls:i});const p="platform_create-worker ended";return performance.mark(p),performance.measure("Create Platform Web Worker",e,p),o})():Promise.resolve()}},function(e){"use strict";var t;t=10580,e(e.s=t)}]);
        //# sourceMappingURL=https://static.parastorage.com/services/wix-thunderbolt/dist/openPerformanceTool.inline.dbde83bd.bundle.min.js.map</script>

    <!-- lodash script -->
    <script async="" onload="externalsRegistry.lodash.onload()" src="/index_files/lodash.min.js"></script>

    <!-- react -->
    <script crossorigin="" defer="" onload="externalsRegistry.react.onload()" src="/index_files/react.production.min(1).js"></script>

    <!-- Initial CSS -->
    <style data-url="./index_files/main.ab700508.min.css">
        a,abbr,acronym,address,applet,b,big,blockquote,body,button,caption,center,cite,code,dd,del,dfn,div,dl,dt,em,fieldset,font,footer,form,h1,h2,h3,h4,h5,h6,header,html,i,iframe,img,ins,kbd,label,legend,li,nav,object,ol,p,pre,q,s,samp,section,small,span,strike,strong,sub,sup,table,tbody,td,tfoot,th,thead,title,tr,tt,u,ul,var{margin:0;padding:0;border:0;outline:0;vertical-align:baseline;background:transparent}body{font-size:10px;font-family:Arial,Helvetica,sans-serif}input,select,textarea{font-family:Helvetica,Arial,sans-serif;box-sizing:border-box}ol,ul{list-style:none}blockquote,q{quotes:none}ins{text-decoration:none}del{text-decoration:line-through}table{border-collapse:collapse;border-spacing:0}a{cursor:pointer;text-decoration:none}body,html{height:100%}body{overflow-x:auto;overflow-y:scroll}.testStyles{overflow-y:hidden}.reset-button{background:none;border:0;outline:0;color:inherit;font:inherit;line-height:normal;overflow:visible;padding:0;-webkit-appearance:none;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none}:focus{outline:none}body.device-mobile-optimized{overflow-x:hidden;overflow-y:scroll}body.device-mobile-optimized:not(.responsive) #SITE_CONTAINER{width:320px;overflow-x:visible;position:relative;margin-left:auto;margin-right:auto}body.device-mobile-optimized:not(.responsive):not(.blockSiteScrolling) #SITE_CONTAINER{margin-top:0}body.device-mobile-optimized>*{max-width:100%!important}body.device-mobile-optimized #site-root{overflow-x:hidden;overflow-y:hidden}@supports (overflow:clip){body.device-mobile-optimized.dont-overflow-hidden-site-root #site-root{overflow-x:clip;overflow-y:clip}}body.device-mobile-non-optimized #SITE_CONTAINER #site-root{overflow-x:hidden;overflow-y:auto}body.device-mobile-non-optimized.fullScreenMode{background-color:#5f6360}body.device-mobile-non-optimized.fullScreenMode #MOBILE_ACTIONS_MENU,body.device-mobile-non-optimized.fullScreenMode #site-root,body.device-mobile-non-optimized.fullScreenMode #SITE_BACKGROUND,body.fullScreenMode #WIX_ADS{visibility:hidden}body.fullScreenMode{overflow-x:hidden!important;overflow-y:hidden!important}body.fullScreenMode.device-mobile-optimized #TINY_MENU{opacity:0;pointer-events:none}body.fullScreenMode-scrollable.device-mobile-optimized{overflow-x:hidden!important;overflow-y:auto!important}body.fullScreenMode-scrollable.device-mobile-optimized #masterPage,body.fullScreenMode-scrollable.device-mobile-optimized #site-root{overflow-x:hidden!important;overflow-y:hidden!important}body.fullScreenMode-scrollable.device-mobile-optimized #masterPage,body.fullScreenMode-scrollable.device-mobile-optimized #SITE_BACKGROUND{height:auto!important}body.fullScreenMode-scrollable.device-mobile-optimized #masterPage.mesh-layout{height:0!important}body.blockSiteScrolling{position:fixed;width:100%;overflow:hidden}body.blockSiteScrolling #SITE_CONTAINER{margin-top:calc(var(--blocked-site-scroll-margin-top) * -1)}body.blockSiteScrolling:not(.responsive) #WIX_ADS{margin-top:var(--blocked-site-scroll-margin-top)}.fullScreenOverlay{z-index:1005;position:fixed;left:0;top:-60px;right:0;bottom:0;display:flex;justify-content:center;overflow-y:hidden}.fullScreenOverlay>.fullScreenOverlayContent{margin:0 auto;position:absolute;right:0;top:60px;left:0;bottom:0;overflow:hidden;transform:translateZ(0)}[data-mesh-id$=centeredContent],[data-mesh-id$=form],[data-mesh-id$=inlineContent]{position:relative;pointer-events:none}[data-mesh-id$=-gridWrapper],[data-mesh-id$=-rotated-wrapper]{pointer-events:none}[data-mesh-id$=-gridContainer]>*,[data-mesh-id$=-rotated-wrapper]>*,[data-mesh-id$=inlineContent]>:not([data-mesh-id$=-gridContainer]){pointer-events:auto}.device-mobile-optimized #masterPage.mesh-layout #SOSP_CONTAINER_CUSTOM_ID{-ms-grid-row:2;grid-area:2/1/3/2;position:relative}#masterPage.mesh-layout{display:-ms-grid;display:grid;-ms-grid-rows:max-content max-content min-content max-content;grid-template-rows:-webkit-max-content -webkit-max-content -webkit-min-content -webkit-max-content;grid-template-rows:max-content max-content min-content max-content;-ms-grid-columns:100%;grid-template-columns:100%;align-items:start;justify-content:stretch}#masterPage.mesh-layout #PAGES_CONTAINER,#masterPage.mesh-layout #SITE_FOOTER-placeholder,#masterPage.mesh-layout #SITE_FOOTER_WRAPPER,#masterPage.mesh-layout #SITE_HEADER-placeholder,#masterPage.mesh-layout #SITE_HEADER_WRAPPER,#masterPage.mesh-layout #soapAfterPagesContainer,#masterPage.mesh-layout #soapBeforePagesContainer,#masterPage.mesh-layout #SOSP_CONTAINER_CUSTOM_ID[data-state~=mobileView]{-ms-grid-column:1;-ms-grid-row-align:start;-ms-grid-column-align:start}#masterPage.mesh-layout #SITE_HEADER-placeholder,#masterPage.mesh-layout #SITE_HEADER_WRAPPER{-ms-grid-row:1;grid-area:1/1/2/2}#masterPage.mesh-layout #PAGES_CONTAINER,#masterPage.mesh-layout #soapAfterPagesContainer,#masterPage.mesh-layout #soapBeforePagesContainer{-ms-grid-row:3;grid-area:3/1/4/2}#masterPage.mesh-layout #soapAfterPagesContainer,#masterPage.mesh-layout #soapBeforePagesContainer{width:100%}#masterPage.mesh-layout #PAGES_CONTAINER{align-self:stretch}#masterPage.mesh-layout main#PAGES_CONTAINER{display:block}#masterPage.mesh-layout #SITE_FOOTER-placeholder,#masterPage.mesh-layout #SITE_FOOTER_WRAPPER{-ms-grid-row:4;grid-area:4/1/5/2}#masterPage.mesh-layout #SITE_PAGES,#masterPage.mesh-layout [data-mesh-id=PAGES_CONTAINERcenteredContent],#masterPage.mesh-layout [data-mesh-id=PAGES_CONTAINERinlineContent]{height:100%}#masterPage.mesh-layout.desktop>*{width:100%}#masterPage.mesh-layout #masterPageinlineContent,#masterPage.mesh-layout #PAGES_CONTAINER,#masterPage.mesh-layout #SITE_FOOTER,#masterPage.mesh-layout #SITE_FOOTER_WRAPPER,#masterPage.mesh-layout #SITE_HEADER,#masterPage.mesh-layout #SITE_HEADER_WRAPPER,#masterPage.mesh-layout #SITE_PAGES,#site-root{position:relative}#site-root{top:var(--wix-ads-top-height);min-height:100%;margin:0 auto}#site-root img:not([src]){visibility:hidden}#site-root svg img:not([src]){visibility:visible}body:not(.responsive) #site-root{width:100%;min-width:var(--site-width)}#SITE_CONTAINER{position:relative}.auto-generated-link{color:inherit}body:not([data-js-loaded]) [data-hide-prejs]{visibility:hidden}#SCROLL_TO_BOTTOM,#SCROLL_TO_TOP{height:0}.has-click-trigger{cursor:pointer}[data-z-counter]{z-index:0}[data-z-counter="0"]{z-index:auto}.wixSiteProperties{-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}
    </style>



    <script defer="" src="/index_files/siteTags.bundle.min.js"></script>

    <meta name="format-detection" content="telephone=no">
    <meta name="skype_toolbar" content="skype_toolbar_parser_compatible">






    <!--pageHtmlEmbeds.head start-->
    <script type="wix/htmlEmbeds" id="pageHtmlEmbeds.head start"></script>

    <script type="wix/htmlEmbeds" id="pageHtmlEmbeds.head end"></script>
    <!--pageHtmlEmbeds.head end-->


    <!-- initCustomElements -->
    <meta name="wix-dynamic-custom-elements" content="DropDownMenu">
    <script data-url="https://static.parastorage.com/services/wix-thunderbolt/dist/initCustomElements.inline.6137fd3f.bundle.min.js">(self.webpackJsonp__wix_thunderbolt_app=self.webpackJsonp__wix_thunderbolt_app||[]).push([[3727,2892],{25536:function(e,t,i){"use strict";var n={};i.r(n),i.d(n,{BackgroundParallax:function(){return l},BackgroundParallaxZoom:function(){return g},BackgroundReveal:function(){return m},BgCloseUp:function(){return f},BgExpand:function(){return p},BgFabeBack:function(){return b},BgFadeIn:function(){return I},BgFadeOut:function(){return _},BgFake3D:function(){return w},BgPanLeft:function(){return v},BgPanRight:function(){return T},BgParallax:function(){return E},BgPullBack:function(){return L},BgReveal:function(){return O},BgRotate:function(){return y},BgShrink:function(){return S},BgSkew:function(){return M},BgUnwind:function(){return A},BgZoomIn:function(){return R},BgZoomOut:function(){return x}});var r={};i.r(r),i.d(r,{STATIC_MEDIA_URL:function(){return Et},alignTypes:function(){return H},fittingTypes:function(){return C},getData:function(){return pt},getPlaceholder:function(){return ft},htmlTag:function(){return j},populateGlobalFeatureSupport:function(){return me},sdk:function(){return Tt},upscaleMethods:function(){return W}});var o=i(31191),s=i(96114),a=i.n(s);var c=i(59277),d=function(e){return e*Math.PI/180},h=function(e,t){return{width:e,height:t}},u=function(e,t,i){return{width:e,height:Math.max(t,i)}};var l={hasParallax:!0,getMediaDimensions:u},g={hasParallax:!0,getMediaDimensions:u},m={hasParallax:!0,getMediaDimensions:u},f={getMediaDimensions:h},p={getMediaDimensions:h},b={getMediaDimensions:h},I={getMediaDimensions:h},_={getMediaDimensions:h},w={hasParallax:!0,getMediaDimensions:u},v={getMediaDimensions:function(e,t){return{width:1.2*e,height:t}}},T={getMediaDimensions:function(e,t){return{width:1.2*e,height:t}}},E={hasParallax:!0,getMediaDimensions:u},L={getMediaDimensions:h},O={hasParallax:!0,getMediaDimensions:u},y={getMediaDimensions:function(e,t){return function(e,t,i){var n=d(i),r=Math.hypot(e,t)/2,o=Math.acos(e/2/r),s=e*Math.abs(Math.cos(n))+t*Math.abs(Math.sin(n)),a=e*Math.abs(Math.sin(n))+t*Math.abs(Math.cos(n));return{width:Math.ceil(n<o?s:2*r),height:Math.ceil(n<d(90)-o?a:2*r)}}(e,t,22)}},S={getMediaDimensions:h},M={getMediaDimensions:function(e,t){return function(e,t,i){var n=d(i);return{width:e,height:e*Math.tan(n)+t}}(e,t,20)}},A={getMediaDimensions:h},R={hasParallax:!0,getMediaDimensions:u},x={getMediaDimensions:function(e,t){return{width:1.15*e,height:1.15*t}}};const C={SCALE_TO_FILL:"fill",SCALE_TO_FIT:"fit",STRETCH:"stretch",ORIGINAL_SIZE:"original_size",TILE:"tile",TILE_HORIZONTAL:"tile_horizontal",TILE_VERTICAL:"tile_vertical",FIT_AND_TILE:"fit_and_tile",LEGACY_STRIP_TILE:"legacy_strip_tile",LEGACY_STRIP_TILE_HORIZONTAL:"legacy_strip_tile_horizontal",LEGACY_STRIP_TILE_VERTICAL:"legacy_strip_tile_vertical",LEGACY_STRIP_SCALE_TO_FILL:"legacy_strip_fill",LEGACY_STRIP_SCALE_TO_FIT:"legacy_strip_fit",LEGACY_STRIP_FIT_AND_TILE:"legacy_strip_fit_and_tile",LEGACY_STRIP_ORIGINAL_SIZE:"legacy_strip_original_size",LEGACY_ORIGINAL_SIZE:"actual_size",LEGACY_FIT_WIDTH:"fitWidth",LEGACY_FIT_HEIGHT:"fitHeight",LEGACY_FULL:"full",LEGACY_BG_FIT_AND_TILE:"legacy_tile",LEGACY_BG_FIT_AND_TILE_HORIZONTAL:"legacy_tile_horizontal",LEGACY_BG_FIT_AND_TILE_VERTICAL:"legacy_tile_vertical",LEGACY_BG_NORMAL:"legacy_normal"},P="fit",N="fill",G="fill_focal",k="crop",B="legacy_crop",F="legacy_fill",H={CENTER:"center",TOP:"top",TOP_LEFT:"top_left",TOP_RIGHT:"top_right",BOTTOM:"bottom",BOTTOM_LEFT:"bottom_left",BOTTOM_RIGHT:"bottom_right",LEFT:"left",RIGHT:"right"},D={[H.CENTER]:{x:.5,y:.5},[H.TOP_LEFT]:{x:0,y:0},[H.TOP_RIGHT]:{x:1,y:.5},[H.TOP]:{x:.5,y:0},[H.BOTTOM_LEFT]:{x:0,y:1},[H.BOTTOM_RIGHT]:{x:1,y:1},[H.BOTTOM]:{x:.5,y:1},[H.RIGHT]:{x:1,y:.5},[H.LEFT]:{x:0,y:.5}},$={center:"c",top:"t",top_left:"tl",top_right:"tr",bottom:"b",bottom_left:"bl",bottom_right:"br",left:"l",right:"r"},j={BG:"bg",IMG:"img",SVG:"svg"},W={AUTO:"auto",CLASSIC:"classic",SUPER:"super"},z=1,U=2,Y={radius:"0.66",amount:"1.00",threshold:"0.01"},V={uri:"",css:{img:{},container:{}},attr:{img:{},container:{}},transformed:!1},q=25e6,Z=[1.5,2,4],J={HIGH:{size:196e4,quality:90,maxUpscale:1},MEDIUM:{size:36e4,quality:85,maxUpscale:1},LOW:{size:16e4,quality:80,maxUpscale:1.2},TINY:{size:0,quality:80,maxUpscale:1.4}},X="HIGH",Q="MEDIUM",K="LOW",ee="TINY",te="contrast",ie="brightness",ne="saturation",re="hue",oe="blur",se="jpg",ae="jpeg",ce="jpe",de="png",he="webp",ue="gif",le="unrecognized",ge={isMobile:!1};function me(){if("undefined"!=typeof window&&"undefined"!=typeof navigator){const t=window.matchMedia&&window.matchMedia("(max-width: 767px)").matches,i=/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);e=t&&i,ge["isMobile"]=e}var e}function fe(e,...t){return function(...i){const n=i[i.length-1]||{},r=[e[0]];return t.forEach((function(t,o){const s=Number.isInteger(t)?i[t]:n[t];r.push(s,e[o+1])})),r.join("")}}function pe(e){return e[e.length-1]}const be=[de,ae,se,ce,"wix_ico_mp","wix_mp"],Ie=[ae,se,ce];function _e(e,t,i){return i&&t&&!(!(n=t.id)||!n.trim()||"none"===n.toLowerCase())&&Object.values(C).includes(e);var n}function we(e){return function(e){return be.includes(Oe(e))}(e)&&!/(^https?)|(^data)|(^\/\/)/.test(e)}function ve(e){return Oe(e)===de}const Te=["/","\\","?","<",">","|","\u201c",":",'"'].map(encodeURIComponent),Ee=["\\.","\\*"];function Le(e){return function(e){return Ie.includes(Oe(e))}(e)?se:ve(e)?de:function(e){return Oe(e)===he}(e)?he:le}function Oe(e){return(/[.]([^.]+)$/.exec(e)&&/[.]([^.]+)$/.exec(e)[1]||"").toLowerCase()}function ye(e,t,i,n,r){let o;return o=r===N?function(e,t,i,n){return Math.max(i/e,n/t)}(e,t,i,n):r===P?function(e,t,i,n){return Math.min(i/e,n/t)}(e,t,i,n):1,o}function Se(e,t,i,n,r,o){e=e||n.width,t=t||n.height;const{scaleFactor:s,width:a,height:c}=function(e,t,i,n,r){let o,s=i,a=n;if(o=ye(e,t,i,n,r),r===P&&(s=e*o,a=t*o),s&&a&&s*a>q){const i=Math.sqrt(q/(s*a));s*=i,a*=i,o=ye(e,t,s,a,r)}return{scaleFactor:o,width:s,height:a}}(e,t,n.width*r,n.height*r,i);return function(e,t,i,n,r,o,s){const{optimizedScaleFactor:a,upscaleMethodValue:c,forceUSM:d}=function(e,t,i,n){if("auto"===n)return function(e,t){const i=xe(e,t);return{optimizedScaleFactor:J[i].maxUpscale,upscaleMethodValue:z,forceUSM:!1}}(e,t);if("super"===n)return function(e){return{optimizedScaleFactor:pe(Z),upscaleMethodValue:U,forceUSM:!(Z.includes(e)||e>pe(Z))}}(i);return function(e,t){const i=xe(e,t);return{optimizedScaleFactor:J[i].maxUpscale,upscaleMethodValue:z,forceUSM:!1}}(e,t)}(e,t,o,r);let h=i,u=n;if(o<=a)return{width:h,height:u,scaleFactor:o,upscaleMethodValue:c,forceUSM:d,cssUpscaleNeeded:!1};switch(s){case N:h=i*(a/o),u=n*(a/o);break;case P:h=e*a,u=t*a}return{width:h,height:u,scaleFactor:a,upscaleMethodValue:c,forceUSM:d,cssUpscaleNeeded:!0}}(e,t,a,c,o,s,i)}function Me(e,t,i,n){const r=Re(i)||function(e=H.CENTER){return D[e]}(n);return{x:Math.max(0,Math.min(e.width-t.width,r.x*e.width-t.width/2)),y:Math.max(0,Math.min(e.height-t.height,r.y*e.height-t.height/2)),width:Math.min(e.width,t.width),height:Math.min(e.height,t.height)}}function Ae(e){return e.alignment&&$[e.alignment]||$[H.CENTER]}function Re(e){let t;return!e||"number"!=typeof e.x||isNaN(e.x)||"number"!=typeof e.y||isNaN(e.y)||(t={x:Ce(Math.max(0,Math.min(100,e.x))/100,2),y:Ce(Math.max(0,Math.min(100,e.y))/100,2)}),t}function xe(e,t){const i=e*t;return i>J[X].size?X:i>J[Q].size?Q:i>J[K].size?K:ee}function Ce(e,t){const i=Math.pow(10,t||0);return(e*i/i).toFixed(t)}function Pe(e){return e&&e.upscaleMethod&&W[e.upscaleMethod.toUpperCase()]||W.AUTO}function Ne(e,t){const i={css:{container:{}}},{css:n}=i,{fittingType:r}=e;switch(r){case C.ORIGINAL_SIZE:case C.LEGACY_ORIGINAL_SIZE:case C.LEGACY_STRIP_ORIGINAL_SIZE:n.container.backgroundSize="auto",n.container.backgroundRepeat="no-repeat";break;case C.SCALE_TO_FIT:case C.LEGACY_STRIP_SCALE_TO_FIT:n.container.backgroundSize="contain",n.container.backgroundRepeat="no-repeat";break;case C.STRETCH:n.container.backgroundSize="100% 100%",n.container.backgroundRepeat="no-repeat";break;case C.SCALE_TO_FILL:case C.LEGACY_STRIP_SCALE_TO_FILL:n.container.backgroundSize="cover",n.container.backgroundRepeat="no-repeat";break;case C.TILE_HORIZONTAL:case C.LEGACY_STRIP_TILE_HORIZONTAL:n.container.backgroundSize="auto",n.container.backgroundRepeat="repeat-x";break;case C.TILE_VERTICAL:case C.LEGACY_STRIP_TILE_VERTICAL:n.container.backgroundSize="auto",n.container.backgroundRepeat="repeat-y";break;case C.TILE:case C.LEGACY_STRIP_TILE:n.container.backgroundSize="auto",n.container.backgroundRepeat="repeat";break;case C.LEGACY_STRIP_FIT_AND_TILE:n.container.backgroundSize="contain",n.container.backgroundRepeat="repeat";break;case C.FIT_AND_TILE:case C.LEGACY_BG_FIT_AND_TILE:n.container.backgroundSize="auto",n.container.backgroundRepeat="repeat";break;case C.LEGACY_BG_FIT_AND_TILE_HORIZONTAL:n.container.backgroundSize="auto",n.container.backgroundRepeat="repeat-x";break;case C.LEGACY_BG_FIT_AND_TILE_VERTICAL:n.container.backgroundSize="auto",n.container.backgroundRepeat="repeat-y";break;case C.LEGACY_BG_NORMAL:n.container.backgroundSize="auto",n.container.backgroundRepeat="no-repeat"}switch(t.alignment){case H.CENTER:n.container.backgroundPosition="center center";break;case H.LEFT:n.container.backgroundPosition="left center";break;case H.RIGHT:n.container.backgroundPosition="right center";break;case H.TOP:n.container.backgroundPosition="center top";break;case H.BOTTOM:n.container.backgroundPosition="center bottom";break;case H.TOP_RIGHT:n.container.backgroundPosition="right top";break;case H.TOP_LEFT:n.container.backgroundPosition="left top";break;case H.BOTTOM_RIGHT:n.container.backgroundPosition="right bottom";break;case H.BOTTOM_LEFT:n.container.backgroundPosition="left bottom"}return i}const Ge={[H.CENTER]:"center",[H.TOP]:"top",[H.TOP_LEFT]:"top left",[H.TOP_RIGHT]:"top right",[H.BOTTOM]:"bottom",[H.BOTTOM_LEFT]:"bottom left",[H.BOTTOM_RIGHT]:"bottom right",[H.LEFT]:"left",[H.RIGHT]:"right"},ke={position:"absolute",top:"auto",right:"auto",bottom:"auto",left:"auto"};function Be(e,t){const i={css:{container:{},img:{}}},{css:n}=i,{fittingType:r}=e,o=t.alignment;switch(n.container.position="relative",r){case C.ORIGINAL_SIZE:case C.LEGACY_ORIGINAL_SIZE:e.parts&&e.parts.length?(n.img.width=e.parts[0].width,n.img.height=e.parts[0].height):(n.img.width=e.src.width,n.img.height=e.src.height);break;case C.SCALE_TO_FIT:case C.LEGACY_FIT_WIDTH:case C.LEGACY_FIT_HEIGHT:case C.LEGACY_FULL:n.img.width=t.width,n.img.height=t.height,n.img.objectFit="contain",n.img.objectPosition=Ge[o]||"unset";break;case C.LEGACY_BG_NORMAL:n.img.width="100%",n.img.height="100%",n.img.objectFit="none",n.img.objectPosition=Ge[o]||"unset";break;case C.STRETCH:n.img.width=t.width,n.img.height=t.height,n.img.objectFit="fill";break;case C.SCALE_TO_FILL:n.img.width=t.width,n.img.height=t.height,n.img.objectFit="cover"}if("number"==typeof n.img.width&&"number"==typeof n.img.height&&(n.img.width!==t.width||n.img.height!==t.height)){const e=Math.round((t.height-n.img.height)/2),i=Math.round((t.width-n.img.width)/2);Object.assign(n.img,ke,function(e,t,i){return{[H.TOP_LEFT]:{top:0,left:0},[H.TOP_RIGHT]:{top:0,right:0},[H.TOP]:{top:0,left:t},[H.BOTTOM_LEFT]:{top:0,left:0},[H.BOTTOM_RIGHT]:{bottom:0,right:0},[H.BOTTOM]:{bottom:0,left:t},[H.RIGHT]:{top:e,right:0},[H.LEFT]:{top:e,left:0},[H.CENTER]:{width:i.width,height:i.height,objectFit:"none"}}}(e,i,t)[o])}return i}function Fe(e,t){const i={css:{container:{}},attr:{container:{},img:{}}},{css:n,attr:r}=i,{fittingType:o}=e,s=t.alignment,{width:a,height:c}=e.src;let d;switch(n.container.position="relative",o){case C.ORIGINAL_SIZE:case C.LEGACY_ORIGINAL_SIZE:case C.TILE:e.parts&&e.parts.length?(r.img.width=e.parts[0].width,r.img.height=e.parts[0].height):(r.img.width=a,r.img.height=c),r.img.preserveAspectRatio="xMidYMid slice";break;case C.SCALE_TO_FIT:case C.LEGACY_FIT_WIDTH:case C.LEGACY_FIT_HEIGHT:case C.LEGACY_FULL:r.img.width="100%",r.img.height="100%",r.img.transform="",r.img.preserveAspectRatio="";break;case C.STRETCH:r.img.width=t.width,r.img.height=t.height,r.img.x=0,r.img.y=0,r.img.transform="",r.img.preserveAspectRatio="none";break;case C.SCALE_TO_FILL:we(e.src.id)?(r.img.width=t.width,r.img.height=t.height):(d=function(e,t,i,n,r){const o=ye(e,t,i,n,r);return{width:Math.round(e*o),height:Math.round(t*o)}}(a,c,t.width,t.height,N),r.img.width=d.width,r.img.height=d.height),r.img.x=0,r.img.y=0,r.img.transform="",r.img.preserveAspectRatio="xMidYMid slice"}if("number"==typeof r.img.width&&"number"==typeof r.img.height&&(r.img.width!==t.width||r.img.height!==t.height)){let e,i,n=0,a=0;o===C.TILE?(e=t.width%r.img.width,i=t.height%r.img.height):(e=t.width-r.img.width,i=t.height-r.img.height);const c=Math.round(e/2),d=Math.round(i/2);switch(s){case H.TOP_LEFT:n=0,a=0;break;case H.TOP:n=c,a=0;break;case H.TOP_RIGHT:n=e,a=0;break;case H.LEFT:n=0,a=d;break;case H.CENTER:n=c,a=d;break;case H.RIGHT:n=e,a=d;break;case H.BOTTOM_LEFT:n=0,a=i;break;case H.BOTTOM:n=c,a=i;break;case H.BOTTOM_RIGHT:n=e,a=i}r.img.x=n,r.img.y=a}return r.container.width=t.width,r.container.height=t.height,r.container.viewBox=[0,0,t.width,t.height].join(" "),i}function He(){return ge["isMobile"]}function De(e,t,i){let n;switch(t.crop&&(n=function(e,t){const i=Math.max(0,Math.min(e.width,t.x+t.width)-Math.max(0,t.x)),n=Math.max(0,Math.min(e.height,t.y+t.height)-Math.max(0,t.y));return i&&n&&(e.width!==i||e.height!==n)?{x:Math.max(0,t.x),y:Math.max(0,t.y),width:i,height:n}:null}(t,t.crop),n&&(e.src.width=n.width,e.src.height=n.height,e.src.isCropped=!0,e.parts.push(je(n)))),e.fittingType){case C.SCALE_TO_FIT:case C.LEGACY_FIT_WIDTH:case C.LEGACY_FIT_HEIGHT:case C.LEGACY_FULL:case C.FIT_AND_TILE:case C.LEGACY_BG_FIT_AND_TILE:case C.LEGACY_BG_FIT_AND_TILE_HORIZONTAL:case C.LEGACY_BG_FIT_AND_TILE_VERTICAL:case C.LEGACY_BG_NORMAL:e.parts.push($e(e,i));break;case C.SCALE_TO_FILL:e.parts.push(function(e,t){const i=Se(e.src.width,e.src.height,N,t,e.devicePixelRatio,e.upscaleMethod),n=Re(e.focalPoint);return{transformType:n?G:N,width:Math.round(i.width),height:Math.round(i.height),alignment:Ae(t),focalPointX:n&&n.x,focalPointY:n&&n.y,upscale:i.scaleFactor>1,forceUSM:i.forceUSM,scaleFactor:i.scaleFactor,cssUpscaleNeeded:i.cssUpscaleNeeded,upscaleMethodValue:i.upscaleMethodValue}}(e,i));break;case C.STRETCH:e.parts.push(function(e,t){const i=ye(e.src.width,e.src.height,t.width,t.height,N),n=Object.assign({},t);return n.width=e.src.width*i,n.height=e.src.height*i,$e(e,n)}(e,i));break;case C.TILE_HORIZONTAL:case C.TILE_VERTICAL:case C.TILE:case C.LEGACY_ORIGINAL_SIZE:case C.ORIGINAL_SIZE:n=Me(e.src,i,e.focalPoint,i.alignment),e.src.isCropped?(Object.assign(e.parts[0],n),e.src.width=n.width,e.src.height=n.height):e.parts.push(je(n));break;case C.LEGACY_STRIP_TILE_HORIZONTAL:case C.LEGACY_STRIP_TILE_VERTICAL:case C.LEGACY_STRIP_TILE:case C.LEGACY_STRIP_ORIGINAL_SIZE:e.parts.push(function(e){return{transformType:B,width:Math.round(e.width),height:Math.round(e.height),alignment:Ae(e),upscale:!1,forceUSM:!1,scaleFactor:1,cssUpscaleNeeded:!1}}(i));break;case C.LEGACY_STRIP_SCALE_TO_FIT:case C.LEGACY_STRIP_FIT_AND_TILE:e.parts.push(function(e){return{transformType:P,width:Math.round(e.width),height:Math.round(e.height),upscale:!1,forceUSM:!0,scaleFactor:1,cssUpscaleNeeded:!1}}(i));break;case C.LEGACY_STRIP_SCALE_TO_FILL:e.parts.push(function(e){return{transformType:F,width:Math.round(e.width),height:Math.round(e.height),alignment:Ae(e),upscale:!1,forceUSM:!0,scaleFactor:1,cssUpscaleNeeded:!1}}(i))}}function $e(e,t){const i=Se(e.src.width,e.src.height,P,t,e.devicePixelRatio,e.upscaleMethod);return{transformType:N,width:Math.round(i.width),height:Math.round(i.height),alignment:$.center,upscale:i.scaleFactor>1,forceUSM:i.forceUSM,scaleFactor:i.scaleFactor,cssUpscaleNeeded:i.cssUpscaleNeeded,upscaleMethodValue:i.upscaleMethodValue}}function je(e){return{transformType:k,x:Math.round(e.x),y:Math.round(e.y),width:Math.round(e.width),height:Math.round(e.height),upscale:!1,forceUSM:!1,scaleFactor:1,cssUpscaleNeeded:!1}}function We(e,t){var i;t=t||{},e.quality=function(e,t){const i=e.fileType===de,n=e.fileType===se;if(n||i){const n=pe(e.parts),s=(r=n.width,o=n.height,J[xe(r,o)].quality);let a=t.quality&&t.quality>=5&&t.quality<=90?t.quality:s;return a=i?a+5:a,a}var r,o;return 0}(e,t),e.progressive=function(e){return!1!==e.progressive}(t),e.watermark=function(e){return e.watermark}(t),e.autoEncode=null===(i=t.autoEncode)||void 0===i||i,e.unsharpMask=function(e,t){var i,n,r;if(function(e){const t="number"==typeof(e=e||{}).radius&&!isNaN(e.radius)&&e.radius>=.1&&e.radius<=500,i="number"==typeof e.amount&&!isNaN(e.amount)&&e.amount>=0&&e.amount<=10,n="number"==typeof e.threshold&&!isNaN(e.threshold)&&e.threshold>=0&&e.threshold<=255;return t&&i&&n}(t.unsharpMask))return{radius:Ce(null===(i=t.unsharpMask)||void 0===i?void 0:i.radius,2),amount:Ce(null===(n=t.unsharpMask)||void 0===n?void 0:n.amount,2),threshold:Ce(null===(r=t.unsharpMask)||void 0===r?void 0:r.threshold,2)};if(("number"!=typeof(o=(o=t.unsharpMask)||{}).radius||isNaN(o.radius)||0!==o.radius||"number"!=typeof o.amount||isNaN(o.amount)||0!==o.amount||"number"!=typeof o.threshold||isNaN(o.threshold)||0!==o.threshold)&&function(e){const t=pe(e.parts);return!(t.scaleFactor>=1)||t.forceUSM}(e))return Y;var o;return}(e,t),e.filters=function(e){const t=e.filters||{},i={};ze(t[te],-100,100)&&(i[te]=t[te]);ze(t[ie],-100,100)&&(i[ie]=t[ie]);ze(t[ne],-100,100)&&(i[ne]=t[ne]);ze(t[re],-180,180)&&(i[re]=t[re]);ze(t[oe],0,100)&&(i[oe]=t[oe]);return i}(t)}function ze(e,t,i){return"number"==typeof e&&!isNaN(e)&&0!==e&&e>=t&&e<=i}function Ue(e,t,i,n){const r=function(e){var t;return null!==(t=null==e?void 0:e.isSEOBot)&&void 0!==t&&t}(n),o=Le(t.id),s=function(e,t){const i=/\.([^.]*)$/,n=new RegExp(`(${Te.concat(Ee).join("|")})`,"g");if(t&&t.length){let e=t;const r=t.match(i);return r&&be.includes(r[1])&&(e=t.replace(i,"")),encodeURIComponent(e).replace(n,"_")}const r=e.match(/\/(.*?)$/);return(r?r[1]:e).replace(i,"")}(t.id,t.name),a=r?1:function(e){return Math.min(e.pixelAspectRatio||1,2)}(i),c=Oe(t.id),d=c,h=we(t.id),u={fileName:s,fileExtension:c,fileType:o,fittingType:e,preferredExtension:d,src:{id:t.id,width:t.width,height:t.height,isCropped:!1},focalPoint:{x:t.focalPoint&&t.focalPoint.x,y:t.focalPoint&&t.focalPoint.y},parts:[],devicePixelRatio:a,quality:0,upscaleMethod:Pe(n),progressive:!0,watermark:"",unsharpMask:{},filters:{},transformed:h};return h&&(De(u,t,i),We(u,n)),u}function Ye(e,t,i){const n=Object.assign({},i),r=He();switch(e){case C.LEGACY_BG_FIT_AND_TILE:case C.LEGACY_BG_FIT_AND_TILE_HORIZONTAL:case C.LEGACY_BG_FIT_AND_TILE_VERTICAL:case C.LEGACY_BG_NORMAL:const e=r?1e3:1920,i=r?1e3:1920;n.width=Math.min(e,t.width),n.height=Math.min(i,Math.round(n.width/(t.width/t.height))),n.pixelAspectRatio=1}return n}const Ve=fe`fit/w_${"width"},h_${"height"}`,qe=fe`fill/w_${"width"},h_${"height"},al_${"alignment"}`,Ze=fe`fill/w_${"width"},h_${"height"},fp_${"focalPointX"}_${"focalPointY"}`,Je=fe`crop/x_${"x"},y_${"y"},w_${"width"},h_${"height"}`,Xe=fe`crop/w_${"width"},h_${"height"},al_${"alignment"}`,Qe=fe`fill/w_${"width"},h_${"height"},al_${"alignment"}`,Ke=fe`,lg_${"upscaleMethodValue"}`,et=fe`,q_${"quality"}`,tt=fe`,usm_${"radius"}_${"amount"}_${"threshold"}`,it=fe`,bl`,nt=fe`,wm_${"watermark"}`,rt={[te]:fe`,con_${"contrast"}`,[ie]:fe`,br_${"brightness"}`,[ne]:fe`,sat_${"saturation"}`,[re]:fe`,hue_${"hue"}`,[oe]:fe`,blur_${"blur"}`},ot=fe`,enc_auto`;function st(e,t,i,n={},r){return we(t.id)?function(e){const t=[];e.parts.forEach((e=>{switch(e.transformType){case k:t.push(Je(e));break;case B:t.push(Xe(e));break;case F:let i=Qe(e);e.upscale&&(i+=Ke(e)),t.push(i);break;case P:let n=Ve(e);e.upscale&&(n+=Ke(e)),t.push(n);break;case N:let r=qe(e);e.upscale&&(r+=Ke(e)),t.push(r);break;case G:let o=Ze(e);e.upscale&&(o+=Ke(e)),t.push(o)}}));let i=t.join("/");return e.quality&&(i+=et(e)),e.unsharpMask&&(i+=tt(e.unsharpMask)),e.progressive||(i+=it(e)),e.watermark&&(i+=nt(e)),e.filters&&(i+=Object.keys(e.filters).map((t=>rt[t](e.filters))).join("")),e.autoEncode&&e.fileType!==ue&&(i+=ot(e)),`${e.src.id}/v1/${i}/${e.fileName}.${e.preferredExtension}`}(r=r||Ue(e,t,i,n)):t.id}const at={[H.CENTER]:"50% 50%",[H.TOP_LEFT]:"0% 0%",[H.TOP_RIGHT]:"100% 0%",[H.TOP]:"50% 0%",[H.BOTTOM_LEFT]:"0% 100%",[H.BOTTOM_RIGHT]:"100% 100%",[H.BOTTOM]:"50% 100%",[H.RIGHT]:"100% 50%",[H.LEFT]:"0% 50%"},ct=Object.entries(at).reduce(((e,[t,i])=>(e[i]=t,e)),{}),dt=[C.TILE,C.TILE_HORIZONTAL,C.TILE_VERTICAL,C.LEGACY_BG_FIT_AND_TILE,C.LEGACY_BG_FIT_AND_TILE_HORIZONTAL,C.LEGACY_BG_FIT_AND_TILE_VERTICAL],ht=[C.LEGACY_ORIGINAL_SIZE,C.ORIGINAL_SIZE,C.LEGACY_BG_NORMAL];function ut(e,t,{width:i,height:n}){return e===C.TILE&&t.width>i&&t.height>n}function lt(e,t,i,n="center"){const r={img:{},container:{}};if(e===C.SCALE_TO_FILL){const e=t.focalPoint&&function(e){const t=`${e.x}% ${e.y}%`;return ct[t]||""}(t.focalPoint),o=e||n;t.focalPoint&&!e?r.img={objectPosition:gt(t,i,t.focalPoint)}:r.img={objectPosition:at[o]}}else[C.LEGACY_ORIGINAL_SIZE,C.ORIGINAL_SIZE].includes(e)?r.img={objectFit:"none",top:"auto",left:"auto",right:"auto",bottom:"auto"}:dt.includes(e)&&(r.container={backgroundSize:`${t.width}px ${t.height}px`});return r}function gt(e,t,i){const{width:n,height:r}=e,{width:o,height:s}=t,{x:a,y:c}=i;if(!o||!s)return`${a}% ${c}%`;const d=Math.max(o/n,s/r),h=n*d,u=r*d,l=Math.max(0,Math.min(h-o,h*(a/100)-o/2)),g=Math.max(0,Math.min(u-s,u*(c/100)-s/2));return`${l&&Math.floor(l/(h-o)*100)}% ${g&&Math.floor(g/(u-s)*100)}%`}const mt={width:"100%",height:"100%"};function ft(e,t,i,n={}){if(!_e(e,t,i))return V;const{autoEncode:r=!0,isSEOBot:o,shouldLoadHQImage:s}=n,a=we(t.id);if(!a||s)return pt(e,t,i,Object.assign(Object.assign({},n),{autoEncode:r,useSrcset:a}));const c=Object.assign(Object.assign({},i),function(e,{width:t,height:i}){if(!t||!i){const n=t||Math.min(980,e.width),r=n/e.width;return{width:n,height:i||e.height*r}}return{width:t,height:i}}(t,i)),{alignment:d,htmlTag:h}=c,u=ut(e,t,c),l=function(e,t,{width:i,height:n},r=!1){if(r)return{width:i,height:n};const o=!ht.includes(e),s=ut(e,t,{width:i,height:n}),a=!s&&dt.includes(e),c=a?t.width:i,d=a?t.height:n,h=o?function(e,t){return e>900?t?.05:.15:e>500?t?.1:.18:e>200?.25:1}(c,ve(t.id)):1;return{width:s?1920:c*h,height:d*h}}(e,t,c,o),g=function(e,t,i){return i?0:dt.includes(t)?1:e>200?2:3}(c.width,e,o),m=function(e,t){const i=dt.includes(e)&&!t;return e===C.SCALE_TO_FILL||i?C.SCALE_TO_FIT:e}(e,u),f=lt(e,t,i,d),{uri:p}=pt(m,t,Object.assign(Object.assign({},l),{alignment:d,htmlTag:h}),{autoEncode:r,filters:g?{blur:g}:{}}),{attr:b={},css:I}=pt(e,t,Object.assign(Object.assign({},c),{alignment:d,htmlTag:h}),{});return I.img=I.img||{},I.container=I.container||{},Object.assign(I.img,f.img,mt),Object.assign(I.container,f.container),{uri:p,css:I,attr:b,transformed:!0}}function pt(e,t,i,n){let r={};if(_e(e,t,i)){const o=Ye(e,t,i),s=Ue(e,t,o,n);r.uri=st(e,t,o,n,s),(null==n?void 0:n.useSrcset)&&(r.srcset=function(e,t,i,n,r){const o=i.pixelAspectRatio||1;return{dpr:[`${1===o?r.uri:st(e,t,Object.assign(Object.assign({},i),{pixelAspectRatio:1}),n)} 1x`,`${2===o?r.uri:st(e,t,Object.assign(Object.assign({},i),{pixelAspectRatio:2}),n)} 2x`]}}(e,t,o,n,r)),Object.assign(r,function(e,t){let i;return i=t.htmlTag===j.BG?Ne:t.htmlTag===j.SVG?Fe:Be,i(e,t)}(s,o),{transformed:s.transformed})}else r=V;return r}function bt(e,t,i,n){if(_e(e,t,i)){const r=Ye(e,t,i);return{uri:st(e,t,r,n||{},Ue(e,t,r,n))}}return{uri:""}}const It="https://static.wixstatic.com/media/",_t=/^media\//i,wt="undefined"!=typeof window?window.devicePixelRatio:1,vt=(e,t)=>{const i=t&&t.baseHostURL;return i?`${i}${e}`:(e=>_t.test(e)?`https://static.wixstatic.com/${e}`:`${It}${e}`)(e)};me(),me();const Tt={getScaleToFitImageURL:function(e,t,i,n,r,o){const s=bt(C.SCALE_TO_FIT,{id:e,width:t,height:i,name:o&&o.name},{width:n,height:r,htmlTag:j.IMG,alignment:H.CENTER,pixelAspectRatio:wt},o);return vt(s.uri,o)},getScaleToFillImageURL:function(e,t,i,n,r,o){const s=bt(C.SCALE_TO_FILL,{id:e,width:t,height:i,name:o&&o.name,focalPoint:{x:o&&o.focalPoint&&o.focalPoint.x,y:o&&o.focalPoint&&o.focalPoint.y}},{width:n,height:r,htmlTag:j.IMG,alignment:H.CENTER,pixelAspectRatio:wt},o);return vt(s.uri,o)},getCropImageURL:function(e,t,i,n,r,o,s,a,c,d){const h=bt(C.SCALE_TO_FILL,{id:e,width:t,height:i,name:d&&d.name,crop:{x:n,y:r,width:o,height:s}},{width:a,height:c,htmlTag:j.IMG,alignment:H.CENTER,pixelAspectRatio:wt},d);return vt(h.uri,d)}},Et=It,Lt=(e,t,i)=>{const n=(Array.isArray(t)?t:t.split(".")).reduce(((e,t)=>e&&void 0!==e[t]?e[t]:null),e);return null!==n?n:i};function Ot(...e){let t=e[0];for(let i=1;i<e.length;++i)t=`${t.replace(/\/$/,"")}/${e[i].replace(/^\//,"")}`;return t}const yt=(e,t,i)=>{const{targetWidth:n,targetHeight:r,imageData:o,filters:s,displayMode:a=C.SCALE_TO_FILL}=e;if(!n||!r||!o.uri)return{uri:"",css:{}};const{width:c,height:d,crop:h,name:u,focalPoint:l,upscaleMethod:g,quality:m,devicePixelRatio:f=t.devicePixelRatio}=o,p=Object.assign({filters:s,upscaleMethod:g},m),b=Mt(f),I=pt(a,Object.assign(Object.assign(Object.assign({id:o.uri,width:c,height:d},h&&{crop:h}),l&&{focalPoint:l}),u&&{name:u}),{width:n,height:r,htmlTag:i||"img",pixelAspectRatio:b,alignment:e.alignType||H.CENTER},p);return I.uri=St(I.uri,t.staticMediaUrl,t.mediaRootUrl),I},St=(e,t,i)=>{if(/(^https?)|(^data)|(^blob)|(^\/\/)/.test(e))return e;let n=`${t}/`;return e&&(/^micons\//.test(e)?n=i:"ico"===/[^.]+$/.exec(e)[0]&&(n=n.replace("media","ficons"))),n+e},Mt=e=>{const t=window.location.search.split("&").map((e=>e.split("="))).find((e=>e[0].toLowerCase().includes("devicepixelratio")));return(t?Number(t[1]):null)||e||1},At=(e,t)=>e.getAttribute(t?"xlink:href":"src");function Rt(e){return e.isExperimentOpen("specs.thunderbolt.tb_stop_client_images")||e.isExperimentOpen("specs.thunderbolt.final_force_webp")||e.isExperimentOpen("specs.thunderbolt.final_force_no_webp")}const xt={columnCount:1,columns:1,fontWeight:1,lineHeight:1,opacity:1,zIndex:1,zoom:1},Ct=(e,t)=>e&&t&&Object.keys(t).forEach((i=>e.setAttribute(i,t[i]))),Pt=(e,t)=>e&&t&&Object.keys(t).forEach((i=>{const n=t[i];void 0!==n?e.style[i]=((e,t)=>"number"!=typeof t||xt[e]?t:`${t}px`)(i,n):e.style.removeProperty(i)})),Nt=(e,t)=>e&&t&&Object.keys(t).forEach((i=>{e.style.setProperty(i,t[i])})),Gt=(e,t,i=!0)=>{return e&&i?(n=e.dataset[t])?"true"===n||"false"!==n&&("null"===n?null:""+ +n===n?+n:n):n:e.dataset[t];var n},kt=(e,t)=>e&&t&&Object.assign(e.dataset,t),Bt=()=>window?window.innerHeight||document.documentElement.clientHeight:0,Ft={fit:"contain",fill:"cover"};const Ht=(e=window)=>({measure:function(e,t,i,{containerId:n,bgEffectName:r},o){const s=i[e],a=i[n],{width:c,height:d}=o.getMediaDimensionsByEffect(r,a.offsetWidth,a.offsetHeight,Bt());t.width=c,t.height=d,t.currentSrc=s.style.backgroundImage,t.bgEffectName=s.dataset.bgEffectName},patch:function(t,i,n,r,o){const s=n[t];r.targetWidth=i.width,r.targetHeight=i.height;const a=yt(r,o,"bg");!function(e="",t){return!e.includes(t)||!!e!=!!t}(i.currentSrc,a.uri)?Pt(s,a.css.container):function(t,i){const n=Object.assign({backgroundImage:`url("${i.uri}")`},i.css.container),r=new e.Image;r.onload=Pt.bind(null,t,n),r.src=i.uri}(s,a)}});var Dt=function(e,t,i,n=window){const r=Ht(n);return class extends e{constructor(){super()}reLayout(){if(Rt(t))return;const e={},o={},s=this.getAttribute("id"),a=JSON.parse(this.dataset.tiledImageInfo),{bgEffectName:c}=this.dataset,{containerId:d}=a,h=n.document.getElementById(d);e[s]=this,e[d]=h,a.displayMode=a.imageData.displayMode,t.mutationService.measure((()=>{r.measure(s,o,e,{containerId:d,bgEffectName:c},t)})),t.mutationService.mutate((()=>{r.patch(s,o,e,a,i,t)}))}attributeChangedCallback(e,t){t&&this.reLayout()}disconnectedCallback(){super.disconnectedCallback()}static get observedAttributes(){return["data-tiled-image-info"]}}};var $t=function(e,t,i=window){const n={width:void 0,height:void 0,left:void 0};return class extends e{constructor(){super()}reLayout(){const{containerId:e,pageId:r,useCssVars:o,bgEffectName:s}=this.dataset,a=i.document.getElementById(`${e}`),c=i.document.getElementById(`${r}`),d={};t.mutationService.measure((()=>{const e="fixed"===i.getComputedStyle(this).position,n=i.document.documentElement.clientHeight,r=a.getBoundingClientRect(),h=c.getBoundingClientRect(),u=t.getMediaDimensionsByEffect(s,r.width,r.height,n),{hasParallax:l}=u,{width:g,height:m}=u,f=`${g}px`,p=`${m}px`,b=e?r.left-h.left+"px":(r.width-g)/2+"px",I=e||l?0:(r.height-m)/2+"px",_=o?{"--containerW":f,"--containerH":p,"--containerL":b,"--screenH_val":`${n}`}:{width:f,height:p,left:b,top:I};Object.assign(d,_)})),t.mutationService.mutate((()=>{o?(Pt(this,n),Nt(this,d)):Pt(this,d)}))}connectedCallback(){super.connectedCallback(),t.windowResizeService.observe(this)}disconnectedCallback(){super.disconnectedCallback(),t.windowResizeService.unobserve(this)}attributeChangedCallback(e,t){t&&this.reLayout()}static get observedAttributes(){return["data-is-full-height","data-container-size"]}}};const jt="__more__",Wt="moreContainer",zt=(e=window)=>{const t=(e,t,i,n,r,o,s,a)=>{if(e-=r*(s?n.length:n.length-1),e-=a.left+a.right,t&&(n=n.map((()=>o))),n.some((e=>0===e)))return null;let c=0;const d=n.reduce(((e,t)=>e+t),0);if(d>e)return null;if(t){if(i){const t=Math.floor(e/n.length),i=n.map((()=>t));if(c=t*n.length,c<e){const t=Math.floor(e-c);n.forEach(((e,n)=>{n<=t-1&&i[n]++}))}return i}return n}if(i){const t=Math.floor((e-d)/n.length);c=0;const i=n.map((e=>(c+=e+t,e+t)));if(c<e){const t=Math.floor(e-c);n.forEach(((e,n)=>{n<=t-1&&i[n]++}))}return i}return n},i=e=>Math.round(e),n=e=>{const t=parseFloat(e);return isFinite(t)?t:0},r=t=>t.getBoundingClientRect().top>e.innerHeight/2,o=(e,t,i,n)=>{const{width:r,height:o,alignButtons:s,hoverListPosition:a,menuItemContainerExtraPixels:c}=t,d=t.absoluteLeft,h=((e,t,i,n,r,o,s,a,c)=>{let d="0px",h="auto";const u=o.left,l=o.width;"left"===t?d="left"===r?0:`${u+e.left}px`:"right"===t?(h="right"===r?0:n-u-l-e.right+"px",d="auto"):"left"===r?d=`${u+(l+e.left-i)/2}px`:"right"===r?(d="auto",h=(l+e.right-(i+e.width))/2+"px"):d=`${e.left+u+(l-(i+e.width))/2}px`,"auto"!==d&&(d=s+parseInt(d,10)<0?0:d);"auto"!==h&&(h=a-parseInt(h,10)>c?0:h);return{moreContainerLeft:d,moreContainerRight:h}})(c,s,n,r,a,i,d,d+r,t.bodyClientWidth);return{left:h.moreContainerLeft,right:h.moreContainerRight,top:t.needToOpenMenuUp?"auto":`${o}px`,bottom:t.needToOpenMenuUp?`${o}px`:"auto"}},s=e=>!isNaN(parseFloat(e))&&isFinite(e);return{measure:(o,s)=>{const a={},c={};c[o]=e.document.getElementById(`${o}`);let d=1;s.siteService&&(d=s.siteService.getSiteScale());const h=(e=>{const t=+Gt(e,"numItems");return t<=0||t>Number.MAX_SAFE_INTEGER?[]:new Array(t).fill(0).map(((e,t)=>String(t)))})(c[o]),u=(e=>["moreContainer","itemsContainer","dropWrapper"].concat(e,[jt]))(h);u.forEach((t=>{const i=`${o}${t}`;c[i]=e.document.getElementById(`${i}`)})),a.children=((e,t,n,r)=>{const o={};return n.forEach((n=>{const s=`${e}${n}`,a=t[s];a&&(o[s]={width:a.offsetWidth,boundingClientRectWidth:i(a.getBoundingClientRect().width/r),height:a.offsetHeight})})),o})(o,c,u,d);const l=c[o],g=c[`${o}itemsContainer`],m=g.childNodes,f=c[`${o}moreContainer`],p=f.childNodes,b=Gt(l,"stretchButtonsToMenuWidth"),I=Gt(l,"sameWidthButtons"),_=l.getBoundingClientRect();a.absoluteLeft=_.left,a.bodyClientWidth=e.document.body.clientWidth,a.alignButtons=Gt(l,"dropalign"),a.hoverListPosition=Gt(l,"drophposition"),a.menuBorderY=parseInt(Gt(l,"menuborderY"),10),a.ribbonExtra=parseInt(Gt(l,"ribbonExtra"),10),a.ribbonEls=parseInt(Gt(l,"ribbonEls"),10),a.labelPad=parseInt(Gt(l,"labelPad"),10),a.menuButtonBorder=parseInt(Gt(l,"menubtnBorder"),10),a.menuItemContainerMargins=(t=>{const i=t.lastChild,n=e.getComputedStyle(i);return(parseInt(n.marginLeft,10)||0)+(parseInt(n.marginRight,10)||0)})(g),a.menuItemContainerExtraPixels=((t,i)=>{const r=e.getComputedStyle(t);let o=n(r.borderTopWidth)+n(r.paddingTop),s=n(r.borderBottomWidth)+n(r.paddingBottom),a=n(r.borderLeftWidth)+n(r.paddingLeft),c=n(r.borderRightWidth)+n(r.paddingRight);return i&&(o+=n(r.marginTop),s+=n(r.marginBottom),a+=n(r.marginLeft),c+=n(r.marginRight)),{top:o,bottom:s,left:a,right:c,height:o+s,width:a+c}})(g,!0),a.needToOpenMenuUp=r(l),a.menuItemMarginForAllChildren=!b||"false"!==g.getAttribute("data-marginAllChildren"),a.moreSubItem=[],a.labelWidths={},a.linkIds={},a.parentId={},a.menuItems={},a.labels={},p.forEach(((t,i)=>{a.parentId[t.id]=Gt(t,"parentId");const n=Gt(t,"dataId");a.menuItems[n]={dataId:n,parentId:Gt(t,"parentId"),moreDOMid:t.id,moreIndex:i},c[t.id]=t;const r=t.querySelector("p");c[r.id]=r,a.labels[r.id]={width:r.offsetWidth,height:r.offsetHeight,left:r.offsetLeft,lineHeight:parseInt(e.getComputedStyle(r).fontSize,10)},a.moreSubItem.push(t.id)})),m.forEach(((e,t)=>{const n=Gt(e,"dataId");a.menuItems[n]=a.menuItems[n]||{},a.menuItems[n].menuIndex=t,a.menuItems[n].menuDOMid=e.id,a.children[e.id].left=e.offsetLeft;const r=e.querySelector("p");c[r.id]=r,a.labelWidths[r.id]=((e,t)=>i(e.getBoundingClientRect().width/t))(r,d);const o=e.querySelector("p");c[o.id]=o,a.linkIds[e.id]=o.id}));const w=l.offsetHeight;a.height=w,a.width=l.offsetWidth,a.lineHeight=((e,t)=>e-t.menuBorderY-t.labelPad-t.ribbonEls-t.menuButtonBorder-t.ribbonExtra+"px")(w,a);const v=((e,i,n,r,o)=>{const s=i.width;i.hasOriginalGapData={},i.originalGapBetweenTextAndBtn={};const a=o.map((t=>{const n=r[e+t];let o;const s=Gt(n,"originalGapBetweenTextAndBtn");return void 0===s?(i.hasOriginalGapData[t]=!1,o=i.children[e+t].boundingClientRectWidth-i.labelWidths[`${e+t}label`],i.originalGapBetweenTextAndBtn[e+t]=o):(i.hasOriginalGapData[t]=!0,o=parseFloat(s)),i.children[e+t].width>0?Math.floor(i.labelWidths[`${e+t}label`]+o):0})),c=a.pop(),d=n.sameWidthButtons,h=n.stretchButtonsToMenuWidth;let u=!1;const l=i.menuItemContainerMargins,g=i.menuItemMarginForAllChildren,m=i.menuItemContainerExtraPixels,f=(e=>e.reduce(((e,t)=>e>t?e:t),-1/0))(a);let p=t(s,d,h,a,l,f,g,m);if(!p){for(let e=1;e<=a.length;e++)if(p=t(s,d,h,a.slice(0,-1*e).concat(c),l,f,g,m),p){u=!0;break}p||(u=!0,p=[c])}if(u){const e=p[p.length-1];for(p=p.slice(0,-1);p.length<o.length;)p.push(0);p[p.length-1]=e}return{realWidths:p,moreShown:u}})(o,a,{sameWidthButtons:I,stretchButtonsToMenuWidth:b},c,h.concat(jt));return a.realWidths=v.realWidths,a.isMoreShown=v.moreShown,a.menuItemIds=h,a.hoverState=Gt(f,"hover",!1),{measures:a,domNodes:c}},patch:(e,t,i)=>{const n=i[e];Pt(n,{overflowX:"visible"});const{menuItemIds:r,needToOpenMenuUp:a}=t,c=r.concat(jt);kt(n,{dropmode:a?"dropUp":"dropDown"});let d=0;if(t.hoverState===jt){const e=t.realWidths.indexOf(0),n=t.menuItems[(h=t.menuItems,u=t=>t.menuIndex===e,Object.keys(h).find((e=>u(h[e],e))))],o=n.moreIndex,s=o===r.length-1;n.moreDOMid&&Ct(i[n.moreDOMid],{"data-listposition":s?"dropLonely":"top"}),Object.values(t.menuItems).filter((e=>!!e.moreDOMid)).forEach((e=>{if(e.moreIndex<o)Pt(i[e.moreDOMid],{display:"none"});else{const i=`${e.moreDOMid}label`;d=Math.max(t.labels[i].width,d)}}))}else t.hoverState&&t.moreSubItem.forEach(((i,n)=>{const r=`${e+Wt+n}label`;d=Math.max(t.labels[r].width,d)}));var h,u;((e,t,i,n)=>{const{hoverState:r}=t;if("-1"!==r){const{menuItemIds:a}=t,c=a.indexOf(r);if(s(t.hoverState)||r===jt){if(!t.realWidths)return;const r=Math.max(n,t.children[-1!==c?e+c:e+jt].width),a=((e,t)=>e+15+t.menuBorderY+t.labelPad+t.menuButtonBorder)(0!==t.moreSubItem.length?t.labels[`${t.moreSubItem[0]}label`].lineHeight:0,t);t.moreSubItem.forEach((e=>{Pt(i[e],{minWidth:`${r}px`}),Pt(i[`${e}label`],{minWidth:"0px",lineHeight:`${a}px`})}));const d=s(t.hoverState)?t.hoverState:"__more__",h={width:t.children[e+d].width,left:t.children[e+d].left},u=o(0,t,h,r);Pt(i[`${e}moreContainer`],{left:u.left,right:u.right}),Pt(i[`${e}dropWrapper`],{left:u.left,right:u.right,top:u.top,bottom:u.bottom})}}})(e,t,i,d),t.originalGapBetweenTextAndBtn&&c.forEach((n=>{t.hasOriginalGapData[n]||kt(i[`${e}${n}`],{originalGapBetweenTextAndBtn:t.originalGapBetweenTextAndBtn[`${e}${n}`]})})),((e,t,i,n)=>{const{realWidths:r,height:o,menuItemContainerExtraPixels:s}=i;let a=0,c=null,d=null;const h=i.lineHeight,u=o-s.height;for(let o=0;o<n.length;o++){const s=r[o],l=s>0,g=e+n[o];d=i.linkIds[g],l?(a++,c=g,Pt(t[g],{width:`${s}px`,height:`${u}px`,position:"relative","box-sizing":"border-box",overflow:"visible",visibility:"inherit"}),Pt(t[`${g}label`],{"line-height":h}),Ct(t[g],{"aria-hidden":!1})):(Pt(t[g],{height:"0px",overflow:"hidden",position:"absolute",visibility:"hidden"}),Ct(t[g],{"aria-hidden":!0}),Ct(t[d],{tabIndex:-1}))}1===a&&(kt(t[`${e}moreContainer`],{listposition:"lonely"}),kt(t[c],{listposition:"lonely"}))})(e,i,t,c)}}};var Ut=function(e,t,i=window){const n=zt(i);return class extends e{constructor(){super(...arguments),this._visible=!1,this._mutationIds={read:null,write:null},this._itemsContainer=null,this._dropContainer=null,this._labelItems=[]}static get observedAttributes(){return["data-hovered-item"]}attributeChangedCallback(){this._isVisible()&&this.reLayout()}connectedCallback(){this._id=this.getAttribute("id"),this._hideElement(),this._waitForDomLoad().then((()=>{super.observeResize(),this._observeChildrenResize(),this.reLayout()}))}disconnectedCallback(){t.mutationService.clear(this._mutationIds.read),t.mutationService.clear(this._mutationIds.write),super.disconnectedCallback()}_waitForDomLoad(){let e;const t=new Promise((t=>{e=t}));return this._isDomReady()?e():(this._waitForDomReadyObserver=new i.MutationObserver((()=>this._onRootMutate(e))),this._waitForDomReadyObserver.observe(this,{childList:!0,subtree:!0})),t}_isDomReady(){return this._itemsContainer=i.document.getElementById(`${this._id}itemsContainer`),this._dropContainer=i.document.getElementById(`${this._id}dropWrapper`),this._itemsContainer&&this._dropContainer}_onRootMutate(e){this._isDomReady()&&(this._waitForDomReadyObserver.disconnect(),e())}_observeChildrenResize(){const e=Array.from(this._itemsContainer.childNodes);this._labelItems=e.map((e=>i.document.getElementById(`${e.getAttribute("id")}label`))),this._labelItems.forEach((e=>super.observeChildResize(e)))}_setVisibility(e){this._visible=e,this.style.visibility=e?"inherit":"hidden"}_isVisible(){return this._visible}_hideElement(){this._setVisibility(!1)}_showElement(){this._setVisibility(!0)}reLayout(){let e,i;t.mutationService.clear(this._mutationIds.read),t.mutationService.clear(this._mutationIds.write),this._mutationIds.read=t.mutationService.measure((()=>{const r=n.measure(this._id,t);e=r.measures,i=r.domNodes})),this._mutationIds.write=t.mutationService.mutate((()=>{n.patch(this._id,e,i),this._showElement()}))}}};var Yt=function(e,t=window){class i extends t.HTMLElement{constructor(){super()}reLayout(){}connectedCallback(){this.observeResize(),this.reLayout()}disconnectedCallback(){this.unobserveResize(),this.unobserveChildren()}observeResize(){e.resizeService.observe(this)}unobserveResize(){e.resizeService.unobserve(this)}observeChildren(e){this.childListObserver||(this.childListObserver=new t.MutationObserver((()=>this.reLayout()))),this.childListObserver.observe(e,{childList:!0})}observeChildAttributes(e,i=[]){this.childrenAttributesObservers||(this.childrenAttributesObservers=[]);const n=new t.MutationObserver((()=>this.reLayout()));n.observe(e,{attributeFilter:i}),this.childrenAttributesObservers.push(n)}observeChildResize(t){this.childrenResizeObservers||(this.childrenResizeObservers=[]),e.resizeService.observeChild(t,this),this.childrenResizeObservers.push(t)}unobserveChildrenResize(){this.childrenResizeObservers&&(this.childrenResizeObservers.forEach((t=>{e.resizeService.unobserveChild(t)})),this.childrenResizeObservers=null)}unobserveChildren(){if(this.childListObserver&&(this.childListObserver.disconnect(),this.childListObserver=null),this.childrenAttributesObservers){for(let e of this.childrenAttributesObservers)e.disconnect(),e=null;this.childrenAttributesObservers=null}this.unobserveChildrenResize()}}return i},Vt={APP_IFRAME_START_LOADING:{eventId:642,src:42,params:{widget_id:"widgetId",widget_name:"widgetName",instance_id:"compId",appId:"appDefinitionId",loading_time:"loadingTime",pid:"pageId",pn:"pageNo",iss:"ssr",tts:"totalLoadingTime",external_app_id:"externalAppDefinitionId",external_widget_id:"externalWidgetId",lazy_load:"lazyLoad"}}};var qt=function(e,t){return class extends e{constructor(){super()}reportIframeStartLoading(e){const{isTpa:i,widgetId:n,appDefinitionId:r}=this.dataset;t&&t.biService&&"true"===i&&t.biService.reportTpaBiEvent({reportDef:Vt.APP_IFRAME_START_LOADING,params:{},compId:e.getAttribute("name"),isWixTPA:!0,widgetId:n,appDefinitionId:r})}reLayout(){const e=this.querySelector("iframe");if(e){const t=e.dataset.src;t&&e.src!==t&&(e.src=t,e.dataset.src="",this.dataset.src="",this.reportIframeStartLoading(e))}}attributeChangedCallback(e,t,i){i&&this.reLayout()}static get observedAttributes(){return["data-src"]}}};const Zt="scroll-css-var--scrollEffect";var Jt=class{constructor(e,t=window){this.mutationService=e,(e=>e&&"IntersectionObserver"in e&&"IntersectionObserverEntry"in e&&"intersectionRatio"in e.IntersectionObserverEntry.prototype&&"isIntersecting"in e.IntersectionObserverEntry.prototype&&!(e=>/Edge\/18/.test(e.navigator.userAgent))(e))(t)&&(this.intersectionObserver=new t.IntersectionObserver(this.getViewPortIntersectionHandler(),{rootMargin:"50% 0px"}),this.scrollEffectsIntersectionObserver=new t.IntersectionObserver(this.getScrollEffectsIntersectionHandler(),{rootMargin:"10% 0px"}))}isImageInViewPort(e,t){return e.top+e.height>=0&&e.bottom-e.height<=t}loadImage(e,{screenHeight:t,boundingRect:i,withScrollEffectVars:n}){!this.intersectionObserver||this.isImageInViewPort(i,t)?this.setImageSource(e):(this.intersectionObserver.unobserve(e),this.intersectionObserver.observe(e)),n&&this.scrollEffectsIntersectionObserver&&(this.scrollEffectsIntersectionObserver.unobserve(e),this.scrollEffectsIntersectionObserver.observe(e))}onImageDisconnected(e){this.intersectionObserver&&this.intersectionObserver.unobserve(e),this.scrollEffectsIntersectionObserver&&this.scrollEffectsIntersectionObserver.unobserve(e)}setSrcAttribute(e,t,i,n){At(e,t)!==n&&(t?e.setAttributeNS("http://www.w3.org/1999/xlink","xlink:href",n):(e.hasAttribute("loading")&&e.removeAttribute("loading"),i&&e.setAttribute("srcset",n),e.src=n))}setSourceSetAttribute(e,t){e.srcset!==t&&(e.srcset=t)}setImageSource(e){const t="true"===e.dataset.isSvg,i=e.querySelector(t?"image":"img"),n=i.hasAttribute("srcset"),r=e.querySelector("picture");this.setSrcAttribute(i,t,n,e.dataset.src),r&&Array.from(r.querySelectorAll("source")).forEach((e=>{this.setSourceSetAttribute(e,e.dataset.srcset)}))}getViewPortIntersectionHandler(){return(e,t)=>{e.filter((e=>e.isIntersecting)).forEach((e=>{const i=e.target;this.setImageSource(i),t.unobserve(i)}))}}getScrollEffectsIntersectionHandler(){return e=>e.forEach((e=>{const t=e.target;e.isIntersecting?this.mutationService.mutate((()=>t.classList.add(Zt))):this.mutationService.mutate((()=>t.classList.remove(Zt)))}))}},Xt={eventId:348,adapter:"ugc-viewer",params:{ow:"originalWidth",oh:"originalHeight",tw:"targetWidth",th:"targetHeight",dpr:"devicePixelRatio",um:"upscaleMethod",url:"url"}};function Qt(e,t){const i=function(e){const t=(0,o._T)(e,[]),i={};for(e in t)""!==t[e]&&(i[e]=t[e]);return i}(e);return"number"==typeof t&&(i.opacity=t),i}var Kt={measure:function(e,t,i,{containerElm:n,isSvgImage:r,isSvgMask:o,mediaHeightOverrideType:s,bgEffectName:a},c){const d=i.image,h=i[e],u=Bt(),l=n&&a?n:h,{width:g,height:m}=c.getMediaDimensionsByEffect(a,l.offsetWidth,l.offsetHeight,u);if(!d)return;const f=At(d,r);t.width=g,t.screenHeight=u,t.height=function(e,t){return"fixed"===e||"viewport"===e?document.documentElement.clientHeight+80:t}(s,m),t.isZoomed=h.getAttribute("data-image-zoomed"),t.isSvgImage=r,t.imgSrc=f,t.renderedStyles=h.getAttribute("data-style"),t.boundingRect=h.getBoundingClientRect(),t.mediaHeightOverrideType=s,o&&(t.bBox=function(e){if(e){const{type:t}=e.dataset;if(t&&"ugc"!==t&&!e.dataset.bbox){const{x:t,y:i,width:n,height:r}=e.getBBox();return`${t} ${i} ${n} ${r}`}}return null}(i.maskSvg))},patch:function(e,t,i,n,r,o,s,a){if(!Object.keys(t).length)return;const c=(d=t.renderedStyles)&&d.split?d.split(";").reduce((function(e,t){const i=t.split(":");return i[0]&&i[1]&&(e[i[0].trim()]=i[1].trim()),e}),{}):{};var d;const{imageData:h}=n;a&&(h.devicePixelRatio=1);const u=Object.assign(Object.assign(Object.assign({},n),n.skipMeasure?{}:{targetWidth:t.isZoomed?h.width:t.width,targetHeight:t.isZoomed?h.height:t.height}),{displayMode:h.displayMode});let l;if(t.isSvgImage)l=yt(u,o,"svg"),Ct(i.svg,t.isZoomed?l.attr.container:{});else{l=yt(u,o,"img");const e=Lt(l,["css","img"])||{},n=function(e,t,i){if(!e)return t;const n=Object.assign({},t);return"fill"===i&&(n.position="absolute",n.top=0),"fit"===i&&(n.height="100%"),"fixed"===e&&(n["will-change"]="transform"),n.objectPosition&&(n.objectPosition=t.objectPosition.replace(/(center|bottom)$/,"top")),n}(t.mediaHeightOverrideType,e,h.displayMode);Pt(i.image,n)}t.bBox&&i.maskSvg&&Ct(i.maskSvg,{viewBox:t.bBox});const g=Qt(c,h.opacity);Pt(i[e],g);const m=Lt(l,"uri"),f=t.imgSrc;Ct(i[e],{"data-src":m}),Ct(i[e],{"data-has-ssr-src":""}),s&&(!function(e,t,i,n,r){const o=r.uri.match(/,lg_(\d)/);t.isViewerMode&&r.uri!==n.currentSrc&&o&&e.reportBI(Xt,{originalWidth:i.imageData.width,originalHeight:i.imageData.height,targetWidth:Math.round(i.targetWidth),targetHeight:Math.round(i.targetHeight),upscaleMethod:"1"===o[1]?"classic":"super",devicePixelRatio:Math.floor(100*t.devicePixelRatio),url:n.src})}(r.biService,o,u,{src:m,currentSrc:f},l),r.imageLoader.loadImage(i[e],{screenHeight:t.screenHeight,boundingRect:t.boundingRect}))}};function ei(e,t,i=1.5){return{parallax:e.height*i,fixed:e.screenHeight}[t]||e.height}var ti={measure:function(e,t,i){const n=i.image;if(!n)return;const r=At(n);t.width=i[e].offsetWidth,t.height=i[e].offsetHeight,t.imgSrc=r,t.screenHeight=Bt(),t.boundingRect=i[e].getBoundingClientRect(),t.documentScroll=window?window.pageYOffset||document.documentElement.scrollTop:0},patch:function(e,t,i,n,r,o,s){const{imageData:a,parallaxSpeed:c}=n,d=Object.assign(Object.assign({},n),{targetWidth:t.width,targetHeight:ei(t,a.scrollEffect,c),displayMode:a.displayMode}),h=function(e){const t={};return"number"==typeof e&&(t.opacity=e),t}(a.opacity);Pt(i[e],h);const u=yt(d,o,"img"),l=Lt(u,"uri");Ct(i[e],{"data-src":l});const g=function(e,t=[]){return"parallax"===e||t.some((e=>"parallax"===e.scrollEffect))}(a.scrollEffect,n.sourceSets);g&&Nt(i[e],function(e){return{"--compH":e.height,"--top":Math.ceil(e.boundingRect.top)+e.documentScroll,"--scroll":e.documentScroll}}(t));const m=function(e){const t=Lt(e,["css","img"]);return{width:"100%",objectFit:t?t.objectFit:void 0}}(u);Pt(i.image,m),i.picture&&function(e,t,i,n){const{sourceSets:r}=t;if(!r||!r.length)return;const o=JSON.parse(JSON.stringify(t)),{parallaxSpeed:s}=o;r.forEach((t=>{const r=n.querySelector(`source[media='${t.mediaQuery}']`);o.imageData.crop=t.crop,o.imageData.displayMode=t.displayMode,o.imageData.focalPoint=t.focalPoint,o.targetHeight=ei(e,t.scrollEffect,s);const a=yt(o,i,"img");Ct(r,{"data-srcset":Lt(a,"uri")})}))}(t,d,o,i.picture),Ct(i[e],{"data-has-ssr-src":""}),s&&r.imageLoader.loadImage(i[e],{screenHeight:t.screenHeight,boundingRect:t.boundingRect,withScrollEffectVars:g})}};var ii=function(e,t,i,n=window){return t.imageLoader||(t.imageLoader=new Jt(t.mutationService,n)),class extends e{constructor(){super(),this.childListObserver=null,this.timeoutId=null}reLayout(){if(Rt(t))return;const e={},r={},o=this.getAttribute("id"),s=JSON.parse(this.dataset.imageInfo),a="true"===this.dataset.isSvg,c="true"===this.dataset.isSvgMask,d="true"===this.dataset.isResponsive,{bgEffectName:h}=this.dataset;e[o]=this,s.containerId&&(e[s.containerId]=n.document.getElementById(`${s.containerId}`)),e.image=this.querySelector(a?"image":"img"),e.svg=a?this.querySelector("svg"):null,e.picture=this.querySelector("picture");const u=s.containerId&&e[s.containerId],l=u&&u.dataset.mediaHeightOverrideType;if(c&&(e.maskSvg=e.svg&&e.svg.querySelector("svg")),!e.image){const t=a&&e.svg||this;return void this.observeChildren(t)}this.unobserveChildren(),this.observeChildren(this);const g=d||e.picture?ti:Kt;t.mutationService.measure((()=>{g.measure(o,r,e,{containerElm:u,isSvg:a,isSvgMask:c,mediaHeightOverrideType:l,bgEffectName:h},t)}));const m=n=>{t.mutationService.mutate((()=>{g.patch(o,r,e,s,t,i,n,h)}))};!At(e.image,a)||this.dataset.hasSsrSrc?m(!0):this.debounceImageLoad(m)}debounceImageLoad(e){clearTimeout(this.timeoutId),this.timeoutId=setTimeout((()=>{e(!0)}),250),e(!1)}attributeChangedCallback(e,t){t&&this.reLayout()}disconnectedCallback(){super.disconnectedCallback(),t.imageLoader.onImageDisconnected(this),this.unobserveChildren()}static get observedAttributes(){return["data-image-info"]}}},ni={measure(e,t,{hasBgScrollEffect:i,videoWidth:n,videoHeight:r,fittingType:o,alignType:s="center",qualities:a,staticVideoUrl:c,videoId:d,videoFormat:h,focalPoint:u}){const l=i?t.offsetWidth:e.parentElement.offsetWidth,g=e.parentElement.offsetHeight,m=parseInt(n,10),f=parseInt(r,10),p=function(e,t,i,n){return{wScale:e/i,hScale:t/n}}(l,g,m,f),b=function(e,t,i,n){let r;r=e===C.SCALE_TO_FIT?Math.min(t.wScale,t.hScale):Math.max(t.wScale,t.hScale);return{width:Math.round(i*r),height:Math.round(n*r)}}(o,p,m,f),I=function(e,{width:t,height:i}){const n=((e,t)=>{const i=e.reduce(((e,i)=>(e[t(i)]=i,e)),{});return Object.values(i)})(e,(e=>e.size));return n.find((e=>e.size>t*i))||e[e.length-1]}(a,b),_=function(e,t,i,n){if("mp4"===n)return e.url?Ot(t,e.url):Ot(t,i,e.quality,n,"file.mp4");return""}(I,c,d,h),w=function(e,t){const i=e.networkState===e.NETWORK_NO_SOURCE,n=!e.currentSrc.endsWith(t);return t&&(n||i)}(e,_),v=Ft[o]||"cover",T=u?function(e,t,i){const{width:n,height:r}=e,{width:o,height:s}=t,{x:a,y:c}=i;if(!o||!s)return`${a}% ${c}%`;const d=Math.max(o/n,s/r),h=n*d,u=r*d,l=Math.max(0,Math.min(h-o,h*(a/100)-o/2)),g=Math.max(0,Math.min(u-s,u*(c/100)-s/2)),m=l&&Math.floor(l/(h-o)*100),f=g&&Math.floor(g/(u-s)*100);return`${m}% ${f}%`}(b,{width:l,height:g},u):"",E=s.replace("_"," ");return{videoSourceUrl:_,needsSrcUpdate:w,videoStyle:{height:"100%",width:"100%",objectFit:v,objectPosition:T||E}}},mutate(e,t,i,n,r,o,s,a,c,d,h){t?Pt(t,n):(!function(e,t,i,n,r,o){o&&t.paused&&(i.style.opacity="1",t.style.opacity="0");const s=t.paused||""===t.currentSrc;if((e||o)&&s)if(t.ontimeupdate=null,t.onseeked=null,t.onplay=null,!o&&r){const e=t.muted;t.muted=!0,t.ontimeupdate=()=>{t.currentTime>0&&(t.ontimeupdate=null,t.onseeked=()=>{t.onseeked=null,t.muted=e,ri(t,i,n)},t.currentTime=0)}}else t.onplay=()=>{t.onplay=null,ri(t,i,n)}}(s,i,e,a,r,h),r?i.setAttribute("autoplay",""):i.removeAttribute("autoplay"),Pt(i,n)),function(e,t,i){e&&(t.src=i,t.load())}(s,i,o),i.playbackRate=d}};function ri(e,t,i){"fade"===i&&(t.style.transition="opacity 1.6s ease-out"),t.style.opacity="0",e.style.opacity="1"}var oi=function(e,t,i,n=window){return class extends e{constructor(){super()}reLayout(){const{isVideoDataExists:e,videoWidth:r,videoHeight:o,qualities:s,videoId:a,videoFormat:c,alignType:d,fittingType:h,focalPoint:u,hasBgScrollEffect:l,autoPlay:g,animatePoster:m,containerId:f,isEditorMode:p,playbackRate:b,hasAlpha:I}=JSON.parse(this.dataset.videoInfo);if(!e)return;const _=!i.prefersReducedMotion&&g,w=this.querySelector(`video[id^="${f}"]`),v=this.querySelector(`.bgVideoposter[id^="${f}"]`);if(this.unobserveChildren(),!w||!v)return void this.observeChildren(this);const T=n.document.getElementById(`${f}`),E=T.querySelector(`.webglcanvas[id^="${f}"]`);!(I||"true"===T.dataset.hasAlpha)||E?t.mutationService.measure((()=>{const e=ni.measure(w,T,{hasBgScrollEffect:l,videoWidth:r,videoHeight:o,fittingType:h,alignType:d,qualities:s,staticVideoUrl:i.staticVideoUrl,videoId:a,videoFormat:c,focalPoint:u}),{videoSourceUrl:n,needsSrcUpdate:g,videoStyle:f}=e;t.mutationService.mutate((()=>{ni.mutate(v,E,w,f,_,n,g,m,c,b,p)}))})):requestAnimationFrame((()=>this.reLayout()))}attributeChangedCallback(e,t){t&&this.reLayout()}static get observedAttributes(){return["data-video-info"]}}};var si={init:function(e,t=window){!
                    /**
                     * @license
                     * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
                     * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
                     * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
                     * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
                     * Code distributed by Google as part of the polymer project is also
                     * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
                     */function(e){if(void 0===e.Reflect||void 0===e.customElements||e.customElements.hasOwnProperty("polyfillWrapFlushCallback"))return;const t=e.HTMLElement;e.HTMLElement=function(){return e.Reflect.construct(t,[],this.constructor)},e.HTMLElement.prototype=t.prototype,e.HTMLElement.prototype.constructor=e.HTMLElement,e.Object.setPrototypeOf(e.HTMLElement,t)}(t);const i=e.resizeService.init((e=>{r.getLayoutTargets(e.map((e=>e.target))).forEach((e=>e.reLayout()))})),n={registry:new Set,observe(e){n.registry.add(e)},unobserve(e){n.registry.delete(e)}};e.windowResizeService.init(((e,t=window)=>{let i=!1;return(...n)=>{i||(i=!0,t.requestAnimationFrame((()=>{i=!1,e(...n)})))}})((()=>n.registry.forEach((e=>e.reLayout())))),t);const r={observedElementToRelayoutTarget:new Map,getLayoutTargets(e){const t=new Set;return e.forEach((e=>t.add(r.observedElementToRelayoutTarget.get(e)))),t},observe:e=>{r.observedElementToRelayoutTarget.set(e,e),i.observe(e)},unobserve:e=>{r.observedElementToRelayoutTarget.delete(e),i.unobserve(e)},observeChild:(e,t)=>{r.observedElementToRelayoutTarget.set(e,t),i.observe(e)},unobserveChild:e=>{r.observedElementToRelayoutTarget.delete(e),i.unobserve(e)}},o=(e,i)=>{void 0===t.customElements.get(e)&&t.customElements.define(e,i)},s=Yt({resizeService:r},t);return o("wix-element",s),{defineWixImage:(e,i)=>{const n=ii(s,e,i,t);o("wix-image",n)},defineWixBgImage:(e,i)=>{const n=Dt(s,e,i,t);o("wix-bg-image",n)},defineWixBgMedia:e=>{const i=$t(s,Object.assign({windowResizeService:n},e),t);o("wix-bg-media",i)},defineWixDropdownMenu:e=>{const i=Ut(s,e,t);o("wix-dropdown-menu",i)},defineWixVideo:(e,i)=>{const n=oi(s,e,i,t);o("wix-video",n)},defineWixIframe:e=>{const t=qt(s,e);o("wix-iframe",t)}}}},ai=Object.assign({imageClientApi:r},si);const ci={columnCount:1,columns:1,fontWeight:1,lineHeight:1,opacity:1,zIndex:1,zoom:1},di=(e,t)=>(Array.isArray(t)?t:[t]).reduce(((t,i)=>{const n=e[i];return void 0!==n?Object.assign(t,{[i]:n}):t}),{}),hi=(e,t)=>e&&t&&Object.keys(t).forEach((i=>{const n=t[i];void 0!==n?e.style[i]=((e,t)=>"number"!=typeof t||ci[e]?t:`${t}px`)(i,n):e.style.removeProperty(i)})),ui=(e,t,i)=>{var n;if(/(^https?)|(^data)|(^blob)|(^\/\/)/.test(e))return e;let r=`${t}/`;return e&&(/^micons\//.test(e)?r=i:"ico"===(null===(n=/[^.]+$/.exec(e))||void 0===n?void 0:n[0])&&(r=r.replace("media","ficons"))),r+e},li=e=>{const t=window.location.search.split("&").map((e=>e.split("="))).find((e=>e[0].toLowerCase().includes("devicepixelratio")));return(t?Number(t[1]):null)||e||1},gi=e=>e.getAttribute("src"),mi=()=>window&&"IntersectionObserver"in window&&"IntersectionObserverEntry"in window&&"intersectionRatio"in window.IntersectionObserverEntry.prototype&&"isIntersecting"in window.IntersectionObserverEntry.prototype&&!/Edge\/18/.test(window.navigator.userAgent);var fi=class{constructor(e){this.mutationService=e,mi()&&(this.intersectionObserver=new IntersectionObserver(this.getViewPortIntersectionHandler(),{rootMargin:"50% 0px"}))}isImageInViewPort(e,t){return e.top+e.height>=0&&e.bottom-e.height<=t}loadImage(e,{screenHeight:t,boundingRect:i}){!this.intersectionObserver||this.isImageInViewPort(i,t)?this.setImageSource(e):(this.intersectionObserver.unobserve(e),this.intersectionObserver.observe(e))}onImageDisconnected(e){this.intersectionObserver&&this.intersectionObserver.unobserve(e)}setSrcAttribute(e,t){gi(e)!==t&&(e.src=t)}setImageSource(e){const t=e.querySelector("img");this.setSrcAttribute(t,e.dataset.src)}getViewPortIntersectionHandler(){return(e,t)=>{e.filter((e=>e.isIntersecting)).forEach((e=>{const i=e.target;this.setImageSource(i),t.unobserve(i)}))}}};var pi={measure:function(e,t,i,{containerElm:n,bgEffectName:r="none"},o){var s;const a=i.image,c=i[e],d=window?window.innerHeight||document.documentElement.clientHeight:0,h=null==n?void 0:n.dataset.mediaHeightOverrideType,u=n&&"none"!==r?n:c,{width:l,height:g}=(null===(s=o.getMediaDimensionsByEffect)||void 0===s?void 0:s.call(o,r,u.offsetWidth,u.offsetHeight,d))||{width:c.offsetWidth,height:c.offsetHeight};if(!a)return;const m=gi(a);t.width=l,t.height=function(e,t){return"fixed"===t?document.documentElement.clientHeight+80:e}(g,h),t.screenHeight=d,t.imgSrc=m,t.boundingRect=c.getBoundingClientRect(),t.mediaHeightOverrideType=h},patch:function(e,t,i,n,r,o,s,a){var c;if(!Object.keys(t).length)return;const{imageData:d}=n,h=i[e],u=i.image;a&&(d.devicePixelRatio=1);const l=((e,t,i)=>{if(!e.targetWidth||!e.targetHeight||!e.imageData.uri)return{uri:"",css:{},transformed:!1};const{imageData:n}=e,r=e.displayMode||C.SCALE_TO_FILL,o=Object.assign(di(n,"upscaleMethod"),di(e,"filters"),e.quality||n.quality),s=e.imageData.devicePixelRatio||t.devicePixelRatio,a=li(s),c=pt(r,Object.assign(di(n,["width","height","crop","name","focalPoint"]),{id:n.uri}),{width:e.targetWidth,height:e.targetHeight,htmlTag:i||"img",pixelAspectRatio:a,alignment:e.alignType||H.CENTER},o);return c.uri=ui(c.uri,t.staticMediaUrl,t.mediaRootUrl),c})(Object.assign(Object.assign({targetWidth:t.width,targetHeight:t.height},n),{displayMode:d.displayMode}),o,"img"),g=(null===(c=null==l?void 0:l.css)||void 0===c?void 0:c.img)||{},m=function(e,t,i){if(!e)return t;const n=Object.assign({},t);return"fill"===i?(n.position="absolute",n.top="0"):"fit"===i&&(n.height="100%"),"fixed"===e&&(n["will-change"]="transform"),n.objectPosition&&(n.objectPosition=t.objectPosition.replace(/(center|bottom)$/,"top")),n}(t.mediaHeightOverrideType,g,d.displayMode);hi(u,m);const f=null==l?void 0:l.uri;h.setAttribute("data-src",f||""),h.setAttribute("data-has-ssr-src",""),n.isLQIP&&n.lqipTransition&&!("transitioned"in h.dataset)&&(h.dataset.transitioned="",u.complete?u.onload=function(){u.dataset.loadDone=""}:u.onload=function(){u.complete?u.dataset.loadDone="":u.onload=function(){u.dataset.loadDone=""}}),s&&r.imageLoader.loadImage(h,{screenHeight:t.screenHeight,boundingRect:t.boundingRect})}};var bi=function(e,t){return e.imageLoader||(e.imageLoader=new fi(e.mutationService)),class extends HTMLElement{constructor(){super(),this.childListObserver=null,this.timeoutId=null}attributeChangedCallback(e,t){t&&this.reLayout()}connectedCallback(){this.observeResize(),this.reLayout()}disconnectedCallback(){this.unobserveResize(),e.imageLoader.onImageDisconnected(this),this.unobserveChildren()}static get observedAttributes(){return["data-image-info"]}reLayout(){const i={},n={},r=this.getAttribute("id"),o=JSON.parse(this.dataset.imageInfo||""),{bgEffectName:s}=this.dataset;i[r]=this,o.containerId&&(i[o.containerId]=document.getElementById(`${o.containerId}`)),i.image=this.querySelector("img");const a=o.containerId?i[o.containerId]:void 0;if(!i.image){const e=this;return void this.observeChildren(e)}this.unobserveChildren(),this.observeChildren(this),e.mutationService.measure((()=>{pi.measure(r,n,i,{containerElm:a,bgEffectName:s},e)}));const c=a=>{e.mutationService.mutate((()=>{pi.patch(r,n,i,o,e,t,a,s)}))};!gi(i.image)||this.dataset.hasSsrSrc?c(!0):this.debounceImageLoad(c)}debounceImageLoad(e){clearTimeout(this.timeoutId),this.timeoutId=setTimeout((()=>{e(!0)}),250),e(!1)}observeResize(){var t;null===(t=e.resizeService)||void 0===t||t.observe(this)}unobserveResize(){var t;null===(t=e.resizeService)||void 0===t||t.unobserve(this)}observeChildren(e){this.childListObserver||(this.childListObserver=new window.MutationObserver((()=>this.reLayout()))),this.childListObserver.observe(e,{childList:!0})}unobserveChildren(){this.childListObserver&&(this.childListObserver.disconnect(),this.childListObserver=null)}}};function Ii(e={}){if("undefined"==typeof window)return;const t={staticMediaUrl:"https://static.wixstatic.com/media",mediaRootUrl:"https://static.wixstatic.com",experiments:{},devicePixelRatio:/iemobile/i.test(navigator.userAgent)?Math.round(window.screen.availWidth/(window.screen.width||window.document.documentElement.clientWidth)):window.devicePixelRatio},i=function(){const e="wow-image";if(void 0===customElements.get(e)){let t;return window.ResizeObserver&&(t=new window.ResizeObserver((e=>{e.map((e=>e.target.reLayout()))}))),function(i,n){const r=bi(Object.assign({resizeService:t},i),n);customElements.define(e,r)}}}();i&&i(Object.assign({mutationService:a()},e),t)}const _i=()=>({getSiteScale:()=>{const e=document.querySelector("#site-root");return e?e.getBoundingClientRect().width/e.offsetWidth:1}}),wi=()=>{const e={init:e=>new ResizeObserver(e)},t={init:e=>window.addEventListener("resize",e)},i=_i();return ai.init({resizeService:e,windowResizeService:t,siteService:i})},vi=(e,t,i,r)=>{const s=n[e]||{},{getMediaDimensions:a}=s,c=(0,o._T)(s,["getMediaDimensions"]);return a?Object.assign(Object.assign({},a(t,i,r)),c):Object.assign({width:t,height:i},c)};var Ti=i(87931);const{experiments:Ei,media:Li,requestUrl:Oi}=window.viewerModel;((e,t,n)=>{const r=Promise.all([!("customElements"in window)&&i.e(6211).then(i.t.bind(i,45918,23)),!("IntersectionObserver"in window)&&i.e(7294).then(i.t.bind(i,47946,23)),!("ResizeObserver"in window)&&i.e(7971).then(i.bind(i,5653)).then((e=>window.ResizeObserver=e.default))]).then((()=>((e,t,i)=>{const n={staticMediaUrl:e.media.staticMediaUrl,mediaRootUrl:e.media.mediaRootUrl,experiments:{},isViewerMode:!0,devicePixelRatio:/iemobile/i.test(navigator.userAgent)?Math.round(window.screen.availWidth/(window.screen.width||window.document.documentElement.clientWidth)):window.devicePixelRatio},r={mutationService:a(),biService:t,isExperimentOpen:t=>Boolean(e.experiments[t]),siteService:_i()},o=Object.assign({getMediaDimensionsByEffect:vi},r);return Object.assign(Object.assign({},e),{wixCustomElements:i||wi(),services:r,environmentConsts:n,mediaServices:o})})(e,t,n))),o=new Promise((e=>{"complete"===document.readyState||"interactive"===document.readyState?e():document.addEventListener("readystatechange",(()=>e()),{once:!0})}));Promise.all([r,o]).then((([{services:e,environmentConsts:t,wixCustomElements:i,media:n,requestUrl:r,mediaServices:o}])=>{Ii({getMediaDimensionsByEffect:vi}),i.defineWixVideo(o,Object.assign(Object.assign({},t),{staticVideoUrl:n.staticVideoUrl,prefersReducedMotion:(0,c.n)(window,r,e.isExperimentOpen)})),i.defineWixDropdownMenu(e,t),i.defineWixIframe(e,t),i.defineWixImage(o,t),i.defineWixBgImage(o,t),i.defineWixBgMedia(o,t)})),window.__imageClientApi__=ai.imageClientApi,window.externalsRegistry.imageClientApi.onload()})({experiments:Ei,media:Li,requestUrl:Oi},Ti.e)},60306:function(e,t,i){"use strict";i.d(t,{w1:function(){return n},un:function(){return r},vU:function(){return o},G6:function(){return s},Wc:function(){return a},i7:function(){return c},kX:function(){return d},VE:function(){return u},gn:function(){return l},oK:function(){return g},ED:function(){return m},ZS:function(){return f}});const n=e=>!!e&&!!e.document&&!!e.document.documentMode,r=e=>h(e).indexOf("edg")>-1,o=e=>h(e).indexOf("firefox")>-1,s=e=>{const t=h(e);return t.indexOf("safari")>-1&&t.indexOf("version")>-1},a=e=>{if(s(e)){let t=h(e).split(" ");return t=t.find((e=>e.startsWith("version/"))),t=t.split("/")[1],parseInt(t,10)}return-1},c=e=>h(e).indexOf("chrome")>-1,d=e=>{const t=h(e);return t.indexOf("safari")>-1&&t.indexOf("crios")>-1},h=e=>e&&e.navigator&&e.navigator.userAgent?e.navigator.userAgent.toLowerCase():"",u=e=>{const t=h(e);return/ip(hone|od|ad).*os 11/.test(t)},l=e=>{const t=(e=>e&&e.navigator&&e.navigator.platform||"")(e);return!!t&&/iPad|iPhone|iPod/.test(t)},g=e=>{const t=h(e),i=/(iphone|ipod|ipad).*os (\d+)_/;if(!i.test(t))return NaN;const n=t.match(i);return n&&Number(n[2])},m=e=>{const t=h(e);return!!t&&/.*\(win.*\).*/i.test(t)},f=e=>{const t=e.navigator.userAgent.toLowerCase(),i=-1!==t.indexOf("ipad"),n=-1!==t.indexOf("mac");return!!(!i&&n&&e.navigator.maxTouchPoints&&e.navigator.maxTouchPoints>2)||i}},59277:function(e,t,i){"use strict";i.d(t,{n:function(){return r}});var n=i(60306);const r=(e,t="",i)=>{const r=(0,n.ED)(e)&&!i("specs.thunderbolt.allow_windows_reduced_motion");return t.toLowerCase().includes("forcereducedmotion")||!(!e||r)&&e.matchMedia("(prefers-reduced-motion: reduce)").matches}},80459:function(e,t,i){"use strict";var n,r;i.d(t,{sT:function(){return n},$7:function(){return r},i7:function(){return o}}),function(e){e[e.START=1]="START",e[e.VISIBLE=2]="VISIBLE",e[e.PAGE_FINISH=33]="PAGE_FINISH",e[e.FIRST_CDN_RESPONSE=4]="FIRST_CDN_RESPONSE",e[e.TBD=-1]="TBD",e[e.PAGE_NAVIGATION=101]="PAGE_NAVIGATION",e[e.PAGE_NAVIGATION_DONE=103]="PAGE_NAVIGATION_DONE"}(n||(n={})),function(e){e[e.NAVIGATION=1]="NAVIGATION",e[e.DYNAMIC_REDIRECT=2]="DYNAMIC_REDIRECT",e[e.INNER_ROUTE=3]="INNER_ROUTE",e[e.NAVIGATION_ERROR=4]="NAVIGATION_ERROR",e[e.CANCELED=5]="CANCELED"}(r||(r={}));const o={1:"page-navigation",2:"page-navigation-redirect",3:"page-navigation-inner-route",4:"navigation-error",5:"navigation-canceled"}}},function(e){"use strict";e.O(0,[4767,142,8050],(function(){return t=25536,e(e.s=t);var t}));e.O()}]);
        //# sourceMappingURL=https://static.parastorage.com/services/wix-thunderbolt/dist/initCustomElements.inline.6137fd3f.bundle.min.js.map</script>
    <script async="" id="wix-perf-measure" src="/index_files/wix-perf-measure.bundle.min.js"></script>



    <!-- head performance data start -->

    <!-- head performance data end -->


    <!-- react-dom -->
    <script crossorigin="" defer="" onload="externalsRegistry.reactDOM.onload()" src="/index_files/react-dom.production.min(1).js"></script>

    <meta http-equiv="X-Wix-Meta-Site-Id" content="a231884f-e942-45d8-998a-334660ffb56b">
    <meta http-equiv="X-Wix-Application-Instance-Id" content="efee9b1b-f085-466c-8316-095656a4ebef">

    <meta http-equiv="X-Wix-Published-Version" content="22">



    <meta http-equiv="etag" content="bug">

    <!-- render-head end -->

    <style data-href="https://static.parastorage.com/services/editor-elements/dist/rb_wixui.thunderbolt~bootstrap-classic.e2411168.chunk.min.css">
        ._1vNJf{position:relative;overflow:visible}._2RKTZ{-webkit-backface-visibility:hidden;backface-visibility:hidden}.b9KOL{cursor:pointer}._1-6YJ{width:100%;height:100%;display:block}._3SQN-,._3wnIc{position:absolute;top:0;width:100%;height:100%}._3wnIc{left:0;overflow:hidden;-webkit-mask-image:var(--mask-image,none);mask-image:var(--mask-image,none);-webkit-mask-position:var(--mask-position,0);mask-position:var(--mask-position,0);-webkit-mask-size:var(--mask-size,100%);mask-size:var(--mask-size,100%);-webkit-mask-repeat:var(--mask-repeat,no-repeat);mask-repeat:var(--mask-repeat,no-repeat);pointer-events:var(--fill-layer-background-media-pointer-events)}._3wnIc._7WhTV{clip:rect(0,auto,auto,0)}@media not all and (min-resolution:0.001dpcm){@supports (-webkit-appearance:none){._3wnIc._7WhTV{clip:auto;-webkit-clip-path:inset(0)}}}._2GUhU{height:100%}._3KzuS{background-color:var(--bg-overlay-color);background-image:var(--bg-gradient)}._2L00W{opacity:var(--fill-layer-image-opacity);height:var(--fill-layer-image-height,100%)}._2L00W img{width:100%;height:100%}._1PtAB,._6ICOI{opacity:var(--fill-layer-video-opacity)}._2gFri{position:absolute;width:100%;height:var(--media-padding-height);top:var(--media-padding-top);bottom:var(--media-padding-bottom)}._1Q5R5{transform:scale(var(--scale,1));transition:var(--transform-duration,transform 0s)}._6ICOI{position:relative;width:100%;height:100%}._3hRfg{-webkit-clip-path:var(--fill-layer-clip);clip-path:var(--fill-layer-clip)}._2IRVt,._3hRfg{position:absolute;top:0}._1QuqS img,._2IRVt,._3hRfg{width:100%;height:100%}._3vVMz{opacity:0}._3vVMz,._3192S{position:absolute;top:0}._3192S{width:0;height:0;left:0;overflow:hidden}._3BaUs{position:var(--fill-layer-background-media-position);pointer-events:var(--fill-layer-background-media-pointer-events);left:0}._1eCSh,._3BaUs,._37Ao-{width:100%;height:100%;top:0}._37Ao-{position:absolute}._1eCSh{background-color:var(--fill-layer-background-overlay-color);position:var(--fill-layer-background-overlay-position);opacity:var(--fill-layer-background-overlay-blend-opacity-fallback,1);transform:var(--fill-layer-background-overlay-transform)}@supports (mix-blend-mode:overlay){._1eCSh{mix-blend-mode:var(--fill-layer-background-overlay-blend-mode);opacity:var(--fill-layer-background-overlay-blend-opacity,1)}}._2o25M{position:absolute;top:0;right:0;bottom:0;width:100%;left:0}.FqFZT{transition:.2s ease-in;transform:translateY(-100%)}._1CvyK{transition:.2s}._12-8f{transition:.2s ease-in;opacity:0}._12-8f._1fxAO{z-index:-1!important}._2nqfT{transition:.2s;opacity:1}._1_UPn{height:auto}._1_UPn,._38XI2{position:relative;width:100%}body:not(.device-mobile-optimized) ._2P6JN{margin-left:calc((100% - var(--site-width)) / 2);width:var(--site-width)}.ZW5SX[data-focuscycled=active]{outline:1px solid transparent}.ZW5SX[data-focuscycled=active]:not(:focus-within){outline:2px solid transparent;transition:outline .01s ease}.ZW5SX ._1nRb9{top:0;bottom:0;box-shadow:var(--shd,0 0 5px rgba(0,0,0,.7));background-color:var(--screenwidth-corvid-background-color,rgba(var(--bg,var(--color_11)),var(--alpha-bg,1)));border-top:var(--brwt,0) solid var(--screenwidth-corvid-border-color,rgba(var(--brd,var(--color_15)),var(--alpha-brd,1)));border-bottom:var(--brwb,0) solid var(--screenwidth-corvid-border-color,rgba(var(--brd,var(--color_15)),var(--alpha-brd,1)))}.ZW5SX ._1dlfY,.ZW5SX ._1nRb9{position:absolute;right:0;left:0}.ZW5SX ._1dlfY{top:var(--brwt,0);bottom:var(--brwb,0);border-radius:var(--rd,0);background-color:rgba(var(--bgctr,var(--color_11)),var(--alpha-bgctr,1))}.ZW5SX ._2P6JN{position:absolute;top:0;right:0;bottom:0;left:0}body.device-mobile-optimized .ZW5SX ._2P6JN{left:10px;right:10px}._1KV2M{pointer-events:none}._2v3yk{position:var(--bg-position);top:var(--wix-ads-height);min-height:calc(100vh - var(--wix-ads-height));height:100%;width:100%;min-width:var(--site-width)}._1fbEI{text-align:initial;display:flex;align-items:center;box-sizing:border-box;width:-webkit-max-content;width:-moz-max-content;width:max-content;justify-content:var(--label-align);min-width:100%}._1fbEI:before{max-width:var(--margin-left,0)}._1fbEI:after,._1fbEI:before{content:"";flex-grow:1;align-self:stretch}._1fbEI:after{max-width:var(--margin-right,0)}._2UgQw{height:100%}._2UgQw ._1fbEI{border-radius:var(--corvid-border-radius,var(--rd,0));position:absolute;top:0;right:0;bottom:0;left:0;transition:var(--trans1,border-color .4s ease 0s,background-color .4s ease 0s);box-shadow:var(--shd,0 1px 4px rgba(0,0,0,.6))}._2UgQw ._1fbEI:link,._2UgQw ._1fbEI:visited{border-color:transparent}._2UgQw ._1Qjd7{font:var(--fnt,var(--font_5));transition:var(--trans2,color .4s ease 0s);color:var(--corvid-color,rgb(var(--txt,var(--color_15))));position:relative;white-space:nowrap;margin:0}._2UgQw[aria-disabled=false] ._1fbEI{background-color:var(--corvid-background-color,rgba(var(--bg,var(--color_17)),var(--alpha-bg,1)));border:solid var(--corvid-border-color,rgba(var(--brd,var(--color_15)),var(--alpha-brd,1))) var(--corvid-border-width,var(--brw,0));cursor:pointer!important}body.device-mobile-optimized ._2UgQw[aria-disabled=false]:active ._1fbEI{background-color:rgba(var(--bgh,var(--color_18)),var(--alpha-bgh,1));border-color:rgba(var(--brdh,var(--color_15)),var(--alpha-brdh,1))}body.device-mobile-optimized ._2UgQw[aria-disabled=false]:active ._1Qjd7{color:rgb(var(--txth,var(--color_15)))}body:not(.device-mobile-optimized) ._2UgQw[aria-disabled=false]:hover ._1fbEI{background-color:rgba(var(--bgh,var(--color_18)),var(--alpha-bgh,1));border-color:rgba(var(--brdh,var(--color_15)),var(--alpha-brdh,1))}body:not(.device-mobile-optimized) ._2UgQw[aria-disabled=false]:hover ._1Qjd7{color:rgb(var(--txth,var(--color_15)))}._2UgQw[aria-disabled=true] ._1fbEI{background-color:rgba(var(--bgd,204,204,204),var(--alpha-bgd,1));border-color:rgba(var(--brdd,204,204,204),var(--alpha-brdd,1));border-width:var(--corvid-border-width,var(--brw,0));border-style:solid}._2UgQw[aria-disabled=true] ._1Qjd7{color:rgb(var(--txtd,255,255,255))}._6lnTT{text-align:initial;display:flex;align-items:center;box-sizing:border-box;width:-webkit-max-content;width:-moz-max-content;width:max-content;justify-content:var(--label-align);min-width:100%}._6lnTT:before{max-width:var(--margin-left,0)}._6lnTT:after,._6lnTT:before{content:"";flex-grow:1;align-self:stretch}._6lnTT:after{max-width:var(--margin-right,0)}._2kLdM[aria-disabled=false] ._6lnTT{cursor:pointer}body.device-mobile-optimized ._2kLdM[aria-disabled=false]:active .wQYUw,body:not(.device-mobile-optimized) ._2kLdM[aria-disabled=false]:hover .wQYUw{color:rgb(var(--txth,var(--color_15)));transition:var(--trans,color .4s ease 0s)}._2kLdM ._6lnTT{position:absolute;top:0;right:0;bottom:0;left:0}._2kLdM .wQYUw{font:var(--fnt,var(--font_5));transition:var(--trans,color .4s ease 0s);color:var(--corvid-color,rgb(var(--txt,var(--color_15))));white-space:nowrap}._2kLdM[aria-disabled=true] .wQYUw{color:rgb(var(--txtd,255,255,255))}body:not(.device-mobile-optimized) ._3d64y{display:var(--display);--display:flex}body:not(.device-mobile-optimized) ._1uldx{display:flex;position:relative;width:calc(100% - var(--padding) * 2);margin:0 auto}body:not(.device-mobile-optimized) ._1uldx>*{flex:var(--column-flex) 1 0%;min-width:0;margin-top:var(--padding);margin-bottom:var(--padding);position:relative;left:0;top:0;margin-left:var(--margin)}body:not(.device-mobile-optimized) ._1uldx>:first-child{margin-left:0}body.device-mobile-optimized ._1uldx{display:block;position:relative;padding:var(--padding) 0}body.device-mobile-optimized ._1uldx>*{margin-bottom:var(--margin);position:relative}body.device-mobile-optimized ._1uldx>:first-child{margin-top:var(--firstChildMarginTop,0)}body.device-mobile-optimized ._1uldx>:last-child{margin-bottom:var(--lastChildMarginBottom,var(--margin))}._2LYf8{-webkit-backface-visibility:hidden;backface-visibility:hidden}._3WMIc{--divider-pin-height__:min(1,calc(var(--divider-layers-pin-factor__) + 1));--divider-pin-layer-height__:var(--divider-layers-pin-factor__);--divider-pin-border__:min(1,calc(var(--divider-layers-pin-factor__) / -1 + 1));height:calc(var(--divider-height__) + var(--divider-pin-height__) * var(--divider-layers-size__) * var(--divider-layers-y__))}._3WMIc,._3WMIc ._2NMWe{position:absolute;left:0;width:100%}._3WMIc ._2NMWe{--divider-layer-i__:var(--divider-layer-i,0);height:calc(var(--divider-height__) + var(--divider-pin-layer-height__) * var(--divider-layer-i__) * var(--divider-layers-y__));opacity:calc(1 - var(--divider-layer-i__) / (var(--divider-layer-i__) + 1));background-repeat:repeat-x;background-position:left calc(50% + var(--divider-offset-x__) + var(--divider-layers-x__) * var(--divider-layer-i__)) bottom;border-bottom-width:calc(var(--divider-pin-border__) * var(--divider-layer-i__) * var(--divider-layers-y__));border-bottom-style:solid}._3fTM1{--divider-height__:var(--divider-top-height,auto);--divider-offset-x__:var(--divider-top-offset-x,0px);--divider-layers-size__:var(--divider-top-layers-size,0);--divider-layers-y__:var(--divider-top-layers-y,0px);--divider-layers-x__:var(--divider-top-layers-x,0px);--divider-layers-pin-factor__:var(--divider-top-layers-pin-factor,0);top:0;transform:var(--divider-top-flip,scaleY(-1));opacity:var(--divider-top-opacity,1);border-top:var(--divider-top-padding,0) solid var(--divider-top-color,currentColor)}._3fTM1 ._2NMWe{bottom:0;border-color:var(--divider-top-color,currentColor);background-image:var(--divider-top-image,none);background-size:var(--divider-top-size,contain)}._2bMgb{--divider-height__:var(--divider-bottom-height,auto);--divider-offset-x__:var(--divider-bottom-offset-x,0px);--divider-layers-size__:var(--divider-bottom-layers-size,0);--divider-layers-y__:var(--divider-bottom-layers-y,0px);--divider-layers-x__:var(--divider-bottom-layers-x,0px);--divider-layers-pin-factor__:var(--divider-bottom-layers-pin-factor,0);bottom:0;transform:var(--divider-bottom-flip,none);opacity:var(--divider-bottom-opacity,1);border-bottom:var(--divider-bottom-padding,0) solid var(--divider-bottom-color,currentColor)}._2bMgb ._2NMWe{bottom:0;border-color:var(--divider-bottom-color,currentColor);background-image:var(--divider-bottom-image,none);background-size:var(--divider-bottom-size,contain)}._3vrwT{width:100%;height:100%;overflow:hidden}._3vrwT._1rAnL:hover{cursor:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAAH6ji2bAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA3FpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpmMWUzNTlkMS1hYjZhLTNkNDctYmM0ZC03MWMyZDYyMWNmNDgiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6ODM3MEUzMUU4OTAyMTFFMzk3Q0FCMkFEODdDNzUzMjQiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6ODM3MEUzMUQ4OTAyMTFFMzk3Q0FCMkFEODdDNzUzMjQiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChXaW5kb3dzKSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjk0ZTkyMTRlLThiNDQtNjc0My04MWZiLTZlYjIzYTA2ZjcwNCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpmMWUzNTlkMS1hYjZhLTNkNDctYmM0ZC03MWMyZDYyMWNmNDgiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4bqsJgAAACF0lEQVR42mJgQAd8fHz/gdRvRigfxGEACCA4YvwPBMgCbgABBGOAJP6LiooiZBUUFMCC7969Awk6AQQQA1bAxMTUOnXq1P8/f/78j2zdf5BDQDgoKAgiyMgItv0/1AkozlgJlHwPpDWB+AhAACFL1EJVwvBPIGZHd8P/OXPmgI0F2YdmxXQUhX///sVQqK2tDVL4DFkhF8zK2NjY/4aGhshOOMJAJAB5ZjdAADGQCpiB4Cear3uwKQR74vv372BPLFq0CKZ4GnLcdMGiFtnXmzZtQo0Bdnb2r/b29nBFMIwUjkxghby8vHfFxMQwTMQWp0YggZcvX/5HBpqamhgKQdafAQnq6en9j4+P/4/me150nzsCPfYOKrkWKvYCymcjJozPgqIYIMAYcUjKAnEcELsDbVECOpkNiO8B+buAeCEQ3yUqFllYWNYh+4Obm/u/ubn5f0tLy//QPIqM90ATHVagDHTJH5BCfn7+/xcvXvyPC9y7d+8/KHqghv4FYj0M04BxeAOkQEhI6P+vX79QDECOeBj49+/ffzk5OZih91FyP4gAGiIDooH5hIGVlRUsAXQpGMMAMh+Y1xksLCzg5QxGrAFzwAxY2GzYsIGgC48cOYIclsuwBiIbG9sCmCJFRcX/+/fvxwi/EydOwIoDGH6JLQEiA26ga1egxSY2vAUpkcKKEV5iCwVOIObBU8w8RzLYgYHaAAACg5CxaxSLgwAAAABJRU5ErkJggg==),auto}._3vrwT._1E5xK:hover{cursor:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAAH6ji2bAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA3FpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpmMWUzNTlkMS1hYjZhLTNkNDctYmM0ZC03MWMyZDYyMWNmNDgiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6N0I4QkNGQTI4OTAyMTFFMzg0RDlBRkM5NDA5QjczRTEiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6N0I4QkNGQTE4OTAyMTFFMzg0RDlBRkM5NDA5QjczRTEiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChXaW5kb3dzKSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjk0ZTkyMTRlLThiNDQtNjc0My04MWZiLTZlYjIzYTA2ZjcwNCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpmMWUzNTlkMS1hYjZhLTNkNDctYmM0ZC03MWMyZDYyMWNmNDgiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7hiSPZAAACGklEQVR42mJgQAd8fHz/gdRvRigfxGEACCA4YvwPBMgCbgABBGOAJP6LiooiZBUUFMCC7969Awk6AQQQA1bAxMTUOnXq1P8/f/78j2zdf5BDQDgoKAgiyMgItv0/1AkozlgJlHwPpDWB+AhAACFL1EJVwvBPIGZHd8P/OXPm/EcHUA3TURT+/fsXQ6G2tjZI4TNkhVwwK2NjY/8bGhoiO+EIA5EA5JndAAHEQCpgBoKfaL7uwaYQHLrfv38He2LRokUwxdOQ46YLFrXIYNOmTagxwM7O/tXe3h4sCYs3EEYKRyawQl5e3rtiYmL/sQH0ODUCCbx8+RJFkaamJoZCkPVnQIJ6enr/4+Pj/6P5nhfd545Aj72DSq6Fir2A8tmICeOzoCgGCDBGHJKyQBwHxO5AW5SATmYD4ntA/i4gXgjEd4mKRRYWlnXI/uDm5v5vbm7+39LS8j80jyLjPdBEhxUoA13yB6SQn5///8WLF//jAvfu3fsPih6ooX+BWA/DNGAc3gApEBIS+v/r16//hMC/f//+y8nJwQy9j2wWC4gAGiIDooH5hIGVlRUsAXQpVq98/PgRVBAwWFhYMDx69AhczkBj7RdyFpgBC5sNGzYQdOGRI0eQw3IZVpvZ2NgWwBQpKir+379/P4ZBJ06cgBUHMPwSWwJEBtxA165Ai01seAtSIoUVI7zEFgqcQMyDp5h5jmSwAwO1AQBU5q033XYWQwAAAABJRU5ErkJggg==),auto}._2S73V{cursor:pointer}._2Ea4I{width:0;height:0;left:0;top:0;overflow:hidden;position:absolute}.xQ_iF{width:100%;height:100%;box-sizing:border-box}._1Fe8-{min-height:var(--image-min-height);min-width:var(--image-min-width)}._1Fe8- img{-o-object-position:var(--object-position);object-position:var(--object-position);filter:var(--filter-effect-svg-url);-webkit-mask-image:var(--mask-image,none);mask-image:var(--mask-image,none);-webkit-mask-position:var(--mask-position,0);mask-position:var(--mask-position,0);-webkit-mask-size:var(--mask-size,100% 100%);mask-size:var(--mask-size,100% 100%);-webkit-mask-repeat:var(--mask-repeat,no-repeat);mask-repeat:var(--mask-repeat,no-repeat)}.XUUsC{position:var(--position-fixed,static);left:var(--left,auto);top:var(--top,auto);z-index:var(--z-index,auto)}.XUUsC ._1Fe8- img{position:static;box-shadow:0 0 0 #000;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.XUUsC .xQ_iF{display:block}.XUUsC ._1Fe8-,.XUUsC .xQ_iF{overflow:hidden}._3jgRX{position:absolute;top:0;right:0;bottom:0;left:0}._2ufc7{height:auto;width:100%;position:relative}body:not(.responsive) ._3LL-w{height:100%;position:relative;left:0;grid-area:1/1/1/1;align-self:start;justify-self:stretch}body:not(.device-mobile-optimized) ._3jgRX{margin-left:calc((100% - var(--site-width)) / 2);width:var(--site-width)}._1FOTJ ._3jgRX{overflow:hidden;background-color:rgba(var(--bg,var(--color_11)),var(--alpha-bg,1))}body.device-mobile-optimized ._3jgRX{left:10px;right:10px}._3CemL{position:absolute;top:0;right:0;bottom:0;left:0}._3K7uv{height:auto;width:100%;position:relative}body:not(.responsive) .Ry26q{height:100%;position:relative;left:0;grid-area:1/1/1/1;align-self:start;justify-self:stretch}body:not(.device-mobile-optimized) ._3CemL{margin-left:calc((100% - var(--site-width)) / 2);width:var(--site-width)}body.device-mobile-optimized ._3CemL{left:10px;right:10px}
    </style>
    <style data-href="https://static.parastorage.com/services/editor-elements/dist/rb_wixui.thunderbolt~common-site-members-dialogs.384b0b57.chunk.min.css">
        @-webkit-keyframes Njak4{to{opacity:1;transform:rotate(115deg)}}@keyframes Njak4{to{opacity:1;transform:rotate(115deg)}}@-webkit-keyframes _-93Hx{0%{-webkit-animation-timing-function:ease-out;animation-timing-function:ease-out;transform:rotate(180deg)}45%{transform:rotate(198deg)}55%{transform:rotate(234deg)}to{transform:rotate(540deg)}}@keyframes _-93Hx{0%{-webkit-animation-timing-function:ease-out;animation-timing-function:ease-out;transform:rotate(180deg)}45%{transform:rotate(198deg)}55%{transform:rotate(234deg)}to{transform:rotate(540deg)}}._3XWHL.mLATz{-webkit-animation:_-93Hx 1s linear 1ms infinite;animation:_-93Hx 1s linear 1ms infinite;height:30px;left:50%;margin-left:-15px;margin-top:-15px;overflow:hidden;position:absolute;top:var(--preloaderTop,50%);transform-origin:100% 50%;width:15px}._3XWHL.mLATz:before{color:#7fccf7}._3XWHL.mLATz:after,._3XWHL.mLATz:before{content:"";top:0;left:0;right:-100%;bottom:0;border:3px solid currentColor;border-color:currentColor transparent transparent currentColor;border-radius:50%;position:absolute;transform:rotate(-45deg);-webkit-animation:Njak4 .5s linear 1ms infinite alternate;animation:Njak4 .5s linear 1ms infinite alternate}._3XWHL.mLATz:after{color:#3899ec;opacity:0}._3XWHL.mLATz.swe5O:before{color:#f0f0f0}._3XWHL.mLATz.swe5O:after{color:#dcdcdc}._3XWHL._2sBQu:after,._3XWHL._2sBQu:before{color:rgba(var(--color_15),var(--alpha-color_15,1))}._1m0P5{position:fixed!important;width:100%!important;height:100%!important;top:0;right:0;bottom:0;left:0;display:flex;align-items:center;justify-content:center;z-index:calc(var(--above-all-z-index) + 1);color:rgb(var(--txt,156,156,156))}._1m0P5 ._3QxVv{position:fixed;touch-action:manipulation;top:60px;right:60px;background:none;z-index:3}._1m0P5 ._3QxVv:hover{cursor:pointer}._1m0P5 ._3QxVv svg{width:25px;height:25px;stroke:param-color(clr,color_15);stroke-width:15;cursor:pointer;pointer-events:none;fill:rgba(var(--color_15),var(--alpha-color_15,1))}._1m0P5[data-layout=popup] ._3QxVv{position:absolute;top:24px;right:24px}._1m0P5 .CiRbb{background-color:rgba(var(--bg-clr,var(--color_11)),var(--alpha-bg-clr,1));position:fixed;top:0;right:0;bottom:0;left:0;visibility:visible;zoom:1;overflow:auto}._1m0P5[data-layout=popup] .CiRbb{background-color:#000;opacity:.6}._1m0P5 .krFaH{position:fixed;display:flex;flex-direction:column;align-items:center;overflow:auto}body:not(.device-mobile-optimized) ._1m0P5 .krFaH[data-dialogposition=center]>*{margin:auto}._1m0P5[data-layout=fullscreen] .krFaH{width:100%;height:100%}._1m0P5[data-layout=popup] .krFaH{background-color:rgba(var(--bg-clr,var(--color_11)),var(--alpha-bg-clr,1));box-sizing:border-box;max-height:100%}body.device-mobile-optimized ._1m0P5[data-layout=popup] .krFaH{height:100%;width:100%}body:not(.device-mobile-optimized) ._1m0P5[data-layout=popup] .krFaH{padding:70px 80px 80px;width:480px}._1m0P5[data-layout=customPopup] .CiRbb{background-color:#000;opacity:.6}._1m0P5[data-layout=customPopup] ._3QxVv{position:absolute;top:24px;right:24px}._1m0P5[data-layout=customPopup] .krFaH{background-color:rgba(var(--bg-clr,var(--color_11)),var(--alpha-bg-clr,1));box-sizing:border-box;max-height:100%}body.device-mobile-optimized ._1m0P5[data-layout=customPopup] .krFaH{height:100%;width:100%;padding:80px 32px}body:not(.device-mobile-optimized) ._1m0P5[data-layout=customPopup] .krFaH{padding:80px 32px;width:572px;height:600px}.C1xAY,.c2aD3{width:100%;height:48px}.C1xAY button,.c2aD3 button{box-sizing:border-box;--fnt:getVar(font_8);font-size:16px;--rd:0px;--trans1:border-color 0.4s ease 0s,background-color 0.4s ease 0s;--shd:none;--trans2:color 0.4s ease 0s;--bgd:204,204,204;--brdd:204,204,204;--txtd:255,255,255;--boxShadowToggleOn-shd:none;--alpha-txt:1;--alpha-brd:1;--alpha-txth:1;--alpha-bgh:1;--label-align:center;--label-text-align:center}.C1xAY{--brw:0px;--txt:var(--color_8);--bg:var(--color_18);--brd:var(--color_14);--bgh:var(--color_18);--brdh:var(--color_12);--txth:var(--color_11);--alpha-brdh:0;--alpha-bg:1;--shc-mutated-brightness:28,37,106;--alpha-bgh:0.7}.c2aD3 button{--brw:1px;--txt:var(--color_15);--bg:(--color_11);--brd:var(--color_13);--bgh:var(--color_11);--brdh:var(--color_15);--txth:var(--color_15);--alpha-brdh:1;--alpha-bg:0;--shc-mutated-brightness:128,128,128}body:not(.device-mobile-optimized) .C1xAY,body:not(.device-mobile-optimized) .c2aD3{font-weight:300}body.device-mobile-optimized ._1m0P5 ._3QxVv{top:30px;right:30px;width:20px;height:20px}body.device-mobile-optimized ._1m0P5 ._3QxVv svg{width:16px;height:16px;stroke-width:12px}body.device-mobile-optimized ._1m0P5 .c2aD3 button{font-size:14px}._2hrvQ{text-align:center}._2hrvQ._2fAw_{direction:rtl}body:not(.device-mobile-optimized) ._2hrvQ{position:relative;top:calc(50% - 230px)}body.device-mobile-optimized ._2hrvQ{margin-top:40px;width:280px}._2hrvQ._3i9MM{opacity:.6}._2hrvQ .jysNN{color:rgba(var(--color_15),var(--alpha-color_15,1));font:var(--ttlFnt,var(--font_2));font-size:48px;line-height:48px;margin-bottom:24px;margin-top:10px;padding-top:0;padding-bottom:0;text-align:center}body.device-mobile-optimized ._2hrvQ .jysNN{font-size:36px;margin-bottom:8px}body:not(.device-mobile-optimized) ._2hrvQ ._28E4_{width:auto;max-width:800px;display:inline-block}body:not(.device-mobile-optimized) ._2hrvQ ._28E4_._4YPVO .kTI1Z{width:320px}body:not(.device-mobile-optimized) ._2hrvQ ._28E4_._4YPVO ._1VhE7{border-right:1px solid param-color(secondary-color,color_13);margin:-20px 66px}._2hrvQ ._28E4_._4YPVO .IBW7d{position:relative;width:320px;margin:auto}body.device-mobile-optimized ._2hrvQ ._28E4_._4YPVO .IBW7d{margin-top:10px;width:100%}._2hrvQ ._28E4_._4YPVO .N4wCO{font-size:14px;line-height:14px;text-decoration:underline}._2hrvQ ._28E4_._4YPVO .N4wCO .hPy9b{float:none}._2hrvQ ._28E4_.JuWp2 .IBW7d{display:block}._2hrvQ ._28E4_.JuWp2 ._1leTB{display:block!important;margin:34px auto}body:not(.device-mobile-optimized) ._2hrvQ ._28E4_ .kTI1Z{width:320px;margin:auto}._2hrvQ ._28E4_ .kTI1Z ._3Awhk,._2hrvQ ._28E4_ .kTI1Z .rWhCT{--textAlign:left;--shd:0.00px 2.00px 0px 0px rgba(var(--color_15),1);--rd:0px;--fnt:normal normal normal 15px/1.4em futura-lt-w01-book,sans-serif;--brw:0px;--bg:245,255,254;--txt:82,82,82;--brd:82,82,82;--txt2:var(--color_15);--brwh:0px;--bgh:232,255,253;--brdh:0,0,0;--brwf:0px;--bgf:245,255,254;--brdf:0,0,0;--brwe:1px;--bge:255,64,64;--brde:255,64,64;--trns:opacity 0.5s ease 0s,border 0.5s ease 0s,color 0.5s ease 0s;--bgd:204,204,204;--txtd:232,232,232;--brwd:1px;--brdd:219,219,219;--fntlbl:normal normal normal 15px/1.4em futura-lt-w01-book,sans-serif;--txtlbl:var(--color_15);--txtlblrq:null;--alpha-brdf:0;--alpha-brdh:0;--alpha-brd:1;--alpha-bgd:0;--alpha-bge:0;--alpha-bgf:0;--alpha-bg:0;--alpha-bgh:0;height:auto;--dir:ltr;--labelMarginBottom:14px;--inputHeight:37px;--textPadding:3px 3px 3px 0px;--labelPadding:0 20px 0 0px;--requiredIndicationDisplay:none}body:not(.device-mobile-optimized) ._2hrvQ ._28E4_ .IBW7d{width:320px;margin:auto}._2hrvQ ._28E4_ .IBW7d ._3bn6H{width:100%;opacity:0;transition:opacity 1s ease-in}._2hrvQ ._28E4_ .IBW7d ._3bn6H.veWtg{opacity:1}._2hrvQ ._28E4_ .IBW7d ._9t6ai{opacity:0;transition:opacity 1s ease-in}._2hrvQ ._28E4_ .IBW7d ._9t6ai.veWtg{opacity:1}._2hrvQ ._1nZnX{text-align:center;padding-top:0;margin-bottom:32px}._2hrvQ ._1nZnX ._2DZur{color:rgba(var(--color_15),var(--alpha-color_15,1));margin-right:4px;margin-left:4px}body:not(.device-mobile-optimized) ._2hrvQ ._1nZnX ._2DZur{font:var(--fnt,var(--font_8));font-size:18px}body.device-mobile-optimized ._2hrvQ ._1nZnX ._2DZur{font:var(--fnt,var(--font_8));font-size:15px}._2hrvQ ._1nZnX ._3WTly{color:rgb(var(--txt,var(--color_18)));border-bottom:1px solid #0198ff;float:none;border:none;text-align:left;font-weight:400;text-decoration:none;cursor:pointer}body.device-mobile-optimized ._2hrvQ ._1nZnX ._3WTly{font:var(--fnt,var(--font_8));font-size:15px}body:not(.device-mobile-optimized) ._2hrvQ ._1nZnX ._3WTly{font:var(--fnt,var(--font_8));font-size:18px}._2hrvQ ._1nZnX ._3WTly:hover{border-bottom-color:#04f}._2hrvQ ._2BQVP{width:320px;text-align:center;margin:12px auto 24px;height:11px;border-bottom:1px solid;border-color:rgba(var(--color_13),var(--alpha-color_13,1))}body.device-mobile-optimized ._2hrvQ ._2BQVP{width:100%}._2hrvQ ._2BQVP span{font:var(--fnt,var(--font_8));font-size:16px;color:rgba(var(--color_15),var(--alpha-color_15,1));background-color:rgba(var(--bg,var(--color_11)),var(--alpha-bg,1));padding:0 13px}._2hrvQ ._3Awhk,._2hrvQ .rWhCT{position:relative;margin-bottom:25px}._2hrvQ .C1xAY,._2hrvQ .c2aD3{position:relative;font:var(--font_8)}._2hrvQ .c2aD3{height:40px}._2hrvQ .TMDGQ{font:var(--fnt,var(--font_8));color:rgba(var(--color_15),var(--alpha-color_15,1));font-size:16px;line-height:17px;text-decoration:underline;cursor:pointer;opacity:1;transition:opacity .2s ease-in-out;border:none;text-align:left}._2hrvQ .TMDGQ:hover{opacity:.8;transition:opacity .2s ease-in-out}._2hrvQ .TMDGQ._1CB5j{font-size:14px;line-height:14px;padding-top:24px}body.device-mobile-optimized ._2hrvQ .TMDGQ._2ENj1,body:not(.device-mobile-optimized) ._2hrvQ .TMDGQ._1CB5j{display:none}._2hrvQ ._3HzhV{display:flex;justify-content:space-between;padding-top:13px;padding-bottom:18px}._3Z7oj{--preloaderTop:calc(50% - 1270px)}._3bn6H,._9t6ai{width:100%}._9t6ai{height:32px}._1mwa4{display:none}._3Hr7N{margin-bottom:20px}._3zQAy{margin-bottom:8px;color:#df3131;font-size:12px}._1e_cj{position:fixed;bottom:0;left:0;background-color:#212121;padding:16px 20px;box-shadow:inset 0 4px 8px 0 #000,0 0 4px 0 #000;width:100%;font-size:16px;text-align:initial;color:#fff;visibility:hidden;opacity:0;transition:visibility .1s,opacity .1s linear;box-sizing:border-box}.VFEck{visibility:visible;opacity:1}._2D5_F{margin:48px 0}._15OdW{width:280px;margin:0 auto}._2CG36{font:var(--fnt,var(--font_8));text-align:center}body:not(.device-mobile-optimized) ._2CG36{width:320px}._2CG36 .vXtsk{direction:ltr;margin-bottom:13px;height:18px}._2CG36 .vXtsk ._2VsLL{margin-right:12px;width:18px;height:18px}._2CG36 .vXtsk ._2VsLL:last-child{margin-right:0}._2CG36 ._21YUs{font:var(--font_8);font-size:15px;font-weight:300}._2CG36 ._13hoR,._2CG36 ._21YUs{color:rgba(var(--color_15),var(--alpha-color_15,1));line-height:1.6}._2CG36 ._13hoR{text-decoration:underline;font:var(--font_8);font-size:15px;cursor:pointer;opacity:1;transition:opacity .2s ease-in-out}._2CG36 ._13hoR:hover{opacity:.8;transition:opacity .2s ease-in-out}._3JKX0{--shd:none;--rd:0px;--fnt:var(--font_8);--brw:0px 0px 1px 0px;--bg:245,255,254;--txt:var(--color_15);--brd:var(--color_13);--txt2:var(--color_13);--brwh:0px 0px 1px 0px;--bgh:232,255,253;--brdh:var(--color_15);--brwf:0px 0px 1px 0px;--bgf:245,255,254;--brdf:var(--color_18);--brwe:1px;--bge:255,61,61;--brde:255,61,61;--trns:opacity 0.5s ease 0s,border 0.5s ease 0s,color 0.5s ease 0s;--bgd:204,204,204;--txtd:232,232,232;--brwd:1px;--brdd:219,219,219;--fntlbl:var(--font_8);--txtlbl:var(--color_14);--txtlblrq:null;--boxShadowToggleOn-shd:none;--alpha-brdf:1;--alpha-brdh:1;--alpha-brd:1;--alpha-bgd:0;--alpha-bge:0;--alpha-bgf:0;--alpha-bg:0;--alpha-bgh:0;height:53px;--dir:ltr;--textAlign:left;--textPadding:2px;--labelMarginBottom:0;--inputHeight:26px;--labelPadding:0;--requiredIndicationDisplay:none}._3JKX0.FmyJo{--dir:rtl;--textAlign:right}._3JKX0.l2ouQ input{border-top-width:0!important;border-right-width:0!important;border-left-width:0!important}._3JKX0 span{font:var(--font_8);display:block;padding-top:4px;color:#ff3d3d;text-align:left;font-size:12px}._3JKX0 span.FmyJo{text-align:right;direction:rtl}._3JKX0 input[type=email]{font-size:16px}._3JKX0 input[type=password],body.device-mobile-optimized ._3JKX0 input[type=email]{font-size:14px}._3JKX0 input::-moz-placeholder{font-size:16px}._3JKX0 input:-ms-input-placeholder{font-size:16px}._3JKX0 input::placeholder{font-size:16px}._3JKX0 label{font-size:16px}._1SOvY._2fZKw[type=number]::-webkit-inner-spin-button{-webkit-appearance:none;-moz-appearance:none;margin:0}._2dBhC{position:relative;min-height:25px;display:var(--display);--display:flex;flex-direction:column}._2dBhC .XRJUI{height:var(--inputHeight);position:relative}._2dBhC ._1yU6F{font:var(--fntprefix,normal normal normal 16px/1.4em helvetica-w01-roman);position:absolute;left:0;top:0;width:50px;min-height:100%;max-height:100%;display:flex;justify-content:center;align-items:center}._2dBhC ._1SOvY,._2dBhC ._1yU6F{color:var(--corvid-color,rgb(var(--txt,var(--color_15))))}._2dBhC ._1SOvY{box-shadow:var(--shd,0 0 0 transparent);font:var(--fnt,var(--font_8));-webkit-appearance:none;-moz-appearance:none;border-radius:var(--corvid-border-radius,var(--rd,0));background-color:var(--corvid-background-color,rgba(var(--bg,255,255,255),var(--alpha-bg,1)));border-color:var(--corvid-border-color,rgba(var(--brd,227,227,227),var(--alpha-brd,1)));border-width:var(--corvid-border-width,var(--brw,1px));box-sizing:border-box!important;border-style:solid;padding:var(--textPadding);margin:0;max-width:100%;text-overflow:ellipsis;text-align:var(--textAlign);direction:var(--dir);min-height:var(--inputHeight);width:100%}._2dBhC ._1SOvY[type=number]{-webkit-appearance:textfield;-moz-appearance:textfield;width:100%}._2dBhC ._1SOvY::-moz-placeholder{color:rgb(var(--txt2,var(--color_15)))}._2dBhC ._1SOvY:-ms-input-placeholder{color:rgb(var(--txt2,var(--color_15)))}._2dBhC ._1SOvY::placeholder{color:rgb(var(--txt2,var(--color_15)))}._2dBhC ._1SOvY:hover{border-width:var(--brwh,1px);background-color:rgba(var(--bgh,255,255,255),var(--alpha-bgh,1));border-style:solid;border-color:rgba(var(--brdh,163,217,246),var(--alpha-brdh,1))}._2dBhC ._1SOvY:disabled{background-color:rgba(var(--bgd,204,204,204),var(--alpha-bgd,1));color:rgb(var(--txtd,255,255,255));border-width:var(--brwd,1px);border-style:solid;border-color:rgba(var(--brdd,163,217,246),var(--alpha-brdd,1))}._2dBhC:not(._3TyBu) ._1SOvY:focus{border-width:var(--brwf,1px);background-color:rgba(var(--bgf,255,255,255),var(--alpha-bgf,1));border-style:solid;border-color:rgba(var(--brdf,163,217,246),var(--alpha-brdf,1))}._2dBhC._3TyBu ._1SOvY:invalid{border-width:var(--brwe,1px);background-color:rgba(var(--bge,255,255,255),var(--alpha-bge,1));border-style:solid;border-color:rgba(var(--brde,163,217,246),var(--alpha-brde,1))}._2dBhC._3TyBu ._1SOvY:not(:invalid):focus{border-width:var(--brwf,1px);background-color:rgba(var(--bgf,255,255,255),var(--alpha-bgf,1));border-style:solid;border-color:rgba(var(--brdf,163,217,246),var(--alpha-brdf,1))}._2dBhC .aHD7c{display:none}._2dBhC._2nVk2 .aHD7c{font:var(--fntlbl,var(--font_8));color:rgb(var(--txtlbl,var(--color_15)));word-break:break-word;display:inline-block;line-height:1;margin-bottom:var(--labelMarginBottom);padding:var(--labelPadding);text-align:var(--textAlign);direction:var(--dir)}._2dBhC._2nVk2._65cjg .aHD7c:after{display:var(--requiredIndicationDisplay,none);content:" *";color:rgba(var(--txtlblrq,0,0,0),var(--alpha-txtlblrq,0))}.nK06J{display:var(--display);--display:flex;flex-direction:column}.nK06J .XRJUI{flex:1;display:flex;flex-direction:column;position:relative}.nK06J ._1yU6F{font:var(--fntprefix,normal normal normal 16px/1.4em helvetica-w01-roman);position:absolute;left:0;top:0;width:50px;max-height:100%;display:flex;justify-content:center;align-items:center}.nK06J ._1SOvY,.nK06J ._1yU6F{color:var(--corvid-color,rgb(var(--txt,var(--color_15))));min-height:100%}.nK06J ._1SOvY{box-shadow:var(--shd,0 0 0 transparent);font:var(--fnt,var(--font_8));-webkit-appearance:none;-moz-appearance:none;border-radius:var(--corvid-border-radius,var(--rd,0));background-color:var(--corvid-background-color,rgba(var(--bg,255,255,255),var(--alpha-bg,1)));border-color:var(--corvid-border-color,rgba(var(--brd,227,227,227),var(--alpha-brd,1)));border-width:var(--corvid-border-width,var(--brw,1px));box-sizing:border-box!important;border-style:solid;padding:var(--textPadding);margin:0;width:100%;flex:1;text-overflow:ellipsis;text-align:var(--textAlign);direction:var(--dir)}.nK06J ._1SOvY[type=number]{-webkit-appearance:textfield;-moz-appearance:textfield}.nK06J ._1SOvY::-moz-placeholder{color:rgb(var(--txt2,var(--color_15)))}.nK06J ._1SOvY:-ms-input-placeholder{color:rgb(var(--txt2,var(--color_15)))}.nK06J ._1SOvY::placeholder{color:rgb(var(--txt2,var(--color_15)))}.nK06J ._1SOvY:hover{border-width:var(--brwh,1px);background-color:rgba(var(--bgh,255,255,255),var(--alpha-bgh,1));border-style:solid;border-color:rgba(var(--brdh,163,217,246),var(--alpha-brdh,1))}.nK06J ._1SOvY:disabled{background-color:rgba(var(--bgd,204,204,204),var(--alpha-bgd,1));color:rgb(var(--txtd,255,255,255));border-width:var(--brwd,1px);border-style:solid;border-color:rgba(var(--brdd,163,217,246),var(--alpha-brdd,1))}.nK06J:not(._3TyBu) ._1SOvY:focus{border-width:var(--brwf,1px);background-color:rgba(var(--bgf,255,255,255),var(--alpha-bgf,1));border-style:solid;border-color:rgba(var(--brdf,163,217,246),var(--alpha-brdf,1))}.nK06J._3TyBu ._1SOvY:invalid{border-width:var(--brwe,1px);background-color:rgba(var(--bge,255,255,255),var(--alpha-bge,1));border-style:solid;border-color:rgba(var(--brde,163,217,246),var(--alpha-brde,1))}.nK06J._3TyBu ._1SOvY:not(:invalid):focus{border-width:var(--brwf,1px);background-color:rgba(var(--bgf,255,255,255),var(--alpha-bgf,1));border-style:solid;border-color:rgba(var(--brdf,163,217,246),var(--alpha-brdf,1))}.nK06J .aHD7c{display:none}.nK06J._2nVk2 .aHD7c{font:var(--fntlbl,var(--font_8));color:rgb(var(--txtlbl,var(--color_15)));word-break:break-word;display:inline-block;line-height:1;margin-bottom:var(--labelMarginBottom);padding:var(--labelPadding);text-align:var(--textAlign);direction:var(--dir)}.nK06J._2nVk2._65cjg .aHD7c:after{display:var(--requiredIndicationDisplay,none);content:" *";color:rgba(var(--txtlblrq,0,0,0),var(--alpha-txtlblrq,0))}.Captcha3940957316__root{position:relative}@-webkit-keyframes Captcha3940957316__spinner-spin{to{transform:rotate(1turn)}}@keyframes Captcha3940957316__spinner-spin{to{transform:rotate(1turn)}}@-webkit-keyframes Captcha3940957316__fadeOut{0%{opacity:1;width:304px;height:78px}99%{opacity:.000001;width:304px;height:78px}to{opacity:0;width:0;height:0}}@keyframes Captcha3940957316__fadeOut{0%{opacity:1;width:304px;height:78px}99%{opacity:.000001;width:304px;height:78px}to{opacity:0;width:0;height:0}}.Captcha3940957316__root .Captcha3940957316__captchaLoader{position:absolute;top:0;left:0;display:flex;justify-content:center;align-items:center}.Captcha3940957316__root .Captcha3940957316__captchaLoader:before{content:"";display:block;background-color:transparent;border:6px solid #4d90fe;border-radius:36px;border-bottom-color:transparent;border-left-color:transparent;height:36px;outline:0;width:36px;box-sizing:border-box;-webkit-animation:Captcha3940957316__spinner-spin 1s linear infinite;animation:Captcha3940957316__spinner-spin 1s linear infinite;-webkit-animation-play-state:running;animation-play-state:running;transition-duration:1s;opacity:1}.Captcha3940957316__root:not(.Captcha3940957316--loaded) .Captcha3940957316__captchaLoader{width:304px;height:78px;opacity:1}.Captcha3940957316__root.Captcha3940957316--loaded .Captcha3940957316__captchaLoader{-webkit-animation:Captcha3940957316__fadeOut .5s ease;animation:Captcha3940957316__fadeOut .5s ease;-webkit-animation-play-state:running;animation-play-state:running;opacity:0;width:0;height:0}.Captcha3940957316__root:not(.Captcha3940957316--loaded) .Captcha3940957316__captcha{width:304px;height:78px;opacity:0}.Captcha3940957316__root.Captcha3940957316--loaded .Captcha3940957316__captcha{opacity:1;transition:opacity .5s}.Captcha3940957316__root.Captcha3940957316--isMobileFriendly .Captcha3940957316__captcha,.Captcha3940957316__root.Captcha3940957316--isMobileFriendly .Captcha3940957316__captchaLoader{transform:scale(.77);transform-origin:top left}.Captcha3940957316__root .Captcha3940957316__checkbox{position:absolute;opacity:0;bottom:0;left:0;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}._2SGQR{position:fixed!important;width:100%!important;height:100%!important;top:0;right:0;bottom:0;left:0;display:flex;align-items:center;justify-content:center;z-index:calc(var(--above-all-z-index) + 1);color:rgb(var(--txt,156,156,156))}._2SGQR ._2ftQT{position:fixed;touch-action:manipulation;top:60px;right:60px;background:none;z-index:3}._2SGQR ._2ftQT:hover{cursor:pointer}._2SGQR ._2ftQT svg{width:25px;height:25px;stroke:param-color(clr,color_15);stroke-width:15;cursor:pointer;pointer-events:none;fill:rgba(var(--color_15),var(--alpha-color_15,1))}._2SGQR[data-layout=popup] ._2ftQT{position:absolute;top:24px;right:24px}._2SGQR ._9mKKC{background-color:rgba(var(--bg-clr,var(--color_11)),var(--alpha-bg-clr,1));position:fixed;top:0;right:0;bottom:0;left:0;visibility:visible;zoom:1;overflow:auto}._2SGQR[data-layout=popup] ._9mKKC{background-color:#000;opacity:.6}._2SGQR .hfAwd{position:fixed;display:flex;flex-direction:column;align-items:center;overflow:auto}body:not(.device-mobile-optimized) ._2SGQR .hfAwd[data-dialogposition=center]>*{margin:auto}._2SGQR[data-layout=fullscreen] .hfAwd{width:100%;height:100%}._2SGQR[data-layout=popup] .hfAwd{background-color:rgba(var(--bg-clr,var(--color_11)),var(--alpha-bg-clr,1));box-sizing:border-box;max-height:100%}body.device-mobile-optimized ._2SGQR[data-layout=popup] .hfAwd{height:100%;width:100%}body:not(.device-mobile-optimized) ._2SGQR[data-layout=popup] .hfAwd{padding:70px 80px 80px;width:480px}._2SGQR[data-layout=customPopup] ._9mKKC{background-color:#000;opacity:.6}._2SGQR[data-layout=customPopup] ._2ftQT{position:absolute;top:24px;right:24px}._2SGQR[data-layout=customPopup] .hfAwd{background-color:rgba(var(--bg-clr,var(--color_11)),var(--alpha-bg-clr,1));box-sizing:border-box;max-height:100%}body.device-mobile-optimized ._2SGQR[data-layout=customPopup] .hfAwd{height:100%;width:100%;padding:80px 32px}body:not(.device-mobile-optimized) ._2SGQR[data-layout=customPopup] .hfAwd{padding:80px 32px;width:572px;height:600px}._2qccF,.EMaKv{width:100%;height:48px}._2qccF button,.EMaKv button{box-sizing:border-box;--fnt:getVar(font_8);font-size:16px;--rd:0px;--trans1:border-color 0.4s ease 0s,background-color 0.4s ease 0s;--shd:none;--trans2:color 0.4s ease 0s;--bgd:204,204,204;--brdd:204,204,204;--txtd:255,255,255;--boxShadowToggleOn-shd:none;--alpha-txt:1;--alpha-brd:1;--alpha-txth:1;--alpha-bgh:1;--label-align:center;--label-text-align:center}.EMaKv{--brw:0px;--txt:var(--color_8);--bg:var(--color_18);--brd:var(--color_14);--bgh:var(--color_18);--brdh:var(--color_12);--txth:var(--color_11);--alpha-brdh:0;--alpha-bg:1;--shc-mutated-brightness:28,37,106;--alpha-bgh:0.7}._2qccF button{--brw:1px;--txt:var(--color_15);--bg:(--color_11);--brd:var(--color_13);--bgh:var(--color_11);--brdh:var(--color_15);--txth:var(--color_15);--alpha-brdh:1;--alpha-bg:0;--shc-mutated-brightness:128,128,128}body:not(.device-mobile-optimized) ._2qccF,body:not(.device-mobile-optimized) .EMaKv{font-weight:300}body.device-mobile-optimized ._2SGQR ._2ftQT{top:30px;right:30px;width:20px;height:20px}body.device-mobile-optimized ._2SGQR ._2ftQT svg{width:16px;height:16px;stroke-width:12px}body.device-mobile-optimized ._2SGQR ._2qccF button{font-size:14px}._3bAue{position:fixed!important;width:100%!important;height:100%!important;top:0;right:0;bottom:0;left:0;display:flex;align-items:center;justify-content:center;z-index:calc(var(--above-all-z-index) + 1);color:rgb(var(--txt,156,156,156))}._3bAue ._3so_5{position:fixed;touch-action:manipulation;top:60px;right:60px;background:none;z-index:3}._3bAue ._3so_5:hover{cursor:pointer}._3bAue ._3so_5 svg{width:25px;height:25px;stroke:param-color(clr,color_15);stroke-width:15;cursor:pointer;pointer-events:none;fill:rgba(var(--color_15),var(--alpha-color_15,1))}._3bAue[data-layout=popup] ._3so_5{position:absolute;top:24px;right:24px}._3bAue ._2du_b{background-color:rgba(var(--bg-clr,var(--color_11)),var(--alpha-bg-clr,1));position:fixed;top:0;right:0;bottom:0;left:0;visibility:visible;zoom:1;overflow:auto}._3bAue[data-layout=popup] ._2du_b{background-color:#000;opacity:.6}._3bAue ._5RODg{position:fixed;display:flex;flex-direction:column;align-items:center;overflow:auto}body:not(.device-mobile-optimized) ._3bAue ._5RODg[data-dialogposition=center]>*{margin:auto}._3bAue[data-layout=fullscreen] ._5RODg{width:100%;height:100%}._3bAue[data-layout=popup] ._5RODg{background-color:rgba(var(--bg-clr,var(--color_11)),var(--alpha-bg-clr,1));box-sizing:border-box;max-height:100%}body.device-mobile-optimized ._3bAue[data-layout=popup] ._5RODg{height:100%;width:100%}body:not(.device-mobile-optimized) ._3bAue[data-layout=popup] ._5RODg{padding:70px 80px 80px;width:480px}._3bAue[data-layout=customPopup] ._2du_b{background-color:#000;opacity:.6}._3bAue[data-layout=customPopup] ._3so_5{position:absolute;top:24px;right:24px}._3bAue[data-layout=customPopup] ._5RODg{background-color:rgba(var(--bg-clr,var(--color_11)),var(--alpha-bg-clr,1));box-sizing:border-box;max-height:100%}body.device-mobile-optimized ._3bAue[data-layout=customPopup] ._5RODg{height:100%;width:100%;padding:80px 32px}body:not(.device-mobile-optimized) ._3bAue[data-layout=customPopup] ._5RODg{padding:80px 32px;width:572px;height:600px}._3pWgf,._22uF5{width:100%;height:48px}._3pWgf button,._22uF5 button{box-sizing:border-box;--fnt:getVar(font_8);font-size:16px;--rd:0px;--trans1:border-color 0.4s ease 0s,background-color 0.4s ease 0s;--shd:none;--trans2:color 0.4s ease 0s;--bgd:204,204,204;--brdd:204,204,204;--txtd:255,255,255;--boxShadowToggleOn-shd:none;--alpha-txt:1;--alpha-brd:1;--alpha-txth:1;--alpha-bgh:1;--label-align:center;--label-text-align:center}._3pWgf{--brw:0px;--txt:var(--color_8);--bg:var(--color_18);--brd:var(--color_14);--bgh:var(--color_18);--brdh:var(--color_12);--txth:var(--color_11);--alpha-brdh:0;--alpha-bg:1;--shc-mutated-brightness:28,37,106;--alpha-bgh:0.7}._22uF5 button{--brw:1px;--txt:var(--color_15);--bg:(--color_11);--brd:var(--color_13);--bgh:var(--color_11);--brdh:var(--color_15);--txth:var(--color_15);--alpha-brdh:1;--alpha-bg:0;--shc-mutated-brightness:128,128,128}body:not(.device-mobile-optimized) ._3pWgf,body:not(.device-mobile-optimized) ._22uF5{font-weight:300}body.device-mobile-optimized ._3bAue ._3so_5{top:30px;right:30px;width:20px;height:20px}body.device-mobile-optimized ._3bAue ._3so_5 svg{width:16px;height:16px;stroke-width:12px}body.device-mobile-optimized ._3bAue ._22uF5 button{font-size:14px}._277W2,._277W2 ._3YwvX{text-align:center}._277W2 ._3YwvX{font:var(--headingL,var(--font_2));font-size:48px;line-height:.8em;margin-bottom:23px;margin-top:10px;padding-top:0;padding-bottom:0}._277W2 ._3YwvX,._277W2 ._15npN{color:rgba(var(--clr,var(--color_15)),var(--alpha-clr,1))}._277W2 ._15npN{background:transparent;font-family:Helvetica}body.device-mobile-optimized ._277W2 ._15npN{width:100%}._277W2 ._15npN ._1Kr65{font:var(--fnt,var(--font_8));font-size:18px;color:rgba(var(--clr,var(--color_15)),var(--alpha-clr,1));text-align:center;margin-top:0;padding-bottom:60px}._277W2 ._15npN ._1QEG4{width:100%;padding-bottom:0}._277W2 ._15npN ._3pWgf{font:var(--font_8);font-size:16px;margin-top:25px;position:relative}body:not(.device-mobile-optimized) ._277W2 ._15npN ._3pWgf{width:320px}body.device-mobile-optimized ._277W2{width:280px;margin-top:74px}body.device-mobile-optimized ._277W2 ._3YwvX{line-height:.8em;margin-bottom:18px;font-size:36px}body.device-mobile-optimized ._277W2 ._15npN{height:auto;padding:0 0 20px}body.device-mobile-optimized ._277W2 ._15npN ._1Kr65{font-size:15px;margin-top:18px;padding-bottom:35px}body:not(.device-mobile-optimized) ._277W2 ._15npN{width:320px;max-width:450px;display:inline-block}._257XU{position:fixed!important;width:100%!important;height:100%!important;top:0;right:0;bottom:0;left:0;display:flex;align-items:center;justify-content:center;z-index:calc(var(--above-all-z-index) + 1);color:rgb(var(--txt,156,156,156))}._257XU .cl1s_{position:fixed;touch-action:manipulation;top:60px;right:60px;background:none;z-index:3}._257XU .cl1s_:hover{cursor:pointer}._257XU .cl1s_ svg{width:25px;height:25px;stroke:param-color(clr,color_15);stroke-width:15;cursor:pointer;pointer-events:none;fill:rgba(var(--color_15),var(--alpha-color_15,1))}._257XU[data-layout=popup] .cl1s_{position:absolute;top:24px;right:24px}._257XU ._3icA8{background-color:rgba(var(--bg-clr,var(--color_11)),var(--alpha-bg-clr,1));position:fixed;top:0;right:0;bottom:0;left:0;visibility:visible;zoom:1;overflow:auto}._257XU[data-layout=popup] ._3icA8{background-color:#000;opacity:.6}._257XU ._3BCw5{position:fixed;display:flex;flex-direction:column;align-items:center;overflow:auto}body:not(.device-mobile-optimized) ._257XU ._3BCw5[data-dialogposition=center]>*{margin:auto}._257XU[data-layout=fullscreen] ._3BCw5{width:100%;height:100%}._257XU[data-layout=popup] ._3BCw5{background-color:rgba(var(--bg-clr,var(--color_11)),var(--alpha-bg-clr,1));box-sizing:border-box;max-height:100%}body.device-mobile-optimized ._257XU[data-layout=popup] ._3BCw5{height:100%;width:100%}body:not(.device-mobile-optimized) ._257XU[data-layout=popup] ._3BCw5{padding:70px 80px 80px;width:480px}._257XU[data-layout=customPopup] ._3icA8{background-color:#000;opacity:.6}._257XU[data-layout=customPopup] .cl1s_{position:absolute;top:24px;right:24px}._257XU[data-layout=customPopup] ._3BCw5{background-color:rgba(var(--bg-clr,var(--color_11)),var(--alpha-bg-clr,1));box-sizing:border-box;max-height:100%}body.device-mobile-optimized ._257XU[data-layout=customPopup] ._3BCw5{height:100%;width:100%;padding:80px 32px}body:not(.device-mobile-optimized) ._257XU[data-layout=customPopup] ._3BCw5{padding:80px 32px;width:572px;height:600px}._2Dbuo,._2Qbrk{width:100%;height:48px}._2Dbuo button,._2Qbrk button{box-sizing:border-box;--fnt:getVar(font_8);font-size:16px;--rd:0px;--trans1:border-color 0.4s ease 0s,background-color 0.4s ease 0s;--shd:none;--trans2:color 0.4s ease 0s;--bgd:204,204,204;--brdd:204,204,204;--txtd:255,255,255;--boxShadowToggleOn-shd:none;--alpha-txt:1;--alpha-brd:1;--alpha-txth:1;--alpha-bgh:1;--label-align:center;--label-text-align:center}._2Dbuo{--brw:0px;--txt:var(--color_8);--bg:var(--color_18);--brd:var(--color_14);--bgh:var(--color_18);--brdh:var(--color_12);--txth:var(--color_11);--alpha-brdh:0;--alpha-bg:1;--shc-mutated-brightness:28,37,106;--alpha-bgh:0.7}._2Qbrk button{--brw:1px;--txt:var(--color_15);--bg:(--color_11);--brd:var(--color_13);--bgh:var(--color_11);--brdh:var(--color_15);--txth:var(--color_15);--alpha-brdh:1;--alpha-bg:0;--shc-mutated-brightness:128,128,128}body:not(.device-mobile-optimized) ._2Dbuo,body:not(.device-mobile-optimized) ._2Qbrk{font-weight:300}body.device-mobile-optimized ._257XU .cl1s_{top:30px;right:30px;width:20px;height:20px}body.device-mobile-optimized ._257XU .cl1s_ svg{width:16px;height:16px;stroke-width:12px}body.device-mobile-optimized ._257XU ._2Qbrk button{font-size:14px}._3q03n{text-align:center}._3q03n._282Cx{direction:rtl}body:not(.device-mobile-optimized) ._3q03n{position:relative;top:calc(50% - 230px)}body.device-mobile-optimized ._3q03n{margin-top:40px;width:280px}._3q03n._2zx3b{opacity:.6}._3q03n ._108U5{color:rgba(var(--color_15),var(--alpha-color_15,1));font:var(--ttlFnt,var(--font_2));font-size:48px;line-height:48px;margin-bottom:24px;margin-top:10px;padding-top:0;padding-bottom:0;text-align:center}body.device-mobile-optimized ._3q03n ._108U5{font-size:36px;margin-bottom:8px}body:not(.device-mobile-optimized) ._3q03n ._3P6-V{width:auto;max-width:800px;display:inline-block}body:not(.device-mobile-optimized) ._3q03n ._3P6-V._2pjHG ._1wr1f{width:320px}body:not(.device-mobile-optimized) ._3q03n ._3P6-V._2pjHG ._7ZI3j{border-right:1px solid param-color(secondary-color,color_13);margin:-20px 66px}._3q03n ._3P6-V._2pjHG .C5X_K{position:relative;width:320px;margin:auto}body.device-mobile-optimized ._3q03n ._3P6-V._2pjHG .C5X_K{margin-top:10px;width:100%}._3q03n ._3P6-V._2pjHG .QorYY{font-size:14px;line-height:14px;text-decoration:underline}._3q03n ._3P6-V._2pjHG .QorYY ._2SBsu{float:none}._3q03n ._3P6-V._2OgZa .C5X_K{display:block}._3q03n ._3P6-V._2OgZa .tTRde{display:block!important;margin:34px auto}body:not(.device-mobile-optimized) ._3q03n ._3P6-V ._1wr1f{width:320px;margin:auto}._3q03n ._3P6-V ._1wr1f ._3ix4Z,._3q03n ._3P6-V ._1wr1f ._386jm{--textAlign:left;--shd:0.00px 2.00px 0px 0px rgba(var(--color_15),1);--rd:0px;--fnt:normal normal normal 15px/1.4em futura-lt-w01-book,sans-serif;--brw:0px;--bg:245,255,254;--txt:82,82,82;--brd:82,82,82;--txt2:var(--color_15);--brwh:0px;--bgh:232,255,253;--brdh:0,0,0;--brwf:0px;--bgf:245,255,254;--brdf:0,0,0;--brwe:1px;--bge:255,64,64;--brde:255,64,64;--trns:opacity 0.5s ease 0s,border 0.5s ease 0s,color 0.5s ease 0s;--bgd:204,204,204;--txtd:232,232,232;--brwd:1px;--brdd:219,219,219;--fntlbl:normal normal normal 15px/1.4em futura-lt-w01-book,sans-serif;--txtlbl:var(--color_15);--txtlblrq:null;--alpha-brdf:0;--alpha-brdh:0;--alpha-brd:1;--alpha-bgd:0;--alpha-bge:0;--alpha-bgf:0;--alpha-bg:0;--alpha-bgh:0;height:auto;--dir:ltr;--labelMarginBottom:14px;--inputHeight:37px;--textPadding:3px 3px 3px 0px;--labelPadding:0 20px 0 0px;--requiredIndicationDisplay:none}body:not(.device-mobile-optimized) ._3q03n ._3P6-V .C5X_K{width:320px;margin:auto}._3q03n ._3P6-V .C5X_K ._2smz9{width:100%;opacity:0;transition:opacity 1s ease-in}._3q03n ._3P6-V .C5X_K ._2smz9._2EzZt{opacity:1}._3q03n ._3P6-V .C5X_K ._2FC1o{opacity:0;transition:opacity 1s ease-in}._3q03n ._3P6-V .C5X_K ._2FC1o._2EzZt{opacity:1}._3q03n ._24tJo{text-align:center;padding-top:0;margin-bottom:32px}._3q03n ._24tJo ._2ya-H{color:rgba(var(--color_15),var(--alpha-color_15,1));margin-right:4px;margin-left:4px}body:not(.device-mobile-optimized) ._3q03n ._24tJo ._2ya-H{font:var(--fnt,var(--font_8));font-size:18px}body.device-mobile-optimized ._3q03n ._24tJo ._2ya-H{font:var(--fnt,var(--font_8));font-size:15px}._3q03n ._24tJo ._3VCEv{color:rgb(var(--txt,var(--color_18)));border-bottom:1px solid #0198ff;float:none;border:none;text-align:left;font-weight:400;text-decoration:none;cursor:pointer}body.device-mobile-optimized ._3q03n ._24tJo ._3VCEv{font:var(--fnt,var(--font_8));font-size:15px}body:not(.device-mobile-optimized) ._3q03n ._24tJo ._3VCEv{font:var(--fnt,var(--font_8));font-size:18px}._3q03n ._24tJo ._3VCEv:hover{border-bottom-color:#04f}._3q03n ._1KGY5{width:320px;text-align:center;margin:12px auto 24px;height:11px;border-bottom:1px solid;border-color:rgba(var(--color_13),var(--alpha-color_13,1))}body.device-mobile-optimized ._3q03n ._1KGY5{width:100%}._3q03n ._1KGY5 span{font:var(--fnt,var(--font_8));font-size:16px;color:rgba(var(--color_15),var(--alpha-color_15,1));background-color:rgba(var(--bg,var(--color_11)),var(--alpha-bg,1));padding:0 13px}._3q03n ._3ix4Z,._3q03n ._386jm{position:relative;margin-bottom:25px}._3q03n ._2Dbuo,._3q03n ._2Qbrk{position:relative;font:var(--font_8)}._3q03n ._2Qbrk{height:40px}._3q03n .dq7tr{font:var(--fnt,var(--font_8));color:rgba(var(--color_15),var(--alpha-color_15,1));font-size:16px;line-height:17px;text-decoration:underline;cursor:pointer;opacity:1;transition:opacity .2s ease-in-out;border:none;text-align:left}._3q03n .dq7tr:hover{opacity:.8;transition:opacity .2s ease-in-out}._3q03n .dq7tr._3-OPK{font-size:14px;line-height:14px;padding-top:24px}body.device-mobile-optimized ._3q03n .dq7tr._2CZUy,body:not(.device-mobile-optimized) ._3q03n .dq7tr._3-OPK{display:none}._3q03n .YJmfJ{display:flex;justify-content:space-between;padding-top:13px;padding-bottom:18px}._2vlNF{--preloaderTop:calc(50% - 1270px)}._2FC1o,._2smz9{width:100%}._2FC1o{height:32px}._1SgFc{display:none}._3Wc6Z{margin-bottom:20px}._2SEkn{margin-bottom:8px;color:#df3131;font-size:12px}._3c3w7{position:fixed;bottom:0;left:0;background-color:#212121;padding:16px 20px;box-shadow:inset 0 4px 8px 0 #000,0 0 4px 0 #000;width:100%;font-size:16px;text-align:initial;color:#fff;visibility:hidden;opacity:0;transition:visibility .1s,opacity .1s linear;box-sizing:border-box}._2EJ9U{visibility:visible;opacity:1}._3PWVz{margin:48px 0}._2OZ2i{width:280px;margin:0 auto}._1TTJD{font:var(--fnt,var(--font_8));text-align:center}body:not(.device-mobile-optimized) ._1TTJD{width:320px}._1TTJD .FOEsm{direction:ltr;margin-bottom:13px;height:18px}._1TTJD .FOEsm ._1zRfH{margin-right:12px;width:18px;height:18px}._1TTJD .FOEsm ._1zRfH:last-child{margin-right:0}._1TTJD ._10Bye{font:var(--font_8);font-size:15px;font-weight:300}._1TTJD ._2_m8e,._1TTJD ._10Bye{color:rgba(var(--color_15),var(--alpha-color_15,1));line-height:1.6}._1TTJD ._2_m8e{text-decoration:underline;font:var(--font_8);font-size:15px;cursor:pointer;opacity:1;transition:opacity .2s ease-in-out}._1TTJD ._2_m8e:hover{opacity:.8;transition:opacity .2s ease-in-out}.QWdF9{font:var(--font_8);display:flex;flex-direction:column;margin-top:48px;font-size:15px;line-height:1.6;text-align:center;color:rgba(var(--color_15),var(--alpha-color_15,1));font-weight:300}.QWdF9:empty{margin-top:0}.QWdF9 .KXQVw{color:rgba(var(--color_15),var(--alpha-color_15,1));font-weight:300;text-decoration:underline;cursor:pointer;opacity:1}.QWdF9 .KXQVw,.QWdF9 .KXQVw:hover{transition:opacity .2s ease-in-out}.QWdF9 .KXQVw:hover{opacity:.8}._1UZRs{position:relative;align-self:center;font-size:15px;margin-bottom:10px;max-width:540px}body.device-mobile-optimized ._1UZRs{width:100%}._1UZRs ._5B5Lw{display:flex;align-items:center;justify-content:center}body.device-mobile-optimized ._1UZRs ._5B5Lw{margin-bottom:10px}._1UZRs ._5B5Lw label{margin-right:5px}._1UZRs ._5B5Lw button{text-decoration:underline;cursor:pointer;font-size:15px}._1UZRs ._5B5Lw input{margin:5px 10px 3px 0;width:12px;cursor:pointer;vertical-align:top}._1UZRs ._5B5Lw._282Cx label{margin:0 0 0 5px}._1UZRs ._5B5Lw._282Cx input{margin:5px 0 3px 10px}.vtylp>:not(:last-child){margin-right:4px}._1xMOU{display:block}
    </style>
    <style data-href="https://static.parastorage.com/services/editor-elements/dist/rb_wixui.thunderbolt[VerticalMenu_VerticalMenuSolidColorSkin].34b72cf9.min.css">
        ._2w_0a{margin:0 10px;opacity:.6}._27jH9{height:auto!important}._2VltG .EtZuE{border:solid var(--brw,1px) rgba(var(--brd,var(--color_15)),var(--alpha-brd,1));box-shadow:var(--shd,0 1px 4px rgba(0,0,0,.6));border-radius:var(--rd,0)}._2VltG ._2G_G3{height:100%;width:100%;position:relative;border-bottom:solid var(--sepw,1px) rgba(var(--sep,var(--color_15)),var(--alpha-sep,1))}._2VltG ._2G_G3:last-child{border-bottom:0}._2VltG .qM7uv{box-sizing:border-box;position:absolute;min-width:100%;z-index:999;visibility:hidden;border:solid var(--brw,1px) rgba(var(--brd,var(--color_15)),var(--alpha-brd,1));border-radius:var(--SKINS_submenuBR,0);box-shadow:var(--shd,0 1px 4px rgba(0,0,0,.6))}._2VltG .qM7uv.NF5Je{left:calc(100% + var(--SKINS_submenuMargin, 8px))}._2VltG .qM7uv._2_mcI{right:calc(100% + var(--SKINS_submenuMargin, 8px))}._2VltG .qM7uv._174FM{bottom:var(--brw,1px)}._2VltG .qM7uv.NAZFX{top:calc(-1 * var(--brw, 1px))}._2VltG .qM7uv:before{content:" ";height:100%;width:var(--SKINS_submenuMargin,8px);position:absolute;top:0;right:var(--sub-menu-open-direction-right,auto);left:var(--sub-menu-open-direction-left,0);margin-right:calc(-1 * var(--SKINS_submenuMargin, 8px));margin-left:calc(-1 * var(--SKINS_submenuMargin, 8px))}._2VltG ._2hPNU{background-color:rgba(var(--bg,var(--color_11)),var(--alpha-bg,1));height:var(--item-height,50px);transition:var(--itemBGColorTrans,background-color .4s ease 0s)}._2VltG ._2hPNU._2oQc7>._1Fk74>._1wg1m{cursor:default}._2VltG ._2hPNU._38rtH{background-color:rgba(var(--bgs,var(--color_15)),var(--alpha-bgs,1))}._2VltG ._2hPNU._38rtH>._1Fk74>._1wg1m{color:rgb(var(--txts,var(--color_13)))}._2VltG ._2hPNU.MqKU0>.qM7uv{visibility:visible}._2VltG ._2hPNU.MqKU0:not(._2oQc7){background-color:rgba(var(--bgh,var(--color_15)),var(--alpha-bgh,1))}._2VltG ._2hPNU.MqKU0:not(._2oQc7)>._1Fk74>._1wg1m{color:rgb(var(--txth,var(--color_13)))}._2VltG .qM7uv ._2hPNU{background-color:rgba(var(--SKINS_bgSubmenu,var(--color_11)),var(--alpha-SKINS_bgSubmenu,1))}._2VltG .qM7uv ._2G_G3 ._2hPNU{border-radius:0}._2VltG ._2G_G3:first-child ._2hPNU{border-radius:var(--rd,0);border-bottom-left-radius:0;border-bottom-right-radius:0}._2VltG ._2G_G3:last-child ._2hPNU{border-radius:var(--rd,0);border-top-left-radius:0;border-top-right-radius:0;border-bottom:0 solid transparent}._2VltG ._1wg1m{box-sizing:border-box;cursor:pointer;color:rgb(var(--txt,var(--color_15)));font:var(--fnt,var(--font_1));position:relative;display:flex;justify-content:var(--text-align,flex-start);height:100%;padding-left:var(--textSpacing,10px);padding-right:var(--textSpacing,10px);white-space:nowrap}._2VltG ._1wg1m,._2VltG ._1wg1m:after{background-color:transparent;width:100%}._2VltG ._1wg1m:after{cursor:default;content:" ";height:var(--sepw,1px);position:absolute;top:100%;left:0}._2VltG ._2G_G3 ._2hPNU ._1wg1m{line-height:var(--item-height,50px)}._2VltG .qM7uv ._1wg1m{font:var(--SKINS_fntSubmenu,var(--font_1))}
    </style>
    <style data-href="https://static.parastorage.com/services/editor-elements/dist/rb_wixui.thunderbolt~bootstrap.f47ae840.chunk.min.css">
        .StylableButton2545352419__root{-archetype:box;cursor:pointer;border:none;display:block;min-width:10px;min-height:10px;width:100%;height:100%;box-sizing:border-box;padding:0}.StylableButton2545352419__root[disabled]{pointer-events:none}.StylableButton2545352419__root.StylableButton2545352419--hasBackgroundColor{background-color:var(--corvid-background-color)!important}.StylableButton2545352419__root.StylableButton2545352419--hasBorderColor{border-color:var(--corvid-border-color)!important}.StylableButton2545352419__root.StylableButton2545352419--hasBorderRadius{border-radius:var(--corvid-border-radius)!important}.StylableButton2545352419__root.StylableButton2545352419--hasBorderWidth{border-width:var(--corvid-border-width)!important}.StylableButton2545352419__root.StylableButton2545352419--hasColor,.StylableButton2545352419__root.StylableButton2545352419--hasColor .StylableButton2545352419__label{color:var(--corvid-color)!important}.StylableButton2545352419__link{-archetype:box;text-decoration:none;box-sizing:border-box;color:#000}.StylableButton2545352419__container{display:flex;flex-basis:auto;justify-content:center;flex-direction:row;flex-grow:1;align-items:center;overflow:hidden;height:100%;width:100%;transition:all .2s ease,visibility 0s}.StylableButton2545352419__label{-archetype:text;-controller-part-type:LayoutChildDisplayDropdown,LayoutFlexChildSpacing(first);overflow:hidden;text-overflow:ellipsis;text-align:center;white-space:nowrap;min-width:1.8em;max-width:100%;transition:inherit}.StylableButton2545352419__root.StylableButton2545352419--isMaxContent .StylableButton2545352419__label{text-overflow:unset}.StylableButton2545352419__root.StylableButton2545352419--isWrapText .StylableButton2545352419__label{overflow-wrap:break-word;white-space:break-spaces;word-break:break-word;min-width:10px}.StylableButton2545352419__icon{-archetype:icon;-controller-part-type:LayoutChildDisplayDropdown,LayoutFlexChildSpacing(last);min-width:1px;height:50px;transition:inherit;flex-shrink:0}.StylableButton2545352419__icon.StylableButton2545352419--override{display:block!important}.StylableButton2545352419__icon>div,.StylableButton2545352419__icon svg{display:flex;width:inherit;height:inherit}.qhwIj{pointer-events:none;overflow:hidden;padding:0;white-space:nowrap}._1kIwI{cursor:pointer}.rmFV4{position:absolute;top:0;right:0;bottom:0;width:100%;left:0}._32Yq6{transition:.2s ease-in;transform:translateY(-100%)}._18iiC{transition:.2s}.Gz2xy{transition:.2s ease-in;opacity:0}.Gz2xy.Xf6e1{z-index:-1!important}._38jLj{transition:.2s;opacity:1}._1U65c{height:auto}._1U65c,._319u9{position:relative;width:100%}body:not(.device-mobile-optimized) ._3N4he{margin-left:calc((100% - var(--site-width)) / 2);width:var(--site-width)}._3Fgqs[data-focuscycled=active]{outline:1px solid transparent}._3Fgqs[data-focuscycled=active]:not(:focus-within){outline:2px solid transparent;transition:outline .01s ease}._3Fgqs ._3N4he{position:absolute;top:0;right:0;bottom:0;left:0}._2O-Ry .xb9fU ._3lu8e{display:var(--item-display);width:var(--item-size);height:var(--item-size);margin:var(--item-margin)}._2O-Ry .xb9fU ._3lu8e:last-child{margin:0}._2O-Ry .xb9fU ._3lu8e ._26AQd{display:block}._2O-Ry .xb9fU ._3lu8e ._26AQd .uWpzU{width:var(--item-size);height:var(--item-size)}._2O-Ry .xb9fU{position:absolute;width:100%;height:100%;white-space:nowrap}body.device-mobile-optimized ._2O-Ry .xb9fU{white-space:normal}._1-1oy{opacity:0}._341Ph{transition:opacity var(--transition-duration) cubic-bezier(.37,0,.63,1)}._341Ph,.VjMj0{opacity:1}.eZ2hw{transition:opacity var(--transition-duration) cubic-bezier(.37,0,.63,1)}.eZ2hw,.lBzae{opacity:0}._2c9JY{transition:opacity var(--transition-duration) cubic-bezier(.64,0,.78,0)}._2c9JY,.TiUWe{opacity:1}._1rVdW{opacity:0;transition:opacity var(--transition-duration) cubic-bezier(.22,1,.36,1)}._1RCYt{transform:translateX(100%)}.DowXu{transition:transform var(--transition-duration) cubic-bezier(.87,0,.13,1)}._31_kx,.DowXu{transform:translateX(0)}._2Q7R4{transition:transform var(--transition-duration) cubic-bezier(.87,0,.13,1)}._2mLeX,._2Q7R4{transform:translateX(-100%)}._2z6Li{transition:transform var(--transition-duration) cubic-bezier(.87,0,.13,1)}._2_vil,._2z6Li{transform:translateX(0)}._2DtIH{transform:translateX(100%);transition:transform var(--transition-duration) cubic-bezier(.87,0,.13,1)}._1qXF8{transform:translateY(100%)}._3eaIv{transition:transform var(--transition-duration) cubic-bezier(.87,0,.13,1)}._3eaIv,.qKSqT{transform:translateY(0)}._2QIi8{transition:transform var(--transition-duration) cubic-bezier(.87,0,.13,1)}._2eIOW,._2QIi8{transform:translateY(-100%)}.mvyRX{transition:transform var(--transition-duration) cubic-bezier(.87,0,.13,1)}.mvyRX,.NzCsb{transform:translateY(0)}.r7Qfi{transform:translateY(100%);transition:transform var(--transition-duration) cubic-bezier(.87,0,.13,1)}body:not(.responsive) ._1gF1C{overflow-x:clip}._2YGAo{height:100%;display:grid;grid-template-rows:1fr;grid-template-columns:1fr}._2YGAo>div{justify-self:stretch!important;align-self:stretch!important}._2wYm8{position:absolute;top:0;right:0;bottom:0;left:0}.N1N2o{cursor:pointer}._3bLYT{-webkit-tap-highlight-color:rgba(0,0,0,0);opacity:var(--opacity);fill:var(--fill);fill-opacity:var(--fill-opacity);stroke:var(--stroke);stroke-opacity:var(--stroke-opacity);stroke-width:var(--stroke-width);transform:var(--flip);filter:var(--drop-shadow,none)}._3bLYT,._3bLYT svg{position:absolute;top:0;right:0;bottom:0;left:0}._3bLYT svg{width:var(--svg-calculated-width,100%);height:var(--svg-calculated-height,100%);padding:var(--svg-calculated-padding,0);margin:auto}._2OIRR *{vector-effect:non-scaling-stroke}._3hiPA svg,.Fspl4 svg{overflow:visible!important}@media not all and (min-resolution:0.001dpcm){@supports (-webkit-appearance:none){._3bLYT._3hiPA{will-change:filter}}}ol.font_100,ul.font_100{color:#080808;font-family:"Arial, Helvetica, sans-serif",serif;font-size:10px;font-style:normal;font-variant:normal;font-weight:400;margin:0;text-decoration:none;line-height:normal;letter-spacing:normal}ol.font_100 li,ul.font_100 li{margin-bottom:12px}ol.wix-list-text-align,ul.wix-list-text-align{list-style-position:inside}ol.wix-list-text-align h1,ol.wix-list-text-align h2,ol.wix-list-text-align h3,ol.wix-list-text-align h4,ol.wix-list-text-align h5,ol.wix-list-text-align h6,ol.wix-list-text-align p,ul.wix-list-text-align h1,ul.wix-list-text-align h2,ul.wix-list-text-align h3,ul.wix-list-text-align h4,ul.wix-list-text-align h5,ul.wix-list-text-align h6,ul.wix-list-text-align p{display:inline}._2TSXm{cursor:pointer}.RWEwb{border:0;clip:rect(0 0 0 0);height:1px;margin:-1px;overflow:hidden;padding:0;position:absolute;width:1px}._3bcaz [data-attr-richtext-marker=true] table{border-collapse:collapse;margin:15px 0;width:100%}._3bcaz [data-attr-richtext-marker=true] table td{position:relative;padding:12px}._3bcaz [data-attr-richtext-marker=true] table td:after{content:"";position:absolute;top:0;left:0;bottom:0;right:0;border-left:1px solid currentColor;border-bottom:1px solid currentColor;opacity:.2}._3bcaz [data-attr-richtext-marker=true] table tr td:last-child:after{border-right:1px solid currentColor}._3bcaz [data-attr-richtext-marker=true] table tr:first-child td:after{border-top:1px solid currentColor}.xu_ui{min-height:var(--min-height);min-width:var(--min-width)}.xu_ui .H4KOs{position:relative;width:100%;height:100%;word-wrap:break-word;overflow-wrap:break-word}.xu_ui .H4KOs ul{list-style:disc inside}.xu_ui .H4KOs li{margin-bottom:12px}._1Q9if blockquote,._1Q9if h1,._1Q9if h2,._1Q9if h3,._1Q9if h4,._1Q9if h5,._1Q9if h6,._1Q9if p{line-height:normal;letter-spacing:normal}._1UxX6{min-height:var(--min-height);min-width:var(--min-width)}._1UxX6 .H4KOs{position:relative;width:100%;height:100%;word-wrap:break-word;overflow-wrap:break-word}._1UxX6 .H4KOs ol,._1UxX6 .H4KOs ul{padding-left:1.3em;margin-left:.5em;line-height:normal;letter-spacing:normal}._1UxX6 .H4KOs ol[dir=rtl],._1UxX6 .H4KOs ul[dir=rtl]{padding-right:1.3em;margin-right:.5em}._1UxX6 .H4KOs ul{list-style-type:disc}._1UxX6 .H4KOs ol{list-style-type:decimal}._1UxX6 .H4KOs ol[dir=rtl],._1UxX6 .H4KOs ul[dir=rtl]{padding-right:1.3em;margin-right:.5em}._1UxX6 .H4KOs ol ul,._1UxX6 .H4KOs ul ul{list-style-type:circle;line-height:normal}._1UxX6 .H4KOs ol ol ul,._1UxX6 .H4KOs ol ul ul,._1UxX6 .H4KOs ul ol ul,._1UxX6 .H4KOs ul ul ul{list-style-type:square;line-height:normal}._1UxX6 .H4KOs li{font-style:inherit;font-weight:inherit;line-height:inherit;letter-spacing:normal}._1UxX6 .H4KOs h1,._1UxX6 .H4KOs h2,._1UxX6 .H4KOs h3,._1UxX6 .H4KOs h4,._1UxX6 .H4KOs h5,._1UxX6 .H4KOs h6,._1UxX6 .H4KOs p{margin:0;line-height:normal;letter-spacing:normal}._1UxX6 .H4KOs a{color:inherit}._1Q9if,._2Hij5{word-wrap:break-word;overflow-wrap:break-word;text-align:start;pointer-events:none;min-height:var(--min-height);min-width:var(--min-width)}._1Q9if>*,._2Hij5>*{pointer-events:auto}._1Q9if li,._2Hij5 li{font-style:inherit;font-weight:inherit;line-height:inherit;letter-spacing:normal}._1Q9if ol,._1Q9if ul,._2Hij5 ol,._2Hij5 ul{padding-left:1.3em;padding-right:0;margin-left:.5em;margin-right:0;line-height:normal;letter-spacing:normal}._1Q9if ul,._2Hij5 ul{list-style-type:disc}._1Q9if ol,._2Hij5 ol{list-style-type:decimal}._1Q9if ol ul,._1Q9if ul ul,._2Hij5 ol ul,._2Hij5 ul ul{list-style-type:circle}._1Q9if ol ol ul,._1Q9if ol ul ul,._1Q9if ul ol ul,._1Q9if ul ul ul,._2Hij5 ol ol ul,._2Hij5 ol ul ul,._2Hij5 ul ol ul,._2Hij5 ul ul ul{list-style-type:square}._1Q9if ol[dir=rtl],._1Q9if ol[dir=rtl] ol,._1Q9if ol[dir=rtl] ul,._1Q9if ul[dir=rtl],._1Q9if ul[dir=rtl] ol,._1Q9if ul[dir=rtl] ul,._2Hij5 ol[dir=rtl],._2Hij5 ol[dir=rtl] ol,._2Hij5 ol[dir=rtl] ul,._2Hij5 ul[dir=rtl],._2Hij5 ul[dir=rtl] ol,._2Hij5 ul[dir=rtl] ul{padding-left:0;padding-right:1.3em;margin-left:0;margin-right:.5em}._1Q9if blockquote,._1Q9if h1,._1Q9if h2,._1Q9if h3,._1Q9if h4,._1Q9if h5,._1Q9if h6,._1Q9if p,._2Hij5 blockquote,._2Hij5 h1,._2Hij5 h2,._2Hij5 h3,._2Hij5 h4,._2Hij5 h5,._2Hij5 h6,._2Hij5 p{margin:0}._1Q9if a,._2Hij5 a{color:inherit}.riLfl{top:0;left:0;position:fixed;width:100%;height:calc(100% - var(--wix-ads-height));margin-top:var(--wix-ads-height);display:grid;grid-template-columns:1fr;grid-template-rows:1fr}._337lI,.riLfl{pointer-events:none;z-index:var(--pinned-layer-in-container,var(--above-all-in-container))}
    </style>
    <style data-href="https://static.parastorage.com/services/editor-elements/dist/rb_wixui.thunderbolt~bootstrap-responsive.f3ca696f.chunk.min.css">
        ._3SSUZ{overflow-x:hidden}._3SSUZ .l76As{display:flex;flex-direction:column;height:100%;width:100%}._3SSUZ .l76As ._2Q8KO{flex:1}._3SSUZ .l76As ._2MHbW{width:calc(100% - (var(--menuTotalBordersX, 0px)));height:calc(100% - (var(--menuTotalBordersY, 0px)));white-space:nowrap;overflow:visible}._3SSUZ .l76As ._2MHbW .Y5j6d{display:inline-block}._3SSUZ .l76As ._2MHbW ._3hSjZ{display:block;width:100%}._3SSUZ ._2vbYi{z-index:99999;display:block;opacity:1}._3SSUZ ._2vbYi ._1mmDc{overflow:visible;display:inherit;white-space:nowrap;width:auto;visibility:inherit}._3SSUZ ._2vbYi._2GxvV{transition:visibility;transition-delay:.2s;visibility:visible}._3SSUZ ._2vbYi ._2nffT{display:inline-block}._3SSUZ ._1T0ju{display:none}._1oZ90>nav{top:0;right:0;bottom:0;left:0}._1oZ90 ._2MHbW,._1oZ90 ._2vbYi,._1oZ90>nav{position:absolute}._1oZ90 ._2vbYi{visibility:hidden;margin-top:7px}._1oZ90 ._2vbYi[data-dropMode=dropUp]{margin-top:0;margin-bottom:7px}._1oZ90 ._1mmDc{background-color:rgba(var(--bgDrop,var(--color_11)),var(--alpha-bgDrop,1));border-radius:var(--rd,0);box-shadow:var(--shd,0 1px 4px rgba(0,0,0,.6))}._1pt7e,._3KF65{height:100%;width:auto;position:relative;box-sizing:border-box;overflow:visible}._1pt7e[data-state~=header] a,._1pt7e[data-state~=header] div,._3KF65[data-state~=header] a,._3KF65[data-state~=header] div{cursor:default!important}._1pt7e ._2DUrw,._3KF65 ._2DUrw{display:inline-block;height:100%;width:100%}._3KF65{display:var(--display);--display:inline-block;cursor:pointer;font:var(--fnt,var(--font_1))}._3KF65 ._1u8sp{padding:0 var(--pad,5px)}._3KF65 ._1zyfI{display:inline-block;padding:0 10px;color:rgb(var(--txt,var(--color_15)));transition:var(--trans,color .4s ease 0s)}._3KF65[data-state~=drop]{width:100%;display:block}._3KF65[data-state~=drop] ._1zyfI{padding:0 .5em}._3KF65[data-state~=link]:hover ._1zyfI,._3KF65[data-state~=over] ._1zyfI{color:rgb(var(--txth,var(--color_14)));transition:var(--trans,color .4s ease 0s)}._3KF65[data-state~=selected] ._1zyfI{color:rgb(var(--txts,var(--color_14)));transition:var(--trans,color .4s ease 0s)}._1Bha0 ._1I2VE{position:absolute;top:0;right:0;bottom:0;left:0;overflow:hidden;background-color:rgba(var(--bg,var(--color_11)),var(--alpha-bg,1))}
    </style>
    <style data-href="https://static.parastorage.com/services/editor-elements/dist/rb_wixui.thunderbolt[LoginSocialBar].44cdbf02.min.css">
        ._3Wf7h{border-radius:var(--rd,0);box-shadow:var(--shd,0 0 0 transparent);display:var(--display);--display:flex;align-items:center;box-sizing:border-box;background-color:rgba(var(--bg,var(--color_11)),var(--alpha-bg,1));border:var(--brw,0) solid rgba(var(--brd,var(--color_15)),var(--alpha-brd,1))}._2NuO2{display:contents}._1uOLS._3gdA9 ._1qtDu{padding-left:0}._1uOLS._34F_W ._1qtDu{padding-right:0}._1qtDu{cursor:pointer;display:flex;align-items:center;white-space:nowrap;padding:6px 7px;position:relative;min-width:0}._1qtDu,._1qtDu ._1i-z_{color:rgb(var(--txt,var(--color_18)));font:var(--fnt,var(--font_8))}._1qtDu ._1i-z_{overflow:hidden;text-overflow:ellipsis;min-width:60px}._1qtDu ._1hHt1,._1qtDu ._1i-z_{padding-left:7px;padding-right:7px}._1qtDu ._1hHt1{display:flex;align-self:center}._1qtDu ._1hHt1 svg{width:var(--arrow-size,14px);height:var(--arrow-size,14px)}._1qtDu ._1hHt1 polygon{fill:rgb(var(--fillcolor,var(--color_18)))}._1qtDu:hover ._1i-z_{color:rgb(var(--txth,var(--color_19)))}._1qtDu:hover ._1hHt1 polygon{fill:rgb(var(--txth,var(--color_19)))}.Oj72I{display:flex;align-self:center;flex-shrink:0;padding-top:6px;padding-bottom:6px}.Oj72I ._2Tv0x{position:relative;width:21px;height:21px}.Oj72I ._2Tv0x svg{width:21px;height:21px;fill:rgba(var(--fillcolor,var(--color_0)),var(--alpha-fillcolor,1));fill-opacity:var(--alpha-fillcolor)}.Oj72I._31yWt{padding-left:14px;padding-right:3px}.Oj72I._31yWt ._7goxk{margin-right:10px}.Oj72I._31yWt .tOfYI{left:50%}.Oj72I._3Z5sI{padding-left:3px;padding-right:14px}.Oj72I._3Z5sI ._7goxk{margin-left:10px}.Oj72I._3Z5sI .tOfYI{right:50%}.tOfYI{display:block;background-color:rgba(var(--badge-bg,226,28,33),var(--alpha-badge-bg,1));color:rgb(var(--badge-txt,var(--color_11)));border-radius:10px;position:absolute;top:0;pointer-events:none;text-align:center;height:18px;line-height:18px;letter-spacing:1px;padding-left:6px;padding-right:6px}._3OYGH{flex-shrink:0;padding-left:7px;padding-right:7px;fill:rgba(var(--fillcolor,var(--color_0)),var(--alpha-fillcolor,1));fill-opacity:var(--alpha-fillcolor);position:inherit;height:var(--icon-size,26px)}._3OYGH._18z3l{height:calc(var(--icon-size, 26px) + var(--avatarAndArrowStrokeWidth, 0px))}._3OYGH._18z3l ._1EdKQ,._3OYGH._18z3l img,._3OYGH._18z3l svg{border-radius:var(--avatarCornerRadius,100px);border:var(--avatarAndArrowStrokeWidth,0) solid rgba(var(--brd,var(--color_15)),var(--alpha-brd,1))}._3OYGH .KkkFB{position:static}._3OYGH img,._3OYGH svg{position:static!important}._3OYGH ._1EdKQ,._3OYGH img,._3OYGH svg{width:var(--icon-size,26px)!important;height:var(--icon-size,26px)!important}._3OYGH ._1EdKQ{background-color:rgba(var(--fillcolor,var(--color_18)),var(--alpha-fillcolor,1))}._3OYGH ._1EdKQ div{color:#fff;font-size:calc(var(--icon-size, 26px) * .5)!important;line-height:var(--icon-size,26px)!important}.Avatarbase142013732__root{aspect-ratio:1;align-items:center;border-radius:50%;display:flex;justify-content:center;letter-spacing:0;text-align:center;width:100%;height:100%;overflow:hidden;position:relative}.Avatarbase142013732__anonymous{width:100%;height:100%}.Avatarbase142013732__image{opacity:1;width:100%;height:100%}.Avatarbase142013732__image.WowImage249947393__root:not(.WowImage249947393--isError){background-color:"color(--overridable)";border:solid;border-radius:"fallback(50%, 0px)";overflow:hidden}.Avatarbase142013732__image.WowImage249947393__root:not(.WowImage249947393--isError).WowImage249947393--noImage{background-color:"fallback(fallback(color(--overridable), opacity(color(color-5), 0.2)))"}.Avatarbase142013732__image.WowImage249947393__root .WowImage249947393__image{opacity:"fallback(--overridable, 100%)"}.Avatarbase142013732__loadingWrapper{position:absolute;width:100%;height:100%;display:flex;align-items:center;justify-content:center}.WowImage249947393__root{display:grid;height:100%;position:relative}.WowImage249947393__root.WowImage249947393--forceImageContain.WowImage249947393---resize-7-contain{width:100%}.WowImage249947393__root.WowImage249947393--forceImageContain.WowImage249947393---resize-7-contain>*{border:inherit;border-radius:inherit;display:flex;align-items:center;justify-content:center}.WowImage249947393__root.WowImage249947393--forceImageContain.WowImage249947393---resize-7-contain img{max-height:100%;max-width:100%;width:unset!important;height:unset!important;border:inherit;border-radius:inherit}.WowImage249947393__root.WowImage249947393--forceImageContain.WowImage249947393---resize-7-contain.WowImage249947393--verticalContainer img{width:min(var(--wut-source-width,100%),100%)!important}.WowImage249947393__root.WowImage249947393--forceImageContain.WowImage249947393---resize-7-contain.WowImage249947393--horizontalContainer img{height:min(var(--wut-source-height,100%),100%)!important}.WowImage249947393__root.WowImage249947393--noImage{background-color:"opacity(color(color-5), 0.2)"}.WowImage249947393__root img{vertical-align:middle}.WowImage249947393__root.WowImage249947393--focalPoint img{-o-object-position:var(--WowImage249947393-focalPointX,0) var(--WowImage249947393-focalPointY,0);object-position:var(--WowImage249947393-focalPointX,0) var(--WowImage249947393-focalPointY,0)}.WowImage249947393__root.WowImage249947393---resize-7-contain .WowImage249947393__image{-o-object-fit:contain;object-fit:contain}.WowImage249947393__root.WowImage249947393---resize-5-cover .WowImage249947393__image{-o-object-fit:cover;object-fit:cover}.WowImage249947393__root.WowImage249947393--fluid .WowImage249947393__image{width:100%;height:100%;overflow:hidden}.WowImage249947393__root:not(.WowImage249947393--stretchImage){align-items:center}.WowImage249947393__root.WowImage249947393--fluid:not(.WowImage249947393--stretchImage) .WowImage249947393__image,.WowImage249947393__root:not(.WowImage249947393--stretchImage) .WowImage249947393__image{width:min(var(--wut-source-width,100%),100%);height:min(var(--wut-source-height,100%),100%);margin:0 auto}.WowImage249947393__root.WowImage249947393---hoverEffect-4-zoom{overflow:hidden}.WowImage249947393__root.WowImage249947393---hoverEffect-4-zoom .WowImage249947393__image{overflow:initial;transition:all .5s cubic-bezier(.18,.73,.63,1);transform:scale(calc(100 / 107)) translate(-3.5%,-3.5%)}.WowImage249947393__root.WowImage249947393---hoverEffect-4-zoom:hover .WowImage249947393__image{transform:scale(1) translate(-3.5%,-3.5%)}.WowImage249947393__root.WowImage249947393---hoverEffect-6-darken:hover .WowImage249947393__image{filter:brightness(85%) contrast(115%)}.WowImage249947393__root:not(.WowImage249947393--isError).WowImage249947393__overrideStyleParams{background-color:"color(--overridable)";border:solid;border-radius:"fallback(--overridable, 0px)";overflow:hidden}.WowImage249947393__root:not(.WowImage249947393--isError).WowImage249947393--noImage.WowImage249947393__overrideStyleParams{background-color:"fallback(fallback(color(--overridable), opacity(color(color-5), 0.2)))"}.WowImage249947393__root.WowImage249947393__overrideStyleParams .WowImage249947393__image{opacity:"fallback(--overridable, 100%)"}.WowImage249947393__root.WowImage249947393--isError{background-color:"color(color-2)";position:relative}.WowImage249947393__root.WowImage249947393--isError img{display:none}.WowImage249947393__root .WowImage249947393__errorWrapper{display:flex;align-items:center;justify-content:center;position:absolute;flex-direction:column;background:rgba(0,0,0,.6);height:100%;width:100%;z-index:1}.WowImage249947393__errorMessage{--Text1234506454-primary-color:"color(fallback(color(color-1), color-5))";--Text1234506454-secondary-color:"color(fallback(color(color-1), color-4))"}.WowImage249947393__errorMessage.Text1234506454---priority-7-primary{color:var(--wut-text-color,var(--Text1234506454-primary-color))}.WowImage249947393__errorMessage.Text1234506454---priority-9-secondary{color:var(--wut-placeholder-color,var(--Text1234506454-secondary-color))}.WowImage249947393__errorMessage.Text1234506454---typography-10-smallTitle{font:"fallback(font({theme: 'Body-M', size:'14px', lineHeight: '1.5em'}), font({theme: 'Page-title', size: '24px', lineHeight: '1.33em'}))"}.WowImage249947393__errorMessage.Text1234506454---typography-11-runningText{font:"fallback(font({theme: 'Body-M', size:'14px', lineHeight: '1.5em'}), font({theme: 'Body-M', size: '16px', lineHeight: '1.5em'}))"}.WowImage249947393__errorMessage.Text1234506454---typography-8-listText{font:"fallback(font({theme: 'Body-M', size:'14px', lineHeight: '1.5em'}), font({theme: 'Body-M', size: '16px', lineHeight: '2em'}))"}.WowImage249947393__errorMessage.Text1234506454---typography-10-largeTitle{font:"fallback(font({theme: 'Body-M', size:'14px', lineHeight: '1.5em'}), font({theme: 'Heading-M', size: '32px', lineHeight: '1.25em'}))"}.WowImage249947393__errorMessage.Text1234506454--mobile.Text1234506454---typography-10-smallTitle{font:"fallback(font({theme: 'Body-M', size:'14px', lineHeight: '1.5em'}), font({theme: 'Page-title', size: '20px', lineHeight: '1.4em'}))"}.WowImage249947393__errorMessage.Text1234506454--mobile.Text1234506454---typography-11-runningText{font:"fallback(font({theme: 'Body-M', size:'14px', lineHeight: '1.5em'}), font({theme: 'Body-M', size: '14px', lineHeight: '1.42em'}))"}.WowImage249947393__errorMessage.Text1234506454--mobile.Text1234506454---typography-8-listText{font:"fallback(font({theme: 'Body-M', size:'14px', lineHeight: '1.5em'}), font({theme: 'Body-M', size: '14px', lineHeight: '1.72em'}))"}.WowImage249947393__errorMessage.Text1234506454--mobile.Text1234506454---typography-10-largeTitle{font:"fallback(font({theme: 'Body-M', size:'14px', lineHeight: '1.5em'}), font({theme: 'Heading-M', size: '24px', lineHeight: '1.33em'}))"}.WowImage249947393__errorMessage .Text1234506454__sr-only,.WowImage249947393__srError{border:0!important;clip:rect(1px,1px,1px,1px)!important;-webkit-clip-path:inset(50%)!important;clip-path:inset(50%)!important;height:1px!important;margin:-1px!important;overflow:hidden!important;padding:0!important;position:absolute!important;width:1px!important;white-space:nowrap!important}.WowImage249947393__errorIcon{color:"color(color-1)"}.WowImage249947393__spinnerOverlay{position:absolute;top:0;left:0;width:100%;height:100%;display:none;background-color:rgba(0,0,0,.6)}.WowImage249947393__root.WowImage249947393--loadSpinner:not(.WowImage249947393--loaded) .WowImage249947393__spinnerOverlay{display:block}.WowImage249947393__spinner .Spinner270281470__circle{stroke:#fff}.ZKMes{display:block;width:100%;height:100%}.ZKMes[data-animate-blur] img{filter:blur(9px);transition:filter .8s ease-in}.ZKMes[data-animate-blur] img[data-load-done]{filter:none}.Text1234506454__root{--Text1234506454-primary-color:"color(fallback(--overridable, color-5))";--Text1234506454-secondary-color:"color(fallback(--overridable, color-4))"}.Text1234506454__root.Text1234506454---priority-7-primary{color:var(--wut-text-color,var(--Text1234506454-primary-color))}.Text1234506454__root.Text1234506454---priority-9-secondary{color:var(--wut-placeholder-color,var(--Text1234506454-secondary-color))}.Text1234506454__root.Text1234506454---typography-10-smallTitle{font:"fallback(font(--overridable), font({theme: 'Page-title', size: '24px', lineHeight: '1.33em'}))"}.Text1234506454__root.Text1234506454---typography-11-runningText{font:"fallback(font(--overridable), font({theme: 'Body-M', size: '16px', lineHeight: '1.5em'}))"}.Text1234506454__root.Text1234506454---typography-8-listText{font:"fallback(font(--overridable), font({theme: 'Body-M', size: '16px', lineHeight: '2em'}))"}.Text1234506454__root.Text1234506454---typography-10-largeTitle{font:"fallback(font(--overridable), font({theme: 'Heading-M', size: '32px', lineHeight: '1.25em'}))"}.Text1234506454__root.Text1234506454--mobile.Text1234506454---typography-10-smallTitle{font:"fallback(font(--overridable), font({theme: 'Page-title', size: '20px', lineHeight: '1.4em'}))"}.Text1234506454__root.Text1234506454--mobile.Text1234506454---typography-11-runningText{font:"fallback(font(--overridable), font({theme: 'Body-M', size: '14px', lineHeight: '1.42em'}))"}.Text1234506454__root.Text1234506454--mobile.Text1234506454---typography-8-listText{font:"fallback(font(--overridable), font({theme: 'Body-M', size: '14px', lineHeight: '1.72em'}))"}.Text1234506454__root.Text1234506454--mobile.Text1234506454---typography-10-largeTitle{font:"fallback(font(--overridable), font({theme: 'Heading-M', size: '24px', lineHeight: '1.33em'}))"}.Text1234506454__sr-only{border:0!important;clip:rect(1px,1px,1px,1px)!important;-webkit-clip-path:inset(50%)!important;clip-path:inset(50%)!important;height:1px!important;margin:-1px!important;overflow:hidden!important;padding:0!important;position:absolute!important;width:1px!important;white-space:nowrap!important}.Spinner270281470__root{-webkit-animation:Spinner270281470__rotate 2s linear infinite;animation:Spinner270281470__rotate 2s linear infinite}.Spinner270281470__circle{stroke:"color(fallback(--overridable, color(color-5)))";-webkit-animation:Spinner270281470__dash 1.5s ease-in-out infinite;animation:Spinner270281470__dash 1.5s ease-in-out infinite}.Spinner270281470__root.Spinner270281470--centered{position:absolute}.Spinner270281470__root.Spinner270281470--static,.Spinner270281470__root.Spinner270281470--static .Spinner270281470__circle{-webkit-animation:none;animation:none}@-webkit-keyframes Spinner270281470__rotate{to{transform:rotate(1turn)}}@keyframes Spinner270281470__rotate{to{transform:rotate(1turn)}}@-webkit-keyframes Spinner270281470__dash{0%{stroke-dasharray:1,150;stroke-dashoffset:0}50%{stroke-dasharray:90,150;stroke-dashoffset:-35}to{stroke-dasharray:90,150;stroke-dashoffset:-124}}@keyframes Spinner270281470__dash{0%{stroke-dasharray:1,150;stroke-dashoffset:0}50%{stroke-dasharray:90,150;stroke-dashoffset:-35}to{stroke-dasharray:90,150;stroke-dashoffset:-124}}.Avatar1539094544__root{background-color:#d8d8d8}.Avatar1539094544__root .Avatarbase142013732__content{color:#979797;font-family:HelveticaNeueW01-65Medi,Arial,"sans-serif";font-weight:500}.Avatar1539094544__root.Avatar1539094544---size-6-xLarge{font-size:36px;height:60px;width:60px}.Avatar1539094544__root.Avatar1539094544---size-5-large{font-size:32px;height:52px;width:52px}.Avatar1539094544__root.Avatar1539094544---size-6-medium{font-size:20px;height:36px;width:36px}.Avatar1539094544__root.Avatar1539094544---size-5-small{font-size:16px;height:28px;width:28px}.Avatar1539094544__root.Avatar1539094544---size-6-xSmall{font-size:12px;height:20px;width:20px}.Avatar1539094544__root.Avatar1539094544---size-7-xxSmall{font-size:8px;height:16px;width:16px}.J1I8M{opacity:0;cursor:pointer;width:100%;height:100%;left:0;font-size:var(--fnt-size-dd,15px)}.J1I8M,.Z4bA1{position:absolute}.Z4bA1{display:none;top:calc(100% + var(--brw, 0px));padding:10px 0;z-index:99999;background-color:rgba(var(--bg-dd,var(--color_11)),var(--alpha-bg-dd,1));border:var(--brw-dd,1px) solid rgba(var(--brd-dd,var(--color_15)),var(--alpha-brd-dd,1));box-sizing:border-box;border-radius:var(--rd-dd,0);box-shadow:var(--shd-dd,0 0 0 transparent);font:var(--fnt,var(--font_8));font-size:var(--fnt-size-dd,15px);min-width:100px;max-width:300px}.Z4bA1._29X3z{display:block}.Z4bA1 hr{margin:5px 20px;opacity:.4}._1489R{color:rgb(var(--txt-dd,var(--color_15)));cursor:pointer;padding:0 20px;display:flex;line-height:260%;border-radius:var(--rd-dd,0)}._1489R._22Uzt{color:rgb(var(--txt-slct-dd,var(--color_19)))}._1489R:hover{color:rgb(var(--txth-dd,var(--color_19)))}._1489R .anaUy{overflow:hidden;text-overflow:ellipsis;white-space:nowrap}._1489R ._3huLT{opacity:.6}._2Lgi0{right:14px}._2Lgi0 ._3huLT{padding-left:12px}._1N-WE{left:14px}._1N-WE ._3huLT{padding-right:12px}._3rfhC ._3huLT{padding-left:12px}._3rfhC ._1HMCm{direction:ltr}._2tw-X ._3huLT{padding-right:12px}._2tw-X ._1HMCm{direction:rtl}._2POEy ._3huLT{padding-left:12px}._2POEy ._1489R{justify-content:center}._3omZ_{border-radius:var(--rd,0);font:var(--fnt,var(--font_8));cursor:pointer;display:flex;align-items:center;white-space:nowrap;padding:6px 7px;position:relative;min-width:0;color:rgb(var(--txt,var(--color_18)))}._3omZ_ ._1UDJF{padding-left:7px;padding-right:7px}._3omZ_:hover ._1UDJF{opacity:.7}
    </style>
    <style data-href="https://static.parastorage.com/services/editor-elements/dist/rb_wixui.thunderbolt[TextAreaInput].2b8b066f.min.css">
        .bItEI ._1VWbH{-webkit-appearance:none;box-shadow:var(--shd,0 0 0 transparent);border-radius:var(--corvid-border-radius,var(--rd,0));font:var(--fnt,var(--font_8));border-width:var(--corvid-border-width,var(--brw,1px));resize:none;background-color:var(--corvid-background-color,rgba(var(--bg,255,255,255),var(--alpha-bg,1)));box-sizing:border-box!important;color:var(--corvid-color,rgb(var(--txt,var(--color_15))));border-style:solid;border-color:var(--corvid-border-color,rgba(var(--brd,227,227,227),var(--alpha-brd,1)));padding:var(--textPadding,3px);margin:0;padding-top:.75em;max-width:100%;min-width:100%;overflow-y:auto;text-align:var(--textAlign);direction:var(--dir);height:var(--inputHeight);display:block}.bItEI ._1VWbH::-moz-placeholder{color:rgb(var(--txt2,var(--color_15)))}.bItEI ._1VWbH:-ms-input-placeholder{color:rgb(var(--txt2,var(--color_15)))}.bItEI ._1VWbH::placeholder{color:rgb(var(--txt2,var(--color_15)))}.bItEI ._1VWbH:hover{border-width:var(--brwh,1px);background-color:rgba(var(--bgh,255,255,255),var(--alpha-bgh,1));border-style:solid;border-color:rgba(var(--brdh,163,217,246),var(--alpha-brdh,1))}.bItEI ._1VWbH:disabled{background-color:rgba(var(--bgd,204,204,204),var(--alpha-bgd,1));color:rgb(var(--txtd,255,255,255));border-width:var(--brwd,1px);border-style:solid;border-color:rgba(var(--brdd,163,217,246),var(--alpha-brdd,1))}.bItEI:not(.gPrpT) ._1VWbH:focus{border-width:var(--brwf,1px);background-color:rgba(var(--bgf,255,255,255),var(--alpha-bgf,1));border-style:solid;border-color:rgba(var(--brdf,163,217,246),var(--alpha-brdf,1))}.bItEI.gPrpT ._1VWbH:invalid{border-width:var(--brwe,1px);background-color:rgba(var(--bge,255,255,255),var(--alpha-bge,1));border-style:solid;border-color:rgba(var(--brde,163,217,246),var(--alpha-brde,1))}.bItEI.gPrpT ._1VWbH:not(:invalid):focus{border-width:var(--brwf,1px);background-color:rgba(var(--bgf,255,255,255),var(--alpha-bgf,1));border-style:solid;border-color:rgba(var(--brdf,163,217,246),var(--alpha-brdf,1))}.bItEI ._20uhs{display:none}.bItEI._1mQNr ._20uhs{font:var(--fntlbl,var(--font_8));color:rgb(var(--txtlbl,var(--color_15)));word-break:break-word;display:inline-block;line-height:1;margin-bottom:var(--labelMarginBottom,14px);padding:var(--labelPadding,0);text-align:var(--textAlign);direction:var(--dir);width:100%;box-sizing:border-box}.bItEI._1mQNr._22Vl0 ._20uhs:after{display:var(--requiredIndicationDisplay,none);content:" *";color:rgba(var(--txtlblrq,0,0,0),var(--alpha-txtlblrq,0))}._1kepl{display:var(--display);--display:flex;flex-direction:column}._1kepl ._1VWbH{flex:1;-webkit-appearance:none;box-shadow:var(--shd,0 0 0 transparent);border-radius:var(--corvid-border-radius,var(--rd,0));font:var(--fnt,var(--font_8));border-width:var(--corvid-border-width,var(--brw,1px));resize:none;background-color:var(--corvid-background-color,rgba(var(--bg,255,255,255),var(--alpha-bg,1)));box-sizing:border-box!important;color:var(--corvid-color,rgb(var(--txt,var(--color_15))));border-style:solid;border-color:var(--corvid-border-color,rgba(var(--brd,227,227,227),var(--alpha-brd,1)));padding:var(--textPadding,3px);margin:0;overflow-y:auto;width:100%;height:100%;text-align:var(--textAlign);direction:var(--dir)}._1kepl ._1VWbH::-moz-placeholder{color:rgb(var(--txt2,var(--color_15)))}._1kepl ._1VWbH:-ms-input-placeholder{color:rgb(var(--txt2,var(--color_15)))}._1kepl ._1VWbH::placeholder{color:rgb(var(--txt2,var(--color_15)))}._1kepl ._1VWbH:hover{border-width:var(--brwh,1px);background-color:rgba(var(--bgh,255,255,255),var(--alpha-bgh,1));border-style:solid;border-color:rgba(var(--brdh,163,217,246),var(--alpha-brdh,1))}._1kepl ._1VWbH:disabled{background-color:rgba(var(--bgd,204,204,204),var(--alpha-bgd,1));color:rgb(var(--txtd,255,255,255));border-width:var(--brwd,1px);border-style:solid;border-color:rgba(var(--brdd,163,217,246),var(--alpha-brdd,1))}._1kepl:not(.gPrpT) ._1VWbH:focus{border-width:var(--brwf,1px);background-color:rgba(var(--bgf,255,255,255),var(--alpha-bgf,1));border-style:solid;border-color:rgba(var(--brdf,163,217,246),var(--alpha-brdf,1))}._1kepl.gPrpT ._1VWbH:invalid{border-width:var(--brwe,1px);background-color:rgba(var(--bge,255,255,255),var(--alpha-bge,1));border-style:solid;border-color:rgba(var(--brde,163,217,246),var(--alpha-brde,1))}._1kepl.gPrpT ._1VWbH:not(:invalid):focus{border-width:var(--brwf,1px);background-color:rgba(var(--bgf,255,255,255),var(--alpha-bgf,1));border-style:solid;border-color:rgba(var(--brdf,163,217,246),var(--alpha-brdf,1))}._1kepl ._20uhs{display:none}._1kepl._1mQNr ._20uhs{font:var(--fntlbl,var(--font_8));color:rgb(var(--txtlbl,var(--color_15)));word-break:break-word;display:inline-block;line-height:1;margin-bottom:var(--labelMarginBottom,14px);padding:var(--labelPadding,0);text-align:var(--textAlign);direction:var(--dir);width:100%;box-sizing:border-box}._1kepl._1mQNr._22Vl0 ._20uhs:after{display:var(--requiredIndicationDisplay,none);content:" *";color:rgba(var(--txtlblrq,0,0,0),var(--alpha-txtlblrq,0))}
    </style>
    <style data-href="https://static.parastorage.com/services/editor-elements/dist/rb_wixui.thunderbolt[Container_DefaultAreaSkin].b604fd33.min.css">
        ._1ncY2{--container-corvid-border-color:rgba(var(--brd,var(--color_15)),var(--alpha-brd,1));--container-corvid-border-size:var(--brw,1px);--container-corvid-background-color:rgba(var(--bg,var(--color_11)),var(--alpha-bg,1))}._13Lxq{border:var(--container-corvid-border-width,var(--brw,1px)) solid var(--container-corvid-border-color,rgba(var(--brd,var(--color_15)),var(--alpha-brd,1)));background-color:var(--container-corvid-background-color,rgba(var(--bg,var(--color_11)),var(--alpha-bg,1)));border-radius:var(--rd,5px);box-shadow:var(--shd,0 1px 4px rgba(0,0,0,.6));position:absolute;top:0;right:0;bottom:0;left:0}
    </style>
    <style data-href="https://static.parastorage.com/services/editor-elements/dist/rb_wixui.thunderbolt[AppWidget_Classic].eec152dc.min.css">
        @-moz-document url-prefix(){:invalid{box-shadow:none}:-moz-submit-invalid,:-moz-ui-invalid{box-shadow:none}}@-webkit-keyframes _1vrB2{0%{transform:rotate(180deg);-webkit-animation-timing-function:ease-out;animation-timing-function:ease-out}45%{transform:rotate(198deg)}55%{transform:rotate(234deg)}to{transform:rotate(540deg)}}@keyframes _1vrB2{0%{transform:rotate(180deg);-webkit-animation-timing-function:ease-out;animation-timing-function:ease-out}45%{transform:rotate(198deg)}55%{transform:rotate(234deg)}to{transform:rotate(540deg)}}@-webkit-keyframes mbnqu{to{transform:rotate(115deg);opacity:1}}@keyframes mbnqu{to{transform:rotate(115deg);opacity:1}}._11gHK._1nr5o{display:var(--display);--display:flex;align-items:center;justify-content:center}._1fiKf{width:36px;height:72px;margin-left:-18px;position:absolute;overflow:hidden;transform-origin:100% 50%;-webkit-animation:_1vrB2 1s linear infinite;animation:_1vrB2 1s linear infinite}._1fiKf:after,._1fiKf:before{content:"";top:0;left:0;right:-100%;bottom:0;border:3px solid currentColor;border-color:currentColor transparent transparent currentColor;border-radius:50%;position:absolute;transform:rotate(-45deg);-webkit-animation:mbnqu .5s linear infinite alternate;animation:mbnqu .5s linear infinite alternate}._1fiKf:before{color:#7fccf7}._1fiKf:after{color:#3899ec;opacity:0}
    </style>
    <style data-href="https://static.parastorage.com/services/editor-elements/dist/rb_wixui.thunderbolt[FormContainer_FormContainerSkin].7c602eb2.min.css">
        ._2BVIx,.yBJuM{box-shadow:var(--shd,0 0 0 transparent);border-radius:var(--corvid-border-radius,var(--rd,0));background-color:var(--corvid-background-color,rgba(var(--bg,0,0,0),var(--alpha-bg,0)));border:solid var(--corvid-border-color,rgba(var(--brd,227,227,227),var(--alpha-brd,1))) var(--corvid-border-width,var(--brw,0))}._3_X_x{position:absolute;width:100%;height:100%;left:-var(--corvid-border-width,var(--brw,0));top:-var(--corvid-border-width,var(--brw,0))}
    </style>
    <style data-href="https://static.parastorage.com/services/editor-elements/dist/rb_wixui.thunderbolt[FreemiumBannerDesktop].4da3e2b2.min.css">
        @font-face{font-display:swap;font-family:wixFreemiumFontW01-35Thin;src:url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/56be84de-9d60-4089-8df0-0ea6ec786b84.eot#iefix);src:url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/56be84de-9d60-4089-8df0-0ea6ec786b84.eot#iefix) format("eot"),url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/50d35bbc-dfd4-48f1-af16-cf058f69421d.woff) format("woff"),url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/278bef59-6be1-4800-b5ac-1f769ab47430.ttf) format("truetype"),url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/2e309b1b-08b8-477f-bc9e-7067cf0af0b3.svg#2e309b1b-08b8-477f-bc9e-7067cf0af0b3) format("svg")}@font-face{font-display:swap;font-family:wixFreemiumFontW01-45Ligh;src:url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/ae1656aa-5f8f-4905-aed0-93e667bd6e4a.eot#iefix);src:url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/ae1656aa-5f8f-4905-aed0-93e667bd6e4a.eot#iefix) format("eot"),url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/530dee22-e3c1-4e9f-bf62-c31d510d9656.woff) format("woff"),url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/688ab72b-4deb-4e15-a088-89166978d469.ttf) format("truetype"),url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/7816f72f-f47e-4715-8cd7-960e3723846a.svg#7816f72f-f47e-4715-8cd7-960e3723846a) format("svg")}@font-face{font-display:swap;font-family:wixFreemiumFontW01-55Roma;src:url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/b7693a83-b861-4aa6-85e0-9ecf676bc4d6.eot#iefix);src:url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/b7693a83-b861-4aa6-85e0-9ecf676bc4d6.eot#iefix) format("eot"),url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/bcf54343-d033-41ee-bbd7-2b77df3fe7ba.woff) format("woff"),url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/b0ffdcf0-26da-47fd-8485-20e4a40d4b7d.ttf) format("truetype"),url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/da09f1f1-062a-45af-86e1-2bbdb3dd94f9.svg#da09f1f1-062a-45af-86e1-2bbdb3dd94f9) format("svg")}@font-face{font-display:swap;font-family:wixFreemiumFontW01-65Medi;font-weight:700;src:url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/07fe0fec-b63f-4963-8ee1-535528b67fdb.eot#iefix);src:url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/07fe0fec-b63f-4963-8ee1-535528b67fdb.eot#iefix) format("eot"),url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/60be5c39-863e-40cb-9434-6ebafb62ab2b.woff) format("woff"),url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/4c6503c9-859b-4d3b-a1d5-2d42e1222415.ttf) format("truetype"),url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/36c182c6-ef98-4021-9b0d-d63122c2bbf5.svg#36c182c6-ef98-4021-9b0d-d63122c2bbf5) format("svg")}@font-face{font-display:swap;font-family:wixFreemiumFontW02-35Thin;src:url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/30b6ffc3-3b64-40dd-9ff8-a3a850daf535.eot#iefix);src:url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/30b6ffc3-3b64-40dd-9ff8-a3a850daf535.eot#iefix) format("eot"),url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/775a65da-14aa-4634-be95-6724c05fd522.woff) format("woff"),url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/988eaaa7-5565-4f65-bb17-146b650ce9e9.ttf) format("truetype"),url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/3503a1a6-91c3-4c42-8e66-2ea7b2b57541.svg#3503a1a6-91c3-4c42-8e66-2ea7b2b57541) format("svg")}@font-face{font-display:swap;font-family:wixFreemiumFontW02-45Ligh;src:url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/88fcd49a-13c7-4d0c-86b1-ad1e258bd75d.eot#iefix);src:url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/88fcd49a-13c7-4d0c-86b1-ad1e258bd75d.eot#iefix) format("eot"),url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/9a2e4855-380f-477f-950e-d98e8db54eac.woff) format("woff"),url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/fa82d0ee-4fbd-4cc9-bf9f-226ad1fcbae2.ttf) format("truetype"),url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/48d599a6-92b5-4d43-a4ac-8959f6971853.svg#48d599a6-92b5-4d43-a4ac-8959f6971853) format("svg")}@font-face{font-display:swap;font-family:wixFreemiumFontW02-55Roma;src:url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/0b3a3fca-0fad-402b-bd38-fdcbad1ef776.eot#iefix);src:url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/0b3a3fca-0fad-402b-bd38-fdcbad1ef776.eot#iefix) format("eot"),url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/d5af76d8-a90b-4527-b3a3-182207cc3250.woff) format("woff"),url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/1d238354-d156-4dde-89ea-4770ef04b9f9.ttf) format("truetype"),url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/b68875cb-14a9-472e-8177-0247605124d7.svg#b68875cb-14a9-472e-8177-0247605124d7) format("svg")}@font-face{font-display:swap;font-family:wixFreemiumFontW02-65Medi;font-weight:700;src:url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/55f60419-09c3-42bd-b81f-1983ff093852.eot#iefix);src:url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/55f60419-09c3-42bd-b81f-1983ff093852.eot#iefix) format("eot"),url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/5b4a262e-3342-44e2-8ad7-719998a68134.woff) format("woff"),url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/4a3ef5d8-cfd9-4b96-bd67-90215512f1e5.ttf) format("truetype"),url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/58ab5075-53ea-46e6-9783-cbb335665f88.svg#58ab5075-53ea-46e6-9783-cbb335665f88) format("svg")}@font-face{font-display:swap;font-family:wixFreemiumFontW10-35Thin;src:url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/93b6bf6a-418e-4a8f-8f79-cb9c99ef3e32.eot#iefix);src:url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/93b6bf6a-418e-4a8f-8f79-cb9c99ef3e32.eot#iefix) format("eot"),url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/c881c21b-4148-4a11-a65d-f35e42999bc8.woff) format("woff"),url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/03634cf1-a9c9-4e13-b049-c90d830423d4.ttf) format("truetype"),url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/1bc99c0a-298b-46f9-b325-18b5e5169795.svg#1bc99c0a-298b-46f9-b325-18b5e5169795) format("svg")}@font-face{font-display:swap;font-family:wixFreemiumFontW10-45Ligh;src:url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/5b85c7cc-6ad4-4226-83f5-9d19e2974123.eot#iefix);src:url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/5b85c7cc-6ad4-4226-83f5-9d19e2974123.eot#iefix) format("eot"),url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/835e7b4f-b524-4374-b57b-9a8fc555fd4e.woff) format("woff"),url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/2c694ef6-9615-473e-8cf4-d8d00c6bd973.ttf) format("truetype"),url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/3fc84193-a13f-4fe8-87f7-238748a4ac54.svg#3fc84193-a13f-4fe8-87f7-238748a4ac54) format("svg")}@font-face{font-display:swap;font-family:wixFreemiumFontW10-65Medi;font-weight:700;src:url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/7092fdcc-7036-48ce-ae23-cfbc9be2e3b0.eot#iefix);src:url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/7092fdcc-7036-48ce-ae23-cfbc9be2e3b0.eot#iefix) format("eot"),url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/5b29e833-1b7a-40ab-82a5-cfd69c8650f4.woff) format("woff"),url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/b0298148-2d59-44d1-9ec9-1ca6bb097603.ttf) format("truetype"),url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/ae1dea8c-a953-4845-b616-74a257ba72e6.svg#ae1dea8c-a953-4845-b616-74a257ba72e6) format("svg")}@font-face{font-display:swap;font-family:wixFreemiumFontW10-55Roma;src:url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/f1feaed7-6bce-400a-a07e-a893ae43a680.eot#iefix);src:url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/f1feaed7-6bce-400a-a07e-a893ae43a680.eot#iefix) format("eot"),url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/8ac9e38d-29c6-41ea-8e47-4ae4d2b1a4e1.woff) format("woff"),url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/4bd09087-655e-4abb-844c-dccdeb68003d.ttf) format("truetype"),url(//static.parastorage.com/services/third-party/fonts/Helvetica/Fonts/df234d87-eada-4058-aa80-5871e7fbe1c3.svg#df234d87-eada-4058-aa80-5871e7fbe1c3) format("svg")}.KxUm9{width:100%}.KxUm9._1CbU3{display:var(--display);--display:none}.KxUm9._1Oe2B{display:block;visibility:visible}.KxUm9 ._3jDdZ{direction:rtl}.KxUm9 ._4Ue8X{direction:ltr}.KxUm9._3CXj5{z-index:var(--above-all-z-index);position:fixed;top:0}.KxUm9 .BNB_2{box-sizing:border-box;background:#eff1f2;display:flex;justify-content:center;align-items:center;border-bottom:3px solid #a0138e;width:100%;height:50px}.KxUm9 .BNB_2._1186i{background-color:red;border:none}.KxUm9 .BNB_2:focus{box-shadow:0 0 0 1px #fff,0 0 0 3px #116dff,inset -2px 2px 0 0 #116dff,inset 2px 2px 0 0 #116dff,inset 0 3px 0 0 #fff!important}.KxUm9 .BNB_2>._1oNmL{display:flex;align-items:center}.KxUm9 .BNB_2>._1oNmL ._3lBpL{font-family:wixFreemiumFontW01-65Medi,wixFreemiumFontW02-65Medi,wixFreemiumFontW10-65Medi,Helvetica Neue,Helvetica,Arial,メイリオ,meiryo,ヒラギノ角ゴ pro w3,hiragino kaku gothic pro,sans-serif;font-size:14px;color:#20303c;line-height:24px;flex-shrink:0}.KxUm9 .BNB_2>._1oNmL ._3lBpL ._2GtTS{fill:#20303c;width:36px;vertical-align:middle;padding-bottom:6px;height:16px}.KxUm9 .BNB_2>._1oNmL ._3lBpL ._2GtTS>.zAzVm{fill:#fc0}.KxUm9 .BNB_2>._1oNmL ._3lBpL ._22hH6{color:#20303c}.KxUm9 .BNB_2>._1oNmL ._3ITII{font-family:wixFreemiumFontW01-65Medi,wixFreemiumFontW02-65Medi,wixFreemiumFontW10-65Medi,Helvetica Neue,Helvetica,Arial,メイリオ,meiryo,ヒラギノ角ゴ pro w3,hiragino kaku gothic pro,sans-serif;font-size:14px;color:#a0138e;border:1px solid #a0138e;display:inline-flex;width:112px;height:35px;justify-content:center;align-items:center;border-radius:17px;flex-shrink:0;text-align:center}.KxUm9 .BNB_2>._1oNmL ._3ITII._4Ue8X{margin-left:6px}.KxUm9 .BNB_2>._1oNmL ._3ITII._3jDdZ{margin-right:6px}.KxUm9 .BNB_2:not(._1186i):hover{cursor:pointer;background:#fff}.KxUm9 .BNB_2:not(._1186i):hover ._3ITII{color:#fff;background-color:#a0138e}
    </style>
    <style data-href="https://static.parastorage.com/services/editor-elements/dist/rb_wixui.thunderbolt[SkipToContentButton].99a6a99f.min.css">
        ._2SeL5{position:absolute;top:60px;left:50%;pointer-events:none;margin-left:-94px;z-index:9999;background:#fff;color:#116dff;font-family:Helvetica,Arial,メイリオ,meiryo,ヒラギノ角ゴ pro w3,hiragino kaku gothic pro,sans-serif;font-size:14px;border-radius:24px;padding:0 24px 0 24px;width:0;height:0;opacity:0;cursor:pointer}._2SeL5:focus{border:2px solid;opacity:1;width:auto;height:40px;pointer-events:auto}</style>
    <style data-href="https://static.parastorage.com/services/editor-elements/dist/rb_wixui.thunderbolt[FiveGridLine_SolidLine].f7e8d006.min.css">._2UdPt{box-sizing:border-box;border-top:var(--lnw,2px) solid rgba(var(--brd,var(--color_15)),var(--alpha-brd,1));height:0}</style>
    <style data-href="https://static.parastorage.com/services/editor-elements/dist/rb_wixui.thunderbolt[SlideShowContainer].62a3290c.min.css">
        ._3Ebtn._2Jgim,._3OF8h._2Jgim{display:var(--display);--display:grid;background-color:transparent;box-sizing:border-box;position:relative;min-height:50px}._3Ebtn._2Jgim ._3VTyv,._3OF8h._2Jgim ._3VTyv{display:var(--nav-button-display);position:absolute;z-index:1;cursor:pointer;width:var(--nav-button-width);grid-area:1/1/1/1;align-self:center;margin:0 var(--nav-button-offset)}._3Ebtn._2Jgim ._3VTyv.DdGao,._3OF8h._2Jgim ._3VTyv.DdGao{justify-self:start}._3Ebtn._2Jgim ._3VTyv._2D94o,._3OF8h._2Jgim ._3VTyv._2D94o{justify-self:end}._3Ebtn._2Jgim ._9R56T,._3OF8h._2Jgim ._9R56T{grid-area:1/1/1/1;position:absolute;width:100%;height:100%;overflow:var(--slides-overflow)}._3Ebtn._2Jgim ._9R56T._1DL_h,._3OF8h._2Jgim ._9R56T._1DL_h{overflow:var(--transition-overflow,var(--slides-overflow))}._3Ebtn._2Jgim ._9R56T>*,._3OF8h._2Jgim ._9R56T>*{overflow:var(--slides-overflow)}._3Ebtn._2Jgim ._2WI33._9R56T,._3OF8h._2Jgim ._2WI33._9R56T{position:relative;height:auto}._3Ebtn._2Jgim ._2WI33._9R56T>*,._3OF8h._2Jgim ._2WI33._9R56T>*{top:0}._3Ebtn._2Jgim ._2WI33._9R56T>:not(:first-child),._3OF8h._2Jgim ._2WI33._9R56T>:not(:first-child){position:absolute!important}._3Ebtn._2Jgim ._2MKmI,._3OF8h._2Jgim ._2MKmI{position:absolute;top:0;right:0;bottom:0;left:0;pointer-events:none;box-shadow:var(--shd,0 0 0 rgba(0,0,0,.6));border-radius:var(--rd,0)}._3Ebtn._2Jgim ._281t-,._3OF8h._2Jgim ._281t-{display:var(--nav-dot-section-display);grid-area:1/1/1/1;align-self:end;position:absolute;justify-self:center;margin-bottom:var(--nav-dot-section-bottom-margin);transform:translateY(50%)}._3Ebtn._2Jgim ._281t- ._2UmXg,._3OF8h._2Jgim ._281t- ._2UmXg{display:flex;align-items:center;justify-content:center;position:relative;width:100%;pointer-events:none}._3Ebtn._2Jgim ._281t- ._3clGI,._3OF8h._2Jgim ._281t- ._3clGI{position:relative;display:block;box-sizing:border-box;cursor:pointer;pointer-events:auto;margin:0 var(--nav-dot-margin);width:var(--nav-dot-size);height:var(--nav-dot-size)}._3Ebtn._2Jgim ._281t- ._3clGI._2HGJH,._3OF8h._2Jgim ._281t- ._3clGI._2HGJH{width:var(--nav-dot-size-selected);height:var(--nav-dot-size-selected)}._3Ebtn ._3VTyv:hover,._3OF8h ._3VTyv:hover{opacity:.6}._3Ebtn ._3VTyv._2D94o,._3OF8h ._3VTyv._2D94o{transform:scaleX(-1)}._3Ebtn ._3VTyv svg,._3OF8h ._3VTyv svg{fill:rgba(var(--arrowColor,var(--color_12)),var(--alpha-arrowColor,1));stroke:rgba(var(--arrowColor,var(--color_12)),var(--alpha-arrowColor,1));stroke-width:1px}._3OF8h ._3clGI{border-radius:50%;background-color:rgba(var(--dotsColor,var(--color_12)),var(--alpha-dotsColor,1))}._3OF8h ._3clGI._2HGJH{background-color:rgba(var(--dotsSelectedColor,var(--color_11)),var(--alpha-dotsSelectedColor,1))}._3Ebtn ._3clGI{border-radius:50%;background-color:rgba(var(--dotsColor,var(--color_12)),var(--alpha-dotsColor,1))}._3Ebtn ._3clGI._2HGJH{background-color:transparent;border:2px solid rgba(var(--dotsColor,var(--color_12)),var(--alpha-dotsColor,1))}._1nLKN._2Jgim{display:var(--display);--display:grid;background-color:transparent;box-sizing:border-box;position:relative;min-height:50px}._1nLKN._2Jgim ._3VTyv{display:var(--nav-button-display);position:absolute;z-index:1;cursor:pointer;width:var(--nav-button-width);grid-area:1/1/1/1;align-self:center;margin:0 var(--nav-button-offset)}._1nLKN._2Jgim ._3VTyv.DdGao{justify-self:start}._1nLKN._2Jgim ._3VTyv._2D94o{justify-self:end}._1nLKN._2Jgim ._9R56T{grid-area:1/1/1/1;position:absolute;width:100%;height:100%;overflow:var(--slides-overflow)}._1nLKN._2Jgim ._9R56T._1DL_h{overflow:var(--transition-overflow,var(--slides-overflow))}._1nLKN._2Jgim ._9R56T>*{overflow:var(--slides-overflow)}._1nLKN._2Jgim ._2WI33._9R56T{position:relative;height:auto}._1nLKN._2Jgim ._2WI33._9R56T>*{top:0}._1nLKN._2Jgim ._2WI33._9R56T>:not(:first-child){position:absolute!important}._1nLKN._2Jgim ._2MKmI{position:absolute;top:0;right:0;bottom:0;left:0;pointer-events:none;box-shadow:var(--shd,0 0 0 rgba(0,0,0,.6));border-radius:var(--rd,0)}._1nLKN._2Jgim ._281t-{display:var(--nav-dot-section-display);grid-area:1/1/1/1;align-self:end;position:absolute;justify-self:center;margin-bottom:var(--nav-dot-section-bottom-margin);transform:translateY(50%)}._1nLKN._2Jgim ._281t- ._2UmXg{display:flex;align-items:center;justify-content:center;position:relative;width:100%;pointer-events:none}._1nLKN._2Jgim ._281t- ._3clGI{position:relative;display:block;box-sizing:border-box;cursor:pointer;pointer-events:auto;margin:0 var(--nav-dot-margin);width:var(--nav-dot-size);height:var(--nav-dot-size)}._1nLKN._2Jgim ._281t- ._3clGI._2HGJH{width:var(--nav-dot-size-selected);height:var(--nav-dot-size-selected)}._1nLKN ._3VTyv:hover{opacity:.6}._1nLKN ._3VTyv.DdGao{transform:scaleX(-1)}._1nLKN ._3VTyv svg{fill:rgba(var(--arrowColor,var(--color_12)),var(--alpha-arrowColor,1))}._1nLKN ._3clGI{border-radius:50%;background-color:rgba(var(--dotsColor,var(--color_12)),var(--alpha-dotsColor,1))}._1nLKN ._3clGI._2HGJH{background-color:transparent;border:2px solid rgba(var(--dotsColor,var(--color_12)),var(--alpha-dotsColor,1))}._1959y._2Jgim{display:var(--display);--display:grid;background-color:transparent;box-sizing:border-box;position:relative;min-height:50px}._1959y._2Jgim ._3VTyv{display:var(--nav-button-display);position:absolute;z-index:1;cursor:pointer;width:var(--nav-button-width);grid-area:1/1/1/1;align-self:center;margin:0 var(--nav-button-offset)}._1959y._2Jgim ._3VTyv.DdGao{justify-self:start}._1959y._2Jgim ._3VTyv._2D94o{justify-self:end}._1959y._2Jgim ._9R56T{grid-area:1/1/1/1;position:absolute;width:100%;height:100%;overflow:var(--slides-overflow)}._1959y._2Jgim ._9R56T._1DL_h{overflow:var(--transition-overflow,var(--slides-overflow))}._1959y._2Jgim ._9R56T>*{overflow:var(--slides-overflow)}._1959y._2Jgim ._2WI33._9R56T{position:relative;height:auto}._1959y._2Jgim ._2WI33._9R56T>*{top:0}._1959y._2Jgim ._2WI33._9R56T>:not(:first-child){position:absolute!important}._1959y._2Jgim ._2MKmI{position:absolute;top:0;right:0;bottom:0;left:0;pointer-events:none;box-shadow:var(--shd,0 0 0 rgba(0,0,0,.6));border-radius:var(--rd,0)}._1959y._2Jgim ._281t-{display:var(--nav-dot-section-display);grid-area:1/1/1/1;align-self:end;position:absolute;justify-self:center;margin-bottom:var(--nav-dot-section-bottom-margin);transform:translateY(50%)}._1959y._2Jgim ._281t- ._2UmXg{display:flex;align-items:center;justify-content:center;position:relative;width:100%;pointer-events:none}._1959y._2Jgim ._281t- ._3clGI{position:relative;display:block;box-sizing:border-box;cursor:pointer;pointer-events:auto;margin:0 var(--nav-dot-margin);width:var(--nav-dot-size);height:var(--nav-dot-size)}._1959y._2Jgim ._281t- ._3clGI._2HGJH{width:var(--nav-dot-size-selected);height:var(--nav-dot-size-selected)}._1959y ._3VTyv:hover{opacity:.6}._1959y ._3VTyv._2D94o{transform:scaleX(-1)}._1959y ._3VTyv svg path:first-child{fill:rgba(var(--arrowContainerColor,var(--color_11)),var(--alpha-arrowContainerColor,1))}._1959y ._3VTyv svg path:last-child{fill:rgba(var(--arrowColor,var(--color_12)),var(--alpha-arrowColor,1))}._1959y ._3clGI{background-color:rgba(var(--dotsColor,var(--color_12)),var(--alpha-dotsColor,1))}._1959y ._3clGI._2HGJH{background-color:rgba(var(--dotsSelectedColor,var(--color_11)),var(--alpha-dotsSelectedColor,1))}._2EiDF._2Jgim{display:var(--display);--display:grid;background-color:transparent;box-sizing:border-box;position:relative;min-height:50px}._2EiDF._2Jgim ._3VTyv{display:var(--nav-button-display);position:absolute;z-index:1;cursor:pointer;width:var(--nav-button-width);grid-area:1/1/1/1;align-self:center;margin:0 var(--nav-button-offset)}._2EiDF._2Jgim ._3VTyv.DdGao{justify-self:start}._2EiDF._2Jgim ._3VTyv._2D94o{justify-self:end}._2EiDF._2Jgim ._9R56T{grid-area:1/1/1/1;position:absolute;width:100%;height:100%;overflow:var(--slides-overflow)}._2EiDF._2Jgim ._9R56T._1DL_h{overflow:var(--transition-overflow,var(--slides-overflow))}._2EiDF._2Jgim ._9R56T>*{overflow:var(--slides-overflow)}._2EiDF._2Jgim ._2WI33._9R56T{position:relative;height:auto}._2EiDF._2Jgim ._2WI33._9R56T>*{top:0}._2EiDF._2Jgim ._2WI33._9R56T>:not(:first-child){position:absolute!important}._2EiDF._2Jgim ._2MKmI{position:absolute;top:0;right:0;bottom:0;left:0;pointer-events:none;box-shadow:var(--shd,0 0 0 rgba(0,0,0,.6));border-radius:var(--rd,0)}._2EiDF._2Jgim ._281t-{display:var(--nav-dot-section-display);grid-area:1/1/1/1;align-self:end;position:absolute;justify-self:center;margin-bottom:var(--nav-dot-section-bottom-margin);transform:translateY(50%)}._2EiDF._2Jgim ._281t- ._2UmXg{display:flex;align-items:center;justify-content:center;position:relative;width:100%;pointer-events:none}._2EiDF._2Jgim ._281t- ._3clGI{position:relative;display:block;box-sizing:border-box;cursor:pointer;pointer-events:auto;margin:0 var(--nav-dot-margin);width:var(--nav-dot-size);height:var(--nav-dot-size)}._2EiDF._2Jgim ._281t- ._3clGI._2HGJH{width:var(--nav-dot-size-selected);height:var(--nav-dot-size-selected)}._2EiDF._2Jgim{min-height:unset}
    </style>
    <style data-href="https://static.parastorage.com/services/editor-elements/dist/rb_wixui.thunderbolt[SlideShowSlide].1f15f937.min.css">._30khq ._1FJJ6{border-radius:var(--rd,0);will-change:var(--corners-overflow-fix-will-change,initial)}._30khq ._2uaqg{position:absolute;top:0;right:0;bottom:0;left:0;border:var(--brw,0) solid rgba(var(--brd,var(--color_11)),var(--alpha-brd,1));border-radius:var(--rd,0);pointer-events:none}</style>
    <style data-href="https://static.parastorage.com/services/wix-thunderbolt/dist/TPABaseComponent.fdd66901.chunk.min.css">._2JOHk,._49_rs{overflow:hidden}._49_rs{position:relative;width:100%;height:100%}._49_rs:-webkit-full-screen{min-height:auto!important}._49_rs:-ms-fullscreen{min-height:auto!important}._49_rs:fullscreen{min-height:auto!important}._1DEc4{visibility:hidden}</style>
    <style data-href="https://static.parastorage.com/services/wix-thunderbolt/dist/ooi.abdee6a1.chunk.min.css">._2PSyL{height:auto!important}</style>
    <title>Australia 601, 600 tourist visa assistance | Malaysia Agency that makes Visa and Job easier</title>
    <link rel="canonical" href="index.html">
    <meta property="og:title" content="Australia 601, 600 tourist visa assistance | Malaysia Agency that makes Visa and Job easier">
    <meta property="og:url" content="index.html">
    <meta property="og:site_name" content="My Site">
    <meta property="og:type" content="website">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Australia 601, 600 tourist visa assistance | Malaysia Agency that makes Visa and Job easier">
    <meta name="description" content="We help you submit your Australia 601, 600 visa applications as one stop solution">
    <meta name="keywords" content="Australia 601 Visa">
    <meta name="keywords" content="Australia 601 visa, Australia 600 visa, 601 visa application, 600 visa application, Australian visa assistance, visa application services, visa consultants">
    <meta name="keywords" content="Subclass 601 Visa">

    <script type="application/json" id="registry-components-manifests">
        {"libraries":[{"version":"2.0","host":"thunderbolt","namespace":"wixui","baseURL":"https://static.parastorage.com/services/editor-elements/dist/","model":["bae026a0.bundle.min.js","6080f6d4.min.css"],"assets":[["stylable-metadata","editor-elements-library.578d50cc5726c26f15daf8b2d018a1603dd3672a.metadata.json"]],"components":{"AppWidget_Classic":["af065e1e.bundle.min.js","eec152dc.min.css",[0,1]],"Container_DefaultAreaSkin":["a514c5de.bundle.min.js","b604fd33.min.css",[0,1,6,7]],"FiveGridLine_SolidLine":["c88b5af9.bundle.min.js","f7e8d006.min.css"],"FormContainer_FormContainerSkin":["abd5cf4a.bundle.min.js","7c602eb2.min.css",[0,1]],"FreemiumBannerDesktop":["0bb72b13.bundle.min.js","4da3e2b2.min.css",[0,1]],"LoginSocialBar":["0f5f289a.bundle.min.js","44cdbf02.min.css",[0,1,6,7]],"MeshGroup":["5d510b9c.bundle.min.js",[0,1]],"SkipToContentButton":["09136db3.bundle.min.js","99a6a99f.min.css",[0,1]],"SlideShowContainer":["ba24bc27.bundle.min.js","62a3290c.min.css",[0,1,6,7,4,5]],"SlideShowSlide":["6e181ac0.bundle.min.js","1f15f937.min.css",[0,1]],"TextAreaInput":["c8f3f5d0.bundle.min.js","2b8b066f.min.css",[0,1]],"TextInput":["6b388b9c.bundle.min.js",[0,1,4,5]],"VerticalMenu_VerticalMenuSolidColorSkin":["b731a961.bundle.min.js","34b72cf9.min.css",[0,1]]},"batches":{"bootstrap":{"url":["a60fe588.bundle.min.js",[0,1,6,7]],"components":["Anchor","FooterContainer_TransparentScreen","HeaderContainer_TransparentScreen","LinkBar_Classic","BackgroundGroup","PageGroup","PagesContainer","VectorImage","WRichText","MasterPage","PinnedLayer"],"parts":[],"url_v2":["f47ae840.chunk.min.css","62d8ebce.chunk.min.js",[0,1]]},"bootstrap-classic":{"url":["cc6af045.bundle.min.js",[0,1]],"components":["Column","Group","PageBackground","SiteButton_BasicButton","StripColumnsContainer","WPhoto_NoSkinPhoto","ContainerWrapper","Page_TransparentPageSkin"],"parts":[],"url_v2":["e2411168.chunk.min.css","84522751.chunk.min.js"]},"bootstrap-responsive":{"url":["d357e40d.bundle.min.js",[0,1,2,3]],"components":["DropDownMenu_TextOnlyMenuButtonSkin"],"parts":[],"url_v2":["f3ca696f.chunk.min.css","39ad77f8.chunk.min.js",[0,1]]}},"shared":["rb_wixui.thunderbolt~bootstrap-classic.e2411168.chunk.min.css","rb_wixui.thunderbolt~bootstrap-classic.84522751.chunk.min.js","rb_wixui.thunderbolt~bootstrap-responsive.f3ca696f.chunk.min.css","rb_wixui.thunderbolt~bootstrap-responsive.39ad77f8.chunk.min.js","rb_wixui.thunderbolt~common-site-members-dialogs.384b0b57.chunk.min.css","rb_wixui.thunderbolt~common-site-members-dialogs.5bc7c5a4.chunk.min.js","rb_wixui.thunderbolt~bootstrap.f47ae840.chunk.min.css","rb_wixui.thunderbolt~bootstrap.62d8ebce.chunk.min.js","-","-","-"],"statics":{},"parts":{}},{"version":"2.0","host":"thunderbolt","namespace":"dsgnsys","baseURL":"https://static.parastorage.com/services/editor-elements/dist/","model":["c05ec6fc.bundle.min.js","9cc10c27.min.css"],"assets":[["stylable-metadata","editor-elements-design-systems.fe5cafb15059f5e2435e1080fd1255e5aa7612b6.metadata.json"]],"components":{},"shared":[],"batches":{},"statics":{},"parts":{}}]}
    </script>
    <script>
        window.componentsRegistry.runtime = JSON.parse(document.getElementById('registry-components-manifests').textContent)
    </script>
    <script>if (window.componentsRegistry.manifestsLoadedResolve) { window.componentsRegistry.manifestsLoadedResolve() }</script>


    <script src="/index_files/rb_wixui.thunderbolt_bootstrap-classic.84522751.chunk.min.js"></script>
    <script src="/index_files/rb_wixui.thunderbolt_bootstrap.62d8ebce.chunk.min.js"></script>
    <script src="/index_files/rb_wixui.thunderbolt[FiveGridLine_SolidLine].c88b5af9.bundle.min.js"></script><script src="/index_files/rb_wixui.thunderbolt_common-site-members-dialogs.5bc7c5a4.chunk.min.js"></script><script src="/index_files/requirejs.min.js"></script><script src="/index_files/rb_wixui.thunderbolt[VerticalMenu_VerticalMenuSolidColorSkin].b731a961.bundle.min.js"></script><script src="/index_files/rb_wixui.thunderbolt_bootstrap-responsive.39ad77f8.chunk.min.js"></script><script src="/index_files/rb_wixui.thunderbolt[TextAreaInput].c8f3f5d0.bundle.min.js"></script><script src="/index_files/rb_wixui.thunderbolt[AppWidget_Classic].af065e1e.bundle.min.js"></script><script src="/index_files/rb_wixui.thunderbolt[FormContainer_FormContainerSkin].abd5cf4a.bundle.min.js"></script><script src="/index_files/rb_wixui.thunderbolt[MeshGroup].5d510b9c.bundle.min.js"></script><script src="/index_files/rb_wixui.thunderbolt[FreemiumBannerDesktop].0bb72b13.bundle.min.js"></script><script src="/index_files/rb_wixui.thunderbolt[SkipToContentButton].09136db3.bundle.min.js"></script><script src="/index_files/rb_wixui.thunderbolt[SlideShowSlide].6e181ac0.bundle.min.js"></script><script src="/index_files/rb_wixui.thunderbolt[LoginSocialBar].0f5f289a.bundle.min.js"></script><script src="/index_files/rb_wixui.thunderbolt[Container_DefaultAreaSkin].a514c5de.bundle.min.js"></script><script src="/index_files/rb_wixui.thunderbolt[TextInput].6b388b9c.bundle.min.js"></script><script src="/index_files/rb_wixui.thunderbolt[SlideShowContainer].ba24bc27.bundle.min.js"></script><script charset="utf-8" src="/index_files/ProGalleryInfoElement.chunk.min.js"></script><script charset="utf-8" src="/index_files/AsyncEventHandler.chunk.min.js"></script><script src="/index_files/rb_wixui.thunderbolt_bootstrap-classic.84522751.chunk.min.js"></script><script src="/index_files/rb_wixui.thunderbolt_bootstrap.62d8ebce.chunk.min.js"></script><link rel="stylesheet" type="text/css" href="./index_files/rb_wixui.thunderbolt[Container_RectangleArea].02b07b80.min.css"><script src="/index_files/rb_wixui.thunderbolt[Container_RectangleArea].53158066.bundle.min.js"></script><link rel="stylesheet" type="text/css" href="./index_files/post-list-pro-gallery.chunk.min.css" crossorigin="anonymous"><style type="text/css"> span.PIN_1663335131296_embed_grid {
            width: 100%;
            max-width: 257px;
            min-width: 140px;
            display: inline-block;
            border: 1px solid rgba(0,0,0,.1);
            border-radius: 16px;
            overflow: hidden;
            font: 12px "Helvetica Neue", Helvetica, arial, sans-serif;
            color: rgb(54, 54, 54);
            box-sizing: border-box;
            background: #fff;
            cursor: pointer;
            -webkit-font-smoothing: antialiased;
        }
        span.PIN_1663335131296_embed_grid * {
            display: block;
            position: relative;
            font: inherit;
            cursor: inherit;
            color: inherit;
            box-sizing: inherit;
            margin: 0;
            padding: 0;
            text-align: left;
        }
        span.PIN_1663335131296_embed_grid .PIN_1663335131296_hd {
            height: 55px;
        }
        span.PIN_1663335131296_embed_grid .PIN_1663335131296_hd .PIN_1663335131296_img {
            position: absolute;
            top: 10px;
            left: 10px;
            height: 36px;
            width: 36px;
            border-radius: 18px;
            background: transparent url () 0 0 no-repeat;
            background-size: cover;
        }
        span.PIN_1663335131296_embed_grid .PIN_1663335131296_hd .PIN_1663335131296_pinner {
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
            width: 75%;
            position: absolute;
            top: 20px;
            left: 56px;
            font-size: 14px;
            font-weight: bold;
        }
        span.PIN_1663335131296_embed_grid .PIN_1663335131296_bd {
            padding: 0 10px;
            -moz-scrollbars: none;
            -ms-overflow-style: none;
            overflow-x: hidden;
            overflow-y: auto;
        }
        span.PIN_1663335131296_embed_grid .PIN_1663335131296_bd .PIN_1663335131296_ct {
            width: 100%;
            height: auto;
        }
        span.PIN_1663335131296_embed_grid .PIN_1663335131296_bd .PIN_1663335131296_ct .PIN_1663335131296_col {
            display: inline-block;
            width: 100%;
            padding: 1px;
            vertical-align: top;
            min-width: 60px;
        }
        span.PIN_1663335131296_embed_grid .PIN_1663335131296_bd .PIN_1663335131296_ct .PIN_1663335131296_col .PIN_1663335131296_img {
            margin: 0;
            display: inline-block;
            width: 100%;
            background: transparent url() 0 0 no-repeat;
            background-size: cover;
            border-radius: 8px;
        }
        span.PIN_1663335131296_embed_grid .PIN_1663335131296_ft {
            padding: 10px;
        }
        span.PIN_1663335131296_embed_grid .PIN_1663335131296_ft .PIN_1663335131296_button {
            border-radius: 16px;
            text-align: center;
            background-color: #efefef;
            border: 1px solid #efefef;
            position: relative;
            display: block;
            overflow: hidden;
            height: 32px;
            width: 100%;
            min-width: 70px;
            padding: 0 3px;
        }
        span.PIN_1663335131296_embed_grid .PIN_1663335131296_ft .PIN_1663335131296_button .PIN_1663335131296_label {
            position: absolute;
            left: 0;
            width: 100%;
            text-align: center;
        }
        span.PIN_1663335131296_embed_grid .PIN_1663335131296_ft .PIN_1663335131296_button .PIN_1663335131296_label.PIN_1663335131296_top {
            top: 0;
        }
        span.PIN_1663335131296_embed_grid .PIN_1663335131296_ft .PIN_1663335131296_button .PIN_1663335131296_label.PIN_1663335131296_bottom {
            bottom: 0;
        }
        span.PIN_1663335131296_embed_grid .PIN_1663335131296_ft .PIN_1663335131296_button .PIN_1663335131296_label .PIN_1663335131296_string {
            white-space: pre;
            color: #746d6a;
            font-size: 13px;
            font-weight: bold;
            vertical-align: top;
            display: inline-block;
            height: 32px;
            line-height: 32px;
        }
        span.PIN_1663335131296_embed_grid .PIN_1663335131296_ft .PIN_1663335131296_button .PIN_1663335131296_label .PIN_1663335131296_logo {
            display: inline-block;
            vertical-align: bottom;
            height: 32px;
            width: 80px;
            background: transparent url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIGhlaWdodD0iMTJweCIgd2lkdGg9IjUwcHgiIHZpZXdCb3g9IjAgMCA1MCAxMiI+PGc+PHBhdGggZD0iTTE5LjY5LDkuMjggTDE5LjY5LDQuMjggTDIxLjI3LDQuMjggTDIxLjI3LDkuMjggTDE5LjY5LDkuMjggWiBNNS45NywwLjAwIEM5LjI3LDAuMDAgMTEuOTUsMi42OSAxMS45NSw2LjAwIEMxMS45NSw5LjMxIDkuMjcsMTIuMDAgNS45NywxMi4wMCBDNS4zOCwxMi4wMCA0LjgwLDExLjkxIDQuMjYsMTEuNzUgQzQuMjYsMTEuNzUgNC4yNiwxMS43NSA0LjI2LDExLjc1IEM0LjI1LDExLjc1IDQuMjQsMTEuNzQgNC4yMywxMS43NCBMNC4yMSwxMS43MyBDNC4yMSwxMS43MyA0LjIxLDExLjczIDQuMjEsMTEuNzMgQzQuNDUsMTEuMzMgNC44MSwxMC42OCA0Ljk1LDEwLjE2IEM1LjAyLDkuODggNS4zMiw4LjczIDUuMzIsOC43MyBDNS41Miw5LjExIDYuMDgsOS40MiA2LjY5LDkuNDIgQzguNDksOS40MiA5Ljc5LDcuNzYgOS43OSw1LjY5IEM5Ljc5LDMuNzEgOC4xOCwyLjIzIDYuMTEsMi4yMyBDMy41MywyLjIzIDIuMTYsMy45NiAyLjE2LDUuODYgQzIuMTYsNi43NCAyLjYzLDcuODMgMy4zNyw4LjE4IEMzLjQ5LDguMjMgMy41NSw4LjIxIDMuNTcsOC4xMCBDMy41OSw4LjAyIDMuNjksNy42MSAzLjc0LDcuNDIgQzMuNzUsNy4zNiAzLjc1LDcuMzEgMy43MCw3LjI1IEMzLjQ1LDYuOTUgMy4yNSw2LjM5IDMuMjUsNS44OCBDMy4yNSw0LjU1IDQuMjUsMy4yNyA1Ljk1LDMuMjcgQzcuNDIsMy4yNyA4LjQ1LDQuMjggOC40NSw1LjcxIEM4LjQ1LDcuMzQgNy42Myw4LjQ2IDYuNTcsOC40NiBDNS45OCw4LjQ2IDUuNTQsNy45OCA1LjY4LDcuMzggQzUuODUsNi42NyA2LjE4LDUuOTAgNi4xOCw1LjM4IEM2LjE4LDQuOTIgNS45Myw0LjU0IDUuNDIsNC41NCBDNC44Miw0LjU0IDQuMzQsNS4xNiA0LjM0LDUuOTkgQzQuMzQsNi41MiA0LjUyLDYuODggNC41Miw2Ljg4IEM0LjUyLDYuODggMy45Myw5LjQwIDMuODIsOS44NyBDMy43MCwxMC4zOCAzLjc1LDExLjExIDMuODAsMTEuNTkgTDMuODAsMTEuNTkgQzMuNzksMTEuNTkgMy43OCwxMS41OCAzLjc4LDExLjU4IEMzLjc3LDExLjU4IDMuNzYsMTEuNTggMy43NiwxMS41NyBDMy43NiwxMS41NyAzLjc2LDExLjU3IDMuNzYsMTEuNTcgQzEuNTYsMTAuNjkgMC4wMCw4LjUzIDAuMDAsNi4wMCBDMC4wMCwyLjY5IDIuNjcsMC4wMCA1Ljk3LDAuMDAgWiBNMTYuODcsMi4zMSBDMTcuNzEsMi4zMSAxOC4zNCwyLjU0IDE4Ljc2LDIuOTUgQzE5LjIxLDMuMzcgMTkuNDYsMy45NiAxOS40Niw0LjY2IEMxOS40Niw2LjAwIDE4LjU0LDYuOTUgMTcuMTEsNi45NSBMMTUuNzIsNi45NSBMMTUuNzIsOS4yOCBMMTQuMTIsOS4yOCBMMTQuMTIsMi4zMSBMMTYuODcsMi4zMSBaIE0xNi45NCw1LjU4IEMxNy41Niw1LjU4IDE3LjkxLDUuMjEgMTcuOTEsNC42NSBDMTcuOTEsNC4xMCAxNy41NSwzLjc2IDE2Ljk0LDMuNzYgTDE1LjcyLDMuNzYgTDE1LjcyLDUuNTggTDE2Ljk0LDUuNTggWiBNNTAuMDAsNS4yOCBMNDkuMTksNS4yOCBMNDkuMTksNy42MiBDNDkuMTksOC4wMSA0OS40MCw4LjExIDQ5Ljc0LDguMTEgQzQ5LjgzLDguMTEgNDkuOTMsOC4xMCA1MC4wMCw4LjEwIEw1MC4wMCw5LjI4IEM0OS44NCw5LjMxIDQ5LjU4LDkuMzMgNDkuMjIsOS4zMyBDNDguMzAsOS4zMyA0Ny42NCw5LjAzIDQ3LjY0LDcuOTYgTDQ3LjY0LDUuMjggTDQ3LjE2LDUuMjggTDQ3LjE2LDQuMjggTDQ3LjY0LDQuMjggTDQ3LjY0LDIuNzAgTDQ5LjE5LDIuNzAgTDQ5LjE5LDQuMjggTDUwLjAwLDQuMjggTDUwLjAwLDUuMjggWiBNNDUuMzEsNi4xMyBDNDYuMTgsNi4yNyA0Ny4yMSw2LjUwIDQ3LjIxLDcuNzcgQzQ3LjIxLDguODcgNDYuMjUsOS40MyA0NC45NSw5LjQzIEM0My41NSw5LjQzIDQyLjY1LDguODEgNDIuNTQsNy43OCBMNDQuMDUsNy43OCBDNDQuMTUsOC4yMCA0NC40Niw4LjQwIDQ0Ljk0LDguNDAgQzQ1LjQyLDguNDAgNDUuNzIsOC4yMiA0NS43Miw3LjkwIEM0NS43Miw3LjQ1IDQ1LjEyLDcuNDAgNDQuNDYsNy4yOSBDNDMuNTksNy4xNCA0Mi42Nyw2LjkxIDQyLjY3LDUuNzQgQzQyLjY3LDQuNjggNDMuNjQsNC4xNCA0NC44Miw0LjE0IEM0Ni4yMiw0LjE0IDQ2Ljk4LDQuNzUgNDcuMDYsNS43NCBMNDUuNjAsNS43NCBDNDUuNTQsNS4yOSA0NS4yNCw1LjE1IDQ0LjgwLDUuMTUgQzQ0LjQyLDUuMTUgNDQuMTIsNS4zMCA0NC4xMiw1LjYxIEM0NC4xMiw1Ljk2IDQ0LjY4LDYuMDEgNDUuMzEsNi4xMyBaIE0yMC40OCwyLjAwIEMyMS4wMCwyLjAwIDIxLjQzLDIuNDIgMjEuNDMsMi45NSBDMjEuNDMsMy40OCAyMS4wMCwzLjkwIDIwLjQ4LDMuOTAgQzE5Ljk1LDMuOTAgMTkuNTMsMy40OCAxOS41MywyLjk1IEMxOS41MywyLjQyIDE5Ljk1LDIuMDAgMjAuNDgsMi4wMCBaIE0yOC40OCw3LjYyIEMyOC40OCw4LjAxIDI4LjcwLDguMTEgMjkuMDQsOC4xMSBDMjkuMTAsOC4xMSAyOS4xOCw4LjEwIDI5LjI0LDguMTAgTDI5LjI0LDkuMjkgQzI5LjA4LDkuMzEgMjguODMsOS4zMyAyOC41Miw5LjMzIEMyNy42MCw5LjMzIDI2Ljk0LDkuMDMgMjYuOTQsNy45NiBMMjYuOTQsNS4yOCBMMjYuNDIsNS4yOCBMMjYuNDIsNC4yOCBMMjYuOTQsNC4yOCBMMjYuOTQsMi43MCBMMjguNDgsMi43MCBMMjguNDgsNC4yOCBMMjkuMjQsNC4yOCBMMjkuMjQsNS4yOCBMMjguNDgsNS4yOCBMMjguNDgsNy42MiBaIE0yNC42OSw0LjE0IEMyNS43Nyw0LjE0IDI2LjQxLDQuOTIgMjYuNDEsNi4wMyBMMjYuNDEsOS4yOCBMMjQuODMsOS4yOCBMMjQuODMsNi4zNSBDMjQuODMsNS44MiAyNC41Nyw1LjQ2IDI0LjA1LDUuNDYgQzIzLjUzLDUuNDYgMjMuMTgsNS45MCAyMy4xOCw2LjUyIEwyMy4xOCw5LjI4IEwyMS42MCw5LjI4IEwyMS42MCw0LjI4IEwyMy4xMiw0LjI4IEwyMy4xMiw0Ljk3IEwyMy4xNSw0Ljk3IEMyMy41Miw0LjQzIDI0LjAwLDQuMTQgMjQuNjksNC4xNCBaIE0zMy40Miw0Ljc2IEMzMi45OSw0LjM3IDMyLjQzLDQuMTQgMzEuNzIsNC4xNCBDMzAuMjAsNC4xNCAyOS4xNiw1LjI4IDI5LjE2LDYuNzcgQzI5LjE2LDguMjggMzAuMTcsOS40MiAzMS44MSw5LjQyIEMzMi40NCw5LjQyIDMyLjk1LDkuMjYgMzMuMzcsOC45NiBDMzMuODAsOC42NiAzNC4xMCw4LjIzIDM0LjIwLDcuNzggTDMyLjY2LDcuNzggQzMyLjUyLDguMTAgMzIuMjUsOC4yOCAzMS44Myw4LjI4IEMzMS4xOCw4LjI4IDMwLjgxLDcuODYgMzAuNzIsNy4xOSBMMzQuMjksNy4xOSBDMzQuMzAsNi4xOCAzNC4wMSw1LjMxIDMzLjQyLDQuNzYgTDMzLjQyLDQuNzYgWiBNNDEuNjYsNC43NiBDNDIuMjYsNS4zMSA0Mi41NSw2LjE4IDQyLjU0LDcuMTkgTDM4Ljk3LDcuMTkgQzM5LjA2LDcuODYgMzkuNDMsOC4yOCA0MC4wOCw4LjI4IEM0MC41MCw4LjI4IDQwLjc3LDguMTAgNDAuOTEsNy43OCBMNDIuNDUsNy43OCBDNDIuMzQsOC4yMyA0Mi4wNSw4LjY2IDQxLjYyLDguOTYgQzQxLjIwLDkuMjYgNDAuNjksOS40MiA0MC4wNiw5LjQyIEMzOC40Miw5LjQyIDM3LjQxLDguMjggMzcuNDEsNi43NyBDMzcuNDEsNS4yOCAzOC40NSw0LjE0IDM5Ljk3LDQuMTQgQzQwLjY3LDQuMTQgNDEuMjQsNC4zNyA0MS42Niw0Ljc2IFogTTMwLjczLDYuMjQgQzMwLjgzLDUuNjUgMzEuMTQsNS4yNyAzMS43NSw1LjI3IEMzMi4yNiw1LjI3IDMyLjYzLDUuNjUgMzIuNjksNi4yNCBMMzAuNzMsNi4yNCBaIE0zOC45OCw2LjI0IEw0MC45NCw2LjI0IEM0MC44OCw1LjY1IDQwLjUxLDUuMjcgNDAuMDAsNS4yNyBDMzkuMzksNS4yNyAzOS4wOCw1LjY1IDM4Ljk4LDYuMjQgWiBNMzcuNTQsNC4yMSBMMzcuNTQsNS42MCBDMzYuNjQsNS41MSAzNi4wNyw1Ljk5IDM2LjA3LDcuMDMgTDM2LjA3LDkuMjggTDM0LjQ4LDkuMjggTDM0LjQ4LDQuMjggTDM2LjAwLDQuMjggTDM2LjAwLDUuMDYgTDM2LjAzLDUuMDYgQzM2LjM4LDQuNDcgMzYuNzgsNC4yMSAzNy4zOSw0LjIxIEMzNy40NSw0LjIxIDM3LjUwLDQuMjEgMzcuNTQsNC4yMSBaIiBmaWxsPSIjZTYwMDIzIj48L3BhdGg+PC9nPjwvc3ZnPg==) 50% 50% no-repeat;
            background-size: contain;
        }
        span.PIN_1663335131296_embed_grid .PIN_1663335131296_ft .PIN_1663335131296_button:hover {
            border: 1px solid rgba(0,0,0,.1);
        }
        span.PIN_1663335131296_embed_grid.PIN_1663335131296_noscroll .PIN_1663335131296_bd {
            overflow: hidden;
        }
        span.PIN_1663335131296_embed_grid.PIN_1663335131296_board .PIN_1663335131296_hd .PIN_1663335131296_pinner {
            top: 10px;
        }
        span.PIN_1663335131296_embed_grid.PIN_1663335131296_board .PIN_1663335131296_hd .PIN_1663335131296_board {
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
            width: 75%;
            position: absolute;
            bottom: 10px;
            left: 56px;
            color: #363636;
            font-size: 12px;
        }
        span.PIN_1663335131296_embed_grid.PIN_1663335131296_c2 {
            max-width: 494px;
            min-width: 140px;
        }
        span.PIN_1663335131296_embed_grid.PIN_1663335131296_c2 .PIN_1663335131296_bd .PIN_1663335131296_ct .PIN_1663335131296_col {
            width: 50%;
        }
        span.PIN_1663335131296_embed_grid.PIN_1663335131296_c3 {
            max-width: 731px;
            min-width: 200px;
        }
        span.PIN_1663335131296_embed_grid.PIN_1663335131296_c3 .PIN_1663335131296_bd .PIN_1663335131296_ct .PIN_1663335131296_col {
            width: 33.33%;
        }
        span.PIN_1663335131296_embed_grid.PIN_1663335131296_c4 {
            max-width: 968px;
            min-width: 260px;
        }
        span.PIN_1663335131296_embed_grid.PIN_1663335131296_c4 .PIN_1663335131296_bd .PIN_1663335131296_ct .PIN_1663335131296_col {
            width: 25%;
        }
        span.PIN_1663335131296_embed_grid.PIN_1663335131296_c5 {
            max-width: 1205px;
            min-width: 320px;
        }
        span.PIN_1663335131296_embed_grid.PIN_1663335131296_c5 .PIN_1663335131296_bd .PIN_1663335131296_ct .PIN_1663335131296_col {
            width: 20%;
        }
        span.PIN_1663335131296_embed_grid.PIN_1663335131296_c6 {
            max-width: 1442px;
            min-width: 380px;
        }
        span.PIN_1663335131296_embed_grid.PIN_1663335131296_c6 .PIN_1663335131296_bd .PIN_1663335131296_ct .PIN_1663335131296_col {
            width: 16.66%;
        }
        span.PIN_1663335131296_embed_grid.PIN_1663335131296_c7 {
            max-width: 1679px;
            min-width: 440px;
        }
        span.PIN_1663335131296_embed_grid.PIN_1663335131296_c7 .PIN_1663335131296_bd .PIN_1663335131296_ct .PIN_1663335131296_col {
            width: 14.28%;
        }
        span.PIN_1663335131296_embed_grid.PIN_1663335131296_c8 {
            max-width: 1916px;
            min-width: 500px;
        }
        span.PIN_1663335131296_embed_grid.PIN_1663335131296_c8 .PIN_1663335131296_bd .PIN_1663335131296_ct .PIN_1663335131296_col {
            width: 12.5%;
        }
        span.PIN_1663335131296_embed_grid.PIN_1663335131296_c9 {
            max-width: 2153px;
            min-width: 560px;
        }
        span.PIN_1663335131296_embed_grid.PIN_1663335131296_c9 .PIN_1663335131296_bd .PIN_1663335131296_ct .PIN_1663335131296_col {
            width: 11.11%;
        }
        span.PIN_1663335131296_embed_grid.PIN_1663335131296_c10 {
            max-width: 2390px;
            min-width: 620px;
        }
        span.PIN_1663335131296_embed_grid.PIN_1663335131296_c10 .PIN_1663335131296_bd .PIN_1663335131296_ct .PIN_1663335131296_col {
            width: 10%;
        }
        span.PIN_1663335131296_embed_pin {
            min-width: 160px;
            max-width: 236px;
            width: 100%;
            border-radius: 16px;
            font: 12px "SF Pro", "Helvetica Neue", Helvetica, arial, sans-serif;
            display: inline-block;
            background: rgba(0,0,0,.1);
            overflow: hidden;
            border: 1px solid rgba(0,0,0,.1);
            box-sizing: border-box;
            -webkit-font-smoothing: antialiased;
            -webkit-osx-font-smoothing: grayscale;
        }
        span.PIN_1663335131296_embed_pin * {
            display: block;
            position: relative;
            font: inherit;
            cursor: inherit;
            color: inherit;
            box-sizing: inherit;
            margin: 0;
            padding: 0;
            text-align: left;
        }
        span.PIN_1663335131296_embed_pin .PIN_1663335131296_pages {
            height: 100%;
            width: 100%;
            display: block;
            position: relative;
            overflow: hidden;
        }
        span.PIN_1663335131296_embed_pin .PIN_1663335131296_pages .PIN_1663335131296_page {
            position: absolute;
            left: 0;
            transition-property: left;
            transition-duration: .25s;
            transition-timing-function: ease-in;
            height: inherit;
            width: inherit;
        }
        span.PIN_1663335131296_embed_pin .PIN_1663335131296_pages .PIN_1663335131296_page.PIN_1663335131296_past {
            left: -100%;
        }
        span.PIN_1663335131296_embed_pin .PIN_1663335131296_pages .PIN_1663335131296_page.PIN_1663335131296_future {
            left: 100%;
        }
        span.PIN_1663335131296_embed_pin .PIN_1663335131296_pages .PIN_1663335131296_page .PIN_1663335131296_blocks {
            height: inherit;
            width: inherit;
            overflow: hidden;
        }
        span.PIN_1663335131296_embed_pin .PIN_1663335131296_pages .PIN_1663335131296_page .PIN_1663335131296_blocks .PIN_1663335131296_block {
            position: absolute;
            height: 100%;
            width: 100%;
            overflowX: hidden;
        }
        span.PIN_1663335131296_embed_pin .PIN_1663335131296_pages .PIN_1663335131296_page .PIN_1663335131296_blocks .PIN_1663335131296_block .PIN_1663335131296_container {
            position: absolute;
            width: 100%;
        }
        span.PIN_1663335131296_embed_pin .PIN_1663335131296_pages .PIN_1663335131296_page .PIN_1663335131296_blocks .PIN_1663335131296_block .PIN_1663335131296_container.PIN_1663335131296_top {
            top: 0;
        }
        span.PIN_1663335131296_embed_pin .PIN_1663335131296_pages .PIN_1663335131296_page .PIN_1663335131296_blocks .PIN_1663335131296_block .PIN_1663335131296_container.PIN_1663335131296_middle {
            top: 50%;
            transform: translateY(-50%);
        }
        span.PIN_1663335131296_embed_pin .PIN_1663335131296_pages .PIN_1663335131296_page .PIN_1663335131296_blocks .PIN_1663335131296_block .PIN_1663335131296_container.PIN_1663335131296_bottom {
            bottom: 0;
        }
        span.PIN_1663335131296_embed_pin .PIN_1663335131296_pages .PIN_1663335131296_page .PIN_1663335131296_blocks .PIN_1663335131296_block .PIN_1663335131296_container.PIN_1663335131296_left {
            text-align: left;
        }
        span.PIN_1663335131296_embed_pin .PIN_1663335131296_pages .PIN_1663335131296_page .PIN_1663335131296_blocks .PIN_1663335131296_block .PIN_1663335131296_container.PIN_1663335131296_center {
            text-align: center;
        }
        span.PIN_1663335131296_embed_pin .PIN_1663335131296_pages .PIN_1663335131296_page .PIN_1663335131296_blocks .PIN_1663335131296_block .PIN_1663335131296_container.PIN_1663335131296_right {
            text-align: right;
        }
        span.PIN_1663335131296_embed_pin .PIN_1663335131296_pages .PIN_1663335131296_page .PIN_1663335131296_blocks .PIN_1663335131296_block .PIN_1663335131296_container .PIN_1663335131296_paragraph {
            text-align: inherit;
            display: inline;
            word-break: break-word;
            font-size: 16px;
            line-height: 1.24em;
            border-radius: 3px;
            padding: 0.1em 0.2em;
        }
        span.PIN_1663335131296_embed_pin .PIN_1663335131296_pages .PIN_1663335131296_page .PIN_1663335131296_blocks .PIN_1663335131296_block .PIN_1663335131296_container _video {
            position: absolute;
            height: 100%;
            width: auto;
            left: 50%;
            transform: translateX(-50%);
        }
        span.PIN_1663335131296_embed_pin .PIN_1663335131296_pages .PIN_1663335131296_page .PIN_1663335131296_blocks .PIN_1663335131296_block .PIN_1663335131296_container.PIN_1663335131296_video {
            height: 100%;
        }
        span.PIN_1663335131296_embed_pin .PIN_1663335131296_pages .PIN_1663335131296_page .PIN_1663335131296_blocks .PIN_1663335131296_block .PIN_1663335131296_image {
            position: absolute;
            height: 100%;
            width: 100%;
            background-position: 50% 50%;
            background-repeat: no-repeat;
        }
        span.PIN_1663335131296_embed_pin .PIN_1663335131296_pages .PIN_1663335131296_page .PIN_1663335131296_blocks .PIN_1663335131296_block .PIN_1663335131296_image.PIN_1663335131296_containMe {
            background-size: contain;
        }
        span.PIN_1663335131296_embed_pin .PIN_1663335131296_pages .PIN_1663335131296_page .PIN_1663335131296_blocks .PIN_1663335131296_block .PIN_1663335131296_image.PIN_1663335131296_coverMe {
            background-size: cover;
        }
        span.PIN_1663335131296_embed_pin .PIN_1663335131296_pages .PIN_1663335131296_page .PIN_1663335131296_blocks .PIN_1663335131296_block video {
            position: absolute;
            height: 100%;
            width: auto;
            left: 50%;
            transform: translateX(-50%);
        }
        span.PIN_1663335131296_embed_pin .PIN_1663335131296_pages .PIN_1663335131296_page .PIN_1663335131296_blocks .PIN_1663335131296_block video.PIN_1663335131296_isNative {
            width: 100%;
        }
        span.PIN_1663335131296_embed_pin .PIN_1663335131296_pages .PIN_1663335131296_overlay {
            position: absolute;
            height: 100%;
            width: 100%;
            opacity: .001;
            background: rgba(0,0,0,.03);
            cursor: pointer;
            user-select: none;
        }
        span.PIN_1663335131296_embed_pin .PIN_1663335131296_pages .PIN_1663335131296_overlay .PIN_1663335131296_repin {
            position: absolute;
            top: 12px;
            right: 12px;
            height: 40px;
            color: #fff;
            background: #e60023 url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIGhlaWdodD0iMzBweCIgd2lkdGg9IjMwcHgiIHZpZXdCb3g9Ii0xIC0xIDMxIDMxIj48Zz48cGF0aCBkPSJNMjkuNDQ5LDE0LjY2MiBDMjkuNDQ5LDIyLjcyMiAyMi44NjgsMjkuMjU2IDE0Ljc1LDI5LjI1NiBDNi42MzIsMjkuMjU2IDAuMDUxLDIyLjcyMiAwLjA1MSwxNC42NjIgQzAuMDUxLDYuNjAxIDYuNjMyLDAuMDY3IDE0Ljc1LDAuMDY3IEMyMi44NjgsMC4wNjcgMjkuNDQ5LDYuNjAxIDI5LjQ0OSwxNC42NjIiIGZpbGw9IiNmZmYiIHN0cm9rZT0iI2ZmZiIgc3Ryb2tlLXdpZHRoPSIxIj48L3BhdGg+PHBhdGggZD0iTTE0LjczMywxLjY4NiBDNy41MTYsMS42ODYgMS42NjUsNy40OTUgMS42NjUsMTQuNjYyIEMxLjY2NSwyMC4xNTkgNS4xMDksMjQuODU0IDkuOTcsMjYuNzQ0IEM5Ljg1NiwyNS43MTggOS43NTMsMjQuMTQzIDEwLjAxNiwyMy4wMjIgQzEwLjI1MywyMi4wMSAxMS41NDgsMTYuNTcyIDExLjU0OCwxNi41NzIgQzExLjU0OCwxNi41NzIgMTEuMTU3LDE1Ljc5NSAxMS4xNTcsMTQuNjQ2IEMxMS4xNTcsMTIuODQyIDEyLjIxMSwxMS40OTUgMTMuNTIyLDExLjQ5NSBDMTQuNjM3LDExLjQ5NSAxNS4xNzUsMTIuMzI2IDE1LjE3NSwxMy4zMjMgQzE1LjE3NSwxNC40MzYgMTQuNDYyLDE2LjEgMTQuMDkzLDE3LjY0MyBDMTMuNzg1LDE4LjkzNSAxNC43NDUsMTkuOTg4IDE2LjAyOCwxOS45ODggQzE4LjM1MSwxOS45ODggMjAuMTM2LDE3LjU1NiAyMC4xMzYsMTQuMDQ2IEMyMC4xMzYsMTAuOTM5IDE3Ljg4OCw4Ljc2NyAxNC42NzgsOC43NjcgQzEwLjk1OSw4Ljc2NyA4Ljc3NywxMS41MzYgOC43NzcsMTQuMzk4IEM4Ljc3NywxNS41MTMgOS4yMSwxNi43MDkgOS43NDksMTcuMzU5IEM5Ljg1NiwxNy40ODggOS44NzIsMTcuNiA5Ljg0LDE3LjczMSBDOS43NDEsMTguMTQxIDkuNTIsMTkuMDIzIDkuNDc3LDE5LjIwMyBDOS40MiwxOS40NCA5LjI4OCwxOS40OTEgOS4wNCwxOS4zNzYgQzcuNDA4LDE4LjYyMiA2LjM4NywxNi4yNTIgNi4zODcsMTQuMzQ5IEM2LjM4NywxMC4yNTYgOS4zODMsNi40OTcgMTUuMDIyLDYuNDk3IEMxOS41NTUsNi40OTcgMjMuMDc4LDkuNzA1IDIzLjA3OCwxMy45OTEgQzIzLjA3OCwxOC40NjMgMjAuMjM5LDIyLjA2MiAxNi4yOTcsMjIuMDYyIEMxNC45NzMsMjIuMDYyIDEzLjcyOCwyMS4zNzkgMTMuMzAyLDIwLjU3MiBDMTMuMzAyLDIwLjU3MiAxMi42NDcsMjMuMDUgMTIuNDg4LDIzLjY1NyBDMTIuMTkzLDI0Ljc4NCAxMS4zOTYsMjYuMTk2IDEwLjg2MywyNy4wNTggQzEyLjA4NiwyNy40MzQgMTMuMzg2LDI3LjYzNyAxNC43MzMsMjcuNjM3IEMyMS45NSwyNy42MzcgMjcuODAxLDIxLjgyOCAyNy44MDEsMTQuNjYyIEMyNy44MDEsNy40OTUgMjEuOTUsMS42ODYgMTQuNzMzLDEuNjg2IiBmaWxsPSIjZTYwMDIzIj48L3BhdGg+PC9nPjwvc3ZnPg==) 10px 50% no-repeat;
            background-size: 18px 18px;
            text-indent: 36px;
            font-size: 14px;
            line-height: 40px;
            font-weight: bold;
            border-radius: 20px;
            padding: 0 12px 0 0;
            width: auto;
            z-index: 2;
        }
        span.PIN_1663335131296_embed_pin .PIN_1663335131296_pages .PIN_1663335131296_overlay .PIN_1663335131296_price {
            position: absolute;
            top: 12px;
            left: 12px;
            height: 40px;
            background: rgba(255,255,255,.9);
            font-size: 14px;
            line-height: 40px;
            font-weight: bold;
            border-radius: 20px;
            padding: 0 12px;
            width: auto;
            z-index: 2;
        }
        span.PIN_1663335131296_embed_pin .PIN_1663335131296_pages .PIN_1663335131296_overlay .PIN_1663335131296_controls {
            position: absolute;
            height: 64px;
            width: 64px;
            top: 50%;
            left: 50%;
            margin-top: -32px;
            margin-left: -32px;
        }
        span.PIN_1663335131296_embed_pin .PIN_1663335131296_pages .PIN_1663335131296_overlay .PIN_1663335131296_controls .PIN_1663335131296_play {
            background: rgba(0,0,0,.8) url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIGhlaWdodD0iMjRweCIgd2lkdGg9IjI0cHgiIHZpZXdCb3g9IjAgMCAyNCAyNCI+PGc+PHBhdGggZD0iTTIyLjYyIDkuNDhMOC42My40OEEzIDMgMCAwIDAgNCAzdjE4YTMgMyAwIDAgMCA0LjYzIDIuNTJsMTQtOWEzIDMgMCAwIDAgMC01LjA0IiBmaWxsPSIjZmZmIj48L3BhdGg+PC9nPjwvc3ZnPg==) 50% 50% no-repeat;
        }
        span.PIN_1663335131296_embed_pin .PIN_1663335131296_pages .PIN_1663335131296_overlay .PIN_1663335131296_controls .PIN_1663335131296_pause {
            background: rgba(0,0,0,.8) url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIGhlaWdodD0iMjRweCIgd2lkdGg9IjI0cHgiIHZpZXdCb3g9IjAgMCAyNCAyNCI+PGc+PHBhdGggZD0iTTcgMGMxLjY1IDAgMyAxLjM1IDMgM3YxOGMwIDEuNjUtMS4zNSAzLTMgM3MtMy0xLjM1LTMtM1YzYzAtMS42NSAxLjM1LTMgMy0zem0xMCAwYzEuNjUgMCAzIDEuMzUgMyAzdjE4YzAgMS42NS0xLjM1IDMtMyAzcy0zLTEuMzUtMy0zVjNjMC0xLjY1IDEuMzUtMyAzLTN6IiBmaWxsPSIjZmZmIj48L3BhdGg+PC9nPjwvc3ZnPg==) 50% 50% no-repeat;
        }
        span.PIN_1663335131296_embed_pin .PIN_1663335131296_pages .PIN_1663335131296_overlay .PIN_1663335131296_controls .PIN_1663335131296_pause,  span.PIN_1663335131296_embed_pin .PIN_1663335131296_pages .PIN_1663335131296_overlay .PIN_1663335131296_controls .PIN_1663335131296_play {
            position: absolute;
            display: block;
            height: 64px;
            width: 64px;
            border-radius: 32px;
        }
        span.PIN_1663335131296_embed_pin .PIN_1663335131296_pages .PIN_1663335131296_overlay .PIN_1663335131296_nav {
            user-select: none;
            position: absolute;
            height: 100%;
            width: 20%;
            background: transparent url() 0 0 no-repeat;
            background-size: 24px 24px;
            z-index: 1;
        }
        span.PIN_1663335131296_embed_pin .PIN_1663335131296_pages .PIN_1663335131296_overlay .PIN_1663335131296_nav.PIN_1663335131296_forward,  span.PIN_1663335131296_embed_pin .PIN_1663335131296_pages .PIN_1663335131296_overlay .PIN_1663335131296_nav.PIN_1663335131296_forward_noop {
            right: 0;
        }
        span.PIN_1663335131296_embed_pin .PIN_1663335131296_pages .PIN_1663335131296_overlay .PIN_1663335131296_nav.PIN_1663335131296_backward,  span.PIN_1663335131296_embed_pin .PIN_1663335131296_pages .PIN_1663335131296_overlay .PIN_1663335131296_nav.PIN_1663335131296_backward_noop {
            left: 0;
        }
        span.PIN_1663335131296_embed_pin .PIN_1663335131296_pages .PIN_1663335131296_overlay .PIN_1663335131296_nav.PIN_1663335131296_forward {
            background-image: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIGhlaWdodD0iMjRweCIgd2lkdGg9IjI0cHgiIHZpZXdCb3g9IjAgMCAyNCAyNCI+PGc+PHBhdGggZD0iTTYuNzIgMjRjLjU3IDAgMS4xNC0uMjIgMS41Ny0uNjZMMTkuNSAxMiA4LjI5LjY2Yy0uODYtLjg4LTIuMjctLjg4LTMuMTQgMC0uODcuODgtLjg3IDIuMyAwIDMuMThMMTMuMjEgMTJsLTguMDYgOC4xNmMtLjg3Ljg4LS44NyAyLjMgMCAzLjE4LjQzLjQ0IDEgLjY2IDEuNTcuNjYiIGZpbGw9IiNmZmYiIHN0cm9rZT0iI2FhYSIgc3Ryb2tlLXdpZHRoPSIuNSI+PC9wYXRoPjwvZz48L3N2Zz4=);
            background-position: 100% 50%;
        }
        span.PIN_1663335131296_embed_pin .PIN_1663335131296_pages .PIN_1663335131296_overlay .PIN_1663335131296_nav.PIN_1663335131296_backward {
            background-image: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIGhlaWdodD0iMjRweCIgd2lkdGg9IjI0cHgiIHZpZXdCb3g9IjAgMCAyNCAyNCI+PGc+PHBhdGggZD0iTTE3LjI4IDI0Yy0uNTcgMC0xLjE0LS4yMi0xLjU4LS42Nkw0LjUgMTIgMTUuNy42NmEyLjIxIDIuMjEgMCAwIDEgMy4xNSAwYy44Ny44OC44NyAyLjMgMCAzLjE4TDEwLjc5IDEybDguMDYgOC4xNmMuODcuODguODcgMi4zIDAgMy4xOC0uNDQuNDQtMSAuNjYtMS41Ny42NiIgZmlsbD0iI2ZmZiIgc3Ryb2tlPSIjYWFhIiBzdHJva2Utd2lkdGg9Ii41Ij48L3BhdGg+PC9nPjwvc3ZnPg==);
            background-position: 0 50%;
        }
        span.PIN_1663335131296_embed_pin .PIN_1663335131296_pages .PIN_1663335131296_overlay .PIN_1663335131296_progress {
            position: absolute;
            bottom: 0;
            left: 0;
            height: 36px;
            width: 100%;
            background: linear-gradient(rgba(0,0,0,0), rgba(0,0,0,.2));
            text-align: center;
        }
        span.PIN_1663335131296_embed_pin .PIN_1663335131296_pages .PIN_1663335131296_overlay .PIN_1663335131296_progress .PIN_1663335131296_indicator {
            display: inline-block;
            height: 8px;
            width: 8px;
            margin: 16px 2px 0;
            background: rgba(255,255,255,.5);
            border-radius: 50%;
        }
        span.PIN_1663335131296_embed_pin .PIN_1663335131296_pages .PIN_1663335131296_overlay .PIN_1663335131296_progress .PIN_1663335131296_indicator.PIN_1663335131296_current {
            background: #fff;
        }
        span.PIN_1663335131296_embed_pin .PIN_1663335131296_pages .PIN_1663335131296_overlay:hover {
            opacity: 1;
        }
        span.PIN_1663335131296_embed_pin .PIN_1663335131296_footer {
            position: relative;
            display: block;
            height: 96px;
            padding: 16px;
            color: #333;
            background: #fff;
        }
        span.PIN_1663335131296_embed_pin .PIN_1663335131296_footer .PIN_1663335131296_container {
            position: relative;
            display: block;
            width: 100%;
            height: 100%;
            background: #fff;
        }
        span.PIN_1663335131296_embed_pin .PIN_1663335131296_footer .PIN_1663335131296_container .PIN_1663335131296_title {
            position: absolute;
            left: 0px;
            right: 0px;
            font-size: 16px;
            font-weight: bold;
            overflow: hidden;
            white-space: pre;
            text-overflow: ellipsis;
        }
        span.PIN_1663335131296_embed_pin .PIN_1663335131296_footer .PIN_1663335131296_container .PIN_1663335131296_avatar {
            position: absolute;
            bottom: 0;
            left: 0;
            height: 30px;
            width: 30px;
            border-radius: 50%;
            background: transparent url() 0 0 no-repeat;
            background-size: cover;
            box-shadow: 0 0 1px rgba(0,0,0,.5);
        }
        span.PIN_1663335131296_embed_pin .PIN_1663335131296_footer .PIN_1663335131296_container .PIN_1663335131296_deets {
            position: absolute;
            left: 40px;
            right: 0px;
            bottom: 0px;
            height: 30px;
        }
        span.PIN_1663335131296_embed_pin .PIN_1663335131296_footer .PIN_1663335131296_container .PIN_1663335131296_deets span {
            left: 0px;
            right: 0px;
            position: absolute;
            font-size: 12px;
            overflow: hidden;
            white-space: pre;
            text-overflow: ellipsis;
        }
        span.PIN_1663335131296_embed_pin .PIN_1663335131296_footer .PIN_1663335131296_container .PIN_1663335131296_deets .PIN_1663335131296_topline {
            top: 0;
        }
        span.PIN_1663335131296_embed_pin .PIN_1663335131296_footer .PIN_1663335131296_container .PIN_1663335131296_deets .PIN_1663335131296_bottomline {
            bottom: 0;
        }
        span.PIN_1663335131296_embed_pin .PIN_1663335131296_footer::after {
            content: "------------------------------------------------------------------------------------------------------------------------";
            display: block;
            height: 1px;
            line-height: 1px;
            color: transparent;
        }
        span.PIN_1663335131296_embed_pin .PIN_1663335131296_footer.PIN_1663335131296_uno .PIN_1663335131296_container .PIN_1663335131296_deets .PIN_1663335131296_topline {
            top: 8px;
        }
        span.PIN_1663335131296_embed_pin .PIN_1663335131296_footer.PIN_1663335131296_uno .PIN_1663335131296_container .PIN_1663335131296_deets .PIN_1663335131296_bottomline {
            display: none;
        }
        span.PIN_1663335131296_embed_pin.PIN_1663335131296_fresh .PIN_1663335131296_pages .PIN_1663335131296_overlay {
            opacity: 1;
        }
        span.PIN_1663335131296_embed_pin.PIN_1663335131296_medium {
            min-width: 237px;
            max-width: 345px;
            border-radius: 24px;
        }
        span.PIN_1663335131296_embed_pin.PIN_1663335131296_medium .PIN_1663335131296_pages .PIN_1663335131296_page .PIN_1663335131296_blocks .PIN_1663335131296_block .PIN_1663335131296_container .PIN_1663335131296_paragraph {
            font-size: 21px;
            line-height: 1.23em;
            border-radius: 5px;
            padding: 2px 3px;
        }
        span.PIN_1663335131296_embed_pin.PIN_1663335131296_medium .PIN_1663335131296_pages .PIN_1663335131296_overlay .PIN_1663335131296_progress .PIN_1663335131296_indicator {
            margin: 16px 4px 0;
        }
        span.PIN_1663335131296_embed_pin.PIN_1663335131296_large {
            min-width: 346px;
            max-width: 600px;
            border-radius: 36px;
        }
        span.PIN_1663335131296_embed_pin.PIN_1663335131296_large .PIN_1663335131296_pages .PIN_1663335131296_page .PIN_1663335131296_blocks .PIN_1663335131296_block .PIN_1663335131296_container .PIN_1663335131296_paragraph {
            font-size: 27px;
            line-height: 1.11em;
            border-radius: 5px;
            padding: 3px 4.5px;
        }
        span.PIN_1663335131296_embed_pin.PIN_1663335131296_large .PIN_1663335131296_pages .PIN_1663335131296_overlay .PIN_1663335131296_progress .PIN_1663335131296_indicator {
            margin: 16px 4px 0;
        }
        span.PIN_1663335131296_embed_pin.PIN_1663335131296_atStart .PIN_1663335131296_pages .PIN_1663335131296_overlay .PIN_1663335131296_backward {
            display: none;
        }
        span.PIN_1663335131296_embed_pin.PIN_1663335131296_atEnd .PIN_1663335131296_pages .PIN_1663335131296_overlay .PIN_1663335131296_forward {
            display: none;
        }
        .PIN_1663335131296_button_follow {
            display: inline-block;
            color: #363636;
            box-sizing: border-box;
            box-shadow: inset 0 0 1px #888;
            border-radius: 3px;
            font: bold 11px/20px "Helvetica Neue", Helvetica, arial, sans-serif !important;
            cursor: pointer;
            -webkit-font-smoothing: antialiased;
            height: 20px;
            padding: 0 4px 0 20px;
            background-color: #efefef;
            position: relative;
            white-space: nowrap;
            vertical-align: baseline;
        }
        .PIN_1663335131296_button_follow:hover {
            box-shadow: inset 0 0 1px #000;
        }
        .PIN_1663335131296_button_follow::after {
            content: "";
            position: absolute;
            height: 14px;
            width: 14px;
            top: 3px;
            left: 3px;
            background: transparent url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIGhlaWdodD0iMzBweCIgd2lkdGg9IjMwcHgiIHZpZXdCb3g9Ii0xIC0xIDMxIDMxIj48Zz48cGF0aCBkPSJNMjkuNDQ5LDE0LjY2MiBDMjkuNDQ5LDIyLjcyMiAyMi44NjgsMjkuMjU2IDE0Ljc1LDI5LjI1NiBDNi42MzIsMjkuMjU2IDAuMDUxLDIyLjcyMiAwLjA1MSwxNC42NjIgQzAuMDUxLDYuNjAxIDYuNjMyLDAuMDY3IDE0Ljc1LDAuMDY3IEMyMi44NjgsMC4wNjcgMjkuNDQ5LDYuNjAxIDI5LjQ0OSwxNC42NjIiIGZpbGw9IiNmZmYiIHN0cm9rZT0iI2ZmZiIgc3Ryb2tlLXdpZHRoPSIxIj48L3BhdGg+PHBhdGggZD0iTTE0LjczMywxLjY4NiBDNy41MTYsMS42ODYgMS42NjUsNy40OTUgMS42NjUsMTQuNjYyIEMxLjY2NSwyMC4xNTkgNS4xMDksMjQuODU0IDkuOTcsMjYuNzQ0IEM5Ljg1NiwyNS43MTggOS43NTMsMjQuMTQzIDEwLjAxNiwyMy4wMjIgQzEwLjI1MywyMi4wMSAxMS41NDgsMTYuNTcyIDExLjU0OCwxNi41NzIgQzExLjU0OCwxNi41NzIgMTEuMTU3LDE1Ljc5NSAxMS4xNTcsMTQuNjQ2IEMxMS4xNTcsMTIuODQyIDEyLjIxMSwxMS40OTUgMTMuNTIyLDExLjQ5NSBDMTQuNjM3LDExLjQ5NSAxNS4xNzUsMTIuMzI2IDE1LjE3NSwxMy4zMjMgQzE1LjE3NSwxNC40MzYgMTQuNDYyLDE2LjEgMTQuMDkzLDE3LjY0MyBDMTMuNzg1LDE4LjkzNSAxNC43NDUsMTkuOTg4IDE2LjAyOCwxOS45ODggQzE4LjM1MSwxOS45ODggMjAuMTM2LDE3LjU1NiAyMC4xMzYsMTQuMDQ2IEMyMC4xMzYsMTAuOTM5IDE3Ljg4OCw4Ljc2NyAxNC42NzgsOC43NjcgQzEwLjk1OSw4Ljc2NyA4Ljc3NywxMS41MzYgOC43NzcsMTQuMzk4IEM4Ljc3NywxNS41MTMgOS4yMSwxNi43MDkgOS43NDksMTcuMzU5IEM5Ljg1NiwxNy40ODggOS44NzIsMTcuNiA5Ljg0LDE3LjczMSBDOS43NDEsMTguMTQxIDkuNTIsMTkuMDIzIDkuNDc3LDE5LjIwMyBDOS40MiwxOS40NCA5LjI4OCwxOS40OTEgOS4wNCwxOS4zNzYgQzcuNDA4LDE4LjYyMiA2LjM4NywxNi4yNTIgNi4zODcsMTQuMzQ5IEM2LjM4NywxMC4yNTYgOS4zODMsNi40OTcgMTUuMDIyLDYuNDk3IEMxOS41NTUsNi40OTcgMjMuMDc4LDkuNzA1IDIzLjA3OCwxMy45OTEgQzIzLjA3OCwxOC40NjMgMjAuMjM5LDIyLjA2MiAxNi4yOTcsMjIuMDYyIEMxNC45NzMsMjIuMDYyIDEzLjcyOCwyMS4zNzkgMTMuMzAyLDIwLjU3MiBDMTMuMzAyLDIwLjU3MiAxMi42NDcsMjMuMDUgMTIuNDg4LDIzLjY1NyBDMTIuMTkzLDI0Ljc4NCAxMS4zOTYsMjYuMTk2IDEwLjg2MywyNy4wNTggQzEyLjA4NiwyNy40MzQgMTMuMzg2LDI3LjYzNyAxNC43MzMsMjcuNjM3IEMyMS45NSwyNy42MzcgMjcuODAxLDIxLjgyOCAyNy44MDEsMTQuNjYyIEMyNy44MDEsNy40OTUgMjEuOTUsMS42ODYgMTQuNzMzLDEuNjg2IiBmaWxsPSIjZTYwMDIzIj48L3BhdGg+PC9nPjwvc3ZnPg==) 0 0 no-repeat;
            background-size: 14px 14px;
        }
        .PIN_1663335131296_button_follow.PIN_1663335131296_tall {
            height: 26px;
            line-height: 26px;
            font-size: 13px;
            padding: 0 6px 0 25px;
            border-radius: 3px;
        }
        .PIN_1663335131296_button_follow.PIN_1663335131296_tall::after {
            height: 18px;
            width: 18px;
            top: 4px;
            left: 4px;
            background-size: 18px 18px;
        }
        .PIN_1663335131296_button_pin {
            cursor: pointer;
            display: inline-block;
            box-sizing: border-box;
            box-shadow: inset 0 0 1px #888;
            border-radius: 3px;
            height: 20px;
            width: 40px;
            -webkit-font-smoothing: antialiased;
            background: #efefef url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIGhlaWdodD0iMThweCIgd2lkdGg9IjQycHgiIHZpZXdCb3g9IjAgMCA0MiAxOCI+PGc+PHBhdGggZD0iTTE2Ljg1Myw2LjM0NSBDMTcuNjMyLDYuMzQ1IDE4LjM4LDUuNzAyIDE4LjUxLDQuOTA5IEMxOC42NjQsNC4xMzggMTguMTM1LDMuNDk0IDE3LjM1NywzLjQ5NCBDMTYuNTc4LDMuNDk0IDE1LjgzLDQuMTM4IDE1LjY5OCw0LjkwOSBDMTUuNTQ2LDUuNzAyIDE2LjA1Myw2LjM0NSAxNi44NTMsNi4zNDUgWiBNNy40NTgsMCBDMi41LDAgMCwzLjUyMiAwLDYuNDU5IEMwLDguMjM3IDAuNjgsOS44MTkgMi4xMzcsMTAuNDA5IEMyLjM3NiwxMC41MDUgMi41OSwxMC40MTIgMi42NiwxMC4xNSBDMi43MDgsOS45NjkgMi44MjIsOS41MTEgMi44NzMsOS4zMiBDMi45NDMsOS4wNjEgMi45MTYsOC45NyAyLjcyMyw4Ljc0NCBDMi4zMDIsOC4yNTMgMi4wMzQsNy42MTcgMi4wMzQsNi43MTYgQzIuMDM0LDQuMTA0IDQuMDA3LDEuNzY1IDcuMTcyLDEuNzY1IEM5Ljk3NSwxLjc2NSAxMS41MTQsMy40NjEgMTEuNTE0LDUuNzI2IEMxMS41MTQsOC43MDggMTAuMTgzLDExLjE4IDguMjA2LDExLjE4IEM3LjExNCwxMS4xOCA2LjI5NywxMC4zMjkgNi41NTksOS4yMzMgQzYuODcyLDcuOTIyIDcuNDgsNi41MDkgNy40OCw1LjU2NCBDNy40OCw0LjcxNyA3LjAyMiw0LjAxMSA2LjA3Miw0LjAxMSBDNC45NTYsNC4wMTEgNC4wNiw1LjE1NSA0LjA2LDYuNjg3IEM0LjA2LDcuNjYzIDQuMzkzLDguMzIzIDQuMzkzLDguMzIzIEM0LjM5Myw4LjMyMyAzLjI1MSwxMy4xMTcgMy4wNTEsMTMuOTU3IEMyLjY1MiwxNS42MjkgMi45OTEsMTcuNjc5IDMuMDE5LDE3Ljg4NiBDMy4wMzYsMTguMDA5IDMuMTk1LDE4LjAzOCAzLjI2NywxNy45NDYgQzMuMzcsMTcuODEyIDQuNywxNi4xODcgNS4xNTEsMTQuNTYyIEM1LjI3OSwxNC4xMDIgNS44ODUsMTEuNzIgNS44ODUsMTEuNzIgQzYuMjQ4LDEyLjQwNiA3LjMwOCwxMy4wMDkgOC40MzUsMTMuMDA5IEMxMS43OSwxMy4wMDkgMTQuMDY2LDkuOTc5IDE0LjA2Niw1LjkyMyBDMTQuMDY2LDIuODU3IDExLjQ0NCwwIDcuNDU4LDAgWiBNMjYuODk2LDE0LjE4OSBDMjYuMzQ4LDE0LjE4OSAyNi4xMTcsMTMuOTE1IDI2LjExNywxMy4zMjggQzI2LjExNywxMi40MDQgMjcuMDM1LDEwLjA5MSAyNy4wMzUsOS4wNDEgQzI3LjAzNSw3LjYzOCAyNi4yNzYsNi44MjYgMjQuNzIsNi44MjYgQzIzLjczOSw2LjgyNiAyMi43MjIsNy40NTMgMjIuMjkxLDguMDAzIEMyMi4yOTEsOC4wMDMgMjIuNDIyLDcuNTUzIDIyLjQ2Nyw3LjM4IEMyMi41MTUsNy4xOTYgMjIuNDE1LDYuODg0IDIyLjE3Myw2Ljg4NCBMMjAuNjUxLDYuODg0IEMyMC4zMjgsNi44ODQgMjAuMjM4LDcuMDU1IDIwLjE5MSw3LjI0NCBDMjAuMTcyLDcuMzIgMTkuNjI0LDkuNTg0IDE5LjA5OCwxMS42MzIgQzE4LjczOCwxMy4wMzQgMTcuODYzLDE0LjIwNSAxNi45MjgsMTQuMjA1IEMxNi40NDcsMTQuMjA1IDE2LjIzMywxMy45MDYgMTYuMjMzLDEzLjM5OSBDMTYuMjMzLDEyLjk1OSAxNi41MTksMTEuODc3IDE2Ljg2LDEwLjUzNCBDMTcuMjc2LDguODk4IDE3LjY0Miw3LjU1MSAxNy42ODEsNy4zOTQgQzE3LjczMiw3LjE5MiAxNy42NDIsNy4wMTcgMTcuMzc5LDcuMDE3IEwxNS44NDksNy4wMTcgQzE1LjU3Miw3LjAxNyAxNS40NzMsNy4xNjEgMTUuNDE0LDcuMzYxIEMxNS40MTQsNy4zNjEgMTQuOTgzLDguOTc3IDE0LjUyNywxMC43NzUgQzE0LjE5NiwxMi4wNzkgMTMuODMsMTMuNDA5IDEzLjgzLDE0LjAzNCBDMTMuODMsMTUuMTQ4IDE0LjMzNiwxNS45NDQgMTUuNzI0LDE1Ljk0NCBDMTYuNzk2LDE1Ljk0NCAxNy42NDQsMTUuNDUgMTguMjkyLDE0Ljc2NCBDMTguMTk3LDE1LjEzNSAxOC4xMzYsMTUuNDE0IDE4LjEzLDE1LjQzOSBDMTguMDc0LDE1LjY1IDE4LjE0MiwxNS44MzggMTguMzk0LDE1LjgzOCBMMTkuOTYxLDE1LjgzOCBDMjAuMjMzLDE1LjgzOCAyMC4zMzcsMTUuNzMgMjAuMzk0LDE1LjQ5NCBDMjAuNDQ5LDE1LjI2OSAyMS42MTksMTAuNjY3IDIxLjYxOSwxMC42NjcgQzIxLjkyOCw5LjQ0MyAyMi42OTIsOC42MzIgMjMuNzY4LDguNjMyIEMyNC4yNzksOC42MzIgMjQuNzIsOC45NjcgMjQuNjY5LDkuNjE4IEMyNC42MTIsMTAuMzMzIDIzLjc0MSwxMi45MDMgMjMuNzQxLDE0LjAzMSBDMjMuNzQxLDE0Ljg4NCAyNC4wNiwxNS45NDUgMjUuNjgzLDE1Ljk0NSBDMjYuNzg5LDE1Ljk0NSAyNy42MDMsMTUuNDY0IDI4LjE5NSwxNC43ODYgTDI3LjQ4OSwxMy45NDEgQzI3LjMxMSwxNC4wOTQgMjcuMTE0LDE0LjE4OSAyNi44OTYsMTQuMTg5IFogTTQxLjcwMSw2Ljg3MyBMNDAuMTM0LDYuODczIEM0MC4xMzQsNi44NzMgNDAuODU2LDQuMTA5IDQwLjg3Myw0LjAzNSBDNDAuOTQyLDMuNzQ1IDQwLjY5OCwzLjU3OCA0MC40NDEsMy42MzEgQzQwLjQ0MSwzLjYzMSAzOS4yMywzLjg2NiAzOS4wMDUsMy45MTMgQzM4Ljc3OSwzLjk1OCAzOC42MDQsNC4wODEgMzguNTIyLDQuNDAzIEMzOC41MTIsNC40NDUgMzcuODgsNi44NzMgMzcuODgsNi44NzMgTDM2LjYyMiw2Ljg3MyBDMzYuMzg1LDYuODczIDM2LjI0NSw2Ljk2OCAzNi4xOTIsNy4xODggQzM2LjExNSw3LjUwNCAzNS45NzUsOC4xNDUgMzUuOTM2LDguMjk3IEMzNS44ODUsOC40OTQgMzYsOC42NDQgMzYuMjIyLDguNjQ0IEwzNy40NTcsOC42NDQgQzM3LjQ0OCw4LjY3NyAzNy4wNjQsMTAuMTI1IDM2LjcyNSwxMS41MjEgTDM2LjcyNCwxMS41MTYgQzM2LjcyLDExLjUzMiAzNi43MTYsMTEuNTQ2IDM2LjcxMiwxMS41NjIgTDM2LjcxMiwxMS41NTYgQzM2LjcxMiwxMS41NTYgMzYuNzA4LDExLjU3MSAzNi43MDIsMTEuNTk4IEMzNi4zMjQsMTIuOTY4IDM1LjExOCwxNC4yMDkgMzQuMjAxLDE0LjIwOSBDMzMuNzIxLDE0LjIwOSAzMy41MDYsMTMuOTA5IDMzLjUwNiwxMy40MDIgQzMzLjUwNiwxMi45NjMgMzMuNzkyLDExLjg4IDM0LjEzNCwxMC41MzcgQzM0LjU0OSw4LjkwMSAzNC45MTUsNy41NTUgMzQuOTU1LDcuMzk3IEMzNS4wMDYsNy4xOTYgMzQuOTE1LDcuMDIgMzQuNjUyLDcuMDIgTDMzLjEyMiw3LjAyIEMzMi44NDUsNy4wMiAzMi43NDYsNy4xNjQgMzIuNjg3LDcuMzY0IEMzMi42ODcsNy4zNjQgMzIuMjU3LDguOTggMzEuOCwxMC43NzggQzMxLjQ2OSwxMi4wODMgMzEuMTAzLDEzLjQxMiAzMS4xMDMsMTQuMDM3IEMzMS4xMDMsMTUuMTUxIDMxLjYwOSwxNS45NDggMzIuOTk3LDE1Ljk0OCBDMzQuMDcsMTUuOTQ4IDM1LjEzNiwxNS40NTMgMzUuNzgzLDE0Ljc2NyBDMzUuNzgzLDE0Ljc2NyAzNi4wMTEsMTQuNTIxIDM2LjIzLDE0LjIyOSBDMzYuMjQxLDE0LjU4MSAzNi4zMjQsMTQuODM3IDM2LjQxMSwxNS4wMTggQzM2LjQ1OCwxNS4xMTkgMzYuNTE1LDE1LjIxNSAzNi41ODEsMTUuMzAzIEMzNi41ODIsMTUuMzA0IDM2LjU4MywxNS4zMDYgMzYuNTg1LDE1LjMwOCBMMzYuNTg1LDE1LjMwOCBDMzYuODkxLDE1LjcxMyAzNy4zOTgsMTUuOTYyIDM4LjE1MSwxNS45NjIgQzM5Ljg5NCwxNS45NjIgNDAuOTQ0LDE0LjkzOCA0MS41NjIsMTMuOTA5IEw0MC43MDQsMTMuMjM5IEM0MC4zMzMsMTMuNzc0IDM5LjgzOSwxNC4xNzUgMzkuMzI0LDE0LjE3NSBDMzguODQ2LDE0LjE3NSAzOC41NzksMTMuODc4IDM4LjU3OSwxMy4zNzIgQzM4LjU3OSwxMi45MzUgMzguODg5LDExLjg2OCAzOS4yMjksMTAuNTMgQzM5LjM0NCwxMC4wODMgMzkuNTE2LDkuNDAxIDM5LjcwOCw4LjY0NCBMNDEuMzAyLDguNjQ0IEM0MS41MzksOC42NDQgNDEuNjc4LDguNTQ5IDQxLjczMiw4LjMyOSBDNDEuODA4LDguMDEyIDQxLjk0OCw3LjM3MiA0MS45ODgsNy4yMjEgQzQyLjAzOSw3LjAyMyA0MS45MjMsNi44NzMgNDEuNzAxLDYuODczIFogTTM0LjEyNiw2LjM0OCBDMzQuOTA1LDYuMzQ4IDM1LjY1Myw1LjcwNiAzNS43ODMsNC45MTIgQzM1LjkzNyw0LjE0MSAzNS40MDksMy40OTggMzQuNjMsMy40OTggQzMzLjg1MSwzLjQ5OCAzMy4xMDMsNC4xNDEgMzIuOTcxLDQuOTEyIEMzMi44MTksNS43MDYgMzMuMzI2LDYuMzQ4IDM0LjEyNiw2LjM0OCBaIiBmaWxsPSIjZTYwMDIzIj48L3BhdGg+PC9nPjwvc3ZnPg==) 50% 50% no-repeat;
            background-size: 75%;
            position: relative;
            font: 12px "Helvetica Neue", Helvetica, arial, sans-serif;
            color: #555;
            text-align: center;
            vertical-align: baseline;
        }
        .PIN_1663335131296_button_pin:hover {
            box-shadow: inset 0 0 1px #000;
        }
        .PIN_1663335131296_button_pin.PIN_1663335131296_above .PIN_1663335131296_count {
            position: absolute;
            top: -28px;
            left: 0;
            height: 28px;
            width: inherit;
            line-height: 24px;
            background: transparent url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIGhlaWdodD0iNzZweCIgd2lkdGg9IjExNHB4IiB2aWV3Qm94PSIwIDAgMTE0IDc2Ij48Zz48cGF0aCBkPSJNOSAxQzQuNiAxIDEgNC42IDEgOXY0M2MwIDQuMyAzLjYgOCA4IDhoMjZsMTggMTVoNy41bDE2LTE1SDEwNWM0LjQgMCA4LTMuNyA4LThWOWMwLTQuNC0zLjYtOC04LThIOXoiIGZpbGw9IiNmZmYiIHN0cm9rZT0iI2I1YjViNSIgc3Ryb2tlLXdpZHRoPSIyIj48L3BhdGg+PC9nPjwvc3ZnPg==) 0 0 no-repeat;
            background-size: 40px 28px;
        }
        .PIN_1663335131296_button_pin.PIN_1663335131296_above.PIN_1663335131296_padded {
            margin-top: 28px;
        }
        .PIN_1663335131296_button_pin.PIN_1663335131296_beside .PIN_1663335131296_count {
            position: absolute;
            right: -45px;
            text-align: center;
            text-indent: 5px;
            height: inherit;
            width: 45px;
            font-size: 11px;
            line-height: 20px;
            background: transparent url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIGhlaWdodD0iNTZweCIgd2lkdGg9IjEyNnB4IiB2aWV3Qm94PSIyIDAgMTMwIDYwIj48Zz48cGF0aCBkPSJNMTE5LjYgMmM0LjUgMCA4IDMuNiA4IDh2NDBjMCA0LjQtMy41IDgtOCA4SDIzLjNMMS42IDMyLjR2LTQuNkwyMy4zIDJoOTYuM3oiIGZpbGw9IiNmZmYiIHN0cm9rZT0iI2I1YjViNSIgc3Ryb2tlLXdpZHRoPSIyIj48L3BhdGg+PC9nPjwvc3ZnPg==);
            background-size: cover;
        }
        .PIN_1663335131296_button_pin.PIN_1663335131296_beside.PIN_1663335131296_padded {
            margin-right: 45px;
        }
        .PIN_1663335131296_button_pin.PIN_1663335131296_ja {
            background-image: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIGhlaWdodD0iMThweCIgd2lkdGg9IjQxcHgiIHZpZXdCb3g9IjAgMCA0MSAxOCI+PGc+PHBhdGggZD0iTTE5LjgyMiw3LjE3MyBDMTkuODIyLDYuNTEgMTkuODM1LDYuMjc2IDE5Ljg4Nyw1Ljk2NCBMMTguMTQ1LDUuOTY0IEMxOC4xOTcsNi4yODkgMTguMTk3LDYuNDk3IDE4LjE5Nyw3LjE2IEwxOC4yMSwxMy4xOTIgQzE4LjIxLDEzLjk0NiAxOC4yMjMsMTQuMTY3IDE4LjI0OSwxNC4zODggQzE4LjMyNywxNS4wMjUgMTguNTIyLDE1LjQ0MSAxOC44ODYsMTUuNzE0IEMxOS4zOTMsMTYuMTA0IDIwLjI5LDE2LjI3MyAyMS45MjgsMTYuMjczIEMyMi43MjEsMTYuMjczIDI0LjM1OSwxNi4xOTUgMjUuMTI2LDE2LjExNyBDMjYuNTA0LDE1Ljk4NyAyNi41NjksMTUuOTc0IDI2Ljg0MiwxNS45NzQgTDI2Ljc2NCwxNC4yNDUgQzI2LjE5MiwxNC40MTQgMjUuOTA2LDE0LjQ3OSAyNS4yODIsMTQuNTU3IEMyNC4zMzMsMTQuNjg3IDIzLjEzNywxNC43NjUgMjIuMjY2LDE0Ljc2NSBDMjEuMDA1LDE0Ljc2NSAyMC4yNjQsMTQuNjQ4IDIwLjA0MywxNC40MjcgQzE5Ljg2MSwxNC4yNDUgMTkuODA5LDEzLjk1OSAxOS44MDksMTMuMjMxIEMxOS44MDksMTMuMTc5IDE5LjgwOSwxMy4xMDEgMTkuODIyLDEzLjAyMyBMMTkuODIyLDExLjMwNyBDMjEuOTkzLDEwLjkwNCAyNC4wMDgsMTAuMjI4IDI1LjkzMiw5LjI0IEwyNi4yNyw5LjA3MSBDMjYuMzc0LDkuMDE5IDI2LjQsOS4wMDYgMjYuNTQzLDguOTU0IEwyNS41MDMsNy40ODUgQzI0LjY1OCw4LjI3OCAyMS43ODUsOS40MzUgMTkuODIyLDkuNzk5IEwxOS44MjIsNy4xNzMgWiBNMjcuMzEsNC44NzIgQzI2LjQ5MSw0Ljg3MiAyNS44MTUsNS41NDggMjUuODE1LDYuMzY3IEMyNS44MTUsNy4xOTkgMjYuNDkxLDcuODc1IDI3LjMxLDcuODc1IEMyOC4xNDIsNy44NzUgMjguODE4LDcuMTk5IDI4LjgxOCw2LjM2NyBDMjguODE4LDUuNTQ4IDI4LjE0Miw0Ljg3MiAyNy4zMSw0Ljg3MiBMMjcuMzEsNC44NzIgWiBNMjcuMzEsNS41MjIgQzI3Ljc5MSw1LjUyMiAyOC4xNjgsNS44OTkgMjguMTY4LDYuMzY3IEMyOC4xNjgsNi44MzUgMjcuNzkxLDcuMjI1IDI3LjMxLDcuMjI1IEMyNi44NDIsNy4yMjUgMjYuNDY1LDYuODM1IDI2LjQ2NSw2LjM2NyBDMjYuNDY1LDUuODk5IDI2Ljg0Miw1LjUyMiAyNy4zMSw1LjUyMiBMMjcuMzEsNS41MjIgWiBNMzAuNTg2LDcuNjU0IEMzMS43OTUsOC4zMyAzMi44NjEsOS4xODggMzMuOTAxLDEwLjI5MyBMMzUuMDE5LDguODc2IEMzNC4wMTgsNy45MjcgMzMuMjEyLDcuMzI5IDMxLjY2NSw2LjM2NyBMMzAuNTg2LDcuNjU0IFogTTMxLjA0MSwxNi4yMzQgQzMxLjM0LDE2LjEzIDMxLjM3OSwxNi4xMTcgMzEuODk5LDE2LjAxMyBDMzMuOTE0LDE1LjU4NCAzNS41MjYsMTQuOTQ3IDM2Ljg1MiwxNC4wNjMgQzM4LjYzMywxMi44OCAzOS44NjgsMTEuMzQ2IDQwLjk3Myw4Ljk2NyBDNDAuMzEsOC40OTkgNDAuMTAyLDguMzA0IDM5LjU5NSw3LjY5MyBDMzkuMjA1LDguNzQ2IDM4Ljg0MSw5LjQ2MSAzOC4yNjksMTAuMjkzIEMzNy4yNDIsMTEuNzc1IDM2LjAzMywxMi43NzYgMzQuNDA4LDEzLjQ3OCBDMzMuMjI1LDEzLjk5OCAzMS42NzgsMTQuMzc1IDMwLjU2LDE0LjQ0IEwzMS4wNDEsMTYuMjM0IFogTTcuNDU4LDAgQzIuNSwwIDAsMy41MjIgMCw2LjQ1OSBDMCw4LjIzNyAwLjY4LDkuODE5IDIuMTM3LDEwLjQwOSBDMi4zNzYsMTAuNTA1IDIuNTksMTAuNDEyIDIuNjYsMTAuMTUgQzIuNzA4LDkuOTY5IDIuODIyLDkuNTExIDIuODczLDkuMzIgQzIuOTQzLDkuMDYxIDIuOTE2LDguOTcgMi43MjMsOC43NDQgQzIuMzAyLDguMjUzIDIuMDM0LDcuNjE3IDIuMDM0LDYuNzE2IEMyLjAzNCw0LjEwNCA0LjAwNywxLjc2NSA3LjE3MiwxLjc2NSBDOS45NzUsMS43NjUgMTEuNTE0LDMuNDYxIDExLjUxNCw1LjcyNiBDMTEuNTE0LDguNzA4IDEwLjE4MywxMS4xOCA4LjIwNiwxMS4xOCBDNy4xMTQsMTEuMTggNi4yOTcsMTAuMzI5IDYuNTU5LDkuMjMzIEM2Ljg3Miw3LjkyMiA3LjQ4LDYuNTA5IDcuNDgsNS41NjQgQzcuNDgsNC43MTcgNy4wMjIsNC4wMTEgNi4wNzIsNC4wMTEgQzQuOTU2LDQuMDExIDQuMDYsNS4xNTUgNC4wNiw2LjY4NyBDNC4wNiw3LjY2MyA0LjM5Myw4LjMyMyA0LjM5Myw4LjMyMyBDNC4zOTMsOC4zMjMgMy4yNTEsMTMuMTE3IDMuMDUxLDEzLjk1NyBDMi42NTIsMTUuNjI5IDIuOTkxLDE3LjY3OSAzLjAxOSwxNy44ODYgQzMuMDM2LDE4LjAwOSAzLjE5NSwxOC4wMzggMy4yNjcsMTcuOTQ2IEMzLjM3LDE3LjgxMiA0LjcsMTYuMTg3IDUuMTUxLDE0LjU2MiBDNS4yNzksMTQuMTAyIDUuODg1LDExLjcyIDUuODg1LDExLjcyIEM2LjI0OCwxMi40MDYgNy4zMDgsMTMuMDA5IDguNDM1LDEzLjAwOSBDMTEuNzksMTMuMDA5IDE0LjA2Niw5Ljk3OSAxNC4wNjYsNS45MjMgQzE0LjA2NiwyLjg1NyAxMS40NDQsMCA3LjQ1OCwwIFoiIGZpbGw9IiNlNjAwMjMiPjwvcGF0aD48L2c+PC9zdmc+);
            background-size: 72%;
        }
        .PIN_1663335131296_button_pin.PIN_1663335131296_red {
            background-color: #e60023;
            background-image: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIGhlaWdodD0iMThweCIgd2lkdGg9IjQycHgiIHZpZXdCb3g9IjAgMCA0MiAxOCI+PGc+PHBhdGggZD0iTTE2Ljg1Myw2LjM0NSBDMTcuNjMyLDYuMzQ1IDE4LjM4LDUuNzAyIDE4LjUxLDQuOTA5IEMxOC42NjQsNC4xMzggMTguMTM1LDMuNDk0IDE3LjM1NywzLjQ5NCBDMTYuNTc4LDMuNDk0IDE1LjgzLDQuMTM4IDE1LjY5OCw0LjkwOSBDMTUuNTQ2LDUuNzAyIDE2LjA1Myw2LjM0NSAxNi44NTMsNi4zNDUgWiBNNy40NTgsMCBDMi41LDAgMCwzLjUyMiAwLDYuNDU5IEMwLDguMjM3IDAuNjgsOS44MTkgMi4xMzcsMTAuNDA5IEMyLjM3NiwxMC41MDUgMi41OSwxMC40MTIgMi42NiwxMC4xNSBDMi43MDgsOS45NjkgMi44MjIsOS41MTEgMi44NzMsOS4zMiBDMi45NDMsOS4wNjEgMi45MTYsOC45NyAyLjcyMyw4Ljc0NCBDMi4zMDIsOC4yNTMgMi4wMzQsNy42MTcgMi4wMzQsNi43MTYgQzIuMDM0LDQuMTA0IDQuMDA3LDEuNzY1IDcuMTcyLDEuNzY1IEM5Ljk3NSwxLjc2NSAxMS41MTQsMy40NjEgMTEuNTE0LDUuNzI2IEMxMS41MTQsOC43MDggMTAuMTgzLDExLjE4IDguMjA2LDExLjE4IEM3LjExNCwxMS4xOCA2LjI5NywxMC4zMjkgNi41NTksOS4yMzMgQzYuODcyLDcuOTIyIDcuNDgsNi41MDkgNy40OCw1LjU2NCBDNy40OCw0LjcxNyA3LjAyMiw0LjAxMSA2LjA3Miw0LjAxMSBDNC45NTYsNC4wMTEgNC4wNiw1LjE1NSA0LjA2LDYuNjg3IEM0LjA2LDcuNjYzIDQuMzkzLDguMzIzIDQuMzkzLDguMzIzIEM0LjM5Myw4LjMyMyAzLjI1MSwxMy4xMTcgMy4wNTEsMTMuOTU3IEMyLjY1MiwxNS42MjkgMi45OTEsMTcuNjc5IDMuMDE5LDE3Ljg4NiBDMy4wMzYsMTguMDA5IDMuMTk1LDE4LjAzOCAzLjI2NywxNy45NDYgQzMuMzcsMTcuODEyIDQuNywxNi4xODcgNS4xNTEsMTQuNTYyIEM1LjI3OSwxNC4xMDIgNS44ODUsMTEuNzIgNS44ODUsMTEuNzIgQzYuMjQ4LDEyLjQwNiA3LjMwOCwxMy4wMDkgOC40MzUsMTMuMDA5IEMxMS43OSwxMy4wMDkgMTQuMDY2LDkuOTc5IDE0LjA2Niw1LjkyMyBDMTQuMDY2LDIuODU3IDExLjQ0NCwwIDcuNDU4LDAgWiBNMjYuODk2LDE0LjE4OSBDMjYuMzQ4LDE0LjE4OSAyNi4xMTcsMTMuOTE1IDI2LjExNywxMy4zMjggQzI2LjExNywxMi40MDQgMjcuMDM1LDEwLjA5MSAyNy4wMzUsOS4wNDEgQzI3LjAzNSw3LjYzOCAyNi4yNzYsNi44MjYgMjQuNzIsNi44MjYgQzIzLjczOSw2LjgyNiAyMi43MjIsNy40NTMgMjIuMjkxLDguMDAzIEMyMi4yOTEsOC4wMDMgMjIuNDIyLDcuNTUzIDIyLjQ2Nyw3LjM4IEMyMi41MTUsNy4xOTYgMjIuNDE1LDYuODg0IDIyLjE3Myw2Ljg4NCBMMjAuNjUxLDYuODg0IEMyMC4zMjgsNi44ODQgMjAuMjM4LDcuMDU1IDIwLjE5MSw3LjI0NCBDMjAuMTcyLDcuMzIgMTkuNjI0LDkuNTg0IDE5LjA5OCwxMS42MzIgQzE4LjczOCwxMy4wMzQgMTcuODYzLDE0LjIwNSAxNi45MjgsMTQuMjA1IEMxNi40NDcsMTQuMjA1IDE2LjIzMywxMy45MDYgMTYuMjMzLDEzLjM5OSBDMTYuMjMzLDEyLjk1OSAxNi41MTksMTEuODc3IDE2Ljg2LDEwLjUzNCBDMTcuMjc2LDguODk4IDE3LjY0Miw3LjU1MSAxNy42ODEsNy4zOTQgQzE3LjczMiw3LjE5MiAxNy42NDIsNy4wMTcgMTcuMzc5LDcuMDE3IEwxNS44NDksNy4wMTcgQzE1LjU3Miw3LjAxNyAxNS40NzMsNy4xNjEgMTUuNDE0LDcuMzYxIEMxNS40MTQsNy4zNjEgMTQuOTgzLDguOTc3IDE0LjUyNywxMC43NzUgQzE0LjE5NiwxMi4wNzkgMTMuODMsMTMuNDA5IDEzLjgzLDE0LjAzNCBDMTMuODMsMTUuMTQ4IDE0LjMzNiwxNS45NDQgMTUuNzI0LDE1Ljk0NCBDMTYuNzk2LDE1Ljk0NCAxNy42NDQsMTUuNDUgMTguMjkyLDE0Ljc2NCBDMTguMTk3LDE1LjEzNSAxOC4xMzYsMTUuNDE0IDE4LjEzLDE1LjQzOSBDMTguMDc0LDE1LjY1IDE4LjE0MiwxNS44MzggMTguMzk0LDE1LjgzOCBMMTkuOTYxLDE1LjgzOCBDMjAuMjMzLDE1LjgzOCAyMC4zMzcsMTUuNzMgMjAuMzk0LDE1LjQ5NCBDMjAuNDQ5LDE1LjI2OSAyMS42MTksMTAuNjY3IDIxLjYxOSwxMC42NjcgQzIxLjkyOCw5LjQ0MyAyMi42OTIsOC42MzIgMjMuNzY4LDguNjMyIEMyNC4yNzksOC42MzIgMjQuNzIsOC45NjcgMjQuNjY5LDkuNjE4IEMyNC42MTIsMTAuMzMzIDIzLjc0MSwxMi45MDMgMjMuNzQxLDE0LjAzMSBDMjMuNzQxLDE0Ljg4NCAyNC4wNiwxNS45NDUgMjUuNjgzLDE1Ljk0NSBDMjYuNzg5LDE1Ljk0NSAyNy42MDMsMTUuNDY0IDI4LjE5NSwxNC43ODYgTDI3LjQ4OSwxMy45NDEgQzI3LjMxMSwxNC4wOTQgMjcuMTE0LDE0LjE4OSAyNi44OTYsMTQuMTg5IFogTTQxLjcwMSw2Ljg3MyBMNDAuMTM0LDYuODczIEM0MC4xMzQsNi44NzMgNDAuODU2LDQuMTA5IDQwLjg3Myw0LjAzNSBDNDAuOTQyLDMuNzQ1IDQwLjY5OCwzLjU3OCA0MC40NDEsMy42MzEgQzQwLjQ0MSwzLjYzMSAzOS4yMywzLjg2NiAzOS4wMDUsMy45MTMgQzM4Ljc3OSwzLjk1OCAzOC42MDQsNC4wODEgMzguNTIyLDQuNDAzIEMzOC41MTIsNC40NDUgMzcuODgsNi44NzMgMzcuODgsNi44NzMgTDM2LjYyMiw2Ljg3MyBDMzYuMzg1LDYuODczIDM2LjI0NSw2Ljk2OCAzNi4xOTIsNy4xODggQzM2LjExNSw3LjUwNCAzNS45NzUsOC4xNDUgMzUuOTM2LDguMjk3IEMzNS44ODUsOC40OTQgMzYsOC42NDQgMzYuMjIyLDguNjQ0IEwzNy40NTcsOC42NDQgQzM3LjQ0OCw4LjY3NyAzNy4wNjQsMTAuMTI1IDM2LjcyNSwxMS41MjEgTDM2LjcyNCwxMS41MTYgQzM2LjcyLDExLjUzMiAzNi43MTYsMTEuNTQ2IDM2LjcxMiwxMS41NjIgTDM2LjcxMiwxMS41NTYgQzM2LjcxMiwxMS41NTYgMzYuNzA4LDExLjU3MSAzNi43MDIsMTEuNTk4IEMzNi4zMjQsMTIuOTY4IDM1LjExOCwxNC4yMDkgMzQuMjAxLDE0LjIwOSBDMzMuNzIxLDE0LjIwOSAzMy41MDYsMTMuOTA5IDMzLjUwNiwxMy40MDIgQzMzLjUwNiwxMi45NjMgMzMuNzkyLDExLjg4IDM0LjEzNCwxMC41MzcgQzM0LjU0OSw4LjkwMSAzNC45MTUsNy41NTUgMzQuOTU1LDcuMzk3IEMzNS4wMDYsNy4xOTYgMzQuOTE1LDcuMDIgMzQuNjUyLDcuMDIgTDMzLjEyMiw3LjAyIEMzMi44NDUsNy4wMiAzMi43NDYsNy4xNjQgMzIuNjg3LDcuMzY0IEMzMi42ODcsNy4zNjQgMzIuMjU3LDguOTggMzEuOCwxMC43NzggQzMxLjQ2OSwxMi4wODMgMzEuMTAzLDEzLjQxMiAzMS4xMDMsMTQuMDM3IEMzMS4xMDMsMTUuMTUxIDMxLjYwOSwxNS45NDggMzIuOTk3LDE1Ljk0OCBDMzQuMDcsMTUuOTQ4IDM1LjEzNiwxNS40NTMgMzUuNzgzLDE0Ljc2NyBDMzUuNzgzLDE0Ljc2NyAzNi4wMTEsMTQuNTIxIDM2LjIzLDE0LjIyOSBDMzYuMjQxLDE0LjU4MSAzNi4zMjQsMTQuODM3IDM2LjQxMSwxNS4wMTggQzM2LjQ1OCwxNS4xMTkgMzYuNTE1LDE1LjIxNSAzNi41ODEsMTUuMzAzIEMzNi41ODIsMTUuMzA0IDM2LjU4MywxNS4zMDYgMzYuNTg1LDE1LjMwOCBMMzYuNTg1LDE1LjMwOCBDMzYuODkxLDE1LjcxMyAzNy4zOTgsMTUuOTYyIDM4LjE1MSwxNS45NjIgQzM5Ljg5NCwxNS45NjIgNDAuOTQ0LDE0LjkzOCA0MS41NjIsMTMuOTA5IEw0MC43MDQsMTMuMjM5IEM0MC4zMzMsMTMuNzc0IDM5LjgzOSwxNC4xNzUgMzkuMzI0LDE0LjE3NSBDMzguODQ2LDE0LjE3NSAzOC41NzksMTMuODc4IDM4LjU3OSwxMy4zNzIgQzM4LjU3OSwxMi45MzUgMzguODg5LDExLjg2OCAzOS4yMjksMTAuNTMgQzM5LjM0NCwxMC4wODMgMzkuNTE2LDkuNDAxIDM5LjcwOCw4LjY0NCBMNDEuMzAyLDguNjQ0IEM0MS41MzksOC42NDQgNDEuNjc4LDguNTQ5IDQxLjczMiw4LjMyOSBDNDEuODA4LDguMDEyIDQxLjk0OCw3LjM3MiA0MS45ODgsNy4yMjEgQzQyLjAzOSw3LjAyMyA0MS45MjMsNi44NzMgNDEuNzAxLDYuODczIFogTTM0LjEyNiw2LjM0OCBDMzQuOTA1LDYuMzQ4IDM1LjY1Myw1LjcwNiAzNS43ODMsNC45MTIgQzM1LjkzNyw0LjE0MSAzNS40MDksMy40OTggMzQuNjMsMy40OTggQzMzLjg1MSwzLjQ5OCAzMy4xMDMsNC4xNDEgMzIuOTcxLDQuOTEyIEMzMi44MTksNS43MDYgMzMuMzI2LDYuMzQ4IDM0LjEyNiw2LjM0OCBaIiBmaWxsPSIjZmZmIj48L3BhdGg+PC9nPjwvc3ZnPg==);
        }
        .PIN_1663335131296_button_pin.PIN_1663335131296_red.PIN_1663335131296_ja {
            background-image: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIGhlaWdodD0iMThweCIgd2lkdGg9IjQxcHgiIHZpZXdCb3g9IjAgMCA0MSAxOCI+PGc+PHBhdGggZD0iTTE5LjgyMiw3LjE3MyBDMTkuODIyLDYuNTEgMTkuODM1LDYuMjc2IDE5Ljg4Nyw1Ljk2NCBMMTguMTQ1LDUuOTY0IEMxOC4xOTcsNi4yODkgMTguMTk3LDYuNDk3IDE4LjE5Nyw3LjE2IEwxOC4yMSwxMy4xOTIgQzE4LjIxLDEzLjk0NiAxOC4yMjMsMTQuMTY3IDE4LjI0OSwxNC4zODggQzE4LjMyNywxNS4wMjUgMTguNTIyLDE1LjQ0MSAxOC44ODYsMTUuNzE0IEMxOS4zOTMsMTYuMTA0IDIwLjI5LDE2LjI3MyAyMS45MjgsMTYuMjczIEMyMi43MjEsMTYuMjczIDI0LjM1OSwxNi4xOTUgMjUuMTI2LDE2LjExNyBDMjYuNTA0LDE1Ljk4NyAyNi41NjksMTUuOTc0IDI2Ljg0MiwxNS45NzQgTDI2Ljc2NCwxNC4yNDUgQzI2LjE5MiwxNC40MTQgMjUuOTA2LDE0LjQ3OSAyNS4yODIsMTQuNTU3IEMyNC4zMzMsMTQuNjg3IDIzLjEzNywxNC43NjUgMjIuMjY2LDE0Ljc2NSBDMjEuMDA1LDE0Ljc2NSAyMC4yNjQsMTQuNjQ4IDIwLjA0MywxNC40MjcgQzE5Ljg2MSwxNC4yNDUgMTkuODA5LDEzLjk1OSAxOS44MDksMTMuMjMxIEMxOS44MDksMTMuMTc5IDE5LjgwOSwxMy4xMDEgMTkuODIyLDEzLjAyMyBMMTkuODIyLDExLjMwNyBDMjEuOTkzLDEwLjkwNCAyNC4wMDgsMTAuMjI4IDI1LjkzMiw5LjI0IEwyNi4yNyw5LjA3MSBDMjYuMzc0LDkuMDE5IDI2LjQsOS4wMDYgMjYuNTQzLDguOTU0IEwyNS41MDMsNy40ODUgQzI0LjY1OCw4LjI3OCAyMS43ODUsOS40MzUgMTkuODIyLDkuNzk5IEwxOS44MjIsNy4xNzMgWiBNMjcuMzEsNC44NzIgQzI2LjQ5MSw0Ljg3MiAyNS44MTUsNS41NDggMjUuODE1LDYuMzY3IEMyNS44MTUsNy4xOTkgMjYuNDkxLDcuODc1IDI3LjMxLDcuODc1IEMyOC4xNDIsNy44NzUgMjguODE4LDcuMTk5IDI4LjgxOCw2LjM2NyBDMjguODE4LDUuNTQ4IDI4LjE0Miw0Ljg3MiAyNy4zMSw0Ljg3MiBMMjcuMzEsNC44NzIgWiBNMjcuMzEsNS41MjIgQzI3Ljc5MSw1LjUyMiAyOC4xNjgsNS44OTkgMjguMTY4LDYuMzY3IEMyOC4xNjgsNi44MzUgMjcuNzkxLDcuMjI1IDI3LjMxLDcuMjI1IEMyNi44NDIsNy4yMjUgMjYuNDY1LDYuODM1IDI2LjQ2NSw2LjM2NyBDMjYuNDY1LDUuODk5IDI2Ljg0Miw1LjUyMiAyNy4zMSw1LjUyMiBMMjcuMzEsNS41MjIgWiBNMzAuNTg2LDcuNjU0IEMzMS43OTUsOC4zMyAzMi44NjEsOS4xODggMzMuOTAxLDEwLjI5MyBMMzUuMDE5LDguODc2IEMzNC4wMTgsNy45MjcgMzMuMjEyLDcuMzI5IDMxLjY2NSw2LjM2NyBMMzAuNTg2LDcuNjU0IFogTTMxLjA0MSwxNi4yMzQgQzMxLjM0LDE2LjEzIDMxLjM3OSwxNi4xMTcgMzEuODk5LDE2LjAxMyBDMzMuOTE0LDE1LjU4NCAzNS41MjYsMTQuOTQ3IDM2Ljg1MiwxNC4wNjMgQzM4LjYzMywxMi44OCAzOS44NjgsMTEuMzQ2IDQwLjk3Myw4Ljk2NyBDNDAuMzEsOC40OTkgNDAuMTAyLDguMzA0IDM5LjU5NSw3LjY5MyBDMzkuMjA1LDguNzQ2IDM4Ljg0MSw5LjQ2MSAzOC4yNjksMTAuMjkzIEMzNy4yNDIsMTEuNzc1IDM2LjAzMywxMi43NzYgMzQuNDA4LDEzLjQ3OCBDMzMuMjI1LDEzLjk5OCAzMS42NzgsMTQuMzc1IDMwLjU2LDE0LjQ0IEwzMS4wNDEsMTYuMjM0IFogTTcuNDU4LDAgQzIuNSwwIDAsMy41MjIgMCw2LjQ1OSBDMCw4LjIzNyAwLjY4LDkuODE5IDIuMTM3LDEwLjQwOSBDMi4zNzYsMTAuNTA1IDIuNTksMTAuNDEyIDIuNjYsMTAuMTUgQzIuNzA4LDkuOTY5IDIuODIyLDkuNTExIDIuODczLDkuMzIgQzIuOTQzLDkuMDYxIDIuOTE2LDguOTcgMi43MjMsOC43NDQgQzIuMzAyLDguMjUzIDIuMDM0LDcuNjE3IDIuMDM0LDYuNzE2IEMyLjAzNCw0LjEwNCA0LjAwNywxLjc2NSA3LjE3MiwxLjc2NSBDOS45NzUsMS43NjUgMTEuNTE0LDMuNDYxIDExLjUxNCw1LjcyNiBDMTEuNTE0LDguNzA4IDEwLjE4MywxMS4xOCA4LjIwNiwxMS4xOCBDNy4xMTQsMTEuMTggNi4yOTcsMTAuMzI5IDYuNTU5LDkuMjMzIEM2Ljg3Miw3LjkyMiA3LjQ4LDYuNTA5IDcuNDgsNS41NjQgQzcuNDgsNC43MTcgNy4wMjIsNC4wMTEgNi4wNzIsNC4wMTEgQzQuOTU2LDQuMDExIDQuMDYsNS4xNTUgNC4wNiw2LjY4NyBDNC4wNiw3LjY2MyA0LjM5Myw4LjMyMyA0LjM5Myw4LjMyMyBDNC4zOTMsOC4zMjMgMy4yNTEsMTMuMTE3IDMuMDUxLDEzLjk1NyBDMi42NTIsMTUuNjI5IDIuOTkxLDE3LjY3OSAzLjAxOSwxNy44ODYgQzMuMDM2LDE4LjAwOSAzLjE5NSwxOC4wMzggMy4yNjcsMTcuOTQ2IEMzLjM3LDE3LjgxMiA0LjcsMTYuMTg3IDUuMTUxLDE0LjU2MiBDNS4yNzksMTQuMTAyIDUuODg1LDExLjcyIDUuODg1LDExLjcyIEM2LjI0OCwxMi40MDYgNy4zMDgsMTMuMDA5IDguNDM1LDEzLjAwOSBDMTEuNzksMTMuMDA5IDE0LjA2Niw5Ljk3OSAxNC4wNjYsNS45MjMgQzE0LjA2NiwyLjg1NyAxMS40NDQsMCA3LjQ1OCwwIFoiIGZpbGw9IiNmZmYiPjwvcGF0aD48L2c+PC9zdmc+);
        }
        .PIN_1663335131296_button_pin.PIN_1663335131296_white {
            background-color: #fff;
        }
        .PIN_1663335131296_button_pin.PIN_1663335131296_save {
            border-radius: 2px;
            text-indent: 20px;
            width: auto;
            padding: 0 4px 0 0;
            text-align: center;
            text-decoration: none;
            font: 11px/20px "Helvetica Neue", Helvetica, sans-serif;
            font-weight: bold;
            color: #fff!important;
            background: #e60023 url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIGhlaWdodD0iMzBweCIgd2lkdGg9IjMwcHgiIHZpZXdCb3g9Ii0xIC0xIDMxIDMxIj48Zz48cGF0aCBkPSJNMjkuNDQ5LDE0LjY2MiBDMjkuNDQ5LDIyLjcyMiAyMi44NjgsMjkuMjU2IDE0Ljc1LDI5LjI1NiBDNi42MzIsMjkuMjU2IDAuMDUxLDIyLjcyMiAwLjA1MSwxNC42NjIgQzAuMDUxLDYuNjAxIDYuNjMyLDAuMDY3IDE0Ljc1LDAuMDY3IEMyMi44NjgsMC4wNjcgMjkuNDQ5LDYuNjAxIDI5LjQ0OSwxNC42NjIiIGZpbGw9IiNmZmYiIHN0cm9rZT0iI2ZmZiIgc3Ryb2tlLXdpZHRoPSIxIj48L3BhdGg+PHBhdGggZD0iTTE0LjczMywxLjY4NiBDNy41MTYsMS42ODYgMS42NjUsNy40OTUgMS42NjUsMTQuNjYyIEMxLjY2NSwyMC4xNTkgNS4xMDksMjQuODU0IDkuOTcsMjYuNzQ0IEM5Ljg1NiwyNS43MTggOS43NTMsMjQuMTQzIDEwLjAxNiwyMy4wMjIgQzEwLjI1MywyMi4wMSAxMS41NDgsMTYuNTcyIDExLjU0OCwxNi41NzIgQzExLjU0OCwxNi41NzIgMTEuMTU3LDE1Ljc5NSAxMS4xNTcsMTQuNjQ2IEMxMS4xNTcsMTIuODQyIDEyLjIxMSwxMS40OTUgMTMuNTIyLDExLjQ5NSBDMTQuNjM3LDExLjQ5NSAxNS4xNzUsMTIuMzI2IDE1LjE3NSwxMy4zMjMgQzE1LjE3NSwxNC40MzYgMTQuNDYyLDE2LjEgMTQuMDkzLDE3LjY0MyBDMTMuNzg1LDE4LjkzNSAxNC43NDUsMTkuOTg4IDE2LjAyOCwxOS45ODggQzE4LjM1MSwxOS45ODggMjAuMTM2LDE3LjU1NiAyMC4xMzYsMTQuMDQ2IEMyMC4xMzYsMTAuOTM5IDE3Ljg4OCw4Ljc2NyAxNC42NzgsOC43NjcgQzEwLjk1OSw4Ljc2NyA4Ljc3NywxMS41MzYgOC43NzcsMTQuMzk4IEM4Ljc3NywxNS41MTMgOS4yMSwxNi43MDkgOS43NDksMTcuMzU5IEM5Ljg1NiwxNy40ODggOS44NzIsMTcuNiA5Ljg0LDE3LjczMSBDOS43NDEsMTguMTQxIDkuNTIsMTkuMDIzIDkuNDc3LDE5LjIwMyBDOS40MiwxOS40NCA5LjI4OCwxOS40OTEgOS4wNCwxOS4zNzYgQzcuNDA4LDE4LjYyMiA2LjM4NywxNi4yNTIgNi4zODcsMTQuMzQ5IEM2LjM4NywxMC4yNTYgOS4zODMsNi40OTcgMTUuMDIyLDYuNDk3IEMxOS41NTUsNi40OTcgMjMuMDc4LDkuNzA1IDIzLjA3OCwxMy45OTEgQzIzLjA3OCwxOC40NjMgMjAuMjM5LDIyLjA2MiAxNi4yOTcsMjIuMDYyIEMxNC45NzMsMjIuMDYyIDEzLjcyOCwyMS4zNzkgMTMuMzAyLDIwLjU3MiBDMTMuMzAyLDIwLjU3MiAxMi42NDcsMjMuMDUgMTIuNDg4LDIzLjY1NyBDMTIuMTkzLDI0Ljc4NCAxMS4zOTYsMjYuMTk2IDEwLjg2MywyNy4wNTggQzEyLjA4NiwyNy40MzQgMTMuMzg2LDI3LjYzNyAxNC43MzMsMjcuNjM3IEMyMS45NSwyNy42MzcgMjcuODAxLDIxLjgyOCAyNy44MDEsMTQuNjYyIEMyNy44MDEsNy40OTUgMjEuOTUsMS42ODYgMTQuNzMzLDEuNjg2IiBmaWxsPSIjZTYwMDIzIj48L3BhdGg+PC9nPjwvc3ZnPg==) 3px 50% no-repeat;
            background-size: 14px 14px;
            -webkit-font-smoothing: antialiased;
        }
        .PIN_1663335131296_button_pin.PIN_1663335131296_save:hover {
            background-color: #e60023;
            box-shadow: none;
            color: #fff!important;
        }
        .PIN_1663335131296_button_pin.PIN_1663335131296_save .PIN_1663335131296_count {
            text-indent: 0;
            position: absolute;
            color: #555;
            background: #efefef;
            border-radius: 2px;
        }
        .PIN_1663335131296_button_pin.PIN_1663335131296_save .PIN_1663335131296_count::before {
            content: "";
            position: absolute;
        }
        .PIN_1663335131296_button_pin.PIN_1663335131296_save.PIN_1663335131296_beside .PIN_1663335131296_count {
            right: -46px;
            height: 20px;
            width: 40px;
            font-size: 10px;
            line-height: 20px;
        }
        .PIN_1663335131296_button_pin.PIN_1663335131296_save.PIN_1663335131296_beside .PIN_1663335131296_count::before {
            top: 3px;
            left: -4px;
            border-right: 7px solid #efefef;
            border-top: 7px solid transparent;
            border-bottom: 7px solid transparent;
        }
        .PIN_1663335131296_button_pin.PIN_1663335131296_save.PIN_1663335131296_above .PIN_1663335131296_count {
            top: -36px;
            width: 100%;
            height: 30px;
            font-size: 10px;
            line-height: 30px;
        }
        .PIN_1663335131296_button_pin.PIN_1663335131296_save.PIN_1663335131296_above .PIN_1663335131296_count::before {
            bottom: -4px;
            left: 4px;
            border-top: 7px solid #efefef;
            border-right: 7px solid transparent;
            border-left: 7px solid transparent;
        }
        .PIN_1663335131296_button_pin.PIN_1663335131296_save.PIN_1663335131296_above.PIN_1663335131296_padded {
            margin-top: 28px;
        }
        .PIN_1663335131296_button_pin.PIN_1663335131296_tall {
            height: 28px;
            width: 56px;
            border-radius: 3px;
        }
        .PIN_1663335131296_button_pin.PIN_1663335131296_tall.PIN_1663335131296_above .PIN_1663335131296_count {
            position: absolute;
            height: 37px;
            width: inherit;
            top: -37px;
            left: 0;
            line-height: 30px;
            font-size: 14px;
            background: transparent url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIGhlaWdodD0iNzZweCIgd2lkdGg9IjExNHB4IiB2aWV3Qm94PSIwIDAgMTE0IDc2Ij48Zz48cGF0aCBkPSJNOSAxQzQuNiAxIDEgNC42IDEgOXY0M2MwIDQuMyAzLjYgOCA4IDhoMjZsMTggMTVoNy41bDE2LTE1SDEwNWM0LjQgMCA4LTMuNyA4LThWOWMwLTQuNC0zLjYtOC04LThIOXoiIGZpbGw9IiNmZmYiIHN0cm9rZT0iI2I1YjViNSIgc3Ryb2tlLXdpZHRoPSIyIj48L3BhdGg+PC9nPjwvc3ZnPg==);
            background-size: cover;
        }
        .PIN_1663335131296_button_pin.PIN_1663335131296_tall.PIN_1663335131296_above.PIN_1663335131296_padded {
            margin-top: 37px;
        }
        .PIN_1663335131296_button_pin.PIN_1663335131296_tall.PIN_1663335131296_beside .PIN_1663335131296_count {
            text-indent: 5px;
            position: absolute;
            right: -63px;
            height: inherit;
            width: 63px;
            font-size: 14px;
            line-height: 28px;
            background: transparent url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIGhlaWdodD0iNTZweCIgd2lkdGg9IjEyNnB4IiB2aWV3Qm94PSIyIDAgMTMwIDYwIj48Zz48cGF0aCBkPSJNMTE5LjYgMmM0LjUgMCA4IDMuNiA4IDh2NDBjMCA0LjQtMy41IDgtOCA4SDIzLjNMMS42IDMyLjR2LTQuNkwyMy4zIDJoOTYuM3oiIGZpbGw9IiNmZmYiIHN0cm9rZT0iI2I1YjViNSIgc3Ryb2tlLXdpZHRoPSIyIj48L3BhdGg+PC9nPjwvc3ZnPg==);
            background-size: cover;
        }
        .PIN_1663335131296_button_pin.PIN_1663335131296_tall.PIN_1663335131296_beside.PIN_1663335131296_padded {
            margin-right: 63px;
        }
        .PIN_1663335131296_button_pin.PIN_1663335131296_tall.PIN_1663335131296_save {
            border-radius: 4px;
            width: auto;
            background-position-x: 6px;
            background-size: 18px 18px;
            text-indent: 29px;
            font: 14px/28px "Helvetica Neue", Helvetica, Arial, "sans-serif";
            font-weight: bold;
            padding: 0 6px 0 0;
        }
        .PIN_1663335131296_button_pin.PIN_1663335131296_tall.PIN_1663335131296_save .PIN_1663335131296_count {
            position: absolute;
            color: #555;
            font-size: 12px;
            text-indent: 0;
            background: #efefef;
            border-radius: 4px;
        }
        .PIN_1663335131296_button_pin.PIN_1663335131296_tall.PIN_1663335131296_save .PIN_1663335131296_count::before {
            content: "";
            position: absolute;
        }
        .PIN_1663335131296_button_pin.PIN_1663335131296_tall.PIN_1663335131296_save.PIN_1663335131296_above .PIN_1663335131296_count {
            font-size: 14px;
            top: -50px;
            width: 100%;
            height: 44px;
            line-height: 44px;
        }
        .PIN_1663335131296_button_pin.PIN_1663335131296_tall.PIN_1663335131296_save.PIN_1663335131296_above .PIN_1663335131296_count::before {
            bottom: -4px;
            left: 7px;
            border-top: 7px solid #efefef;
            border-right: 7px solid transparent;
            border-left: 7px solid transparent;
        }
        .PIN_1663335131296_button_pin.PIN_1663335131296_tall.PIN_1663335131296_save.PIN_1663335131296_beside .PIN_1663335131296_count {
            font-size: 14px;
            right: -63px;
            width: 56px;
            height: 28px;
            line-height: 28px;
        }
        .PIN_1663335131296_button_pin.PIN_1663335131296_tall.PIN_1663335131296_save.PIN_1663335131296_beside .PIN_1663335131296_count::before {
            top: 7px;
            left: -4px;
            border-right: 7px solid #efefef;
            border-top: 7px solid transparent;
            border-bottom: 7px solid transparent;
        }
        .PIN_1663335131296_button_pin.PIN_1663335131296_round {
            height: 16px;
            width: 16px;
            background: transparent url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIGhlaWdodD0iMzBweCIgd2lkdGg9IjMwcHgiIHZpZXdCb3g9Ii0xIC0xIDMxIDMxIj48Zz48cGF0aCBkPSJNMjkuNDQ5LDE0LjY2MiBDMjkuNDQ5LDIyLjcyMiAyMi44NjgsMjkuMjU2IDE0Ljc1LDI5LjI1NiBDNi42MzIsMjkuMjU2IDAuMDUxLDIyLjcyMiAwLjA1MSwxNC42NjIgQzAuMDUxLDYuNjAxIDYuNjMyLDAuMDY3IDE0Ljc1LDAuMDY3IEMyMi44NjgsMC4wNjcgMjkuNDQ5LDYuNjAxIDI5LjQ0OSwxNC42NjIiIGZpbGw9IiNmZmYiIHN0cm9rZT0iI2ZmZiIgc3Ryb2tlLXdpZHRoPSIxIj48L3BhdGg+PHBhdGggZD0iTTE0LjczMywxLjY4NiBDNy41MTYsMS42ODYgMS42NjUsNy40OTUgMS42NjUsMTQuNjYyIEMxLjY2NSwyMC4xNTkgNS4xMDksMjQuODU0IDkuOTcsMjYuNzQ0IEM5Ljg1NiwyNS43MTggOS43NTMsMjQuMTQzIDEwLjAxNiwyMy4wMjIgQzEwLjI1MywyMi4wMSAxMS41NDgsMTYuNTcyIDExLjU0OCwxNi41NzIgQzExLjU0OCwxNi41NzIgMTEuMTU3LDE1Ljc5NSAxMS4xNTcsMTQuNjQ2IEMxMS4xNTcsMTIuODQyIDEyLjIxMSwxMS40OTUgMTMuNTIyLDExLjQ5NSBDMTQuNjM3LDExLjQ5NSAxNS4xNzUsMTIuMzI2IDE1LjE3NSwxMy4zMjMgQzE1LjE3NSwxNC40MzYgMTQuNDYyLDE2LjEgMTQuMDkzLDE3LjY0MyBDMTMuNzg1LDE4LjkzNSAxNC43NDUsMTkuOTg4IDE2LjAyOCwxOS45ODggQzE4LjM1MSwxOS45ODggMjAuMTM2LDE3LjU1NiAyMC4xMzYsMTQuMDQ2IEMyMC4xMzYsMTAuOTM5IDE3Ljg4OCw4Ljc2NyAxNC42NzgsOC43NjcgQzEwLjk1OSw4Ljc2NyA4Ljc3NywxMS41MzYgOC43NzcsMTQuMzk4IEM4Ljc3NywxNS41MTMgOS4yMSwxNi43MDkgOS43NDksMTcuMzU5IEM5Ljg1NiwxNy40ODggOS44NzIsMTcuNiA5Ljg0LDE3LjczMSBDOS43NDEsMTguMTQxIDkuNTIsMTkuMDIzIDkuNDc3LDE5LjIwMyBDOS40MiwxOS40NCA5LjI4OCwxOS40OTEgOS4wNCwxOS4zNzYgQzcuNDA4LDE4LjYyMiA2LjM4NywxNi4yNTIgNi4zODcsMTQuMzQ5IEM2LjM4NywxMC4yNTYgOS4zODMsNi40OTcgMTUuMDIyLDYuNDk3IEMxOS41NTUsNi40OTcgMjMuMDc4LDkuNzA1IDIzLjA3OCwxMy45OTEgQzIzLjA3OCwxOC40NjMgMjAuMjM5LDIyLjA2MiAxNi4yOTcsMjIuMDYyIEMxNC45NzMsMjIuMDYyIDEzLjcyOCwyMS4zNzkgMTMuMzAyLDIwLjU3MiBDMTMuMzAyLDIwLjU3MiAxMi42NDcsMjMuMDUgMTIuNDg4LDIzLjY1NyBDMTIuMTkzLDI0Ljc4NCAxMS4zOTYsMjYuMTk2IDEwLjg2MywyNy4wNTggQzEyLjA4NiwyNy40MzQgMTMuMzg2LDI3LjYzNyAxNC43MzMsMjcuNjM3IEMyMS45NSwyNy42MzcgMjcuODAxLDIxLjgyOCAyNy44MDEsMTQuNjYyIEMyNy44MDEsNy40OTUgMjEuOTUsMS42ODYgMTQuNzMzLDEuNjg2IiBmaWxsPSIjZTYwMDIzIj48L3BhdGg+PC9nPjwvc3ZnPg==) 0 0 no-repeat;
            background-size: 16px 16px;
            box-shadow: none;
        }
        .PIN_1663335131296_button_pin.PIN_1663335131296_round.PIN_1663335131296_tall {
            height: 32px;
            width: 32px;
            background-size: 32px 32px;
        }
    </style></head>
<body class="CqOQfS">
<script type="text/javascript">
    var bodyCacheable = true;

    var exclusionReason = {"shouldRender":true,"forced":false};
    var ssrInfo = {"renderBodyTime":504,"renderTimeStamp":1663334335694}
</script>




<script>
    window.clientSideRender = false;
    window.bi.wixBiSession.isServerSide = 1;

</script>


<!--pageHtmlEmbeds.bodyStart start-->
<script type="wix/htmlEmbeds" id="pageHtmlEmbeds.bodyStart start"></script>

<script type="wix/htmlEmbeds" id="pageHtmlEmbeds.bodyStart end"></script>
<!--pageHtmlEmbeds.bodyStart end-->




<script id="wix-first-paint">
    if (window.ResizeObserver &&
        (!window.PerformanceObserver || !PerformanceObserver.supportedEntryTypes || PerformanceObserver.supportedEntryTypes.indexOf('paint') === -1)) {
        new ResizeObserver(function (entries, observer) {
            entries.some(function (entry) {
                var contentRect = entry.contentRect;
                if (contentRect.width > 0 && contentRect.height > 0) {
                    requestAnimationFrame(function (now) {
                        window.wixFirstPaint = now;
                        dispatchEvent(new CustomEvent('wixFirstPaint'));
                    });
                    observer.disconnect();
                    return true;
                }
            });
        }).observe(document.body);
    }
</script>

<pages-css id="pages-css">
    <style id="css_masterPage">
        :root{--color_0:255,255,255;--color_1:255,255,255;--color_2:0,0,0;--color_3:237,28,36;--color_4:0,136,203;--color_5:255,203,5;--color_6:114,114,114;--color_7:176,176,176;--color_8:255,255,255;--color_9:114,114,114;--color_10:176,176,176;--color_11:255,255,255;--color_12:247,247,247;--color_13:179,179,179;--color_14:89,89,89;--color_15:20,20,20;--color_16:175,206,229;--color_17:139,176,203;--color_18:9,91,150;--color_19:9,54,87;--color_20:4,37,61;--color_21:255,206,191;--color_22:255,182,160;--color_23:255,109,64;--color_24:170,73,43;--color_25:85,36,21;--color_26:193,183,234;--color_27:157,143,212;--color_28:91,66,191;--color_29:61,44,127;--color_30:30,22,64;--color_31:187,216,195;--color_32:145,177,154;--color_33:46,99,61;--color_34:12,54,24;--color_35:2,20,8;--font_0:normal normal bold 35px/1.4em avenir-lt-w01_35-light1475496,avenir-lt-w05_35-light,sans-serif;--font_1:normal normal normal 16px/1.4em din-next-w01-light,din-next-w02-light,din-next-w10-light,sans-serif;--font_2:normal normal bold 30px/1.4em avenir-lt-w01_35-light1475496,avenir-lt-w05_35-light,sans-serif;--font_3:normal normal bold 26px/1.4em avenir-lt-w01_35-light1475496,avenir-lt-w05_35-light,sans-serif;--font_4:normal normal bold 20px/1.4em barlow-extralight,barlow,sans-serif;--font_5:normal normal bold 66px/1.4em avenir-lt-w01_35-light1475496,avenir-lt-w05_35-light,sans-serif;--font_6:normal normal normal 22px/1.4em avenir-lt-w01_85-heavy1475544,avenir-lt-w05_85-heavy,sans-serif;--font_7:normal normal normal 22px/1.4em barlow-medium,barlow,sans-serif;--font_8:normal normal bold 18px/1.4em barlow-extralight,barlow,sans-serif;--font_9:normal normal bold 16px/1.4em barlow-extralight,barlow,sans-serif;--font_10:normal normal normal 12px/1.4em din-next-w01-light,din-next-w02-light,din-next-w10-light,sans-serif;--wix-ads-height:50px;--wix-ads-top-height:50px;--site-width:100%;--above-all-z-index:100000;--portals-z-index:100001;--minViewportSize:320;--maxViewportSize:1920}.font_0{font:var(--font_0);color:rgb(var(--color_15));letter-spacing:0em}.font_1{font:var(--font_1);color:rgb(var(--color_14));letter-spacing:0em}.font_2{font:var(--font_2);color:rgb(var(--color_15));letter-spacing:0em}.font_3{font:var(--font_3);color:rgb(var(--color_15));letter-spacing:0em}.font_4{font:var(--font_4);color:rgb(var(--color_15));letter-spacing:0em}.font_5{font:var(--font_5);color:rgb(var(--color_15));letter-spacing:0em}.font_6{font:var(--font_6);color:rgb(var(--color_15));letter-spacing:0em}.font_7{font:var(--font_7);color:rgb(var(--color_15));letter-spacing:0em}.font_8{font:var(--font_8);color:rgb(var(--color_15));letter-spacing:0em}.font_9{font:var(--font_9);color:rgb(var(--color_15));letter-spacing:0em}.font_10{font:var(--font_10);color:rgb(var(--color_14));letter-spacing:0em}.color_0{color:rgb(var(--color_0))}.color_1{color:rgb(var(--color_1))}.color_2{color:rgb(var(--color_2))}.color_3{color:rgb(var(--color_3))}.color_4{color:rgb(var(--color_4))}.color_5{color:rgb(var(--color_5))}.color_6{color:rgb(var(--color_6))}.color_7{color:rgb(var(--color_7))}.color_8{color:rgb(var(--color_8))}.color_9{color:rgb(var(--color_9))}.color_10{color:rgb(var(--color_10))}.color_11{color:rgb(var(--color_11))}.color_12{color:rgb(var(--color_12))}.color_13{color:rgb(var(--color_13))}.color_14{color:rgb(var(--color_14))}.color_15{color:rgb(var(--color_15))}.color_16{color:rgb(var(--color_16))}.color_17{color:rgb(var(--color_17))}.color_18{color:rgb(var(--color_18))}.color_19{color:rgb(var(--color_19))}.color_20{color:rgb(var(--color_20))}.color_21{color:rgb(var(--color_21))}.color_22{color:rgb(var(--color_22))}.color_23{color:rgb(var(--color_23))}.color_24{color:rgb(var(--color_24))}.color_25{color:rgb(var(--color_25))}.color_26{color:rgb(var(--color_26))}.color_27{color:rgb(var(--color_27))}.color_28{color:rgb(var(--color_28))}.color_29{color:rgb(var(--color_29))}.color_30{color:rgb(var(--color_30))}.color_31{color:rgb(var(--color_31))}.color_32{color:rgb(var(--color_32))}.color_33{color:rgb(var(--color_33))}.color_34{color:rgb(var(--color_34))}.color_35{color:rgb(var(--color_35))}.backcolor_0{background-color:rgb(var(--color_0))}.backcolor_1{background-color:rgb(var(--color_1))}.backcolor_2{background-color:rgb(var(--color_2))}.backcolor_3{background-color:rgb(var(--color_3))}.backcolor_4{background-color:rgb(var(--color_4))}.backcolor_5{background-color:rgb(var(--color_5))}.backcolor_6{background-color:rgb(var(--color_6))}.backcolor_7{background-color:rgb(var(--color_7))}.backcolor_8{background-color:rgb(var(--color_8))}.backcolor_9{background-color:rgb(var(--color_9))}.backcolor_10{background-color:rgb(var(--color_10))}.backcolor_11{background-color:rgb(var(--color_11))}.backcolor_12{background-color:rgb(var(--color_12))}.backcolor_13{background-color:rgb(var(--color_13))}.backcolor_14{background-color:rgb(var(--color_14))}.backcolor_15{background-color:rgb(var(--color_15))}.backcolor_16{background-color:rgb(var(--color_16))}.backcolor_17{background-color:rgb(var(--color_17))}.backcolor_18{background-color:rgb(var(--color_18))}.backcolor_19{background-color:rgb(var(--color_19))}.backcolor_20{background-color:rgb(var(--color_20))}.backcolor_21{background-color:rgb(var(--color_21))}.backcolor_22{background-color:rgb(var(--color_22))}.backcolor_23{background-color:rgb(var(--color_23))}.backcolor_24{background-color:rgb(var(--color_24))}.backcolor_25{background-color:rgb(var(--color_25))}.backcolor_26{background-color:rgb(var(--color_26))}.backcolor_27{background-color:rgb(var(--color_27))}.backcolor_28{background-color:rgb(var(--color_28))}.backcolor_29{background-color:rgb(var(--color_29))}.backcolor_30{background-color:rgb(var(--color_30))}.backcolor_31{background-color:rgb(var(--color_31))}.backcolor_32{background-color:rgb(var(--color_32))}.backcolor_33{background-color:rgb(var(--color_33))}.backcolor_34{background-color:rgb(var(--color_34))}.backcolor_35{background-color:rgb(var(--color_35))}#SITE_CONTAINER.focus-ring-active :not(.has-custom-focus):not(.ignore-focus):focus, #SITE_CONTAINER.focus-ring-active :not(.has-custom-focus):not(.ignore-focus):focus ~ .wixSdkShowFocusOnSibling{box-shadow:0 0 0 1px #ffffff, 0 0 0 3px #116dff !important;z-index:1}[data-mesh-id=SITE_HEADERinlineContent]{height:auto;width:100%}[data-mesh-id=SITE_HEADERinlineContent-gridContainer]{position:static;display:grid;height:auto;width:100%;min-height:auto;grid-template-rows:1fr;grid-template-columns:100%}[data-mesh-id=SITE_HEADERinlineContent-gridContainer] > [id="comp-kdmsih9l"]{position:relative;margin:0px 0px 0px calc((100% - 980px) * 0.5);left:0px;grid-area:1 / 1 / 2 / 2;justify-self:start;align-self:start}[data-mesh-id=SOSP_CONTAINER_CUSTOM_IDinlineContent]{height:auto;width:291px}[data-mesh-id=SOSP_CONTAINER_CUSTOM_IDinlineContent-gridContainer]{position:static;display:grid;height:auto;width:100%;min-height:auto;grid-template-rows:min-content 1fr;grid-template-columns:100%}[data-mesh-id=SOSP_CONTAINER_CUSTOM_IDinlineContent-gridContainer] > [id="comp-kdmrs3cn"]{position:relative;margin:100px 0px -86px 0;left:20px;grid-area:1 / 1 / 2 / 2;justify-self:start;align-self:start}[data-mesh-id=SOSP_CONTAINER_CUSTOM_IDinlineContent-gridContainer] > [id="comp-kdzvucho"]{position:relative;margin:0px 0px 56px 0;left:20px;grid-area:2 / 1 / 3 / 2;justify-self:start;align-self:start}[data-mesh-id=SITE_FOOTERinlineContent]{height:auto;width:100%}[data-mesh-id=SITE_FOOTERinlineContent-gridContainer]{position:static;display:grid;height:auto;width:100%;min-height:auto;grid-template-rows:min-content 1fr;grid-template-columns:100%}[data-mesh-id=SITE_FOOTERinlineContent-gridContainer] > [id="comp-kdmvddgy"]{position:relative;margin:0px 0px 17px calc((100% - 980px) * 0.5);left:0px;grid-area:1 / 1 / 2 / 2;justify-self:start;align-self:start}[data-mesh-id=SITE_FOOTERinlineContent-gridContainer] > [id="comp-kdmvot8b"]{position:relative;margin:0px 0px 63px calc((100% - 980px) * 0.5);left:6px;grid-area:2 / 1 / 3 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdmsihb7inlineContent]{height:auto;width:100%}[data-mesh-id=comp-kdmsihb7inlineContent-gridContainer]{position:static;display:grid;height:auto;width:100%;min-height:auto;grid-template-rows:1fr;grid-template-columns:100%}[data-mesh-id=comp-kdmsihb7inlineContent-gridContainer] > [id="comp-kyfn4iuj"]{position:relative;margin:19px 0px 22px calc((100% - 176px) * 0);left:1px;grid-area:1 / 1 / 2 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdmsir58inlineContent]{height:auto;width:100%}[data-mesh-id=comp-kdmsir58inlineContent-gridContainer]{position:static;display:grid;height:auto;width:100%;min-height:auto;grid-template-rows:1fr;grid-template-columns:100%}[data-mesh-id=comp-kdmsir58inlineContent-gridContainer] > [id="comp-kdmrh8vz"]{position:relative;margin:28px 0px 35px calc((100% - 636px) * 0.5);left:0px;grid-area:1 / 1 / 2 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdoiv97iinlineContent]{height:auto;width:100%}[data-mesh-id=comp-kdoiv97iinlineContent-gridContainer]{position:static;display:grid;height:auto;width:100%;min-height:auto;grid-template-rows:1fr;grid-template-columns:100%}[data-mesh-id=comp-kdoiv97iinlineContent-gridContainer] > [id="comp-kdoiv99l1"]{position:relative;margin:34px 0px 41px calc((100% - 168px) * 1);left:8px;grid-area:1 / 1 / 2 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdmvddhuinlineContent]{height:auto;width:100%}[data-mesh-id=comp-kdmvddhuinlineContent-gridContainer]{position:static;display:grid;height:auto;width:100%;min-height:518px;grid-template-rows:repeat(6, min-content) 1fr;grid-template-columns:100%}[data-mesh-id=comp-kdmvddhuinlineContent-gridContainer] > [id="comp-kyfn5b7m"]{position:relative;margin:72px 0px 60px calc((100% - 324px) * 0.5);left:1px;grid-area:1 / 1 / 4 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdmvddhuinlineContent-gridContainer] > [id="comp-kdmviuth"]{position:relative;margin:69px 0px 0 calc((100% - 324px) * 0.5);left:76px;grid-area:1 / 1 / 2 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdmvddhuinlineContent-gridContainer] > [id="comp-kdmvjwdk"]{position:relative;margin:0px 0px 63px calc((100% - 324px) * 0.5);left:77px;grid-area:2 / 1 / 3 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdmvddhuinlineContent-gridContainer] > [id="comp-kdmvmk7n"]{position:relative;margin:0px 0px 55px calc((100% - 324px) * 0.5);left:18px;grid-area:4 / 1 / 5 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdmvddhuinlineContent-gridContainer] > [id="comp-kdmvo5ke"]{position:relative;margin:0px 0px 0 calc((100% - 324px) * 0.5);left:18px;grid-area:5 / 1 / 6 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdmvddhuinlineContent-gridContainer] > [id="comp-kdmwh2tg"]{position:relative;margin:0px 0px 6px calc((100% - 324px) * 0.5);left:18px;grid-area:6 / 1 / 7 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdmvddhuinlineContent-gridContainer] > [id="comp-kdmvqgp9"]{position:relative;margin:0px 0px 10px calc((100% - 324px) * 0.5);left:18px;grid-area:7 / 1 / 8 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdmvemejinlineContent]{height:auto;width:100%}[data-mesh-id=comp-kdmvemejinlineContent-gridContainer]{position:static;display:grid;height:auto;width:100%;min-height:auto;grid-template-rows:1fr;grid-template-columns:100%}[data-mesh-id=comp-kdmvemejinlineContent-gridContainer] > [id="comp-kdmvsx7c"]{position:relative;margin:62px 0px 51px calc((100% - 328px) * 0.5);left:4px;grid-area:1 / 1 / 2 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdmveqzuinlineContent]{height:auto;width:100%}[data-mesh-id=comp-kdmveqzuinlineContent-gridContainer]{position:static;display:grid;height:auto;width:100%;min-height:518px;grid-template-rows:repeat(4, min-content) 1fr;grid-template-columns:100%}[data-mesh-id=comp-kdmveqzuinlineContent-gridContainer] > [id="comp-kdmwbq4r"]{position:relative;margin:62px 0px 37px calc((100% - 328px) * 0.5);left:50px;grid-area:1 / 1 / 2 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdmveqzuinlineContent-gridContainer] > [id="comp-kdmwc0ek"]{position:relative;margin:0px 0px 38px calc((100% - 328px) * 0.5);left:50px;grid-area:2 / 1 / 3 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdmveqzuinlineContent-gridContainer] > [id="comp-kdmwc9xh"]{position:relative;margin:0px 0px 37px calc((100% - 328px) * 0.5);left:50px;grid-area:3 / 1 / 4 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdmveqzuinlineContent-gridContainer] > [id="comp-kdmwd2c5"]{position:relative;margin:0px 0px 38px calc((100% - 328px) * 0.5);left:50px;grid-area:4 / 1 / 5 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdmveqzuinlineContent-gridContainer] > [id="comp-kdmwcpui"]{position:relative;margin:0px 0px 10px calc((100% - 328px) * 0.5);left:50px;grid-area:5 / 1 / 6 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdmvsx7cinlineContent]{height:auto;width:320px}[data-mesh-id=comp-kdmvsx7cinlineContent-gridContainer]{position:static;display:grid;height:auto;width:100%;min-height:auto;grid-template-rows:1fr;grid-template-columns:100%}[data-mesh-id=comp-kdmvsx7cinlineContent-gridContainer] > [id="comp-kdmvsx7w"]{position:relative;margin:0px 0px 0px 0;left:0px;grid-area:1 / 1 / 2 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdmvsx7winlineContent]{height:auto;width:320px}[data-mesh-id=comp-kdmvsx7winlineContent-gridContainer]{position:static;display:grid;height:auto;width:100%;min-height:auto;grid-template-rows:repeat(5, min-content) 1fr;grid-template-columns:100%}[data-mesh-id=comp-kdmvsx7winlineContent-gridContainer] > [id="comp-kdmvsx892"]{position:relative;margin:0px 0px 35px 0;left:5px;grid-area:1 / 1 / 2 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdmvsx7winlineContent-gridContainer] > [id="comp-kdmvsx8d"]{position:relative;margin:0px 0px 24px 0;left:5px;grid-area:2 / 1 / 3 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdmvsx7winlineContent-gridContainer] > [id="comp-kdmvsx8j"]{position:relative;margin:0px 0px 24px 0;left:171px;grid-area:2 / 1 / 3 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdmvsx7winlineContent-gridContainer] > [id="comp-kdmvsx8l1"]{position:relative;margin:0px 0px 45px 0;left:5px;grid-area:3 / 1 / 4 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdmvsx7winlineContent-gridContainer] > [id="comp-kdmvsx8n3"]{position:relative;margin:0px 0px 45px 0;left:171px;grid-area:3 / 1 / 4 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdmvsx7winlineContent-gridContainer] > [id="comp-kdmvsx8p4"]{position:relative;margin:0px 0px 32px 0;left:5px;grid-area:4 / 1 / 5 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdmvsx7winlineContent-gridContainer] > [id="comp-kdmvsx8u"]{position:relative;margin:0px 0px 7px 0;left:5px;grid-area:5 / 1 / 6 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdmvsx7winlineContent-gridContainer] > [id="comp-kdmvsx8x1"]{position:relative;margin:0px 0px 7px 0;left:5px;grid-area:6 / 1 / 7 / 2;justify-self:start;align-self:start}[id="soapAfterPagesContainer"].page-without-sosp [data-mesh-id=soapAfterPagesContainerinlineContent]{height:auto;width:100%;position:static;min-height:auto;padding-bottom:0px;box-sizing:border-box}[id="soapAfterPagesContainer"].page-without-sosp [data-mesh-id=soapAfterPagesContainerinlineContent-gridContainer] > [id="CONTROLLER_COMP_CUSTOM_ID"]{position:absolute;top:15px;left:21px;margin-left:calc((100% - 980px) * 0.5)}[id="soapAfterPagesContainer"].page-with-sosp [data-mesh-id=soapAfterPagesContainerinlineContent]{height:auto;width:100%;display:flex}[id="soapAfterPagesContainer"].page-with-sosp [data-mesh-id=soapAfterPagesContainerinlineContent-gridContainer]{position:static;display:grid;height:auto;width:100%;min-height:auto;margin-top:-7px;grid-template-rows:1fr;grid-template-columns:100%;padding-bottom:0px;box-sizing:border-box}[id="soapAfterPagesContainer"].page-with-sosp [data-mesh-id=soapAfterPagesContainerinlineContent-gridContainer] > [id="CONTROLLER_COMP_CUSTOM_ID"]{position:absolute;top:-98px;left:21px;margin-left:calc((100% - 980px) * 0.5)}[id="soapAfterPagesContainer"].page-with-sosp [data-mesh-id=soapAfterPagesContainerinlineContent-gridContainer] > [id="SOSP_CONTAINER_CUSTOM_ID"]{position:relative;margin:0px 0px 0 calc((100% - 980px) * 0.5);left:0px;grid-area:1 / 1 / 2 / 2;justify-self:start;align-self:start}#comp-ki5t1mt7{width:370px;height:610px;justify-self:end;align-self:end;position:absolute;grid-area:1 / 1 / 2 / 2;pointer-events:auto}#comp-kdmrs3cn{width:253px;height:182px}#comp-kdzvucho{width:251px;height:400px}#comp-kdmvot8b{width:969px;height:25px}#comp-kyfn4iuj{width:106px;height:72px}#comp-kdmrh8vz{width:637px;height:50px}#comp-kdoiv99l1{width:150px;height:38px}#comp-kyfn5b7m{width:86px;height:58px}#comp-kdmviuth{width:250px;height:36px}#comp-kdmvjwdk{width:250px;height:22px}#comp-kdmvmk7n{width:101px;height:21px}#comp-kdmvo5ke{width:255px;height:26px}#comp-kdmwh2tg{width:255px;height:75px}#comp-kdmvqgp9{width:255px;height:50px}#comp-kdmwbq4r{width:279px;height:31px}#comp-kdmwc0ek{width:279px;height:31px}#comp-kdmwc9xh{width:279px;height:31px}#comp-kdmwd2c5{width:279px;height:31px}#comp-kdmwcpui{width:279px;height:31px}#comp-kdmvsx892{width:313px;height:35px}#comp-kdmvsx8d{width:150px;height:41px}#comp-kdmvsx8j{width:149px;height:41px}#comp-kdmvsx8l1{width:150px;height:41px}#comp-kdmvsx8n3{width:149px;height:41px}#comp-kdmvsx8p4{width:315px;height:68px}#comp-kdmvsx8u{width:315px;height:51px}#comp-kdmvsx8x1{width:315px;height:19px}#masterPage{left:0;margin-left:0;width:100%;min-width:100%;}#SITE_HEADER{left:0;margin-left:0;width:100%;min-width:100%;--pinned-layer-in-container:50;--above-all-in-container:49}#PAGES_CONTAINER{left:0;margin-left:0;width:100%;min-width:100%;--pinned-layer-in-container:51;--above-all-in-container:49}#SOSP_CONTAINER_CUSTOM_ID{width:291px;--pinned-layer-in-container:53;--above-all-in-container:49}#SITE_FOOTER{left:0;margin-left:0;width:100%;min-width:100%;--pinned-layer-in-container:54;--above-all-in-container:49}#comp-kdmsih9l{margin-left:auto;margin-right:auto;width:calc(100% - 80px - 80px);min-width:100%}#SITE_PAGES{left:0;margin-left:0;width:100%;min-width:100%}#comp-kdmvddgy{margin-left:auto;margin-right:auto;width:calc(100% - 80px - 80px);min-width:100%}#comp-kdmsihb7{width:176px}#comp-kdmsir58{width:636px}#comp-kdoiv97i{width:168px}#comp-kdmvddhu{width:324px}#comp-kdmvemej{width:328px}#comp-kdmveqzu{width:328px}#comp-kdmvsx7c{width:320px}#comp-kdmvsx7w{width:320px}#comp-ki5t1mt7-pinned-layer{z-index:calc(var(--pinned-layers-in-page, 0) + 55);--above-all-in-container:10000}#masterPage.landingPage #SITE_HEADER{display:none}#masterPage.landingPage #CONTROLLER_COMP_CUSTOM_ID{display:none}#masterPage.landingPage #SOSP_CONTAINER_CUSTOM_ID{display:none}#masterPage.landingPage #SITE_FOOTER{display:none}#masterPage.landingPage #SITE_HEADER-placeholder{display:none}#masterPage.landingPage #SITE_FOOTER-placeholder{display:none}#masterPage:not(.landingPage) #PAGES_CONTAINER{margin-top:0px;margin-bottom:0px}#CONTROLLER_COMP_CUSTOM_ID{--pinned-layer-in-container:52;--above-all-in-container:49}
        @font-face {
            font-display: block;
            font-family:"Avenir-LT-W01_35-Light1475496";
            src:url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/edefe737-dc78-4aa3-ad03-3c6f908330ed.eot?#iefix");
            src:url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/edefe737-dc78-4aa3-ad03-3c6f908330ed.eot?#iefix") format("eot"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/0078f486-8e52-42c0-ad81-3c8d3d43f48e.woff2") format("woff2"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/908c4810-64db-4b46-bb8e-823eb41f68c0.woff") format("woff"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/4577388c-510f-4366-addb-8b663bcc762a.ttf") format("truetype"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/b0268c31-e450-4159-bfea-e0d20e2b5c0c.svg#b0268c31-e450-4159-bfea-e0d20e2b5c0c") format("svg");
        }
        @font-face {
            font-family: "Avenir-LT-W05_35-Light";
            src: url("//static.parastorage.com/services/santa-resources/resources/viewer/user-site-fonts/fonts/Avenir_Family_Pack/v1/AvenirLTW05-35Light.woff2") format("woff2"),
            url("//static.parastorage.com/services/santa-resources/resources/viewer/user-site-fonts/fonts/Avenir_Family_Pack/v1/AvenirLTW05-35Light.woff") format("woff");
        }
        @font-face {
            font-display: block;
            font-family: "DIN-Next-W01-Light";
            src: url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/3e0b2cd7-9657-438b-b4af-e04122e8f1f7.eot?#iefix");
            src: url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/3e0b2cd7-9657-438b-b4af-e04122e8f1f7.eot?#iefix") format("eot"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/bc176270-17fa-4c78-a343-9fe52824e501.woff") format("woff"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/3516f91d-ac48-42cd-acfe-1be691152cc4.ttf") format("truetype"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/d1b1e866-a411-42ba-8f75-72bf28e23694.svg#d1b1e866-a411-42ba-8f75-72bf28e23694") format("svg");
        }
        @font-face {
            font-display: block;
            font-family: "DIN-Next-W02-Light";
            src: url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/48e5a0e1-2d56-46e5-8fc4-3d6d5c973cbf.eot?#iefix");
            src: url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/48e5a0e1-2d56-46e5-8fc4-3d6d5c973cbf.eot?#iefix") format("eot"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/07d62b21-8d7a-4c36-be86-d32ab1089972.woff") format("woff"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/c0050890-bbed-44b9-94df-2611d72dbb06.ttf") format("truetype"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/9f774d17-c03a-418e-a375-34f3beecbc7a.svg#9f774d17-c03a-418e-a375-34f3beecbc7a") format("svg");
        }
        @font-face {
            font-display: block;
            font-family: "DIN-Next-W10-Light";
            src: url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/3d009cd7-c8fe-40c0-93da-74f4ea8c530b.eot?#iefix");
            src: url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/3d009cd7-c8fe-40c0-93da-74f4ea8c530b.eot?#iefix") format("eot"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/a9e95a29-98a7-404a-90ee-1929ad09c696.woff") format("woff"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/0a7663fd-eae8-4e50-a67a-225271f8cceb.ttf") format("truetype"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/58ae9be9-5d95-44b6-8b6c-e6da6a46822c.svg#58ae9be9-5d95-44b6-8b6c-e6da6a46822c") format("svg");
        }
        @font-face {
            font-display: block;
            font-family: "Barlow-ExtraLight";
            src: url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/51v0xj5VPw1cLYHNhfd8NFtXRa8TVwTICgirnJhmVJw.woff2") format("woff2"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/51v0xj5VPw1cLYHNhfd8ND8E0i7KZn-EPnyo3HZu7kw.woff") format("woff"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/51v0xj5VPw1cLYHNhfd8NKCWcynf_cDxXwCLxiixG1c.ttf") format("truetype"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/barlow-extralight-Svg.svg") format("svg");
        }
        @font-face {
            font-display: block;
            font-family: "Barlow-ExtraLight";
            font-weight: 700;
            src: url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/2woyxyDnPU0v4IiqYU9D1g.woff2") format("woff2"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/-HJgNsTwx9qXGSxqew62RQ.woff") format("woff"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/qoExc9IJQUjYXhlVZNNLgg.ttf") format("truetype"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/barlow-extralight-700-Svg.svg") format("svg");
        }
        @font-face {
            font-display: block;
            font-family: "Barlow-ExtraLight";
            font-style: italic;
            src: url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/14AxwKgJhKIO-YYUP_KtZag5eI2G47JWe0-AuFtD150.woff2") format("woff2"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/14AxwKgJhKIO-YYUP_KtZdIh4imgI8P11RFo6YPCPC0.woff") format("woff"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/14AxwKgJhKIO-YYUP_KtZV02b4v3fUxqf9CZJ1qUoIA.ttf") format("truetype"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/barlow-extralight-italic-Svg.svg") format("svg");
        }
        @font-face {
            font-display: block;
            font-family: "Barlow-ExtraLight";
            font-weight: 700;
            font-style: italic;
            src: url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/cdbGxfKO8gdkBd5U5TuXqPesZW2xOQ-xsNqO47m55DA.woff2") format("woff2"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/W1XpMGU0WrpbCawEdG1FM_esZW2xOQ-xsNqO47m55DA.woff") format("woff"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/_lIpJP17FZmSeklpAeOdnvesZW2xOQ-xsNqO47m55DA.ttf") format("truetype"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/barlow-extralight-700-italic-Svg.svg") format("svg");
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: italic;
            font-weight: 400;
            src: local('Barlow Italic'), local('Barlow-Italic'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHrv4kjgoGqM7E_Cfs0wH8DnzcjTrA.woff2") format('woff2');
            unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: italic;
            font-weight: 400;
            src: local('Barlow Italic'), local('Barlow-Italic'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHrv4kjgoGqM7E_Cfs1wH8DnzcjTrA.woff2") format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: italic;
            font-weight: 400;
            src: local('Barlow Italic'), local('Barlow-Italic'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHrv4kjgoGqM7E_Cfs7wH8Dnzcj.woff2") format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: italic;
            font-weight: 700;
            src: local('Barlow Bold Italic'), local('Barlow-BoldItalic'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHsv4kjgoGqM7E_CfOA5WohvToJdLm8BvE.woff2") format('woff2');
            unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: italic;
            font-weight: 700;
            src: local('Barlow Bold Italic'), local('Barlow-BoldItalic'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHsv4kjgoGqM7E_CfOA5WogvToJdLm8BvE.woff2") format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: italic;
            font-weight: 700;
            src: local('Barlow Bold Italic'), local('Barlow-BoldItalic'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHsv4kjgoGqM7E_CfOA5WouvToJdLm8.woff2") format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: normal;
            font-weight: 400;
            src: local('Barlow Regular'), local('Barlow-Regular'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHpv4kjgoGqM7E_A8s5ynghnQci.woff2") format('woff2');
            unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: normal;
            font-weight: 400;
            src: local('Barlow Regular'), local('Barlow-Regular'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHpv4kjgoGqM7E_Ass5ynghnQci.woff2") format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: normal;
            font-weight: 400;
            src: local('Barlow Regular'), local('Barlow-Regular'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHpv4kjgoGqM7E_DMs5ynghnQ.woff2") format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: normal;
            font-weight: 700;
            src: local('Barlow Bold'), local('Barlow-Bold'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHqv4kjgoGqM7E3t-4s6Fostz0rdom9.woff2") format('woff2');
            unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: normal;
            font-weight: 700;
            src: local('Barlow Bold'), local('Barlow-Bold'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHqv4kjgoGqM7E3t-4s6Vostz0rdom9.woff2") format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: normal;
            font-weight: 700;
            src: local('Barlow Bold'), local('Barlow-Bold'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHqv4kjgoGqM7E3t-4s51ostz0rdg.woff2") format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }
        @font-face {
            font-display: block;
            font-family:"Avenir-LT-W01_85-Heavy1475544";
            src:url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/6af9989e-235b-4c75-8c08-a83bdaef3f66.eot?#iefix");
            src:url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/6af9989e-235b-4c75-8c08-a83bdaef3f66.eot?#iefix") format("eot"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/d513e15e-8f35-4129-ad05-481815e52625.woff2") format("woff2"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/61bd362e-7162-46bd-b67e-28f366c4afbe.woff") format("woff"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/ccd17c6b-e7ed-4b73-b0d2-76712a4ef46b.ttf") format("truetype"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/20577853-40a7-4ada-a3fb-dd6e9392f401.svg#20577853-40a7-4ada-a3fb-dd6e9392f401") format("svg");
        }
        @font-face {
            font-family: "Avenir-LT-W05_85-Heavy";
            src: url("//static.parastorage.com/services/santa-resources/resources/viewer/user-site-fonts/fonts/Avenir_Family_Pack/v1/AvenirLTW05-85Heavy.woff2") format("woff2"),
            url("//static.parastorage.com/services/santa-resources/resources/viewer/user-site-fonts/fonts/Avenir_Family_Pack/v1/AvenirLTW05-85Heavy.woff") format("woff");
        }
        @font-face {
            font-display: block;
            font-family: "Barlow-Medium";
            src: url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-medium/v1/ZqlneECqpsd9SXlmAsD2E1tXRa8TVwTICgirnJhmVJw.woff2") format("woff2"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-medium/v1/ZqlneECqpsd9SXlmAsD2Ez8E0i7KZn-EPnyo3HZu7kw.woff") format("woff"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-medium/v1/ZqlneECqpsd9SXlmAsD2E6CWcynf_cDxXwCLxiixG1c.ttf") format("truetype"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-medium/v1/barlow-medium-Svg.svg") format("svg");
        }
        @font-face {
            font-display: block;
            font-family: "Barlow-Medium";
            font-weight: 700;
            src: url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-medium/v1/yS165lxqGuDghyUMXeu6xVtXRa8TVwTICgirnJhmVJw.woff2") format("woff2"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-medium/v1/yS165lxqGuDghyUMXeu6xT8E0i7KZn-EPnyo3HZu7kw.woff") format("woff"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-medium/v1/yS165lxqGuDghyUMXeu6xaCWcynf_cDxXwCLxiixG1c.ttf") format("truetype"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-medium/v1/barlow-medium-700-Svg.svg") format("svg");
        }
        @font-face {
            font-display: block;
            font-family: "Barlow-Medium";
            font-style: italic;
            src: url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-medium/v1/xJLokI-F3wr7NRWXgS0pZ6g5eI2G47JWe0-AuFtD150.woff2") format("woff2"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-medium/v1/xJLokI-F3wr7NRWXgS0pZ9Ih4imgI8P11RFo6YPCPC0.woff") format("woff"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-medium/v1/xJLokI-F3wr7NRWXgS0pZ102b4v3fUxqf9CZJ1qUoIA.ttf") format("truetype"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-medium/v1/barlow-medium-italic-Svg.svg") format("svg");
        }
        @font-face {
            font-display: block;
            font-family: "Barlow-Medium";
            font-weight: 700;
            font-style: italic;
            src: url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-medium/v1/hw7DQwyFvE7wFOFzpow4xqg5eI2G47JWe0-AuFtD150.woff2") format("woff2"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-medium/v1/hw7DQwyFvE7wFOFzpow4xtIh4imgI8P11RFo6YPCPC0.woff") format("woff"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-medium/v1/hw7DQwyFvE7wFOFzpow4xl02b4v3fUxqf9CZJ1qUoIA.ttf") format("truetype"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-medium/v1/barlow-medium-700-italic-Svg.svg") format("svg");
        }
        @font-face {
            font-display: block;
            font-family: "Helvetica-W01-Roman";
            src: url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/ea95b44a-eab7-4bd1-861c-e73535e7f652.eot?#iefix");
            src: url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/ea95b44a-eab7-4bd1-861c-e73535e7f652.eot?#iefix") format("eot"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/4021a3b9-f782-438b-aeb4-c008109a8b64.woff") format("woff"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/669f79ed-002c-4ff6-965c-9da453968504.ttf") format("truetype"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/d17bc040-9e8b-4397-8356-8153f4a64edf.svg#d17bc040-9e8b-4397-8356-8153f4a64edf") format("svg");
        }
        @font-face {
            font-display: block;
            font-family: "Helvetica-W02-Roman";
            src: url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/e4bd4516-4480-43df-aa6e-4e9b9029f53e.eot?#iefix");
            src: url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/e4bd4516-4480-43df-aa6e-4e9b9029f53e.eot?#iefix") format("eot"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/b56b944e-bbe0-4450-a241-de2125d3e682.woff") format("woff"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/7da02f05-ae8b-43a1-aeb9-83b3c0527c06.ttf") format("truetype"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/66cac56e-d017-4544-9d0c-f7d978f0c5c2.svg#66cac56e-d017-4544-9d0c-f7d978f0c5c2") format("svg");
        }
        @font-face {
            font-display: block;
            font-family: "Helvetica-LT-W10-Roman";
            src: url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/686a6a06-e711-4bd2-b393-8504a497bb3c.eot?#iefix");
            src: url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/686a6a06-e711-4bd2-b393-8504a497bb3c.eot?#iefix") format("eot"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/6f8d1983-4d34-4fa4-9110-988f6c495757.woff") format("woff"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/7903ee3f-e9ab-4bdc-b7d2-d232de2da580.ttf") format("truetype"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/9c58e9ea-fdea-4b9c-b0f9-0a2157389ed0.svg#9c58e9ea-fdea-4b9c-b0f9-0a2157389ed0") format("svg");
        }

        #CONTROLLER_COMP_CUSTOM_ID { --bgh:43,104,156;--alpha-bgh:1;--boxShadowToggleOn -shd:none;--bg:61,155,233;--brw:0px;--alpha - brdh:1;--fnt:normal normal normal 14px/1.4em raleway;--alpha - txth:1;--shd:0 1px 4px rgba(0, 0, 0, 0.6);;--txt:255,255,255;--alpha-txt:1;--rd:20px;--brdh:61,155,233;--alpha-brdh:1;--brd:43,104,156;--alpha-brd:1;--alpha-bg:1;--alpha -bgh:1;--txth:255,255,255;--alpha-txth:1;--alpha - txt:1;--alpha - brd:1 }#comp-kdzvucho { --brw:0px;--brd:var(--color_15);--shd:none;--rd:0px;--sepw:0px;--sep:var(--color_15);--itemBGColorTrans:background-color 0.4s ease 0s;--bgh:var(--color_11);--txth:var(--color_15);--alpha-txth:1;--bgs:var(--color_11);--txts:var(--color_15);--alpha-txts:1;--textSpacing:30px;--bg:var(--color_12);--fnt:var(--font_8);--txt:var(--color_15);--alpha-txt:1;--subMenuOpacityTrans:all 0.4s ease 0s;--SKINS_submenuBR:0px;--SKINS_bgSubmenu:255,255,255;--SKINS_fntSubmenu:var(--font_8);--SKINS_submenuMargin:0px;--subItemAlterAlignPad:30px;--separatorHeight:0px;--dropdownMarginReal:0px;--boxShadowToggleOn-shd:none;--alpha-SKINS_bgSubmenu:1;--alpha-brd:1;--alpha-sep:1;--alpha-bg:1;--alpha-bgh:1;--alpha-bgs:1;--item-height:40px;--item-align:left;--text-align:flex-start;--sub-menu-open-direction-right:auto;--sub-menu-open-direction-left:0;--separator-height-adjusted:0px }#comp-kdmvot8b { height:auto }#comp-kyfn4iuj { --contentPaddingLeft:0px;--contentPaddingRight:0px;--contentPaddingTop:0px;--contentPaddingBottom:0px;--height:72px;--width:106px }#comp-kdmrh8vz { --menuTotalBordersX:0px;--menuTotalBordersY:0px;--bgDrop:var(--color_11);--rd:0px;--shd:none;--fnt:var(--font_9);--pad:5px;--txt:var(--color_15);--trans:color 0.4s ease 0s;--txth:var(--color_14);--txts:var(--color_14);--boxShadowToggleOn-shd:none;--alpha-txt:1;--alpha-bgDrop:1;--alpha-txth:1;--alpha-txts:1 }#comp-kdoiv99l1 { --bg:var(--color_12);--brw:0px;--brd:var(--color_15);--rd:0px;--shd:none;--bg-dd:var(--color_11);--alpha-bg-dd:1;--brw-dd:1px;--brd-dd:var(--color_15);--rd-dd:0px;--shd-dd:none;--fnt:normal normal bold 16px/1.4em barlow-extralight,barlow,sans-serif;--fnt-size-dd:15px;--txt-dd:var(--color_15);--alpha-txt-dd:1;--txt-slct-dd:var(--color_15);--alpha-txt-slct-dd:1;--txth-dd:var(--color_15);--alpha-txth-dd:1;--txth:var(--color_15);--alpha-txth:1;--txt:var(--color_15);--alpha-txt:1;--avatarCornerRadius:100px;--avatarAndArrowStrokeWidth:0px;--badge-bg:var(--color_23);--alpha-badge-bg:1;--badge-txt:var(--color_11);--alpha-badge-txt:1;--fillcolor:var(--color_15);--alpha-fillcolor:1;--boxShadowToggleOn-shd:none;--alpha-brd:1;--alpha-brd-dd:0.2;--boxShadowToggleOn-shd-dd:none;--alpha-bg:0;justify-content:flex-end;direction:ltr;--icon-size:23px }#comp-kyfn5b7m { --contentPaddingLeft:0px;--contentPaddingRight:0px;--contentPaddingTop:0px;--contentPaddingBottom:0px;--height:58px;--width:86px }#comp-kdmviuth { height:auto }#comp-kdmvjwdk { height:auto }#comp-kdmvmk7n { --item-size:21px;--item-margin:0 19px 0 0;--item-display:inline-block;width:101px;height:21px }#comp-kdmvo5ke { height:auto }#comp-kdmwh2tg { height:auto }#comp-kdmvqgp9 { height:auto }#comp-kdmwbq4r { height:auto }#comp-kdmwc0ek { height:auto }#comp-kdmwc9xh { height:auto }#comp-kdmwd2c5 { height:auto }#comp-kdmwcpui { height:auto }#comp-kdmvsx892 { --min-height:35px;height:auto }#comp-kdmvsx8d { --shd:none;--rd:0px;--fnt:normal normal bold 16px/1.4em barlow-extralight,barlow,sans-serif;--brw:0px 0px 1px 0px;--bg:var(--color_11);--txt:var(--color_15);--alpha-txt:1;--brd:var(--color_15);--txt2:var(--color_15);--alpha-txt2:1;--brwh:0px 0px 2px 0px;--bgh:255,255,255;--brdh:var(--color_15);--brwf:0px 0px 2px 0px;--bgf:255,255,255;--brdf:var(--color_15);--brwe:0px 0px 5px 0px;--bge:255,255,255;--brde:255,64,64;--trns:opacity 0.5s ease 0s, border 0.5s ease 0s, color 0.5s ease 0s;--bgd:255,255,255;--txtd:219,219,219;--alpha-txtd:1;--brwd:1px;--brdd:219,219,219;--alpha-brdd:1;--fntlbl:normal normal 700 16px/1.4em barlow-extralight,barlow,sans-serif;--txtlbl:var(--color_15);--alpha-txtlbl:1;--txtlblrq:var(--color_15);--alpha-txtlblrq:1;--fntprefix:normal normal normal 16px/1.4em helvetica-w01-roman,helvetica-w02-roman,helvetica-lt-w10-roman,sans-serif;--boxShadowToggleOn-shd:none;--alpha-brde:1;--alpha-brdf:1;--alpha-btn_brd:0.55;--alpha-brdh:1;--btn_brw:1;--alpha-brd:1;--fnt2:var(--font_8);--btn_fnt:var(--font_8);--alpha-bgd:1;--btn_brd:var(--color_15);--alpha-bge:0;--alpha-bgf:0;--alpha-bg:0;--alpha-bgh:0;--txt-placeholder:var(--color_14);--alpha-txt-placeholder:1;--dir:ltr;--textAlign:left;--textPadding:3px 3px 3px 7px;--labelPadding:0 20px 0 0px;--requiredIndicationDisplay:inline;--labelMarginBottom:0px;height:auto;--inputHeight:25px }#comp-kdmvsx8j { --shd:none;--rd:0px;--fnt:normal normal bold 16px/1.4em barlow-extralight,barlow,sans-serif;--brw:0px 0px 1px 0px;--bg:var(--color_11);--txt:var(--color_15);--alpha-txt:1;--brd:var(--color_15);--txt2:var(--color_15);--alpha-txt2:1;--brwh:0px 0px 2px 0px;--bgh:255,255,255;--brdh:var(--color_15);--brwf:0px 0px 2px 0px;--bgf:255,255,255;--brdf:var(--color_15);--brwe:0px 0px 5px 0px;--bge:255,255,255;--brde:255,64,64;--trns:opacity 0.5s ease 0s, border 0.5s ease 0s, color 0.5s ease 0s;--bgd:255,255,255;--txtd:219,219,219;--alpha-txtd:1;--brwd:1px;--brdd:219,219,219;--alpha-brdd:1;--fntlbl:normal normal 700 16px/1.4em barlow-extralight,barlow,sans-serif;--txtlbl:var(--color_15);--alpha-txtlbl:1;--txtlblrq:var(--color_15);--alpha-txtlblrq:1;--fntprefix:normal normal normal 16px/1.4em helvetica-w01-roman,helvetica-w02-roman,helvetica-lt-w10-roman,sans-serif;--boxShadowToggleOn-shd:none;--alpha-brde:1;--alpha-brdf:1;--alpha-btn_brd:0.55;--alpha-brdh:1;--btn_brw:1;--alpha-brd:1;--fnt2:var(--font_8);--btn_fnt:var(--font_8);--alpha-bgd:1;--btn_brd:var(--color_15);--alpha-bge:0;--alpha-bgf:0;--alpha-bg:0;--alpha-bgh:0;--txt-placeholder:var(--color_14);--alpha-txt-placeholder:1;--dir:ltr;--textAlign:left;--textPadding:3px 3px 3px 0px;--labelPadding:0 20px 0 0px;--requiredIndicationDisplay:inline;--labelMarginBottom:0px;height:auto;--inputHeight:25px }#comp-kdmvsx8l1 { --shd:none;--rd:0px;--fnt:normal normal bold 16px/1.4em barlow-extralight,barlow,sans-serif;--brw:0px 0px 1px 0px;--bg:var(--color_11);--txt:var(--color_15);--alpha-txt:1;--brd:var(--color_15);--txt2:var(--color_15);--alpha-txt2:1;--brwh:0px 0px 2px 0px;--bgh:255,255,255;--brdh:var(--color_15);--brwf:0px 0px 2px 0px;--bgf:255,255,255;--brdf:var(--color_15);--brwe:0px 0px 5px 0px;--bge:255,255,255;--brde:255,64,64;--trns:opacity 0.5s ease 0s, border 0.5s ease 0s, color 0.5s ease 0s;--bgd:255,255,255;--txtd:219,219,219;--alpha-txtd:1;--brwd:1px;--brdd:219,219,219;--alpha-brdd:1;--fntlbl:normal normal 700 16px/1.4em barlow-extralight,barlow,sans-serif;--txtlbl:var(--color_15);--alpha-txtlbl:1;--txtlblrq:var(--color_15);--alpha-txtlblrq:1;--fntprefix:normal normal normal 16px/1.4em helvetica-w01-roman,helvetica-w02-roman,helvetica-lt-w10-roman,sans-serif;--boxShadowToggleOn-shd:none;--alpha-brde:1;--alpha-brdf:1;--alpha-btn_brd:0.55;--alpha-brdh:1;--btn_brw:1;--alpha-brd:1;--fnt2:var(--font_8);--btn_fnt:var(--font_8);--alpha-bgd:1;--btn_brd:var(--color_15);--alpha-bge:0;--alpha-bgf:0;--alpha-bg:0;--alpha-bgh:0;--txt-placeholder:var(--color_14);--alpha-txt-placeholder:1;--dir:ltr;--textAlign:left;--textPadding:3px 3px 3px 7px;--labelPadding:0 20px 0 0px;--requiredIndicationDisplay:inline;--labelMarginBottom:0px;height:auto;--inputHeight:25px }#comp-kdmvsx8n3 { --shd:none;--rd:0px;--fnt:normal normal bold 16px/1.4em barlow-extralight,barlow,sans-serif;--brw:0px 0px 1px 0px;--bg:var(--color_11);--txt:var(--color_15);--alpha-txt:1;--brd:var(--color_15);--txt2:var(--color_15);--alpha-txt2:1;--brwh:0px 0px 2px 0px;--bgh:255,255,255;--brdh:var(--color_15);--brwf:0px 0px 2px 0px;--bgf:255,255,255;--brdf:var(--color_15);--brwe:0px 0px 5px 0px;--bge:255,255,255;--brde:255,64,64;--trns:opacity 0.5s ease 0s, border 0.5s ease 0s, color 0.5s ease 0s;--bgd:255,255,255;--txtd:219,219,219;--alpha-txtd:1;--brwd:1px;--brdd:219,219,219;--alpha-brdd:1;--fntlbl:normal normal 700 16px/1.4em barlow-extralight,barlow,sans-serif;--txtlbl:var(--color_15);--alpha-txtlbl:1;--txtlblrq:var(--color_15);--alpha-txtlblrq:1;--fntprefix:normal normal normal 16px/1.4em helvetica-w01-roman,helvetica-w02-roman,helvetica-lt-w10-roman,sans-serif;--boxShadowToggleOn-shd:none;--alpha-brde:1;--alpha-brdf:1;--alpha-btn_brd:0.55;--alpha-brdh:1;--btn_brw:1;--alpha-brd:1;--fnt2:var(--font_8);--btn_fnt:var(--font_8);--alpha-bgd:1;--btn_brd:var(--color_15);--alpha-bge:0;--alpha-bgf:0;--alpha-bg:0;--alpha-bgh:0;--txt-placeholder:var(--color_14);--alpha-txt-placeholder:1;--dir:ltr;--textAlign:left;--textPadding:3px 3px 3px 7px;--labelPadding:0 20px 0 0px;--requiredIndicationDisplay:inline;--labelMarginBottom:0px;height:auto;--inputHeight:25px }#comp-kdmvsx8p4 { --shd:none;--rd:0px;--fnt:normal normal bold 16px/1.4em barlow-extralight,barlow,sans-serif;--brw:0px 0px 1px 0px;--bg:var(--color_11);--txt:var(--color_15);--alpha-txt:1;--brd:var(--color_15);--txt2:var(--color_15);--alpha-txt2:1;--brwh:0px 0px 2px 0px;--bgh:255,255,255;--brdh:var(--color_15);--bgd:255,255,255;--txtd:219,219,219;--alpha-txtd:1;--brwd:1px;--brdd:219,219,219;--alpha-brdd:1;--brwf:0px 0px 2px 0px;--bgf:255,255,255;--brdf:var(--color_15);--brwe:0px 0px 5px 0px;--bge:255,255,255;--brde:255,64,64;--fntlbl:normal normal 700 16px/1.4em barlow-extralight,barlow,sans-serif;--txtlbl:var(--color_15);--alpha-txtlbl:1;--txtlblrq:var(--color_15);--alpha-txtlblrq:1;--boxShadowToggleOn-shd:none;--alpha-brde:1;--alpha-brdf:1;--alpha-btn_brd:0.55;--alpha-brdh:1;--btn_brw:1;--alpha-brd:1;--fnt2:var(--font_8);--btn_fnt:var(--font_8);--bg2:170,170,170;--alpha-bg2:1;--alpha-bgd:1;--btn_brd:var(--color_15);--alpha-bge:0;--alpha-bgf:0;--alpha-bg:0;--alpha-bgh:0;--txt-placeholder:var(--color_14);--alpha-txt-placeholder:1;--dir:ltr;--textAlign:left;--textPadding:3px 10px 3px 7px;--labelPadding:0 20px 0 0px;--labelMarginBottom:0px;--requiredIndicationDisplay:inline;height:auto;--inputHeight:52px }#comp-kdmvsx8u { --rd:0px;--trans1:border-color 0.4s ease 0s, background-color 0.4s ease 0s;--shd:none;--fnt:normal normal normal 16px/1.4em avenir-lt-w01_35-light1475496,avenir-lt-w05_35-light,sans-serif;--trans2:color 0.4s ease 0s;--txt:var(--color_15);--brw:1px;--bg:var(--color_12);--brd:var(--color_15);--bgh:var(--color_15);--brdh:var(--color_12);--txth:var(--color_11);--bgd:204,204,204;--alpha-bgd:1;--brdd:204,204,204;--alpha-brdd:1;--txtd:255,255,255;--alpha-txtd:1;--boxShadowToggleOn-shd:none;--alpha-txt:1;--alpha-brdh:0;--alpha-brd:1;--alpha-txth:1;--alpha-bg:0;--alpha-bgh:1;--shc-mutated-brightness:124,124,124;--label-align:center;--label-text-align:center }#comp-kdmvsx8x1 { height:auto }#SOSP_CONTAINER_CUSTOM_ID { --brw:0px;--brd:var(--color_15);--bg:var(--color_1);--rd:0px;--shd:none;--alpha-bg:0;--alpha-brd:1;--boxShadowToggleOn-shd:none;--shc-mutated-brightness:128,128,128 }#comp-kdmsih9l { --bg-overlay-color:transparent;--bg-gradient:none;--padding:0px;--margin:0px;min-width:100%;--firstChildMarginTop:-1px;--lastChildMarginBottom:-1px }#SITE_PAGES { --transition-duration:700ms }#comp-kdmvddgy { --bg-overlay-color:transparent;--bg-gradient:none;--padding:0px;--margin:0px;min-width:100%;--firstChildMarginTop:-1px;--lastChildMarginBottom:-1px }#comp-kdmsihb7 { --bg-overlay-color:rgb(var(--color_12));--bg-gradient:none;width:100%;--column-width:176px;--column-flex:176 }#comp-kdmsir58 { --bg-overlay-color:rgb(var(--color_12));--bg-gradient:none;width:100%;--column-width:636px;--column-flex:636 }#comp-kdoiv97i { --bg-overlay-color:rgb(var(--color_12));--bg-gradient:none;width:100%;--column-width:168px;--column-flex:168 }#comp-kdmvddhu { --bg-overlay-color:rgb(var(--color_11));--bg-gradient:none;width:100%;--column-width:324px;--column-flex:324 }#comp-kdmvemej { --bg-overlay-color:rgb(var(--color_11));--bg-gradient:none;width:100%;--column-width:328px;--column-flex:328 }#comp-kdmveqzu { --bg-overlay-color:rgb(var(--color_11));--bg-gradient:none;width:100%;--column-width:328px;--column-flex:328 }#comp-kdmvsx7w { --rd:0px;--shd:0 0 0 rgba(0, 0, 0, 0);--bg:var(--color_11);--alpha-bg:1;--brd:var(--color_13);--alpha-brd:1;--brw:0px }#comp-kdmrs3cn { height:auto }#BACKGROUND_GROUP { --transition-duration:700ms }
    </style>
    <style id="css_c1dmp">
        [data-mesh-id=comp-kdmrgvllinlineContent]{height:auto;width:100%}[data-mesh-id=comp-kdmrgvllinlineContent-gridContainer]{position:static;display:grid;height:auto;width:100%;min-height:928px;grid-template-rows:repeat(4, min-content) 1fr;grid-template-columns:100%}[data-mesh-id="comp-kdms14ak-rotated-wrapper"] > [id="comp-kdms14ak"]{position:relative;left:158px;top:-20px}[data-mesh-id=comp-kdms14ak-rotated-wrapper]{position:static;height:20px;width:0;margin:355px 0px 10px calc((100% - 735px) * 0);grid-area:1 / 1 / 2 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdmrgvllinlineContent-gridContainer] > [id="comp-kdmrncb1"]{position:relative;margin:0px 0px 0 calc((100% - 735px) * 0);left:89px;grid-area:3 / 1 / 4 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdmrgvllinlineContent-gridContainer] > [id="comp-kdmrqb46"]{position:relative;margin:0px 0px 13px calc((100% - 735px) * 0);left:89px;grid-area:4 / 1 / 5 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdmrgvllinlineContent-gridContainer] > [id="comp-kfcmqpy2"]{position:relative;margin:0px 0px 10px calc((100% - 735px) * 0);left:89px;grid-area:5 / 1 / 6 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdmrgvllinlineContent-wedge-3]{visibility:hidden;height:627px;width:0;grid-area:1 / 1 / 3 / 2}[data-mesh-id=comp-kdmrkt18inlineContent]{height:auto;width:100%;position:static;min-height:928px}[data-mesh-id=comp-kdmt06diinlineContent]{height:auto;width:100%}[data-mesh-id=comp-kdmt06diinlineContent-gridContainer]{position:static;display:grid;height:auto;width:100%;min-height:auto;grid-template-rows:repeat(2, min-content) 1fr;grid-template-columns:100%}[data-mesh-id=comp-kdmt06diinlineContent-gridContainer] > [id="comp-kdmswlbk"]{position:relative;margin:146px 0px 34px calc((100% - 980px) * 0);left:294px;grid-area:1 / 1 / 2 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdmt06diinlineContent-gridContainer] > [id="comp-kdmt5r6h"]{position:relative;margin:0px 0px 28px calc((100% - 980px) * 0);left:294px;grid-area:2 / 1 / 3 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdmt06diinlineContent-gridContainer] > [id="comp-kdmt39ab"]{position:relative;margin:0px 0px 58px calc((100% - 980px) * 0);left:293px;grid-area:3 / 1 / 4 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kfdto1pjinlineContent]{height:auto;width:100%;position:static;min-height:83px}[data-mesh-id=comp-kdmtpsybinlineContent]{height:auto;width:100%;position:static;min-height:800px}[data-mesh-id=comp-kdmtqs9binlineContent]{height:auto;width:100%;position:static;min-height:800px}[data-mesh-id=comp-kdmttxjpinlineContent]{height:auto;width:100%}[data-mesh-id=comp-kdmttxjpinlineContent-gridContainer]{position:static;display:grid;height:auto;width:100%;min-height:496px;grid-template-rows:repeat(3, min-content) 1fr;grid-template-columns:100%}[data-mesh-id=comp-kdmttxjpinlineContent-gridContainer] > [id="comp-kdmtvktv"]{position:relative;margin:91px 0px 34px calc((100% - 394px) * 0.5);left:0px;grid-area:1 / 1 / 2 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdmttxjpinlineContent-gridContainer] > [id="comp-kdmtvkua"]{position:relative;margin:0px 0px 29px calc((100% - 394px) * 0.5);left:0px;grid-area:2 / 1 / 3 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdmttxjpinlineContent-gridContainer] > [id="comp-kdmtvkul"]{position:relative;margin:0px 0px 44px calc((100% - 394px) * 0.5);left:0px;grid-area:3 / 1 / 4 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdmttxjpinlineContent-gridContainer] > [id="comp-kdmtx5uh"]{position:relative;margin:0px 0px 10px calc((100% - 394px) * 0.5);left:0px;grid-area:4 / 1 / 5 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdmtu9beinlineContent]{height:auto;width:100%;position:static;min-height:496px}[data-mesh-id=comp-kfdtl30einlineContent]{height:auto;width:100%;position:static;min-height:80px}[data-mesh-id=comp-kdmutj7ninlineContent]{height:auto;width:100%}[data-mesh-id=comp-kdmutj7ninlineContent-gridContainer]{position:static;display:grid;height:auto;width:100%;min-height:529px;grid-template-rows:min-content 1fr;grid-template-columns:100%}[data-mesh-id=comp-kdmutj7ninlineContent-gridContainer] > [id="comp-kdmuy4bf"]{position:relative;margin:98px 0px 28px calc((100% - 980px) * 0.5);left:10px;grid-area:1 / 1 / 2 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdmutj7ninlineContent-gridContainer] > [id="comp-kdmuy4bu"]{position:relative;margin:0px 0px 10px calc((100% - 980px) * 0.5);left:444px;grid-area:2 / 1 / 3 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdmu0n7uinlineContent]{height:auto;width:100%}[data-mesh-id=comp-kdmu0n7uinlineContent-gridContainer]{position:static;display:grid;height:auto;width:100%;min-height:auto;grid-template-rows:min-content 1fr;grid-template-columns:100%}[data-mesh-id=comp-kdmu0n7uinlineContent-gridContainer] > [id="comp-kdmv5b7t"]{position:relative;margin:26px 0px 31px calc((100% - 980px) * 0.5);left:291px;grid-area:1 / 1 / 2 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdmu0n7uinlineContent-gridContainer] > [id="comp-kdmv5b8i"]{position:relative;margin:0px 0px 29px calc((100% - 980px) * 0.5);left:309px;grid-area:2 / 1 / 3 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdmu0n8cinlineContent]{height:auto;width:100%}[data-mesh-id=comp-kdmu0n8cinlineContent-gridContainer]{position:static;display:grid;height:auto;width:100%;min-height:211px;grid-template-rows:min-content 1fr;grid-template-columns:100%}[data-mesh-id=comp-kdmu0n8cinlineContent-gridContainer] > [id="comp-kdmv5mr7"]{position:relative;margin:28px 0px 30px calc((100% - 980px) * 0.5);left:290px;grid-area:1 / 1 / 2 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdmu0n8cinlineContent-gridContainer] > [id="comp-kdmv5mrt"]{position:relative;margin:0px 0px 10px calc((100% - 980px) * 0.5);left:308px;grid-area:2 / 1 / 3 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdmu0n8h1inlineContent]{height:auto;width:100%}[data-mesh-id=comp-kdmu0n8h1inlineContent-gridContainer]{position:static;display:grid;height:auto;width:100%;min-height:211px;grid-template-rows:min-content 1fr;grid-template-columns:100%}[data-mesh-id=comp-kdmu0n8h1inlineContent-gridContainer] > [id="comp-kdmv3h7i"]{position:relative;margin:27px 0px 31px calc((100% - 980px) * 0.5);left:290px;grid-area:1 / 1 / 2 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdmu0n8h1inlineContent-gridContainer] > [id="comp-kdmv3z41"]{position:relative;margin:0px 0px 10px calc((100% - 980px) * 0.5);left:308px;grid-area:2 / 1 / 3 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kfdtokcqinlineContent]{height:auto;width:100%;position:static;min-height:80px}[data-mesh-id=comp-kdmv8oys3inlineContent]{height:auto;width:100%;position:static;min-height:819px}[data-mesh-id=comp-kdmv8oyrinlineContent]{height:auto;width:100%;position:static;min-height:819px}[data-mesh-id=comp-kdmvb0giinlineContent]{height:auto;width:100%;position:static;min-height:496px}[data-mesh-id=comp-kdmvb0g6inlineContent]{height:auto;width:100%}[data-mesh-id=comp-kdmvb0g6inlineContent-gridContainer]{position:static;display:grid;height:auto;width:100%;min-height:496px;grid-template-rows:repeat(3, min-content) 1fr;grid-template-columns:100%}[data-mesh-id=comp-kdmvb0g6inlineContent-gridContainer] > [id="comp-kdmvb0g91"]{position:relative;margin:91px 0px 34px calc((100% - 394px) * 0.5);left:23px;grid-area:1 / 1 / 2 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdmvb0g6inlineContent-gridContainer] > [id="comp-kdmvb0gc"]{position:relative;margin:0px 0px 29px calc((100% - 394px) * 0.5);left:20px;grid-area:2 / 1 / 3 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdmvb0g6inlineContent-gridContainer] > [id="comp-kdmvb0gf1"]{position:relative;margin:0px 0px 43px calc((100% - 394px) * 0.5);left:28px;grid-area:3 / 1 / 4 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdmvb0g6inlineContent-gridContainer] > [id="comp-kdmvb0gg3"]{position:relative;margin:0px 0px 10px calc((100% - 394px) * 0.5);left:28px;grid-area:4 / 1 / 5 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kfdtoucu1inlineContent]{height:auto;width:100%;position:static;min-height:80px}[data-mesh-id=Containerc1dmpinlineContent]{height:auto;width:100%}[data-mesh-id=Containerc1dmpinlineContent-gridContainer]{position:static;display:grid;height:auto;width:100%;min-height:500px;grid-template-rows:repeat(12, min-content) 1fr;grid-template-columns:100%;padding-bottom:0px;box-sizing:border-box}[data-mesh-id=Containerc1dmpinlineContent-gridContainer] > [id="comp-kdmrgvj8"]{position:relative;margin:0px 0px 0 calc((100% - 980px) * 0.5);left:0px;grid-area:1 / 1 / 2 / 2;justify-self:start;align-self:start}[data-mesh-id=Containerc1dmpinlineContent-gridContainer] > [id="comp-kdmt06ch"]{position:relative;margin:0px 0px 0 calc((100% - 980px) * 0.5);left:0px;grid-area:2 / 1 / 3 / 2;justify-self:start;align-self:start}[data-mesh-id=Containerc1dmpinlineContent-gridContainer] > [id="comp-kdmt6lj8"]{position:relative;margin:0px 80px 12px 80px;left:0;grid-area:3 / 1 / 4 / 2;justify-self:stretch;align-self:start}[data-mesh-id=Containerc1dmpinlineContent-gridContainer] > [id="comp-kdzqkilc"]{position:relative;margin:0px 80px 7px 80px;left:0;grid-area:4 / 1 / 5 / 2;justify-self:stretch;align-self:start}[data-mesh-id=Containerc1dmpinlineContent-gridContainer] > [id="comp-kfdto1n5"]{position:relative;margin:0px 0px 1px calc((100% - 980px) * 0.5);left:0px;grid-area:5 / 1 / 6 / 2;justify-self:start;align-self:start}[data-mesh-id=Containerc1dmpinlineContent-gridContainer] > [id="comp-kdmtpsvt"]{position:relative;margin:0px 0px 0 calc((100% - 980px) * 0.5);left:0px;grid-area:6 / 1 / 7 / 2;justify-self:start;align-self:start}[data-mesh-id=Containerc1dmpinlineContent-gridContainer] > [id="comp-kdmttxi5"]{position:relative;margin:0px 0px 10px calc((100% - 980px) * 0.5);left:0px;grid-area:6 / 1 / 7 / 2;justify-self:start;align-self:start}[data-mesh-id=Containerc1dmpinlineContent-gridContainer] > [id="comp-kfdtl2xk"]{position:relative;margin:0px 0px 0 calc((100% - 980px) * 0.5);left:0px;grid-area:7 / 1 / 8 / 2;justify-self:start;align-self:start}[data-mesh-id=Containerc1dmpinlineContent-gridContainer] > [id="comp-kdmutj5y"]{position:relative;margin:0px 0px 0 calc((100% - 980px) * 0.5);left:0px;grid-area:8 / 1 / 9 / 2;justify-self:start;align-self:start}[data-mesh-id=Containerc1dmpinlineContent-gridContainer] > [id="comp-kdmu0n5v"]{position:relative;margin:244px 0px 10px calc((100% - 980px) * 0.5);left:0px;grid-area:8 / 1 / 9 / 2;justify-self:start;align-self:start}[data-mesh-id=Containerc1dmpinlineContent-gridContainer] > [id="comp-kfdtokbo"]{position:relative;margin:0px 0px 10px calc((100% - 980px) * 0.5);left:0px;grid-area:9 / 1 / 10 / 2;justify-self:start;align-self:start}[data-mesh-id=Containerc1dmpinlineContent-gridContainer] > [id="comp-kdmv8oxt"]{position:relative;margin:18px 0px 62px calc((100% - 980px) * 0.5);left:0px;grid-area:9 / 1 / 13 / 2;justify-self:start;align-self:start}[data-mesh-id=Containerc1dmpinlineContent-gridContainer] > [id="comp-kdmvb0f7"]{position:relative;margin:0px 0px 10px calc((100% - 980px) * 0.5);left:0px;grid-area:11 / 1 / 12 / 2;justify-self:start;align-self:start}[data-mesh-id=Containerc1dmpinlineContent-gridContainer] > [id="comp-kdmw87jo"]{position:relative;margin:26px 0 33px 0;left:0;grid-area:13 / 1 / 14 / 2;justify-self:stretch;align-self:start}[data-mesh-id=Containerc1dmpinlineContent-gridContainer] > [id="comp-kfdtouby"]{position:relative;margin:0px 0px 0 calc((100% - 980px) * 0.5);left:0px;grid-area:13 / 1 / 14 / 2;justify-self:start;align-self:start}[data-mesh-id=Containerc1dmpinlineContent-wedge-11]{visibility:hidden;height:284px;width:0;grid-area:9 / 1 / 11 / 2}#comp-kdmt6lj8{left:0;margin-left:80px;width:calc(100% - 80px - 80px);min-width:initial;height:645px}#comp-kdzqkilc{left:0;margin-left:80px;width:calc(100% - 80px - 80px);min-width:initial;height:5px}#comp-kdmw87jo{left:0;margin-left:0;width:100%;min-width:initial;height:21px}#comp-kdms14ak{transform:rotate(270deg);width:19px;height:60px}#comp-kdmrncb1{width:585px;height:79px}#comp-kdmrqb46{width:360px;height:72px}#comp-kfcmqpy2{width:142px;height:40px}#comp-kdmswlbk{width:680px;height:36px}#comp-kdmt5r6h{width:94px;height:19px}#comp-kdmt39ab{width:404px;height:125px}#comp-kdmtvktv{width:365px;height:36px}#comp-kdmtvkua{width:94px;height:18px}#comp-kdmtvkul{width:365px;height:100px}#comp-kdmtx5uh{width:139px;height:46px}#comp-kdmuy4bf{width:960px;height:72px}#comp-kdmuy4bu{width:94px;height:18px}#comp-kdmv5b7t{width:402px;height:100px}#comp-kdmv5b8i{width:366px;height:25px}#comp-kdmv5mr7{width:400px;height:50px}#comp-kdmv5mrt{width:364px;height:25px}#comp-kdmv3h7i{width:400px;height:50px}#comp-kdmv3z41{width:364px;height:25px}#comp-kdmvb0g91{width:365px;height:72px}#comp-kdmvb0gc{width:94px;height:18px}#comp-kdmvb0gf1{width:366px;height:75px}#comp-kdmvb0gg3{width:125px;height:46px}#c1dmp{left:0;margin-left:0;width:100%;min-width:100%;}#comp-kdmrgvj8{left:0;margin-left:0;width:100%;min-width:100%}#comp-kdmt06ch{left:0;margin-left:0;width:100%;min-width:100%}#comp-kfdto1n5{left:0;margin-left:0;width:100%;min-width:100%}#comp-kdmtpsvt{margin-left:auto;margin-right:auto;width:calc(100% - 80px - 80px);min-width:980px}#comp-kdmttxi5{margin-left:auto;margin-right:auto;width:calc(100% - 80px - 80px);min-width:980px}#comp-kfdtl2xk{left:0;margin-left:0;width:100%;min-width:100%}#comp-kdmutj5y{left:0;margin-left:0;width:100%;min-width:100%}#comp-kdmu0n5v{left:0;margin-left:0;width:100%;min-width:100%}#comp-kfdtokbo{left:0;margin-left:0;width:100%;min-width:100%}#comp-kdmv8oxt{margin-left:auto;margin-right:auto;width:calc(100% - 80px - 80px);min-width:980px}#comp-kdmvb0f7{margin-left:auto;margin-right:auto;width:calc(100% - 80px - 80px);min-width:980px}#comp-kfdtouby{left:0;margin-left:0;width:100%;min-width:100%}#comp-kdmrgvll{width:735px}#comp-kdmrkt18{width:245px}#comp-kdmt06di{width:980px}#comp-kfdto1pj{width:980px}#comp-kdmtpsyb{width:294px}#comp-kdmtqs9b{width:686px}#comp-kdmttxjp{width:394px}#comp-kdmtu9be{width:586px}#comp-kfdtl30e{width:980px}#comp-kdmutj7n{width:980px}#comp-kdmu0n7u{width:1843px}#comp-kdmu0n8c{width:2560px}#comp-kdmu0n8h1{width:2560px}#comp-kfdtokcq{width:980px}#comp-kdmv8oys3{width:686px}#comp-kdmv8oyr{width:294px}#comp-kdmvb0gi{width:586px}#comp-kdmvb0g6{width:394px}#comp-kfdtoucu1{width:980px}#masterPage{--pinned-layers-in-page:0}
        @font-face {
            font-display: block;
            font-family: "Barlow-ExtraLight";
            src: url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/51v0xj5VPw1cLYHNhfd8NFtXRa8TVwTICgirnJhmVJw.woff2") format("woff2"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/51v0xj5VPw1cLYHNhfd8ND8E0i7KZn-EPnyo3HZu7kw.woff") format("woff"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/51v0xj5VPw1cLYHNhfd8NKCWcynf_cDxXwCLxiixG1c.ttf") format("truetype"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/barlow-extralight-Svg.svg") format("svg");
        }
        @font-face {
            font-display: block;
            font-family: "Barlow-ExtraLight";
            font-weight: 700;
            src: url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/2woyxyDnPU0v4IiqYU9D1g.woff2") format("woff2"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/-HJgNsTwx9qXGSxqew62RQ.woff") format("woff"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/qoExc9IJQUjYXhlVZNNLgg.ttf") format("truetype"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/barlow-extralight-700-Svg.svg") format("svg");
        }
        @font-face {
            font-display: block;
            font-family: "Barlow-ExtraLight";
            font-style: italic;
            src: url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/14AxwKgJhKIO-YYUP_KtZag5eI2G47JWe0-AuFtD150.woff2") format("woff2"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/14AxwKgJhKIO-YYUP_KtZdIh4imgI8P11RFo6YPCPC0.woff") format("woff"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/14AxwKgJhKIO-YYUP_KtZV02b4v3fUxqf9CZJ1qUoIA.ttf") format("truetype"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/barlow-extralight-italic-Svg.svg") format("svg");
        }
        @font-face {
            font-display: block;
            font-family: "Barlow-ExtraLight";
            font-weight: 700;
            font-style: italic;
            src: url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/cdbGxfKO8gdkBd5U5TuXqPesZW2xOQ-xsNqO47m55DA.woff2") format("woff2"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/W1XpMGU0WrpbCawEdG1FM_esZW2xOQ-xsNqO47m55DA.woff") format("woff"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/_lIpJP17FZmSeklpAeOdnvesZW2xOQ-xsNqO47m55DA.ttf") format("truetype"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/barlow-extralight-700-italic-Svg.svg") format("svg");
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: italic;
            font-weight: 400;
            src: local('Barlow Italic'), local('Barlow-Italic'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHrv4kjgoGqM7E_Cfs0wH8DnzcjTrA.woff2") format('woff2');
            unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: italic;
            font-weight: 400;
            src: local('Barlow Italic'), local('Barlow-Italic'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHrv4kjgoGqM7E_Cfs1wH8DnzcjTrA.woff2") format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: italic;
            font-weight: 400;
            src: local('Barlow Italic'), local('Barlow-Italic'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHrv4kjgoGqM7E_Cfs7wH8Dnzcj.woff2") format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: italic;
            font-weight: 700;
            src: local('Barlow Bold Italic'), local('Barlow-BoldItalic'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHsv4kjgoGqM7E_CfOA5WohvToJdLm8BvE.woff2") format('woff2');
            unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: italic;
            font-weight: 700;
            src: local('Barlow Bold Italic'), local('Barlow-BoldItalic'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHsv4kjgoGqM7E_CfOA5WogvToJdLm8BvE.woff2") format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: italic;
            font-weight: 700;
            src: local('Barlow Bold Italic'), local('Barlow-BoldItalic'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHsv4kjgoGqM7E_CfOA5WouvToJdLm8.woff2") format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: normal;
            font-weight: 400;
            src: local('Barlow Regular'), local('Barlow-Regular'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHpv4kjgoGqM7E_A8s5ynghnQci.woff2") format('woff2');
            unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: normal;
            font-weight: 400;
            src: local('Barlow Regular'), local('Barlow-Regular'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHpv4kjgoGqM7E_Ass5ynghnQci.woff2") format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: normal;
            font-weight: 400;
            src: local('Barlow Regular'), local('Barlow-Regular'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHpv4kjgoGqM7E_DMs5ynghnQ.woff2") format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: normal;
            font-weight: 700;
            src: local('Barlow Bold'), local('Barlow-Bold'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHqv4kjgoGqM7E3t-4s6Fostz0rdom9.woff2") format('woff2');
            unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: normal;
            font-weight: 700;
            src: local('Barlow Bold'), local('Barlow-Bold'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHqv4kjgoGqM7E3t-4s6Vostz0rdom9.woff2") format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: normal;
            font-weight: 700;
            src: local('Barlow Bold'), local('Barlow-Bold'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHqv4kjgoGqM7E3t-4s51ostz0rdg.woff2") format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }
        @font-face {
            font-display: block;
            font-family:"Avenir-LT-W01_85-Heavy1475544";
            src:url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/6af9989e-235b-4c75-8c08-a83bdaef3f66.eot?#iefix");
            src:url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/6af9989e-235b-4c75-8c08-a83bdaef3f66.eot?#iefix") format("eot"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/d513e15e-8f35-4129-ad05-481815e52625.woff2") format("woff2"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/61bd362e-7162-46bd-b67e-28f366c4afbe.woff") format("woff"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/ccd17c6b-e7ed-4b73-b0d2-76712a4ef46b.ttf") format("truetype"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/20577853-40a7-4ada-a3fb-dd6e9392f401.svg#20577853-40a7-4ada-a3fb-dd6e9392f401") format("svg");
        }
        @font-face {
            font-family: "Avenir-LT-W05_85-Heavy";
            src: url("//static.parastorage.com/services/santa-resources/resources/viewer/user-site-fonts/fonts/Avenir_Family_Pack/v1/AvenirLTW05-85Heavy.woff2") format("woff2"),
            url("//static.parastorage.com/services/santa-resources/resources/viewer/user-site-fonts/fonts/Avenir_Family_Pack/v1/AvenirLTW05-85Heavy.woff") format("woff");
        }
        @font-face {
            font-display: block;
            font-family:"Avenir-LT-W01_35-Light1475496";
            src:url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/edefe737-dc78-4aa3-ad03-3c6f908330ed.eot?#iefix");
            src:url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/edefe737-dc78-4aa3-ad03-3c6f908330ed.eot?#iefix") format("eot"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/0078f486-8e52-42c0-ad81-3c8d3d43f48e.woff2") format("woff2"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/908c4810-64db-4b46-bb8e-823eb41f68c0.woff") format("woff"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/4577388c-510f-4366-addb-8b663bcc762a.ttf") format("truetype"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/b0268c31-e450-4159-bfea-e0d20e2b5c0c.svg#b0268c31-e450-4159-bfea-e0d20e2b5c0c") format("svg");
        }
        @font-face {
            font-family: "Avenir-LT-W05_35-Light";
            src: url("//static.parastorage.com/services/santa-resources/resources/viewer/user-site-fonts/fonts/Avenir_Family_Pack/v1/AvenirLTW05-35Light.woff2") format("woff2"),
            url("//static.parastorage.com/services/santa-resources/resources/viewer/user-site-fonts/fonts/Avenir_Family_Pack/v1/AvenirLTW05-35Light.woff") format("woff");
        }
        @font-face {
            font-display: block;
            font-family: "Helvetica-W01-Light";
            src: url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/717f8140-20c9-4892-9815-38b48f14ce2b.eot?#iefix");
            src: url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/717f8140-20c9-4892-9815-38b48f14ce2b.eot?#iefix") format("eot"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/03805817-4611-4dbc-8c65-0f73031c3973.woff") format("woff"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/d5f9f72d-afb7-4c57-8348-b4bdac42edbb.ttf") format("truetype"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/05ad458f-263b-413f-b054-6001a987ff3e.svg#05ad458f-263b-413f-b054-6001a987ff3e") format("svg");
        }
        @font-face {
            font-display: block;
            font-family: "Helvetica-W02-Light";
            src: url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/ff80873b-6ac3-44f7-b029-1b4111beac76.eot?#iefix");
            src: url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/ff80873b-6ac3-44f7-b029-1b4111beac76.eot?#iefix") format("eot"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/80c34ad2-27c2-4d99-90fa-985fd64ab81a.woff") format("woff"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/b8cb02c2-5b58-48d8-9501-8d02869154c2.ttf") format("truetype"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/92c941ea-2b06-4b72-9165-17476d424d6c.svg#92c941ea-2b06-4b72-9165-17476d424d6c") format("svg");
        }
        @font-face {
            font-display: block;
            font-family: "Barlow-Medium";
            src: url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-medium/v1/ZqlneECqpsd9SXlmAsD2E1tXRa8TVwTICgirnJhmVJw.woff2") format("woff2"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-medium/v1/ZqlneECqpsd9SXlmAsD2Ez8E0i7KZn-EPnyo3HZu7kw.woff") format("woff"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-medium/v1/ZqlneECqpsd9SXlmAsD2E6CWcynf_cDxXwCLxiixG1c.ttf") format("truetype"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-medium/v1/barlow-medium-Svg.svg") format("svg");
        }
        @font-face {
            font-display: block;
            font-family: "Barlow-Medium";
            font-weight: 700;
            src: url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-medium/v1/yS165lxqGuDghyUMXeu6xVtXRa8TVwTICgirnJhmVJw.woff2") format("woff2"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-medium/v1/yS165lxqGuDghyUMXeu6xT8E0i7KZn-EPnyo3HZu7kw.woff") format("woff"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-medium/v1/yS165lxqGuDghyUMXeu6xaCWcynf_cDxXwCLxiixG1c.ttf") format("truetype"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-medium/v1/barlow-medium-700-Svg.svg") format("svg");
        }
        @font-face {
            font-display: block;
            font-family: "Barlow-Medium";
            font-style: italic;
            src: url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-medium/v1/xJLokI-F3wr7NRWXgS0pZ6g5eI2G47JWe0-AuFtD150.woff2") format("woff2"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-medium/v1/xJLokI-F3wr7NRWXgS0pZ9Ih4imgI8P11RFo6YPCPC0.woff") format("woff"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-medium/v1/xJLokI-F3wr7NRWXgS0pZ102b4v3fUxqf9CZJ1qUoIA.ttf") format("truetype"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-medium/v1/barlow-medium-italic-Svg.svg") format("svg");
        }
        @font-face {
            font-display: block;
            font-family: "Barlow-Medium";
            font-weight: 700;
            font-style: italic;
            src: url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-medium/v1/hw7DQwyFvE7wFOFzpow4xqg5eI2G47JWe0-AuFtD150.woff2") format("woff2"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-medium/v1/hw7DQwyFvE7wFOFzpow4xtIh4imgI8P11RFo6YPCPC0.woff") format("woff"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-medium/v1/hw7DQwyFvE7wFOFzpow4xl02b4v3fUxqf9CZJ1qUoIA.ttf") format("truetype"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-medium/v1/barlow-medium-700-italic-Svg.svg") format("svg");
        }

        #comp-kdzqkilc { --lnw:1px;--brd:var(--color_13);--alpha-brd:1;transform-origin:center 0.5px }#comp-kdms14ak { --opacity:1 }#comp-kdmrncb1 { height:auto }#comp-kdmrqb46 { height:auto }#comp-kfcmqpy2 { --rd:20px;--trans1:border-color 0.4s ease 0s, background-color 0.4s ease 0s;--shd:none;--fnt:normal normal normal 16px/1.4em avenir-lt-w01_85-heavy1475544,avenir-lt-w05_85-heavy,sans-serif;--trans2:color 0.4s ease 0s;--txt:var(--color_11);--brw:2px;--bg:255,255,255;--brd:var(--color_11);--bgh:50,50,50;--brdh:50,50,50;--txth:255,255,255;--bgd:204,204,204;--alpha-bgd:1;--brdd:204,204,204;--alpha-brdd:1;--txtd:255,255,255;--alpha-txtd:1;--alpha-bg:0;--alpha-bgh:1;--alpha-brd:1;--alpha-brdh:0;--alpha-txt:1;--alpha-txth:1;--boxShadowToggleOn-shd:none;--shc-mutated-brightness:128,128,128;--label-align:center;--label-text-align:center }#comp-kdmswlbk { height:auto }#comp-kdmt5r6h { --opacity:1 }#comp-kdmt39ab { height:auto }#comp-kdmtvktv { height:auto }#comp-kdmtvkua { --opacity:1 }#comp-kdmtvkul { height:auto }#comp-kdmtx5uh { --rd:0px;--trans1:border-color 0.4s ease 0s, background-color 0.4s ease 0s;--shd:none;--fnt:normal normal normal 16px/1.4em avenir-lt-w01_35-light1475496,avenir-lt-w05_35-light,sans-serif;--trans2:color 0.4s ease 0s;--txt:var(--color_15);--brw:1px;--bg:var(--color_12);--brd:var(--color_15);--bgh:var(--color_15);--brdh:var(--color_12);--txth:var(--color_11);--bgd:204,204,204;--alpha-bgd:1;--brdd:204,204,204;--alpha-brdd:1;--txtd:255,255,255;--alpha-txtd:1;--boxShadowToggleOn-shd:none;--alpha-txt:1;--alpha-brdh:0;--alpha-brd:1;--alpha-txth:1;--alpha-bg:1;--alpha-bgh:1;--shc-mutated-brightness:124,124,124;--label-align:center;--label-text-align:center }#comp-kdmuy4bf { height:auto }#comp-kdmuy4bu { --opacity:1 }#comp-kdmv5b7t { height:auto }#comp-kdmv5b8i { height:auto }#comp-kdmv5mr7 { height:auto }#comp-kdmv5mrt { height:auto }#comp-kdmv3h7i { height:auto }#comp-kdmv3z41 { height:auto }#comp-kdmvb0g91 { height:auto }#comp-kdmvb0gc { --opacity:1 }#comp-kdmvb0gf1 { height:auto }#comp-kdmvb0gg3 { --rd:0px;--trans1:border-color 0.4s ease 0s, background-color 0.4s ease 0s;--shd:none;--fnt:normal normal normal 16px/1.4em avenir-lt-w01_35-light1475496,avenir-lt-w05_35-light,sans-serif;--trans2:color 0.4s ease 0s;--txt:var(--color_15);--brw:1px;--bg:var(--color_12);--brd:var(--color_15);--bgh:var(--color_15);--brdh:var(--color_12);--txth:var(--color_11);--bgd:204,204,204;--alpha-bgd:1;--brdd:204,204,204;--alpha-brdd:1;--txtd:255,255,255;--alpha-txtd:1;--boxShadowToggleOn-shd:none;--alpha-txt:1;--alpha-brdh:0;--alpha-brd:1;--alpha-txth:1;--alpha-bg:1;--alpha-bgh:1;--shc-mutated-brightness:124,124,124;--label-align:center;--label-text-align:center }#c1dmp { --param_boolean_previewHover:false;width:auto;min-height:500px }#comp-kdmrgvj8 { --param_boolean_previewHover:false;--bg-overlay-color:rgb(var(--color_13));--bg-gradient:none;--fill-layer-image-opacity:1;--padding:0px;--margin:0px;min-width:100%;--firstChildMarginTop:-1px;--lastChildMarginBottom:-1px }#comp-kdmt06ch { --param_boolean_previewHover:false;--bg-overlay-color:transparent;--bg-gradient:none;--padding:0px;--margin:0px;min-width:100%;--firstChildMarginTop:-1px;--lastChildMarginBottom:-1px }#comp-kfdto1n5 { --bg-overlay-color:transparent;--bg-gradient:none;--padding:0px;--margin:0px;min-width:100%;--firstChildMarginTop:-1px;--lastChildMarginBottom:-1px }#comp-kdmtpsvt { --bg-overlay-color:transparent;--bg-gradient:none;--padding:0px;--margin:0px;min-width:100%;--firstChildMarginTop:-1px;--lastChildMarginBottom:-1px }#comp-kdmttxi5 { --param_boolean_previewHover:false;--bg-overlay-color:transparent;--bg-gradient:none;--padding:0px;--margin:0px;min-width:100%;--firstChildMarginTop:-1px;--lastChildMarginBottom:-1px }#comp-kfdtl2xk { --bg-overlay-color:transparent;--bg-gradient:none;--padding:0px;--margin:0px;min-width:100%;--firstChildMarginTop:-1px;--lastChildMarginBottom:-1px }#comp-kdmutj5y { --bg-overlay-color:transparent;--bg-gradient:none;--padding:0px;--margin:0px;min-width:100%;--firstChildMarginTop:-1px;--lastChildMarginBottom:-1px }#comp-kdmu0n5v { --shd:0 0 0 rgba(0, 0, 0, 0.6);--dotsColor:var(--color_13);--dotsSelectedColor:var(--color_15);--arrowColor:var(--color_15);--rd:0px;--brw:0px;--brd:var(--color_11);--alpha-brd:1;--alpha-arrowColor:1;--alpha-dotsColor:1;--alpha-dotsSelectedColor:1;height:211px;--nav-dot-section-display:block;--nav-dot-section-bottom-margin:50px;--nav-dot-margin:12px;--nav-dot-size:6px;--nav-dot-size-selected:6px;--nav-button-width:29px;--nav-button-offset:100px;--nav-button-display:none;--slides-overflow:hidden;--transition-duration:1000ms }#comp-kfdtokbo { --bg-overlay-color:transparent;--bg-gradient:none;--padding:0px;--margin:0px;min-width:100%;--firstChildMarginTop:-1px;--lastChildMarginBottom:-1px }#comp-kdmv8oxt { --bg-overlay-color:transparent;--bg-gradient:none;--padding:0px;--margin:0px;min-width:100%;--firstChildMarginTop:-1px;--lastChildMarginBottom:-1px }#comp-kdmvb0f7 { --bg-overlay-color:transparent;--bg-gradient:none;--padding:0px;--margin:0px;min-width:100%;--firstChildMarginTop:-1px;--lastChildMarginBottom:-1px }#comp-kfdtouby { --bg-overlay-color:transparent;--bg-gradient:none;--padding:0px;--margin:0px;min-width:100%;--firstChildMarginTop:-1px;--lastChildMarginBottom:-1px }#comp-kdmrgvll { --bg-overlay-color:transparent;--bg-gradient:none;width:100%;--column-width:735px;--column-flex:735 }#comp-kdmrkt18 { --bg-overlay-color:transparent;--bg-gradient:none;width:100%;--column-width:245px;--column-flex:245 }#comp-kdmt06di { --bg-overlay-color:rgb(var(--color_12));--bg-gradient:none;width:100%;--column-width:980px;--column-flex:980 }#comp-kfdto1pj { --bg-overlay-color:rgb(var(--color_12));--bg-gradient:none;width:100%;--column-width:980px;--column-flex:980 }#comp-kdmtpsyb { --bg-overlay-color:transparent;--bg-gradient:none;width:100%;--column-width:294px;--column-flex:294 }#comp-kdmtqs9b { --bg-overlay-color:rgb(var(--color_13));--bg-gradient:none;--fill-layer-image-opacity:1;width:100%;--column-width:686px;--column-flex:686 }#comp-kdmttxjp { --bg-overlay-color:rgb(var(--color_12));--bg-gradient:none;width:100%;--column-width:394px;--column-flex:394 }#comp-kdmtu9be { --bg-overlay-color:transparent;--bg-gradient:none;width:100%;--column-width:586px;--column-flex:586 }#comp-kfdtl30e { --bg-overlay-color:rgb(var(--color_12));--bg-gradient:none;width:100%;--column-width:980px;--column-flex:980 }#comp-kdmutj7n { --bg-overlay-color:rgb(var(--color_11));--bg-gradient:none;--fill-layer-image-opacity:0.1;width:100%;--column-width:980px;--column-flex:980 }#comp-kfdtokcq { --bg-overlay-color:rgb(var(--color_12));--bg-gradient:none;width:100%;--column-width:980px;--column-flex:980 }#comp-kdmv8oys3 { --bg-overlay-color:rgb(var(--color_12));--bg-gradient:none;--fill-layer-image-opacity:1;width:100%;--column-width:686px;--column-flex:686 }#comp-kdmv8oyr { --bg-overlay-color:rgb(var(--color_12));--bg-gradient:none;width:100%;--column-width:294px;--column-flex:294 }#comp-kdmvb0gi { --bg-overlay-color:transparent;--bg-gradient:none;width:100%;--column-width:586px;--column-flex:586 }#comp-kdmvb0g6 { --bg-overlay-color:rgb(var(--color_12));--bg-gradient:none;width:100%;--column-width:394px;--column-flex:394 }#comp-kfdtoucu1 { --bg-overlay-color:rgb(var(--color_12));--bg-gradient:none;width:100%;--column-width:980px;--column-flex:980 }#comp-kdmt6lj8 { height:auto }#pageBackground_c1dmp { --bg-position:absolute;--bg-overlay-color:rgb(var(--color_12));--bg-gradient:none;--fill-layer-background-overlay-color:transparent;--fill-layer-background-overlay-position:absolute }#comp-kdmu0n7u { position:absolute;width:100%;height:100%;--bg-overlay-color:transparent;--bg-gradient:none }#comp-kdmu0n8c { position:absolute;width:100%;height:100%;--bg-overlay-color:transparent;--bg-gradient:none }#comp-kdmu0n8h1 { position:absolute;width:100%;height:100%;--bg-overlay-color:transparent;--bg-gradient:none }
    </style>
    <style id="css_oh38i">
        [data-mesh-id=comp-kdn08irkinlineContent]{height:auto;width:100%;position:static;min-height:650px}[data-mesh-id=comp-kdn08irl1inlineContent]{height:auto;width:100%;position:static;min-height:650px}[data-mesh-id=comp-kdn08it2inlineContent]{height:auto;width:100%}[data-mesh-id=comp-kdn08it2inlineContent-gridContainer]{position:static;display:grid;height:auto;width:100%;min-height:500px;grid-template-rows:repeat(2, min-content) 1fr;grid-template-columns:100%}[data-mesh-id=comp-kdn08it2inlineContent-gridContainer] > [id="comp-kdn022uw"]{position:relative;margin:69px 0px 28px calc((100% - 441px) * 0.5);left:11px;grid-area:1 / 1 / 2 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdn08it2inlineContent-gridContainer] > [id="comp-kdn022vq"]{position:relative;margin:0px 0px 23px calc((100% - 441px) * 0.5);left:11px;grid-area:2 / 1 / 3 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdn08it2inlineContent-gridContainer] > [id="comp-kdn022w3"]{position:relative;margin:0px 0px 10px calc((100% - 441px) * 0.5);left:11px;grid-area:3 / 1 / 4 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdn08itdinlineContent]{height:auto;width:100%;position:static;min-height:500px}[data-mesh-id=comp-kdodoe0binlineContent]{height:auto;width:100%}[data-mesh-id=comp-kdodoe0binlineContent-gridContainer]{position:static;display:grid;height:auto;width:100%;min-height:767px;grid-template-rows:repeat(3, min-content) 1fr;grid-template-columns:100%}[data-mesh-id=comp-kdodoe0binlineContent-gridContainer] > [id="comp-kdn20d02"]{position:relative;margin:126px 0px 24px calc((100% - 490px) * 0.5);left:63px;grid-area:1 / 1 / 2 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdodoe0binlineContent-gridContainer] > [id="comp-kdn20cyz"]{position:relative;margin:0px 0px 14px calc((100% - 490px) * 0.5);left:63px;grid-area:2 / 1 / 3 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdodoe0binlineContent-gridContainer] > [id="comp-kdn20czj"]{position:relative;margin:0px 0px 37px calc((100% - 490px) * 0.5);left:63px;grid-area:3 / 1 / 4 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdodoe0binlineContent-gridContainer] > [id="comp-kfe4kvzz"]{position:relative;margin:0px 0px 10px calc((100% - 490px) * 0.5);left:63px;grid-area:4 / 1 / 5 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdodop22inlineContent]{height:auto;width:100%}[data-mesh-id=comp-kdodop22inlineContent-gridContainer]{position:static;display:grid;height:auto;width:100%;min-height:767px;grid-template-rows:repeat(3, min-content) 1fr;grid-template-columns:100%}[data-mesh-id=comp-kdodop22inlineContent-gridContainer] > [id="comp-kdn21pve"]{position:relative;margin:174px 0px 24px calc((100% - 490px) * 0.5);left:50px;grid-area:1 / 1 / 2 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdodop22inlineContent-gridContainer] > [id="comp-kdpydynd"]{position:relative;margin:0px 0px 49px calc((100% - 490px) * 0.5);left:50px;grid-area:2 / 1 / 3 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdodop22inlineContent-gridContainer] > [id="comp-kdn223yc"]{position:relative;margin:0px 0px 24px calc((100% - 490px) * 0.5);left:50px;grid-area:3 / 1 / 4 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdodop22inlineContent-gridContainer] > [id="comp-kdpyf5hl"]{position:relative;margin:0px 0px 10px calc((100% - 490px) * 0.5);left:50px;grid-area:4 / 1 / 5 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdpyl5rb2inlineContent]{height:auto;width:100%;position:static;min-height:615px}[data-mesh-id=comp-kdpyl5quinlineContent]{height:auto;width:100%}[data-mesh-id=comp-kdpyl5quinlineContent-gridContainer]{position:static;display:grid;height:auto;width:100%;min-height:615px;grid-template-rows:repeat(5, min-content) 1fr;grid-template-columns:100%}[data-mesh-id=comp-kdpyl5quinlineContent-gridContainer] > [id="comp-kdodpszv"]{position:relative;margin:100px 0px 26px calc((100% - 450px) * 0.5);left:11px;grid-area:1 / 1 / 2 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdpyl5quinlineContent-gridContainer] > [id="comp-kdodpt0g"]{position:relative;margin:0px 0px 31px calc((100% - 450px) * 0.5);left:13px;grid-area:2 / 1 / 3 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdpyl5quinlineContent-gridContainer] > [id="comp-kdodpt10"]{position:relative;margin:0px 0px 40px calc((100% - 450px) * 0.5);left:11px;grid-area:3 / 1 / 4 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdpyl5quinlineContent-gridContainer] > [id="comp-kdodpt1j"]{position:relative;margin:0px 0px 13px calc((100% - 450px) * 0.5);left:11px;grid-area:4 / 1 / 5 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdpyl5quinlineContent-gridContainer] > [id="comp-kdodpt22"]{position:relative;margin:0px 0px 13px calc((100% - 450px) * 0.5);left:11px;grid-area:5 / 1 / 6 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdpyl5quinlineContent-gridContainer] > [id="comp-kdodpt2l"]{position:relative;margin:0px 0px 10px calc((100% - 450px) * 0.5);left:11px;grid-area:6 / 1 / 7 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdoduiah1inlineContent]{height:auto;width:100%}[data-mesh-id=comp-kdoduiah1inlineContent-gridContainer]{position:static;display:grid;height:auto;width:100%;min-height:279px;grid-template-rows:min-content 1fr;grid-template-columns:100%}[data-mesh-id=comp-kdoduiah1inlineContent-gridContainer] > [id="comp-kdoduzhr"]{position:relative;margin:103px 0px 29px calc((100% - 490px) * 0.5);left:63px;grid-area:1 / 1 / 2 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdoduiah1inlineContent-gridContainer] > [id="comp-kdoduzid"]{position:relative;margin:0px 0px 10px calc((100% - 490px) * 0.5);left:63px;grid-area:2 / 1 / 3 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdoduy4linlineContent]{height:auto;width:100%;position:static;min-height:279px}[data-mesh-id=comp-kdodwmqh2inlineContent]{height:auto;width:100%}[data-mesh-id=comp-kdodwmqh2inlineContent-gridContainer]{position:static;display:grid;height:auto;width:100%;min-height:auto;grid-template-rows:1fr;grid-template-columns:100%}[data-mesh-id=comp-kdodwmqh2inlineContent-gridContainer] > [id="comp-kdpnp7fu"]{position:relative;margin:606px 0px 0px calc((100% - 446px) * 0);left:0px;grid-area:1 / 1 / 2 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdodwmqm1inlineContent]{height:auto;width:100%}[data-mesh-id=comp-kdodwmqm1inlineContent-gridContainer]{position:static;display:grid;height:auto;width:100%;min-height:auto;grid-template-rows:1fr;grid-template-columns:100%}[data-mesh-id=comp-kdodwmqm1inlineContent-gridContainer] > [id="comp-kdpnom3v"]{position:relative;margin:606px 0px 0px calc((100% - 454px) * 0);left:0px;grid-area:1 / 1 / 2 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdpnp7fuinlineContent]{height:auto;width:455px}[data-mesh-id=comp-kdpnp7fuinlineContent-gridContainer]{position:static;display:grid;height:auto;width:100%;min-height:auto;grid-template-rows:min-content 1fr;grid-template-columns:100%}[data-mesh-id=comp-kdpnp7fuinlineContent-gridContainer] > [id="comp-kdn28yjr1"]{position:relative;margin:58px 0px 26px 0;left:54px;grid-area:1 / 1 / 2 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdpnp7fuinlineContent-gridContainer] > [id="comp-kdoe6ee8"]{position:relative;margin:0px 0px 57px 0;left:54px;grid-area:2 / 1 / 3 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdpnom3vinlineContent]{height:auto;width:465px}[data-mesh-id=comp-kdpnom3vinlineContent-gridContainer]{position:static;display:grid;height:auto;width:100%;min-height:auto;grid-template-rows:min-content 1fr;grid-template-columns:100%}[data-mesh-id=comp-kdpnom3vinlineContent-gridContainer] > [id="comp-kdn28yjs3"]{position:relative;margin:58px 0px 26px 0;left:54px;grid-area:1 / 1 / 2 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdpnom3vinlineContent-gridContainer] > [id="comp-kdoe7cn6"]{position:relative;margin:0px 0px 57px 0;left:54px;grid-area:2 / 1 / 3 / 2;justify-self:start;align-self:start}[data-mesh-id=Containeroh38iinlineContent]{height:auto;width:100%}[data-mesh-id=Containeroh38iinlineContent-gridContainer]{position:static;display:grid;height:auto;width:100%;min-height:3392px;grid-template-rows:repeat(6, min-content) 1fr;grid-template-columns:100%;padding-bottom:0px;box-sizing:border-box}[data-mesh-id=Containeroh38iinlineContent-gridContainer] > [id="comp-kdn08iq0"]{position:relative;margin:0px 0px 10px calc((100% - 980px) * 0.5);left:0px;grid-area:1 / 1 / 2 / 2;justify-self:start;align-self:start}[data-mesh-id=Containeroh38iinlineContent-gridContainer] > [id="comp-kdn08iqf"]{position:relative;margin:0px 0px 10px calc((100% - 980px) * 0.5);left:0px;grid-area:1 / 1 / 2 / 2;justify-self:start;align-self:start}[data-mesh-id=Containeroh38iinlineContent-gridContainer] > [id="comp-kdoksitn"]{position:relative;margin:0px 0 10px 0;left:0;grid-area:3 / 1 / 4 / 2;justify-self:stretch;align-self:start}[data-mesh-id=Containeroh38iinlineContent-gridContainer] > [id="comp-kdododxq"]{position:relative;margin:7px 0px 45px calc((100% - 980px) * 0.5);left:0px;grid-area:3 / 1 / 4 / 2;justify-self:start;align-self:start}[data-mesh-id=Containeroh38iinlineContent-gridContainer] > [id="comp-kdpz8y5z"]{position:relative;margin:0px 0 14px 0;left:0;grid-area:4 / 1 / 5 / 2;justify-self:stretch;align-self:start}[data-mesh-id=Containeroh38iinlineContent-gridContainer] > [id="comp-kdpyl5ov"]{position:relative;margin:0px 0px 0 calc((100% - 980px) * 0.5);left:0px;grid-area:5 / 1 / 6 / 2;justify-self:start;align-self:start}[data-mesh-id=Containeroh38iinlineContent-gridContainer] > [id="comp-kdodui9e"]{position:relative;margin:0px 0px 0 calc((100% - 980px) * 0.5);left:0px;grid-area:6 / 1 / 7 / 2;justify-self:start;align-self:start}[data-mesh-id=Containeroh38iinlineContent-gridContainer] > [id="comp-kdokukt0"]{position:relative;margin:71px 0 10px 0;left:0;grid-area:6 / 1 / 7 / 2;justify-self:stretch;align-self:start}[data-mesh-id=Containeroh38iinlineContent-gridContainer] > [id="comp-kdodwmpl"]{position:relative;margin:0px 0px 0 calc((100% - 980px) * 0.5);left:0px;grid-area:7 / 1 / 8 / 2;justify-self:start;align-self:start}[data-mesh-id=Containeroh38iinlineContent-wedge-3]{visibility:hidden;height:723px;width:0;grid-area:1 / 1 / 3 / 2}#comp-kdoksitn{left:0;margin-left:0;width:100%;min-width:initial;height:21px}#comp-kdpz8y5z{left:0;margin-left:0;width:100%;min-width:initial;height:21px}#comp-kdokukt0{left:0;margin-left:0;width:100%;min-width:initial;height:21px}#comp-kdn022uw{width:423px;height:42px}#comp-kdn022vq{width:94px;height:18px}#comp-kdn022w3{width:417px;height:140px}#comp-kdn20d02{width:365px;height:42px}#comp-kdn20cyz{width:94px;height:18px}#comp-kdn20czj{width:365px;height:168px}#comp-kfe4kvzz{width:142px;height:40px}#comp-kdn21pve{width:363px;height:26px}#comp-kdpydynd{width:324px;height:112px}#comp-kdn223yc{width:363px;height:26px}#comp-kdpyf5hl{width:324px;height:112px}#comp-kdodpszv{width:429px;height:36px}#comp-kdodpt0g{width:94px;height:18px}#comp-kdodpt10{width:369px;height:112px}#comp-kdodpt1j{width:429px;height:24px}#comp-kdodpt22{width:429px;height:24px}#comp-kdodpt2l{width:429px;height:24px}#comp-kdoduzhr{width:410px;height:36px}#comp-kdoduzid{width:94px;height:18px}#comp-kdn28yjr1{width:324px;height:31px}#comp-kdoe6ee8{width:324px;height:66px}#comp-kdn28yjs3{width:320px;height:31px}#comp-kdoe7cn6{width:324px;height:66px}#oh38i{left:0;margin-left:0;width:100%;min-width:100%}#comp-kdn08iq0{margin-left:auto;margin-right:auto;width:calc(100% - 80px - 80px);min-width:980px}#comp-kdn08iqf{margin-left:auto;margin-right:auto;width:calc(100% - 80px - 80px);min-width:980px}#comp-kdododxq{margin-left:auto;margin-right:auto;width:calc(100% - 80px - 80px);min-width:980px}#comp-kdpyl5ov{margin-left:auto;margin-right:auto;width:calc(100% - 80px - 80px);min-width:980px}#comp-kdodui9e{margin-left:auto;margin-right:auto;width:calc(100% - 80px - 80px);min-width:980px}#comp-kdodwmpl{margin-left:auto;margin-right:auto;width:calc(100% - 80px - 80px);min-width:980px}#comp-kdn08irk{width:294px}#comp-kdn08irl1{width:686px}#comp-kdn08it2{width:441px}#comp-kdn08itd{width:539px}#comp-kdodoe0b{width:490px}#comp-kdodop22{width:490px}#comp-kdpyl5rb2{width:450px}#comp-kdpyl5qu{width:450px}#comp-kdoduiah1{width:490px}#comp-kdoduy4l{width:490px}#comp-kdodwmqh2{width:446px}#comp-kdodwmqm1{width:454px}#comp-kdpnp7fu{width:455px}#comp-kdpnom3v{width:465px}#masterPage{--pinned-layers-in-page:0}
        @font-face {
            font-display: block;
            font-family: "Barlow-ExtraLight";
            src: url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/51v0xj5VPw1cLYHNhfd8NFtXRa8TVwTICgirnJhmVJw.woff2") format("woff2"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/51v0xj5VPw1cLYHNhfd8ND8E0i7KZn-EPnyo3HZu7kw.woff") format("woff"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/51v0xj5VPw1cLYHNhfd8NKCWcynf_cDxXwCLxiixG1c.ttf") format("truetype"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/barlow-extralight-Svg.svg") format("svg");
        }
        @font-face {
            font-display: block;
            font-family: "Barlow-ExtraLight";
            font-weight: 700;
            src: url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/2woyxyDnPU0v4IiqYU9D1g.woff2") format("woff2"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/-HJgNsTwx9qXGSxqew62RQ.woff") format("woff"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/qoExc9IJQUjYXhlVZNNLgg.ttf") format("truetype"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/barlow-extralight-700-Svg.svg") format("svg");
        }
        @font-face {
            font-display: block;
            font-family: "Barlow-ExtraLight";
            font-style: italic;
            src: url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/14AxwKgJhKIO-YYUP_KtZag5eI2G47JWe0-AuFtD150.woff2") format("woff2"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/14AxwKgJhKIO-YYUP_KtZdIh4imgI8P11RFo6YPCPC0.woff") format("woff"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/14AxwKgJhKIO-YYUP_KtZV02b4v3fUxqf9CZJ1qUoIA.ttf") format("truetype"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/barlow-extralight-italic-Svg.svg") format("svg");
        }
        @font-face {
            font-display: block;
            font-family: "Barlow-ExtraLight";
            font-weight: 700;
            font-style: italic;
            src: url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/cdbGxfKO8gdkBd5U5TuXqPesZW2xOQ-xsNqO47m55DA.woff2") format("woff2"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/W1XpMGU0WrpbCawEdG1FM_esZW2xOQ-xsNqO47m55DA.woff") format("woff"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/_lIpJP17FZmSeklpAeOdnvesZW2xOQ-xsNqO47m55DA.ttf") format("truetype"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/barlow-extralight-700-italic-Svg.svg") format("svg");
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: italic;
            font-weight: 400;
            src: local('Barlow Italic'), local('Barlow-Italic'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHrv4kjgoGqM7E_Cfs0wH8DnzcjTrA.woff2") format('woff2');
            unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: italic;
            font-weight: 400;
            src: local('Barlow Italic'), local('Barlow-Italic'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHrv4kjgoGqM7E_Cfs1wH8DnzcjTrA.woff2") format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: italic;
            font-weight: 400;
            src: local('Barlow Italic'), local('Barlow-Italic'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHrv4kjgoGqM7E_Cfs7wH8Dnzcj.woff2") format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: italic;
            font-weight: 700;
            src: local('Barlow Bold Italic'), local('Barlow-BoldItalic'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHsv4kjgoGqM7E_CfOA5WohvToJdLm8BvE.woff2") format('woff2');
            unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: italic;
            font-weight: 700;
            src: local('Barlow Bold Italic'), local('Barlow-BoldItalic'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHsv4kjgoGqM7E_CfOA5WogvToJdLm8BvE.woff2") format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: italic;
            font-weight: 700;
            src: local('Barlow Bold Italic'), local('Barlow-BoldItalic'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHsv4kjgoGqM7E_CfOA5WouvToJdLm8.woff2") format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: normal;
            font-weight: 400;
            src: local('Barlow Regular'), local('Barlow-Regular'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHpv4kjgoGqM7E_A8s5ynghnQci.woff2") format('woff2');
            unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: normal;
            font-weight: 400;
            src: local('Barlow Regular'), local('Barlow-Regular'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHpv4kjgoGqM7E_Ass5ynghnQci.woff2") format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: normal;
            font-weight: 400;
            src: local('Barlow Regular'), local('Barlow-Regular'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHpv4kjgoGqM7E_DMs5ynghnQ.woff2") format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: normal;
            font-weight: 700;
            src: local('Barlow Bold'), local('Barlow-Bold'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHqv4kjgoGqM7E3t-4s6Fostz0rdom9.woff2") format('woff2');
            unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: normal;
            font-weight: 700;
            src: local('Barlow Bold'), local('Barlow-Bold'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHqv4kjgoGqM7E3t-4s6Vostz0rdom9.woff2") format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: normal;
            font-weight: 700;
            src: local('Barlow Bold'), local('Barlow-Bold'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHqv4kjgoGqM7E3t-4s51ostz0rdg.woff2") format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }
        @font-face {
            font-display: block;
            font-family:"Avenir-LT-W01_35-Light1475496";
            src:url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/edefe737-dc78-4aa3-ad03-3c6f908330ed.eot?#iefix");
            src:url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/edefe737-dc78-4aa3-ad03-3c6f908330ed.eot?#iefix") format("eot"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/0078f486-8e52-42c0-ad81-3c8d3d43f48e.woff2") format("woff2"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/908c4810-64db-4b46-bb8e-823eb41f68c0.woff") format("woff"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/4577388c-510f-4366-addb-8b663bcc762a.ttf") format("truetype"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/b0268c31-e450-4159-bfea-e0d20e2b5c0c.svg#b0268c31-e450-4159-bfea-e0d20e2b5c0c") format("svg");
        }
        @font-face {
            font-family: "Avenir-LT-W05_35-Light";
            src: url("//static.parastorage.com/services/santa-resources/resources/viewer/user-site-fonts/fonts/Avenir_Family_Pack/v1/AvenirLTW05-35Light.woff2") format("woff2"),
            url("//static.parastorage.com/services/santa-resources/resources/viewer/user-site-fonts/fonts/Avenir_Family_Pack/v1/AvenirLTW05-35Light.woff") format("woff");
        }

        #comp-kdn022uw { height:auto }#comp-kdn022vq { --opacity:1 }#comp-kdn022w3 { height:auto }#comp-kdn20d02 { height:auto }#comp-kdn20cyz { --opacity:1 }#comp-kdn20czj { height:auto }#comp-kfe4kvzz { --rd:0px;--trans1:border-color 0.4s ease 0s, background-color 0.4s ease 0s;--shd:none;--fnt:normal normal normal 16px/1.4em avenir-lt-w01_35-light1475496,avenir-lt-w05_35-light,sans-serif;--trans2:color 0.4s ease 0s;--txt:var(--color_15);--brw:1px;--bg:var(--color_12);--brd:var(--color_15);--bgh:var(--color_15);--brdh:var(--color_12);--txth:var(--color_11);--bgd:204,204,204;--alpha-bgd:1;--brdd:204,204,204;--alpha-brdd:1;--txtd:255,255,255;--alpha-txtd:1;--boxShadowToggleOn-shd:none;--alpha-txt:1;--alpha-brdh:0;--alpha-brd:1;--alpha-txth:1;--alpha-bg:0;--alpha-bgh:1;--shc-mutated-brightness:124,124,124;--label-align:center;--label-text-align:center }#comp-kdn21pve { --min-height:24px;height:auto }#comp-kdpydynd { height:auto }#comp-kdn223yc { --min-height:24px;height:auto }#comp-kdpyf5hl { height:auto }#comp-kdodpszv { height:auto }#comp-kdodpt0g { --opacity:1 }#comp-kdodpt10 { height:auto }#comp-kdodpt1j { --min-height:24px;height:auto }#comp-kdodpt22 { --min-height:24px;height:auto }#comp-kdodpt2l { --min-height:24px;height:auto }#comp-kdoduzhr { height:auto }#comp-kdoduzid { --opacity:1 }#comp-kdn28yjr1 { --min-height:24px;height:auto }#comp-kdoe6ee8 { height:auto }#comp-kdn28yjs3 { --min-height:24px;height:auto }#comp-kdoe7cn6 { height:auto }#oh38i { --param_boolean_previewHover:false;width:auto;min-height:3392px }#comp-kdn08iq0 { --bg-overlay-color:transparent;--bg-gradient:none;--padding:0px;--margin:0px;min-width:100%;--firstChildMarginTop:-1px;--lastChildMarginBottom:-1px }#comp-kdn08iqf { --bg-overlay-color:transparent;--bg-gradient:none;--padding:0px;--margin:0px;min-width:100%;--firstChildMarginTop:-1px;--lastChildMarginBottom:-1px }#comp-kdododxq { --bg-overlay-color:transparent;--bg-gradient:none;--padding:0px;--margin:0px;min-width:100%;--firstChildMarginTop:-1px;--lastChildMarginBottom:-1px }#comp-kdpyl5ov { --bg-overlay-color:transparent;--bg-gradient:none;--padding:0px;--margin:80px;min-width:100%;--lastChildMarginBottom:undefined }#comp-kdodui9e { --bg-overlay-color:transparent;--bg-gradient:none;--padding:0px;--margin:0px;min-width:100%;--firstChildMarginTop:-1px;--lastChildMarginBottom:-1px }#comp-kdodwmpl { --bg-overlay-color:transparent;--bg-gradient:none;--padding:0px;--margin:80px;min-width:100%;--lastChildMarginBottom:undefined }#comp-kdn08irk { --bg-overlay-color:rgb(var(--color_12));--bg-gradient:none;width:100%;--column-width:294px;--column-flex:294 }#comp-kdn08irl1 { --bg-overlay-color:rgb(var(--color_13));--bg-gradient:none;--fill-layer-image-opacity:1;width:100%;--column-width:686px;--column-flex:686 }#comp-kdn08it2 { --bg-overlay-color:rgb(var(--color_12));--bg-gradient:none;width:100%;--column-width:441px;--column-flex:441 }#comp-kdn08itd { --bg-overlay-color:transparent;--bg-gradient:none;width:100%;--column-width:539px;--column-flex:539 }#comp-kdodoe0b { --bg-overlay-color:rgb(var(--color_11));--bg-gradient:none;width:100%;--column-width:490px;--column-flex:490 }#comp-kdodop22 { --bg-overlay-color:rgb(var(--color_11));--bg-gradient:none;width:100%;--column-width:490px;--column-flex:490 }#comp-kdpyl5rb2 { --bg-overlay-color:rgb(var(--color_13));--bg-gradient:none;--fill-layer-image-opacity:1;width:100%;--column-width:450px;--column-flex:450 }#comp-kdpyl5qu { --bg-overlay-color:rgb(var(--color_12));--bg-gradient:none;width:100%;--column-width:450px;--column-flex:450 }#comp-kdoduiah1 { --bg-overlay-color:rgb(var(--color_12));--bg-gradient:none;width:100%;--column-width:490px;--column-flex:490 }#comp-kdoduy4l { --bg-overlay-color:rgb(var(--color_12));--bg-gradient:none;width:100%;--column-width:490px;--column-flex:490 }#comp-kdodwmqh2 { --bg-overlay-color:rgb(var(--color_13));--bg-gradient:none;--fill-layer-image-opacity:1;width:100%;--column-width:446px;--column-flex:446 }#comp-kdodwmqm1 { --bg-overlay-color:rgb(var(--color_13));--bg-gradient:none;--fill-layer-image-opacity:1;width:100%;--column-width:454px;--column-flex:454 }#comp-kdpnp7fu { --bg:var(--color_12);--alpha-bg:1;--shc-mutated-brightness:124,124,124 }#comp-kdpnom3v { --bg:var(--color_12);--alpha-bg:1;--shc-mutated-brightness:124,124,124 }#pageBackground_oh38i { --bg-position:absolute;--bg-overlay-color:rgb(var(--color_12));--bg-gradient:none;--fill-layer-background-overlay-color:transparent;--fill-layer-background-overlay-position:absolute }
    </style>
    <style id="css_ewwie">
        [data-mesh-id=comp-kdn6ur35inlineContent]{height:auto;width:100%;position:static;min-height:650px}[data-mesh-id=comp-kdn6ur37inlineContent]{height:auto;width:100%;position:static;min-height:650px}[data-mesh-id=comp-kdn6vlkk1inlineContent]{height:auto;width:100%}[data-mesh-id=comp-kdn6vlkk1inlineContent-gridContainer]{position:static;display:grid;height:auto;width:100%;min-height:500px;grid-template-rows:repeat(2, min-content) 1fr;grid-template-columns:100%}[data-mesh-id=comp-kdn6vlkk1inlineContent-gridContainer] > [id="comp-kdn6vlkn"]{position:relative;margin:70px 0px 22px calc((100% - 441px) * 0.5);left:11px;grid-area:1 / 1 / 2 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdn6vlkk1inlineContent-gridContainer] > [id="comp-kdn6vlkq1"]{position:relative;margin:0px 0px 22px calc((100% - 441px) * 0.5);left:11px;grid-area:2 / 1 / 3 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdn6vlkk1inlineContent-gridContainer] > [id="comp-kdn6vlkt2"]{position:relative;margin:0px 0px 10px calc((100% - 441px) * 0.5);left:11px;grid-area:3 / 1 / 4 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdn6vlkw1inlineContent]{height:auto;width:100%;position:static;min-height:500px}[data-mesh-id=comp-kdoea6t9inlineContent]{height:auto;width:100%;position:static;min-height:863px}[data-mesh-id=ContainerewwieinlineContent]{height:auto;width:100%}[data-mesh-id=ContainerewwieinlineContent-gridContainer]{position:static;display:grid;height:auto;width:100%;min-height:1654px;grid-template-rows:min-content 1fr;grid-template-columns:100%;padding-bottom:0px;box-sizing:border-box}[data-mesh-id=ContainerewwieinlineContent-gridContainer] > [id="comp-kdn6ur1m"]{position:relative;margin:0px 0px 70px calc((100% - 980px) * 0.5);left:0px;grid-area:1 / 1 / 2 / 2;justify-self:start;align-self:start}[data-mesh-id=ContainerewwieinlineContent-gridContainer] > [id="comp-kdn6vlj8"]{position:relative;margin:0px 0px 10px calc((100% - 980px) * 0.5);left:0px;grid-area:1 / 1 / 2 / 2;justify-self:start;align-self:start}[data-mesh-id=ContainerewwieinlineContent-gridContainer] > [id="comp-kdoea6rk"]{position:relative;margin:0px 0px 0 calc((100% - 980px) * 0.5);left:0px;grid-area:2 / 1 / 3 / 2;justify-self:start;align-self:start}[data-mesh-id=ContainerewwieinlineContent-gridContainer] > [id="TPASection_kdmrrxuv"]{position:relative;margin:67px 0px 3px calc((100% - 980px) * 0.5);left:0px;grid-area:2 / 1 / 3 / 2;justify-self:start;align-self:start}#TPASection_kdmrrxuv{width:981px;height:793px}#comp-kdn6vlkn{width:420px;height:42px}#comp-kdn6vlkq1{width:94px;height:18px}#comp-kdn6vlkt2{width:414px;height:140px}#ewwie{left:0;margin-left:0;width:100%;min-width:100%}#comp-kdn6ur1m{margin-left:auto;margin-right:auto;width:calc(100% - 80px - 80px);min-width:980px}#comp-kdn6vlj8{margin-left:auto;margin-right:auto;width:calc(100% - 80px - 80px);min-width:980px}#comp-kdoea6rk{margin-left:auto;margin-right:auto;width:calc(100% - 80px - 80px);min-width:980px}#comp-kdn6ur35{width:294px}#comp-kdn6ur37{width:686px}#comp-kdn6vlkk1{width:441px}#comp-kdn6vlkw1{width:539px}#comp-kdoea6t9{width:980px}#masterPage{--pinned-layers-in-page:0}
        @font-face {
            font-display: block;
            font-family: "Barlow-ExtraLight";
            src: url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/51v0xj5VPw1cLYHNhfd8NFtXRa8TVwTICgirnJhmVJw.woff2") format("woff2"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/51v0xj5VPw1cLYHNhfd8ND8E0i7KZn-EPnyo3HZu7kw.woff") format("woff"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/51v0xj5VPw1cLYHNhfd8NKCWcynf_cDxXwCLxiixG1c.ttf") format("truetype"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/barlow-extralight-Svg.svg") format("svg");
        }
        @font-face {
            font-display: block;
            font-family: "Barlow-ExtraLight";
            font-weight: 700;
            src: url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/2woyxyDnPU0v4IiqYU9D1g.woff2") format("woff2"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/-HJgNsTwx9qXGSxqew62RQ.woff") format("woff"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/qoExc9IJQUjYXhlVZNNLgg.ttf") format("truetype"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/barlow-extralight-700-Svg.svg") format("svg");
        }
        @font-face {
            font-display: block;
            font-family: "Barlow-ExtraLight";
            font-style: italic;
            src: url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/14AxwKgJhKIO-YYUP_KtZag5eI2G47JWe0-AuFtD150.woff2") format("woff2"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/14AxwKgJhKIO-YYUP_KtZdIh4imgI8P11RFo6YPCPC0.woff") format("woff"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/14AxwKgJhKIO-YYUP_KtZV02b4v3fUxqf9CZJ1qUoIA.ttf") format("truetype"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/barlow-extralight-italic-Svg.svg") format("svg");
        }
        @font-face {
            font-display: block;
            font-family: "Barlow-ExtraLight";
            font-weight: 700;
            font-style: italic;
            src: url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/cdbGxfKO8gdkBd5U5TuXqPesZW2xOQ-xsNqO47m55DA.woff2") format("woff2"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/W1XpMGU0WrpbCawEdG1FM_esZW2xOQ-xsNqO47m55DA.woff") format("woff"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/_lIpJP17FZmSeklpAeOdnvesZW2xOQ-xsNqO47m55DA.ttf") format("truetype"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/barlow-extralight-700-italic-Svg.svg") format("svg");
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: italic;
            font-weight: 400;
            src: local('Barlow Italic'), local('Barlow-Italic'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHrv4kjgoGqM7E_Cfs0wH8DnzcjTrA.woff2") format('woff2');
            unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: italic;
            font-weight: 400;
            src: local('Barlow Italic'), local('Barlow-Italic'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHrv4kjgoGqM7E_Cfs1wH8DnzcjTrA.woff2") format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: italic;
            font-weight: 400;
            src: local('Barlow Italic'), local('Barlow-Italic'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHrv4kjgoGqM7E_Cfs7wH8Dnzcj.woff2") format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: italic;
            font-weight: 700;
            src: local('Barlow Bold Italic'), local('Barlow-BoldItalic'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHsv4kjgoGqM7E_CfOA5WohvToJdLm8BvE.woff2") format('woff2');
            unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: italic;
            font-weight: 700;
            src: local('Barlow Bold Italic'), local('Barlow-BoldItalic'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHsv4kjgoGqM7E_CfOA5WogvToJdLm8BvE.woff2") format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: italic;
            font-weight: 700;
            src: local('Barlow Bold Italic'), local('Barlow-BoldItalic'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHsv4kjgoGqM7E_CfOA5WouvToJdLm8.woff2") format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: normal;
            font-weight: 400;
            src: local('Barlow Regular'), local('Barlow-Regular'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHpv4kjgoGqM7E_A8s5ynghnQci.woff2") format('woff2');
            unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: normal;
            font-weight: 400;
            src: local('Barlow Regular'), local('Barlow-Regular'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHpv4kjgoGqM7E_Ass5ynghnQci.woff2") format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: normal;
            font-weight: 400;
            src: local('Barlow Regular'), local('Barlow-Regular'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHpv4kjgoGqM7E_DMs5ynghnQ.woff2") format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: normal;
            font-weight: 700;
            src: local('Barlow Bold'), local('Barlow-Bold'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHqv4kjgoGqM7E3t-4s6Fostz0rdom9.woff2") format('woff2');
            unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: normal;
            font-weight: 700;
            src: local('Barlow Bold'), local('Barlow-Bold'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHqv4kjgoGqM7E3t-4s6Vostz0rdom9.woff2") format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: normal;
            font-weight: 700;
            src: local('Barlow Bold'), local('Barlow-Bold'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHqv4kjgoGqM7E3t-4s51ostz0rdg.woff2") format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }
        @font-face {
            font-display: block;
            font-family:"Avenir-LT-W01_35-Light1475496";
            src:url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/edefe737-dc78-4aa3-ad03-3c6f908330ed.eot?#iefix");
            src:url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/edefe737-dc78-4aa3-ad03-3c6f908330ed.eot?#iefix") format("eot"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/0078f486-8e52-42c0-ad81-3c8d3d43f48e.woff2") format("woff2"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/908c4810-64db-4b46-bb8e-823eb41f68c0.woff") format("woff"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/4577388c-510f-4366-addb-8b663bcc762a.ttf") format("truetype"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/b0268c31-e450-4159-bfea-e0d20e2b5c0c.svg#b0268c31-e450-4159-bfea-e0d20e2b5c0c") format("svg");
        }

        #comp-kdn6vlkn { height:auto }#comp-kdn6vlkq1 { --opacity:1 }#comp-kdn6vlkt2 { height:auto }#ewwie { width:auto;min-height:1654px }#comp-kdn6ur1m { --bg-overlay-color:transparent;--bg-gradient:none;--padding:0px;--margin:0px;min-width:100%;--firstChildMarginTop:-1px;--lastChildMarginBottom:-1px }#comp-kdn6vlj8 { --bg-overlay-color:transparent;--bg-gradient:none;--padding:0px;--margin:0px;min-width:100%;--firstChildMarginTop:-1px;--lastChildMarginBottom:-1px }#comp-kdoea6rk { --bg-overlay-color:transparent;--bg-gradient:none;--padding:0px;--margin:0px;min-width:100%;--firstChildMarginTop:-1px;--lastChildMarginBottom:-1px }#comp-kdn6ur35 { --bg-overlay-color:rgb(var(--color_12));--bg-gradient:none;width:100%;--column-width:294px;--column-flex:294 }#comp-kdn6ur37 { --bg-overlay-color:rgb(var(--color_13));--bg-gradient:none;--fill-layer-image-opacity:1;width:100%;--column-width:686px;--column-flex:686 }#comp-kdn6vlkk1 { --bg-overlay-color:rgb(var(--color_12));--bg-gradient:none;width:100%;--column-width:441px;--column-flex:441 }#comp-kdn6vlkw1 { --bg-overlay-color:transparent;--bg-gradient:none;width:100%;--column-width:539px;--column-flex:539 }#comp-kdoea6t9 { --bg-overlay-color:rgb(var(--color_11));--bg-gradient:none;width:100%;--column-width:980px;--column-flex:980 }#TPASection_kdmrrxuv { height:auto }#pageBackground_ewwie { --bg-position:absolute;--bg-overlay-color:rgb(var(--color_12));--bg-gradient:none;--fill-layer-background-overlay-color:transparent;--fill-layer-background-overlay-position:absolute }
    </style>
    <style id="css_bcs2h">
        [data-mesh-id=comp-kdo7vo6jinlineContent]{height:auto;width:100%;position:static;min-height:650px}[data-mesh-id=comp-kdo7vo6k2inlineContent]{height:auto;width:100%;position:static;min-height:650px}[data-mesh-id=comp-kdo7vo7xinlineContent]{height:auto;width:100%}[data-mesh-id=comp-kdo7vo7xinlineContent-gridContainer]{position:static;display:grid;height:auto;width:100%;min-height:500px;grid-template-rows:repeat(2, min-content) 1fr;grid-template-columns:100%}[data-mesh-id=comp-kdo7vo7xinlineContent-gridContainer] > [id="comp-kdo7vo7z2"]{position:relative;margin:69px 0px 28px calc((100% - 441px) * 0.5);left:13px;grid-area:1 / 1 / 2 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdo7vo7xinlineContent-gridContainer] > [id="comp-kdo7vo821"]{position:relative;margin:0px 0px 23px calc((100% - 441px) * 0.5);left:13px;grid-area:2 / 1 / 3 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdo7vo7xinlineContent-gridContainer] > [id="comp-kdo7vo8c"]{position:relative;margin:0px 0px 10px calc((100% - 441px) * 0.5);left:13px;grid-area:3 / 1 / 4 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdo7vo8g3inlineContent]{height:auto;width:100%;position:static;min-height:500px}[data-mesh-id=Containerbcs2hinlineContent]{height:auto;width:100%}[data-mesh-id=Containerbcs2hinlineContent-gridContainer]{position:static;display:grid;height:auto;width:100%;min-height:3621px;grid-template-rows:min-content 1fr;grid-template-columns:100%;padding-bottom:0px;box-sizing:border-box}[data-mesh-id=Containerbcs2hinlineContent-gridContainer] > [id="comp-kdo7vo4m"]{position:relative;margin:0px 0px 0 calc((100% - 980px) * 0.5);left:0px;grid-area:1 / 1 / 2 / 2;justify-self:start;align-self:start}[data-mesh-id=Containerbcs2hinlineContent-gridContainer] > [id="comp-kdo7vo53"]{position:relative;margin:0px 0px 10px calc((100% - 980px) * 0.5);left:0px;grid-area:1 / 1 / 2 / 2;justify-self:start;align-self:start}[data-mesh-id=Containerbcs2hinlineContent-gridContainer] > [id="TPASection_kdmw3930"]{position:relative;margin:0px 80px 0 80px;left:0;grid-area:2 / 1 / 3 / 2;justify-self:stretch;align-self:start}#TPASection_kdmw3930{left:0;margin-left:80px;width:calc(100% - 80px - 80px);min-width:initial;height:4258px}#comp-kdo7vo7z2{width:420px;height:42px}#comp-kdo7vo821{width:94px;height:18px}#comp-kdo7vo8c{width:415px;height:140px}#bcs2h{left:0;margin-left:0;width:100%;min-width:100%}#comp-kdo7vo4m{margin-left:auto;margin-right:auto;width:calc(100% - 80px - 80px);min-width:980px}#comp-kdo7vo53{margin-left:auto;margin-right:auto;width:calc(100% - 80px - 80px);min-width:980px}#comp-kdo7vo6j{width:294px}#comp-kdo7vo6k2{width:686px}#comp-kdo7vo7x{width:441px}#comp-kdo7vo8g3{width:539px}#masterPage{--pinned-layers-in-page:0}
        @font-face {
            font-display: block;
            font-family: "Barlow-ExtraLight";
            src: url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/51v0xj5VPw1cLYHNhfd8NFtXRa8TVwTICgirnJhmVJw.woff2") format("woff2"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/51v0xj5VPw1cLYHNhfd8ND8E0i7KZn-EPnyo3HZu7kw.woff") format("woff"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/51v0xj5VPw1cLYHNhfd8NKCWcynf_cDxXwCLxiixG1c.ttf") format("truetype"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/barlow-extralight-Svg.svg") format("svg");
        }
        @font-face {
            font-display: block;
            font-family: "Barlow-ExtraLight";
            font-weight: 700;
            src: url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/2woyxyDnPU0v4IiqYU9D1g.woff2") format("woff2"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/-HJgNsTwx9qXGSxqew62RQ.woff") format("woff"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/qoExc9IJQUjYXhlVZNNLgg.ttf") format("truetype"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/barlow-extralight-700-Svg.svg") format("svg");
        }
        @font-face {
            font-display: block;
            font-family: "Barlow-ExtraLight";
            font-style: italic;
            src: url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/14AxwKgJhKIO-YYUP_KtZag5eI2G47JWe0-AuFtD150.woff2") format("woff2"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/14AxwKgJhKIO-YYUP_KtZdIh4imgI8P11RFo6YPCPC0.woff") format("woff"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/14AxwKgJhKIO-YYUP_KtZV02b4v3fUxqf9CZJ1qUoIA.ttf") format("truetype"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/barlow-extralight-italic-Svg.svg") format("svg");
        }
        @font-face {
            font-display: block;
            font-family: "Barlow-ExtraLight";
            font-weight: 700;
            font-style: italic;
            src: url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/cdbGxfKO8gdkBd5U5TuXqPesZW2xOQ-xsNqO47m55DA.woff2") format("woff2"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/W1XpMGU0WrpbCawEdG1FM_esZW2xOQ-xsNqO47m55DA.woff") format("woff"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/_lIpJP17FZmSeklpAeOdnvesZW2xOQ-xsNqO47m55DA.ttf") format("truetype"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/barlow-extralight-700-italic-Svg.svg") format("svg");
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: italic;
            font-weight: 400;
            src: local('Barlow Italic'), local('Barlow-Italic'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHrv4kjgoGqM7E_Cfs0wH8DnzcjTrA.woff2") format('woff2');
            unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: italic;
            font-weight: 400;
            src: local('Barlow Italic'), local('Barlow-Italic'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHrv4kjgoGqM7E_Cfs1wH8DnzcjTrA.woff2") format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: italic;
            font-weight: 400;
            src: local('Barlow Italic'), local('Barlow-Italic'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHrv4kjgoGqM7E_Cfs7wH8Dnzcj.woff2") format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: italic;
            font-weight: 700;
            src: local('Barlow Bold Italic'), local('Barlow-BoldItalic'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHsv4kjgoGqM7E_CfOA5WohvToJdLm8BvE.woff2") format('woff2');
            unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: italic;
            font-weight: 700;
            src: local('Barlow Bold Italic'), local('Barlow-BoldItalic'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHsv4kjgoGqM7E_CfOA5WogvToJdLm8BvE.woff2") format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: italic;
            font-weight: 700;
            src: local('Barlow Bold Italic'), local('Barlow-BoldItalic'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHsv4kjgoGqM7E_CfOA5WouvToJdLm8.woff2") format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: normal;
            font-weight: 400;
            src: local('Barlow Regular'), local('Barlow-Regular'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHpv4kjgoGqM7E_A8s5ynghnQci.woff2") format('woff2');
            unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: normal;
            font-weight: 400;
            src: local('Barlow Regular'), local('Barlow-Regular'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHpv4kjgoGqM7E_Ass5ynghnQci.woff2") format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: normal;
            font-weight: 400;
            src: local('Barlow Regular'), local('Barlow-Regular'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHpv4kjgoGqM7E_DMs5ynghnQ.woff2") format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: normal;
            font-weight: 700;
            src: local('Barlow Bold'), local('Barlow-Bold'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHqv4kjgoGqM7E3t-4s6Fostz0rdom9.woff2") format('woff2');
            unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: normal;
            font-weight: 700;
            src: local('Barlow Bold'), local('Barlow-Bold'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHqv4kjgoGqM7E3t-4s6Vostz0rdom9.woff2") format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: normal;
            font-weight: 700;
            src: local('Barlow Bold'), local('Barlow-Bold'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHqv4kjgoGqM7E3t-4s51ostz0rdg.woff2") format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }
        @font-face {
            font-display: block;
            font-family:"Avenir-LT-W01_35-Light1475496";
            src:url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/edefe737-dc78-4aa3-ad03-3c6f908330ed.eot?#iefix");
            src:url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/edefe737-dc78-4aa3-ad03-3c6f908330ed.eot?#iefix") format("eot"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/0078f486-8e52-42c0-ad81-3c8d3d43f48e.woff2") format("woff2"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/908c4810-64db-4b46-bb8e-823eb41f68c0.woff") format("woff"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/4577388c-510f-4366-addb-8b663bcc762a.ttf") format("truetype"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/b0268c31-e450-4159-bfea-e0d20e2b5c0c.svg#b0268c31-e450-4159-bfea-e0d20e2b5c0c") format("svg");
        }
        @font-face {
            font-family: "Avenir-LT-W05_35-Light";
            src: url("//static.parastorage.com/services/santa-resources/resources/viewer/user-site-fonts/fonts/Avenir_Family_Pack/v1/AvenirLTW05-35Light.woff2") format("woff2"),
            url("//static.parastorage.com/services/santa-resources/resources/viewer/user-site-fonts/fonts/Avenir_Family_Pack/v1/AvenirLTW05-35Light.woff") format("woff");
        }

        #comp-kdo7vo7z2 { height:auto }#comp-kdo7vo821 { --opacity:1 }#comp-kdo7vo8c { height:auto }#bcs2h { width:auto;min-height:3621px }#comp-kdo7vo4m { --bg-overlay-color:transparent;--bg-gradient:none;--padding:0px;--margin:0px;min-width:100%;--firstChildMarginTop:-1px;--lastChildMarginBottom:-1px }#comp-kdo7vo53 { --bg-overlay-color:transparent;--bg-gradient:none;--padding:0px;--margin:0px;min-width:100%;--firstChildMarginTop:-1px;--lastChildMarginBottom:-1px }#comp-kdo7vo6j { --bg-overlay-color:rgb(var(--color_12));--bg-gradient:none;width:100%;--column-width:294px;--column-flex:294 }#comp-kdo7vo6k2 { --bg-overlay-color:rgb(var(--color_13));--bg-gradient:none;--fill-layer-image-opacity:1;width:100%;--column-width:686px;--column-flex:686 }#comp-kdo7vo7x { --bg-overlay-color:rgb(var(--color_12));--bg-gradient:none;width:100%;--column-width:441px;--column-flex:441 }#comp-kdo7vo8g3 { --bg-overlay-color:transparent;--bg-gradient:none;width:100%;--column-width:539px;--column-flex:539 } .s__9Pd_I.ogGSJT--madefor{--wbu-font-stack:var(--wix-font-stack);--wbu-font-weight-regular:var(--wix-font-weight-regular);--wbu-font-weight-medium:var(--wix-font-weight-medium);--wbu-font-weight-bold:var(--wix-font-weight-bold)} .sz2_jE.oLpVI6--madefor{--wbu-font-stack:var(--wix-font-stack);--wbu-font-weight-regular:var(--wix-font-weight-regular);--wbu-font-weight-medium:var(--wix-font-weight-medium);--wbu-font-weight-bold:var(--wix-font-weight-bold)}span,applet,object,iframe,h1,h2,h3,h4,h5,h6,p,blockquote,pre,abbr,acronym,address,big,cite,code,del,dfn,em,ins,kbd,q,s,samp,small,strike,strong,sub,sup,tt,var,b,u,i,center,dl,dt,dd,ol,ul,li,fieldset,form,label,legend,table,caption,tbody,tfoot,thead,tr,th,td,article,aside,canvas,details,embed,figure,figcaption,header,hgroup,main,menu,nav,output,ruby,summary,time,mark,audio,video{margin:0;padding:0;border:0;font-size:100%;font:inherit} .CqOQfS *:focus{outline:none !important;box-shadow:none !important} .wLMp8e{padding:21px 0 20px;color:#fff;background-color:#ffb200;text-align:center;font-family:HelveticaNeueW01-55Roma,Helvetica,Arial,sans-serif;font-size:14px} .L5x0Fp{position:relative;min-height:100%;box-sizing:border-box;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale} .L5x0Fp *{-webkit-tap-highlight-color:rgba(0,0,0,0)} .L5x0Fp *, .L5x0Fp *::before, .L5x0Fp *::after{box-sizing:inherit} .L5x0Fp a, .L5x0Fp a:visited{text-decoration:none} .L5x0Fp textarea{font-family:inherit}@media screen{html.enable-scroll .L5x0Fp,html.enable-scroll .aVF3iR,html.enable-scroll body,html.enable-scroll #root{height:100%;display:flex;flex-direction:column}} .aVF3iR{position:relative}html.enable-scroll .aVF3iR{flex:1;overflow-y:auto} .medium-gap .aVF3iR{padding-top:50px} .large-gap .aVF3iR{padding-top:100px}html{height:100%;margin:0;padding:0;-webkit-tap-highlight-color:rgba(0,0,0,0);touch-action:manipulation}body{margin:0;padding:0;-webkit-touch-callout:none;-webkit-text-size-adjust:none;-webkit-highlight:none;-webkit-tap-highlight-color:rgba(0,0,0,0)} #root{margin:0;padding:0}a{-webkit-tap-highlight-color:rgba(0,0,0,0)} .gallery-item-common-info.gallery-item-right-info{position:absolute;/*!rtl:begin:ignore*/right:0;/*!rtl:end:ignore*/} .gallery-item-common-info.gallery-item-left-info{position:absolute;/*!rtl:begin:ignore*/left:0;/*!rtl:end:ignore*/} #pro-gallery-container button.nav-arrows-container{box-sizing:border-box} .Led6wX{padding:0 20px 20px} .I5oIvV{position:fixed;top:0;bottom:0;z-index:999999999} .modal-enter .KEE15u, .modal-leave-active .KEE15u{opacity:0} .modal-enter .KEE15u.LuvPzG .CXAfWC, .modal-leave-active .KEE15u.LuvPzG .CXAfWC{transform:translate3d(0, -30%, 0)} .modal-enter .KEE15u.mROcZh .CXAfWC, .modal-leave-active .KEE15u.mROcZh .CXAfWC{transform:translate3d(0, 30%, 0)} .modal-enter .KEE15u.zK1G4N .CXAfWC, .modal-leave-active .KEE15u.zK1G4N .CXAfWC{/*!rtl:begin:ignore*/transform:translate3d(30%, 0, 0);/*!rtl:end:ignore*/} .modal-enter-active .KEE15u, .modal-leave-active .KEE15u{transition:opacity .2s ease-in} .modal-enter-active .KEE15u.LuvPzG .CXAfWC, .modal-leave-active .KEE15u.LuvPzG .CXAfWC{transition:transform .2s ease-in} .modal-enter-active .KEE15u.mROcZh .CXAfWC, .modal-leave-active .KEE15u.mROcZh .CXAfWC{transition:transform .2s ease-in} .modal-enter-active .KEE15u.zK1G4N .CXAfWC, .modal-leave-active .KEE15u.zK1G4N .CXAfWC{transition:transform .2s ease-in} .modal-enter-active .KEE15u{opacity:1} .modal-enter-active .KEE15u.LuvPzG .CXAfWC{transform:translate3d(0, 0, 0)} .modal-enter-active .KEE15u.mROcZh .CXAfWC{transform:translate3d(0, 0, 0)} .modal-enter-active .KEE15u.zK1G4N .CXAfWC{/*!rtl:begin:ignore*/transform:translate3d(0, 0, 0);/*!rtl:end:ignore*/} .UwtYxn{position:fixed;top:80px;transform:translateX(-50%);z-index:999999999999999} .UwtYxn .message-enter, .UwtYxn .message-leave-active{opacity:0} .UwtYxn .message-enter-active{transition:opacity .2s ease-in} .UwtYxn .message-leave-active{transition:opacity .4s ease-in} .UwtYxn .message-enter-active{opacity:1} .QCPRM5{position:fixed;top:0;display:flex;align-items:center;justify-content:space-between;padding:0 15px;z-index:3000;height:60px;font-family:"Avenir Next";font-size:14px;font-weight:500;color:#fff;transform:translateZ(0)} .QCPRM5.CKLwFf{font-family:HelveticaNeueW01-45Ligh,Helvetica,Arial,sans-serif;font-weight:300;font-size:16px} .QCPRM5.EdzGzM{background:#80c979} .QCPRM5.vcU6xU{background:#f06448} .QCPRM5.A1e1WH{background:#60adf0} .QCPRM5.E1Fz0F{justify-content:center} .QCPRM5.B58XC1{position:static} .Us6ADw{width:10px;height:10px;background:url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' width='10' height='10' viewBox='0 0 10 10'%3e %3cdefs%3e %3cstyle%3e .cls-1 %7b fill: white%3b fill-rule: evenodd%3b %7d %3c/style%3e %3c/defs%3e %3cpath id='x' class='cls-1' d='M3984.99%2c617.882l-1.11%2c1.109-3.88-3.882-3.88%2c3.882-1.11-1.109%2c3.88-3.882-3.88-3.882%2c1.11-1.109%2c3.88%2c3.882%2c3.88-3.882%2c1.11%2c1.109L3981.11%2c614Z' transform='translate(-3975 -609)'/%3e %3c/svg%3e") no-repeat center center;border:0} .Us6ADw:focus{outline:0} .QCPRM5.message-enter, .QCPRM5.message-leave-active{opacity:0} .QCPRM5.message-enter-active{transition:opacity 50ms ease-in} .QCPRM5.message-leave-active{transition:opacity .4s ease-in} .QCPRM5.message-enter-active{opacity:1} .qi0cF2{position:fixed;transform:translateX(-50%);z-index:999999999999999;top:0px;width:var(--root-width)} .app-desktop .qi0cF2{top:80px} .app-mobile .qi0cF2{width:100%} .WmZAIu{width:100%;display:flex;height:100%;position:relative} .jgXCfy{flex-shrink:0;position:relative;display:inline-block;width:48px;height:48px;padding:0;border:0;background:none;cursor:pointer} .jgXCfy:active::after{content:"";width:100%;height:100%;position:absolute;top:0;background-color:rgba(255,255,255,.3);border-radius:50%;pointer-events:none} .xaRdfw{display:inline-flex;width:100%;height:100%;align-items:center;justify-content:center} .srEdfd{display:none} .tSamLJ{position:absolute;bottom:11px;display:flex;justify-content:center;height:14px;width:14px;border-radius:50%;font-family:"Helvetica Neue",Helvetica,Arial,sans-serif;font-size:9px;line-height:14px} .VNQZuX{cursor:pointer} .HvPwlI{display:flex;align-items:center;justify-content:center;width:48px;height:48px} .HvPwlI:active::after{content:"";width:100%;height:100%;position:absolute;top:0;background-color:rgba(255,255,255,.3);border-radius:24px;pointer-events:none} .pFrNlV{display:none} .lR3yj5{width:100%;height:100%;border-radius:50%;-o-object-fit:cover;object-fit:cover;overflow:hidden} .lR3yj5:active::after{background-color:rgba(0,0,0,.3)} .lR3yj5::after{content:"";display:block;padding-bottom:100%} .dDcMfv{width:100%;height:100%} .Gka3kA{display:flex;align-items:center;justify-content:center;width:58px;height:48px;cursor:pointer} .mP9yec{display:block;width:30px;height:30px;border:2px solid rgba(255,255,255,.5);border-radius:50%;position:relative} .cERGdn{position:absolute;max-width:312px;z-index:2001;padding:14px 24px;border:1px solid;pointer-events:none;word-wrap:break-word} .Wi_s5V{font-size:14px;line-height:1.5;opacity:.8} .x7XlNO{position:absolute;top:100%;width:18px;height:12px;overflow:hidden} .yl6xpz{position:absolute;top:-5px;width:10px;height:10px;transform:rotate(45deg);border:1px solid} .ntKNd_{position:relative;display:inline-block;height:40px;margin:0;padding:0 24px;border-radius:0;border-style:none;cursor:pointer;vertical-align:middle;white-space:nowrap} .ntKNd_.rx6417:focus{box-shadow:inset 0 0 0 2px #1f77ff,inset 0 0 0 1px #fff !important} .ntKNd_.k7dt_P{width:100%} .ntKNd_.HZg_Yv{width:100%} .ntKNd_._9GNaw{padding:0 18px}@media(min-width: 980px){ .use-media-queries .ntKNd_._9GNaw{padding:0 24px}} .gt-sm .ntKNd_._9GNaw{padding:0 24px} .ntKNd_.EwBavj._9GNaw{padding:0 17px}@media(min-width: 980px){ .use-media-queries .ntKNd_.EwBavj._9GNaw{padding:0 17px}} .gt-sm .ntKNd_.EwBavj._9GNaw{padding:0 17px} .ntKNd_._v8VEx{height:50px} .ntKNd_.EwBavj{border-style:solid;border-width:1px;background-color:rgba(0,0,0,0)} .ntKNd_[disabled], .ntKNd_.v7Ydcc{opacity:.5} .ntKNd_:active:not([disabled]):not(.v7Ydcc)::after{content:"";width:100%;height:100%;position:absolute;top:0;pointer-events:none;background-color:rgba(0,0,0,.2)} .I4gY7Y{height:100%;width:100%;display:flex;align-items:center;justify-content:center;font-size:14px} .lDdct5{background-color:#3899ec;color:#fff} .L4KUvL{border-color:#3899ec;color:#3899ec} .pvCG_v{position:relative;min-height:100%;box-sizing:border-box;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale} .pvCG_v *{-webkit-tap-highlight-color:rgba(0,0,0,0)} .pvCG_v *, .pvCG_v *::before, .pvCG_v *::after{box-sizing:inherit} .pvCG_v a, .pvCG_v a:visited{text-decoration:none} .pvCG_v textarea{font-family:inherit}@media screen{html.enable-scroll .pvCG_v,html.enable-scroll .l7pLjR,html.enable-scroll body,html.enable-scroll #root{height:100%;display:flex;flex-direction:column}} .l7pLjR{position:relative}html.enable-scroll .l7pLjR{flex:1;overflow-y:auto} .medium-gap .l7pLjR{padding-top:50px} .large-gap .l7pLjR{padding-top:100px} .juyn_2{position:relative;display:inline-block} .juyn_2 .SHXBPR{width:40px;padding:0 17px} .juyn_2 .SHXBPR *{width:19px} .JvXA2e{position:absolute;top:38px;z-index:10;min-width:140px;border:1px solid} .KJPy_8 .JvXA2e{transform:translateX(-50%)} .JvXA2e.ovMwby{top:auto;bottom:38px} .OISqbJ{position:absolute;width:10px;height:10px;top:-5px;z-index:-1;margin:0 auto;transform:rotate(45deg);border:1px solid} .ovMwby .OISqbJ{top:auto;bottom:-5px} .Jv0dXX{fill:#fff} .QNVRC4{display:flex;align-items:center;justify-content:center;height:100%} .QNVRC4 .RiU29q{min-width:200px;top:40px} .Iv7yDs{display:inline-flex;align-items:center;height:100%;padding:0 16px;border:0;font-size:14px;font-family:"Avenir Next";font-weight:500;white-space:nowrap;color:#fff;background:rgba(0,0,0,0)} .zXI4K_{margin:0;font-size:14px;white-space:nowrap;overflow:hidden;text-overflow:ellipsis} .zXI4K_ a{color:inherit} .u8HfJE{flex-shrink:0;font-size:14px !important;text-decoration:none !important}@media print{ .u8HfJE{font-size:10pt !important;color:#000 !important}} .isG_om{display:flex;justify-content:space-between;align-items:center;height:50px} .Y1KjpM{flex:0 1 auto;display:flex;align-items:center;height:100%;overflow:hidden} .LoSEse{position:absolute;height:3px;background:rgba(0,0,0,.2)} .py6zKi{position:absolute;padding-top:20px;z-index:10} .IVqWoX{display:flex;position:relative} .IVqWoX:focus-visible:before{content:"";position:absolute;top:3px;left:3px;width:calc(100% - 6px);height:calc(100% - 6px);box-shadow:0 0 0 1px #fff,0 0 0 3px #116dff} .u3G4HW{padding-top:22px;box-shadow:0 10px 11px 0 rgba(0,0,0,.05);display:table} .u3G4HW .ucwOAz{padding:0} .u3G4HW .ucwOAz .TbpLtF{padding:15px 30px;overflow:hidden;text-overflow:ellipsis} .u3G4HW .ucwOAz .I4EMxH{height:1px;width:100%;opacity:.1} .u3G4HW .ucwOAz:first-child .TbpLtF{padding-top:0} .u3G4HW .ucwOAz:last-child .I4EMxH{display:none} .kU2es5{display:flex;width:100%;height:80px;font-family:"Avenir Next";font-size:14px} .LnLd_R{display:flex;overflow:hidden;list-style:none;flex-wrap:wrap;align-items:center;cursor:pointer} .LnLd_R li{display:flex;align-items:center;height:80px;white-space:nowrap} .d3XSOo{display:block;position:absolute;visibility:hidden;font-family:"Avenir Next"} .UlUPo4 a{text-decoration:none} .UlUPo4 a:hover{text-decoration:underline} .ckIWcX{position:relative} .ckIWcX:focus-visible:before{content:"";position:absolute;top:3px;left:3px;width:calc(100% - 6px);height:calc(100% - 6px);box-shadow:0 0 0 1px #fff,0 0 0 3px #116dff} .pMI_Le{border:0 !important;clip:rect(1px, 1px, 1px, 1px) !important;-webkit-clip-path:inset(50%) !important;clip-path:inset(50%) !important;height:1px !important;overflow:hidden !important;padding:0 !important;position:absolute !important;width:1px !important;white-space:nowrap !important} .FDtnt5{z-index:100} .Y0bxin{display:flex;justify-content:space-between;align-items:center;height:80px} .Y0bxin.iNQ68R{max-width:980px} .Y0bxin.uKtW2I{padding:0 20px} .XcjqVD{position:relative;max-width:940px;z-index:1} .kD1Yyw{position:relative;transition:all 500ms ease;height:16px} .kD1Yyw.ON6A2O{width:200px} .kD1Yyw.ON6A2O.ke7j1e{width:100%} .jMrgc4{top:0;bottom:0;margin:auto 0;cursor:pointer} .ON6A2O .jMrgc4{position:absolute} .m2yAmK{position:absolute;top:0;margin:0;width:100%;border:0;border-radius:0;padding:1px 0 0;font-size:14px;font-weight:500;background:rgba(0,0,0,0);transition:all 500ms ease;display:none} .m2yAmK::-webkit-input-placeholder{color:inherit;opacity:1;-webkit-transition:color 500ms;transition:color 500ms} .m2yAmK:-moz-placeholder{color:inherit;opacity:1;-moz-transition:color 500ms;transition:color 500ms} .m2yAmK::-moz-placeholder{color:inherit;opacity:1;-moz-transition:color 500ms;transition:color 500ms} .m2yAmK:-ms-input-placeholder{color:inherit;opacity:1;-ms-transition:color 500ms;transition:color 500ms} .m2yAmK:focus{outline:0} .ON6A2O .m2yAmK{display:inline-block} .ON6A2O .m2yAmK::-webkit-input-placeholder{opacity:.4} .ON6A2O .m2yAmK:-moz-placeholder{opacity:.4} .ON6A2O .m2yAmK::-moz-placeholder{opacity:.4} .ON6A2O .m2yAmK:-ms-input-placeholder{opacity:.4} .iLMCUc{display:none} .ON6A2O .iLMCUc{display:inline-block} .RFnngL{height:25px;padding:1px 0 0;font-size:14px;font-weight:500;visibility:hidden} .BN409S{height:1px;opacity:0;transition:all 500ms ease} .ON6A2O .BN409S{opacity:.5} .NMRR5Q{position:absolute;top:0;bottom:0;margin:auto;border:0;width:20px;height:20px;padding:0;background:rgba(0,0,0,0);cursor:pointer} .TSwWhL{opacity:.8} .clear-leave-active, .clear-enter-active{transition:opacity 500ms} .clear-leave, .clear-enter.clear-enter-active{opacity:1} .clear-leave.clear-leave-active, .clear-enter{opacity:0} .SuMDyr{display:inline-block;position:relative;width:32px;height:32px}@media(min-width: 660px){ .use-media-queries .SuMDyr.A0VEE9{width:40px;height:40px}} .gt-xs .SuMDyr.A0VEE9{width:40px;height:40px}@media(min-width: 660px){ .use-media-queries .SuMDyr.RVgBaT{width:92px;height:92px}} .gt-xs .SuMDyr.RVgBaT{width:92px;height:92px} .SuMDyr.pga4JZ{width:80px;height:80px}@media(min-width: 660px){ .use-media-queries .SuMDyr.pga4JZ{width:100px;height:100px}} .gt-xs .SuMDyr.pga4JZ{width:100px;height:100px} .SuMDyr.FDaKMP{width:40px;height:40px}@media(min-width: 660px){ .use-media-queries .SuMDyr.FDaKMP{width:80px;height:80px}} .gt-xs .SuMDyr.FDaKMP{width:80px;height:80px} .SuMDyr.evZe7a{width:30px;height:30px} .SuMDyr._GsoaO{width:20px;height:20px}@media(min-width: 660px){ .use-media-queries .SuMDyr._GsoaO{width:40px;height:40px}} .gt-xs .SuMDyr._GsoaO{width:40px;height:40px} .SuMDyr.HOJgGI{width:20px;height:20px}@media(min-width: 660px){ .use-media-queries .SuMDyr.HOJgGI{width:30px;height:30px}} .gt-xs .SuMDyr.HOJgGI{width:30px;height:30px} .EKJcYR{display:flex;align-items:center} .EKJcYR .xTozEv{min-width:200px;top:34px} .EKJcYR .dWlnoI .yDOqhU{padding:9px 25px 8px;min-width:122px;height:auto} .EKJcYR .MRlrpJ{font-size:14px} .SPucHg{color:#fff} .ySM9wx{position:relative;width:34px;padding:0;border:2px solid rgba(255,255,255,.5);border-radius:50%;font-size:0;background:rgba(0,0,0,0);cursor:pointer} .BpWyrA{height:36px} .IAJmuB{width:34px;height:34px} .VGQRbr{width:100%}@media(min-width: 980px){ .use-media-queries .VGQRbr{min-width:292px}} .gt-sm .VGQRbr{min-width:292px} .jFX22S{display:flex;justify-content:space-between;align-items:center;width:100%;font-size:16px} .XDcQnF.OdeuKJ{margin:0 -20px;padding:0 20px}@media(max-width: 979px){ .use-media-queries .XDcQnF.WIEJ0e{margin:0}} .lt-md .XDcQnF.WIEJ0e{margin:0} .mbKyhH{display:flex;flex-direction:column;flex:1 0 auto;margin-top:12px}@media(min-width: 980px){ .use-media-queries .mbKyhH{margin-top:0}} .gt-sm .mbKyhH{margin-top:0} .mbKyhH.hYUP9L{margin-top:0} .SO9njw{display:flex;max-width:940px;min-height:450px} .arh2qf{flex:1;display:flex;align-items:center;justify-content:center;flex-direction:column;margin-top:12px;padding:36px 30px;text-align:center;overflow:hidden} .app-mobile .arh2qf{box-shadow:0 1px rgba(0,0,0,.1)}@media(min-width: 980px){ .use-media-queries .arh2qf{height:450px;width:940px;margin:20px auto;padding:0;border-style:solid}} .gt-sm .arh2qf{height:450px;width:940px;margin:20px auto;padding:0;border-style:solid} .cJscj1{margin-bottom:15px;font-size:22px;line-height:34px} .cJscj1:focus{box-shadow:none !important}@media(min-width: 980px){ .use-media-queries .cJscj1{margin-bottom:18px;font-size:32px;max-width:620px;line-height:1.5}} .gt-sm .cJscj1{margin-bottom:18px;font-size:32px;max-width:620px;line-height:1.5} .cTdFFO{line-height:1.5;font-size:16px}@media(min-width: 980px){ .use-media-queries .cTdFFO{max-width:620px;font-size:16px}} .gt-sm .cTdFFO{max-width:620px;font-size:16px} .wdbmEY{font-size:18px;margin-top:18px}@media(min-width: 980px){ .use-media-queries .wdbmEY{margin-top:48px}} .gt-sm .wdbmEY{margin-top:48px} .nAaNon{color:#000;background-color:#fff} .JNxVBA{flex:0 0 auto;display:flex;justify-content:center;align-items:center} .mnxIuN{display:inline-block;width:10px;height:10px;border-radius:100%;margin:18px 2px;opacity:0} .mnxIuN:nth-child(1){-webkit-animation:xDDB8S 1s ease-in-out infinite;animation:xDDB8S 1s ease-in-out infinite} .mnxIuN:nth-child(2){-webkit-animation:xDDB8S 1s ease-in-out .33s infinite;animation:xDDB8S 1s ease-in-out .33s infinite} .mnxIuN:nth-child(3){-webkit-animation:xDDB8S 1s ease-in-out .66s infinite;animation:xDDB8S 1s ease-in-out .66s infinite}@-webkit-keyframes xDDB8S{0%,100%{opacity:0}60%{opacity:1}}@keyframes xDDB8S{0%,100%{opacity:0}60%{opacity:1}} .sWHFDc{display:flex;justify-content:center;align-items:center;margin:30px 0 374px} .Pgg7t6{position:absolute;top:0;width:100%;z-index:1} .Pgg7t6.DOBxQK{opacity:0;transition:opacity .8s} .KYa5Ob{width:100%;position:relative} .UDG0Mv{opacity:0;z-index:2} .UDG0Mv.tVPYRv{opacity:1;transition:opacity .8s} :not(.pA5zEx) .pCwu60 .bDY8vo, .pA5zEx .pCwu60 .bDY8vo{width:105%;height:108%;margin:-2.5% 0 0} .CsBZWG, .IM8lAo{position:absolute;top:0;width:100%;height:100%} .pA5zEx .t_gxYl{opacity:0;transform:translateY(50px);transition:.8s transform cubic-bezier(0.165, 0.84, 0.44, 1),.8s opacity cubic-bezier(0.165, 0.84, 0.44, 1)} .pA5zEx .pCwu60 .t_gxYl{opacity:1;transform:translateY(0px);transition-delay:.3s} .pA5zEx .pCwu60 .bDY8vo{transition-delay:0s} .pA5zEx .bDY8vo{transition:.25s width linear .3s,.25s height linear .3s,.25s margin linear .3s} .pA5zEx .pCwu60 .r8jqp0{transition-delay:.2s} .pA5zEx .r8jqp0{transition:.3s left ease-in-out} .pA5zEx .Dx7Mw7, .pA5zEx .HuGN_e{transition:.2s color linear} .pA5zEx .YSHEnd{transition:.2s fill linear} .FVZjgU{display:flex;flex-direction:row;justify-content:space-between;max-width:1920px;min-width:940px;width:100%}@media(max-width: 1564px){ .use-media-queries .FVZjgU{max-width:1653px}} .lte-banner-w1564 .FVZjgU{max-width:1653px}@media(max-width: 1425px){ .use-media-queries .FVZjgU{max-width:1424px}} .lte-banner-w1425 .FVZjgU{max-width:1424px}@media(max-width: 1181px){ .use-media-queries .FVZjgU{max-width:1275px}} .lte-banner-w1181 .FVZjgU{max-width:1275px}@media(max-width: 980px){ .use-media-queries .FVZjgU{margin:0 20px}} .lte-banner-w980 .FVZjgU{margin:0 20px} .grmtk_{display:flex;flex-direction:column;flex:none;width:460px}@media(max-width: 1564px){ .use-media-queries .grmtk_{width:460px}} .lte-banner-w1564 .grmtk_{width:460px}@media(max-width: 1425px){ .use-media-queries .grmtk_{width:365px}} .lte-banner-w1425 .grmtk_{width:365px}@media(max-width: 1181px){ .use-media-queries .grmtk_{width:312px}} .lte-banner-w1181 .grmtk_{width:312px}@media(max-width: 980px){ .use-media-queries .grmtk_{width:292px}} .lte-banner-w980 .grmtk_{width:292px} .k5QnMY{flex:1} .HuGN_e{line-height:1.3;font-size:48px}@media(max-width: 1564px){ .use-media-queries .HuGN_e{font-size:40px}} .lte-banner-w1564 .HuGN_e{font-size:40px}@media(max-width: 1425px){ .use-media-queries .HuGN_e{font-size:32px}} .lte-banner-w1425 .HuGN_e{font-size:32px}@media(max-width: 1181px){ .use-media-queries .HuGN_e{font-size:28px}} .lte-banner-w1181 .HuGN_e{font-size:28px} .b5cuHs{margin-bottom:23px} .Dx7Mw7{display:inline-block;line-height:1.5;font-size:20px} .YSHEnd{display:inline-block;vertical-align:middle} .Q3CjbN{height:1px} .p165aB{position:relative;flex:none;width:1064px;height:608px;overflow:hidden}@media(max-width: 1564px){ .use-media-queries .p165aB{width:925px;height:528px}} .lte-banner-w1564 .p165aB{width:925px;height:528px}@media(max-width: 1425px){ .use-media-queries .p165aB{width:776px;height:443px}} .lte-banner-w1425 .p165aB{width:776px;height:443px}@media(max-width: 1181px){ .use-media-queries .p165aB{width:628px;height:358px}} .lte-banner-w1181 .p165aB{width:628px;height:358px}@media(max-width: 980px){ .use-media-queries .p165aB{width:608px;height:347px}} .lte-banner-w980 .p165aB{width:608px;height:347px} .bDY8vo{width:100%;height:100%;-o-object-fit:cover;object-fit:cover} :not(.pA5zEx) .IM8lAo{opacity:0} .pA5zEx .IM8lAo{opacity:0;transition:.5s opacity linear .3s} :not(.pA5zEx) .pCwu60 .IM8lAo{opacity:.95} .pA5zEx .pCwu60 .IM8lAo{opacity:.95;transition-delay:0s} .ZUwECn{width:100%;height:100%;display:flex;flex-direction:column;justify-content:center;align-items:center} .w9PuUH{height:47px;overflow:hidden}@media(max-width: 1564px){ .use-media-queries .w9PuUH{height:44px}} .lte-banner-w1564 .w9PuUH{height:44px} .t_gxYl{line-height:1.5;font-size:20px}@media(max-width: 1425px){ .use-media-queries .t_gxYl{font-size:16px}} .lte-banner-w1425 .t_gxYl{font-size:16px} :not(.pA5zEx) .t_gxYl{visibility:hidden} :not(.pA5zEx) .pCwu60 .t_gxYl{visibility:visible} .nsHR1e{position:relative;width:426px}@media(max-width: 1181px){ .use-media-queries .nsHR1e{width:310px}} .lte-banner-w1181 .nsHR1e{width:310px} .r8jqp0{position:absolute;height:1px} .EX6A7O{position:relative;width:100%;padding-top:57.1875%} .onZP9b{position:absolute;top:0;bottom:0} .UUxi80{width:100%;height:100%;-o-object-fit:cover;object-fit:cover} .RJzprR{padding:24px 18px} .IbjI2_{line-height:1.5;font-size:18px} .app-desktop .t7xIqE{margin-bottom:70px} .KhVdjt, .acbUIp, .IeY3vx, .gwgQCb, .lfi41p, .sI4Nnn{line-height:1} .wR7lgz{display:flex} .wR7lgz .sI4Nnn{opacity:1;cursor:inherit} .wR7lgz .RcfyI6, .wR7lgz .ixAZGX{cursor:pointer;font-size:14px} .wR7lgz .ixAZGX{opacity:.8} .Q00AY9{display:flex;align-items:center;height:100%;font-family:"Avenir Next";font-size:16px}@media only screen and (max-width: 500px){ .Q00AY9{max-width:100%;overflow-y:hidden}} .Zleldg, .IEV8qS, .r1vD9L{display:flex;align-items:center;justify-content:center} .IEV8qS, .r1vD9L{margin:0 3px;min-width:36px;min-height:36px;cursor:pointer;color:inherit} .MdMxZP .IEV8qS, .MdMxZP .r1vD9L{margin:0 6px} .FGoscc{cursor:default} .XPZZnN{opacity:.4;cursor:default} .Zleldg{min-width:48px;margin:0 6px} .aq_u4p{display:flex;justify-content:center;align-items:center;margin:16px 0 30px} .aq_u4p.HcArW_{margin:1px 0 0;min-height:68px} .DKOpYy{width:100%;padding:0} .DKOpYy.vvQBt4{max-width:980px;padding:14px 20px;margin:0 auto}@media(min-width: 660px){ .use-media-queries .DKOpYy{padding-top:0}} .gt-xs .DKOpYy{padding-top:0}@media(min-width: 980px){ .use-media-queries .DKOpYy{width:980px;padding:14px 20px;margin:0 auto}} .gt-sm .DKOpYy{width:980px;padding:14px 20px;margin:0 auto} .KgltKb.igf0Lt, .KgltKb.fxXgT_, .KgltKb.yuzH_C{width:100%;padding:14px 20px;margin:0 auto} .F7kZNY{position:relative;margin-bottom:0}@media(min-width: 660px){ .use-media-queries .uHvfGY.yqPclT{min-width:620px}} .gt-xs .uHvfGY.yqPclT{min-width:620px}@media(min-width: 980px){ .use-media-queries .uHvfGY, .use-media-queries .uHvfGY.yqPclT{max-width:940px}} .gt-sm .uHvfGY, .gt-sm .uHvfGY.yqPclT{max-width:940px} .xU3UnN{display:flex;padding-bottom:20px} .xU3UnN:empty{display:none}@media(min-width: 980px){ .use-media-queries .xU3UnN{width:292px} .yuzH_C .use-media-queries .xU3UnN{width:454px}} .gt-sm .xU3UnN{width:292px} .yuzH_C .gt-sm .xU3UnN{width:454px} .Kh_UDc{margin:-16px} .Kh_UDc.BRRma6{display:flex;flex-wrap:wrap} .Kh_UDc.igf0Lt, .Kh_UDc.fxXgT_, .Kh_UDc.yuzH_C{display:grid;grid-template-columns:repeat(auto-fit, 324px);justify-content:center}@media all and (-ms-high-contrast: none),(-ms-high-contrast: active){ .Kh_UDc.igf0Lt, .Kh_UDc.fxXgT_, .Kh_UDc.yuzH_C{display:flex;flex-wrap:wrap;justify-content:center}} .Kh_UDc.yuzH_C{grid-template-columns:repeat(auto-fit, 486px)} .Kh_UDc .rFVtlu{padding:16px} .uqsKHt{display:block;position:relative;list-style-type:none} .uHvfGY .uqsKHt{margin-bottom:32px} .yqPclT .uqsKHt{width:292px} .sVN_en .uqsKHt{width:454px} .oR0wvi .uqsKHt{width:100%} .igf0Lt .uqsKHt{width:292px;height:426px} .igf0Lt.lNJcfx .uqsKHt{height:376px} .vvQBt4 .uqsKHt{width:100%} .fxXgT_ .uqsKHt{width:292px;height:292px} .yuzH_C .uqsKHt{width:454px;height:454px} .R9UFnY{display:flex;flex-direction:column;justify-content:center} .app-desktop .R9UFnY{max-width:1920px;min-width:100%;min-height:330px}@media(max-width: 980px){ .use-media-queries .app-desktop .R9UFnY{padding:0 20px}} .lte-w980 .app-desktop .R9UFnY{padding:0 20px} .app-mobile .R9UFnY{padding:24px 18px} .gUKJE0{line-height:1.3} .app-desktop .gUKJE0{width:940px;font-size:64px}@media(max-width: 1364px){ .use-media-queries .app-desktop .gUKJE0{font-size:48px}} .lte-category-header-w1364 .app-desktop .gUKJE0{font-size:48px} .app-mobile .gUKJE0{font-size:26px} .app-desktop .RpSBjv{width:600px;margin-top:18px} .app-mobile .RpSBjv{margin-top:12px} .KtXVa9{line-height:1.5} .app-desktop .KtXVa9{font-size:20px} .app-mobile .KtXVa9{font-size:16px} .P79xUp{height:50px;font-size:14px} .tr91VF{position:relative;display:flex;align-items:center;justify-content:space-between;height:100%} .w4kgLO{flex:1 1 auto;overflow:hidden;text-overflow:ellipsis;white-space:nowrap} .Rgs8Bb{flex:0 0 auto} .Wi9Gc9 *{box-sizing:border-box} .Wi9Gc9{position:relative;display:inline-block} .Wi9Gc9 a{color:#333;text-decoration:none} .Wi9Gc9>.ZKqkU8{display:inline-block;padding:6px 15px;line-height:1.5em} .Wi9Gc9>.ZKqkU8:after{display:inline-block;content:"▼"} .Wi9Gc9 .E_WBG4{display:none} .Wi9Gc9 ul{display:block;position:absolute;top:calc(1.5em + 14px);padding:6px 0;margin:0;width:100%;z-index:999;list-style:none} .Wi9Gc9 ul a{display:block;padding:6px 15px} .Wi9Gc9 ul:target~a:after{content:"▲"} .Wi9Gc9 ul:target~a.E_WBG4{display:block;position:absolute;top:0;height:100%;width:100%;text-indent:-100vw;z-index:1000} .HdgO4m{position:absolute;top:0;height:100%;width:100%;opacity:0;font-size:16px} .t4o8yl{height:100%;border:0;padding:0;background:rgba(0,0,0,0)} .t4o8yl:focus{outline:0} .opxKQF{flex:0 0 auto} .lnfRGj{display:inline-flex;align-items:center;height:100%} .lnfRGj:active{opacity:.5} .PRDfW5{font-size:14px;line-height:1;color:inherit;white-space:nowrap;text-overflow:ellipsis} .PRDfW5:empty{display:none} .JKDAPr{height:100%;font-size:0;margin:0} .QCDchW{font-size:14px !important;text-decoration:none !important}@media(min-width: 980px){ .use-media-queries .YFveWU{margin:0 auto}} .gt-sm .YFveWU{margin:0 auto} .YFveWU .vYilfO{color:inherit;font:inherit;text-decoration:underline} .JEGk06{font-weight:bold} .Wr5XZS{margin:20px} .cvByyI{max-width:940px;margin:0 auto} .D1xXio{color:inherit} .RiOfiW{border-radius:0;display:block;background:#fff;cursor:default} .RiOfiW.ac4Ofz.JlQ2sc{padding-top:30px} .RiOfiW.jDqjOt, .RiOfiW.lyd6fK, .RiOfiW.i_25UC, .RiOfiW.yWOr1I, .RiOfiW.hKl06e, .RiOfiW.UUSLFD, .RiOfiW.x_FPRX{display:flex;flex-direction:column;height:100%} .app-desktop .RiOfiW{border-width:1px;border-style:solid} .RiOfiW .PoYsMP, .RiOfiW .EfvfyL, .RiOfiW .NAWdyL{display:block} .RiOfiW.ivAnV0, .RiOfiW.NAWdyL, .RiOfiW.PoYsMP, .RiOfiW.EfvfyL, .RiOfiW.g8vcTX, .RiOfiW.TBrkhx{display:flex} .RiOfiW.ivAnV0.ac4Ofz, .RiOfiW.NAWdyL.ac4Ofz, .RiOfiW.PoYsMP.ac4Ofz, .RiOfiW.EfvfyL.ac4Ofz, .RiOfiW.g8vcTX.ac4Ofz, .RiOfiW.TBrkhx.ac4Ofz{height:auto}@media(max-width: 685px){ .use-media-queries .RiOfiW.ivAnV0, .use-media-queries .RiOfiW.NAWdyL, .use-media-queries .RiOfiW.PoYsMP, .use-media-queries .RiOfiW.EfvfyL, .use-media-queries .RiOfiW.g8vcTX, .use-media-queries .RiOfiW.TBrkhx{flex-direction:column}} .lt-sm .RiOfiW.ivAnV0, .lt-sm .RiOfiW.NAWdyL, .lt-sm .RiOfiW.PoYsMP, .lt-sm .RiOfiW.EfvfyL, .lt-sm .RiOfiW.g8vcTX, .lt-sm .RiOfiW.TBrkhx{flex-direction:column} .RiOfiW.TBrkhx, .RiOfiW.g8vcTX, .RiOfiW.PoYsMP, .RiOfiW.EfvfyL, .RiOfiW.NAWdyL{height:100%} .jDqjOt .ITPCIq, .lyd6fK .ITPCIq, .i_25UC .ITPCIq, .yWOr1I .ITPCIq, .hKl06e .ITPCIq, .UUSLFD .ITPCIq, .x_FPRX .ITPCIq{height:165px;padding-bottom:0} .RiOfiW.hKl06e{position:absolute;width:100%} .feQY6g .ITPCIq{height:400px;padding-bottom:0} .ivAnV0 .ITPCIq, .NAWdyL .ITPCIq, .PoYsMP .ITPCIq, .EfvfyL .ITPCIq, .g8vcTX .ITPCIq, .TBrkhx .ITPCIq{padding-bottom:0} .ivAnV0 .pccjOy, .ivAnV0 .ITPCIq, .NAWdyL .pccjOy, .NAWdyL .ITPCIq, .PoYsMP .pccjOy, .PoYsMP .ITPCIq, .EfvfyL .pccjOy, .EfvfyL .ITPCIq, .g8vcTX .pccjOy, .g8vcTX .ITPCIq, .TBrkhx .pccjOy, .TBrkhx .ITPCIq{position:absolute;top:0;bottom:0} .iSTCpN{padding:30px 18px 25px;width:100%} .app-mobile .iSTCpN.QlVZyW{box-shadow:0 1px rgba(0,0,0,.1)} .iSTCpN.glhtJc.JufO9Q, .iSTCpN.WdRrHJ.JufO9Q{padding:30px 18px} .iSTCpN.glhtJc.JlQ2sc, .iSTCpN.WdRrHJ.JlQ2sc{padding-bottom:0} .iSTCpN.G1V6el{padding:23px} .iSTCpN.G1V6el.JufO9Q{padding-bottom:7px} .iSTCpN.KwFkyo, .iSTCpN.x_FPRX{padding:24px 29px} .iSTCpN.KwFkyo.JufO9Q, .iSTCpN.x_FPRX.JufO9Q{padding-bottom:7px} .iSTCpN.feQY6g{padding:42px 47px 37px 47px} .iSTCpN.feQY6g.JufO9Q{padding:42px 47px 13px 47px} .iSTCpN.jDqjOt, .iSTCpN.lyd6fK, .iSTCpN.i_25UC, .iSTCpN.yWOr1I, .iSTCpN.UUSLFD, .iSTCpN.x_FPRX{display:flex;flex-direction:column;height:100%;min-height:0;padding:var(--blog-pl-common-padding-top) 24px var(--blog-pl-common-padding-bottom)} .iSTCpN.hKl06e{display:flex;flex-direction:column;height:100%;min-height:0;padding:var(--blog-pl-slider-padding-top) 16px var(--blog-pl-slider-padding-bottom)} .iSTCpN.jDqjOt.ac4Ofz, .iSTCpN.lyd6fK.ac4Ofz, .iSTCpN.hKl06e.ac4Ofz, .iSTCpN.UUSLFD.ac4Ofz{height:100%} .iSTCpN.jDqjOt.ac4Ofz.JufO9Q, .iSTCpN.lyd6fK.ac4Ofz.JufO9Q, .iSTCpN.hKl06e.ac4Ofz.JufO9Q, .iSTCpN.UUSLFD.ac4Ofz.JufO9Q{height:100%} .iSTCpN.ivAnV0{display:flex;flex-direction:column;padding:40px 48px 37px}@media(min-width: 686px)and (max-width: 980px){ .use-media-queries .iSTCpN.ivAnV0{padding:calc(40px + (var(--root-width) - 981px)/19) calc(48px + (var(--root-width) - 981px)/19)}} .w686-980 .iSTCpN.ivAnV0{padding:calc(40px + (var(--root-width) - 981px)/19) calc(48px + (var(--root-width) - 981px)/19)}@media(max-width: 685px){ .use-media-queries .iSTCpN.ivAnV0{padding:24px}} .lt-sm .iSTCpN.ivAnV0{padding:24px} .iSTCpN.g8vcTX, .iSTCpN.TBrkhx{display:flex;flex-direction:column;padding-top:24px;padding-bottom:24px;overflow:hidden;flex:1} .iSTCpN.PoYsMP, .iSTCpN.EfvfyL, .iSTCpN.NAWdyL{padding:0} .iSTCpN.sCkhyN{height:100%;width:100%;position:absolute;top:0;padding:27px 24px;display:inline-block} .iSTCpN.sCkhyN.ac4Ofz{padding:32px 24px} .iSTCpN.sCkhyN.KEL4fK{background-color:rgba(0,0,0,.45)} .iSTCpN.hp0NoL, .iSTCpN.FK1tXh, .iSTCpN.JnzaaY{display:flex;flex-direction:column} .iSTCpN.LdmkLw{position:absolute;top:0;display:inline-block} .iSTCpN.hp0NoL, .iSTCpN.FK1tXh, .iSTCpN.LdmkLw, .iSTCpN.JnzaaY{height:100%;width:100%;padding:27px 24px} .iSTCpN.hp0NoL.KEL4fK, .iSTCpN.FK1tXh.KEL4fK, .iSTCpN.LdmkLw.KEL4fK, .iSTCpN.JnzaaY.KEL4fK{background-color:rgba(0,0,0,.45)} .iSTCpN.hp0NoL.ac4Ofz, .iSTCpN.FK1tXh.ac4Ofz, .iSTCpN.LdmkLw.ac4Ofz, .iSTCpN.JnzaaY.ac4Ofz{padding:32px 24px} .O16KGI{font:inherit;color:inherit;display:block} .i6wKmL{position:relative} .i6wKmL:focus-visible:before{content:"";position:absolute;top:3px;left:3px;width:calc(100% - 6px);height:calc(100% - 6px);box-shadow:0 0 0 1px #fff,0 0 0 3px #116dff} .JMCi2v{font:inherit;color:inherit;width:100%;flex:1 1 auto;overflow:hidden} .JMCi2v.jDqjOt.mqysW5.ZBB0ua{margin-top:-19px} .JMCi2v.UUSLFD.mqysW5.ZBB0ua{margin-top:-48px} .JMCi2v.hKl06e.mqysW5.ZBB0ua, .JMCi2v.i_25UC.mqysW5.ZBB0ua, .JMCi2v.yWOr1I.mqysW5.ZBB0ua, .JMCi2v.lyd6fK.mqysW5.ZBB0ua{margin-top:-36px} .JMCi2v.x_FPRX.mqysW5.ZBB0ua{margin-top:-25px} .JMCi2v.ivAnV0, .JMCi2v.g8vcTX, .JMCi2v.TBrkhx{display:flex;flex-direction:column} .JMCi2v.ivAnV0.ZBB0ua, .JMCi2v.g8vcTX.ZBB0ua, .JMCi2v.TBrkhx.ZBB0ua{margin-top:-43px} .JMCi2v.ivAnV0.xs2MeC.ZBB0ua, .JMCi2v.g8vcTX.xs2MeC.ZBB0ua, .JMCi2v.TBrkhx.xs2MeC.ZBB0ua{margin-top:-23px} .JMCi2v.jDqjOt.ZBB0ua{position:relative;top:-29px} .JMCi2v.UUSLFD.ZBB0ua{margin-top:-29px} .JMCi2v.hKl06e.ZBB0ua, .JMCi2v.i_25UC.ZBB0ua, .JMCi2v.yWOr1I.ZBB0ua, .JMCi2v.lyd6fK.ZBB0ua{margin-top:-13px} .JMCi2v.x_FPRX.ZBB0ua{margin-top:-6px} .JMCi2v.hp0NoL, .JMCi2v.FK1tXh, .JMCi2v.JnzaaY{height:100%} .JMCi2v.hp0NoL.I5nSmk, .JMCi2v.FK1tXh.I5nSmk, .JMCi2v.JnzaaY.I5nSmk{flex-grow:0;height:unset} .JMCi2v.hKl06e{white-space:normal} .FbwBsX{margin-bottom:var(--blog-pl-default-title-margin)} .FbwBsX.hKl06e p{margin-top:var(--blog-pl-slider-title-margin-top)}@media(min-width: 660px){ .use-media-queries .FbwBsX{margin-bottom:20px} .use-media-queries .FbwBsX.G1V6el{margin-bottom:11px} .use-media-queries .FbwBsX.G1V6el.ZBB0ua{position:relative;top:-24px;margin-bottom:-14px;margin-top:-13px;max-width:209px} .use-media-queries .FbwBsX.G1V6el.ZBB0ua.CzG_Nm{max-width:189px} .use-media-queries .FbwBsX.G1V6el.xs2MeC.ZBB0ua{margin-top:12px;max-width:none} .use-media-queries .FbwBsX.G1V6el.xs2MeC.ZBB0ua.CzG_Nm{max-width:none-20px} .use-media-queries .FbwBsX.KwFkyo{margin-bottom:10px} .use-media-queries .FbwBsX.KwFkyo.ZBB0ua{position:relative;top:-24px;margin-bottom:-13px;margin-top:-13px;max-width:359px} .use-media-queries .FbwBsX.KwFkyo.ZBB0ua.CzG_Nm{max-width:339px} .use-media-queries .FbwBsX.KwFkyo.xs2MeC.ZBB0ua{margin-top:17px;max-width:none} .use-media-queries .FbwBsX.KwFkyo.xs2MeC.ZBB0ua.CzG_Nm{max-width:none-20px} .use-media-queries .FbwBsX.x_FPRX{margin-bottom:10px} .use-media-queries .FbwBsX.feQY6g{max-width:740px;margin-bottom:9px} .use-media-queries .FbwBsX.feQY6g.ZBB0ua{position:relative;top:-31px;margin-bottom:-12px;margin-top:-6px} .use-media-queries .FbwBsX.feQY6g.xs2MeC.ZBB0ua{margin-top:12px;max-width:none} .use-media-queries .FbwBsX.feQY6g.xs2MeC.ZBB0ua.CzG_Nm{max-width:none-20px} .use-media-queries .FbwBsX.jDqjOt{margin-bottom:var(--blog-pl-common-title-margin)} .use-media-queries .FbwBsX.jDqjOt.ZBB0ua{max-width:207px} .use-media-queries .FbwBsX.jDqjOt.ZBB0ua.CzG_Nm{max-width:187px} .use-media-queries .FbwBsX.jDqjOt.xs2MeC.ZBB0ua{max-width:none} .use-media-queries .FbwBsX.jDqjOt.xs2MeC.ZBB0ua.CzG_Nm{max-width:none-20px} .use-media-queries .FbwBsX.hKl06e, .use-media-queries .FbwBsX.lyd6fK, .use-media-queries .FbwBsX.i_25UC, .use-media-queries .FbwBsX.yWOr1I, .use-media-queries .FbwBsX.UUSLFD{margin-bottom:var(--blog-pl-common-title-margin)} .use-media-queries .FbwBsX.ivAnV0.ac4Ofz.ZBB0ua, .use-media-queries .FbwBsX.NAWdyL.ac4Ofz.ZBB0ua, .use-media-queries .FbwBsX.PoYsMP.ac4Ofz.ZBB0ua, .use-media-queries .FbwBsX.EfvfyL.ac4Ofz.ZBB0ua, .use-media-queries .FbwBsX.g8vcTX.ac4Ofz.ZBB0ua, .use-media-queries .FbwBsX.TBrkhx.ac4Ofz.ZBB0ua{max-width:342px} .use-media-queries .FbwBsX.ivAnV0.ac4Ofz.ZBB0ua.CzG_Nm, .use-media-queries .FbwBsX.NAWdyL.ac4Ofz.ZBB0ua.CzG_Nm, .use-media-queries .FbwBsX.PoYsMP.ac4Ofz.ZBB0ua.CzG_Nm, .use-media-queries .FbwBsX.EfvfyL.ac4Ofz.ZBB0ua.CzG_Nm, .use-media-queries .FbwBsX.g8vcTX.ac4Ofz.ZBB0ua.CzG_Nm, .use-media-queries .FbwBsX.TBrkhx.ac4Ofz.ZBB0ua.CzG_Nm{max-width:322px} .use-media-queries .FbwBsX.ivAnV0.ac4Ofz.xs2MeC.ZBB0ua, .use-media-queries .FbwBsX.NAWdyL.ac4Ofz.xs2MeC.ZBB0ua, .use-media-queries .FbwBsX.PoYsMP.ac4Ofz.xs2MeC.ZBB0ua, .use-media-queries .FbwBsX.EfvfyL.ac4Ofz.xs2MeC.ZBB0ua, .use-media-queries .FbwBsX.g8vcTX.ac4Ofz.xs2MeC.ZBB0ua, .use-media-queries .FbwBsX.TBrkhx.ac4Ofz.xs2MeC.ZBB0ua{max-width:none} .use-media-queries .FbwBsX.ivAnV0.ac4Ofz.xs2MeC.ZBB0ua.CzG_Nm, .use-media-queries .FbwBsX.NAWdyL.ac4Ofz.xs2MeC.ZBB0ua.CzG_Nm, .use-media-queries .FbwBsX.PoYsMP.ac4Ofz.xs2MeC.ZBB0ua.CzG_Nm, .use-media-queries .FbwBsX.EfvfyL.ac4Ofz.xs2MeC.ZBB0ua.CzG_Nm, .use-media-queries .FbwBsX.g8vcTX.ac4Ofz.xs2MeC.ZBB0ua.CzG_Nm, .use-media-queries .FbwBsX.TBrkhx.ac4Ofz.xs2MeC.ZBB0ua.CzG_Nm{max-width:none-20px} .use-media-queries .FbwBsX.ZpmKeC{margin-bottom:6px} .use-media-queries .FbwBsX.sCkhyN{margin-bottom:12px} .use-media-queries .FbwBsX.sCkhyN.JufO9Q{margin-bottom:30px;bottom:0} .use-media-queries .FbwBsX.hp0NoL, .use-media-queries .FbwBsX.FK1tXh, .use-media-queries .FbwBsX.LdmkLw, .use-media-queries .FbwBsX.JnzaaY{margin-bottom:18px} .use-media-queries .FbwBsX.hp0NoL.JufO9Q, .use-media-queries .FbwBsX.FK1tXh.JufO9Q, .use-media-queries .FbwBsX.LdmkLw.JufO9Q, .use-media-queries .FbwBsX.JnzaaY.JufO9Q{margin-bottom:36px;bottom:0} .use-media-queries .FbwBsX.hp0NoL.JufO9Q, .use-media-queries .FbwBsX.FK1tXh.JufO9Q, .use-media-queries .FbwBsX.JnzaaY.JufO9Q{margin-bottom:0;padding-bottom:0}} .gt-xs .FbwBsX{margin-bottom:20px} .gt-xs .FbwBsX.G1V6el{margin-bottom:11px} .gt-xs .FbwBsX.G1V6el.ZBB0ua{position:relative;top:-24px;margin-bottom:-14px;margin-top:-13px;max-width:209px} .gt-xs .FbwBsX.G1V6el.ZBB0ua.CzG_Nm{max-width:189px} .gt-xs .FbwBsX.G1V6el.xs2MeC.ZBB0ua{margin-top:12px;max-width:none} .gt-xs .FbwBsX.G1V6el.xs2MeC.ZBB0ua.CzG_Nm{max-width:none-20px} .gt-xs .FbwBsX.KwFkyo{margin-bottom:10px} .gt-xs .FbwBsX.KwFkyo.ZBB0ua{position:relative;top:-24px;margin-bottom:-13px;margin-top:-13px;max-width:359px} .gt-xs .FbwBsX.KwFkyo.ZBB0ua.CzG_Nm{max-width:339px} .gt-xs .FbwBsX.KwFkyo.xs2MeC.ZBB0ua{margin-top:17px;max-width:none} .gt-xs .FbwBsX.KwFkyo.xs2MeC.ZBB0ua.CzG_Nm{max-width:none-20px} .gt-xs .FbwBsX.x_FPRX{margin-bottom:10px} .gt-xs .FbwBsX.feQY6g{max-width:740px;margin-bottom:9px} .gt-xs .FbwBsX.feQY6g.ZBB0ua{position:relative;top:-31px;margin-bottom:-12px;margin-top:-6px} .gt-xs .FbwBsX.feQY6g.xs2MeC.ZBB0ua{margin-top:12px;max-width:none} .gt-xs .FbwBsX.feQY6g.xs2MeC.ZBB0ua.CzG_Nm{max-width:none-20px} .gt-xs .FbwBsX.jDqjOt{margin-bottom:var(--blog-pl-common-title-margin)} .gt-xs .FbwBsX.jDqjOt.ZBB0ua{max-width:207px} .gt-xs .FbwBsX.jDqjOt.ZBB0ua.CzG_Nm{max-width:187px} .gt-xs .FbwBsX.jDqjOt.xs2MeC.ZBB0ua{max-width:none} .gt-xs .FbwBsX.jDqjOt.xs2MeC.ZBB0ua.CzG_Nm{max-width:none-20px} .gt-xs .FbwBsX.hKl06e, .gt-xs .FbwBsX.lyd6fK, .gt-xs .FbwBsX.i_25UC, .gt-xs .FbwBsX.yWOr1I, .gt-xs .FbwBsX.UUSLFD{margin-bottom:var(--blog-pl-common-title-margin)} .gt-xs .FbwBsX.ivAnV0.ac4Ofz.ZBB0ua, .gt-xs .FbwBsX.NAWdyL.ac4Ofz.ZBB0ua, .gt-xs .FbwBsX.PoYsMP.ac4Ofz.ZBB0ua, .gt-xs .FbwBsX.EfvfyL.ac4Ofz.ZBB0ua, .gt-xs .FbwBsX.g8vcTX.ac4Ofz.ZBB0ua, .gt-xs .FbwBsX.TBrkhx.ac4Ofz.ZBB0ua{max-width:342px} .gt-xs .FbwBsX.ivAnV0.ac4Ofz.ZBB0ua.CzG_Nm, .gt-xs .FbwBsX.NAWdyL.ac4Ofz.ZBB0ua.CzG_Nm, .gt-xs .FbwBsX.PoYsMP.ac4Ofz.ZBB0ua.CzG_Nm, .gt-xs .FbwBsX.EfvfyL.ac4Ofz.ZBB0ua.CzG_Nm, .gt-xs .FbwBsX.g8vcTX.ac4Ofz.ZBB0ua.CzG_Nm, .gt-xs .FbwBsX.TBrkhx.ac4Ofz.ZBB0ua.CzG_Nm{max-width:322px} .gt-xs .FbwBsX.ivAnV0.ac4Ofz.xs2MeC.ZBB0ua, .gt-xs .FbwBsX.NAWdyL.ac4Ofz.xs2MeC.ZBB0ua, .gt-xs .FbwBsX.PoYsMP.ac4Ofz.xs2MeC.ZBB0ua, .gt-xs .FbwBsX.EfvfyL.ac4Ofz.xs2MeC.ZBB0ua, .gt-xs .FbwBsX.g8vcTX.ac4Ofz.xs2MeC.ZBB0ua, .gt-xs .FbwBsX.TBrkhx.ac4Ofz.xs2MeC.ZBB0ua{max-width:none} .gt-xs .FbwBsX.ivAnV0.ac4Ofz.xs2MeC.ZBB0ua.CzG_Nm, .gt-xs .FbwBsX.NAWdyL.ac4Ofz.xs2MeC.ZBB0ua.CzG_Nm, .gt-xs .FbwBsX.PoYsMP.ac4Ofz.xs2MeC.ZBB0ua.CzG_Nm, .gt-xs .FbwBsX.EfvfyL.ac4Ofz.xs2MeC.ZBB0ua.CzG_Nm, .gt-xs .FbwBsX.g8vcTX.ac4Ofz.xs2MeC.ZBB0ua.CzG_Nm, .gt-xs .FbwBsX.TBrkhx.ac4Ofz.xs2MeC.ZBB0ua.CzG_Nm{max-width:none-20px} .gt-xs .FbwBsX.ZpmKeC{margin-bottom:6px} .gt-xs .FbwBsX.sCkhyN{margin-bottom:12px} .gt-xs .FbwBsX.sCkhyN.JufO9Q{margin-bottom:30px;bottom:0} .gt-xs .FbwBsX.hp0NoL, .gt-xs .FbwBsX.FK1tXh, .gt-xs .FbwBsX.LdmkLw, .gt-xs .FbwBsX.JnzaaY{margin-bottom:18px} .gt-xs .FbwBsX.hp0NoL.JufO9Q, .gt-xs .FbwBsX.FK1tXh.JufO9Q, .gt-xs .FbwBsX.LdmkLw.JufO9Q, .gt-xs .FbwBsX.JnzaaY.JufO9Q{margin-bottom:36px;bottom:0} .gt-xs .FbwBsX.hp0NoL.JufO9Q, .gt-xs .FbwBsX.FK1tXh.JufO9Q, .gt-xs .FbwBsX.JnzaaY.JufO9Q{margin-bottom:0;padding-bottom:0} .FbwBsX.glhtJc{margin-bottom:0px} .FbwBsX.glhtJc.AkO1lZ, .FbwBsX.WdRrHJ.AkO1lZ{margin-bottom:18px} .FbwBsX.glhtJc.ySFpqx, .FbwBsX.WdRrHJ.ySFpqx{margin-bottom:0} .FbwBsX.glhtJc.ZBB0ua, .FbwBsX.WdRrHJ.ZBB0ua{margin-top:-43px;max-width:263px} .FbwBsX.glhtJc.ZBB0ua.CzG_Nm, .FbwBsX.WdRrHJ.ZBB0ua.CzG_Nm{max-width:243px} .FbwBsX.glhtJc.xs2MeC.ZBB0ua, .FbwBsX.WdRrHJ.xs2MeC.ZBB0ua{margin-top:-19px;max-width:none} .FbwBsX.glhtJc.xs2MeC.ZBB0ua.CzG_Nm, .FbwBsX.WdRrHJ.xs2MeC.ZBB0ua.CzG_Nm{max-width:none-20px} .FbwBsX.glhtJc.I5nSmk, .FbwBsX.glhtJc.xs2MeC.I5nSmk, .FbwBsX.WdRrHJ.I5nSmk, .FbwBsX.WdRrHJ.xs2MeC.I5nSmk{margin-top:0} .FbwBsX.ivAnV0, .FbwBsX.g8vcTX, .FbwBsX.TBrkhx{border-bottom:12px solid rgba(0,0,0,0);margin:0} .FbwBsX.PoYsMP, .FbwBsX.EfvfyL, .FbwBsX.NAWdyL{border-bottom:8px solid rgba(0,0,0,0);margin:0} .nebVix{padding-bottom:18px}@media(min-width: 660px){ .use-media-queries .nebVix{padding-bottom:23px}} .gt-xs .nebVix{padding-bottom:23px} .nebVix.glhtJc{margin-top:10px} .nebVix.glhtJc.JufO9Q, .nebVix.WdRrHJ.JufO9Q{padding-bottom:0} .nebVix.feQY6g{max-width:740px} .nebVix.ivAnV0, .nebVix.NAWdyL, .nebVix.PoYsMP, .nebVix.EfvfyL, .nebVix.g8vcTX, .nebVix.TBrkhx{display:flex;flex:1;padding:0;overflow:hidden} .sCkhyN{color:#fff} .sCkhyN.RiOfiW{display:inline-block;width:292px;height:292px} .sCkhyN.RiOfiW.pu51Xe{width:100%;height:100%} .sCkhyN .pccjOy{height:100%;width:100%} .sCkhyN .ITPCIq{height:100%;width:100%} .sCkhyN.JMCi2v{display:flex;flex-direction:column;justify-content:flex-end} .sCkhyN.FbwBsX{position:absolute;bottom:60px;width:calc(100% - 48px)} .sCkhyN.NPsER6{display:inline-block;position:absolute;bottom:27px} .LdmkLw.FbwBsX{position:absolute;bottom:60px;width:calc(100% - 48px)} .hp0NoL.FbwBsX, .FK1tXh.FbwBsX, .JnzaaY.FbwBsX{padding-bottom:30px} .hp0NoL.FbwBsX.p8Uk20, .FK1tXh.FbwBsX.p8Uk20, .JnzaaY.FbwBsX.p8Uk20{padding:0;margin:0} .LdmkLw, .hp0NoL, .FK1tXh, .JnzaaY{color:#fff} .LdmkLw.RiOfiW, .hp0NoL.RiOfiW, .FK1tXh.RiOfiW, .JnzaaY.RiOfiW{display:inline-block;width:454px;height:454px} .LdmkLw.RiOfiW.pu51Xe, .hp0NoL.RiOfiW.pu51Xe, .FK1tXh.RiOfiW.pu51Xe, .JnzaaY.RiOfiW.pu51Xe{width:100%;height:100%} .LdmkLw .pccjOy, .hp0NoL .pccjOy, .FK1tXh .pccjOy, .JnzaaY .pccjOy{height:100%;width:100%} .LdmkLw .ITPCIq, .hp0NoL .ITPCIq, .FK1tXh .ITPCIq, .JnzaaY .ITPCIq{height:100%;width:100%} .LdmkLw.JMCi2v, .hp0NoL.JMCi2v, .FK1tXh.JMCi2v, .JnzaaY.JMCi2v{display:flex;flex-direction:column;justify-content:flex-end} .LdmkLw.NPsER6, .hp0NoL.NPsER6, .FK1tXh.NPsER6, .JnzaaY.NPsER6{display:inline-block;position:absolute;bottom:27px} .n9F1tl{fill:#fff} .VMF9AQ{color:#fff} .BVf6py{background-color:#fff} .TE7que{height:1px;opacity:.2}@media(min-width: 660px){ .use-media-queries .TE7que{margin-bottom:12px}} .gt-xs .TE7que{margin-bottom:12px} .TE7que:not(.iD0LEx){margin-bottom:var(--blog-pl-footer-separator-margin)} .TE7que.iD0LEx{margin-top:24px} .TE7que.iD0LEx.vxAOcp{margin-top:18px} .sCkhyN .TE7que, .LdmkLw .TE7que, .hp0NoL .TE7que, .FK1tXh .TE7que, .JnzaaY .TE7que{opacity:1} .NPsER6.g8vcTX, .NPsER6.TBrkhx{margin-top:auto} .NPsER6.g8vcTX.kzwb6t, .NPsER6.TBrkhx.kzwb6t{padding-top:12px}@media(max-width: 685px){ .use-media-queries .NPsER6.g8vcTX, .use-media-queries .NPsER6.TBrkhx{padding-top:12px}} .lt-sm .NPsER6.g8vcTX, .lt-sm .NPsER6.TBrkhx{padding-top:12px} .NPsER6.ivAnV0{margin-top:auto} .NPsER6.ivAnV0.kzwb6t{padding-top:36px}@media(max-width: 685px){ .use-media-queries .NPsER6.ivAnV0{padding-top:36px}} .lt-sm .NPsER6.ivAnV0{padding-top:36px} .NPsER6.hKl06e, .NPsER6.jDqjOt, .NPsER6.lyd6fK, .NPsER6.i_25UC, .NPsER6.yWOr1I, .NPsER6.UUSLFD{padding-top:12px;margin-top:auto} .NPsER6.i_25UC.zwuMcM, .NPsER6.yWOr1I.zwuMcM, .NPsER6.lyd6fK.zwuMcM{padding-top:var(--blog-pl-footer-padding-top)} .GGI4XB{display:block;margin:15px 0 -11px} .app-desktop .GGI4XB{display:none} .NtnM8D{opacity:.6} .tFEVbz{flex:0 0 50%} .QP8OH2{width:100%;padding-top:75%;position:relative} .Gf88Ln{overflow:hidden;text-overflow:ellipsis;-webkit-box-orient:vertical;display:-webkit-box}@media(min-width: 686px){ .use-media-queries .Gf88Ln{-webkit-line-clamp:1}} .gt-s .Gf88Ln{-webkit-line-clamp:1}@media(min-width: 740px){ .use-media-queries .Gf88Ln{-webkit-line-clamp:2}} .gt-740 .Gf88Ln{-webkit-line-clamp:2}@media(min-width: 886px){ .use-media-queries .Gf88Ln{-webkit-line-clamp:3}} .gt-886 .Gf88Ln{-webkit-line-clamp:3} .so9KdE.g8vcTX, .so9KdE.TBrkhx{-webkit-mask-image:linear-gradient(to top, transparent, rgb(0, 0, 0) 20px);mask-image:linear-gradient(to top, transparent, rgb(0, 0, 0) 20px)} .hMB1wk{display:inline} .CHRJex{cursor:pointer} .l6L1Fh{height:19px} .bFiLuu{transform:translateZ(0)} .bFiLuu.post-header-icons-leave-active, .bFiLuu.post-header-icons-enter-active{transition:max-width .2s} .bFiLuu.post-header-icons-leave-active .l6L1Fh, .bFiLuu.post-header-icons-enter-active .l6L1Fh{transition:transform .2s} .bFiLuu.post-header-icons-leave, .bFiLuu.post-header-icons-enter.post-header-icons-enter-active{max-width:22px} .bFiLuu.post-header-icons-leave .l6L1Fh, .bFiLuu.post-header-icons-enter.post-header-icons-enter-active .l6L1Fh{transform:scale3d(1, 1, 0)} .bFiLuu.post-header-icons-leave.post-header-icons-leave-active, .bFiLuu.post-header-icons-enter{max-width:0} .bFiLuu.post-header-icons-leave.post-header-icons-leave-active .l6L1Fh, .bFiLuu.post-header-icons-enter .l6L1Fh{transform:scale3d(0, 0, 0)} .XqK7Ko{display:flex;align-items:center} .Bvf4SR{font-size:0;border:none;background:none;padding:0} ._EA3Ke{position:relative;display:flex;justify-content:center;align-items:center;height:24px;width:24px;cursor:pointer} .Z9ijf3{position:absolute;z-index:900;min-width:200px;max-width:300px;box-shadow:0 3px 8px 0 rgba(0,0,0,.2);margin:5px} .tmHsxH{height:1px;opacity:.2} .Dgf2Op{width:1px;height:100%;opacity:.2;flex:0 0 1px;flex-shrink:0} .Ki_0_s{width:2px;height:2px;border-radius:50%;align-self:center} .xUuoH9{display:flex;align-items:center;list-style:none;margin:0;padding:0;max-width:100%;white-space:nowrap;text-overflow:ellipsis} .rmqd9q{margin:0 8px} .F4tRtJ{min-width:0px} .MBUSKJ{font-size:inherit !important} .UZa2Xr{overflow:hidden;text-overflow:ellipsis} .ERiany{position:absolute;padding:4px 9px;background:#2f2e2e;color:#fff;z-index:1000;font-family:"Avenir Next";white-space:normal;font-size:12px;pointer-events:none} .oEw3_Y{content:"";display:block;position:absolute;border:5px solid rgba(0,0,0,0);margin:auto;top:100%;width:0;height:0;transform:rotate(180deg)} .oEw3_Y.wh3B27{border-bottom-color:#2f2e2e} .hULj4j{content:"";display:block;position:absolute;border:4px solid rgba(0,0,0,0);margin:auto;top:100%;width:0;height:0;transform:rotate(180deg)} .hULj4j.wh3B27{border-bottom-color:#2f2e2e} .s3LKPq{position:relative;display:flex;flex-direction:column;align-items:center}@media print{ .s3LKPq *{fill:#000}} .MXGxaz{position:absolute;top:-22px;line-height:1} .tQ0Q1A{display:block;white-space:nowrap;text-overflow:ellipsis;overflow:hidden;max-width:100%}@media print{ .tQ0Q1A{color:#000 !important}} .dZs5e3{display:flex;flex-direction:column;font-size:12px !important;max-width:100%} .dZs5e3.krCqqh{font-size:inherit} .dZs5e3.PZIDK7 ul{line-height:19px} .mJ89ha{font-size:inherit !important} .taLqKM{display:flex;align-items:center;height:19px;max-width:100%} .lBv2XN{display:flex;align-items:center;cursor:pointer} .SbjQym{display:flex;align-items:center;justify-content:flex-end}@media(min-width: 980px){ .use-media-queries .SbjQym{padding-bottom:12px}} .gt-sm .SbjQym{padding-bottom:12px} .SbjQym.IStc0y{padding-bottom:18px} .SbjQym._dR9OP, .SbjQym.YD6Z1G{padding-bottom:12px} .SbjQym.Nn0z1D, .SbjQym.SoK2Wc{padding-bottom:24px} .SbjQym.DKNKji, .SbjQym.rA58nq, .SbjQym.Ij__Zw, .SbjQym.jc3Fi0, .SbjQym.kAgxB9, .SbjQym.MLEYRZ, .SbjQym.AwRwmV, .SbjQym.otAa9D{padding-bottom:12px} .SbjQym.otAa9D.Fsmreb{padding-bottom:0px} .SbjQym.wjdRtO:not(.kAgxB9){padding-bottom:0;margin-bottom:5px} .SbjQym.wjdRtO:not(.kAgxB9).TjmPXo{margin-bottom:var(--blog-pl-header-margin-bottom)} .SbjQym.wjdRtO:not(.kAgxB9).TjmPXo.vL7wf0{margin-bottom:0} .SbjQym.wjdRtO:not(.kAgxB9).TjmPXo.YVAddP{margin-bottom:auto} .SbjQym.Mo4mKl{padding:0;font-size:inherit}@media(min-width: 980px){ .use-media-queries .SbjQym.Mo4mKl{padding:11px 11px 24px}} .gt-sm .SbjQym.Mo4mKl{padding:11px 11px 24px} .V_aJB6{flex:1 1 auto;display:flex;align-items:center;font-size:14px;height:var(--blog-pl-header-height);max-width:calc(100% - 42px)} .V_aJB6.CH7asw{max-width:calc(100% - 54px)} .Mo4mKl .V_aJB6{font-size:inherit}@media(min-width: 980px){ .use-media-queries .PVEWzt .V_aJB6, .use-media-queries .eOIH_4 .V_aJB6, .use-media-queries .otAa9D .V_aJB6, .use-media-queries .G1sD4r .V_aJB6{font-size:12px}} .gt-sm .PVEWzt .V_aJB6, .gt-sm .eOIH_4 .V_aJB6, .gt-sm .otAa9D .V_aJB6, .gt-sm .G1sD4r .V_aJB6{font-size:12px} .pWST8_ .V_aJB6, .vL7wf0 .V_aJB6, .rDlcKq .V_aJB6{height:auto} .YiEou4{display:none}@media(min-width: 980px){ .use-media-queries .YiEou4{display:inline-block}} .gt-sm .YiEou4{display:inline-block} .YfT_t7{opacity:.6} .Dgs_GX{display:flex;align-items:center} .GygROR{margin:0} .Q8iQIg, .dtYGur{opacity:.1;overflow-y:hidden;transform:scale3d(0.1, 0.1, 1)} .VL02DE, .EhKyap{opacity:1;transform:scale3d(1, 1, 1);transition:all .4s ease-in} .Z0hXe1{display:flex} .woUe2C .Bt5sQV{display:grid;grid-template-columns:1fr 5fr 1fr} .woUe2C .Bt5sQV .dXvq5u{flex-direction:column;grid-column:2;place-self:center;height:auto;margin:unset;min-width:0;max-width:100%} .woUe2C .Bt5sQV .dXvq5u .H_gEjP{margin-bottom:5px} .woUe2C .Bt5sQV .dXvq5u .htMcyB{align-items:center;padding:unset} .woUe2C .Bt5sQV .bhsfaV{justify-self:right;align-self:start;padding-top:5px} .woUe2C .JGhsWK{text-align:center;align-items:center} .woUe2C .HhgCcE{text-align:center} .woUe2C .qikwPI .bhsfaV{justify-self:left} .mW_sDH .Bt5sQV{display:flex;justify-content:space-between} .mW_sDH .Bt5sQV .dXvq5u{height:auto;margin:unset} .mW_sDH .Bt5sQV .dXvq5u .htMcyB{align-items:flex-end;padding:unset} .mW_sDH .Bt5sQV, .mW_sDH .dXvq5u, .mW_sDH .bhsfaV{flex-direction:row-reverse} .mW_sDH .JGhsWK{align-items:flex-end} .UbhFJ7{overflow-wrap:break-word;font-weight:inherit;word-break:break-word;margin-top:18px;font-size:22px;line-height:30px} .UbhFJ7:not(.nkqC0Q):active{opacity:.5;cursor:pointer} .UbhFJ7.oTLOCu, .UbhFJ7.ByEZyt, .UbhFJ7.LVfSYs, .UbhFJ7.QlXvtK, .UbhFJ7.tZJNum, .UbhFJ7.CUVChk, .UbhFJ7.tJjC7I, .UbhFJ7.SpEvjS, .UbhFJ7.ogvkLY, .UbhFJ7.Bo1Lly, .UbhFJ7.dY1nUm{margin-top:0}@media(min-width: 660px){ .use-media-queries .UbhFJ7{margin-top:1px;font-size:inherit;line-height:inherit} .use-media-queries .UbhFJ7.bYy0xz, .use-media-queries .UbhFJ7.ByEZyt, .use-media-queries .UbhFJ7.LVfSYs, .use-media-queries .UbhFJ7.QlXvtK, .use-media-queries .UbhFJ7.tZJNum, .use-media-queries .UbhFJ7.ogvkLY{line-height:1.4}} .gt-xs .UbhFJ7{margin-top:1px;font-size:inherit;line-height:inherit} .gt-xs .UbhFJ7.bYy0xz, .gt-xs .UbhFJ7.ByEZyt, .gt-xs .UbhFJ7.LVfSYs, .gt-xs .UbhFJ7.QlXvtK, .gt-xs .UbhFJ7.tZJNum, .gt-xs .UbhFJ7.ogvkLY{line-height:1.4} .UbhFJ7.PAKs7D, .UbhFJ7.H9iA7D{line-height:1.4 !important} .UbhFJ7.PAKs7D.uv4TNa{margin-top:12px} .UbhFJ7.rnQbhA.uv4TNa{margin-bottom:12px} .UbhFJ7.vAXVkR{margin-top:1px;font-size:inherit;line-height:inherit} .UbhFJ7.nkqC0Q{margin:0;padding:0} .UbhFJ7.EOgtUV{font-size:26px !important;line-height:32px !important}@media(min-width: 980px){ .use-media-queries .UbhFJ7.EOgtUV{font-size:40px !important;line-height:48px !important}} .gt-sm .UbhFJ7.EOgtUV{font-size:40px !important;line-height:48px !important}@media(max-width: 659px){ .use-media-queries .UbhFJ7.FG3qXk{line-height:1.2 !important}} .lt-s .UbhFJ7.FG3qXk{line-height:1.2 !important} .UbhFJ7.TkrDaM{font-size:inherit} .f01FFh{color:inherit} .t66ylj{display:inline-flex;align-items:center} .E54HWO{display:flex;align-items:center;overflow:hidden;white-space:nowrap} .lkXNhM{display:inline-flex;color:inherit;font:inherit;overflow:hidden;position:relative} .lkXNhM:focus-visible:before{content:"";position:absolute;top:3px;left:3px;width:calc(100% - 6px);height:calc(100% - 6px);box-shadow:0 0 0 1px #fff,0 0 0 3px #116dff} .P7Wc3k{position:relative} .P7Wc3k:focus-visible:before{content:"";position:absolute;top:3px;left:3px;width:calc(100% - 6px);height:calc(100% - 6px);box-shadow:0 0 0 1px #fff,0 0 0 3px #116dff} .AsLaVa{background-image:url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' width='18' height='16' viewBox='0 0 18 16'%3e %3cdefs%3e %3cstyle%3e .cls-1 %7b fill: %23f04545%3b fill-rule: evenodd%3b %7d %3c/style%3e %3c/defs%3e %3cpath class='cls-1' d='M18%2c4.988A4.926%2c4.926%2c0%2c0%2c0%2c13.139%2c0%2c4.828%2c4.828%2c0%2c0%2c0%2c9%2c2.383%2c4.828%2c4.828%2c0%2c0%2c0%2c4.86%2c0%2c4.925%2c4.925%2c0%2c0%2c0%2c0%2c4.988%2c5.025%2c5.025%2c0%2c0%2c0%2c1.668%2c8.743l6.888%2c7.068a0.616%2c0.616%2c0%2c0%2c0%2c.887%2c0l6.888-7.068A5.025%2c5.025%2c0%2c0%2c0%2c18%2c4.988Z'/%3e %3c/svg%3e")} .LtaU1R{position:relative;display:inline-block;cursor:pointer;width:19px;height:19px;vertical-align:middle} .LtaU1R svg{position:relative;display:inline-block;fill:rgba(0,0,0,0);stroke:#e84a43;stroke-width:1.5px;transition:fill 250ms} .LtaU1R ._zh4Km{display:none} .LtaU1R.rbBvhf svg{opacity:1;fill:#e84a43;stroke:none} .LtaU1R.rbBvhf ._zh4Km{display:inline-block;position:absolute;top:-3px;border-radius:100%;opacity:0;width:24px;height:24px;border:solid 3px #e84a43} .LtaU1R.rbBvhf .IiFRN3{display:inline-block;position:absolute;top:7px;opacity:0;width:4px;height:4px;border-radius:100%;background-color:#e84a43} .U1kLgW.LtaU1R.rbBvhf svg{-webkit-animation:qrbXQX 500ms;animation:qrbXQX 500ms} .U1kLgW.LtaU1R.rbBvhf ._zh4Km{-webkit-animation:kWXE2n 300ms;animation:kWXE2n 300ms} .U1kLgW.LtaU1R.rbBvhf .IiFRN3{-webkit-animation:e1MjpB 500ms;animation:e1MjpB 500ms;-webkit-animation-delay:100ms;animation-delay:100ms} .TlM_il, .h1CBon, .ab2Jus, .GKdqLq{display:inline-block;position:absolute;top:7px;width:6px;height:6px} .TlM_il{transform:rotate(0deg)} .h1CBon{transform:rotate(144deg)} .ab2Jus{transform:rotate(216deg)} .GKdqLq{transform:rotate(288deg)}@-webkit-keyframes qrbXQX{0%{transform:scale(0.1)}40%{transform:scale(1.3)}70%{transform:scale(0.9)}90%{transform:scale(1.1)}100%{transform:scale(1)}}@keyframes qrbXQX{0%{transform:scale(0.1)}40%{transform:scale(1.3)}70%{transform:scale(0.9)}90%{transform:scale(1.1)}100%{transform:scale(1)}}@-webkit-keyframes kWXE2n{0%{transform:scale(0.1);opacity:.1}20%{opacity:1}100%{transform:scale(1.5);opacity:0}}@keyframes kWXE2n{0%{transform:scale(0.1);opacity:.1}20%{opacity:1}100%{transform:scale(1.5);opacity:0}}@-webkit-keyframes e1MjpB{0%{transform:translateX(5px) scale(1);opacity:0}20%{opacity:1}100%{transform:translateX(17px) scale(0);opacity:0}}@keyframes e1MjpB{0%{transform:translateX(5px) scale(1);opacity:0}20%{opacity:1}100%{transform:translateX(17px) scale(0);opacity:0}} .Kh5HMz{border:0;background:none;padding:0;color:inherit;font:inherit;font-size:inherit !important;line-height:1} .h7K_lu{display:inline-flex;align-items:center} .p9gct4{flex-direction:row-reverse} .laz8E8{border:0 !important;clip:rect(1px, 1px, 1px, 1px) !important;-webkit-clip-path:inset(50%) !important;clip-path:inset(50%) !important;height:1px !important;overflow:hidden !important;padding:0 !important;position:absolute !important;width:1px !important;white-space:nowrap !important} .NT9zC5{display:flex;justify-content:space-between;height:var(--blog-pl-footer-content-height);line-height:1.5;font-size:12px !important} .NT9zC5.eJayAP, .NT9zC5.T6pLzn, .NT9zC5.KToN9P{justify-content:flex-start} .zGPuSQ{font-size:inherit !important} .PsEy9r{display:flex;overflow:hidden} .L2cuMO{display:flex;justify-content:flex-end;align-items:center} .j9utVS{border-radius:0;display:block;background:#fff;padding:28px 18px 25px;border:1px solid;cursor:default} .app-mobile .j9utVS{box-shadow:0 1px rgba(0,0,0,.1)}@media(min-width: 686px)and (max-width: 979px){ .use-media-queries .j9utVS{padding:40px 50px}} .sm .j9utVS{padding:40px 50px} .x1QQNM.q0le6Y{height:400px;padding-bottom:0} .Gl8eWB{margin-bottom:9px} .HZXMCr{margin-bottom:15px;height:1px;opacity:.2}@media(min-width: 686px)and (max-width: 979px){ .use-media-queries .HZXMCr{margin-bottom:12px}} .sm .HZXMCr{margin-bottom:12px} .PXvRd6{padding-bottom:23px} .H7IWI8{opacity:.8;word-break:break-word;text-overflow:ellipsis;-webkit-box-orient:vertical;display:-webkit-box;line-height:1.5;overflow:hidden}@media(min-width: 686px)and (max-width: 979px){ .use-media-queries .FAMRGA{margin:0 auto;width:940px}} .sm .FAMRGA{margin:0 auto;width:940px} .gIFXsZ>*{margin-bottom:20px} .VbmcXs{position:relative;margin-bottom:12px;width:100%;list-style-type:none} .B4NYD6{height:80px;display:flex;align-items:center} .taJz3x{margin-top:-1px} .taJz3x:last-child{display:none} .pRy83j{padding-top:135px;padding-bottom:200px;margin:0 auto}@media(min-width: 686px)and (max-width: 979px){ .use-media-queries .pRy83j{margin-bottom:20px}} .sm .pRy83j{margin-bottom:20px} .TM6znx{display:flex;align-items:center;justify-content:center;position:absolute;top:0;width:48px;height:48px;border:0;background:rgba(0,0,0,0)} .TM6znx:focus{outline:0} .nb7lGW{position:relative;display:flex;flex-direction:column;flex:1 0 auto;padding:110px 0 30px;background:#fff;border-style:solid;border-width:0}@media(min-width: 686px)and (max-width: 979px){ .use-media-queries .nb7lGW{margin:0 20px 20px;height:450px}} .sm .nb7lGW{margin:0 20px 20px;height:450px} .WD81sR{margin:35px 0 0;text-align:center;font-size:22px}@media(min-width: 686px)and (max-width: 979px){ .use-media-queries .WD81sR{font-size:32px}} .sm .WD81sR{font-size:32px} .AZOuwY{margin:8px 0 0;text-align:center;line-height:21px;font-size:14px}@media(min-width: 686px)and (max-width: 979px){ .use-media-queries .AZOuwY{margin-top:21px;font-size:16px}} .sm .AZOuwY{margin-top:21px;font-size:16px} .qdoEtQ{margin-top:40px;text-align:center} .F4xjlp{padding:18px} .app-mobile .F4xjlp{box-shadow:0 1px rgba(0,0,0,.1)} .pAfFB0{line-height:1.5} ._4BhfeI{word-break:break-word} .Rr4bcH{position:relative;width:100%;height:100%;background:rgba(0,0,0,.5)} .ISP43W{position:absolute;top:0;bottom:0;color:#fff;font-size:60px;text-indent:-9999em;overflow:hidden;width:1em;height:1em;border-radius:50%;margin:auto;transform:translateZ(0);-webkit-animation:H86O8l 1.7s infinite ease;animation:H86O8l 1.7s infinite ease} .ISP43W.qfwfIl{font-size:40px} .ISP43W.O7VfBU{font-size:20px}@-webkit-keyframes H86O8l{0%{transform:rotate(0deg);box-shadow:0 -0.83em 0 -0.4em,0 -0.83em 0 -0.42em,0 -0.83em 0 -0.44em,0 -0.83em 0 -0.46em,0 -0.83em 0 -0.477em}5%,95%{box-shadow:0 -0.83em 0 -0.4em,0 -0.83em 0 -0.42em,0 -0.83em 0 -0.44em,0 -0.83em 0 -0.46em,0 -0.83em 0 -0.477em}10%,59%{box-shadow:0 -0.83em 0 -0.4em,-0.087em -0.825em 0 -0.42em,-0.173em -0.812em 0 -0.44em,-0.256em -0.789em 0 -0.46em,-0.297em -0.775em 0 -0.477em}20%{box-shadow:0 -0.83em 0 -0.4em,-0.338em -0.758em 0 -0.42em,-0.555em -0.617em 0 -0.44em,-0.671em -0.488em 0 -0.46em,-0.749em -0.34em 0 -0.477em}38%{box-shadow:0 -0.83em 0 -0.4em,-0.377em -0.74em 0 -0.42em,-0.645em -0.522em 0 -0.44em,-0.775em -0.297em 0 -0.46em,-0.82em -0.09em 0 -0.477em}100%{transform:rotate(360deg);box-shadow:0 -0.83em 0 -0.4em,0 -0.83em 0 -0.42em,0 -0.83em 0 -0.44em,0 -0.83em 0 -0.46em,0 -0.83em 0 -0.477em}}@keyframes H86O8l{0%{transform:rotate(0deg);box-shadow:0 -0.83em 0 -0.4em,0 -0.83em 0 -0.42em,0 -0.83em 0 -0.44em,0 -0.83em 0 -0.46em,0 -0.83em 0 -0.477em}5%,95%{box-shadow:0 -0.83em 0 -0.4em,0 -0.83em 0 -0.42em,0 -0.83em 0 -0.44em,0 -0.83em 0 -0.46em,0 -0.83em 0 -0.477em}10%,59%{box-shadow:0 -0.83em 0 -0.4em,-0.087em -0.825em 0 -0.42em,-0.173em -0.812em 0 -0.44em,-0.256em -0.789em 0 -0.46em,-0.297em -0.775em 0 -0.477em}20%{box-shadow:0 -0.83em 0 -0.4em,-0.338em -0.758em 0 -0.42em,-0.555em -0.617em 0 -0.44em,-0.671em -0.488em 0 -0.46em,-0.749em -0.34em 0 -0.477em}38%{box-shadow:0 -0.83em 0 -0.4em,-0.377em -0.74em 0 -0.42em,-0.645em -0.522em 0 -0.44em,-0.775em -0.297em 0 -0.46em,-0.82em -0.09em 0 -0.477em}100%{transform:rotate(360deg);box-shadow:0 -0.83em 0 -0.4em,0 -0.83em 0 -0.42em,0 -0.83em 0 -0.44em,0 -0.83em 0 -0.46em,0 -0.83em 0 -0.477em}} .E5DWdy{position:relative;padding-bottom:56.25%;height:auto;overflow:hidden} .E5DWdy .FMKsVM{position:absolute;top:0;width:100%;height:100%;border:0} .ZkVtZC{display:flex;justify-content:center;align-items:center;position:absolute;top:0;width:100%;height:100%;background-position:center center;background-size:cover} .QdXIPy{position:absolute;top:0;width:100%;height:100%;background:rgba(0,0,0,.2)} .ul_Osp{display:flex;align-items:center;background:#000;border-radius:20px;height:40px;min-width:130px;color:#fff;cursor:pointer;opacity:.8;font-size:14px} .qCU9oG{cursor:pointer;opacity:.8}@media(min-width: 980px){ .use-media-queries .qCU9oG{width:60px;height:60px}} .gt-sm .qCU9oG{width:60px;height:60px} .QSqqb4{height:100%;position:relative} .hZSyIr{width:100%;height:100%;vertical-align:middle;cursor:pointer} .hZSyIr:active::after{content:"";width:100%;height:100%;position:absolute;top:0;background-color:rgba(0,0,0,.3);pointer-events:none} .al_Z_u .hZSyIr{background-size:cover;background-position:center;background-repeat:no-repeat} .tJFr1E .hZSyIr{min-height:70px;max-height:600px;-o-object-fit:cover;object-fit:cover} .e_MLm5{-o-object-fit:cover;object-fit:cover;width:100%;height:100%} .OjkszV{height:100%;position:relative} .XPEMCH{display:flex;justify-content:center;align-items:center;position:absolute;top:0;width:100%;height:100%;background-position:center center;background-size:cover} ._8tAyR{position:absolute;top:0;width:100%;height:100%;background:rgba(0,0,0,.2)} .Ym4kNx{display:flex;align-items:center;background:#000;border-radius:20px;height:40px;min-width:130px;color:#fff;cursor:pointer;opacity:.8;font-size:14px} .bg3nLo{cursor:pointer;opacity:.8}@media(min-width: 980px){ .use-media-queries .bg3nLo{width:60px;height:60px}} .gt-sm .bg3nLo{width:60px;height:60px} .XPEMCH:active::after{content:"";width:100%;height:100%;position:absolute;top:0;background-color:rgba(0,0,0,.3);pointer-events:none} .WEXloj{opacity:.8;word-break:break-word;overflow:hidden;text-overflow:ellipsis;-webkit-box-orient:vertical;display:-webkit-box;-webkit-line-clamp:3;height:100%} .CS4xCt{margin:0px 0 var(--blog-pl-category-label-margin-bottom);line-height:1.2;font-size:0} .pratMU{display:inline-block} .d7TwYL, .g99UQY, .u0T9F0{margin-top:4px} .TRcECH{margin-bottom:var(--blog-pl-slider-category-label-margin-bottom)} .ewnq0g, .dU7Co_, .TpXFK4, .sEWsEY{display:flex;flex-direction:column;justify-content:flex-end;align-items:flex-start;flex-grow:1}@media(max-width: 659px){ .use-media-queries .ewnq0g, .use-media-queries .dU7Co_, .use-media-queries .TpXFK4, .use-media-queries .sEWsEY{margin-bottom:0}} .lt-s .ewnq0g, .lt-s .dU7Co_, .lt-s .TpXFK4, .lt-s .sEWsEY{margin-bottom:0} .dqpczu{border-style:solid} .iT3c03.dqpczu{border-style:solid} .HLTpGi{padding:16px 18px 0;width:100%} .e7MTRm{display:flex} .e7MTRm.Eh6y5W{flex-direction:row-reverse} .uQfB_j{flex:1;min-width:0} .XHCoob{line-height:1.3em !important} .XHCoob.h2vkAZ{margin-bottom:9px} .Uyo3C3{flex:0 0 70px} .FZQqq1{height:19px} .tXgYev{margin:0;align-self:flex-end} .tXgYev>div{height:19px !important;width:19px !important} .qoUizN{width:70px;height:70px} .XHCoob{margin-top:0} ._70Aum{align-items:flex-start;margin-top:8px} .p1Z4NQ{display:flex;justify-content:flex-end;margin-top:24px} .hVP60S{display:flex;align-self:center} .dFwBxR{height:19px;display:inline-block} .YOXOuw{margin-top:12px} .YOXOuw.H0yVSw{margin-top:16px} .C5n4nu{border-radius:0;display:block;background:#fff;color:#fff;cursor:default;position:relative;width:100%;padding-top:100%;margin-bottom:12px} .xZ0Ni7{margin-bottom:auto} .R7pzO1{margin-top:0;overflow:hidden} .pqsREx, .XBPfVh, .SIFL3n{position:absolute;top:0;bottom:0} .pqsREx{padding:24px 18px;display:flex;flex-direction:column;justify-content:flex-end;max-height:100%} .wKAbJd{font:inherit;color:inherit} .QJW4X8{background-color:#fff} .fvrZpT{height:1px;margin-top:18px;margin-bottom:14px} .blog-disabled-border-color{border-color:#f0f0f0} .blog-disabled-background-color{border-color:#f0f0f0} .x-rtl-check{text-align:left} .iqqG_G{display:flex;justify-content:center;align-items:center;min-height:300px} .C9t2EF{display:flex;flex-direction:column;justify-content:center;align-items:center;max-width:490px;padding:16px} .m2ZzPh{font-size:24px !important;margin-bottom:18px;text-align:center} .XEhx62{font-size:16px !important;margin-bottom:32px;text-align:center} .mhx2YL{margin-top:20px;font-size:12px !important} .zkFmzI{position:relative;display:inline-block;height:36px;margin:0;padding:0 24px;border:0;border-style:none;cursor:pointer;vertical-align:middle;white-space:nowrap;min-width:130px;max-width:300px;font-size:14px !important} .zkFmzI:hover{opacity:.8} .zkFmzI[disabled], .zkFmzI.tLLrs1{opacity:.5}.TPASection_kdmw3930 .I5oIvV{left:0;right:0}.TPASection_kdmw3930 .UwtYxn{left:50%}.TPASection_kdmw3930 .QCPRM5{right:0;left:0}.TPASection_kdmw3930 .qi0cF2{left:50%}.TPASection_kdmw3930 .jgXCfy:active::after{left:0}.TPASection_kdmw3930 .tSamLJ{right:8px}.TPASection_kdmw3930 .HvPwlI:active::after{left:0}.TPASection_kdmw3930 .x7XlNO{right:10px}.TPASection_kdmw3930 .XNJAa3 .x7XlNO{right:auto;left:10px}.TPASection_kdmw3930 .yl6xpz{left:4px}.TPASection_kdmw3930 .ntKNd_:active:not([disabled]):not(.v7Ydcc)::after{left:0}.TPASection_kdmw3930 .KJPy_8 .JvXA2e{left:50%}.TPASection_kdmw3930 .NcXno_ .JvXA2e{right:0}.TPASection_kdmw3930 .OISqbJ{left:0;right:0}.TPASection_kdmw3930 .NcXno_ .OISqbJ{left:auto;right:16px}.TPASection_kdmw3930 .QNVRC4{margin-right:-14px}.TPASection_kdmw3930 .oOaDEN:last-child{margin-right:-3px}.TPASection_kdmw3930 .isG_om{padding-left:18px;padding-right:18px}.TPASection_kdmw3930 .IfQDSA{margin-right:-16px}.TPASection_kdmw3930 .LnLd_R li+li{margin-left:40px}.TPASection_kdmw3930 .d3XSOo{left:20px}.TPASection_kdmw3930 .d3XSOo *{float:left}.TPASection_kdmw3930 .d3XSOo *+*{margin-left:40px}.TPASection_kdmw3930 .Y0bxin{margin-left:auto;margin-right:auto}.TPASection_kdmw3930 .XcjqVD{margin-left:auto;margin-right:auto}.TPASection_kdmw3930 .kD1Yyw{margin-left:20px}.TPASection_kdmw3930 .jMrgc4{left:0}.TPASection_kdmw3930 .m2yAmK{left:0;padding-left:29px}.TPASection_kdmw3930 .ON6A2O .m2yAmK{padding-right:20px}.TPASection_kdmw3930 .RFnngL{padding-left:29px}.TPASection_kdmw3930 .ON6A2O .RFnngL{padding-right:20px}.TPASection_kdmw3930 .NMRR5Q{right:0;margin-right:-5px;margin-left:0}.TPASection_kdmw3930 .EKJcYR .dWlnoI{margin-left:24px}.TPASection_kdmw3930 .iTEFQF{margin-left:12px}.TPASection_kdmw3930 .gXfmMa{margin-right:16px}.TPASection_kdmw3930 .BpWyrA{margin-left:24px}.TPASection_kdmw3930 .SO9njw{margin-left:auto;margin-right:auto}.TPASection_kdmw3930 .nav-arrows-container svg{fill:#552415;stroke:#552415}.TPASection_kdmw3930 :not(.pA5zEx) .pCwu60 .bDY8vo,.TPASection_kdmw3930 .pA5zEx .pCwu60 .bDY8vo{margin-left:-2.5%}.TPASection_kdmw3930 :not(.pA5zEx) .r8jqp0,.TPASection_kdmw3930 .pA5zEx .r8jqp0{left:100%}.TPASection_kdmw3930 :not(.pA5zEx) .pCwu60 .r8jqp0,.TPASection_kdmw3930 .pA5zEx .pCwu60 .r8jqp0{left:0%}.TPASection_kdmw3930 .CsBZWG,.TPASection_kdmw3930 .IM8lAo{left:0}.TPASection_kdmw3930 .YSHEnd{margin-left:6px}.TPASection_kdmw3930 .r8jqp0{right:0}.TPASection_kdmw3930 .onZP9b{left:0;right:0}.TPASection_kdmw3930 .wR7lgz .RcfyI6,.TPASection_kdmw3930 .wR7lgz .ixAZGX{margin-right:48px}.TPASection_kdmw3930 .tr91VF{padding-left:18px;padding-right:18px}.TPASection_kdmw3930 .Rgs8Bb{margin-left:3px}.TPASection_kdmw3930 .Wi9Gc9>.ZKqkU8:after{margin-left:6px}.TPASection_kdmw3930 .Wi9Gc9 ul{left:-100vw}.TPASection_kdmw3930 .Wi9Gc9 ul:target{left:0}.TPASection_kdmw3930 .Wi9Gc9 ul:target~a.E_WBG4{left:0}.TPASection_kdmw3930 .HdgO4m{left:0}.TPASection_kdmw3930 *+.PRDfW5{margin-left:7px}.TPASection_kdmw3930 .JKDAPr{margin-left:-4px;margin-right:-12px}.TPASection_kdmw3930 .RiOfiW{margin-left:0;margin-right:0}.TPASection_kdmw3930 .RiOfiW.FnDyW0{direction:ltr}.TPASection_kdmw3930 .ivAnV0 .pccjOy,.TPASection_kdmw3930 .ivAnV0 .ITPCIq,.TPASection_kdmw3930 .NAWdyL .pccjOy,.TPASection_kdmw3930 .NAWdyL .ITPCIq,.TPASection_kdmw3930 .PoYsMP .pccjOy,.TPASection_kdmw3930 .PoYsMP .ITPCIq,.TPASection_kdmw3930 .EfvfyL .pccjOy,.TPASection_kdmw3930 .EfvfyL .ITPCIq,.TPASection_kdmw3930 .g8vcTX .pccjOy,.TPASection_kdmw3930 .g8vcTX .ITPCIq,.TPASection_kdmw3930 .TBrkhx .pccjOy,.TPASection_kdmw3930 .TBrkhx .ITPCIq{left:0;right:0}.TPASection_kdmw3930 .iSTCpN.sCkhyN{left:0}.TPASection_kdmw3930 .iSTCpN.LdmkLw{left:0}.TPASection_kdmw3930 .JMCi2v.pu51Xe{text-align:left}.TPASection_kdmw3930 .JMCi2v.ivAnV0.ac4Ofz,.TPASection_kdmw3930 .JMCi2v.g8vcTX.ac4Ofz,.TPASection_kdmw3930 .JMCi2v.TBrkhx.ac4Ofz{padding-right:0}.TPASection_kdmw3930 .FbwBsX.x_FPRX.ZBB0ua,.TPASection_kdmw3930 .FbwBsX.lyd6fK.ZBB0ua,.TPASection_kdmw3930 .FbwBsX.i_25UC.ZBB0ua,.TPASection_kdmw3930 .FbwBsX.yWOr1I.ZBB0ua,.TPASection_kdmw3930 .FbwBsX.NAWdyL.ZBB0ua,.TPASection_kdmw3930 .FbwBsX.PoYsMP.ZBB0ua,.TPASection_kdmw3930 .FbwBsX.EfvfyL.ZBB0ua,.TPASection_kdmw3930 .FbwBsX.TBrkhx.ZBB0ua,.TPASection_kdmw3930 .FbwBsX.g8vcTX.ZBB0ua,.TPASection_kdmw3930 .FbwBsX.hKl06e.ZBB0ua,.TPASection_kdmw3930 .FbwBsX.UUSLFD.ZBB0ua{margin-right:30px}.TPASection_kdmw3930 .FbwBsX.x_FPRX.xs2MeC.ZBB0ua,.TPASection_kdmw3930 .FbwBsX.lyd6fK.xs2MeC.ZBB0ua,.TPASection_kdmw3930 .FbwBsX.i_25UC.xs2MeC.ZBB0ua,.TPASection_kdmw3930 .FbwBsX.yWOr1I.xs2MeC.ZBB0ua,.TPASection_kdmw3930 .FbwBsX.NAWdyL.xs2MeC.ZBB0ua,.TPASection_kdmw3930 .FbwBsX.PoYsMP.xs2MeC.ZBB0ua,.TPASection_kdmw3930 .FbwBsX.EfvfyL.xs2MeC.ZBB0ua,.TPASection_kdmw3930 .FbwBsX.TBrkhx.xs2MeC.ZBB0ua,.TPASection_kdmw3930 .FbwBsX.g8vcTX.xs2MeC.ZBB0ua,.TPASection_kdmw3930 .FbwBsX.hKl06e.xs2MeC.ZBB0ua,.TPASection_kdmw3930 .FbwBsX.UUSLFD.xs2MeC.ZBB0ua{margin-right:0}@media(min-width: 660px){.TPASection_kdmw3930 .use-media-queries .FbwBsX.hKl06e.ZBB0ua,.TPASection_kdmw3930 .use-media-queries .FbwBsX.lyd6fK.ZBB0ua,.TPASection_kdmw3930 .use-media-queries .FbwBsX.i_25UC.ZBB0ua,.TPASection_kdmw3930 .use-media-queries .FbwBsX.yWOr1I.ZBB0ua,.TPASection_kdmw3930 .use-media-queries .FbwBsX.UUSLFD.ZBB0ua,.TPASection_kdmw3930 .use-media-queries .FbwBsX.NAWdyL.ZBB0ua,.TPASection_kdmw3930 .use-media-queries .FbwBsX.PoYsMP.ZBB0ua,.TPASection_kdmw3930 .use-media-queries .FbwBsX.EfvfyL.ZBB0ua,.TPASection_kdmw3930 .use-media-queries .FbwBsX.g8vcTX.ZBB0ua,.TPASection_kdmw3930 .use-media-queries .FbwBsX.TBrkhx.ZBB0ua{margin-right:30px}.TPASection_kdmw3930 .use-media-queries .FbwBsX.hKl06e.xs2MeC.ZBB0ua,.TPASection_kdmw3930 .use-media-queries .FbwBsX.lyd6fK.xs2MeC.ZBB0ua,.TPASection_kdmw3930 .use-media-queries .FbwBsX.i_25UC.xs2MeC.ZBB0ua,.TPASection_kdmw3930 .use-media-queries .FbwBsX.yWOr1I.xs2MeC.ZBB0ua,.TPASection_kdmw3930 .use-media-queries .FbwBsX.UUSLFD.xs2MeC.ZBB0ua,.TPASection_kdmw3930 .use-media-queries .FbwBsX.NAWdyL.xs2MeC.ZBB0ua,.TPASection_kdmw3930 .use-media-queries .FbwBsX.PoYsMP.xs2MeC.ZBB0ua,.TPASection_kdmw3930 .use-media-queries .FbwBsX.EfvfyL.xs2MeC.ZBB0ua,.TPASection_kdmw3930 .use-media-queries .FbwBsX.g8vcTX.xs2MeC.ZBB0ua,.TPASection_kdmw3930 .use-media-queries .FbwBsX.TBrkhx.xs2MeC.ZBB0ua{margin-right:0}}.TPASection_kdmw3930 .gt-xs .FbwBsX.hKl06e.ZBB0ua,.TPASection_kdmw3930 .gt-xs .FbwBsX.lyd6fK.ZBB0ua,.TPASection_kdmw3930 .gt-xs .FbwBsX.i_25UC.ZBB0ua,.TPASection_kdmw3930 .gt-xs .FbwBsX.yWOr1I.ZBB0ua,.TPASection_kdmw3930 .gt-xs .FbwBsX.UUSLFD.ZBB0ua,.TPASection_kdmw3930 .gt-xs .FbwBsX.NAWdyL.ZBB0ua,.TPASection_kdmw3930 .gt-xs .FbwBsX.PoYsMP.ZBB0ua,.TPASection_kdmw3930 .gt-xs .FbwBsX.EfvfyL.ZBB0ua,.TPASection_kdmw3930 .gt-xs .FbwBsX.g8vcTX.ZBB0ua,.TPASection_kdmw3930 .gt-xs .FbwBsX.TBrkhx.ZBB0ua{margin-right:30px}.TPASection_kdmw3930 .gt-xs .FbwBsX.hKl06e.xs2MeC.ZBB0ua,.TPASection_kdmw3930 .gt-xs .FbwBsX.lyd6fK.xs2MeC.ZBB0ua,.TPASection_kdmw3930 .gt-xs .FbwBsX.i_25UC.xs2MeC.ZBB0ua,.TPASection_kdmw3930 .gt-xs .FbwBsX.yWOr1I.xs2MeC.ZBB0ua,.TPASection_kdmw3930 .gt-xs .FbwBsX.UUSLFD.xs2MeC.ZBB0ua,.TPASection_kdmw3930 .gt-xs .FbwBsX.NAWdyL.xs2MeC.ZBB0ua,.TPASection_kdmw3930 .gt-xs .FbwBsX.PoYsMP.xs2MeC.ZBB0ua,.TPASection_kdmw3930 .gt-xs .FbwBsX.EfvfyL.xs2MeC.ZBB0ua,.TPASection_kdmw3930 .gt-xs .FbwBsX.g8vcTX.xs2MeC.ZBB0ua,.TPASection_kdmw3930 .gt-xs .FbwBsX.TBrkhx.xs2MeC.ZBB0ua{margin-right:0}.TPASection_kdmw3930 .sCkhyN.NPsER6{left:24px;right:24px}.TPASection_kdmw3930 .LdmkLw.NPsER6,.TPASection_kdmw3930 .hp0NoL.NPsER6,.TPASection_kdmw3930 .FK1tXh.NPsER6,.TPASection_kdmw3930 .JnzaaY.NPsER6{left:24px;right:24px}.TPASection_kdmw3930 .XqK7Ko>*+*{margin-left:18px}.TPASection_kdmw3930 .oEw3_Y{right:0;left:0}.TPASection_kdmw3930 .hULj4j{right:0;left:0}.TPASection_kdmw3930 .dZs5e3{padding-left:10px}.TPASection_kdmw3930 .dZs5e3:first-child{padding-left:0px}@media(min-width: 980px){.TPASection_kdmw3930 .use-media-queries .mtJpOF .dZs5e3{padding-left:18px}}.TPASection_kdmw3930 .gt-sm .mtJpOF .dZs5e3{padding-left:18px}.TPASection_kdmw3930 .dlINDG{padding-left:12px}.TPASection_kdmw3930 .dlINDG:first-child{padding-left:0px}.TPASection_kdmw3930 .V_aJB6{margin-right:auto}@media(min-width: 980px){.TPASection_kdmw3930 .use-media-queries .YiEou4{margin-left:auto}}.TPASection_kdmw3930 .gt-sm .YiEou4{margin-left:auto}.TPASection_kdmw3930 .Dgs_GX{margin-right:3px}.TPASection_kdmw3930 .YiEou4+.Dgs_GX{margin-left:12px}.TPASection_kdmw3930 .GygROR{margin-right:-9px}.TPASection_kdmw3930 .GygROR.pWST8_,.TPASection_kdmw3930 .GygROR.rDlcKq{margin-right:-18px}@media(min-width: 980px){.TPASection_kdmw3930 .use-media-queries .Mo4mKl .GygROR{margin-right:0}}.TPASection_kdmw3930 .gt-sm .Mo4mKl .GygROR{margin-right:0}.TPASection_kdmw3930 .mW_sDH .Bt5sQV .dXvq5u .H_gEjP{margin-left:10px}.TPASection_kdmw3930 .mW_sDH .JGhsWK{text-align:right}.TPASection_kdmw3930 .mW_sDH .HhgCcE{text-align:right}@media(min-width: 660px){.TPASection_kdmw3930 .use-media-queries .UbhFJ7.TkrDaM{padding-left:11px;padding-right:11px}}.TPASection_kdmw3930 .gt-xs .UbhFJ7.TkrDaM{padding-left:11px;padding-right:11px}.TPASection_kdmw3930 .t66ylj{padding-right:2px}.TPASection_kdmw3930 .eYQJQu:not(:first-child){margin-left:6px}.TPASection_kdmw3930 .E54HWO>*+*{margin-left:16px}.TPASection_kdmw3930 .LtaU1R.rbBvhf ._zh4Km{left:-3px}.TPASection_kdmw3930 .LtaU1R.rbBvhf .IiFRN3{left:8px}.TPASection_kdmw3930 .TlM_il,.TPASection_kdmw3930 .h1CBon,.TPASection_kdmw3930 .ab2Jus,.TPASection_kdmw3930 .GKdqLq{left:8px}.TPASection_kdmw3930 .FYRNvd{padding-right:5px}.TPASection_kdmw3930 .p9gct4 .FYRNvd{padding-right:0;padding-left:5px}.TPASection_kdmw3930 .PsEy9r{margin-right:16px}.TPASection_kdmw3930 .j9utVS{margin-left:0;margin-right:0}@media(min-width: 686px)and (max-width: 979px){.TPASection_kdmw3930 .use-media-queries .PXvRd6,.TPASection_kdmw3930 .use-media-queries .HZXMCr,.TPASection_kdmw3930 .use-media-queries .xrwGqD{margin-left:11px;margin-right:11px}}.TPASection_kdmw3930 .sm .PXvRd6,.TPASection_kdmw3930 .sm .HZXMCr,.TPASection_kdmw3930 .sm .xrwGqD{margin-left:11px;margin-right:11px}.TPASection_kdmw3930 .B4NYD6{padding-left:80px}.TPASection_kdmw3930 .TM6znx{right:0}.TPASection_kdmw3930 .AZOuwY{padding-left:34px;padding-right:34px}.TPASection_kdmw3930 .ISP43W{right:0;left:0}.TPASection_kdmw3930 .E5DWdy .FMKsVM{left:0}.TPASection_kdmw3930 .ZkVtZC{left:0}.TPASection_kdmw3930 .QdXIPy{left:0}.TPASection_kdmw3930 .ul_Osp{padding-right:18px}.TPASection_kdmw3930 .BbU38r{margin-left:7px;margin-right:2px}.TPASection_kdmw3930 .hZSyIr:active::after{left:0}.TPASection_kdmw3930 .XPEMCH{left:0}.TPASection_kdmw3930 ._8tAyR{left:0}.TPASection_kdmw3930 .Ym4kNx{padding-right:18px}.TPASection_kdmw3930 .wM0E5k{margin-left:7px;margin-right:2px}.TPASection_kdmw3930 .XPEMCH:active::after{left:0}.TPASection_kdmw3930 .dqpczu{font:normal normal bold 14px/1.4em barlow-extralight,barlow,sans-serif;text-decoration: ;color:#FFFFFF;background-color:#095B96;border-color:#095B96;border-width:0px;border-radius:0px;padding:6px 12px}.TPASection_kdmw3930 .dqpczu:hover{color:#FFFFFF;background-color:#095B96;border-color:#095B96}.TPASection_kdmw3930 .SkWvPq{font:normal normal bold 14px/1.4em barlow-extralight,barlow,sans-serif;text-decoration: ;color:#095B96}.TPASection_kdmw3930 .SkWvPq:hover{color:#095B96}.TPASection_kdmw3930 .iT3c03.dqpczu{font:normal normal bold 14px/1.4em barlow-extralight,barlow,sans-serif;text-decoration: ;color:#FFFFFF;background-color:#095B96;border-color:#095B96;border-width:0px;border-radius:0px;padding:6px 12px}.TPASection_kdmw3930 .iT3c03.SkWvPq{font:normal normal bold 14px/1.4em barlow-extralight,barlow,sans-serif;text-decoration: ;color:#095B96}.TPASection_kdmw3930 .Uyo3C3{margin-left:16px}.TPASection_kdmw3930 .Uyo3C3.mACGUD{margin-right:16px;margin-left:0px}.TPASection_kdmw3930 .JBIDX5{margin-left:auto}.TPASection_kdmw3930 .tXgYev{margin-left:auto}.TPASection_kdmw3930 .hVP60S{margin-right:auto}.TPASection_kdmw3930 .dFwBxR{margin-right:12px}.TPASection_kdmw3930 .THUnVE{margin-right:4px}.TPASection_kdmw3930 .C5n4nu{margin-left:0;margin-right:0}.TPASection_kdmw3930 .pqsREx,.TPASection_kdmw3930 .XBPfVh,.TPASection_kdmw3930 .SIFL3n{left:0;right:0}.TPASection_kdmw3930 .blog-navigation-container-font{font:normal normal bold 18px/1.4em barlow-extralight,barlow,sans-serif;text-decoration: ;line-height:normal}.TPASection_kdmw3930 .blog-header-widget-font{font:normal normal bold 18px/1.4em barlow-extralight,barlow,sans-serif;text-decoration: ;line-height:normal}.TPASection_kdmw3930 .blog-desktop-header-search-font{font:normal normal bold 18px/1.4em barlow-extralight,barlow,sans-serif;text-decoration: ;line-height:normal}.TPASection_kdmw3930 .blog-page-description-title-font{font:normal normal bold 32px/1.4em avenir-lt-w01_35-light1475496,avenir-lt-w05_35-light,sans-serif;text-decoration: ;line-height:normal}.TPASection_kdmw3930 .blog-page-description-content-font{font:normal normal bold 16px/1.4em barlow-extralight,barlow,sans-serif;text-decoration: ;line-height:normal}.TPASection_kdmw3930 .blog-page-description-title-header300-font{font:normal normal bold 40px/1.2 avenir-lt-w01_35-light1475496,avenir-lt-w05_35-light,sans-serif;text-decoration: ;line-height:1.2}.TPASection_kdmw3930 .blog-page-description-content-header300-font{font:normal normal bold 18px/1.5 barlow-extralight,barlow,sans-serif;text-decoration: ;line-height:1.5}.TPASection_kdmw3930 .blog-post-title-font{font:normal normal bold 28px/1.4em avenir-lt-w01_35-light1475496,avenir-lt-w05_35-light,sans-serif;text-decoration: ;line-height:normal}.TPASection_kdmw3930 .blog-post-description-style-font{font:normal normal bold 16px/1.4em barlow-extralight,barlow,sans-serif;text-decoration: ;line-height:normal}.TPASection_kdmw3930 .blog-post-description-font{font:normal normal bold 16px/1.4em barlow-extralight,barlow,sans-serif;text-decoration: ;line-height:normal;font-style:inherit;font-weight:inherit;-webkit-text-decoration:inherit;text-decoration:inherit}.TPASection_kdmw3930 .blog-post-page-font{font:normal normal bold 18px/1.4em barlow-extralight,barlow,sans-serif;text-decoration: ;line-height:normal;font-style:inherit;font-weight:inherit;-webkit-text-decoration:inherit;text-decoration:inherit}.TPASection_kdmw3930 .blog-post-page-title-font{font:normal normal bold 40px/1.4em avenir-lt-w01_35-light1475496,avenir-lt-w05_35-light,sans-serif;text-decoration: ;line-height:normal}.TPASection_kdmw3930 .blog-post-quote-font{font:normal normal bold 16px/1.4em barlow-extralight,barlow,sans-serif;text-decoration: ;line-height:normal}.TPASection_kdmw3930 .blog-post-header-two-font{font:undefined;text-decoration: ;line-height:normal}.TPASection_kdmw3930 .blog-post-header-three-font{font:undefined;text-decoration: ;line-height:normal}.TPASection_kdmw3930 .blog-post-homepage-title-font{font:normal normal bold 22px/27px avenir-lt-w01_35-light1475496,avenir-lt-w05_35-light,sans-serif;text-decoration: }.TPASection_kdmw3930 .blog-post-homepage-description-style-font{font:normal normal bold 16px/20px barlow-extralight,barlow,sans-serif;text-decoration: ;line-height:normal}.TPASection_kdmw3930 .blog-post-homepage-description-font{font:normal normal bold 16px/20px barlow-extralight,barlow,sans-serif;text-decoration: ;line-height:normal;font-style:inherit;font-weight:inherit;-webkit-text-decoration:inherit;text-decoration:inherit}.TPASection_kdmw3930 .blog-post-homepage-post-container .blog-post-text-font{font:normal normal bold 16px/20px barlow-extralight,barlow,sans-serif;text-decoration: ;line-height:normal}.TPASection_kdmw3930 .blog-post-category-title-font{font:normal normal bold 28px/1.4em avenir-lt-w01_35-light1475496,avenir-lt-w05_35-light,sans-serif;text-decoration: }.TPASection_kdmw3930 .blog-post-category-description-style-font{font:normal normal bold 16px/1.4em barlow-extralight,barlow,sans-serif;text-decoration: ;line-height:normal}.TPASection_kdmw3930 .blog-post-category-description-font{font:normal normal bold 16px/1.4em barlow-extralight,barlow,sans-serif;text-decoration: ;line-height:normal;font-style:inherit;font-weight:inherit;-webkit-text-decoration:inherit;text-decoration:inherit}.TPASection_kdmw3930 .blog-post-category-post-container .blog-post-text-font{font:normal normal bold 30px/1.4em avenir-lt-w01_35-light1475496,avenir-lt-w05_35-light,sans-serif;text-decoration: ;line-height:normal}.TPASection_kdmw3930 .blog-widget-sign-up-button-font{font:normal normal bold 18px/1.4em barlow-extralight,barlow,sans-serif;text-decoration: ;line-height:normal}.TPASection_kdmw3930 .blog-primary-color{color:#095B96}.TPASection_kdmw3930 .blog-primary-background-color{background-color:#095B96}.TPASection_kdmw3930 .blog-primary-border-color{border-color:#095B96}.TPASection_kdmw3930 .blog-primary-border-bottom-color{border-bottom-color:#095B96}.TPASection_kdmw3930 .blog-primary-border-left-color{border-left-color:#095B96}.TPASection_kdmw3930 .blog-primary-fill{fill:#095B96}.TPASection_kdmw3930 .blog-secondary-fill{fill:#FF6D40}.TPASection_kdmw3930 .blog-header-border-color{border-color:#141414}.TPASection_kdmw3930 .blog-navigation-container-color{color:rgb(20, 20, 20)}.TPASection_kdmw3930 .blog-navigation-container-fill{fill:rgb(20, 20, 20)}.TPASection_kdmw3930 .blog-navigation-container-border-color{border-color:rgb(20, 20, 20)}.TPASection_kdmw3930 .blog-navigation-container-active-color{color:#095B96}.TPASection_kdmw3930 .blog-navigation-container-active-fill{fill:#095B96}.TPASection_kdmw3930 .blog-navigation-container-separator-background-color{background-color:rgb(20, 20, 20)}.TPASection_kdmw3930 .blog-navigation-link-hover-color:hover{color:rgb(89, 89, 89)}.TPASection_kdmw3930 .blog-navigation-link-hover-fill:hover{fill:rgb(89, 89, 89)}.TPASection_kdmw3930 .blog-navigation-link-active-color{color:rgb(89, 89, 89)}.TPASection_kdmw3930 .blog-navigation-link-active-fill{fill:rgb(89, 89, 89)}.TPASection_kdmw3930 .blog-desktop-header-background-color{background-color:#FFFFFF}.TPASection_kdmw3930 .blog-navigation-container-background-color{background-color:rgba(255, 255, 255, 0)}.TPASection_kdmw3930 .blog-header-background-color{background-color:rgba(255, 255, 255, 0)}.TPASection_kdmw3930 .blog-categories-dropdown-background-color{background-color:rgba(255, 255, 255, 0.9)}.TPASection_kdmw3930 .blog-header-widget-text-color{color:rgb(20, 20, 20)}.TPASection_kdmw3930 .blog-header-notification-bubble-background-color{background-color:#FF6D40}.TPASection_kdmw3930 .blog-header-notification-bubble-text-color{color:#FFFFFF}.TPASection_kdmw3930 .blog-header-widget-icon-fill{fill:rgb(20, 20, 20)}.TPASection_kdmw3930 .blog-desktop-header-search-text-color{color:rgb(20, 20, 20)}.TPASection_kdmw3930 .blog-desktop-header-search-icon-fill{fill:rgb(20, 20, 20)}.TPASection_kdmw3930 .blog-desktop-header-search-border-background-color{background-color:rgb(20, 20, 20)}.TPASection_kdmw3930 .blog-text-color{color:#141414}.TPASection_kdmw3930 .blog-text-border-color{border-color:#141414}.TPASection_kdmw3930 .blog-text-background-color{background-color:#141414}.TPASection_kdmw3930 .blog-text-after-background-color::after{background-color:#141414}.TPASection_kdmw3930 .blog-separator-background-color{background-color:#141414}.TPASection_kdmw3930 .blog-icon-fill{fill:#141414}.TPASection_kdmw3930 .blog-icon-stroke{stroke:#141414}.TPASection_kdmw3930 .blog-background-color{background-color:rgba(255, 255, 255, 0)}.TPASection_kdmw3930 .blog-card-background-color{background-color:#FFFFFF}.TPASection_kdmw3930 .blog-dropdown-background-color{background-color:rgb(255, 255, 255)}.TPASection_kdmw3930 .blog-card-fill{fill:#FFFFFF}.TPASection_kdmw3930 .blog-card-stroke{stroke:#FFFFFF}.TPASection_kdmw3930 .blog-card-color{color:#FFFFFF}.TPASection_kdmw3930 .blog-inverted-separator-background-color{background-color:#FFFFFF}.TPASection_kdmw3930 .blog-profile-photo-border-color{border-color:#FFFFFF}.TPASection_kdmw3930 .blog-card-border-color{border-color:#F7F7F7}.TPASection_kdmw3930 .blog-card-border-bottom-color{border-bottom-color:#F7F7F7}.TPASection_kdmw3930 .blog-post-title-color{color:#141414}.TPASection_kdmw3930 .blog-post-quote-color{color:}.TPASection_kdmw3930 .blog-post-header-two-color{color:}.TPASection_kdmw3930 .blog-post-header-three-color{color:}.TPASection_kdmw3930 .blog-post-homepage-title-color{color:rgb(20, 20, 20)}.TPASection_kdmw3930 .blog-post-homepage-TOI-title-color{color:rgb(255, 255, 255)}.TPASection_kdmw3930 .blog-post-homepage-description-color{color:rgb(20, 20, 20)}.TPASection_kdmw3930 .blog-post-homepage-description-fill{fill:rgb(20, 20, 20)}.TPASection_kdmw3930 .blog-post-homepage-description-background-color{background-color:rgb(20, 20, 20)}.TPASection_kdmw3930 .blog-post-homepage-TOI-description-color{color:rgb(255, 255, 255)}.TPASection_kdmw3930 .blog-post-homepage-TOI-description-fill{fill:rgb(255, 255, 255)}.TPASection_kdmw3930 .blog-post-homepage-TOI-description-background-color{background-color:rgb(255, 255, 255)}.TPASection_kdmw3930 .blog-post-homepage-border-color{border-color:rgb(20, 20, 20)}.TPASection_kdmw3930 .blog-post-homepage-overlay-background-color{background-color:rgba(0, 0, 0, 0.35)}.TPASection_kdmw3930 .is-desktop .blog-post-homepage-link-hashtag-hover-color:hover{color:rgb(89, 89, 89)}.TPASection_kdmw3930 .is-desktop .blog-post-homepage-link-hashtag-hover-fill:hover{fill:rgb(89, 89, 89)}.TPASection_kdmw3930 .is-desktop .blog-post-homepage-link-hashtag-hover-color:hover .blog-post-homepage-title-color{color:rgb(89, 89, 89)}.TPASection_kdmw3930 .is-desktop .blog-post-homepage-link-hashtag-hover-color:hover .blog-post-homepage-TOI-title-color{color:rgb(89, 89, 89)}.TPASection_kdmw3930 .is-desktop .blog-post-category-link-hashtag-hover-color:hover .blog-post-category-title-color{color:rgb(9, 91, 150)}.TPASection_kdmw3930 .is-desktop .blog-post-category-link-hashtag-hover-color:hover .blog-post-category-TOI-title-color{color:rgb(9, 91, 150)}.TPASection_kdmw3930 .blog-post-homepage-background-color{background-color:rgb(255, 255, 255)}.TPASection_kdmw3930 .blog-post-homepage-TOI-background-color{background-color:rgb(20, 20, 20)}.TPASection_kdmw3930 .is-desktop .blog-post-homepage-hover-container:hover .blog-hover-container-element-color{color:rgb(89, 89, 89)}.TPASection_kdmw3930 .is-desktop .blog-post-homepage-hover-container:hover .blog-hover-container-element-fill{fill:rgb(89, 89, 89)}.TPASection_kdmw3930 .blog-post-homepage-post-container .blog-link-hashtag-color{color:rgb(89, 89, 89)}.TPASection_kdmw3930 .blog-post-homepage-post-container .blog-post-text-color{color:rgb(20, 20, 20)}.TPASection_kdmw3930 .blog-post-homepage-post-container .blog-icon-fill{fill:rgb(20, 20, 20)}.TPASection_kdmw3930 .is-desktop .blog-post-homepage-post-container .blog-link-hover-color:hover{color:rgb(89, 89, 89)}.TPASection_kdmw3930 .is-desktop .blog-post-homepage-post-container .blog-link-hover-fill:hover{fill:rgb(89, 89, 89)}.TPASection_kdmw3930 .blog-post-category-title-color{color:rgb(20, 20, 20)}.TPASection_kdmw3930 .blog-post-category-TOI-title-color{color:#FFFFFF}.TPASection_kdmw3930 .blog-post-category-description-color{color:rgb(20, 20, 20)}.TPASection_kdmw3930 .blog-post-category-description-fill{fill:rgb(20, 20, 20)}.TPASection_kdmw3930 .blog-post-category-description-background-color{background-color:rgb(20, 20, 20)}.TPASection_kdmw3930 .blog-post-category-TOI-description-color{color:#FFFFFF}.TPASection_kdmw3930 .blog-post-category-TOI-description-fill{fill:#FFFFFF}.TPASection_kdmw3930 .blog-post-category-TOI-description-background-color{background-color:#FFFFFF}.TPASection_kdmw3930 .blog-post-category-border-color{border-color:rgb(247, 247, 247)}.TPASection_kdmw3930 .blog-post-category-overlay-background-color{background-color:rgba(0, 0, 0, 0.55)}.TPASection_kdmw3930 .is-desktop .blog-post-category-link-hashtag-hover-color:hover{color:rgb(9, 91, 150)}.TPASection_kdmw3930 .is-desktop .blog-post-category-link-hashtag-hover-fill:hover{fill:rgb(9, 91, 150)}.TPASection_kdmw3930 .blog-post-category-background-color{background-color:rgb(255, 255, 255)}.TPASection_kdmw3930 .blog-post-category-TOI-background-color{background-color:rgb(20, 20, 20)}.TPASection_kdmw3930 .is-desktop .blog-post-category-hover-container:hover .blog-hover-container-element-color{color:rgb(9, 91, 150)}.TPASection_kdmw3930 .is-desktop .blog-post-category-hover-container:hover .blog-hover-container-element-fill{fill:rgb(9, 91, 150)}.TPASection_kdmw3930 .blog-post-category-post-container .blog-link-hashtag-color{color:rgb(9, 91, 150)}.TPASection_kdmw3930 .blog-post-category-post-container .blog-post-text-color{color:rgb(20, 20, 20)}.TPASection_kdmw3930 .blog-post-category-post-container .blog-icon-fill{fill:rgb(20, 20, 20)}.TPASection_kdmw3930 .is-desktop .blog-post-category-post-container .blog-link-hover-color:hover{color:rgb(9, 91, 150)}.TPASection_kdmw3930 .is-desktop .blog-post-category-post-container .blog-link-hover-fill:hover{fill:rgb(9, 91, 150)}.TPASection_kdmw3930 .blog-link-hashtag-color{color:#095B96}.TPASection_kdmw3930 .blog-button-primary-text-color{color:rgb(255, 255, 255)}.TPASection_kdmw3930 .blog-button-primary-text-background-color{background-color:rgb(255, 255, 255)}.TPASection_kdmw3930 .blog-button-primary-text-border-color{border-color:rgb(255, 255, 255)}.TPASection_kdmw3930 .blog-button-primary-icon-fill{fill:rgb(255, 255, 255)}.TPASection_kdmw3930 .blog-button-primary-icon-stroke{stroke:rgb(255, 255, 255)}.TPASection_kdmw3930 .blog-button-secondary-text-color{color:rgb(20, 20, 20)}.TPASection_kdmw3930 .blog-button-secondary-text-hover-color:hover{color:rgb(20, 20, 20)}.TPASection_kdmw3930 .blog-button-secondary-icon-fill{fill:#095B96}.TPASection_kdmw3930 .blog-button-color{color:rgb(20, 20, 20)}.TPASection_kdmw3930 .blog-button-background-color{background-color:rgb(20, 20, 20)}.TPASection_kdmw3930 .blog-button-border-color{border-color:rgb(20, 20, 20)}.TPASection_kdmw3930 .blog-button-border-bottom-color{border-bottom-color:rgb(20, 20, 20)}.TPASection_kdmw3930 .blog-button-fill{fill:rgb(20, 20, 20)}.TPASection_kdmw3930 .blog-button-stroke{stroke:rgb(20, 20, 20)}.TPASection_kdmw3930 .blog-button-hover-color:hover{color:rgb(20, 20, 20)}.TPASection_kdmw3930 .is-desktop .blog-tab-hover-color:hover{color:rgb(20, 20, 20)}.TPASection_kdmw3930 .is-desktop .blog-button-hover-color:hover{color:#095B96}.TPASection_kdmw3930 .is-desktop .blog-button-hover-fill:hover{fill:#095B96}.TPASection_kdmw3930 .is-desktop .blog-link-hover-color:hover{color:#095B96}.TPASection_kdmw3930 .is-desktop .blog-link-hover-fill:hover{fill:#095B96}.TPASection_kdmw3930 .is-desktop .blog-link-color{color:#095B96}.TPASection_kdmw3930 .is-desktop .blog-link-fill{fill:#095B96}.TPASection_kdmw3930 .is-desktop .blog-link-background-color{background-color:#095B96}.TPASection_kdmw3930 .blog-quote-border-color{border-color:#095B96}.TPASection_kdmw3930 .is-desktop .blog-hover-container:hover .blog-hover-container-element-color{color:#095B96}.TPASection_kdmw3930 .is-desktop .blog-hover-container:hover .blog-hover-container-element-fill{fill:#095B96}.TPASection_kdmw3930 .is-desktop .blog-button-hover-after-border-color:hover::after{border-color:rgb(20, 20, 20)}.TPASection_kdmw3930 .is-desktop .blog-button-after-border-color::after{border-color:rgb(20, 20, 20)}.TPASection_kdmw3930 .blog-icon-secondary-fill{fill:#FF6D40}.TPASection_kdmw3930 .blog-icon-secondary-background-color{background-color:#FF6D40}.TPASection_kdmw3930 .blog-border-color{border-color:rgba(20, 20, 20, 0.2)}.TPASection_kdmw3930 .blog-placeholder-background-color{background-color:rgba(20, 20, 20, 0.2)}.TPASection_kdmw3930 .blog-border-color-focused{border-color:rgba(20, 20, 20, 0.5)}.TPASection_kdmw3930 .blog-placeholder-text-color::-moz-placeholder{color:#141414}.TPASection_kdmw3930 .blog-placeholder-text-color:-ms-input-placeholder{color:#141414}.TPASection_kdmw3930 .blog-placeholder-text-color::placeholder{color:#141414}.TPASection_kdmw3930 .blog-card-background-border-bottom-color{border-bottom-color:#FFFFFF}.TPASection_kdmw3930 .blog-card-border-border-bottom-color{border-bottom-color:rgba(20, 20, 20, 0.2)}.TPASection_kdmw3930 .blog-button-border-bottom-color{border-bottom-color:rgb(20, 20, 20)}.TPASection_kdmw3930 .blog-text-background-color{background-color:#141414}.TPASection_kdmw3930 .blog-cover-image-button-background-color{background-color:rgba(20, 20, 20, 0.8)}.TPASection_kdmw3930 .blog-cover-image-button-hover-background-color{background-color:#141414}.TPASection_kdmw3930 .blog-navigation-divider-border-color{border-color:rgba(20, 20, 20, 0.2)}.TPASection_kdmw3930 .blog-widget-sign-up-button-background-color{background-color:#095B96}.TPASection_kdmw3930 .blog-widget-sign-up-button-border-color{border-color:#095B96}.TPASection_kdmw3930 .blog-widget-sign-up-button-primary-color{color:#FFFFFF}.TPASection_kdmw3930 .blog-widget-sign-up-button-secondary-color{color:#095B96}.TPASection_kdmw3930 .blog-widget-notification-icon-fill{fill:#141414}.TPASection_kdmw3930 .blog-widget-notification-bubble-background-color{background-color:#141414}.TPASection_kdmw3930 .blog-widget-notification-bubble-text-color{color:#FFFFFF}.TPASection_kdmw3930 .blog-widget-avatar-border-color{border-color:#FFFFFF}.TPASection_kdmw3930 .wc-comments-root{--wcButtonColor: rgb(20, 20, 20);--wcTextColor: #141414;--wcTextSecondaryColor: #141414;--wcBorderColor: #F7F7F7;--wcSeparatorColor: rgba(0, 0, 0, 0.2);--wcBackgroundColor: #FFFFFF;--wcContentFont: normal normal bold 18px/1.4em barlow-extralight,barlow,sans-serif;--wcTitleFont: normal normal bold 28px/1.4em avenir-lt-w01_35-light1475496,avenir-lt-w05_35-light,sans-serif}.TPASection_kdmw3930 .app-desktop .md1nXG.SwMATA .YUJc6d{margin-right:calc((100% - 740px) / 2 + 60px);margin-left:calc((100% - 740px)/2)}@media(max-width: 900px){.TPASection_kdmw3930 .use-media-queries .app-desktop .md1nXG.SwMATA .YUJc6d{margin-left:0;margin-right:0}}.TPASection_kdmw3930 .lte-post_content_area_small .app-desktop .md1nXG.SwMATA .YUJc6d{margin-left:0;margin-right:0}.TPASection_kdmw3930 .md1nXG.SwMATA .UyDASD:not(.sxFoib) .x2pFxX{margin-left:-18px;margin-right:0}.TPASection_kdmw3930 .app-desktop .md1nXG.SwMATA .SD5Soh{margin-right:calc((100% - 740px) / 2 + 60px);margin-left:calc((100% - 740px)/2)}@media(max-width: 900px){.TPASection_kdmw3930 .use-media-queries .app-desktop .md1nXG.SwMATA .SD5Soh{margin-left:0;margin-right:0}}.TPASection_kdmw3930 .lte-post_content_area_small .app-desktop .md1nXG.SwMATA .SD5Soh{margin-left:0;margin-right:0}.TPASection_kdmw3930 .app-desktop .md1nXG.SwMATA .zUX7a_:not(.afvS5b){margin-left:-100px;margin-right:0}@media(max-width: 980px){.TPASection_kdmw3930 .use-media-queries .app-desktop .md1nXG.SwMATA .zUX7a_:not(.afvS5b){margin-left:calc((var(--root-width) - 780px)/-2);margin-right:0}}.TPASection_kdmw3930 .lte-w980 .app-desktop .md1nXG.SwMATA .zUX7a_:not(.afvS5b){margin-left:calc((var(--root-width) - 780px)/-2);margin-right:0}@media(max-width: 900px){.TPASection_kdmw3930 .use-media-queries .app-desktop .md1nXG.SwMATA .zUX7a_:not(.afvS5b){margin-left:-60px;margin-right:0}}.TPASection_kdmw3930 .lte-post_content_area_small .app-desktop .md1nXG.SwMATA .zUX7a_:not(.afvS5b){margin-left:-60px;margin-right:0}@media(max-width: 746px){.TPASection_kdmw3930 .use-media-queries .app-desktop .md1nXG.SwMATA .zUX7a_:not(.afvS5b){margin-left:calc((var(--root-width) - 626px)/-2);margin-right:0}}.TPASection_kdmw3930 .lte-post_content_area_min .app-desktop .md1nXG.SwMATA .zUX7a_:not(.afvS5b){margin-left:calc((var(--root-width) - 626px)/-2);margin-right:0}.TPASection_kdmw3930 .app-desktop .IJMkBO.de33a7 .jbsx5L{margin-right:calc((100% - 740px) / 2 + 60px);margin-left:calc((100% - 740px)/2)}@media(max-width: 900px){.TPASection_kdmw3930 .app-desktop .IJMkBO.de33a7 .jbsx5L{margin-left:0;margin-right:0}}.TPASection_kdmw3930 .IJMkBO.de33a7 .C40mix:not(.nYG_Vf) .tTWUXT{margin-left:-18px;margin-right:0}.TPASection_kdmw3930 ._9o1QTX.nNs5jL .IJMkBO.de33a7 .GKZEtj{margin-right:calc((100% - 740px) / 2 + 60px);margin-left:calc((100% - 740px)/2)}@media(max-width: 900px){.TPASection_kdmw3930 ._9o1QTX.nNs5jL .IJMkBO.de33a7 .GKZEtj{margin-left:0;margin-right:0}}.TPASection_kdmw3930 .app-desktop .IJMkBO.de33a7 .bpYAy9:not(.FXeyle){margin-left:-100px;margin-right:0}@media(max-width: 980px){.TPASection_kdmw3930 .app-desktop .IJMkBO.de33a7 .bpYAy9:not(.FXeyle){margin-left:calc((100vw - 780px)/-2);margin-right:0}}@media(max-width: 900px){.TPASection_kdmw3930 .app-desktop .IJMkBO.de33a7 .bpYAy9:not(.FXeyle){margin-left:-60px;margin-right:0}}@media(max-width: 746px){.TPASection_kdmw3930 .app-desktop .IJMkBO.de33a7 .bpYAy9:not(.FXeyle){margin-left:calc((100vw - 626px)/-2);margin-right:0}}.TPASection_kdmw3930 .NHVyAh{left:50%}.TPASection_kdmw3930 .yq9v_n+.yq9v_n{margin-left:36px}@media(min-width: 660px){.TPASection_kdmw3930 .use-media-queries .yq9v_n+.yq9v_n{margin-left:30px}}.TPASection_kdmw3930 .gt-xs .yq9v_n+.yq9v_n{margin-left:30px}.TPASection_kdmw3930 .bVF0DD>*+*{margin-left:6px}.TPASection_kdmw3930 .asGom3>*+*{margin-left:22px}.TPASection_kdmw3930 .ejEA1m>*+*{margin-left:14px}.TPASection_kdmw3930 .UPa2xo{margin-left:auto}.TPASection_kdmw3930 .u5Zxn8{margin-right:-10px;margin-left:8px}.TPASection_kdmw3930 .cD_92h.nD_ScK.UitnHM{padding-right:24px}.TPASection_kdmw3930 .lKoXz3{font:normal normal bold 14px/1.4em barlow-extralight,barlow,sans-serif;text-decoration: }.TPASection_kdmw3930 .qIZJEj{color:#141414}.TPASection_kdmw3930 .qIZJEj:hover{color:#141414}.TPASection_kdmw3930 .zkjxsb{font:normal normal bold 14px/1.4em barlow-extralight,barlow,sans-serif;text-decoration: }.TPASection_kdmw3930 .NUPGYU{color:#141414}.TPASection_kdmw3930 .NUPGYU:hover{color:#141414}.TPASection_kdmw3930 .FKW5xK{background-color:#FFFFFF}.TPASection_kdmw3930 .FKW5xK:hover{background-color:#FFFFFF}.TPASection_kdmw3930 .H_6Rhn{border-color:#141414}.TPASection_kdmw3930 .H_6Rhn:hover{border-color:#F7F7F7}.TPASection_kdmw3930 .Q4OKcc{padding:6px 12px}.TPASection_kdmw3930 .PYiOy1{border-width:1px;border-radius:0px}.TPASection_kdmw3930 .c2QKT1.lKoXz3{font:normal normal bold 14px/1.4em barlow-extralight,barlow,sans-serif;text-decoration: }.TPASection_kdmw3930 .c2QKT1.qIZJEj{color:#141414}.TPASection_kdmw3930 .c2QKT1.qIZJEj:hover{color:#095B96}.TPASection_kdmw3930 .txj3Mv.zkjxsb{font:normal normal bold 14px/1.4em barlow-extralight,barlow,sans-serif;text-decoration: }.TPASection_kdmw3930 .txj3Mv.NUPGYU{color:#141414}.TPASection_kdmw3930 .txj3Mv.NUPGYU:hover{color:#141414}.TPASection_kdmw3930 .txj3Mv.FKW5xK{background-color:#FFFFFF}.TPASection_kdmw3930 .txj3Mv.FKW5xK:hover{background-color:#FFFFFF}.TPASection_kdmw3930 .txj3Mv.H_6Rhn{border-color:#141414}.TPASection_kdmw3930 .txj3Mv.H_6Rhn:hover{border-color:#141414}.TPASection_kdmw3930 .txj3Mv.PYiOy1{border-width:1px;border-radius:0px}.TPASection_kdmw3930 .bgatvV.Q4OKcc{padding:6px 12px}.TPASection_kdmw3930 .yezHed.qDaU7T{color:#141414;background-color:#FFFFFF;font:normal normal bold 18px/1.4em barlow-extralight,barlow,sans-serif;text-decoration: }.TPASection_kdmw3930 .yezHed.O3nool{color:#141414;font:normal normal bold 18px/1.4em barlow-extralight,barlow,sans-serif;text-decoration: }.TPASection_kdmw3930 .post-list-pro-gallery-slider{border-width:1px;border-color:#F7F7F7}.TPASection_kdmw3930 .post-list-pro-gallery-slider .gallery-slideshow .pro-gallery .gallery-slideshow-info{padding-left:0 !important;padding-right:0 !important}.TPASection_kdmw3930 .mobile-slider-border-styles .post-list-pro-gallery-slider{border-width:1px;border-color:#F7F7F7}.TPASection_kdmw3930 .mobile-slider-border-styles .post-list-pro-gallery-slider .gallery-slideshow .pro-gallery .gallery-slideshow-info{padding-left:0 !important;padding-right:0 !important}.TPASection_kdmw3930 .dvQCZJ{left:0;right:0}.TPASection_kdmw3930 .fpyHxy{left:0;right:0}.TPASection_kdmw3930 .ULLWFz{right:19px}.TPASection_kdmw3930 .TzKMlS{right:0;left:0}.TPASection_kdmw3930 .QrY79E{margin-right:24px}@media(min-width: 686px){.TPASection_kdmw3930 .use-media-queries .QrY79E{margin-right:38px}}.TPASection_kdmw3930 .gt-s .QrY79E{margin-right:38px}.TPASection_kdmw3930 .QrY79E:last-of-type{margin-right:0}.TPASection_kdmw3930 .TkqYD3{margin-right:8px}.TPASection_kdmw3930 .VUmEbT ul{padding-left:18px}.TPASection_kdmw3930 .aVOLKV{margin-left:10px}@media(min-width: 686px){.TPASection_kdmw3930 .use-media-queries .aVOLKV{margin-left:12px}}.TPASection_kdmw3930 .gt-s .aVOLKV{margin-left:12px}.TPASection_kdmw3930 .hWag3q{margin-left:12px}.TPASection_kdmw3930 .njVPpi{margin-left:12px} #TPASection_kdmw3930 { height:auto }#pageBackground_bcs2h { --bg-position:absolute;--bg-overlay-color:rgb(var(--color_12));--bg-gradient:none;--fill-layer-background-overlay-color:transparent;--fill-layer-background-overlay-position:absolute }
    </style>
    <style id="css_daa1p">
        [data-mesh-id=comp-kdoefztninlineContent]{height:auto;width:100%;position:static;min-height:650px}[data-mesh-id=comp-kdoefztpinlineContent]{height:auto;width:100%;position:static;min-height:650px}[data-mesh-id=comp-kdo9wepcinlineContent]{height:auto;width:100%}[data-mesh-id=comp-kdo9wepcinlineContent-gridContainer]{position:static;display:grid;height:auto;width:100%;min-height:400px;grid-template-rows:repeat(2, min-content) 1fr;grid-template-columns:100%}[data-mesh-id=comp-kdo9wepcinlineContent-gridContainer] > [id="comp-kdo9wepf2"]{position:relative;margin:69px 0px 29px calc((100% - 441px) * 0.5);left:11px;grid-area:1 / 1 / 2 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdo9wepcinlineContent-gridContainer] > [id="comp-kdo9wepj1"]{position:relative;margin:0px 0px 27px calc((100% - 441px) * 0.5);left:11px;grid-area:2 / 1 / 3 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdo9wepcinlineContent-gridContainer] > [id="comp-kdo9wepn"]{position:relative;margin:0px 0px 10px calc((100% - 441px) * 0.5);left:11px;grid-area:3 / 1 / 4 / 2;justify-self:start;align-self:start}[data-mesh-id=comp-kdo9wepr1inlineContent]{height:auto;width:100%;position:static;min-height:400px}[data-mesh-id=comp-kdoaxd7yinlineContent]{height:auto;width:100%;position:static;min-height:80px}[data-mesh-id=Containerdaa1pinlineContent]{height:auto;width:100%}[data-mesh-id=Containerdaa1pinlineContent-gridContainer]{position:static;display:grid;height:auto;width:100%;min-height:500px;grid-template-rows:repeat(2, min-content) 1fr;grid-template-columns:100%;padding-bottom:0px;box-sizing:border-box}[data-mesh-id=Containerdaa1pinlineContent-gridContainer] > [id="comp-kdoefzsr"]{position:relative;margin:0px 0px 68px calc((100% - 980px) * 0.5);left:0px;grid-area:1 / 1 / 2 / 2;justify-self:start;align-self:start}[data-mesh-id=Containerdaa1pinlineContent-gridContainer] > [id="comp-kdo9wenq"]{position:relative;margin:0px 0px 10px calc((100% - 980px) * 0.5);left:0px;grid-area:1 / 1 / 2 / 2;justify-self:start;align-self:start}[data-mesh-id=Containerdaa1pinlineContent-gridContainer] > [id="comp-kdo9ycue"]{position:relative;margin:0px 80px 0 80px;left:0;grid-area:2 / 1 / 3 / 2;justify-self:stretch;align-self:start}[data-mesh-id=Containerdaa1pinlineContent-gridContainer] > [id="comp-kdoaxd5y"]{position:relative;margin:0px 0px 0 calc((100% - 980px) * 0.5);left:0px;grid-area:3 / 1 / 4 / 2;justify-self:start;align-self:start}#comp-kdo9ycue{left:0;margin-left:80px;width:calc(100% - 80px - 80px);min-width:initial;height:1183px}#comp-kdo9wepf2{width:420px;height:84px}#comp-kdo9wepj1{width:94px;height:18px}#comp-kdo9wepn{width:414px;height:56px}#daa1p{left:0;margin-left:0;width:100%;min-width:100%}#comp-kdoefzsr{margin-left:auto;margin-right:auto;width:calc(100% - 80px - 80px);min-width:980px}#comp-kdo9wenq{margin-left:auto;margin-right:auto;width:calc(100% - 80px - 80px);min-width:980px}#comp-kdoaxd5y{left:0;margin-left:0;width:100%;min-width:100%}#comp-kdoefztn{width:294px}#comp-kdoefztp{width:686px}#comp-kdo9wepc{width:441px}#comp-kdo9wepr1{width:539px}#comp-kdoaxd7y{width:980px}#masterPage{--pinned-layers-in-page:0}
        @font-face {
            font-display: block;
            font-family: "Barlow-ExtraLight";
            src: url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/51v0xj5VPw1cLYHNhfd8NFtXRa8TVwTICgirnJhmVJw.woff2") format("woff2"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/51v0xj5VPw1cLYHNhfd8ND8E0i7KZn-EPnyo3HZu7kw.woff") format("woff"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/51v0xj5VPw1cLYHNhfd8NKCWcynf_cDxXwCLxiixG1c.ttf") format("truetype"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/barlow-extralight-Svg.svg") format("svg");
        }
        @font-face {
            font-display: block;
            font-family: "Barlow-ExtraLight";
            font-weight: 700;
            src: url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/2woyxyDnPU0v4IiqYU9D1g.woff2") format("woff2"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/-HJgNsTwx9qXGSxqew62RQ.woff") format("woff"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/qoExc9IJQUjYXhlVZNNLgg.ttf") format("truetype"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/barlow-extralight-700-Svg.svg") format("svg");
        }
        @font-face {
            font-display: block;
            font-family: "Barlow-ExtraLight";
            font-style: italic;
            src: url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/14AxwKgJhKIO-YYUP_KtZag5eI2G47JWe0-AuFtD150.woff2") format("woff2"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/14AxwKgJhKIO-YYUP_KtZdIh4imgI8P11RFo6YPCPC0.woff") format("woff"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/14AxwKgJhKIO-YYUP_KtZV02b4v3fUxqf9CZJ1qUoIA.ttf") format("truetype"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/barlow-extralight-italic-Svg.svg") format("svg");
        }
        @font-face {
            font-display: block;
            font-family: "Barlow-ExtraLight";
            font-weight: 700;
            font-style: italic;
            src: url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/cdbGxfKO8gdkBd5U5TuXqPesZW2xOQ-xsNqO47m55DA.woff2") format("woff2"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/W1XpMGU0WrpbCawEdG1FM_esZW2xOQ-xsNqO47m55DA.woff") format("woff"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/_lIpJP17FZmSeklpAeOdnvesZW2xOQ-xsNqO47m55DA.ttf") format("truetype"),
            url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow-extralight/v1/barlow-extralight-700-italic-Svg.svg") format("svg");
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: italic;
            font-weight: 400;
            src: local('Barlow Italic'), local('Barlow-Italic'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHrv4kjgoGqM7E_Cfs0wH8DnzcjTrA.woff2") format('woff2');
            unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: italic;
            font-weight: 400;
            src: local('Barlow Italic'), local('Barlow-Italic'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHrv4kjgoGqM7E_Cfs1wH8DnzcjTrA.woff2") format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: italic;
            font-weight: 400;
            src: local('Barlow Italic'), local('Barlow-Italic'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHrv4kjgoGqM7E_Cfs7wH8Dnzcj.woff2") format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: italic;
            font-weight: 700;
            src: local('Barlow Bold Italic'), local('Barlow-BoldItalic'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHsv4kjgoGqM7E_CfOA5WohvToJdLm8BvE.woff2") format('woff2');
            unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: italic;
            font-weight: 700;
            src: local('Barlow Bold Italic'), local('Barlow-BoldItalic'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHsv4kjgoGqM7E_CfOA5WogvToJdLm8BvE.woff2") format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: italic;
            font-weight: 700;
            src: local('Barlow Bold Italic'), local('Barlow-BoldItalic'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHsv4kjgoGqM7E_CfOA5WouvToJdLm8.woff2") format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: normal;
            font-weight: 400;
            src: local('Barlow Regular'), local('Barlow-Regular'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHpv4kjgoGqM7E_A8s5ynghnQci.woff2") format('woff2');
            unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: normal;
            font-weight: 400;
            src: local('Barlow Regular'), local('Barlow-Regular'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHpv4kjgoGqM7E_Ass5ynghnQci.woff2") format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: normal;
            font-weight: 400;
            src: local('Barlow Regular'), local('Barlow-Regular'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHpv4kjgoGqM7E_DMs5ynghnQ.woff2") format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: normal;
            font-weight: 700;
            src: local('Barlow Bold'), local('Barlow-Bold'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHqv4kjgoGqM7E3t-4s6Fostz0rdom9.woff2") format('woff2');
            unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: normal;
            font-weight: 700;
            src: local('Barlow Bold'), local('Barlow-Bold'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHqv4kjgoGqM7E3t-4s6Vostz0rdom9.woff2") format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        @font-face {
            font-display: block;
            font-family: 'Barlow';
            font-style: normal;
            font-weight: 700;
            src: local('Barlow Bold'), local('Barlow-Bold'), url("//static.parastorage.com/services/santa-resources/dist/viewer/user-site-fonts/fonts/Barlow/v4/7cHqv4kjgoGqM7E3t-4s51ostz0rdg.woff2") format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }
        @font-face {
            font-display: block;
            font-family:"Avenir-LT-W01_85-Heavy1475544";
            src:url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/6af9989e-235b-4c75-8c08-a83bdaef3f66.eot?#iefix");
            src:url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/6af9989e-235b-4c75-8c08-a83bdaef3f66.eot?#iefix") format("eot"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/d513e15e-8f35-4129-ad05-481815e52625.woff2") format("woff2"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/61bd362e-7162-46bd-b67e-28f366c4afbe.woff") format("woff"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/ccd17c6b-e7ed-4b73-b0d2-76712a4ef46b.ttf") format("truetype"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/20577853-40a7-4ada-a3fb-dd6e9392f401.svg#20577853-40a7-4ada-a3fb-dd6e9392f401") format("svg");
        }
        @font-face {
            font-family: "Avenir-LT-W05_85-Heavy";
            src: url("//static.parastorage.com/services/santa-resources/resources/viewer/user-site-fonts/fonts/Avenir_Family_Pack/v1/AvenirLTW05-85Heavy.woff2") format("woff2"),
            url("//static.parastorage.com/services/santa-resources/resources/viewer/user-site-fonts/fonts/Avenir_Family_Pack/v1/AvenirLTW05-85Heavy.woff") format("woff");
        }
        @font-face {
            font-display: block;
            font-family:"Avenir-LT-W01_35-Light1475496";
            src:url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/edefe737-dc78-4aa3-ad03-3c6f908330ed.eot?#iefix");
            src:url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/edefe737-dc78-4aa3-ad03-3c6f908330ed.eot?#iefix") format("eot"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/0078f486-8e52-42c0-ad81-3c8d3d43f48e.woff2") format("woff2"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/908c4810-64db-4b46-bb8e-823eb41f68c0.woff") format("woff"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/4577388c-510f-4366-addb-8b663bcc762a.ttf") format("truetype"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/b0268c31-e450-4159-bfea-e0d20e2b5c0c.svg#b0268c31-e450-4159-bfea-e0d20e2b5c0c") format("svg");
        }
        @font-face {
            font-family: "Avenir-LT-W05_35-Light";
            src: url("//static.parastorage.com/services/santa-resources/resources/viewer/user-site-fonts/fonts/Avenir_Family_Pack/v1/AvenirLTW05-35Light.woff2") format("woff2"),
            url("//static.parastorage.com/services/santa-resources/resources/viewer/user-site-fonts/fonts/Avenir_Family_Pack/v1/AvenirLTW05-35Light.woff") format("woff");
        }
        @font-face {
            font-display: block;
            font-family: "Helvetica-W01-Light";
            src: url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/717f8140-20c9-4892-9815-38b48f14ce2b.eot?#iefix");
            src: url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/717f8140-20c9-4892-9815-38b48f14ce2b.eot?#iefix") format("eot"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/03805817-4611-4dbc-8c65-0f73031c3973.woff") format("woff"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/d5f9f72d-afb7-4c57-8348-b4bdac42edbb.ttf") format("truetype"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/05ad458f-263b-413f-b054-6001a987ff3e.svg#05ad458f-263b-413f-b054-6001a987ff3e") format("svg");
        }
        @font-face {
            font-display: block;
            font-family: "Helvetica-W02-Light";
            src: url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/ff80873b-6ac3-44f7-b029-1b4111beac76.eot?#iefix");
            src: url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/ff80873b-6ac3-44f7-b029-1b4111beac76.eot?#iefix") format("eot"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/80c34ad2-27c2-4d99-90fa-985fd64ab81a.woff") format("woff"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/b8cb02c2-5b58-48d8-9501-8d02869154c2.ttf") format("truetype"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/92c941ea-2b06-4b72-9165-17476d424d6c.svg#92c941ea-2b06-4b72-9165-17476d424d6c") format("svg");
        }
        @font-face {
            font-display: block;
            font-family: "Helvetica-W01-Bold";
            src: url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/f70da45a-a05c-490c-ad62-7db4894b012a.eot?#iefix");
            src: url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/f70da45a-a05c-490c-ad62-7db4894b012a.eot?#iefix") format("eot"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/c5749443-93da-4592-b794-42f28d62ef72.woff") format("woff"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/73805f15-38e4-4fb7-8a08-d56bf29b483b.ttf") format("truetype"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/874bbc4a-0091-49f0-93ef-ea4e69c3cc7a.svg#874bbc4a-0091-49f0-93ef-ea4e69c3cc7a") format("svg");
        }
        @font-face {
            font-display: block;
            font-family: "Helvetica-W02-Bold";
            src: url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/8c0d8b0f-d7d6-4a72-a418-c2373e4cbf27.eot?#iefix");
            src: url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/8c0d8b0f-d7d6-4a72-a418-c2373e4cbf27.eot?#iefix") format("eot"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/192dac76-a6d9-413d-bb74-22308f2e0cc5.woff") format("woff"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/47584448-98c4-436c-89b9-8d6fbeb2a776.ttf") format("truetype"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/375c70e5-6822-492b-8408-7cd350440af7.svg#375c70e5-6822-492b-8408-7cd350440af7") format("svg");
        }
        @font-face {
            font-display: block;
            font-family: "Helvetica-LT-W10-Bold";
            src: url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/9fe262dc-5a55-4d75-91a4-aed76bd32190.eot?#iefix");
            src: url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/9fe262dc-5a55-4d75-91a4-aed76bd32190.eot?#iefix") format("eot"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/0a3939d0-3833-4db3-8b85-f64c2b3350d2.woff") format("woff"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/1b128d6d-126f-4c9c-8f87-3e7d30a1671c.ttf") format("truetype"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/b791c850-fde1-48b3-adf0-8998d55b0866.svg#b791c850-fde1-48b3-adf0-8998d55b0866") format("svg");
        }
        @font-face {
            font-display: block;
            font-family:"Proxima-N-W01-Reg";
            src:url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/7e90123f-e4a7-4689-b41f-6bcfe331c00a.eot?#iefix");
            src:url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/7e90123f-e4a7-4689-b41f-6bcfe331c00a.eot?#iefix") format("eot"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/64017d81-9430-4cba-8219-8f5cc28b923e.woff2") format("woff2"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/e56ecb6d-da41-4bd9-982d-2d295bec9ab0.woff") format("woff"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/2aff4f81-3e97-4a83-9e6c-45e33c024796.ttf") format("truetype"),url("//static.parastorage.com/services/third-party/fonts/user-site-fonts/fonts/ab9cd062-380f-4b53-b1a7-c0bec7402235.svg#ab9cd062-380f-4b53-b1a7-c0bec7402235") format("svg");
        }
        @font-face{
            font-family:"Proxima-N-W05-Reg";
            src: url("//static.parastorage.com/services/santa-resources/resources/viewer/user-site-fonts/fonts/ProximNova/v1/ProximaNovaW05-Regular.woff2") format("woff2"),
            url("//static.parastorage.com/services/santa-resources/resources/viewer/user-site-fonts/fonts/ProximNova/v1/ProximaNovaW05-Regular.woff") format("woff");
        }

        #comp-kdo9wepf2 { height:auto }#comp-kdo9wepj1 { --opacity:1 }#comp-kdo9wepn { height:auto }#daa1p { width:auto;min-height:500px }#comp-kdoefzsr { --bg-overlay-color:transparent;--bg-gradient:none;--padding:0px;--margin:0px;min-width:100%;--firstChildMarginTop:-1px;--lastChildMarginBottom:-1px }#comp-kdo9wenq { --param_boolean_previewHover:false;--bg-overlay-color:transparent;--bg-gradient:none;--padding:0px;--margin:0px;min-width:100%;--firstChildMarginTop:-1px;--lastChildMarginBottom:-1px }#comp-kdoaxd5y { --bg-overlay-color:transparent;--bg-gradient:none;--padding:0px;--margin:0px;min-width:100%;--firstChildMarginTop:-1px;--lastChildMarginBottom:-1px }#comp-kdoefztn { --bg-overlay-color:rgb(var(--color_12));--bg-gradient:none;width:100%;--column-width:294px;--column-flex:294 }#comp-kdoefztp { --bg-overlay-color:rgb(var(--color_13));--bg-gradient:none;--fill-layer-image-opacity:1;width:100%;--column-width:686px;--column-flex:686 }#comp-kdo9wepc { --bg-overlay-color:rgb(var(--color_12));--bg-gradient:none;width:100%;--column-width:441px;--column-flex:441 }#comp-kdo9wepr1 { --bg-overlay-color:transparent;--bg-gradient:none;width:100%;--column-width:539px;--column-flex:539 }#comp-kdoaxd7y { --bg-overlay-color:rgb(var(--color_12));--bg-gradient:none;width:100%;--column-width:980px;--column-flex:980 }#comp-kdo9ycue { height:auto }#pageBackground_daa1p { --bg-position:absolute;--bg-overlay-color:rgb(var(--color_12));--bg-gradient:none;--fill-layer-background-overlay-color:transparent;--fill-layer-background-overlay-position:absolute }
    </style>

    <style>
        #comp-kdmrh8vz {
            visibility: inherit !important;
        }
        #SITE_HEADER_WRAPPER{
            margin-top: -35px;
            background: #f7f7f7;
        }
        #comp-kdmrh8vz {
            width: 637px;
            height: 50px;
            margin-top: 60px !important;
        }
        #comp-kyfn4iuj {
            width: 106px;
            height: 72px;
            margin-top: 48px !important;
            margin-bottom: 25px !important;
        }
        ._3KF65 ._1u8sp {
            padding: 0var(--pad,5px);
            width: 0% !important;
        }
        #comp-kdmrh8vz {
            width: 90% !important;
            height: 50px;
            margin-top: 60px !important;
            margin-left: auto;
        }
        [data-mesh-id=comp-kdoiv97iinlineContent-gridContainer] > [id="comp-kdoiv99l1"] {
            position: relative;
            margin: 70px 0px 2px calc((100% - 168px) * 1) !important;
            left: 8px;
            grid-area: 1 / 1 / 2 / 2;
            justify-self: start;
            align-self: start;
        }
        @media (max-width:1350px) {
            [data-mesh-id=SITE_FOOTERinlineContent-gridContainer] > [id="comp-kdmvot8b"] {
                position: relative;
                margin: 0px;
                left: 0px;
                grid-area: 2 / 1 / 3 / 2;
                justify-self: start;
                align-self: start;
            }
            #comp-kdmvot8b {
                width: 100%;
                height: 25px;
            }
            #gallery-wrapper-comp-kdmt6lj8 {
                overflow: scroll !important;
                width: 100%;
            }
            #pro-gallery-comp-kdmt6lj8 {
                width: 130%;
            }
        }
        @media (max-width:1250px) {
            #pro-gallery-comp-kdmt6lj8 {
                width: 150%;
            }
        }
        @media (max-width:1150px) {
            #pro-gallery-comp-kdmt6lj8 {
                width: 200%;
            }
        }
        @media (max-width:991px){
            #pro-gallery-comp-kdmt6lj8 {
                width: 250%;
            }
            #MENU_AS_CONTAINER_TOGGLE {
                --rd: 0px;
                --shd: none;
                --bg: var(--color_11);
                --borderwidth: 0px;
                --bordercolor: var(--color_15);
                --alpha-bordercolor: 1;
                --rdOpen: 0px;
                --shdOpen: none;
                --bgOpen: var(--color_11);
                --borderwidthOpen: 0px;
                --bordercolorOpen: var(--color_15);
                --alpha-bordercolorOpen: 1;
                --lineColor: var(--color_15);
                --alpha-lineColor: 1;
                --lineColorOpen: var(--color_15);
                --alpha-lineColorOpen: 1;
                --boxShadowToggleOn-shd: none;
                --boxShadowToggleOn-shdOpen: none;
                --alpha-bg: 0;
                --alpha-bgOpen: 0;
            }
            #MENU_AS_CONTAINER_TOGGLE {
                display:block !important;
                width: 50px;
                height: 50px;
                position: absolute;
                margin: 0px 0px 10px 0;
                right:15px;
                top:153px;
                grid-area: 2 / 1 / 3 / 2;
                justify-self: start;
                align-self: start;
            }
            ._1XkJp {
                position: relative;
                width: 22px;
                height: 20px;
                z-index:9999;
                margin: auto;
            }
            ._9P1US, .q8uXp {
                bottom: 0;
            }
            ._9P1US {
                top: 0;
                margin: auto;
            }
            ._1j6Hs, ._1j6Hs._3F3MB {
                display: flex;
                justify-content: center;
                align-items: center;
                width: 100%;
                height: 100%;
                box-sizing: border-box;
            }
            ._1j6Hs {
                transition: all .5s;
                border-radius: var(--rd,0);
                box-shadow: var(--shd,0 0 0 rgba(0,0,0,.6));
                background-color: rgba(var(--bg,var(--color_11)),var(--alpha-bg,1));
                border: solid var(--borderwidth,0) rgba(var(--bordercolor,var(--color_11)),var(--alpha-bordercolor,1));
            }
            ._2Z3R- {
                background-color: rgba(var(--lineColor,var(--color_11)),var(--alpha-lineColor,1));
                height: 2px;
                width: 100%;
                border-radius: 60px;
                position: absolute;
                left: 0;
                transition: all .25s ease-in-out;
            }
            [data-mesh-id=comp-kdmsihb7inlineContent-gridContainer] {
                position: static;
                display: flex !important;
                height: auto;
                width: 100%;
                min-height: auto;
                grid-template-rows: 1fr;
                grid-template-columns: 100%;
                justify-content: center;
            }
            #comp-kdoiv97i {
                display:none;
            }
            #comp-kdmsir58{
                display:none;
            }
            #comp-kdmswlbk{
                position: relative;
                margin: 0px;
                margin-top:140px;
                grid-area: 1 / 1 / 2 / 2;
                left: unset !important;
                justify-self: start;
                align-self: start;
                width: 100% !important;
                text-align: center;
            }
            #comp-kdmt06ch {
                width:100%;
                min-width:100%;
            }
            [data-mesh-id=comp-kdmt06diinlineContent-gridContainer] > [id="comp-kdmt5r6h"] {
                position: relative;
                left: 41px;
                grid-area: 2 / 1 / 3 / 2;
                justify-self: start;
                align-self: start;
                MARGIN-TOP: 25PX;
            }
            #comp-kdmt39ab{
                position: relative;
                margin:0px;
                margin-top: 58px;
                left: unset;
                grid-area: 3 / 1 / 4 / 2;
                justify-self: start;
                align-self: start;
                width: 100% !important;
                text-align: center;
            }
            #comp-kdmswlbk > .font_2 {
                width:70%;
                margin: 0 auto;
            }
            #comp-kdmt39ab > .font_8 {
                width:70%;
                margin: 0 auto;
            }
            #comp-kdmt6lj8 {
                left: 0;
                margin-left: 80px;
                width: calc(100% - 80px - 80px);
                min-width: initial;
                height: 520px;
                margin-top: 40px;
            }
        }
        @media (max-width:767px) {
            #pro-gallery-comp-kdmt6lj8 {
                width: 270%;
            }
        }
        @media (max-width:575px) {
            ._2Uq8C ._1W1vp {
                top:145px;
                position: relative;
                box-shadow: var(--shd,0 1px 4px rgba(0,0,0,.6));
                -webkit-transform: translateZ(0);
            }
            .ijfCY ._3slSb>._2-VSr>._20QND {
                position: absolute;
                top: 0;
                left: 0;
                right: 0;
                bottom: 0;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
                font: var(--fnt,var(--font_1));
                line-height: var(--item-height,50px);
                color: rgb(var(--txt,var(--color_15)));
                display: inline;
                white-space: nowrap;
                overflow: hidden;
                text-overflow: ellipsis;
            }
            ._1XkJp._11WHE ._2fQ0v {
                transform: translateY(10px) translateY(-50%) rotate(-45deg);
            }
            ._1XkJp._11WHE ._2Z3R- {
                background-color: rgba(var(--lineColorOpen,var(--color_11)),var(--alpha-lineColorOpen,1));
            }
            ._2Z3R- {
                background-color: rgba(var(--lineColor,var(--color_11)),var(--alpha-lineColor,1));
                height: 2px;
                width: 100%;
                border-radius: 2px;
                position: absolute;
                left: 0;
                transition: all .25s ease-in-out;
            }
            ._1XkJp._11WHE .q8uXp {
                transform: translateY(-10px) translateY(50%) rotate(45deg);
            }
            ._1XkJp._11WHE ._2Z3R- {
                background-color: rgba(var(--lineColorOpen,var(--color_11)),var(--alpha-lineColorOpen,1));
            }
            ._1XkJp._11WHE ._9P1US {
                opacity: 0;
            }
            ._1XkJp._11WHE ._2Z3R- {
                background-color: rgba(var(--lineColorOpen,var(--color_11)),var(--alpha-lineColorOpen,1));
            }
            ._2Uq8C>ul {
                width: 100%;
                box-sizing: border-box;
            }
            ._2Uq8C>ul li {
                display: block;
            }
            .ijfCY {
                --padding-left-lvl1: var(--padding-left,0);
                --padding-right-lvl1: var(--padding-right,0);
                --padding-left-lvl2: var(--sub-padding-left,0);
                --padding-right-lvl2: var(--sub-padding-right,0);
                --padding-left-lvl3: calc(2 * var(--padding-left-lvl2) - var(--padding-left-lvl1));
                --padding-right-lvl3: calc(2 * var(--padding-right-lvl2) - var(--padding-right-lvl1));
                background-color: rgba(var(--bg,var(--color_11)),var(--alpha-bg,1));
                margin: 0;
                position: relative;
                display: flex;
                list-style: none;
                box-sizing: border-box;
                min-width: 100px;
                text-align: var(--text-align,left);
                border-style: solid;
                border-color: rgba(var(--brd,var(--color_15)),var(--alpha-brd,1));
                border-width: var(--brw,1px);
                transition: var(--itemBGColorTrans,background-color .4s ease 0s);
            }
            .ijfCY._2AxyX>._3slSb {
                background-color: rgba(var(--bgs,var(--color_15)),var(--alpha-bgs,1));
            }
            #MENU_AS_CONTAINER_EXPANDABLE_MENU {
                --bgs: var(--color_11);
                --itemBGColorNoTrans: background-color 50ms ease 0s;
                --shd: none;
                --bg: var(--color_11);
                --brw: 0px 0px 0px 0px;
                --brd: var(--color_15);
                --itemBGColorTrans: background-color 0.4s ease 0s;
                --fnt: var(--font_7);
                --txt: var(--color_15);
                --alpha-txt: 1;
                --arrowColor: var(--color_15);
                --alpha-arrowColor: 1;
                --subMenuOpacityTrans: all 0.4s ease 0s;
                --bgsSub: var(--color_11);
                --txtsSub: var(--color_14);
                --alpha-txtsSub: 1;
                --txts: var(--color_14);
                --alpha-txts: 1;
                --bgexpanded: var(--color_11);
                --fntSubMenu: var(--font_7);
                --txtexpanded: var(--color_15);
                --alpha-txtexpanded: 1;
                --menuSpacing: 10px;
                --subMenuSpacing: 25px;
                --boxShadowToggleOn-shd: none;
                --alpha-bgsSub: 0;
                --alpha-SKINS_bgSubmenu: 0;
                --bgh: 230,234,245;
                --SKINS_submenuMargin: 0;
                --alpha-brd: 0.2;
                --SKINS_submenuBR: 90px;
                --separatorHeight: 15;
                --alpha-bgexpanded: 1;
                --rd: 90px;
                --alpha-bg: 1;
                --alpha-bgh: 1;
                --textSpacing: 0;
                --sepw: 1;
                --alpha-bgs: 0;
                height: auto;
                --direction: rtl;
                --item-height: 56px;
                --text-align: center;
                --template-columns: calc(40px + 1em) 1fr calc(40px + 1em);
                --label-grid-column: 2;
                --arrow-grid-column: 3;
                --padding-left: 0px;
                --sub-padding-left: 0px;
                --padding-right: 0px;
                --sub-padding-right: 0px;
            }
            .ijfCY ._3slSb {
                position: relative;
                display: grid;
                cursor: pointer;
                grid-template-columns: 1fr;
                height: var(--item-height,50px);
            }
            #MENU_AS_CONTAINER {
                position: fixed;
                top:0px;
                height: 100%;
                --bg: var(--color_15);
                --alpha-bg: 0.6;
                --bg-overlay-color: rgb(var(--color_11));
                --bg-gradient: none;
                --screen-width: 100vw;
                width: 100%;
                z-index: 999;
            }
            #comp-kdmvb0f7 {
                margin-bottom: -40px;
            }
            #comp-kyfy4db8 {
                --item-size: 39px;
                --item-margin: 0 10px 0 0;
                --item-display: inline-block;
                width: 88px;
                height: 70px;
            }
            [data-mesh-id=SITE_FOOTERinlineContent-gridContainer] {
                position: static;
                display: grid;
                height: auto;
                width: 100%;
                min-height: auto;
                grid-template-rows: repeat(2, min-content) 1fr;
                grid-template-columns: 100%;
            }
            #comp-kdmvot8b {
                width: 209px;
                height: 19px;
            }
            [data-mesh-id=SITE_FOOTERinlineContent-gridContainer] > [id="comp-kdmvot8b"] {
                position: relative;
                margin: 0px 0px 0px calc((100% - 320px) * 0.5);
                left: 25px;
                grid-area: 3 / 1 / 4 / 2;
                justify-self: start;
                align-self: start;
            }
            #comp-kdmvot8b * {
                text-align: left !important;
            }
            #comp-kyfn5b7m {
                width: 175px;
                height: 119px;
            }
            [data-mesh-id=SITE_FOOTERinlineContent-gridContainer] > [id="comp-kyfn5b7m"] {
                position: relative;
                margin: 2px 0px 13px calc((100% - 320px) * 0.5);
                left: 73px;
                grid-area: 1 / 1 / 2 / 2;
                justify-self: start;
                align-self: start;
            }
            body.device-mobile-optimized ._2O-Ry .xb9fU {
                white-space: normal;
            }
            ._2O-Ry .xb9fU {
                text-align: center;
                position: absolute;
                width: 100%;
                height: auto;
                white-space: normal;
            }
            #img_comp-kyfn5b7m > img {
                width: 175px !important;
                height: 119px !important;
                object-fit: cover;
                object-position: 50% 50%;
            }
            #img_1_comp-kyfy4db8 > img{
                width: 39px !important;
                height: 39px !important;
                object-fit: cover;
            }
            [data-mesh-id=comp-kdmvb0g6inlineContent-gridContainer]{
                position: static;
                display: grid;
                height: auto;
                width: 100%;
                min-height: auto;
                grid-template-rows: repeat(3, min-content) 1fr;
                grid-template-columns: 100%;
            }
            [data-mesh-id=comp-kdmvb0g6inlineContent-gridContainer] > [id="comp-kdmvb0g91"] {
                position: relative;
                margin: 38px 0px 20px 0;
                left: 20px;
                grid-area: 1 / 1 / 2 / 2;
                justify-self: start;
                align-self: start;
            }
            #comp-kdmvb0gc {
                width: 60px;
                height: 12px;
            }
            [data-mesh-id=comp-kdmvb0g6inlineContent-gridContainer] > [id="comp-kdmvb0gc"] {
                position: relative;
                margin: 0px 0px 12px 0;
                left: 20px;
                grid-area: 2 / 1 / 3 / 2;
                justify-self: start;
                align-self: start;
            }
            #comp-kdmvb0gf1 {
                width: 246px;
                height: 57px;
            }
            [data-mesh-id=comp-kdmvb0g6inlineContent-gridContainer] > [id="comp-kdmvb0gf1"] {
                position: relative;
                margin: 0px 0px 20px 0;
                left: 20px;
                grid-area: 3 / 1 / 4 / 2;
                justify-self: start;
                align-self: start;
            }
            #comp-kdmvb0gg3 {
                width: 116px;
                height: 38px;
            }
            [data-mesh-id=comp-kdmvb0g6inlineContent-gridContainer] > [id="comp-kdmvb0gg3"] {
                position: relative;
                margin: 0px 0px 44px 0;
                left: 20px;
                grid-area: 4 / 1 / 5 / 2;
                justify-self: start;
                align-self: start;
            }
            #comp-kdmvb0g91 {
                width: 280px;
                height: 48px;
            }
            #comp-kdmv8oxt {
                height:400px;
                margin-bottom:0px;
            }
            #comp-kdmv3h7i  > .font_8 {
                width: 75%;
                margin: 0 auto;
            }
            ._3Ebtn._2Jgim ._281t-, ._3OF8h._2Jgim ._281t-{
                margin-bottom:0px;
            }
            #comp-kdmv8oys3{
                height:400px;
            }
            #comp-kdmutj7n {
                height: 332px;
            }
            [data-mesh-id=comp-kdmrgvllinlineContent] {
                height:486px;
            }
            [data-mesh-id=comp-kdmutj7ninlineContent-gridContainer] > [id="comp-kdmuy4bu"] {
                position: relative;
                margin: 0px 0px 10px 0;
                left: 20px;
                grid-area: 2 / 1 / 3 / 2;
                justify-self: start;
                align-self: start;
            }
            #comp-kdmuy4bu {
                width: 60px;
                height: 12px;
            }
            #comp-kdmuy4bf * {
                text-align: left !important;
            }
            #comp-kdmrgvj8 {
                left: 0;
                margin-left: 0;
                width: 320px;
                height:486px;
            }
            [data-mesh-id=comp-kdmrgvllinlineContent-gridContainer] {
                position: static;
                display: block;
                height: 130px;
                width: 100%;
                margin-top:-25px;
                min-height: auto;
                grid-template-rows: repeat(4, min-content) 1fr;
                grid-template-columns: 100%;
            }
            [data-mesh-id=comp-kdmrgvllinlineContent-gridContainer] > [id="comp-kdmrncb1"] {
                position: relative;
                margin: 0px 0px 9px 0;
                left: 22px;
                grid-area: 3 / 1 / 4 / 2;
                justify-self: start;
                align-self: start;
            }
            #img_comp-kdmrgvj8 > img {
                width: 100% !important;
                height: 486px !important;
                object-fit: cover;
                object-position: 50% 50%;
            }
            ._3SQN-, ._3wnIc {
                position: absolute;
                top: 0;
                width: 100%;
                height: 100%;
            }
            [data-mesh-id=Containerc1dmpinlineContent-gridContainer] > [id="comp-kdmrgvj8"] {
                position: relative;
                margin: 0px 0px 0 calc((100% - 320px) * 0.5);
                left: 0px;
                grid-area: 1 / 1 / 2 / 2;
                justify-self: start;
                align-self: start;
            }
            #comp-kdmuy4bf {
                --font_0: normal normal bold 21.542857142857144px/1.4em avenir-lt-w01_35-light1475496,avenir-lt-w05_35-light,sans-serif;
                --font_1: normal normal normal 11.885714285714286px/1.4em din-next-w01-light,din-next-w02-light,din-next-w10-light,sans-serif;
                --font_2: normal normal bold 20.057142857142857px/1.4em avenir-lt-w01_35-light1475496,avenir-lt-w05_35-light,sans-serif;
                --font_3: normal normal bold 19.314285714285717px/1.4em avenir-lt-w01_35-light1475496,avenir-lt-w05_35-light,sans-serif;
                --font_4: normal normal bold 14.857142857142858px/1.4em barlow-extralight,barlow,sans-serif;
                --font_5: normal normal bold 28.97142857142857px/1.4em avenir-lt-w01_35-light1475496,avenir-lt-w05_35-light,sans-serif;
                --font_6: normal normal normal 16.34285714285714px/1.4em avenir-lt-w01_85-heavy1475544,avenir-lt-w05_85-heavy,sans-serif;
                --font_7: normal normal normal 16.34285714285714px/1.4em barlow-medium,barlow,sans-serif;
                --font_8: normal normal bold 13.371428571428572px/1.4em barlow-extralight,barlow,sans-serif;
                --font_9: normal normal bold 11.885714285714286px/1.4em barlow-extralight,barlow,sans-serif;
                --font_10: normal normal normal 9.657142857142858px/1.4em din-next-w01-light,din-next-w02-light,din-next-w10-light,sans-serif;
                height: auto;
            }
            #comp-kdmuy4bf {
                width: 280px;
                height: 48px;
            }
            #comp-kdmu0n5v {
                --shd: 0 0 0 rgba(0, 0, 0, 0.6);
                --dotsColor: var(--color_13);
                --dotsSelectedColor: var(--color_15);
                --arrowColor: var(--color_15);
                --rd: 0px;
                --brw: 0px;
                --brd: var(--color_11);
                --alpha-brd: 1;
                --alpha-arrowColor: 1;
                --alpha-dotsColor: 1;
                --alpha-dotsSelectedColor: 1;
                height: 174px;
                --nav-dot-section-display: block;
                --nav-dot-section-bottom-margin: 35px;
                --nav-dot-margin: 7.5px;
                --nav-dot-size: 6px;
                --nav-dot-size-selected: 6px;
                --nav-button-width: 15px;
                --nav-button-offset: 22px;
                --nav-button-display: none;
                --slides-overflow: hidden;
                --transition-duration: 1000ms;
            }
            #comp-kdmu0n5v {
                left: 0;
                margin-left: 0;
                width: 320px;
            }
            [data-mesh-id=Containerc1dmpinlineContent-gridContainer] > [id="comp-kdmu0n5v"] {
                position: relative;
                margin: 132px 0px 26px calc((100% - 320px) * 0.5);
                left: 0px;
                grid-area: 8 / 1 / 9 / 2;
                justify-self: start;
                align-self: start;
            }
            ._3Ebtn._2Jgim, ._3OF8h._2Jgim {
                display: var(--display);
                --display: grid;
                background-color: transparent;
                box-sizing: border-box;
                position: relative;
                min-height: 50px;
            }
            [data-mesh-id=comp-kdmutj7ninlineContent-gridContainer] > [id="comp-kdmuy4bf"] {
                position: relative;
                margin: 42px 0px 20px 0;
                left: 20px;
                grid-area: 1 / 1 / 2 / 2;
                justify-self: start;
                align-self: start;
            }
            ._1Q9if, ._2Hij5 {
                word-wrap: break-word;
                overflow-wrap: break-word;
                text-align: start;
                pointer-events: none;
                min-height: var(--min-height);
                min-width: var(--min-width);
            }
            #comp-kdmttxi5 > ._1uldx {
                height:285px;
            }
            #comp-kdzqkilc {
                left: 0;
                margin-left: 0px;
                width: calc(100% - 80px - 80px);
                min-width: initial;
                height: 5px;
            }
            [data-mesh-id=Containerc1dmpinlineContent-gridContainer] > [id="comp-kfdto1n5"] {
                height: 60px;
            }
            [data-mesh-id=comp-kfdto1pjinlineContent-gridContainer] > [id="comp-kdzqkilc"] {
                position: relative;
                margin: 30px 0px 25px 0;
                left: 0px;
                grid-area: 1 / 1 / 2 / 2;
                justify-self: start;
                align-self: start;

            }
            ._2UdPt {
                box-sizing: border-box;
                border-top: var(--lnw,2px) solid rgba(var(--brd,var(--color_15)),var(--alpha-brd,1));
                height: 0;
            }
            .mobileView {
                display:grid !important;
            }
            [data-mesh-id=comp-kdmttxjpinlineContent-gridContainer] > [id="comp-kdmtvktv"] {
                position: relative;
                margin: 26px 0px 20px 0;
                left: 20px;
                grid-area: 1 / 1 / 2 / 2;
                justify-self: start;
                align-self: start;
            }
            #comp-kdmtvkua {
                width: 60px;
                height: 12px;
            }
            [data-mesh-id=comp-kdmttxjpinlineContent-gridContainer] > [id="comp-kdmtvkua"] {
                position: relative;
                margin: 0px 0px 12px 0;
                left: 20px;
                grid-area: 2 / 1 / 3 / 2;
                justify-self: start;
                align-self: start;
            }
            [data-mesh-id=comp-kdmttxjpinlineContent-gridContainer] > [id="comp-kdmtx5uh"] {
                position: relative;
                margin: 0px 0px 33px 0;
                left: 20px;
                grid-area: 4 / 1 / 5 / 2;
                justify-self: start;
                align-self: start;
            }
            #comp-kdmtx5uh {
                width: 133px;
                height: 42px;
            }
            ._1fbEI {
                text-align: initial;
                display: flex;
                align-items: center;
                box-sizing: border-box;
                width: -webkit-max-content;
                width: -moz-max-content;
                width: max-content;
                justify-content: var(--label-align);
                min-width: 100%;
            }
            [data-mesh-id=comp-kdmttxjpinlineContent-gridContainer] > [id="comp-kdmtvkul"] {
                position: relative;
                margin: 0px 0px 20px 0;
                left: 20px;
                grid-area: 3 / 1 / 4 / 2;
                justify-self: start;
                align-self: start;
            }
            .webView {
                display:none !important;
            }
            .grid-container {
                display: grid !important;
                grid-template-columns: auto auto;
                grid-gap: 10px;
                padding: 0px;
            }
            #comp-kdmt6lj8 {
                left: 0;
                margin-left: 0px;
                width: 100% !important;
                min-width: 100% !important;
                height: 520px !important;
                margin-top: 20px !important;
            }
            .image_container {
                display:flex;
                flex-direction:column;
            }
            .image_container > .text_area {
                font-family: barlow-medium, barlow, sans-serif !important;
                font-size:14px;
                color:#141414
            }
            .grid-container > div {
                text-align: center;
                padding: 20px 0;
                font-size: 30px;
            }
            #comp-kdmt6lj8 {
                left: 0;
                margin-left: 0px;
                width: 100% !important;
                min-width: 100% !important;
                height: 645px;
                margin-top: 40px;
            }
            #gallery-wrapper-comp-kdmt6lj8 {
                overflow: hidden !important;
                display:none !important;
            }
            #comp-kdmt5r6h {
                margin: 20px 0px 10px calc((100% - 980px) * 0);
                left: 15px;
            }
            #comp-kdmswlbk {
                display: flex;
                margin-top:60px;
                justify-content: flex-start;
            }
            #comp-kdmswlbk > .font_2 {
                width:310px;
                text-align:start;
                margin:unset;
                padding-left:15px;
            }
            #comp-kdmt39ab {
                position: relative;
                margin: 0px;
                margin-top: 8px;
                left: unset;
                grid-area: 3 / 1 / 4 / 2;
                justify-self: start;
                align-self: start;
                width: 100% !important;
                text-align: center;
            }
            #comp-kdmt39ab > .font_8 {
                width: 70%;
                text-align: start;
                padding-left: 15px;
                margin: unset;
            }
            .mobile_area_button > a {
                right:unset !important;
            }
            #comp-kdmrqb46 {
                width: 210px;
                height: 51px;
            }
            [data-mesh-id=comp-kdmrgvllinlineContent-gridContainer] > [id="comp-kdmrqb46"] {
                position: relative;
                margin: 0px 0px 14px 0;
                left: 26px;
                grid-area: 4 / 1 / 5 / 2;
                justify-self: start;
                align-self: start;
            }
            #comp-kfcmqpy2 {
                width: 103px !important;
                height: 41px !important;
            }
            [data-mesh-id=comp-kdmrgvllinlineContent-gridContainer] > [id="comp-kfcmqpy2"] {
                position: relative;
                margin: 0px 0px 33px 0;
                left: 22px;
                grid-area: 5 / 1 / 6 / 2;
                justify-self: start;
                align-self: start;
            }
        }
        @media (max-width:474px) {
            #comp-kdmt5r6h {
                width: 59px;
                height: 19px;
            }
            #comp-kdmt39ab > .font_8 {
                width: 200px;
            }
            #comp-kyfn4iuj {
                width: 106px;
                height: 72px;
                margin-top: 14px !important;
                margin-bottom: 25px !important;
            }
            #SITE_HEADER_WRAPPER {
                margin-top: -40px;
                background: #f7f7f7;
            }
            #MENU_AS_CONTAINER_TOGGLE {
                display: block !important;
                width: 50px;
                height: 50px;
                position: absolute;
                margin: 0px 0px 10px 0;
                right: 15px;
                top: 110px;
                grid-area: 2 / 1 / 3 / 2;
                justify-self: start;
                align-self: start;
            }
            #comp-kdmt39ab > .font_8 > span  > span > span > span {
                font-size: 14px !important;
            }
            #comp-kdmswlbk > .font_2 {
                width: 205px;
                text-align: start;
                margin: unset;
                padding-left: 15px;
                font-size: 20px !important;
            }
        }
    </style>
</pages-css>
<div id="SITE_CONTAINER" style="margin-top: -3%;">
    <style id="STYLE_OVERRIDES_ID">#comp-kdmvsx8x1{visibility:hidden !important;}</style>
    <div id="main_MF">
        <div id="SCROLL_TO_TOP" class="qhwIj ignore-focus undefined" tabindex="-1" role="region" aria-label="top of page">&nbsp;</div>
        <button id="SKIP_TO_CONTENT_BTN" class="_2SeL5 has-custom-focus" tabindex="0">跳到主要內容</button>
        <div id="BACKGROUND_GROUP">
            <div id="BACKGROUND_GROUP_TRANSITION_GROUP">
                <div id="pageBackground_c1dmp" data-media-height-override-type="" data-media-position-override="false" class="_2v3yk">
                    <div id="bgLayers_pageBackground_c1dmp" data-hook="bgLayers" class="_3wnIc">
                        <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                        <div id="bgMedia_pageBackground_c1dmp" class="_2GUhU"></div>
                        <div data-testid="bgOverlay" class="_1eCSh"></div>
                    </div>
                </div>
            </div>
        </div>
        <div id="site-root">
            <div id="masterPage" class="mesh-layout">
                <!-- header area -->
                <header tabindex="-1" id="SITE_HEADER_WRAPPER">
                    <div id="SITE_HEADER" class="_3Fgqs Xf6e1" tabindex="-1">
                        <div class="rmFV4"></div>
                        <div class="_319u9">
                            <div class="_3N4he"></div>
                            <div class="_1U65c">
                                <div data-mesh-id="SITE_HEADERinlineContent" data-testid="inline-content" class="">
                                    <div data-mesh-id="SITE_HEADERinlineContent-gridContainer" data-testid="mesh-container-content">
                                        <section id="comp-kdmsih9l" class="_3d64y">
                                            <div id="bgLayers_comp-kdmsih9l" data-hook="bgLayers" class="_3wnIc">
                                                <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                                                <div id="bgMedia_comp-kdmsih9l" class="_2GUhU"></div>
                                            </div>
                                            <div data-testid="columns" class="_1uldx">
                                                <div id="comp-kdmsihb7" class="_1vNJf">
                                                    <div id="bgLayers_comp-kdmsihb7" data-hook="bgLayers" class="_3wnIc">
                                                        <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                                                        <div id="bgMedia_comp-kdmsihb7" class="_2GUhU"></div>
                                                    </div>
                                                    <div data-mesh-id="comp-kdmsihb7inlineContent" data-testid="inline-content" class="">
                                                        <div data-mesh-id="comp-kdmsihb7inlineContent-gridContainer" data-testid="mesh-container-content">
                                                            <div id="MENU_AS_CONTAINER_TOGGLE" style="display:none;" class="_88XEI" role="button" aria-haspopup="dialog" aria-label="Open navigation menu" tabindex="0">
                                                                <div class="_1j6Hs">
                                                                    <div class="_1XkJp">
                                                                        <span class="_2Z3R- _2fQ0v"></span>
                                                                        <span class="_2Z3R- _9P1US"></span>
                                                                        <span class="_2Z3R- q8uXp"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div id="comp-kyfn4iuj" class="XUUsC">
                                                                <div data-testid="linkElement" class="xQ_iF">
                                                                    <wix-image id="img_comp-kyfn4iuj" class="_1-6YJ _1Fe8-" data-image-info="{&quot;containerId&quot;:&quot;comp-kyfn4iuj&quot;,&quot;targetWidth&quot;:106,&quot;targetHeight&quot;:72,&quot;displayMode&quot;:&quot;fill&quot;,&quot;imageData&quot;:{&quot;width&quot;:2232,&quot;height&quot;:1516,&quot;uri&quot;:&quot;4da0f3_4fd3ac5abcb4442e9bc31ca435cbd1e2~mv2.png&quot;,&quot;name&quot;:&quot;3311641888468_.pic.png&quot;,&quot;displayMode&quot;:&quot;fill&quot;}}" data-bg-effect-name="" data-has-ssr-src="" data-src="https://static.wixstatic.com/media/4da0f3_4fd3ac5abcb4442e9bc31ca435cbd1e2~mv2.png/v1/fill/w_106,h_72,al_c,q_85,usm_0.66_1.00_0.01,enc_auto/3311641888468__pic.png"><img src="/index_files/3311641888468__pic.png" alt="3311641888468_.pic.png" style="width:106px;height:72px;object-fit:cover" srcset="https://static.wixstatic.com/media/4da0f3_4fd3ac5abcb4442e9bc31ca435cbd1e2~mv2.png/v1/fill/w_106,h_72,al_c,q_85,usm_0.66_1.00_0.01,enc_auto/3311641888468__pic.png 1x, https://static.wixstatic.com/media/4da0f3_4fd3ac5abcb4442e9bc31ca435cbd1e2~mv2.png/v1/fill/w_212,h_144,al_c,q_85,usm_0.66_1.00_0.01,enc_auto/3311641888468__pic.png 2x" fetchpriority="high"></wix-image>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="comp-kdmsir58" class="_1vNJf">
                                                    <div id="bgLayers_comp-kdmsir58" data-hook="bgLayers" class="_3wnIc">
                                                        <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                                                        <div id="bgMedia_comp-kdmsir58" class="_2GUhU"></div>
                                                    </div>
                                                    <div data-mesh-id="comp-kdmsir58inlineContent" data-testid="inline-content" class="">
                                                        <div data-mesh-id="comp-kdmsir58inlineContent-gridContainer" data-testid="mesh-container-content">
                                                            <wix-dropdown-menu id="comp-kdmrh8vz" class="_1oZ90 _3SSUZ hidden-during-prewarmup" tabindex="-1" dir="ltr" data-stretch-buttons-to-menu-width="true" data-same-width-buttons="false" data-num-items="5" data-menuborder-y="0" data-menubtn-border="0" data-ribbon-els="0" data-label-pad="0" data-ribbon-extra="0" data-dropalign="center"  data-dropmode="dropDown">
                                                                <nav class="l76As" id="comp-kdmrh8vznavContainer" aria-label="網址">
                                                                    <ul class="_2MHbW" id="comp-kdmrh8vzitemsContainer" style="text-align:center" data-marginallchildren="true">
                                                                        <li id="comp-kdmrh8vz0" data-direction="ltr" data-listposition="left" data-data-id="dataItem-kdmw2jvz" data-state="menu false  link" data-index="0" class="Y5j6d _3KF65" data-original-gap-between-text-and-btn="10" aria-hidden="false" style="width: 107px; height: 50px; position: relative; box-sizing: border-box; overflow: visible; visibility: inherit;">
                                                                            <a data-testid="linkElement" href="/" target="_self" class="_2DUrw" aria-haspopup="false">
                                                                                <div class="_1u8sp">
                                                                                    <div class="" style="text-align:center">
                                                                                        <p class="_1zyfI" style="text-align: center; line-height: 50px;" id="comp-kdmrh8vz0label">Home</p>
                                                                                    </div>
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        <li id="comp-kdmrh8vz5" data-direction="ltr" data-listposition="right" data-data-id="dataItem-l863j3tg" data-state="menu false  link" data-index="2" class="Y5j6d _3KF65" data-original-gap-between-text-and-btn="10" aria-hidden="false" style="width: 125px; height: 50px; position: relative; box-sizing: border-box; overflow: visible; visibility: inherit;">
                                                                            <a data-testid="linkElement" href="/service" target="_self" class="_2DUrw" aria-haspopup="false">
                                                                                <div class="_1u8sp">
                                                                                    <div class="" style="text-align:center">
                                                                                        <p class="_1zyfI" style="text-align: center; line-height: 50px;" id="comp-kdmrh8vz5label">Our Services</p>
                                                                                    </div>
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        <li id="comp-kdmrh8vz0" data-direction="ltr" data-listposition="left" data-data-id="dataItem-kdmw2jvz" data-state="menu false  link" data-index="0" class="Y5j6d _3KF65" data-original-gap-between-text-and-btn="10" aria-hidden="false" style="width: 107px; height: 50px; position: relative; box-sizing: border-box; overflow: visible; visibility: inherit;">
                                                                            <a data-testid="linkElement" href="/article" target="_self" class="_2DUrw" aria-haspopup="false">
                                                                                <div class="_1u8sp">
                                                                                    <div class="" style="text-align:center">
                                                                                        <p class="_1zyfI" style="text-align: center; line-height: 50px;" id="comp-kdmrh8vz0label">Article</p>
                                                                                    </div>
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        <li id="comp-kdmrh8vz__more__" data-direction="ltr" data-listposition="right" data-state="menu false  header" data-index="__more__" data-dropdown="false" class="_2nffT _3KF65" data-original-gap-between-text-and-btn="10" aria-hidden="true" style="height: 0px; overflow: hidden; position: absolute; visibility: hidden;">
                                                                            <div data-testid="linkElement" class="_2DUrw" tabindex="0" aria-haspopup="false">
                                                                                <div class="_1u8sp">
                                                                                    <div class="" style="text-align:center">
                                                                                        <p class="_1zyfI" style="text-align:center" id="comp-kdmrh8vz__more__label" tabindex="-1">More</p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                    </ul>
                                                                    <div class="_2vbYi" id="comp-kdmrh8vzdropWrapper" data-dropalign="center" data-dropdown-shown="false" style="inset: 50px 0px auto auto;">
                                                                        <ul class="_1mmDc" id="comp-kdmrh8vzmoreContainer" style="left: auto; right: 0px;"></ul>
                                                                    </div>
                                                                    <div style="display:none" id="comp-kdmrh8vznavContainer-hiddenA11ySubMenuIndication">Use tab to navigate through the menu items.</div>
                                                                </nav>
                                                            </wix-dropdown-menu>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="comp-kdoiv97i" class="_1vNJf">
                                                    <div id="bgLayers_comp-kdoiv97i" data-hook="bgLayers" class="_3wnIc">
                                                        <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                                                        <div id="bgMedia_comp-kdoiv97i" class="_2GUhU"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                    <div id="MENU_AS_CONTAINER" style="display: none;" tabindex="0" data-undisplayed="true" class="">
                                        <div id="container-MENU_AS_CONTAINER" class="_2RhtK" style="">
                                            <div id="bgLayers_MENU_AS_CONTAINER" data-hook="bgLayers" class="_3wnIc">
                                                <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                                                <div id="bgMedia_MENU_AS_CONTAINER" class="_2GUhU"></div>
                                            </div>
                                            <div id="inlineContentParent-MENU_AS_CONTAINER" class="_2Lz_A _179d0">
                                                <div data-mesh-id="MENU_AS_CONTAINERinlineContent" data-testid="inline-content" class="">
                                                    <div data-mesh-id="MENU_AS_CONTAINERinlineContent-gridContainer" data-testid="mesh-container-content">
                                                        <nav id="MENU_AS_CONTAINER_EXPANDABLE_MENU" aria-label="網址" class="_2Uq8C">
                                                            <ul class="_1W1vp">
                                                                <li data-testid="MENU_AS_CONTAINER_EXPANDABLE_MENU-0" aria-current="page" class="ijfCY _11rUu _2AxyX">
                                                                    <div data-testid="itemWrapper" class="_3slSb"><span data-testid="linkWrapper" class="_2-VSr"><a data-testid="linkElement" href="index.html" target="_self" class="_20QND">Home</a></span></div>
                                                                </li>
                                                                <li data-testid="MENU_AS_CONTAINER_EXPANDABLE_MENU-1" class="ijfCY _11rUu">
                                                                    <div data-testid="itemWrapper" class="_3slSb"><span data-testid="linkWrapper" class="_2-VSr"><a data-testid="linkElement" data-anchor="dataItem-kdmw87kx" href="index.html" target="_self" class="_20QND">Contact</a></span></div>
                                                                </li>
                                                                <li data-testid="MENU_AS_CONTAINER_EXPANDABLE_MENU-2" class="ijfCY _11rUu">
                                                                    <div data-testid="itemWrapper" class="_3slSb"><span data-testid="linkWrapper" class="_2-VSr"><a data-testid="linkElement" href="/service" target="_self" class="_20QND">Our Services</a></span></div>
                                                                </li>
                                                            </ul>
                                                        </nav>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
                <!-- header area -->
                <main id="PAGES_CONTAINER" tabindex="-1">
                    <div id="SITE_PAGES" class="_1gF1C">
                        <div id="SITE_PAGES_TRANSITION_GROUP" class="_2YGAo">
                            <div id="c1dmp" class="Ry26q">
                                <div class="_3CemL" data-testid="page-bg"></div>
                                <div class="_3K7uv">
                                    <div id="Containerc1dmp" class="_1KV2M">
                                        <div data-mesh-id="Containerc1dmpinlineContent" data-testid="inline-content" class="">
                                            <div data-mesh-id="Containerc1dmpinlineContent-gridContainer" data-testid="mesh-container-content">
                                                <!-- header image section -->

                                                <section id="comp-kdmrgvj8" class="webView _3d64y">
                                                    <div id="bgLayers_comp-kdmrgvj8" data-hook="bgLayers" class="_3wnIc">
                                                        <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                                                        <div id="bgMedia_comp-kdmrgvj8" class="_2GUhU">
                                                            <wix-image id="img_comp-kdmrgvj8" class="_1-6YJ _3SQN- _2L00W _1Q5R5 bgImage" data-image-info="{&quot;containerId&quot;:&quot;comp-kdmrgvj8&quot;,&quot;targetWidth&quot;:1843,&quot;targetHeight&quot;:928,&quot;alignType&quot;:&quot;center&quot;,&quot;displayMode&quot;:&quot;fill&quot;,&quot;imageData&quot;:{&quot;width&quot;:1920,&quot;height&quot;:1200,&quot;uri&quot;:&quot;4da0f3_c4245440962f4dae8355bde6fafb04c4~mv2.jpg&quot;,&quot;displayMode&quot;:&quot;fill&quot;}}" data-has-bg-scroll-effect="" data-bg-effect-name="" data-has-ssr-src="" data-src="https://static.wixstatic.com/media/4da0f3_c4245440962f4dae8355bde6fafb04c4~mv2.jpg/v1/fill/w_1905,h_928,al_c,q_85,usm_0.66_1.00_0.01,enc_auto/4da0f3_c4245440962f4dae8355bde6fafb04c4~mv2.jpg"><img src="/index_files/4da0f3_c4245440962f4dae8355bde6fafb04c4_mv2.jpg" alt="716104.jpg" style="width: 1905px; height: 928px; object-fit: cover; object-position: 50% 50%;"></wix-image>
                                                        </div>
                                                    </div>
                                                    <div data-testid="columns" class="_1uldx">
                                                        <div id="comp-kdmrgvll" class="_1vNJf">
                                                            <div id="bgLayers_comp-kdmrgvll" data-hook="bgLayers" class="_3wnIc">
                                                                <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                                                                <div id="bgMedia_comp-kdmrgvll" class="_2GUhU"></div>
                                                            </div>
                                                            <div data-mesh-id="comp-kdmrgvllinlineContent" data-testid="inline-content" class="">
                                                                <div data-mesh-id="comp-kdmrgvllinlineContent-gridContainer" data-testid="mesh-container-content">
                                                                    <div data-mesh-id="comp-kdms14ak-rotated-wrapper">
                                                                        <div id="comp-kdms14ak" class="">
                                                                            <div data-testid="svgRoot-comp-kdms14ak" class="_3bLYT _2OIRR">
                                                                                <!--?xml version="1.0" encoding="UTF-8"?-->
                                                                                <svg preserveAspectRatio="none" data-bbox="75.501 16.5 48.999 166.999" viewBox="75.501 16.5 48.999 166.999" height="200" width="200" xmlns="http://www.w3.org/2000/svg" data-type="color" role="presentation" aria-hidden="true">
                                                                                    <defs>
                                                                                        <style>#comp-kdms14ak svg [data-color="1"] {fill: #141414;}</style>
                                                                                    </defs>
                                                                                    <g>
                                                                                        <path d="M117.464 139.877a2.013 2.013 0 0 0-1.914-.519 1.986 1.986 0 0 0-1.431 1.359l-3.282 10.935a1.976 1.976 0 0 0 1.357 2.462c1.063.306 2.18-.286 2.494-1.339l2.235-7.443c3.772 5.113 5.227 11.666.988 15.851l-15.755 15.553V36.964c4.745-.985 8.321-5.146 8.321-10.117 0-5.705-4.701-10.347-10.477-10.347-5.78 0-10.481 4.641-10.481 10.347 0 4.972 3.577 9.133 8.325 10.118v139.769l-15.755-15.553c-4.239-4.184-2.784-10.737.988-15.851l2.235 7.447c.314 1.045 1.419 1.641 2.498 1.335a1.983 1.983 0 0 0 1.353-2.466l-3.286-10.935a1.99 1.99 0 0 0-1.431-1.355 2.02 2.02 0 0 0-1.914.519c-6.713 6.631-10.144 17.333-3.282 24.107l19.182 18.936c.384.379.898.581 1.419.581.05 0 .099-.025.149-.029.05.004.099.029.149.029.522 0 1.035-.201 1.419-.581l19.182-18.936c6.866-6.772 3.431-17.475-3.286-24.105zM93.533 26.847c0-3.519 2.902-6.383 6.466-6.383s6.462 2.864 6.462 6.383-2.898 6.379-6.462 6.379-6.466-2.861-6.466-6.379z" fill="#31333A" data-color="1"></path>
                                                                                    </g>
                                                                                </svg>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div id="comp-kdmrncb1" class="_2Hij5 _3bcaz" data-testid="richTextElement">
                                                                        <h1 class="font_5" style="line-height:normal; font-size:66px;"><span class="color_11"><span style="letter-spacing:normal;">Hi There!</span></span></h1>
                                                                    </div>
                                                                    <div id="comp-kdmrqb46" class="_2Hij5 _3bcaz" data-testid="richTextElement">
                                                                        <p class="font_4" style="line-height:normal; font-size:20px;"><span class="color_11">We are The Visa &amp; Immigration Solutions is one of the trusted visa consultancy provider in Malaysia.</span></p>
                                                                    </div>
                                                                    <div class="_2UgQw" id="comp-kfcmqpy2" aria-disabled="false"><a data-testid="linkElement" href="http://wa.me/601125457898" target="_blank" rel="noreferrer noopener" class="_1fbEI" aria-disabled="false"><span class="_1Qjd7">Contact Us</span></a></div>
                                                                    <div data-mesh-id="comp-kdmrgvllinlineContent-wedge-3"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div id="comp-kdmrkt18" class="_1vNJf">
                                                            <div id="bgLayers_comp-kdmrkt18" data-hook="bgLayers" class="_3wnIc">
                                                                <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                                                                <div id="bgMedia_comp-kdmrkt18" class="_2GUhU"></div>
                                                            </div>
                                                            <div data-mesh-id="comp-kdmrkt18inlineContent" data-testid="inline-content" class="">
                                                                <div data-mesh-id="comp-kdmrkt18inlineContent-gridContainer" data-testid="mesh-container-content"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                                <section id="comp-kdmrgvj8" style="display:none;" class="mobileView _3d64y">
                                                    <div id="bgLayers_comp-kdmrgvj8" data-hook="bgLayers" class="_3wnIc">
                                                        <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                                                        <div id="bgMedia_comp-kdmrgvj8" class="_2GUhU">
                                                            <wix-image id="img_comp-kdmrgvj8" class="_1-6YJ _3SQN- _2L00W _1Q5R5 bgImage" data-image-info="{&quot;containerId&quot;:&quot;comp-kdmrgvj8&quot;,&quot;targetWidth&quot;:320,&quot;targetHeight&quot;:488,&quot;alignType&quot;:&quot;center&quot;,&quot;displayMode&quot;:&quot;fill&quot;,&quot;imageData&quot;:{&quot;width&quot;:1920,&quot;height&quot;:1200,&quot;uri&quot;:&quot;4da0f3_c4245440962f4dae8355bde6fafb04c4~mv2.jpg&quot;,&quot;displayMode&quot;:&quot;fill&quot;}}" data-has-bg-scroll-effect="" data-bg-effect-name="" data-has-ssr-src="" data-src="https://static.wixstatic.com/media/4da0f3_c4245440962f4dae8355bde6fafb04c4~mv2.jpg/v1/fill/w_640,h_968,al_c,q_85,usm_0.66_1.00_0.01,enc_auto/4da0f3_c4245440962f4dae8355bde6fafb04c4~mv2.jpg"><img src="https://static.wixstatic.com/media/4da0f3_c4245440962f4dae8355bde6fafb04c4~mv2.jpg/v1/fill/w_640,h_968,al_c,q_85,usm_0.66_1.00_0.01,enc_auto/4da0f3_c4245440962f4dae8355bde6fafb04c4~mv2.jpg" alt="716104.jpg" style="width: 320px; height: 484px; object-fit: cover; object-position: 50% 50%;"></wix-image>
                                                        </div>
                                                    </div>
                                                    <div data-testid="columns" class="_1uldx">
                                                        <div id="comp-kdmrgvll" class="_1vNJf">
                                                            <div id="bgLayers_comp-kdmrgvll" data-hook="bgLayers" class="_3wnIc">
                                                                <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                                                                <div id="bgMedia_comp-kdmrgvll" class="_2GUhU"></div>
                                                            </div>
                                                            <div data-mesh-id="comp-kdmrgvllinlineContent" data-testid="inline-content" class="">
                                                                <div data-mesh-id="comp-kdmrgvllinlineContent-gridContainer" data-testid="mesh-container-content">
                                                                    <div id="comp-kdmrncb1" class="_2Hij5 _3bcaz" data-testid="richTextElement">
                                                                        <h1 class="font_5" style="position:absolute;line-height:normal; font-size:24px; color:rgba(255,255,255,1);"><span class="color_11" style="color:rgba(255,255,255,1);"><span style="letter-spacing:normal;">Hi There!</span></span></h1>
                                                                    </div>
                                                                    <div data-mesh-id="comp-kdms14ak-rotated-wrapper">
                                                                        <div id="comp-kdms14ak" class="">
                                                                            <div data-testid="svgRoot-comp-kdms14ak" class="_3bLYT _2OIRR">
                                                                                <!--?xml version="1.0" encoding="UTF-8"?-->
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div id="comp-kdmrqb46" class="_2Hij5 _3bcaz" data-testid="richTextElement">
                                                                        <p class="font_4" style="line-height:normal; font-size:14px;"><span class="color_11">We are The Visa &amp; Immigration Solutions is one of the trusted visa consultancy provider in Malaysia.</span></p>
                                                                    </div>
                                                                    <div class="_2UgQw" id="comp-kfcmqpy2" aria-disabled="false"><a data-testid="linkElement" href="http://wa.me/601125457898" target="_blank" rel="noreferrer noopener" class="_1fbEI" aria-disabled="false" tabindex="0"><span class="_1Qjd7">Contact Us</span></a></div>
                                                                    <div data-mesh-id="comp-kdmrgvllinlineContent-wedge-3"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>

                                                <!-- header image section -->
                                                <!-- Agriculture work -->

                                                <section id="comp-kdmt06ch" class="webView _3d64y">
                                                    <div id="bgLayers_comp-kdmt06ch" data-hook="bgLayers" class="_3wnIc">
                                                        <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                                                        <div id="bgMedia_comp-kdmt06ch" class="_2GUhU"></div>
                                                    </div>
                                                    <div data-testid="columns" class="_1uldx">
                                                        <div id="comp-kdmt06di" class="_1vNJf">
                                                            <div id="bgLayers_comp-kdmt06di" data-hook="bgLayers" class="_3wnIc">
                                                                <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                                                                <div id="bgMedia_comp-kdmt06di" class="_2GUhU"></div>
                                                            </div>
                                                            <div data-mesh-id="comp-kdmt06diinlineContent" data-testid="inline-content" class="">
                                                                <div data-mesh-id="comp-kdmt06diinlineContent-gridContainer" data-testid="mesh-container-content">
                                                                    <div id="comp-kdmswlbk" class="_2Hij5 _3bcaz" data-testid="richTextElement">
                                                                        <h2 class="font_2" style="line-height:normal; font-size:30px;">Agriculture work, crop farming jobs in Australia.</h2>
                                                                    </div>
                                                                    <div id="comp-kdmt5r6h" class="">
                                                                        <div data-testid="svgRoot-comp-kdmt5r6h" class="_3bLYT _2OIRR">
                                                                            <!--?xml version="1.0" encoding="UTF-8"?-->
                                                                            <svg preserveAspectRatio="xMidYMid meet" data-bbox="23 84.5 154 30.998" viewBox="23 84.5 154 30.998" height="200" width="200" xmlns="http://www.w3.org/2000/svg" data-type="color" role="presentation" aria-hidden="true">
                                                                                <defs>
                                                                                    <style>#comp-kdmt5r6h svg [data-color="1"] {fill: #B3B3B3;}</style>
                                                                                </defs>
                                                                                <g>
                                                                                    <path d="M176.86 92.328a1.85 1.85 0 0 0-1.71-1.143H39.619l-.004.001c-.793-3.812-4.177-6.686-8.218-6.686-4.629 0-8.397 3.767-8.397 8.397 0 4.632 3.768 8.399 8.396 8.399 3.946 0 7.244-2.748 8.137-6.425.03.001.055.017.085.017h36.313l-5.234 5.236a1.852 1.852 0 0 0 2.618 2.619l7.852-7.855h89.513l-14.529 14.534c-3.912 3.907-10.03 2.568-14.807-.913l6.957-2.062a1.852 1.852 0 0 0-1.049-3.552l-10.219 3.029a1.845 1.845 0 0 0-1.269 1.32 1.842 1.842 0 0 0 .485 1.765c3.818 3.82 9.083 6.489 14.044 6.489 3.084 0 6.049-1.031 8.476-3.458l17.689-17.696a1.854 1.854 0 0 0 .402-2.016zM31.396 97.593c-2.589 0-4.693-2.107-4.693-4.695s2.104-4.693 4.693-4.693 4.693 2.105 4.693 4.693-2.104 4.695-4.693 4.695z" fill="#0C1439" data-color="1"></path>
                                                                                </g>
                                                                            </svg>
                                                                        </div>
                                                                    </div>
                                                                    <div id="comp-kdmt39ab" class="_2Hij5 _3bcaz" data-testid="richTextElement">
                                                                        <p class="font_8" style="font-size:18px; line-height:1.4em;"><span style="letter-spacing:normal;"><span style="font-size:18px;"><span style="font-weight:bold;"><span style="font-family:barlow-extralight,barlow,sans-serif;">We can help you find some interesting and well-paid jobs, like supermarket workers or construction workers. And we'll provide you with all the information about those meat factories, like living conditions, salary and working hours.</span></span></span></span></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                                <section id="comp-kdmt06ch" style="display:none" class="mobileView _3d64y">
                                                    <div id="bgLayers_comp-kdmt06ch" data-hook="bgLayers" class="_3wnIc">
                                                        <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                                                        <div id="bgMedia_comp-kdmt06ch" class="_2GUhU"></div>
                                                    </div>
                                                    <div data-testid="columns" class="_1uldx">
                                                        <div id="comp-kdmt06di" class="_1vNJf">
                                                            <div id="bgLayers_comp-kdmt06di" data-hook="bgLayers" class="_3wnIc">
                                                                <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                                                                <div id="bgMedia_comp-kdmt06di" class="_2GUhU"></div>
                                                            </div>
                                                            <div data-mesh-id="comp-kdmt06diinlineContent" data-testid="inline-content" class="">
                                                                <div data-mesh-id="comp-kdmt06diinlineContent-gridContainer" data-testid="mesh-container-content">
                                                                    <div id="comp-kdmswlbk" class="_2Hij5 _3bcaz" data-testid="richTextElement">
                                                                        <h2 class="font_2" style="line-height:normal; font-size:20px;">Agriculture work, crop farming jobs in Australia.</h2>
                                                                    </div>
                                                                    <div id="comp-kdmt5r6h" class="">
                                                                        <div data-testid="svgRoot-comp-kdmt5r6h" class="_3bLYT _2OIRR">
                                                                            <!--?xml version="1.0" encoding="UTF-8"?-->
                                                                            <svg preserveAspectRatio="xMidYMid meet" data-bbox="23 84.5 154 30.998" viewBox="23 84.5 154 30.998" height="200" width="200" xmlns="http://www.w3.org/2000/svg" data-type="color" role="presentation" aria-hidden="true">
                                                                                <defs>
                                                                                    <style>#comp-kdmt5r6h svg [data-color="1"] {fill: #B3B3B3;}</style>
                                                                                </defs>
                                                                                <g>
                                                                                    <path d="M176.86 92.328a1.85 1.85 0 0 0-1.71-1.143H39.619l-.004.001c-.793-3.812-4.177-6.686-8.218-6.686-4.629 0-8.397 3.767-8.397 8.397 0 4.632 3.768 8.399 8.396 8.399 3.946 0 7.244-2.748 8.137-6.425.03.001.055.017.085.017h36.313l-5.234 5.236a1.852 1.852 0 0 0 2.618 2.619l7.852-7.855h89.513l-14.529 14.534c-3.912 3.907-10.03 2.568-14.807-.913l6.957-2.062a1.852 1.852 0 0 0-1.049-3.552l-10.219 3.029a1.845 1.845 0 0 0-1.269 1.32 1.842 1.842 0 0 0 .485 1.765c3.818 3.82 9.083 6.489 14.044 6.489 3.084 0 6.049-1.031 8.476-3.458l17.689-17.696a1.854 1.854 0 0 0 .402-2.016zM31.396 97.593c-2.589 0-4.693-2.107-4.693-4.695s2.104-4.693 4.693-4.693 4.693 2.105 4.693 4.693-2.104 4.695-4.693 4.695z" fill="#0C1439" data-color="1"></path>
                                                                                </g>
                                                                            </svg>
                                                                        </div>
                                                                    </div>
                                                                    <div id="comp-kdmt39ab" class="_2Hij5 _3bcaz" data-testid="richTextElement">
                                                                        <p class="font_8" style="font-size:14px; line-height:1.4em;"><span style="letter-spacing:normal;"><span style="font-size:14px;"><span style="font-weight:bold;"><span style="font-family:barlow-extralight,barlow,sans-serif;">We can help you find some interesting and well-paid jobs, like supermarket workers or construction workers. And we'll provide you with all the information about those meat factories, like living conditions, salary and working hours.</span></span></span></span></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                                <!-- Agriculture work -->

                                                <!-- images section area -->

                                                <div id="comp-kdmt6lj8">
                                                    <div class="comp-kdmt6lj8">
                                                        <style> .nav-arrows-container .custom-nav-arrows svg{width:100%;height:100%}
                                                        </style>
                                                        <style>.comp-kdmt6lj8 div.pro-gallery-parent-container .gallery-item-wrapper-text .gallery-item-content{background-color:#141414}.comp-kdmt6lj8 div.pro-gallery-parent-container .show-more-container i.show-more{color:rgba(0, 0, 0, 0.7)}.comp-kdmt6lj8 div.pro-gallery-parent-container .show-more-container button.show-more{--loadMoreButtonBorderRadius: 0;--loadMoreButtonBorderColor: #141414;--loadMoreButtonBorderWidth: 1;--loadMoreButtonColor: #FFFFFF;--loadMoreButtonFont: normal normal bold 18px/1.4em barlow-extralight,barlow,sans-serif;--loadMoreButtonFontColor: #141414;color:rgba(20, 20, 20, 0.9);font:normal normal bold 18px/1.4em barlow-extralight,barlow,sans-serif;text-decoration: ;background:rgb(255, 255, 255);border-width:1px;border-color:rgb(20, 20, 20);border-radius:0px}.comp-kdmt6lj8 div.pro-gallery-parent-container .show-more-container.pro-gallery-mobile-indicator i.show-more{color:rgba(0, 0, 0, 0.7)}.comp-kdmt6lj8 div.pro-gallery-parent-container .show-more-container.pro-gallery-mobile-indicator button.show-more{--loadMoreButtonBorderRadius: 0;--loadMoreButtonBorderColor: #141414;--loadMoreButtonBorderWidth: 1;--loadMoreButtonColor: #FFFFFF;--loadMoreButtonFont: normal normal bold 18px/1.4em barlow-extralight,barlow,sans-serif;--loadMoreButtonFontColor: #141414;color:rgb(20, 20, 20);font:normal normal bold 18px/22px barlow-extralight,barlow,sans-serif;text-decoration: ;background:rgb(255, 255, 255);border-width:undefinedpx;border-color:rgb(20, 20, 20);border-radius:undefinedpx}.comp-kdmt6lj8 .nav-arrows-container .slideshow-arrow,.comp-kdmt6lj8 .nav-arrows-container .custom-nav-arrows svg{--arrowsColor: #FFFFFF;fill:rgb(255, 255, 255)}.comp-kdmt6lj8 .nav-arrows-container.pro-gallery-mobile-indicator .slideshow-arrow,.comp-kdmt6lj8 .nav-arrows-container.pro-gallery-mobile-indicator .custom-nav-arrows svg{--arrowsColor: #FFFFFF;fill:rgb(255, 255, 255)}.comp-kdmt6lj8 .pro-gallery.inline-styles .auto-slideshow-counter{--itemDescriptionFontSlideshow: normal normal bold 15px/1.4em barlow-extralight,barlow,sans-serif;--itemDescriptionFontColorSlideshow: #141414;color:rgb(20, 20, 20) !important;font:normal normal normal 15px/18px helvetica-w01-light,helvetica-w02-light,sans-serif;text-decoration: }.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-wrapper .gallery-item.load-with-color:not(.image-loaded){--imageLoadingColor: #F7F7F7;background-color:rgb(179, 179, 179)}.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-wrapper .gallery-item.gallery-item-video i.gallery-item-video-play-triangle{--itemFontColor: #FFFFFF;color:rgb(20, 20, 20)}.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-wrapper .gallery-item.gallery-item-video i.gallery-item-video-play-background{--itemOpacity: #141414;color:rgba(247, 247, 247, 0.3)}.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-wrapper .gallery-slideshow-info svg .gallery-item-svg-background,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-wrapper .gallery-item-hover svg .gallery-item-svg-background{--itemOpacity: #141414;fill:rgba(247, 247, 247, 0.3)}.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-wrapper .gallery-slideshow-info .gradient-top,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-wrapper .gallery-item-hover .gradient-top{--itemOpacity: #141414;background:linear-gradient(rgba(247, 247, 247, 0.3) 0, transparent 140px) !important}.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-wrapper .gallery-slideshow-info{--itemIconColorSlideshow: #141414}.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-wrapper .gallery-slideshow-info i:not(.pro-gallery-loved):not(.info-element-loved),.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-wrapper .gallery-slideshow-info button:not(.pro-gallery-loved):not(.info-element-loved):not(.info-element-custom-button-button):not(.artstore-add-to-cart-button),.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-wrapper .gallery-slideshow-info a{color:rgb(20, 20, 20) !important}.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-wrapper .gallery-slideshow-info svg .gallery-item-svg-foreground{fill:rgb(20, 20, 20) !important}.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-wrapper .gallery-slideshow-info .gallery-item-title,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-wrapper .gallery-slideshow-info .info-element-title{--itemFontSlideshow: normal normal bold 66px/1.4em avenir-lt-w01_35-light1475496,avenir-lt-w05_35-light,sans-serif;--itemFontColorSlideshow: #141414;color:rgb(20, 20, 20) !important;font:normal normal normal 22px/1.4em barlow-medium,barlow,sans-serif !important;text-decoration: }.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-wrapper .gallery-slideshow-info .gallery-item-description,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-wrapper .gallery-slideshow-info .info-element-description{--itemDescriptionFontSlideshow: normal normal bold 15px/1.4em barlow-extralight,barlow,sans-serif;--itemDescriptionFontColorSlideshow: #141414;color:rgb(20, 20, 20) !important;font:normal normal normal 15px/18px helvetica-w01-light,helvetica-w02-light,sans-serif !important;text-decoration: }.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-wrapper .gallery-item-hover{--itemIconColor: #FFFFFF}.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-wrapper .gallery-item-hover i:not(.pro-gallery-loved):not(.info-element-loved),.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-wrapper .gallery-item-hover button:not(.pro-gallery-loved):not(.info-element-loved):not(.info-element-custom-button-button),.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-wrapper .gallery-item-hover a{color:rgb(20, 20, 20) !important}.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-wrapper .gallery-item-hover svg .gallery-item-svg-foreground{fill:rgb(20, 20, 20) !important}.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-wrapper .gallery-item-hover .gallery-item-title,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-wrapper .gallery-item-hover .info-element-title{--itemFont: normal normal bold 66px/1.4em avenir-lt-w01_35-light1475496,avenir-lt-w05_35-light,sans-serif;--itemFontColor: #FFFFFF;color:rgb(20, 20, 20);font:normal normal bold 22px/27px avenir-lt-w01_35-light1475496,avenir-lt-w05_35-light,sans-serif;text-decoration: }.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-wrapper .gallery-item-hover .gallery-item-description,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-wrapper .gallery-item-hover .info-element-description{--itemDescriptionFont: normal normal bold 15px/1.4em barlow-extralight,barlow,sans-serif;--itemDescriptionFontColor: #FFFFFF;color:rgb(20, 20, 20) !important;font:normal normal bold 16px/20px barlow-extralight,barlow,sans-serif;text-decoration: }.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-wrapper .gallery-item-hover .custom-button-wrapper,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-wrapper .gallery-item-hover .info-element-custom-button-wrapper{--customButtonFontColor: #FFFFFF;color:rgb(255, 255, 255) !important}.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-wrapper .gallery-item-hover .custom-button-wrapper button,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-wrapper .gallery-item-hover .info-element-custom-button-wrapper button{--customButtonColor: #141414;--customButtonBorderRadius: 0;--customButtonBorderWidth: 1;--customButtonBorderColor: #FFFFFF;--customButtonFont: normal normal bold 18px/1.4em barlow-extralight,barlow,sans-serif;color:rgb(255, 255, 255) !important;font:normal normal bold 15px/18px barlow-extralight,barlow,sans-serif;text-decoration: ;background:rgba(255, 255, 255, 0) !important;border-width:1px;border-radius:0px;border-color:rgb(255, 255, 255)}.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container:not(.invert-hover) .gallery-item-hover:before{--itemOpacity: #141414;background:rgba(247, 247, 247, 0.3) !important}.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container:not(.invert-hover) .gallery-item-hover.default.force-hover:before,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container:not(.invert-hover):hover .gallery-item-hover.default:not(.hide-hover):before{background:#141414 !important}.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.invert-hover .gallery-item-hover:before{--itemOpacity: #141414;background:rgba(247, 247, 247, 0.3) !important}.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.invert-hover .gallery-item-hover.default.force-hover:before,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.invert-hover:hover .gallery-item-hover.default:not(.hide-hover):before{background:#141414 !important}.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-bottom-info .gallery-item-title,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-bottom-info .info-element-title,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-top-info .gallery-item-title,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-top-info .info-element-title,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-left-info .gallery-item-title,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-left-info .info-element-title,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-right-info .gallery-item-title,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-right-info .info-element-title,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-slideshow-info .gallery-item-title,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-slideshow-info .info-element-title{--itemFontSlideshow: normal normal bold 22px/1.4em avenir-lt-w01_35-light1475496,avenir-lt-w05_35-light,sans-serif;--itemFontColorSlideshow: #141414;color:rgb(20, 20, 20) !important;font:normal normal normal 22px/1.4em barlow-medium,barlow,sans-serif !important;text-decoration: }.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-bottom-info .gallery-item-description,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-bottom-info .info-element-description,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-top-info .gallery-item-description,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-top-info .info-element-description,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-left-info .gallery-item-description,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-left-info .info-element-description,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-right-info .gallery-item-description,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-right-info .info-element-description,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-slideshow-info .gallery-item-description,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-slideshow-info .info-element-description{--itemDescriptionFontColorSlideshow: #141414;--itemDescriptionFontSlideshow: normal normal bold 15px/1.4em barlow-extralight,barlow,sans-serif;color:rgb(20, 20, 20) !important;font:normal normal normal 15px/18px helvetica-w01-light,helvetica-w02-light,sans-serif !important;text-decoration: }.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-text .custom-button-wrapper button,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-text .info-element-custom-button-wrapper button,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-bottom-info .custom-button-wrapper button,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-bottom-info .info-element-custom-button-wrapper button,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-top-info .custom-button-wrapper button,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-top-info .info-element-custom-button-wrapper button,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-left-info .custom-button-wrapper button,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-left-info .info-element-custom-button-wrapper button,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-right-info .custom-button-wrapper button,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-item-right-info .info-element-custom-button-wrapper button,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-slideshow-info .custom-button-wrapper button,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container .gallery-slideshow-info .info-element-custom-button-wrapper button{--customButtonFontForHover: normal normal bold 18px/1.4em barlow-extralight,barlow,sans-serif;--customButtonFontColorForHover: #141414;--externalCustomButtonBorderWidth: 0;--externalCustomButtonBorderRadius: 0;font:normal normal bold 15px/18px barlow-extralight,barlow,sans-serif;text-decoration: ;color:rgb(20, 20, 20) !important;background:rgba(9, 91, 150, 0) !important;border-color:rgb(20, 20, 20);border-width:0px;border-radius:0px}.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-wrapper .gallery-item.load-with-color:not(.image-loaded){--imageLoadingColor: #F7F7F7;background-color:rgb(179, 179, 179)}.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-wrapper .gallery-item.gallery-item-video i.gallery-item-video-play-triangle{--itemFontColor: #FFFFFF;color:rgb(20, 20, 20)}.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-wrapper .gallery-item.gallery-item-video i.gallery-item-video-play-background{--itemOpacity: #141414;color:#F7F7F7 !important}.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-wrapper .gallery-slideshow-info svg .gallery-item-svg-background,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-wrapper .gallery-item-hover svg .gallery-item-svg-background{--itemOpacity: #141414;fill:#F7F7F7 !important}.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-wrapper .gallery-slideshow-info .gradient-top,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-wrapper .gallery-item-hover .gradient-top{--itemOpacity: #141414;background:linear-gradient(#F7F7F7 0, transparent 140px) !important}.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-wrapper .gallery-slideshow-info{--itemIconColorSlideshow: #141414}.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-wrapper .gallery-slideshow-info i:not(.pro-gallery-loved):not(.info-element-loved),.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-wrapper .gallery-slideshow-info button:not(.pro-gallery-loved):not(.info-element-loved):not(.info-element-custom-button-button):not(.artstore-add-to-cart-button),.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-wrapper .gallery-slideshow-info a{color:rgb(20, 20, 20) !important}.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-wrapper .gallery-slideshow-info svg .gallery-item-svg-foreground{fill:rgb(20, 20, 20) !important}.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-wrapper .gallery-slideshow-info .gallery-item-title,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-wrapper .gallery-slideshow-info .info-element-title{--itemFontSlideshow: normal normal bold 66px/1.4em avenir-lt-w01_35-light1475496,avenir-lt-w05_35-light,sans-serif;--itemFontColorSlideshow: #141414;color:rgb(20, 20, 20) !important;font:normal normal bold 14px/17px barlow-extralight,barlow,sans-serif !important;text-decoration: }.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-wrapper .gallery-slideshow-info .gallery-item-description,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-wrapper .gallery-slideshow-info .info-element-description{--itemDescriptionFontSlideshow: normal normal bold 15px/1.4em barlow-extralight,barlow,sans-serif;--itemDescriptionFontColorSlideshow: #141414;color:rgb(20, 20, 20) !important;font:normal normal normal 15px/18px helvetica-w01-light,helvetica-w02-light,sans-serif !important;text-decoration: }.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-wrapper .gallery-item-hover{--itemIconColor: #FFFFFF}.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-wrapper .gallery-item-hover i:not(.pro-gallery-loved):not(.info-element-loved),.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-wrapper .gallery-item-hover button:not(.pro-gallery-loved):not(.info-element-loved):not(.info-element-custom-button-button),.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-wrapper .gallery-item-hover a{color:rgb(255, 255, 255) !important}.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-wrapper .gallery-item-hover svg .gallery-item-svg-foreground{fill:rgb(255, 255, 255) !important}.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-wrapper .gallery-item-hover .gallery-item-title,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-wrapper .gallery-item-hover .info-element-title{--itemFont: normal normal bold 66px/1.4em avenir-lt-w01_35-light1475496,avenir-lt-w05_35-light,sans-serif;--itemFontColor: #FFFFFF;color:rgb(20, 20, 20);font:normal normal bold 30px/37px avenir-lt-w01_35-light1475496,avenir-lt-w05_35-light,sans-serif;text-decoration: }.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-wrapper .gallery-item-hover .gallery-item-description,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-wrapper .gallery-item-hover .info-element-description{--itemDescriptionFont: normal normal bold 15px/1.4em barlow-extralight,barlow,sans-serif;--itemDescriptionFontColor: #FFFFFF;color:rgb(20, 20, 20) !important;font:normal normal bold 18px/22px barlow-extralight,barlow,sans-serif;text-decoration: }.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-wrapper .gallery-item-hover .custom-button-wrapper,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-wrapper .gallery-item-hover .info-element-custom-button-wrapper{--customButtonFontColor: #FFFFFF;color:rgb(255, 255, 255) !important}.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-wrapper .gallery-item-hover .custom-button-wrapper button,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-wrapper .gallery-item-hover .info-element-custom-button-wrapper button{--customButtonColor: #141414;--customButtonBorderRadius: 0;--customButtonBorderWidth: 1;--customButtonBorderColor: #FFFFFF;--customButtonFont: normal normal bold 18px/1.4em barlow-extralight,barlow,sans-serif;color:rgb(255, 255, 255) !important;font:normal normal bold 15px/18px barlow-extralight,barlow,sans-serif;text-decoration: ;background:rgba(255, 255, 255, 0) !important;border-width:undefinedpx;border-radius:undefinedpx;border-color:rgb(255, 255, 255)}.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator:not(.invert-hover) .gallery-item-hover:not(.hide-hover):before{--itemOpacity: #141414;background:#F7F7F7 !important}.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator:not(.invert-hover) .gallery-item-hover.default.force-hover:before,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator:not(.invert-hover):hover .gallery-item-hover.default:not(.hide-hover):before{background:#141414 !important}.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator.invert-hover .gallery-item-hover:before{--itemOpacity: #141414;background:#F7F7F7 !important}.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator.invert-hover .gallery-item-hover.default.force-hover:before,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator.invert-hover:hover .gallery-item-hover.default:not(.hide-hover):before{background:#141414 !important}.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-bottom-info .gallery-item-title,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-bottom-info .info-element-title,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-top-info .gallery-item-title,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-top-info .info-element-title,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-left-info .gallery-item-title,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-left-info .info-element-title,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-right-info .gallery-item-title,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-right-info .info-element-title,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-slideshow-info .gallery-item-title,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-slideshow-info .info-element-title{--itemFontSlideshow: normal normal bold 22px/1.4em avenir-lt-w01_35-light1475496,avenir-lt-w05_35-light,sans-serif;--itemFontColorSlideshow: #141414;color:rgb(20, 20, 20) !important;font:normal normal bold 14px/17px barlow-extralight,barlow,sans-serif !important;text-decoration: }.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-bottom-info .gallery-item-description,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-bottom-info .info-element-description,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-top-info .gallery-item-description,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-top-info .info-element-description,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-left-info .gallery-item-description,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-left-info .info-element-description,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-right-info .gallery-item-description,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-right-info .info-element-description,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-slideshow-info .gallery-item-description,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-slideshow-info .info-element-description{--itemDescriptionFontColorSlideshow: #141414;--itemDescriptionFontSlideshow: normal normal bold 15px/1.4em barlow-extralight,barlow,sans-serif;color:rgb(20, 20, 20) !important;font:normal normal normal 15px/18px helvetica-w01-light,helvetica-w02-light,sans-serif !important;text-decoration: }.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-text .custom-button-wrapper button,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-text .info-element-custom-button-wrapper button,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-bottom-info .custom-button-wrapper button,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-bottom-info .info-element-custom-button-wrapper button,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-top-info .custom-button-wrapper button,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-top-info .info-element-custom-button-wrapper button,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-left-info .custom-button-wrapper button,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-left-info .info-element-custom-button-wrapper button,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-right-info .custom-button-wrapper button,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-item-right-info .info-element-custom-button-wrapper button,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-slideshow-info .custom-button-wrapper button,.comp-kdmt6lj8 .pro-gallery.inline-styles .gallery-item-container.pro-gallery-mobile-indicator .gallery-slideshow-info .info-element-custom-button-wrapper button{--customButtonFontForHover: normal normal bold 18px/1.4em barlow-extralight,barlow,sans-serif;--customButtonFontColorForHover: #141414;--externalCustomButtonBorderWidth: 0;--externalCustomButtonBorderRadius: 0;font:normal normal bold 15px/18px barlow-extralight,barlow,sans-serif;text-decoration: ;color:rgb(20, 20, 20) !important;background:rgba(9, 91, 150, 0) !important;border-color:rgb(20, 20, 20);border-width:undefinedpx;border-radius:undefinedpx}.comp-kdmt6lj8 .te-pro-gallery-text-item{font:normal normal bold 16px/1.4em barlow-extralight,barlow,sans-serif;color:#FFFFFF}.comp-kdmt6lj8 .pro-fullscreen-wrapper .pro-fullscreen-text-item{--fullscreen-text-item-bg: #141414;background-color:#141414}.comp-kdmt6lj8 .pro-fullscreen-wrapper #fullscreen-view.fullscreen-bright.pro-fullscreen-inline-styles{--bgColorExpand: #FFFFFF;background-color:rgb(255, 255, 255)}.comp-kdmt6lj8 .pro-fullscreen-wrapper #fullscreen-view.fullscreen-bright.pro-fullscreen-inline-styles .pro-fullscreen-selected-license,.comp-kdmt6lj8 .pro-fullscreen-wrapper #fullscreen-view.fullscreen-bright.pro-fullscreen-inline-styles .pro-fullscreen-checkout-link{--descriptionColorExpand: #141414;--descriptionFontExpand: normal normal bold 18px/1.4em barlow-extralight,barlow,sans-serif;color:rgb(20, 20, 20);font:normal normal bold 18px/1.4em barlow-extralight,barlow,sans-serif;text-decoration: }.comp-kdmt6lj8 .pro-fullscreen-wrapper #fullscreen-view.fullscreen-bright.pro-fullscreen-inline-styles .fullscreen-side-bar,.comp-kdmt6lj8 .pro-fullscreen-wrapper #fullscreen-view.fullscreen-bright.pro-fullscreen-inline-styles .fullscreen-bottom-mobile-info{--bgColorExpand: #FFFFFF;background-color:rgb(255, 255, 255)}.comp-kdmt6lj8 .pro-fullscreen-wrapper #fullscreen-view.fullscreen-bright.pro-fullscreen-inline-styles .fullscreen-side-bar-title{--titleColorExpand: #141414;--titleFontExpand: normal normal bold 66px/1.4em avenir-lt-w01_35-light1475496,avenir-lt-w05_35-light,sans-serif;color:rgb(20, 20, 20);font:normal normal bold 30px/1.4em avenir-lt-w01_35-light1475496,avenir-lt-w05_35-light,sans-serif;text-decoration: }.comp-kdmt6lj8 .pro-fullscreen-wrapper #fullscreen-view.fullscreen-bright.pro-fullscreen-inline-styles .fullscreen-side-bar-description,.comp-kdmt6lj8 .pro-fullscreen-wrapper #fullscreen-view.fullscreen-bright.pro-fullscreen-inline-styles .fullscreen-side-bar-description .fullscreen-side-bar-description-line,.comp-kdmt6lj8 .pro-fullscreen-wrapper #fullscreen-view.fullscreen-bright.pro-fullscreen-inline-styles .fullscreen-side-bar-exif,.comp-kdmt6lj8 .pro-fullscreen-wrapper #fullscreen-view.fullscreen-bright.pro-fullscreen-inline-styles .fullscreen-side-bar-link,.comp-kdmt6lj8 .pro-fullscreen-wrapper #fullscreen-view.fullscreen-bright.pro-fullscreen-inline-styles .fullscreen-bottom-link{--descriptionColorExpand: #141414;--descriptionFontExpand: normal normal bold 18px/1.4em barlow-extralight,barlow,sans-serif;color:rgb(20, 20, 20);font:normal normal bold 18px/1.4em barlow-extralight,barlow,sans-serif;text-decoration: }.comp-kdmt6lj8 .pro-fullscreen-wrapper #fullscreen-view.fullscreen-bright.pro-fullscreen-inline-styles .fullscreen-side-bar-description:after,.comp-kdmt6lj8 .pro-fullscreen-wrapper #fullscreen-view.fullscreen-bright.pro-fullscreen-inline-styles .fullscreen-side-bar-description .fullscreen-side-bar-description-line:after,.comp-kdmt6lj8 .pro-fullscreen-wrapper #fullscreen-view.fullscreen-bright.pro-fullscreen-inline-styles .fullscreen-side-bar-exif:after,.comp-kdmt6lj8 .pro-fullscreen-wrapper #fullscreen-view.fullscreen-bright.pro-fullscreen-inline-styles .fullscreen-side-bar-link:after,.comp-kdmt6lj8 .pro-fullscreen-wrapper #fullscreen-view.fullscreen-bright.pro-fullscreen-inline-styles .fullscreen-bottom-link:after{--descriptionColorExpand: #141414;border-color:rgb(20, 20, 20)}.comp-kdmt6lj8 .pro-fullscreen-wrapper #fullscreen-view.fullscreen-bright.pro-fullscreen-inline-styles .fullscreen-side-bar-social i:not(.pro-gallery-loved),.comp-kdmt6lj8 .pro-fullscreen-wrapper #fullscreen-view.fullscreen-bright.pro-fullscreen-inline-styles .fullscreen-side-bar-social a,.comp-kdmt6lj8 .pro-fullscreen-wrapper #fullscreen-view.fullscreen-bright.pro-fullscreen-inline-styles .fullscreen-side-bar-social button,.comp-kdmt6lj8 .pro-fullscreen-wrapper #fullscreen-view.fullscreen-bright.pro-fullscreen-inline-styles .fullscreen-nav i:not(.pro-gallery-loved),.comp-kdmt6lj8 .pro-fullscreen-wrapper #fullscreen-view.fullscreen-bright.pro-fullscreen-inline-styles .fullscreen-nav a,.comp-kdmt6lj8 .pro-fullscreen-wrapper #fullscreen-view.fullscreen-bright.pro-fullscreen-inline-styles .fullscreen-nav button,.comp-kdmt6lj8 .pro-fullscreen-wrapper #fullscreen-view.fullscreen-bright.pro-fullscreen-inline-styles .fullscreen-mobile-bar i:not(.pro-gallery-loved),.comp-kdmt6lj8 .pro-fullscreen-wrapper #fullscreen-view.fullscreen-bright.pro-fullscreen-inline-styles .fullscreen-mobile-bar a,.comp-kdmt6lj8 .pro-fullscreen-wrapper #fullscreen-view.fullscreen-bright.pro-fullscreen-inline-styles .fullscreen-mobile-bar button,.comp-kdmt6lj8 .pro-fullscreen-wrapper #fullscreen-view.fullscreen-bright.pro-fullscreen-inline-styles .fullscreen-social i:not(.pro-gallery-loved),.comp-kdmt6lj8 .pro-fullscreen-wrapper #fullscreen-view.fullscreen-bright.pro-fullscreen-inline-styles .fullscreen-social a,.comp-kdmt6lj8 .pro-fullscreen-wrapper #fullscreen-view.fullscreen-bright.pro-fullscreen-inline-styles .fullscreen-social button{--descriptionColorExpand: #141414;color:rgb(20, 20, 20)}.comp-kdmt6lj8 .pro-fullscreen-wrapper #fullscreen-view.fullscreen-bright.pro-fullscreen-inline-styles i.fullscreen-item-video-play.progallery-svg-font-icons-play-triangle{--descriptionColorExpand: #141414;color:rgb(20, 20, 20)}.comp-kdmt6lj8 .pro-fullscreen-wrapper #fullscreen-view.fullscreen-bright.pro-fullscreen-inline-styles i.fullscreen-item-video-play.progallery-svg-font-icons-play-background{--bgColorExpand: #FFFFFF;color:rgb(255, 255, 255)}.comp-kdmt6lj8 .pro-fullscreen-wrapper #fullscreen-view.fullscreen-bright.pro-fullscreen-inline-styles .fullscreen-icon{--descriptionColorExpand: #141414;--bgColorExpand: #FFFFFF;color:rgb(20, 20, 20);background:rgb(255, 255, 255)}</style>
                                                        <div id="gallery-wrapper-comp-kdmt6lj8" class="pro-gallery-component-wrapper" style="overflow: hidden; height: 100%; width: 100%;">
                                                            <style>div.comp-kdmt6lj8:not(.fullscreen-comp-wrapper) {
                                                                    width: 100%;
                                                                }
                                                            </style>
                                                            <link rel="stylesheet" href="./index_files/staticCss.min.css">
                                                            <div id="pro-gallery-comp-kdmt6lj8" class="pro-gallery">
                                                                <div data-key="pro-gallery-inner-container" class="" tabindex="-1">
                                                                    <div data-hook="css-scroll-indicator" data-scroll-base="1538" data-scroll-top="1317" class="pgscl--221 pgscl_kdmt6lj8_-40960-0 pgscl_kdmt6lj8_-20480-0 pgscl_kdmt6lj8_-10240-0 pgscl_kdmt6lj8_-5120-0 pgscl_kdmt6lj8_-2560-0 pgscl_kdmt6lj8_-1280-0 pgscl_kdmt6lj8_-640-0 pgscl_kdmt6lj8_-320-0 pgscl_kdmt6lj8_-320--160 pgscl_kdmt6lj8_-240--160 pgscl_kdmt6lj8_-240--200 pgscl_kdmt6lj8_-240--220 pgscl_kdmt6lj8_-230--220" style="display: none;"></div>
                                                                    <div class="pro-gallery-parent-container" role="region">
                                                                        <div id="pro-gallery-container-comp-kdmt6lj8" class="pro-gallery inline-styles  ltr " style="height: 558.333px; overflow-x: hidden;">
                                                                            <div id="pro-gallery-margin-container-comp-kdmt6lj8" class="pro-gallery-margin-container" style="margin: 0px; height: 558.333px; width: 1425px; overflow: visible; position: relative;">
                                                                                <a data-id="dc49adc0-e163-4ade-b9bd-f9c4e4c42594" class="item-link-wrapper" data-idx="0" data-hook="item-link-wrapper" tabindex="-1" href="http://wa.me/601125457898" target="_blank">
                                                                                    <div class="gallery-item-container item-container-regular has-custom-focus visible clickable hover-animation-fade-in zoom-in-on-hover" id="pgidc49adc0e1634adeb9bdf9c4e4c42594_0" tabindex="0" aria-label="Australia Visa Job" data-hash="dc49adc0-e163-4ade-b9bd-f9c4e4c42594" data-id="dc49adc0-e163-4ade-b9bd-f9c4e4c42594" data-idx="0" role="link" data-hook="item-container" style="overflow-y: hidden; position: absolute; inset: 0px auto auto 0px; margin: 0px; width: 338px; height: 558.333px; transition: opacity 0.2s ease 0s; opacity: 1; display: block;">
                                                                                        <div style="overflow:hidden">
                                                                                            <div data-hook="item-wrapper" class="gallery-item-wrapper visible cube-type-fill" id="item-wrapper-dc49adc0-e163-4ade-b9bd-f9c4e4c42594" style="height: 449.333px; width: 338px; margin: 0px;">
                                                                                                <div class="gallery-item-content item-content-regular image-item gallery-item-visible gallery-item gallery-item-preloaded  load-with-color image-loaded " data-hook="image-item" style="width: 338px; height: 449.333px; margin-top: 0px; margin-left: 0px;">
                                                                                                    <picture id="multi_picture_undefined">
                                                                                                        <source srcset="https://static.wixstatic.com/media/11062b_674a504f60094883bfa34ef4d7da1395~mv2.jpg/v1/fill/w_338,h_450,fp_0.31_0.3,q_75/11062b_674a504f60094883bfa34ef4d7da1395~mv2.webp 1x, https://static.wixstatic.com/media/11062b_674a504f60094883bfa34ef4d7da1395~mv2.jpg/v1/fill/w_676,h_900,fp_0.31_0.3,q_75/11062b_674a504f60094883bfa34ef4d7da1395~mv2.webp 2x, https://static.wixstatic.com/media/11062b_674a504f60094883bfa34ef4d7da1395~mv2.jpg/v1/fill/w_1014,h_1350,fp_0.31_0.3,q_75/11062b_674a504f60094883bfa34ef4d7da1395~mv2.webp 3x, https://static.wixstatic.com/media/11062b_674a504f60094883bfa34ef4d7da1395~mv2.jpg/v1/fill/w_1352,h_1800,fp_0.31_0.3,q_75/11062b_674a504f60094883bfa34ef4d7da1395~mv2.webp 4x, https://static.wixstatic.com/media/11062b_674a504f60094883bfa34ef4d7da1395~mv2.jpg/v1/fill/w_1690,h_2250,fp_0.31_0.3,q_75/11062b_674a504f60094883bfa34ef4d7da1395~mv2.webp 5x" type="image/webp">
                                                                                                        <source srcset="https://static.wixstatic.com/media/11062b_674a504f60094883bfa34ef4d7da1395~mv2.jpg/v1/fill/w_338,h_450,fp_0.31_0.3,q_75/11062b_674a504f60094883bfa34ef4d7da1395~mv2.jpg 1x, https://static.wixstatic.com/media/11062b_674a504f60094883bfa34ef4d7da1395~mv2.jpg/v1/fill/w_676,h_900,fp_0.31_0.3,q_75/11062b_674a504f60094883bfa34ef4d7da1395~mv2.jpg 2x, https://static.wixstatic.com/media/11062b_674a504f60094883bfa34ef4d7da1395~mv2.jpg/v1/fill/w_1014,h_1350,fp_0.31_0.3,q_75/11062b_674a504f60094883bfa34ef4d7da1395~mv2.jpg 3x, https://static.wixstatic.com/media/11062b_674a504f60094883bfa34ef4d7da1395~mv2.jpg/v1/fill/w_1352,h_1800,fp_0.31_0.3,q_75/11062b_674a504f60094883bfa34ef4d7da1395~mv2.jpg 4x, https://static.wixstatic.com/media/11062b_674a504f60094883bfa34ef4d7da1395~mv2.jpg/v1/fill/w_1690,h_2250,fp_0.31_0.3,q_75/11062b_674a504f60094883bfa34ef4d7da1395~mv2.jpg 5x" type="image/jpeg">
                                                                                                        <img alt="Australia Visa Job" class="gallery-item-visible gallery-item gallery-item-preloaded" data-hook="gallery-item-image-img" data-idx="0" src="https://static.wixstatic.com/media/11062b_674a504f60094883bfa34ef4d7da1395~mv2.jpg/v1/fill/w_338,h_450,fp_0.31_0.3,q_75/11062b_674a504f60094883bfa34ef4d7da1395~mv2.jpg" loading="eager" style="width: 338px; height: 449.333px;">
                                                                                                    </picture>
                                                                                                </div>
                                                                                                <div class="item-hover-flex-container" style="width: 338px; height: 449.333px; margin-top: 0px; margin-left: 0px; display: flex; position: absolute; top: 0px; left: 0px; align-items: flex-end;">
                                                                                                    <div data-hook="item-hover-0" aria-hidden="true" class="gallery-item-hover fullscreen-enabled item-overlay-regular" style="width: 338px; height: 449.333px; margin: 0px; position: relative;">
                                                                                                        <div class="gallery-item-hover-inner">
                                                                                                            <div data-hook="info-element" style="height: 100%; width: 100%;">
                                                                                                                <div class="hover-info-element" data-hook="hover-info-element" style="height: 100%; box-sizing: border-box; padding: 0px;">
                                                                                                                    <div class="info-element-social info-align-center vertical-item text-external-item info-element-social-absolute" data-hook="item-social"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="gallery-item-common-info-outer " style="height:109px;box-sizing:content-box">
                                                                                            <div style="overflow:hidden;box-sizing:border-box;width:100%;height:109px" class="gallery-item-common-info gallery-item-bottom-info">
                                                                                                <div data-hook="info-element" style="height: 100%; width: 100%; cursor: pointer;">
                                                                                                    <div data-hook="external-info-element" style="height: 100%;">
                                                                                                        <div class="info-element-text" dir="auto" style="align-items: center; text-align: center; padding: 30px 0px; box-sizing: border-box; height: 100%; direction: ltr;">
                                                                                                            <div>
                                                                                                                <div class="info-member info-element-title" data-hook="item-title" style="overflow: hidden; margin-bottom: 0px; visibility: inherit; display: -webkit-box;"><span>Agriculture Work</span></div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </a>
                                                                                <a data-id="0034d276-ca3f-406a-839f-60f5aba81e3d" class="item-link-wrapper" data-idx="1" data-hook="item-link-wrapper" tabindex="-1" href="http://wa.me/601125457898" target="_blank">
                                                                                    <div class="gallery-item-container item-container-regular has-custom-focus visible clickable hover-animation-fade-in zoom-in-on-hover" id="pgi0034d276ca3f406a839f60f5aba81e3d_1" tabindex="-1" aria-label="Australia Visa Job" data-hash="0034d276-ca3f-406a-839f-60f5aba81e3d" data-id="0034d276-ca3f-406a-839f-60f5aba81e3d" data-idx="1" role="link" data-hook="item-container" style="overflow-y: hidden; position: absolute; inset: 0px auto auto 363px; margin: 0px; width: 337px; height: 558.333px; transition: opacity 0.2s ease 0s; opacity: 1; display: block;">
                                                                                        <div style="overflow:hidden">
                                                                                            <div data-hook="item-wrapper" class="gallery-item-wrapper visible cube-type-fill" id="item-wrapper-0034d276-ca3f-406a-839f-60f5aba81e3d" style="height: 449.333px; width: 337px; margin: 0px;">
                                                                                                <div class="gallery-item-content item-content-regular image-item gallery-item-visible gallery-item gallery-item-preloaded  load-with-color image-loaded " data-hook="image-item" style="width: 337px; height: 449.333px; margin-top: 0px; margin-left: 0px;">
                                                                                                    <picture id="multi_picture_undefined">
                                                                                                        <source srcset="https://static.wixstatic.com/media/nsplsh_df6fb61c3f284b7bb012f9abc9b78dcb~mv2.jpg/v1/fill/w_337,h_450,q_75/nsplsh_df6fb61c3f284b7bb012f9abc9b78dcb~mv2.webp 1x, https://static.wixstatic.com/media/nsplsh_df6fb61c3f284b7bb012f9abc9b78dcb~mv2.jpg/v1/fill/w_674,h_900,q_75/nsplsh_df6fb61c3f284b7bb012f9abc9b78dcb~mv2.webp 2x, https://static.wixstatic.com/media/nsplsh_df6fb61c3f284b7bb012f9abc9b78dcb~mv2.jpg/v1/fill/w_1011,h_1350,q_75/nsplsh_df6fb61c3f284b7bb012f9abc9b78dcb~mv2.webp 3x, https://static.wixstatic.com/media/nsplsh_df6fb61c3f284b7bb012f9abc9b78dcb~mv2.jpg/v1/fill/w_1348,h_1800,q_75/nsplsh_df6fb61c3f284b7bb012f9abc9b78dcb~mv2.webp 4x, https://static.wixstatic.com/media/nsplsh_df6fb61c3f284b7bb012f9abc9b78dcb~mv2.jpg/v1/fill/w_1685,h_2250,q_75/nsplsh_df6fb61c3f284b7bb012f9abc9b78dcb~mv2.webp 5x" type="image/webp">
                                                                                                        <source srcset="https://static.wixstatic.com/media/nsplsh_df6fb61c3f284b7bb012f9abc9b78dcb~mv2.jpg/v1/fill/w_337,h_450,q_75/nsplsh_df6fb61c3f284b7bb012f9abc9b78dcb~mv2.jpg 1x, https://static.wixstatic.com/media/nsplsh_df6fb61c3f284b7bb012f9abc9b78dcb~mv2.jpg/v1/fill/w_674,h_900,q_75/nsplsh_df6fb61c3f284b7bb012f9abc9b78dcb~mv2.jpg 2x, https://static.wixstatic.com/media/nsplsh_df6fb61c3f284b7bb012f9abc9b78dcb~mv2.jpg/v1/fill/w_1011,h_1350,q_75/nsplsh_df6fb61c3f284b7bb012f9abc9b78dcb~mv2.jpg 3x, https://static.wixstatic.com/media/nsplsh_df6fb61c3f284b7bb012f9abc9b78dcb~mv2.jpg/v1/fill/w_1348,h_1800,q_75/nsplsh_df6fb61c3f284b7bb012f9abc9b78dcb~mv2.jpg 4x, https://static.wixstatic.com/media/nsplsh_df6fb61c3f284b7bb012f9abc9b78dcb~mv2.jpg/v1/fill/w_1685,h_2250,q_75/nsplsh_df6fb61c3f284b7bb012f9abc9b78dcb~mv2.jpg 5x" type="image/jpeg">
                                                                                                        <img alt="Australia Visa Job" class="gallery-item-visible gallery-item gallery-item-preloaded" data-hook="gallery-item-image-img" data-idx="1" src="https://static.wixstatic.com/media/nsplsh_df6fb61c3f284b7bb012f9abc9b78dcb~mv2.jpg/v1/fill/w_337,h_450,q_75/nsplsh_df6fb61c3f284b7bb012f9abc9b78dcb~mv2.jpg" loading="eager" style="width: 337px; height: 449.333px;">
                                                                                                    </picture>
                                                                                                </div>
                                                                                                <div class="item-hover-flex-container" style="width: 337px; height: 449.333px; margin-top: 0px; margin-left: 0px; display: flex; position: absolute; top: 0px; left: 0px; align-items: flex-end;">
                                                                                                    <div data-hook="item-hover-1" aria-hidden="true" class="gallery-item-hover fullscreen-enabled item-overlay-regular" style="width: 337px; height: 449.333px; margin: 0px; position: relative;">
                                                                                                        <div class="gallery-item-hover-inner">
                                                                                                            <div data-hook="info-element" style="height: 100%; width: 100%;">
                                                                                                                <div class="hover-info-element" data-hook="hover-info-element" style="height: 100%; box-sizing: border-box; padding: 0px;">
                                                                                                                    <div class="info-element-social info-align-center vertical-item text-external-item info-element-social-absolute" data-hook="item-social"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="gallery-item-common-info-outer " style="height:109px;box-sizing:content-box">
                                                                                            <div style="overflow:hidden;box-sizing:border-box;width:100%;height:109px" class="gallery-item-common-info gallery-item-bottom-info">
                                                                                                <div data-hook="info-element" style="height: 100%; width: 100%; cursor: pointer;">
                                                                                                    <div data-hook="external-info-element" style="height: 100%;">
                                                                                                        <div class="info-element-text" dir="auto" style="align-items: center; text-align: center; padding: 30px 0px; box-sizing: border-box; height: 100%; direction: ltr;">
                                                                                                            <div>
                                                                                                                <div class="info-member info-element-title" data-hook="item-title" style="overflow: hidden; margin-bottom: 0px; visibility: inherit; display: -webkit-box;"><span>Meat Factory</span></div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </a>
                                                                                <a data-id="8f51c092-afcd-457b-9c84-bb2dd9751292" class="item-link-wrapper" data-idx="2" data-hook="item-link-wrapper" tabindex="-1" href="http://wa.me/601125457898" target="_blank">
                                                                                    <div class="gallery-item-container item-container-regular has-custom-focus visible clickable hover-animation-fade-in zoom-in-on-hover" id="pgi8f51c092afcd457b9c84bb2dd9751292_2" tabindex="-1" aria-label="Australia Visa Job" data-hash="8f51c092-afcd-457b-9c84-bb2dd9751292" data-id="8f51c092-afcd-457b-9c84-bb2dd9751292" data-idx="2" role="link" data-hook="item-container" style="overflow-y: hidden; position: absolute; inset: 0px auto auto 725px; margin: 0px; width: 338px; height: 558.333px; transition: opacity 0.2s ease 0s; opacity: 1; display: block;">
                                                                                        <div style="overflow:hidden">
                                                                                            <div data-hook="item-wrapper" class="gallery-item-wrapper visible cube-type-fill" id="item-wrapper-8f51c092-afcd-457b-9c84-bb2dd9751292" style="height: 449.333px; width: 338px; margin: 0px;">
                                                                                                <div class="gallery-item-content item-content-regular image-item gallery-item-visible gallery-item gallery-item-preloaded  load-with-color image-loaded " data-hook="image-item" style="width: 338px; height: 449.333px; margin-top: 0px; margin-left: 0px;">
                                                                                                    <picture id="multi_picture_undefined">
                                                                                                        <source srcset="https://static.wixstatic.com/media/11062b_52669a954ee44f869d54e8018d13f653~mv2.jpg/v1/fill/w_338,h_450,q_75/11062b_52669a954ee44f869d54e8018d13f653~mv2.webp 1x, https://static.wixstatic.com/media/11062b_52669a954ee44f869d54e8018d13f653~mv2.jpg/v1/fill/w_676,h_900,q_75/11062b_52669a954ee44f869d54e8018d13f653~mv2.webp 2x, https://static.wixstatic.com/media/11062b_52669a954ee44f869d54e8018d13f653~mv2.jpg/v1/fill/w_1014,h_1350,q_75/11062b_52669a954ee44f869d54e8018d13f653~mv2.webp 3x, https://static.wixstatic.com/media/11062b_52669a954ee44f869d54e8018d13f653~mv2.jpg/v1/fill/w_1352,h_1800,q_75/11062b_52669a954ee44f869d54e8018d13f653~mv2.webp 4x, https://static.wixstatic.com/media/11062b_52669a954ee44f869d54e8018d13f653~mv2.jpg/v1/fill/w_1690,h_2250,q_75/11062b_52669a954ee44f869d54e8018d13f653~mv2.webp 5x" type="image/webp">
                                                                                                        <source srcset="https://static.wixstatic.com/media/11062b_52669a954ee44f869d54e8018d13f653~mv2.jpg/v1/fill/w_338,h_450,q_75/11062b_52669a954ee44f869d54e8018d13f653~mv2.jpg 1x, https://static.wixstatic.com/media/11062b_52669a954ee44f869d54e8018d13f653~mv2.jpg/v1/fill/w_676,h_900,q_75/11062b_52669a954ee44f869d54e8018d13f653~mv2.jpg 2x, https://static.wixstatic.com/media/11062b_52669a954ee44f869d54e8018d13f653~mv2.jpg/v1/fill/w_1014,h_1350,q_75/11062b_52669a954ee44f869d54e8018d13f653~mv2.jpg 3x, https://static.wixstatic.com/media/11062b_52669a954ee44f869d54e8018d13f653~mv2.jpg/v1/fill/w_1352,h_1800,q_75/11062b_52669a954ee44f869d54e8018d13f653~mv2.jpg 4x, https://static.wixstatic.com/media/11062b_52669a954ee44f869d54e8018d13f653~mv2.jpg/v1/fill/w_1690,h_2250,q_75/11062b_52669a954ee44f869d54e8018d13f653~mv2.jpg 5x" type="image/jpeg">
                                                                                                        <img alt="Australia Visa Job" class="gallery-item-visible gallery-item gallery-item-preloaded" data-hook="gallery-item-image-img" data-idx="2" src="https://static.wixstatic.com/media/11062b_52669a954ee44f869d54e8018d13f653~mv2.jpg/v1/fill/w_338,h_450,q_75/11062b_52669a954ee44f869d54e8018d13f653~mv2.jpg" loading="eager" style="width: 338px; height: 449.333px;">
                                                                                                    </picture>
                                                                                                </div>
                                                                                                <div class="item-hover-flex-container" style="width: 338px; height: 449.333px; margin-top: 0px; margin-left: 0px; display: flex; position: absolute; top: 0px; left: 0px; align-items: flex-end;">
                                                                                                    <div data-hook="item-hover-2" aria-hidden="true" class="gallery-item-hover fullscreen-enabled item-overlay-regular" style="width: 338px; height: 449.333px; margin: 0px; position: relative;">
                                                                                                        <div class="gallery-item-hover-inner">
                                                                                                            <div data-hook="info-element" style="height: 100%; width: 100%;">
                                                                                                                <div class="hover-info-element" data-hook="hover-info-element" style="height: 100%; box-sizing: border-box; padding: 0px;">
                                                                                                                    <div class="info-element-social info-align-center vertical-item text-external-item info-element-social-absolute" data-hook="item-social"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="gallery-item-common-info-outer " style="height:109px;box-sizing:content-box">
                                                                                            <div style="overflow:hidden;box-sizing:border-box;width:100%;height:109px" class="gallery-item-common-info gallery-item-bottom-info">
                                                                                                <div data-hook="info-element" style="height: 100%; width: 100%; cursor: pointer;">
                                                                                                    <div data-hook="external-info-element" style="height: 100%;">
                                                                                                        <div class="info-element-text" dir="auto" style="align-items: center; text-align: center; padding: 30px 0px; box-sizing: border-box; height: 100%; direction: ltr;">
                                                                                                            <div>
                                                                                                                <div class="info-member info-element-title" data-hook="item-title" style="overflow: hidden; margin-bottom: 0px; visibility: inherit; display: -webkit-box;"><span>Construction</span></div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </a>
                                                                                <a data-id="2a4a7031-7d03-42c6-8b0c-473e3b2ead39" class="item-link-wrapper" data-idx="3" data-hook="item-link-wrapper" tabindex="-1" href="http://wa.me/601125457898" target="_blank">
                                                                                    <div class="gallery-item-container item-container-regular has-custom-focus visible clickable hover-animation-fade-in zoom-in-on-hover" id="pgi2a4a70317d0342c68b0c473e3b2ead39_3" tabindex="-1" aria-label="Australia Visa Job" data-hash="2a4a7031-7d03-42c6-8b0c-473e3b2ead39" data-id="2a4a7031-7d03-42c6-8b0c-473e3b2ead39" data-idx="3" role="link" data-hook="item-container" style="overflow-y: hidden; position: absolute; inset: 0px auto auto 1088px; margin: 0px; width: 337px; height: 558.333px; transition: opacity 0.2s ease 0s; opacity: 1; display: block;">
                                                                                        <div style="overflow:hidden">
                                                                                            <div data-hook="item-wrapper" class="gallery-item-wrapper visible cube-type-fill" id="item-wrapper-2a4a7031-7d03-42c6-8b0c-473e3b2ead39" style="height: 449.333px; width: 337px; margin: 0px;">
                                                                                                <div class="gallery-item-content item-content-regular image-item gallery-item-visible gallery-item gallery-item-preloaded  load-with-color image-loaded " data-hook="image-item" style="width: 337px; height: 449.333px; margin-top: 0px; margin-left: 0px;">
                                                                                                    <picture id="multi_picture_undefined">
                                                                                                        <source srcset="https://static.wixstatic.com/media/5c559f4556fe4a43bfd68505146b33b3.jpg/v1/fill/w_337,h_450,fp_0.78_0.28,q_75/5c559f4556fe4a43bfd68505146b33b3.webp 1x, https://static.wixstatic.com/media/5c559f4556fe4a43bfd68505146b33b3.jpg/v1/fill/w_674,h_900,fp_0.78_0.28,q_75/5c559f4556fe4a43bfd68505146b33b3.webp 2x, https://static.wixstatic.com/media/5c559f4556fe4a43bfd68505146b33b3.jpg/v1/fill/w_1011,h_1350,fp_0.78_0.28,q_75/5c559f4556fe4a43bfd68505146b33b3.webp 3x, https://static.wixstatic.com/media/5c559f4556fe4a43bfd68505146b33b3.jpg/v1/fill/w_1348,h_1800,fp_0.78_0.28,q_75/5c559f4556fe4a43bfd68505146b33b3.webp 4x, https://static.wixstatic.com/media/5c559f4556fe4a43bfd68505146b33b3.jpg/v1/fill/w_1685,h_2250,fp_0.78_0.28,q_75/5c559f4556fe4a43bfd68505146b33b3.webp 5x" type="image/webp">
                                                                                                        <source srcset="https://static.wixstatic.com/media/5c559f4556fe4a43bfd68505146b33b3.jpg/v1/fill/w_337,h_450,fp_0.78_0.28,q_75/5c559f4556fe4a43bfd68505146b33b3.jpg 1x, https://static.wixstatic.com/media/5c559f4556fe4a43bfd68505146b33b3.jpg/v1/fill/w_674,h_900,fp_0.78_0.28,q_75/5c559f4556fe4a43bfd68505146b33b3.jpg 2x, https://static.wixstatic.com/media/5c559f4556fe4a43bfd68505146b33b3.jpg/v1/fill/w_1011,h_1350,fp_0.78_0.28,q_75/5c559f4556fe4a43bfd68505146b33b3.jpg 3x, https://static.wixstatic.com/media/5c559f4556fe4a43bfd68505146b33b3.jpg/v1/fill/w_1348,h_1800,fp_0.78_0.28,q_75/5c559f4556fe4a43bfd68505146b33b3.jpg 4x, https://static.wixstatic.com/media/5c559f4556fe4a43bfd68505146b33b3.jpg/v1/fill/w_1685,h_2250,fp_0.78_0.28,q_75/5c559f4556fe4a43bfd68505146b33b3.jpg 5x" type="image/jpeg">
                                                                                                        <img alt="Australia Visa Job" class="gallery-item-visible gallery-item gallery-item-preloaded" data-hook="gallery-item-image-img" data-idx="3" src="https://static.wixstatic.com/media/5c559f4556fe4a43bfd68505146b33b3.jpg/v1/fill/w_337,h_450,fp_0.78_0.28,q_75/5c559f4556fe4a43bfd68505146b33b3.jpg" loading="eager" style="width: 337px; height: 449.333px;">
                                                                                                    </picture>
                                                                                                </div>
                                                                                                <div class="item-hover-flex-container" style="width: 337px; height: 449.333px; margin-top: 0px; margin-left: 0px; display: flex; position: absolute; top: 0px; left: 0px; align-items: flex-end;">
                                                                                                    <div data-hook="item-hover-3" aria-hidden="true" class="gallery-item-hover fullscreen-enabled item-overlay-regular" style="width: 337px; height: 449.333px; margin: 0px; position: relative;"></div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="gallery-item-common-info-outer " style="height:109px;box-sizing:content-box">
                                                                                            <div style="overflow:hidden;box-sizing:border-box;width:100%;height:109px" class="gallery-item-common-info gallery-item-bottom-info">
                                                                                                <div data-hook="info-element" style="height: 100%; width: 100%; cursor: pointer;">
                                                                                                    <div data-hook="external-info-element" style="height: 100%;">
                                                                                                        <div class="info-element-text" dir="auto" style="align-items: center; text-align: center; padding: 30px 0px; box-sizing: border-box; height: 100%; direction: ltr;">
                                                                                                            <div>
                                                                                                                <div class="info-member info-element-title" data-hook="item-title" style="overflow: hidden; margin-bottom: 0px; visibility: inherit; display: -webkit-box;"><span>Supermarket</span></div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div data-key="items-styles" style="display: none;">
                                                                        <style id="scrollCss_0">#pro-gallery-comp-kdmt6lj8 #pgidc49adc0e1634adeb9bdf9c4e4c42594_0 {filter: opacity(0); transition: filter 1.100s ease-in !important;}.pgscl_kdmt6lj8_-1040--960 ~ div #pgidc49adc0e1634adeb9bdf9c4e4c42594_0 , .pgscl_kdmt6lj8_-960--640 ~ div #pgidc49adc0e1634adeb9bdf9c4e4c42594_0 , .pgscl_kdmt6lj8_-640-0 ~ div #pgidc49adc0e1634adeb9bdf9c4e4c42594_0 , .pgscl_kdmt6lj8_0-40960 ~ div #pgidc49adc0e1634adeb9bdf9c4e4c42594_0 , .pgscl_kdmt6lj8_40960-81920 ~ div #pgidc49adc0e1634adeb9bdf9c4e4c42594_0 {filter: opacity(1) !important;}</style>
                                                                        <style id="scrollCss_1">#pro-gallery-comp-kdmt6lj8 #pgi0034d276ca3f406a839f60f5aba81e3d_1 {filter: opacity(0); transition: filter 1.200s ease-in !important;}.pgscl_kdmt6lj8_-1040--960 ~ div #pgi0034d276ca3f406a839f60f5aba81e3d_1 , .pgscl_kdmt6lj8_-960--640 ~ div #pgi0034d276ca3f406a839f60f5aba81e3d_1 , .pgscl_kdmt6lj8_-640-0 ~ div #pgi0034d276ca3f406a839f60f5aba81e3d_1 , .pgscl_kdmt6lj8_0-40960 ~ div #pgi0034d276ca3f406a839f60f5aba81e3d_1 , .pgscl_kdmt6lj8_40960-81920 ~ div #pgi0034d276ca3f406a839f60f5aba81e3d_1 {filter: opacity(1) !important;}</style>
                                                                        <style id="scrollCss_2">#pro-gallery-comp-kdmt6lj8 #pgi8f51c092afcd457b9c84bb2dd9751292_2 {filter: opacity(0); transition: filter 1.300s ease-in !important;}.pgscl_kdmt6lj8_-1040--960 ~ div #pgi8f51c092afcd457b9c84bb2dd9751292_2 , .pgscl_kdmt6lj8_-960--640 ~ div #pgi8f51c092afcd457b9c84bb2dd9751292_2 , .pgscl_kdmt6lj8_-640-0 ~ div #pgi8f51c092afcd457b9c84bb2dd9751292_2 , .pgscl_kdmt6lj8_0-40960 ~ div #pgi8f51c092afcd457b9c84bb2dd9751292_2 , .pgscl_kdmt6lj8_40960-81920 ~ div #pgi8f51c092afcd457b9c84bb2dd9751292_2 {filter: opacity(1) !important;}</style>
                                                                        <style id="scrollCss_3">#pro-gallery-comp-kdmt6lj8 #pgi2a4a70317d0342c68b0c473e3b2ead39_3 {filter: opacity(0); transition: filter 1.100s ease-in !important;}.pgscl_kdmt6lj8_-1040--960 ~ div #pgi2a4a70317d0342c68b0c473e3b2ead39_3 , .pgscl_kdmt6lj8_-960--640 ~ div #pgi2a4a70317d0342c68b0c473e3b2ead39_3 , .pgscl_kdmt6lj8_-640-0 ~ div #pgi2a4a70317d0342c68b0c473e3b2ead39_3 , .pgscl_kdmt6lj8_0-40960 ~ div #pgi2a4a70317d0342c68b0c473e3b2ead39_3 , .pgscl_kdmt6lj8_40960-81920 ~ div #pgi2a4a70317d0342c68b0c473e3b2ead39_3 {filter: opacity(1) !important;}</style>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="grid-container" style="display:none;">
                                                            <div class="item1">
                                                                <div class="image_container">
                                                                    <div class="image_area">
                                                                        <img alt="Australia Visa Job" class="gallery-item-visible gallery-item gallery-item-preloaded" data-hook="gallery-item-image-img" data-idx="0" src="https://static.wixstatic.com/media/11062b_674a504f60094883bfa34ef4d7da1395~mv2.jpg/v1/fill/w_148,h_196,fp_0.31_0.3,q_75/11062b_674a504f60094883bfa34ef4d7da1395~mv2.jpg" loading="eager" style="width:148px;height:196px;-webkit-user-select:none;-webkit-touch-callout:none">
                                                                    </div>
                                                                    <div class="text_area">
                                                                        Agriculture Work
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item2">
                                                                <div class="image_container">
                                                                    <div class="image_area">
                                                                        <img alt="Australia Visa Job" class="gallery-item-visible gallery-item gallery-item-preloaded" data-hook="gallery-item-image-img" data-idx="0" src="https://static.wixstatic.com/media/nsplsh_df6fb61c3f284b7bb012f9abc9b78dcb~mv2.jpg/v1/fill/w_147,h_196,q_75/nsplsh_df6fb61c3f284b7bb012f9abc9b78dcb~mv2.jpg" loading="eager" style="width:148px;height:196px;-webkit-user-select:none;-webkit-touch-callout:none">
                                                                    </div>
                                                                    <div class="text_area">
                                                                        Meat Factory
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item3">
                                                                <div class="image_container">
                                                                    <div class="image_area">
                                                                        <img alt="Australia Visa Job" class="gallery-item-visible gallery-item gallery-item-preloaded" data-hook="gallery-item-image-img" data-idx="0" src="https://static.wixstatic.com/media/11062b_52669a954ee44f869d54e8018d13f653~mv2.jpg/v1/fill/w_148,h_196,q_75/11062b_52669a954ee44f869d54e8018d13f653~mv2.jpg" loading="eager" style="width:148px;height:196px;-webkit-user-select:none;-webkit-touch-callout:none">
                                                                    </div>
                                                                    <div class="text_area">
                                                                        Construction
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item4">
                                                                <div class="image_container">
                                                                    <div class="image_area">
                                                                        <img alt="Australia Visa Job" class="gallery-item-visible gallery-item gallery-item-preloaded" data-hook="gallery-item-image-img" data-idx="0" src="https://static.wixstatic.com/media/5c559f4556fe4a43bfd68505146b33b3.jpg/v1/fill/w_147,h_196,fp_0.78_0.28,q_75/5c559f4556fe4a43bfd68505146b33b3.jpg" loading="eager" style="width:148px;height:196px;-webkit-user-select:none;-webkit-touch-callout:none">
                                                                    </div>
                                                                    <div class="text_area">
                                                                        Supermarket
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="comp-kdzqkilc" class="webView _2UdPt"></div>

                                                <!-- images section area -->

                                                <!-- mida section -->

                                                <section id="comp-kfdto1n5" class="_3d64y">
                                                    <div id="bgLayers_comp-kfdto1n5" data-hook="bgLayers" class="_3wnIc">
                                                        <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                                                        <div id="bgMedia_comp-kfdto1n5" class="_2GUhU"></div>
                                                    </div>
                                                    <div data-testid="columns" class="_1uldx">
                                                        <div id="comp-kfdto1pj" class="_1vNJf">
                                                            <div id="bgLayers_comp-kfdto1pj" data-hook="bgLayers" class="_3wnIc">
                                                                <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                                                                <div id="bgMedia_comp-kfdto1pj" class="_2GUhU"></div>
                                                            </div>
                                                            <div data-mesh-id="comp-kfdto1pjinlineContent" data-testid="inline-content" class="">
                                                                <div data-mesh-id="comp-kfdto1pjinlineContent-gridContainer" data-testid="mesh-container-content">
                                                                    <div id="comp-kdzqkilc" class="_2UdPt"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                                <section id="comp-kdmtpsvt" class="webView _3d64y">
                                                    <div id="bgLayers_comp-kdmtpsvt" data-hook="bgLayers" class="_3wnIc">
                                                        <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                                                        <div id="bgMedia_comp-kdmtpsvt" class="_2GUhU"></div>
                                                    </div>
                                                    <div data-testid="columns" class="_1uldx">
                                                        <div id="comp-kdmtpsyb" class="_1vNJf">
                                                            <div id="bgLayers_comp-kdmtpsyb" data-hook="bgLayers" class="_3wnIc">
                                                                <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                                                                <div id="bgMedia_comp-kdmtpsyb" class="_2GUhU"></div>
                                                            </div>
                                                            <div data-mesh-id="comp-kdmtpsybinlineContent" data-testid="inline-content" class="">
                                                                <div data-mesh-id="comp-kdmtpsybinlineContent-gridContainer" data-testid="mesh-container-content"></div>
                                                            </div>
                                                        </div>
                                                        <div id="comp-kdmtqs9b" class="_1vNJf">
                                                            <div id="bgLayers_comp-kdmtqs9b" data-hook="bgLayers" class="_3wnIc">
                                                                <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                                                                <div id="bgMedia_comp-kdmtqs9b" class="_2GUhU">
                                                                    <wix-image id="img_comp-kdmtqs9b" class="_1-6YJ _3SQN- _2L00W _1Q5R5 bgImage" data-image-info="{&quot;containerId&quot;:&quot;comp-kdmtqs9b&quot;,&quot;targetWidth&quot;:686,&quot;targetHeight&quot;:800,&quot;alignType&quot;:&quot;center&quot;,&quot;displayMode&quot;:&quot;fill&quot;,&quot;imageData&quot;:{&quot;width&quot;:1920,&quot;height&quot;:1080,&quot;uri&quot;:&quot;4da0f3_831ad3bd70e24c5097071beb5b28a66e~mv2.jpg&quot;,&quot;displayMode&quot;:&quot;fill&quot;}}" data-has-bg-scroll-effect="" data-bg-effect-name="" data-has-ssr-src="" data-src="https://static.wixstatic.com/media/4da0f3_831ad3bd70e24c5097071beb5b28a66e~mv2.jpg/v1/fill/w_1222,h_800,al_c,q_85,usm_0.66_1.00_0.01,enc_auto/4da0f3_831ad3bd70e24c5097071beb5b28a66e~mv2.jpg"><img src="/index_files/4da0f3_831ad3bd70e24c5097071beb5b28a66e_mv2.jpg" alt="zenith-beach-38270-1920x1080.jpg" style="width: 1222px; height: 800px; object-fit: cover; object-position: 50% 50%;"></wix-image>
                                                                </div>
                                                            </div>
                                                            <div data-mesh-id="comp-kdmtqs9binlineContent" data-testid="inline-content" class="">
                                                                <div data-mesh-id="comp-kdmtqs9binlineContent-gridContainer" data-testid="mesh-container-content"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                                <section id="comp-kdmtpsvt" style="display:none" class="mobileView _3d64y">
                                                    <div id="bgLayers_comp-kdmtpsvt" data-hook="bgLayers" class="_3wnIc">
                                                        <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                                                        <div id="bgMedia_comp-kdmtpsvt" class="_2GUhU"></div>
                                                    </div>
                                                    <div data-testid="columns" class="_1uldx">
                                                        <div id="comp-kdmtqs9b" class="_1vNJf">
                                                            <div id="bgLayers_comp-kdmtqs9b" data-hook="bgLayers" class="_3wnIc">
                                                                <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                                                                <div id="bgMedia_comp-kdmtqs9b" class="_2GUhU">
                                                                    <wix-image id="img_comp-kdmtqs9b" class="_1-6YJ _3SQN- _2L00W _1Q5R5 bgImage" data-image-info="{&quot;containerId&quot;:&quot;comp-kdmtqs9b&quot;,&quot;targetWidth&quot;:320,&quot;targetHeight&quot;:400,&quot;alignType&quot;:&quot;center&quot;,&quot;displayMode&quot;:&quot;fill&quot;,&quot;imageData&quot;:{&quot;width&quot;:1920,&quot;height&quot;:1080,&quot;uri&quot;:&quot;4da0f3_831ad3bd70e24c5097071beb5b28a66e~mv2.jpg&quot;,&quot;displayMode&quot;:&quot;fill&quot;}}" data-has-bg-scroll-effect="" data-bg-effect-name="" data-has-ssr-src="" data-src="https://static.wixstatic.com/media/4da0f3_831ad3bd70e24c5097071beb5b28a66e~mv2.jpg/v1/fill/w_640,h_800,al_c,q_85,usm_0.66_1.00_0.01,enc_auto/4da0f3_831ad3bd70e24c5097071beb5b28a66e~mv2.jpg"><img src="https://static.wixstatic.com/media/4da0f3_831ad3bd70e24c5097071beb5b28a66e~mv2.jpg/v1/fill/w_640,h_800,al_c,q_85,usm_0.66_1.00_0.01,enc_auto/4da0f3_831ad3bd70e24c5097071beb5b28a66e~mv2.jpg" alt="zenith-beach-38270-1920x1080.jpg" style="width: 320px; height: 400px; object-fit: cover; object-position: 50% 50%;"></wix-image>
                                                                </div>
                                                            </div>
                                                            <div data-mesh-id="comp-kdmtqs9binlineContent" data-testid="inline-content" class="">
                                                                <div data-mesh-id="comp-kdmtqs9binlineContent-gridContainer" data-testid="mesh-container-content"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                                <section id="comp-kdmttxi5" class="webView _3d64y">
                                                    <div id="bgLayers_comp-kdmttxi5" data-hook="bgLayers" class="_3wnIc">
                                                        <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                                                        <div id="bgMedia_comp-kdmttxi5" class="_2GUhU"></div>
                                                    </div>
                                                    <div data-testid="columns" class="_1uldx">
                                                        <div id="comp-kdmttxjp" class="_1vNJf">
                                                            <div id="bgLayers_comp-kdmttxjp" data-hook="bgLayers" class="_3wnIc">
                                                                <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                                                                <div id="bgMedia_comp-kdmttxjp" class="_2GUhU"></div>
                                                            </div>
                                                            <div data-mesh-id="comp-kdmttxjpinlineContent" data-testid="inline-content" class="">
                                                                <div data-mesh-id="comp-kdmttxjpinlineContent-gridContainer" data-testid="mesh-container-content">
                                                                    <div id="comp-kdmtvktv" class="_2Hij5 _3bcaz" data-testid="richTextElement">
                                                                        <h2 class="font_2" style="line-height:normal; font-size:30px;"><span style="letter-spacing:normal;">A Unique Adventure</span></h2>
                                                                    </div>
                                                                    <div id="comp-kdmtvkua" class="">
                                                                        <div data-testid="svgRoot-comp-kdmtvkua" class="_3bLYT _2OIRR">
                                                                            <!--?xml version="1.0" encoding="UTF-8"?-->
                                                                            <svg preserveAspectRatio="xMidYMid meet" data-bbox="23 84.5 154 30.998" viewBox="23 84.5 154 30.998" height="200" width="200" xmlns="http://www.w3.org/2000/svg" data-type="color" role="presentation" aria-hidden="true">
                                                                                <defs>
                                                                                    <style>#comp-kdmtvkua svg [data-color="1"] {fill: #B3B3B3;}</style>
                                                                                </defs>
                                                                                <g>
                                                                                    <path d="M176.86 92.328a1.85 1.85 0 0 0-1.71-1.143H39.619l-.004.001c-.793-3.812-4.177-6.686-8.218-6.686-4.629 0-8.397 3.767-8.397 8.397 0 4.632 3.768 8.399 8.396 8.399 3.946 0 7.244-2.748 8.137-6.425.03.001.055.017.085.017h36.313l-5.234 5.236a1.852 1.852 0 0 0 2.618 2.619l7.852-7.855h89.513l-14.529 14.534c-3.912 3.907-10.03 2.568-14.807-.913l6.957-2.062a1.852 1.852 0 0 0-1.049-3.552l-10.219 3.029a1.845 1.845 0 0 0-1.269 1.32 1.842 1.842 0 0 0 .485 1.765c3.818 3.82 9.083 6.489 14.044 6.489 3.084 0 6.049-1.031 8.476-3.458l17.689-17.696a1.854 1.854 0 0 0 .402-2.016zM31.396 97.593c-2.589 0-4.693-2.107-4.693-4.695s2.104-4.693 4.693-4.693 4.693 2.105 4.693 4.693-2.104 4.695-4.693 4.695z" fill="#0C1439" data-color="1"></path>
                                                                                </g>
                                                                            </svg>
                                                                        </div>
                                                                    </div>
                                                                    <div id="comp-kdmtvkul" class="_2Hij5 _3bcaz" data-testid="richTextElement">
                                                                        <p class="font_8" style="font-size:18px; line-height:1.4em;"><span style="letter-spacing:normal;"><span style="font-size:18px;"><span style="font-weight:bold;"><span style="font-family:barlow-extralight,barlow,sans-serif;">We have all you need to know about how to find jobs, travel, and enjoy your great adventure! Begin to contact us for Free Job Search Guide now.</span></span></span></span></p>
                                                                    </div>
                                                                    <div class="_2UgQw" id="comp-kdmtx5uh" aria-disabled="false"><a data-testid="linkElement" href="http://wa.me/601125457898" target="_blank" rel="noreferrer noopener" class="_1fbEI" aria-disabled="false"><span class="_1Qjd7">Contact us</span></a></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div id="comp-kdmtu9be" class="_1vNJf">
                                                            <div id="bgLayers_comp-kdmtu9be" data-hook="bgLayers" class="_3wnIc">
                                                                <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                                                                <div id="bgMedia_comp-kdmtu9be" class="_2GUhU"></div>
                                                            </div>
                                                            <div data-mesh-id="comp-kdmtu9beinlineContent" data-testid="inline-content" class="">
                                                                <div data-mesh-id="comp-kdmtu9beinlineContent-gridContainer" data-testid="mesh-container-content"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                                <section id="comp-kdmttxi5" style="display:none" class="mobileView _3d64y">
                                                    <div id="bgLayers_comp-kdmttxi5" data-hook="bgLayers" class="_3wnIc">
                                                        <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                                                        <div id="bgMedia_comp-kdmttxi5" class="_2GUhU"></div>
                                                    </div>
                                                    <div data-testid="columns" class="_1uldx">
                                                        <div id="comp-kdmttxjp" class="_1vNJf">
                                                            <div id="bgLayers_comp-kdmttxjp" data-hook="bgLayers" class="_3wnIc">
                                                                <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                                                                <div id="bgMedia_comp-kdmttxjp" class="_2GUhU"></div>
                                                            </div>
                                                            <div data-mesh-id="comp-kdmttxjpinlineContent" data-testid="inline-content" class="">
                                                                <div data-mesh-id="comp-kdmttxjpinlineContent-gridContainer" data-testid="mesh-container-content">
                                                                    <div id="comp-kdmtvktv" class="_2Hij5 _3bcaz" data-testid="richTextElement">
                                                                        <h2 class="font_2" style="line-height:normal; font-size:20px; color:rgba(20,20,20,1);"><span style="letter-spacing:normal;">A Unique Adventure</span></h2>
                                                                    </div>
                                                                    <div id="comp-kdmtvkua" class="">
                                                                        <div data-testid="svgRoot-comp-kdmtvkua" class="_3bLYT _2OIRR">
                                                                            <!--?xml version="1.0" encoding="UTF-8"?-->
                                                                            <svg preserveAspectRatio="xMidYMid meet" data-bbox="23 84.5 154 30.998" viewBox="23 84.5 154 30.998" height="200" width="200" xmlns="http://www.w3.org/2000/svg" data-type="color" role="presentation" aria-hidden="true">
                                                                                <defs>
                                                                                    <style>#comp-kdmtvkua svg [data-color="1"] {fill: #B3B3B3;}</style>
                                                                                </defs>
                                                                                <g>
                                                                                    <path d="M176.86 92.328a1.85 1.85 0 0 0-1.71-1.143H39.619l-.004.001c-.793-3.812-4.177-6.686-8.218-6.686-4.629 0-8.397 3.767-8.397 8.397 0 4.632 3.768 8.399 8.396 8.399 3.946 0 7.244-2.748 8.137-6.425.03.001.055.017.085.017h36.313l-5.234 5.236a1.852 1.852 0 0 0 2.618 2.619l7.852-7.855h89.513l-14.529 14.534c-3.912 3.907-10.03 2.568-14.807-.913l6.957-2.062a1.852 1.852 0 0 0-1.049-3.552l-10.219 3.029a1.845 1.845 0 0 0-1.269 1.32 1.842 1.842 0 0 0 .485 1.765c3.818 3.82 9.083 6.489 14.044 6.489 3.084 0 6.049-1.031 8.476-3.458l17.689-17.696a1.854 1.854 0 0 0 .402-2.016zM31.396 97.593c-2.589 0-4.693-2.107-4.693-4.695s2.104-4.693 4.693-4.693 4.693 2.105 4.693 4.693-2.104 4.695-4.693 4.695z" fill="#0C1439" data-color="1"></path>
                                                                                </g>
                                                                            </svg>
                                                                        </div>
                                                                    </div>
                                                                    <div id="comp-kdmtvkul" class="_2Hij5 _3bcaz" data-testid="richTextElement">
                                                                        <p class="font_8" style="font-size:14px; line-height:1.4em;"><span style="letter-spacing:normal;"><span style="font-size:14px;"><span style="font-weight:bold;"><span style="font-family:barlow-extralight,barlow,sans-serif;">We have all you need to know about how to find jobs, travel, and enjoy your great adventure! Begin to contact us for Free Job Search Guide now.</span></span></span></span></p>
                                                                    </div>
                                                                    <div class="mobile_area_button _2UgQw" id="comp-kdmtx5uh" aria-disabled="false"><a style="right:unset !important;" data-testid="linkElement" href="http://wa.me/601125457898" target="_blank" rel="noreferrer noopener" class="_1fbEI" aria-disabled="false"><span class="_1Qjd7">Contact us</span></a></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                                <section id="comp-kfdtl2xk" class="_3d64y">
                                                    <div id="bgLayers_comp-kfdtl2xk" data-hook="bgLayers" class="_3wnIc">
                                                        <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                                                        <div id="bgMedia_comp-kfdtl2xk" class="_2GUhU"></div>
                                                    </div>
                                                    <div data-testid="columns" class="_1uldx">
                                                        <div id="comp-kfdtl30e" class="_1vNJf">
                                                            <div id="bgLayers_comp-kfdtl30e" data-hook="bgLayers" class="_3wnIc">
                                                                <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                                                                <div id="bgMedia_comp-kfdtl30e" class="_2GUhU"></div>
                                                            </div>
                                                            <div data-mesh-id="comp-kfdtl30einlineContent" data-testid="inline-content" class="">
                                                                <div data-mesh-id="comp-kfdtl30einlineContent-gridContainer" data-testid="mesh-container-content"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>

                                                <!-- mida section -->


                                                <!-- Loved by -->
                                                <section id="comp-kdmutj5y" class="webView _3d64y">
                                                    <div id="bgLayers_comp-kdmutj5y" data-hook="bgLayers" class="_3wnIc">
                                                        <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                                                        <div id="bgMedia_comp-kdmutj5y" class="_2GUhU"></div>
                                                    </div>
                                                    <div data-testid="columns" class="_1uldx">
                                                        <div id="comp-kdmutj7n" class="_1vNJf">
                                                            <div id="bgLayers_comp-kdmutj7n" data-hook="bgLayers" class="_3wnIc">
                                                                <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                                                                <div id="bgMedia_comp-kdmutj7n" class="_2GUhU">
                                                                    <wix-image id="img_comp-kdmutj7n" class="_1-6YJ _3SQN- _2L00W _1Q5R5 bgImage" data-image-info="{&quot;containerId&quot;:&quot;comp-kdmutj7n&quot;,&quot;targetWidth&quot;:980,&quot;targetHeight&quot;:529,&quot;alignType&quot;:&quot;bottom&quot;,&quot;displayMode&quot;:&quot;fill&quot;,&quot;imageData&quot;:{&quot;width&quot;:4667,&quot;height&quot;:2000,&quot;uri&quot;:&quot;0b340f_0d1ab852ddde4f09a1e244c2aa3c37a0~mv2.png&quot;,&quot;displayMode&quot;:&quot;fill&quot;}}" data-has-bg-scroll-effect="" data-bg-effect-name="" data-has-ssr-src="" data-src="https://static.wixstatic.com/media/0b340f_0d1ab852ddde4f09a1e244c2aa3c37a0~mv2.png/v1/fill/w_1905,h_529,al_b,q_90,usm_0.66_1.00_0.01,enc_auto/0b340f_0d1ab852ddde4f09a1e244c2aa3c37a0~mv2.png"><img src="/index_files/0b340f_0d1ab852ddde4f09a1e244c2aa3c37a0_mv2.png" alt="" style="width: 1905px; height: 529px; object-fit: cover; object-position: 50% 100%;"></wix-image>
                                                                </div>
                                                            </div>
                                                            <div data-mesh-id="comp-kdmutj7ninlineContent" data-testid="inline-content" class="">
                                                                <div data-mesh-id="comp-kdmutj7ninlineContent-gridContainer" data-testid="mesh-container-content">
                                                                    <div id="comp-kdmuy4bf" class="_1Q9if _3bcaz" data-testid="richTextElement">
                                                                        <h2 class="font_2" style="text-align:center; font-size:30px;">Loved by<br>
                                                                            Friends &amp; Families
                                                                        </h2>
                                                                    </div>
                                                                    <div id="comp-kdmuy4bu" class="">
                                                                        <div data-testid="svgRoot-comp-kdmuy4bu" class="_3bLYT _2OIRR">
                                                                            <!--?xml version="1.0" encoding="UTF-8"?-->
                                                                            <svg preserveAspectRatio="xMidYMid meet" data-bbox="23 84.5 154 30.998" viewBox="23 84.5 154 30.998" height="200" width="200" xmlns="http://www.w3.org/2000/svg" data-type="color" role="presentation" aria-hidden="true">
                                                                                <defs>
                                                                                    <style>#comp-kdmuy4bu svg [data-color="1"] {fill: #B3B3B3;}</style>
                                                                                </defs>
                                                                                <g>
                                                                                    <path d="M176.86 92.328a1.85 1.85 0 0 0-1.71-1.143H39.619l-.004.001c-.793-3.812-4.177-6.686-8.218-6.686-4.629 0-8.397 3.767-8.397 8.397 0 4.632 3.768 8.399 8.396 8.399 3.946 0 7.244-2.748 8.137-6.425.03.001.055.017.085.017h36.313l-5.234 5.236a1.852 1.852 0 0 0 2.618 2.619l7.852-7.855h89.513l-14.529 14.534c-3.912 3.907-10.03 2.568-14.807-.913l6.957-2.062a1.852 1.852 0 0 0-1.049-3.552l-10.219 3.029a1.845 1.845 0 0 0-1.269 1.32 1.842 1.842 0 0 0 .485 1.765c3.818 3.82 9.083 6.489 14.044 6.489 3.084 0 6.049-1.031 8.476-3.458l17.689-17.696a1.854 1.854 0 0 0 .402-2.016zM31.396 97.593c-2.589 0-4.693-2.107-4.693-4.695s2.104-4.693 4.693-4.693 4.693 2.105 4.693 4.693-2.104 4.695-4.693 4.695z" fill="#0C1439" data-color="1"></path>
                                                                                </g>
                                                                            </svg>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                                <section id="comp-kdmutj5y" style="display:none" class="mobileView _3d64y">
                                                    <div id="bgLayers_comp-kdmutj5y" data-hook="bgLayers" class="_3wnIc">
                                                        <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                                                        <div id="bgMedia_comp-kdmutj5y" class="_2GUhU"></div>
                                                    </div>
                                                    <div data-testid="columns" class="_1uldx">
                                                        <div id="comp-kdmutj7n" class="_1vNJf">
                                                            <div id="bgLayers_comp-kdmutj7n" data-hook="bgLayers" class="_3wnIc">
                                                                <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                                                                <div id="bgMedia_comp-kdmutj7n" class="_2GUhU">
                                                                    <wix-image id="img_comp-kdmutj7n" class="_1-6YJ _3SQN- _2L00W _1Q5R5 bgImage" data-image-info="{&quot;containerId&quot;:&quot;comp-kdmutj7n&quot;,&quot;targetWidth&quot;:320,&quot;targetHeight&quot;:332,&quot;alignType&quot;:&quot;bottom&quot;,&quot;displayMode&quot;:&quot;fill&quot;,&quot;imageData&quot;:{&quot;width&quot;:4667,&quot;height&quot;:2000,&quot;uri&quot;:&quot;0b340f_0d1ab852ddde4f09a1e244c2aa3c37a0~mv2.png&quot;,&quot;displayMode&quot;:&quot;fill&quot;}}" data-has-bg-scroll-effect="" data-bg-effect-name="" data-has-ssr-src="" data-src="https://static.wixstatic.com/media/0b340f_0d1ab852ddde4f09a1e244c2aa3c37a0~mv2.png/v1/fill/w_640,h_664,al_b,q_90,usm_0.66_1.00_0.01,enc_auto/0b340f_0d1ab852ddde4f09a1e244c2aa3c37a0~mv2.png"><img src="https://static.wixstatic.com/media/0b340f_0d1ab852ddde4f09a1e244c2aa3c37a0~mv2.png/v1/fill/w_640,h_664,al_b,q_90,usm_0.66_1.00_0.01,enc_auto/0b340f_0d1ab852ddde4f09a1e244c2aa3c37a0~mv2.png" alt="" style="width: 320px; height: 332px; object-fit: cover; object-position: 50% 100%;"></wix-image>
                                                                </div>
                                                            </div>
                                                            <div data-mesh-id="comp-kdmutj7ninlineContent" data-testid="inline-content" class="">
                                                                <div data-mesh-id="comp-kdmutj7ninlineContent-gridContainer" data-testid="mesh-container-content">
                                                                    <div id="comp-kdmuy4bf" class="_1Q9if _3bcaz" data-testid="richTextElement">
                                                                        <h2 class="font_2" style="text-align:center; font-size:20px; color:rgba(20,20,20,1);">Loved by<br>
                                                                            Friends &amp; Families
                                                                        </h2>
                                                                    </div>
                                                                    <div id="comp-kdmuy4bu" class="">
                                                                        <div data-testid="svgRoot-comp-kdmuy4bu" class="_3bLYT _2OIRR">
                                                                            <!--?xml version="1.0" encoding="UTF-8"?-->
                                                                            <svg preserveAspectRatio="xMidYMid meet" data-bbox="23 84.5 154 30.998" viewBox="23 84.5 154 30.998" height="200" width="200" xmlns="http://www.w3.org/2000/svg" data-type="color" role="presentation" aria-hidden="true">
                                                                                <defs>
                                                                                    <style>#comp-kdmuy4bu svg [data-color="1"] {fill: #B3B3B3;}</style>
                                                                                </defs>
                                                                                <g>
                                                                                    <path d="M176.86 92.328a1.85 1.85 0 0 0-1.71-1.143H39.619l-.004.001c-.793-3.812-4.177-6.686-8.218-6.686-4.629 0-8.397 3.767-8.397 8.397 0 4.632 3.768 8.399 8.396 8.399 3.946 0 7.244-2.748 8.137-6.425.03.001.055.017.085.017h36.313l-5.234 5.236a1.852 1.852 0 0 0 2.618 2.619l7.852-7.855h89.513l-14.529 14.534c-3.912 3.907-10.03 2.568-14.807-.913l6.957-2.062a1.852 1.852 0 0 0-1.049-3.552l-10.219 3.029a1.845 1.845 0 0 0-1.269 1.32 1.842 1.842 0 0 0 .485 1.765c3.818 3.82 9.083 6.489 14.044 6.489 3.084 0 6.049-1.031 8.476-3.458l17.689-17.696a1.854 1.854 0 0 0 .402-2.016zM31.396 97.593c-2.589 0-4.693-2.107-4.693-4.695s2.104-4.693 4.693-4.693 4.693 2.105 4.693 4.693-2.104 4.695-4.693 4.695z" fill="#0C1439" data-color="1"></path>
                                                                                </g>
                                                                            </svg>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>

                                                <div id="comp-kdmu0n5v" style="display:none" class="mobileView _2Jgim _3OF8h ignore-focus" role="region" tabindex="-1" aria-label="投影片放映">
                                                    <div data-testid="slidesWrapper" aria-live="off" class="_9R56T"><div id="comp-kdmu0n8h1" class="_30khq"><div id="bgLayers_comp-kdmu0n8h1" data-hook="bgLayers" class="_3wnIc _1FJJ6"><div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div><div id="bgMedia_comp-kdmu0n8h1" class="_2GUhU"></div></div><div class="_2uaqg"></div><div data-mesh-id="comp-kdmu0n8h1inlineContent" data-testid="inline-content" class=""><div data-mesh-id="comp-kdmu0n8h1inlineContent-gridContainer" data-testid="mesh-container-content"><div id="comp-kdmv3h7i" class="_1Q9if _3bcaz" data-testid="richTextElement"><p class="font_8" style="font-size:17px; line-height:1.4em; text-align:center; color:rgba(20,20,20,1);"><span style="font-size:17px;"><span style="font-weight:bold;"><span style="font-family:barlow-extralight,barlow,sans-serif;">I'm a testimonial. Click to edit me and add text that says something nice about you and your services.</span></span></span></p></div><div id="comp-kdmv3z41" class="_1Q9if _3bcaz" data-testid="richTextElement"><p class="font_8" style="line-height:1.4em; text-align:center; font-size:18px;"><span style="font-style:italic;">Summer, 23</span></p></div></div></div></div></div>
                                                    <nav aria-label="投影片" class="_281t-">
                                                        <ol class="_2UmXg">
                                                            <li><a href="./#comp-kdmu0n7u" aria-label="Slide  1" class="_3clGI" tabindex="0"></a></li>
                                                            <li aria-current="true"><a href="./#comp-kdmu0n8c" aria-label="Slide  2" class="_3clGI _2HGJH" tabindex="0"></a></li>
                                                            <li><a href="./#comp-kdmu0n8h1" aria-label="Slide  3" class="_3clGI" tabindex="0"></a></li>
                                                        </ol>
                                                    </nav>
                                                </div>

                                                <div id="comp-kdmu0n5v" class="webView _2Jgim _3OF8h ignore-focus" role="region" tabindex="-1" aria-label="投影片放映">
                                                    <div data-testid="slidesWrapper" aria-live="off" class="_9R56T">
                                                        <div id="comp-kdmu0n7u" class="_30khq">
                                                            <div id="bgLayers_comp-kdmu0n7u" data-hook="bgLayers" class="_3wnIc _1FJJ6">
                                                                <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                                                                <div id="bgMedia_comp-kdmu0n7u" class="_2GUhU"></div>
                                                            </div>
                                                            <div class="_2uaqg"></div>
                                                            <div data-mesh-id="comp-kdmu0n7uinlineContent" data-testid="inline-content" class="">
                                                                <div data-mesh-id="comp-kdmu0n7uinlineContent-gridContainer" data-testid="mesh-container-content">
                                                                    <div id="comp-kdmv5b7t" class="_2Hij5 _3bcaz" data-testid="richTextElement">
                                                                        <p class="font_8" style="font-size:18px; line-height:1.4em; text-align:center;"><span style="letter-spacing:normal;"><span style="font-size:18px;"><span style="font-weight:bold;"><span style="font-family:barlow-extralight,barlow,sans-serif;">Everyone agrees that we do a great job for the price. It's about the best deal around for this kind of service. It feels like you're getting it all done for you, but at a much lower cost.</span></span></span></span></p>
                                                                    </div>
                                                                    <div id="comp-kdmv5b8i" class="_2Hij5 _3bcaz" data-testid="richTextElement">
                                                                        <p class="font_8" style="line-height:1.4em; text-align:center; font-size:18px;"><span style="letter-spacing:normal;"><span style="font-style:italic;">Rick, 33</span></span></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <nav aria-label="投影片" class="_281t-">
                                                        <ol class="_2UmXg">
                                                            <li aria-current="true"><a href="https://ausvj2002.wixsite.com/#comp-kdmu0n7u" aria-label="Slide  1" class="_3clGI _2HGJH"></a></li>
                                                            <li><a href="https://ausvj2002.wixsite.com/#comp-kdmu0n8c" aria-label="Slide  2" class="_3clGI"></a></li>
                                                            <li><a href="https://ausvj2002.wixsite.com/#comp-kdmu0n8h1" aria-label="Slide  3" class="_3clGI"></a></li>
                                                        </ol>
                                                    </nav>
                                                </div>

                                                <!-- Loved by -->

                                                <!-- A unique moment without  -->

                                                <section id="comp-kdmv8oxt" class="webView _3d64y">
                                                    <div id="bgLayers_comp-kdmv8oxt" data-hook="bgLayers" class="_3wnIc">
                                                        <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                                                        <div id="bgMedia_comp-kdmv8oxt" class="_2GUhU"></div>
                                                    </div>
                                                    <div data-testid="columns" class="_1uldx">
                                                        <div id="comp-kdmv8oys3" class="_1vNJf">
                                                            <div id="bgLayers_comp-kdmv8oys3" data-hook="bgLayers" class="_3wnIc">
                                                                <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                                                                <div id="bgMedia_comp-kdmv8oys3" class="_2GUhU">
                                                                    <wix-image id="img_comp-kdmv8oys3" class="_1-6YJ _3SQN- _2L00W _1Q5R5 bgImage" data-image-info="{&quot;containerId&quot;:&quot;comp-kdmv8oys3&quot;,&quot;targetWidth&quot;:686,&quot;targetHeight&quot;:819,&quot;alignType&quot;:&quot;center&quot;,&quot;displayMode&quot;:&quot;fill&quot;,&quot;imageData&quot;:{&quot;width&quot;:1600,&quot;height&quot;:900,&quot;uri&quot;:&quot;4da0f3_07970e4f0e084fb29fcde0b14c9f39d9~mv2.jpg&quot;,&quot;displayMode&quot;:&quot;fill&quot;}}" data-has-bg-scroll-effect="" data-bg-effect-name="" data-has-ssr-src="" data-src="https://static.wixstatic.com/media/4da0f3_07970e4f0e084fb29fcde0b14c9f39d9~mv2.jpg/v1/fill/w_1222,h_819,al_c,q_85,usm_0.66_1.00_0.01,enc_auto/4da0f3_07970e4f0e084fb29fcde0b14c9f39d9~mv2.jpg"><img src="/index_files/4da0f3_07970e4f0e084fb29fcde0b14c9f39d9_mv2.jpg" alt="grapes_plants_green_leaves_nature-1235801.jpg" style="width: 1222px; height: 819px; object-fit: cover; object-position: 50% 50%;"></wix-image>
                                                                </div>
                                                            </div>
                                                            <div data-mesh-id="comp-kdmv8oys3inlineContent" data-testid="inline-content" class="">
                                                                <div data-mesh-id="comp-kdmv8oys3inlineContent-gridContainer" data-testid="mesh-container-content"></div>
                                                            </div>
                                                        </div>
                                                        <div id="comp-kdmv8oyr" class="_1vNJf">
                                                            <div id="bgLayers_comp-kdmv8oyr" data-hook="bgLayers" class="_3wnIc">
                                                                <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                                                                <div id="bgMedia_comp-kdmv8oyr" class="_2GUhU"></div>
                                                            </div>
                                                            <div data-mesh-id="comp-kdmv8oyrinlineContent" data-testid="inline-content" class="">
                                                                <div data-mesh-id="comp-kdmv8oyrinlineContent-gridContainer" data-testid="mesh-container-content"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                                <section id="comp-kdmvb0f7" class="webView _3d64y">
                                                    <div id="bgLayers_comp-kdmvb0f7" data-hook="bgLayers" class="_3wnIc">
                                                        <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                                                        <div id="bgMedia_comp-kdmvb0f7" class="_2GUhU"></div>
                                                    </div>
                                                    <div data-testid="columns" class="_1uldx">
                                                        <div id="comp-kdmvb0gi" class="_1vNJf">
                                                            <div id="bgLayers_comp-kdmvb0gi" data-hook="bgLayers" class="_3wnIc">
                                                                <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                                                                <div id="bgMedia_comp-kdmvb0gi" class="_2GUhU"></div>
                                                            </div>
                                                            <div data-mesh-id="comp-kdmvb0giinlineContent" data-testid="inline-content" class="">
                                                                <div data-mesh-id="comp-kdmvb0giinlineContent-gridContainer" data-testid="mesh-container-content"></div>
                                                            </div>
                                                        </div>
                                                        <div id="comp-kdmvb0g6" class="_1vNJf">
                                                            <div id="bgLayers_comp-kdmvb0g6" data-hook="bgLayers" class="_3wnIc">
                                                                <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                                                                <div id="bgMedia_comp-kdmvb0g6" class="_2GUhU"></div>
                                                            </div>
                                                            <div data-mesh-id="comp-kdmvb0g6inlineContent" data-testid="inline-content" class="">
                                                                <div data-mesh-id="comp-kdmvb0g6inlineContent-gridContainer" data-testid="mesh-container-content">
                                                                    <div id="comp-kdmvb0g91" class="_2Hij5 _3bcaz" data-testid="richTextElement">
                                                                        <h2 class="font_2" style="line-height:normal; font-size:30px;">A unique moment without&nbsp;<br>
                                                                            Stress
                                                                        </h2>
                                                                    </div>
                                                                    <div id="comp-kdmvb0gc" class="">
                                                                        <div data-testid="svgRoot-comp-kdmvb0gc" class="_3bLYT _2OIRR">
                                                                            <!--?xml version="1.0" encoding="UTF-8"?-->
                                                                            <svg preserveAspectRatio="xMidYMid meet" data-bbox="23 84.5 154 30.998" viewBox="23 84.5 154 30.998" height="200" width="200" xmlns="http://www.w3.org/2000/svg" data-type="color" role="presentation" aria-hidden="true">
                                                                                <defs>
                                                                                    <style>#comp-kdmvb0gc svg [data-color="1"] {fill: #B3B3B3;}</style>
                                                                                </defs>
                                                                                <g>
                                                                                    <path d="M176.86 92.328a1.85 1.85 0 0 0-1.71-1.143H39.619l-.004.001c-.793-3.812-4.177-6.686-8.218-6.686-4.629 0-8.397 3.767-8.397 8.397 0 4.632 3.768 8.399 8.396 8.399 3.946 0 7.244-2.748 8.137-6.425.03.001.055.017.085.017h36.313l-5.234 5.236a1.852 1.852 0 0 0 2.618 2.619l7.852-7.855h89.513l-14.529 14.534c-3.912 3.907-10.03 2.568-14.807-.913l6.957-2.062a1.852 1.852 0 0 0-1.049-3.552l-10.219 3.029a1.845 1.845 0 0 0-1.269 1.32 1.842 1.842 0 0 0 .485 1.765c3.818 3.82 9.083 6.489 14.044 6.489 3.084 0 6.049-1.031 8.476-3.458l17.689-17.696a1.854 1.854 0 0 0 .402-2.016zM31.396 97.593c-2.589 0-4.693-2.107-4.693-4.695s2.104-4.693 4.693-4.693 4.693 2.105 4.693 4.693-2.104 4.695-4.693 4.695z" fill="#0C1439" data-color="1"></path>
                                                                                </g>
                                                                            </svg>
                                                                        </div>
                                                                    </div>
                                                                    <div id="comp-kdmvb0gf1" class="_2Hij5 _3bcaz" data-testid="richTextElement">
                                                                        <p class="font_8" style="font-size:18px; line-height:1.4em;"><span style="letter-spacing:normal;"><span style="font-size:18px;"><span style="font-weight:bold;"><span style="font-family:barlow-extralight,barlow,sans-serif;">Vacation time to enjoy, improve your skills in the art of travel. See our website for details and apply today!</span></span></span></span></p>
                                                                    </div>
                                                                    <div class="_2UgQw" id="comp-kdmvb0gg3" aria-disabled="false"><a data-testid="linkElement" href="http://wa.me/601125457898" target="_blank" rel="noreferrer noopener" class="_1fbEI" aria-disabled="false"><span class="_1Qjd7">Contact Us</span></a></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>

                                                <section id="comp-kdmv8oxt" style="display:none;" class="mobileView _3d64y">
                                                    <div id="bgLayers_comp-kdmv8oxt" data-hook="bgLayers" class="_3wnIc">
                                                        <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                                                        <div id="bgMedia_comp-kdmv8oxt" class="_2GUhU"></div>
                                                    </div>
                                                    <div data-testid="columns" class="_1uldx">
                                                        <div id="comp-kdmv8oys3" class="_1vNJf">
                                                            <div id="bgLayers_comp-kdmv8oys3" data-hook="bgLayers" class="_3wnIc">
                                                                <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                                                                <div id="bgMedia_comp-kdmv8oys3" class="_2GUhU">
                                                                    <wix-image id="img_comp-kdmv8oys3" class="_1-6YJ _3SQN- _2L00W _1Q5R5 bgImage" data-image-info="{&quot;containerId&quot;:&quot;comp-kdmv8oys3&quot;,&quot;targetWidth&quot;:320,&quot;targetHeight&quot;:400,&quot;alignType&quot;:&quot;center&quot;,&quot;displayMode&quot;:&quot;fill&quot;,&quot;imageData&quot;:{&quot;width&quot;:1600,&quot;height&quot;:900,&quot;uri&quot;:&quot;4da0f3_07970e4f0e084fb29fcde0b14c9f39d9~mv2.jpg&quot;,&quot;displayMode&quot;:&quot;fill&quot;}}" data-has-bg-scroll-effect="" data-bg-effect-name="" data-has-ssr-src="" data-src="https://static.wixstatic.com/media/4da0f3_07970e4f0e084fb29fcde0b14c9f39d9~mv2.jpg/v1/fill/w_640,h_800,al_c,q_85,usm_0.66_1.00_0.01,enc_auto/4da0f3_07970e4f0e084fb29fcde0b14c9f39d9~mv2.jpg"><img src="https://static.wixstatic.com/media/4da0f3_07970e4f0e084fb29fcde0b14c9f39d9~mv2.jpg/v1/fill/w_640,h_800,al_c,q_85,usm_0.66_1.00_0.01,enc_auto/4da0f3_07970e4f0e084fb29fcde0b14c9f39d9~mv2.jpg" alt="grapes_plants_green_leaves_nature-1235801.jpg" style="width: 320px; height: 400px; object-fit: cover; object-position: 50% 50%;"></wix-image>
                                                                </div>
                                                            </div>
                                                            <div data-mesh-id="comp-kdmv8oys3inlineContent" data-testid="inline-content" class="">
                                                                <div data-mesh-id="comp-kdmv8oys3inlineContent-gridContainer" data-testid="mesh-container-content"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                                <section id="comp-kdmvb0f7" style="display:none;" class="mobileView _3d64y">
                                                    <div id="bgLayers_comp-kdmvb0f7" data-hook="bgLayers" class="_3wnIc">
                                                        <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                                                        <div id="bgMedia_comp-kdmvb0f7" class="_2GUhU"></div>
                                                    </div>
                                                    <div data-testid="columns" class="_1uldx">
                                                        <div id="comp-kdmvb0g6" class="_1vNJf">
                                                            <div id="bgLayers_comp-kdmvb0g6" data-hook="bgLayers" class="_3wnIc">
                                                                <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                                                                <div id="bgMedia_comp-kdmvb0g6" class="_2GUhU"></div>
                                                            </div>
                                                            <div data-mesh-id="comp-kdmvb0g6inlineContent" data-testid="inline-content" class="">
                                                                <div data-mesh-id="comp-kdmvb0g6inlineContent-gridContainer" data-testid="mesh-container-content">
                                                                    <div id="comp-kdmvb0g91" class="_2Hij5 _3bcaz" data-testid="richTextElement">
                                                                        <h2 class="font_2" style="line-height:normal; font-size:20px;">A unique moment without&nbsp;<br>
                                                                            Stress
                                                                        </h2>
                                                                    </div>
                                                                    <div id="comp-kdmvb0gc" class="">
                                                                        <div data-testid="svgRoot-comp-kdmvb0gc" class="_3bLYT _2OIRR">
                                                                            <!--?xml version="1.0" encoding="UTF-8"?-->
                                                                            <svg preserveAspectRatio="xMidYMid meet" data-bbox="23 84.5 154 30.998" viewBox="23 84.5 154 30.998" height="200" width="200" xmlns="http://www.w3.org/2000/svg" data-type="color" role="presentation" aria-hidden="true">
                                                                                <defs>
                                                                                    <style>#comp-kdmvb0gc svg [data-color="1"] {fill: #B3B3B3;}</style>
                                                                                </defs>
                                                                                <g>
                                                                                    <path d="M176.86 92.328a1.85 1.85 0 0 0-1.71-1.143H39.619l-.004.001c-.793-3.812-4.177-6.686-8.218-6.686-4.629 0-8.397 3.767-8.397 8.397 0 4.632 3.768 8.399 8.396 8.399 3.946 0 7.244-2.748 8.137-6.425.03.001.055.017.085.017h36.313l-5.234 5.236a1.852 1.852 0 0 0 2.618 2.619l7.852-7.855h89.513l-14.529 14.534c-3.912 3.907-10.03 2.568-14.807-.913l6.957-2.062a1.852 1.852 0 0 0-1.049-3.552l-10.219 3.029a1.845 1.845 0 0 0-1.269 1.32 1.842 1.842 0 0 0 .485 1.765c3.818 3.82 9.083 6.489 14.044 6.489 3.084 0 6.049-1.031 8.476-3.458l17.689-17.696a1.854 1.854 0 0 0 .402-2.016zM31.396 97.593c-2.589 0-4.693-2.107-4.693-4.695s2.104-4.693 4.693-4.693 4.693 2.105 4.693 4.693-2.104 4.695-4.693 4.695z" fill="#0C1439" data-color="1"></path>
                                                                                </g>
                                                                            </svg>
                                                                        </div>
                                                                    </div>
                                                                    <div id="comp-kdmvb0gf1" class="_2Hij5 _3bcaz" data-testid="richTextElement">
                                                                        <p class="font_8" style="font-size:14px; line-height:1.4em;"><span style="letter-spacing:normal;"><span style="font-size:14px;"><span style="font-weight:bold;"><span style="font-family:barlow-extralight,barlow,sans-serif;">Vacation time to enjoy, improve your skills in the art of travel. See our website for details and apply today!</span></span></span></span></p>
                                                                    </div>
                                                                    <div class="_2UgQw" id="comp-kdmvb0gg3" aria-disabled="false"><a data-testid="linkElement" href="http://wa.me/601125457898" target="_blank" rel="noreferrer noopener" class="_1fbEI" aria-disabled="false"><span class="_1Qjd7">Contact Us</span></a></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>

                                                <!-- A unique moment without  -->

                                                <div id="comp-kdmw87jo" class="qhwIj ignore-focus undefined" tabindex="-1" role="region" aria-label="Contact">&nbsp;</div>
                                                <section id="comp-kfdtouby" class="webView _3d64y">
                                                    <div id="bgLayers_comp-kfdtouby" data-hook="bgLayers" class="_3wnIc">
                                                        <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                                                        <div id="bgMedia_comp-kfdtouby" class="_2GUhU"></div>
                                                    </div>
                                                    <div data-testid="columns" class="_1uldx">
                                                        <div id="comp-kfdtoucu1" class="_1vNJf">
                                                            <div id="bgLayers_comp-kfdtoucu1" data-hook="bgLayers" class="_3wnIc">
                                                                <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                                                                <div id="bgMedia_comp-kfdtoucu1" class="_2GUhU"></div>
                                                            </div>
                                                            <div data-mesh-id="comp-kfdtoucu1inlineContent" data-testid="inline-content" class="">
                                                                <div data-mesh-id="comp-kfdtoucu1inlineContent-gridContainer" data-testid="mesh-container-content"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                                <div data-mesh-id="Containerc1dmpinlineContent-wedge-11"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
                <!-- footer area -->
                <div id="soapAfterPagesContainer" class="page-without-sosp">
                    <div data-mesh-id="soapAfterPagesContainerinlineContent" data-testid="inline-content" class="">
                        <div data-mesh-id="soapAfterPagesContainerinlineContent-gridContainer" data-testid="mesh-container-content">
                            <div id="CONTROLLER_COMP_CUSTOM_ID" style="display:none"></div>
                        </div>
                    </div>
                </div>
                <footer tabindex="-1" style="display:none" class="mobileView" id="SITE_FOOTER_WRAPPER">
                    <div id="pinnedBottomRight">
                        <div id="comp-ki5t1mt7-pinned-layer" class="riLfl">
                            <div id="comp-ki5t1mt7" class="_2JOHk _1DEc4"><iframe class="_49_rs" title="Wix Chat" aria-label="Wix Chat" scrolling="no" src="https://engage.wixapps.net/chat-widget-server/renderChatWidget/index?pageId=masterPage&amp;compId=comp-ki5t1mt7&amp;viewerCompId=comp-ki5t1mt7&amp;siteRevision=24&amp;viewMode=site&amp;deviceType=mobile&amp;locale=zh&amp;tz=Asia%2FKuala_Lumpur&amp;regionalLanguage=zh&amp;width=0&amp;height=0&amp;instance=ITf8zhBPpKSNljrrlHIw9_dpRzcPuMIYRUtMZcmnJqc.eyJpbnN0YW5jZUlkIjoiN2UwZDk4MmEtMjJkOS00NWMxLTgyNTctZmEwYjJhYWM0YWY0IiwiYXBwRGVmSWQiOiIxNDUxN2UxYS0zZmYwLWFmOTgtNDA4ZS0yYmQ2OTUzYzM2YTIiLCJtZXRhU2l0ZUlkIjoiYTIzMTg4NGYtZTk0Mi00NWQ4LTk5OGEtMzM0NjYwZmZiNTZiIiwic2lnbkRhdGUiOiIyMDIyLTA5LTI0VDIxOjE4OjA3Ljg1OVoiLCJkZW1vTW9kZSI6ZmFsc2UsIm9yaWdpbkluc3RhbmNlSWQiOiIxNmZiNjI3MC1hNzU0LTRmMTEtOTMwNi0zZjkwYWIxNDJjNmUiLCJhaWQiOiIxYWZjYTU3ZC0zN2ZkLTRiOGEtODhmMS03ZmFkMWI0NDNhYzEiLCJiaVRva2VuIjoiZGMzYzEwNjUtY2I5Yi0wMDE5LTFiZGQtYzk0ZDRhNTNmZjlmIiwic2l0ZU93bmVySWQiOiI0ZGEwZjM3MC1jZDk2LTRmOWItOTNlMy04YjU0MzUyMDI5YzUifQ&amp;currency=MYR&amp;currentCurrency=MYR&amp;commonConfig=%7B%22brand%22%3A%22wix%22%2C%22bsi%22%3A%2280064abf-d2f7-4160-847e-476079906653%7C1%22%2C%22BSI%22%3A%2280064abf-d2f7-4160-847e-476079906653%7C1%22%7D&amp;vsi=8ac6e26a-b346-46e6-9303-037ff82c6304" allowfullscreen="" allowtransparency="true" allowvr="true" frameborder="0" allow="autoplay;camera;microphone;geolocation;vr"></iframe></div>
                        </div>
                    </div>
                    <div id="SITE_FOOTER" class="_3Fgqs" tabindex="-1">
                        <div class="rmFV4"></div>
                        <div class="_319u9">
                            <div class="_3N4he"></div>
                            <div class="_1U65c">
                                <div data-mesh-id="SITE_FOOTERinlineContent" data-testid="inline-content" class="">
                                    <div data-mesh-id="SITE_FOOTERinlineContent-gridContainer" data-testid="mesh-container-content">
                                        <div id="comp-kdmvot8b" class="_2Hij5 _3bcaz" data-testid="richTextElement">
                                            <p class="font_9" style="line-height:1.6em; text-align:center; font-size:12px;"><span style="letter-spacing:normal;"><span class="color_15">© 2022 By Australia Visa Job</span></span></p>
                                        </div>
                                        <div id="comp-kyfn5b7m" class="XUUsC">
                                            <div data-testid="linkElement" class="xQ_iF">
                                                <wix-image id="img_comp-kyfn5b7m" class="_1-6YJ _1Fe8-" data-image-info="{&quot;containerId&quot;:&quot;comp-kyfn5b7m&quot;,&quot;targetWidth&quot;:175,&quot;targetHeight&quot;:119,&quot;displayMode&quot;:&quot;fill&quot;,&quot;imageData&quot;:{&quot;width&quot;:2232,&quot;height&quot;:1516,&quot;uri&quot;:&quot;4da0f3_4fd3ac5abcb4442e9bc31ca435cbd1e2~mv2.png&quot;,&quot;name&quot;:&quot;3311641888468_.pic.png&quot;,&quot;displayMode&quot;:&quot;fill&quot;}}" data-bg-effect-name="" data-has-ssr-src="" data-src="https://static.wixstatic.com/media/4da0f3_4fd3ac5abcb4442e9bc31ca435cbd1e2~mv2.png/v1/fill/w_350,h_238,al_c,q_85,usm_0.66_1.00_0.01,enc_auto/3311641888468__pic.png"><img src="https://static.wixstatic.com/media/4da0f3_4fd3ac5abcb4442e9bc31ca435cbd1e2~mv2.png/v1/fill/w_350,h_238,al_c,q_85,usm_0.66_1.00_0.01,enc_auto/3311641888468__pic.png" alt="3311641888468_.pic.png" style="width: 175px; height: 119px; object-fit: cover; object-position: 50% 50%;"></wix-image>
                                            </div>
                                        </div>
                                        <div id="comp-kyfy4db8" class="_2O-Ry">
                                            <ul class="xb9fU" aria-label="社交工具列">
                                                <li id="dataItem-kyfy7wbc-comp-kyfy4db8" class="_3lu8e">
                                                    <a data-testid="linkElement" href="https://www.facebook.com/ausvisajob/" target="_blank" rel="noreferrer noopener" class="_26AQd">
                                                        <wix-image id="img_0_comp-kyfy4db8" class="_1-6YJ uWpzU" data-image-info="{&quot;containerId&quot;:&quot;dataItem-kyfy7wbc-comp-kyfy4db8&quot;,&quot;displayMode&quot;:&quot;fill&quot;,&quot;imageData&quot;:{&quot;width&quot;:200,&quot;height&quot;:200,&quot;uri&quot;:&quot;e316f544f9094143b9eac01f1f19e697.png&quot;,&quot;displayMode&quot;:&quot;fill&quot;}}" data-has-bg-scroll-effect="" data-bg-effect-name="" data-has-ssr-src="" data-src="https://static.wixstatic.com/media/e316f544f9094143b9eac01f1f19e697.png/v1/fill/w_78,h_78,al_c,q_85,usm_0.66_1.00_0.01,enc_auto/e316f544f9094143b9eac01f1f19e697.png"><img alt="Facebook社交圖標" src="https://static.wixstatic.com/media/e316f544f9094143b9eac01f1f19e697.png/v1/fill/w_78,h_78,al_c,q_85,usm_0.66_1.00_0.01,enc_auto/e316f544f9094143b9eac01f1f19e697.png" style="width: 39px; height: 39px; object-fit: cover;"></wix-image>
                                                    </a>
                                                </li>
                                                <li id="dataItem-kyfy7wbc2-comp-kyfy4db8" class="_3lu8e">
                                                    <a data-testid="linkElement" href="http://wa.me/601125457898" target="_blank" rel="noreferrer noopener" class="_26AQd">
                                                        <wix-image id="img_1_comp-kyfy4db8" class="_1-6YJ uWpzU" data-image-info="{&quot;containerId&quot;:&quot;dataItem-kyfy7wbc2-comp-kyfy4db8&quot;,&quot;displayMode&quot;:&quot;fill&quot;,&quot;imageData&quot;:{&quot;width&quot;:200,&quot;height&quot;:200,&quot;uri&quot;:&quot;11062b_30e649231eb54cdea5ed586c2691a278~mv2.png&quot;,&quot;displayMode&quot;:&quot;fill&quot;}}" data-has-bg-scroll-effect="" data-bg-effect-name="" data-has-ssr-src="" data-src="https://static.wixstatic.com/media/11062b_30e649231eb54cdea5ed586c2691a278~mv2.png/v1/fill/w_78,h_78,al_c,q_85,usm_0.66_1.00_0.01,enc_auto/11062b_30e649231eb54cdea5ed586c2691a278~mv2.png"><img alt="Whatsapp    " src="https://static.wixstatic.com/media/11062b_30e649231eb54cdea5ed586c2691a278~mv2.png/v1/fill/w_78,h_78,al_c,q_85,usm_0.66_1.00_0.01,enc_auto/11062b_30e649231eb54cdea5ed586c2691a278~mv2.png" style="width: 39px; height: 39px; object-fit: cover;"></wix-image>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
                <footer tabindex="-1" class="webView" id="SITE_FOOTER_WRAPPER">
                    <div id="pinnedBottomRight">
                        <div id="comp-ki5t1mt7-pinned-layer" class="riLfl">
                            <div id="comp-ki5t1mt7" class="_2JOHk" style="width: 230px; height: 66px; content: attr(x);"><iframe class="_49_rs" title="Wix Chat" aria-label="Wix Chat" scrolling="no" src="/index_files/index.html" allowfullscreen="" allowtransparency="true" allowvr="true" frameborder="0" allow="autoplay;camera;microphone;geolocation;vr"></iframe></div>
                        </div>
                    </div>
                    <div id="SITE_FOOTER" class="_3Fgqs" tabindex="-1">
                        <div class="rmFV4"></div>
                        <div class="_319u9">
                            <div class="_3N4he"></div>
                            <div class="_1U65c">
                                <div data-mesh-id="SITE_FOOTERinlineContent" data-testid="inline-content" class="">
                                    <div data-mesh-id="SITE_FOOTERinlineContent-gridContainer" data-testid="mesh-container-content">
                                        <section id="comp-kdmvddgy" class="_3d64y">
                                            <div id="bgLayers_comp-kdmvddgy" data-hook="bgLayers" class="_3wnIc">
                                                <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                                                <div id="bgMedia_comp-kdmvddgy" class="_2GUhU"></div>
                                            </div>
                                            <div data-testid="columns" class="_1uldx">
                                                <div id="comp-kdmvddhu" class="_1vNJf">
                                                    <div id="bgLayers_comp-kdmvddhu" data-hook="bgLayers" class="_3wnIc">
                                                        <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                                                        <div id="bgMedia_comp-kdmvddhu" class="_2GUhU"></div>
                                                    </div>
                                                    <div data-mesh-id="comp-kdmvddhuinlineContent" data-testid="inline-content" class="">
                                                        <div data-mesh-id="comp-kdmvddhuinlineContent-gridContainer" data-testid="mesh-container-content">
                                                            <div id="comp-kyfn5b7m" class="XUUsC">
                                                                <div data-testid="linkElement" class="xQ_iF">
                                                                    <wix-image id="img_comp-kyfn5b7m" class="_1-6YJ _1Fe8-" data-image-info="{&quot;containerId&quot;:&quot;comp-kyfn5b7m&quot;,&quot;targetWidth&quot;:86,&quot;targetHeight&quot;:58,&quot;displayMode&quot;:&quot;fill&quot;,&quot;imageData&quot;:{&quot;width&quot;:2232,&quot;height&quot;:1516,&quot;uri&quot;:&quot;4da0f3_4fd3ac5abcb4442e9bc31ca435cbd1e2~mv2.png&quot;,&quot;name&quot;:&quot;3311641888468_.pic.png&quot;,&quot;displayMode&quot;:&quot;fill&quot;}}" data-bg-effect-name="" data-has-ssr-src="" data-src="https://static.wixstatic.com/media/4da0f3_4fd3ac5abcb4442e9bc31ca435cbd1e2~mv2.png/v1/fill/w_86,h_58,al_c,q_85,usm_0.66_1.00_0.01,enc_auto/3311641888468__pic.png"><img src="/index_files/3311641888468__pic(1).png" alt="3311641888468_.pic.png" style="width: 86px; height: 58px; object-fit: cover; object-position: 50% 50%;"></wix-image>
                                                                </div>
                                                            </div>
                                                            <div id="comp-kdmviuth" class="_2Hij5 _3bcaz" data-testid="richTextElement">
                                                                <p class="font_2" style="line-height:normal; font-size:30px;"><span style="letter-spacing:normal;"><a href="index.html" target="_self">Australia Visa Job</a></span></p>
                                                            </div>
                                                            <div id="comp-kdmvjwdk" class="_2Hij5 _3bcaz" data-testid="richTextElement">
                                                                <p class="font_8" style="line-height:normal; font-size:18px;">​Visa &amp; Job Easy</p>
                                                            </div>
                                                            <div id="comp-kdmvmk7n" class="_2O-Ry">
                                                                <ul class="xb9fU" aria-label="社交工具列">
                                                                    <li id="dataItem-kdmvmk8q1-comp-kdmvmk7n" class="_3lu8e">
                                                                        <a data-testid="linkElement" href="https://www.facebook.com/wix" target="_blank" class="_26AQd">
                                                                            <wix-image id="img_0_comp-kdmvmk7n" class="_1-6YJ uWpzU" data-image-info="{&quot;containerId&quot;:&quot;dataItem-kdmvmk8q1-comp-kdmvmk7n&quot;,&quot;displayMode&quot;:&quot;fill&quot;,&quot;imageData&quot;:{&quot;width&quot;:200,&quot;height&quot;:200,&quot;uri&quot;:&quot;0fdef751204647a3bbd7eaa2827ed4f9.png&quot;,&quot;displayMode&quot;:&quot;fill&quot;}}" data-has-bg-scroll-effect="" data-bg-effect-name="" data-has-ssr-src="" data-src="https://static.wixstatic.com/media/0fdef751204647a3bbd7eaa2827ed4f9.png/v1/fill/w_21,h_21,al_c,q_85,usm_0.66_1.00_0.01,enc_auto/0fdef751204647a3bbd7eaa2827ed4f9.png"><img alt="Facebook" style="width: 21px; height: 21px; object-fit: cover;" src="/index_files/0fdef751204647a3bbd7eaa2827ed4f9.png"></wix-image>
                                                                        </a>
                                                                    </li>
                                                                    <li id="dataItem-kdmvmk8s-comp-kdmvmk7n" class="_3lu8e">
                                                                        <a data-testid="linkElement" href="https://www.twitter.com/wix" target="_blank" class="_26AQd">
                                                                            <wix-image id="img_1_comp-kdmvmk7n" class="_1-6YJ uWpzU" data-image-info="{&quot;containerId&quot;:&quot;dataItem-kdmvmk8s-comp-kdmvmk7n&quot;,&quot;displayMode&quot;:&quot;fill&quot;,&quot;imageData&quot;:{&quot;width&quot;:200,&quot;height&quot;:200,&quot;uri&quot;:&quot;c7d035ba85f6486680c2facedecdcf4d.png&quot;,&quot;displayMode&quot;:&quot;fill&quot;}}" data-has-bg-scroll-effect="" data-bg-effect-name="" data-has-ssr-src="" data-src="https://static.wixstatic.com/media/c7d035ba85f6486680c2facedecdcf4d.png/v1/fill/w_21,h_21,al_c,q_85,usm_0.66_1.00_0.01,enc_auto/c7d035ba85f6486680c2facedecdcf4d.png"><img alt="Twitter" style="width: 21px; height: 21px; object-fit: cover;" src="/index_files/c7d035ba85f6486680c2facedecdcf4d.png"></wix-image>
                                                                        </a>
                                                                    </li>
                                                                    <li id="dataItem-kdmvmk8t3-comp-kdmvmk7n" class="_3lu8e">
                                                                        <a data-testid="linkElement" href="https://www.instagram.com/wix" target="_blank" class="_26AQd">
                                                                            <wix-image id="img_2_comp-kdmvmk7n" class="_1-6YJ uWpzU" data-image-info="{&quot;containerId&quot;:&quot;dataItem-kdmvmk8t3-comp-kdmvmk7n&quot;,&quot;displayMode&quot;:&quot;fill&quot;,&quot;imageData&quot;:{&quot;width&quot;:200,&quot;height&quot;:200,&quot;uri&quot;:&quot;01c3aff52f2a4dffa526d7a9843d46ea.png&quot;,&quot;displayMode&quot;:&quot;fill&quot;}}" data-has-bg-scroll-effect="" data-bg-effect-name="" data-has-ssr-src="" data-src="https://static.wixstatic.com/media/01c3aff52f2a4dffa526d7a9843d46ea.png/v1/fill/w_21,h_21,al_c,q_85,usm_0.66_1.00_0.01,enc_auto/01c3aff52f2a4dffa526d7a9843d46ea.png"><img alt="Instagram" style="width: 21px; height: 21px; object-fit: cover;" src="/index_files/01c3aff52f2a4dffa526d7a9843d46ea.png"></wix-image>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <div id="comp-kdmvo5ke" class="_1Q9if _3bcaz" data-testid="richTextElement">
                                                                <p class="font_7" style="font-size:22px;">Our Location</p>
                                                            </div>
                                                            <div id="comp-kdmwh2tg" class="_2Hij5 _3bcaz" data-testid="richTextElement">
                                                                <p class="font_9" style="line-height:1.6em; font-size:16px;"><span style="letter-spacing:normal;">A-2-32 3rd Floor Miri Times Square @ Marina Parkcity, 98000 Miri, Sarawak</span></p>
                                                            </div>
                                                            <div id="comp-kdmvqgp9" class="_2Hij5 _3bcaz" data-testid="richTextElement">
                                                                <p class="font_9" style="line-height:1.6em; font-size:16px;"><span style="letter-spacing:normal;">Email: <a data-auto-recognition="true" href="mailto:ausvj2002@gmail.com">ausvj2002@gmail.com</a><br>
                                                                    Tel: 6011-25457898</span>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="comp-kdmvemej" class="_1vNJf">
                                                    <div id="bgLayers_comp-kdmvemej" data-hook="bgLayers" class="_3wnIc">
                                                        <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                                                        <div id="bgMedia_comp-kdmvemej" class="_2GUhU"></div>
                                                    </div>
                                                    <div data-mesh-id="comp-kdmvemejinlineContent" data-testid="inline-content" class="">
                                                        <div data-mesh-id="comp-kdmvemejinlineContent-gridContainer" data-testid="mesh-container-content">
                                                            <div id="comp-kdmvsx7c" class="_11gHK">
                                                                <div data-mesh-id="comp-kdmvsx7cinlineContent" data-testid="inline-content" class="">
                                                                    <div data-mesh-id="comp-kdmvsx7cinlineContent-gridContainer" data-testid="mesh-container-content">
                                                                        <form id="comp-kdmvsx7w" class="yBJuM">
                                                                            <div data-mesh-id="comp-kdmvsx7winlineContent" data-testid="inline-content" class="">
                                                                                <div data-mesh-id="comp-kdmvsx7winlineContent-gridContainer" data-testid="mesh-container-content">
                                                                                    <div id="comp-kdmvsx892" class="_2Hij5 _3bcaz" data-testid="richTextElement">
                                                                                        <h2 class="font_7" style="line-height:normal; font-size:22px;"><span style="letter-spacing:normal;">Contact us</span></h2>
                                                                                    </div>
                                                                                    <div id="comp-kdmvsx8d" class="_2dBhC _2nVk2">
                                                                                        <label for="input_comp-kdmvsx8d" class="aHD7c">First Name</label>
                                                                                        <div class="XRJUI"><input type="text" name="first-name" id="input_comp-kdmvsx8d" class="_1SOvY has-custom-focus" value="" placeholder=" " aria-required="false" maxlength="100"></div>
                                                                                    </div>
                                                                                    <div id="comp-kdmvsx8j" class="_2dBhC _2nVk2">
                                                                                        <label for="input_comp-kdmvsx8j" class="aHD7c">Last Name</label>
                                                                                        <div class="XRJUI"><input type="text" name="last-name" id="input_comp-kdmvsx8j" class="_1SOvY has-custom-focus" value="" placeholder=" " aria-required="false" maxlength="100"></div>
                                                                                    </div>
                                                                                    <div id="comp-kdmvsx8l1" class="_2dBhC _2nVk2 _65cjg">
                                                                                        <label for="input_comp-kdmvsx8l1" class="aHD7c">Email</label>
                                                                                        <div class="XRJUI"><input type="email" name="email" id="input_comp-kdmvsx8l1" class="_1SOvY has-custom-focus" value="" placeholder=" " required="" aria-required="true" pattern="^.+@.+\.[a-zA-Z]{2,63}$" maxlength="250"></div>
                                                                                    </div>
                                                                                    <div id="comp-kdmvsx8n3" class="_2dBhC _2nVk2">
                                                                                        <label for="input_comp-kdmvsx8n3" class="aHD7c">Phone number</label>
                                                                                        <div class="XRJUI"><input type="tel" name="phone" id="input_comp-kdmvsx8n3" class="_1SOvY has-custom-focus" value="" placeholder=" " aria-required="false" maxlength="50"></div>
                                                                                    </div>
                                                                                    <div id="comp-kdmvsx8p4" class="bItEI _1mQNr"><label for="textarea_comp-kdmvsx8p4" class="_20uhs">Leave us a message...</label><textarea id="textarea_comp-kdmvsx8p4" class="_1VWbH has-custom-focus" placeholder=" " aria-required="false"></textarea></div>
                                                                                    <div class="_2UgQw" id="comp-kdmvsx8u" aria-disabled="false"><button aria-disabled="false" data-testid="buttonElement" class="_1fbEI"><span class="_1Qjd7">Submit</span></button></div>
                                                                                    <div id="comp-kdmvsx8x1" class="_1Q9if _3bcaz" data-testid="richTextElement">
                                                                                        <p class="font_9" style="font-size:16px;">Thanks for submitting!</p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="comp-kdmveqzu" class="_1vNJf">
                                                    <div id="bgLayers_comp-kdmveqzu" data-hook="bgLayers" class="_3wnIc">
                                                        <div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                                                        <div id="bgMedia_comp-kdmveqzu" class="_2GUhU"></div>
                                                    </div>
                                                    <div data-mesh-id="comp-kdmveqzuinlineContent" data-testid="inline-content" class="">
                                                        <div data-mesh-id="comp-kdmveqzuinlineContent-gridContainer" data-testid="mesh-container-content">
                                                            <div id="comp-kdmwbq4r" class="_1Q9if _3bcaz" data-testid="richTextElement">
                                                                <p class="font_3" style="font-size:26px;"><a href="index.html" target="_self">Home</a></p>
                                                            </div>
                                                            <!--                                                            <div id="comp-kdmwc0ek" class="_1Q9if _3bcaz" data-testid="richTextElement">-->
                                                            <!--                                                                <p class="font_3" style="font-size:26px;"><a href="about.html" target="_self">About</a></p>-->
                                                            <!--                                                            </div>-->
                                                            <!--                                                            <div id="comp-kdmwc9xh" class="_1Q9if _3bcaz" data-testid="richTextElement">-->
                                                            <!--                                                                <p class="font_3" style="font-size:26px;"><a href="charters.html" target="_self">Charters</a></p>-->
                                                            <!--                                                            </div>-->
                                                            <!--                                                            <div id="comp-kdmwd2c5" class="_2Hij5 _3bcaz" data-testid="richTextElement">-->
                                                            <!--                                                                <p class="font_3" style="line-height:normal; font-size:26px;"><span style="letter-spacing:normal;"><a href="fishingreports.html" target="_self">Visa Reports</a></span></p>-->
                                                            <!--                                                            </div>-->
                                                            <!--                                                            <div id="comp-kdmwcpui" class="_1Q9if _3bcaz" data-testid="richTextElement">-->
                                                            <!--                                                                <p class="font_3" style="font-size:26px;"><a href="gallery.html" target="_self">Gallery</a></p>-->
                                                            <!--                                                            </div>-->
                                                            <div id="comp-kdmwcpui" class="_1Q9if _3bcaz" data-testid="richTextElement">
                                                                <p class="font_3" style="font-size:26px;"><a href="/service" target="_self">Our Services</a></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        <div id="comp-kdmvot8b" class="_2Hij5 _3bcaz" data-testid="richTextElement">
                                            <p class="font_9" style="line-height:1.6em; text-align:center; font-size:16px;"><span style="letter-spacing:normal;"><span class="color_15">© 2022 By Australia Visa Job</span></span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
                <!-- footer area -->
            </div>
        </div>
        <div id="SCROLL_TO_BOTTOM" class="qhwIj ignore-focus undefined" tabindex="-1" role="region" aria-label="bottom of page">&nbsp;</div>
    </div>
</div>
<script>
    window.firstPageId = 'c1dmp'
    window.bi.sendBeat(12, 'Partially visible', {pageId: window.firstPageId})
    if (window.requestCloseWelcomeScreen) {
        window.requestCloseWelcomeScreen()
    }
    if (!window.__browser_deprecation__) {
        window.fedops.phaseStarted('partially_visible', {paramsOverrides: { pageId: firstPageId }})
    }
</script>






<!--pageHtmlEmbeds.bodyEnd start-->
<script type="wix/htmlEmbeds" id="pageHtmlEmbeds.bodyEnd start"></script>

<script type="wix/htmlEmbeds" id="pageHtmlEmbeds.bodyEnd end"></script>
<!--pageHtmlEmbeds.bodyEnd end-->


<!-- warmup data start -->
<script type="application/json" id="wix-warmup-data">{"platform":{"ssrPropsUpdates":[{"comp-kdzvucho":{"items":[{"isVisible":true,"isVisibleMobile":true,"items":[],"label":"Profile","link":{"href":"https:\/\/ausvj2002.wixsite.com\/my-site\/profile\/undefined\/profile","target":"_self","type":"DynamicPageLink"}},{"isVisible":true,"isVisibleMobile":true,"items":[],"label":"My Bookings","link":{"href":"https:\/\/ausvj2002.wixsite.com\/my-site\/account\/my-bookings","target":"_self","type":"DynamicPageLink"}},{"isVisible":true,"isVisibleMobile":true,"items":[],"label":"My Wallet","link":{"href":"https:\/\/ausvj2002.wixsite.com\/my-site\/account\/my-wallet","target":"_self","type":"DynamicPageLink"}},{"isVisible":true,"isVisibleMobile":true,"items":[],"label":"Blog Comments ","link":{"href":"https:\/\/ausvj2002.wixsite.com\/my-site\/profile\/undefined\/blog-comments","target":"_self","type":"DynamicPageLink"}},{"isVisible":true,"isVisibleMobile":true,"items":[],"label":"Blog Likes","link":{"href":"https:\/\/ausvj2002.wixsite.com\/my-site\/profile\/undefined\/blog-likes","target":"_self","type":"DynamicPageLink"}},{"isVisible":true,"isVisibleMobile":true,"items":[],"label":"My Account","link":{"href":"https:\/\/ausvj2002.wixsite.com\/my-site\/account\/my-account","target":"_self","type":"DynamicPageLink"}},{"isVisible":true,"isVisibleMobile":true,"items":[],"label":"Notifications","link":{"href":"https:\/\/ausvj2002.wixsite.com\/my-site\/account\/notifications","target":"_self","type":"DynamicPageLink"}},{"isVisible":true,"isVisibleMobile":true,"items":[],"label":"Settings","link":{"href":"https:\/\/ausvj2002.wixsite.com\/my-site\/account\/settings","target":"_self","type":"DynamicPageLink"}}]},"comp-kdmvsx8x1":{"hidden":true},"comp-kdmvsx8d":{"isValid":true,"maxLength":100},"comp-kdmvsx8j":{"isValid":true,"maxLength":100},"comp-kdmvsx8l1":{"isValid":false,"maxLength":250},"comp-kdmvsx8n3":{"isValid":true},"comp-kdmvsx8p4":{"isValid":true}}],"ssrStyleUpdates":[{"comp-kdmvsx8x1":{"visibility":"hidden !important"}}]},"appsWarmupData":{"14271d6f-ba62-d045-549b-ab972ae1f70e":{"pg_experiments":{"specs.pro-gallery.usePictureElement":"true","specs.LocalizationTranslationFlowSpec":"true","specs.pro-gallery.EnableAlbumsStorePremiumValidation":"true","specs.pro-gallery.enableFullResFeatureViewer":"true","specs.pro-gallery.enablePGRenderIndicator":"false","specs.pro-gallery.excludeFromWarmupData":"false","specs.pro-gallery.useReactionService":"true","specs.pro-gallery.excludeFromLayoutFixer":"false","specs.pro-gallery.useWarmupData":"true","specs.pro-gallery.useNewReactionsCalculation-Rollout":"true","specs.pro-gallery.finalReplaceSlideshow":"true","specs.pro-gallery.useReactPortalInArtStore":"true","specs.pro-gallery.blockOAP":"false","specs.pro-gallery.useServerBlueprints-viewer":"false","specs.pro-gallery.removeRoleApplication":"true","specs.pro-gallery.fullscreenLinksUseATag":"false","specs.pro-gallery.horizontalTitlePlacementOptionsViewer":"true","specs.pro-gallery.disableImagePreload":"true","specs.pro-gallery.blockCropOnSSR":"true","specs.pro-gallery.appSettings":"true"},"comp-kdmt6lj8_appSettings":{"pageId":"d34uk","styleId":"style-jyem87tx","upgrades":{"fullscreen":{"date":"Tue Dec 11 2018 18:15:52 GMT+0300 (Москва, стандартное время)"}},"layoutTeaserShowed":true,"galleryId":"81d3d818-fccb-4ea2-b5cc-8df65dc0ff90","originGallerySettings":null},"comp-kdmt6lj8_galleryData":{"items":[{"itemId":"dc49adc0-e163-4ade-b9bd-f9c4e4c42594","isSecure":false,"orderIndex":1596959267.6559167,"metaData":{"title":"Agriculture Work","link":{"target":"_blank","type":"wix","data":{"url":"http:\/\/wa.me\/601125457898","target":"_blank","type":"ExternalLink"},"text":"http:\/\/wa.me\/601125457898"},"alt":"Australia Visa Job","sourceName":"private","tags":["AgritectStartup_","AgritectStartup_2871"],"height":4000,"width":6000,"focalPoint":[0.31608334,0.304875],"fileName":"檢查農作物","name":"11062b_674a504f60094883bfa34ef4d7da1395~mv2.jpg"},"mediaUrl":"11062b_674a504f60094883bfa34ef4d7da1395~mv2.jpg"},{"itemId":"0034d276-ca3f-406a-839f-60f5aba81e3d","isSecure":false,"orderIndex":1596960865.397,"metaData":{"title":"Meat Factory","link":{"target":"_blank","type":"wix","data":{"url":"http:\/\/wa.me\/601125457898","target":"_blank","type":"ExternalLink"},"text":"http:\/\/wa.me\/601125457898"},"alt":"Australia Visa Job","sourceName":"private","tags":["_unsplash","_unsplash_l9vXx8aEYJ8"],"height":5792,"width":8688,"focalPoint":[0.5,0.5],"fileName":"Image by Madie Hamilton","name":"nsplsh_df6fb61c3f284b7bb012f9abc9b78dcb~mv2.jpg"},"mediaUrl":"nsplsh_df6fb61c3f284b7bb012f9abc9b78dcb~mv2.jpg"},{"itemId":"8f51c092-afcd-457b-9c84-bb2dd9751292","isSecure":false,"orderIndex":1596960865.3975,"metaData":{"title":"Construction","link":{"target":"_blank","type":"wix","data":{"url":"http:\/\/wa.me\/601125457898","target":"_blank","type":"ExternalLink"},"text":"http:\/\/wa.me\/601125457898"},"alt":"Australia Visa Job","sourceName":"private","tags":["contributors","contributors_construction"],"height":4000,"width":6000,"focalPoint":[0.5,0.5],"fileName":"水泥地板","name":"11062b_52669a954ee44f869d54e8018d13f653~mv2.jpg"},"mediaUrl":"11062b_52669a954ee44f869d54e8018d13f653~mv2.jpg"},{"itemId":"2a4a7031-7d03-42c6-8b0c-473e3b2ead39","isSecure":false,"orderIndex":1596960865.398,"metaData":{"title":"Supermarket","link":{"target":"_blank","type":"wix","data":{"url":"http:\/\/wa.me\/601125457898","target":"_blank","type":"ExternalLink"},"text":"http:\/\/wa.me\/601125457898"},"alt":"Australia Visa Job","sourceName":"private","tags":["OB_IN_ECOM_SUPERMARKET_AND_GROCERY","OB_IN_GROCERY","OB_IN_GROCERY_PARENT"],"height":3800,"width":5700,"focalPoint":[0.7895614,0.28394738],"fileName":"在超市","name":"5c559f4556fe4a43bfd68505146b33b3.jpg"},"mediaUrl":"5c559f4556fe4a43bfd68505146b33b3.jpg"}],"totalItemsCount":4}}},"ooi":{"failedInSsr":{}}}</script>
<!-- warmup data end -->


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script>
    $('#MENU_AS_CONTAINER_TOGGLE').click(function() {
        let checkClass = $('._1XkJp').hasClass('_11WHE');
        if(!checkClass) {
            $('#MENU_AS_CONTAINER').show();
            $('body').css({
                overFlow:'hidden'
            });
            $('._1XkJp').addClass("_11WHE");
        } else {
            $('#MENU_AS_CONTAINER').hide();
            $('._1XkJp').removeClass("_11WHE");
        }
    });
</script>
<style>
    .float{
        padding: 8px;
        position:fixed;
        width:35px;
        height:35px;
        bottom:40px;
        right:40px;
        background-color:#25d366;
        color:#FFF;
        border-radius:50px;
        text-align:center;
        font-size:33px;
        box-shadow: 2px 2px 3px #999;
        z-index:100;
    }

    .my-float{
        margin-top:16px;
    }
</style>
<link rel="stylesheet" href="/asset/css/font-awesome.min.css">
<a href="http://wa.me/601125457898" class="float" target="_blank">
    <img src="/images/WhatsApp.svg.png" style="width:70%;" />
</a>
</body>
</html>
