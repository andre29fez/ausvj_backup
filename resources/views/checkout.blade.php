
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">

    <title>Checkout example for Bootstrap</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/checkout/">

    <!-- Bootstrap core CSS -->
    <link href="/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Custom styles for this template -->
   <style>
       .container {
           max-width: 960px;
       }

       .border-top { border-top: 1px solid #e5e5e5; }
       .border-bottom { border-bottom: 1px solid #e5e5e5; }
       .border-top-gray { border-top-color: #adb5bd; }

       .box-shadow { box-shadow: 0 .25rem .75rem rgba(0, 0, 0, .05); }

       .lh-condensed { line-height: 1.25; }
   </style>
</head>

<body class="bg-light">

<div class="container">
    <div class="py-5 text-center">
        <img class="d-block mx-auto mb-4" src="https://static.wixstatic.com/media/4da0f3_4fd3ac5abcb4442e9bc31ca435cbd1e2~mv2.png/v1/fill/w_106,h_72,al_c,q_85,usm_0.66_1.00_0.01,enc_auto/3311641888468__pic.png" alt="" width="100" height="72">
        <h2>Checkout form</h2>
    </div>

    <div class="row">
        <div class="col-md-4 order-md-2 mb-4">
            <h4 class="d-flex justify-content-between align-items-center mb-3">
                <span class="text-muted">Your Order</span>

            </h4>
            <ul class="list-group mb-3" id="orderItem">
                <li class="list-group-item d-flex justify-content-between lh-condensed">
                    <div>
                        <h6 class="my-0">{{$product->name}}</h6>
                        <small class="text-muted">{{ $product->description }}</small><br><br>
                        @if($productDiscount != null)
                            @foreach($productDiscount as $discount)
                            <small class="text-muted">Discount {{$discount->percentage}}%, for {{$discount->qty_discount}} Qty 
                                 <button class="button btn btn-sm btn-primary" onclick="countDiscount({{$discount->qty_discount}})"> Choose </button>
                            </small>
                            <br><br>
                            @endforeach
                        @endif
                    </div>
                    <span class="text-muted">RM {{$product->price}}</span>
                </li>
                <li class="list-group-item d-flex justify-content-between lh-condensed">
                    <div>
                        <h6 class="my-0">Quantity </h6>

                    </div>
                    <input type="number" min="1" style="width: 20%" value="1" id="qty">
                </li>
                <li class="list-group-item d-flex justify-content-between">
                    <span>Total (RM)</span>
                    <strong id="total"> </strong>
                    <input type="hidden" id="total_order_price"> 
                    <input type="hidden" id="total_discount"> 
                </li>
            </ul>

            <form class="card p-2">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Promo code">
                    <div class="input-group-append">
                        <button type="submit" class="btn btn-secondary">Redeem</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-8 order-md-1">
            <h4 class="mb-3">Billing address</h4>
            <form class="needs-validation" novalidate>
                <div class="row">
                    <div class="col-md-6 mb-3">
                        <label for="firstName">First name</label>
                        <input type="text" class="form-control" id="first_name" placeholder="" value="" required>
                        <div class="invalid-feedback">
                            Valid first name is required.
                        </div>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="lastName">Last name</label>
                        <input type="text" class="form-control" id="last_name" placeholder="" value="" required>
                        <div class="invalid-feedback">
                            Valid last name is required.
                        </div>
                    </div>
                </div>

                <div class="mb-3">
                    <label for="email">Email <span class="text-muted">(Optional)</span></label>
                    <input type="email" class="form-control" id="email" placeholder="you@example.com">
                    <div class="invalid-feedback">
                        Please enter a valid email address 
                    </div>
                </div>

                <div class="mb-3">
                    <label for="phone">Phone</label>
                    <input type="number" class="form-control" id="phone_number" required>
                    <div class="invalid-feedback">
                        Please enter a valid phone
                    </div>
                </div>

                <div class="mb-3">
                    <label for="address">Address</label>
                    <input type="text" class="form-control" id="address" placeholder="1234 Main St" required>
                    <div class="invalid-feedback">
                        Please enter your address.
                    </div>
                </div>
              
                <hr class="mb-4">

                <h4 class="mb-3">Payment</h4>

                <div class="d-block my-3">
                    <div class="custom-control custom-radio">
                        <input id="credit" name="paymentMethod" type="radio" class="custom-control-input" checked required>
                        <label class="custom-control-label" for="credit">Credit card</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 mb-3">
                        <label for="cc-name">Name on card</label>
                        <input type="text" class="form-control" id="cc-name" placeholder="" required>
                        <small class="text-muted">Full name as displayed on card</small>
                        <div class="invalid-feedback">
                            Name on card is required
                        </div>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="cc-number">Credit card number</label>
                        <input type="text" class="form-control" id="cc-number" placeholder="" required>
                        <div class="invalid-feedback">
                            Credit card number is required
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 mb-3">
                        <label for="cc-expiration-month">Expiration Month</label>
                        <input type="number" class="form-control" id="cc-expiration-month" placeholder="" required><br>
                        <label for="cc-expiration">Expiration year</label>
                        <input type="number" class="form-control" id="cc-expiration-year" placeholder="" required>
                        
                        
                    </div>
                    <div class="col-md-3 mb-3">
                        <label for="cc-cvv">CVV</label>
                        <input type="text" class="form-control" id="cc-cvv" placeholder="" required>
                    </div>
                </div>
                <hr class="mb-4">
            </form>
            <button class="btn btn-primary btn-lg btn-block" onclick="submit()">Process Order</button>
        </div>
    </div>

    <footer class="my-5 pt-5 text-muted text-center text-small">
        <p class="mb-1">&copy; AUSVJ</p>
        <ul class="list-inline">
            <li class="list-inline-item"><a href="#">Privacy</a></li>
            <li class="list-inline-item"><a href="#">Terms</a></li>
            <li class="list-inline-item"><a href="#">Support</a></li>
        </ul>
    </footer>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.1.1.min.js">
<script src="/assets/js/popper.min.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/holder.min.js"></script>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script>
    var productDiscount 
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function() {
        'use strict';

        window.addEventListener('load', function() {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');

            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();


    function getDiscount() { 
         productDiscount =  JSON.parse('{!!$productDiscount!!}');
    }


    function countDiscount(qty){
        let totalDiscount = 0 
        let theDiscount 
        for(var i=0; i<productDiscount.length; i++){
            if (productDiscount[i].qty_discount == qty) { 
                theDiscount = productDiscount[i]
            }
        }

        if(theDiscount != null) { 
            $("#qty").val(qty)
            let countDiscount = qty*{{$product->price}}
            totalDiscount = countDiscount/100*theDiscount.percentage
            
        } 
        
        countTotal(qty, {{$product->price}},totalDiscount)
    }

    function countTotal(qty, productPrice, totalDiscount) {
        let total = productPrice*qty
        let totalAfterDiscount = total-totalDiscount
         $("#total_order_price").val(totalAfterDiscount)
         $("#total_discount").val(totalDiscount)
        
         if(totalDiscount != 0) { 
            $("#total").html(total+" - "+totalDiscount+"<br>"+totalAfterDiscount)
         } else { 
            $("#total").html(totalAfterDiscount)
         }
        
            
    }

    $("#qty").change(()=>{ 
        let price = {{$product->price}}
        let qty = $("#qty").val()

        countDiscount(qty) 
    })


    function submit() { 
        let publicKey = "{{ env('STRIPE_KEY') }}"
        Stripe.setPublishableKey(publicKey);
        Stripe.createToken({
            number: $('#cc-number').val(),
            cvc: $('#cc-cvv').val(),
            exp_month: $('#cc-expiration-month').val(),
            exp_year: $('#cc-expiration-year').val()
          }, stripeResponseHandler);
    }

    function stripeResponseHandler(status, response) {
        if (response.error) {
            alert('Error while processing payment')
            console.log(response)
        } else {
           submitOrder(response.id)
        }
    }

    function submitOrder(stripeToken) { 
        
        var formData = new FormData();
    

        formData.append("first_name",  $("#first_name").val())
        formData.append("last_name",  $("#last_name").val())
        formData.append("email",  $("#email").val())
        formData.append("address", $("#address").val())
        formData.append("phone_number",  $("#phone_number").val())
        formData.append("product_id", getLastUrlSegment(window.location.href))
        formData.append("qty",  $("#qty").val())
        formData.append("price", {{$product->price}})
        formData.append("total_price",  $("#total_order_price").val())
        formData.append("total_discount",  $("#total_discount").val())
        formData.append("stripe_token",  stripeToken)



        $.ajax({
            type: "POST",
            url: "/api/order/create",
            data: formData,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(response) {

                if (response.code === 0) {
                    alert('Transaction {{ trans('common.success') }}')
                    window.location.href("/")
                } else {
                    console.log(response)
                    alert('Error while processing payment')
                }
            },
            error: function(request, status, error) {
                console.log(error)
                alert('Error while processing payment')
            },
            processData: false,
            contentType: false,
        });

        
    }

    function getLastUrlSegment(url) {
        return new URL(url).pathname.split('/').filter(Boolean).pop();
    }


   
   
    countTotal(1,{{$product->price}},0)
    getDiscount()
</script>
</body>
</html>
