@extends('layouts.admin')
@section('css')
    <link href="{{ asset('css/spinner.css') }}" rel="stylesheet" type="text/css">
    <style>
        .img-responsive {
            height: 100px;
        }

        .preview {
            margin-top: 10px;
        }

        .tox-tinymce-aux {
            display: none !important;
        }
    </style>
@endsection
@section('content')
    <div class="row" id="product">
        <div class="col-12">
            <button class="btn btn-sm btn-primary btn-add-item" data-toggle="modal" onclick="openAddModal()" style="float: right;margin: 8px;">{{ trans('common.add') }}</button>
        </div>
        <div class="col-md-12">
            <!-- ============================================================== -->
            <!-- top perfomimg  -->
            <!-- ============================================================== -->
            <div class="card">
                <h5 class="card-header">Article management</h5>
                <div class="card-body p-4">
                    <div class="table-responsive">
                        <table class="table no-wrap p-table x-table">
                        </table>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end top perfomimg  -->
            <!-- ============================================================== -->
            <!-- Modal Start -->
            <div class="modal fade " id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-xl">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="editModalLabel"></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input id="title" name="title" class="form-control" type="text" placeholder="Title" required>
                            </div>
                            <div class="form-group">
                                <label for="picture">Picture</label>
                                <input id="picture" name="picture[]" class="form-control" type="file" placeholder="Picture" required multiple>
                                <input id="oldPicture" name="oldPicture" type="hidden">
                            </div>
                            <div class="form-group">
                                <label for="meta_desc">Meta Description</label>
                                <input id="meta_desc" name="meta_desc" class="form-control" type="text" required>
                            </div>
                            <div class="form-group">
                                <label> Article Content </label>
                               <textarea id="desc">Please insert the article here</textarea>
                            </div>
                            <div class="form-group">
                                <label for="category">Category</label>
                                <select id="category" name="category" class="form-control" placeholder="hello">
                                    <option value="" selected disabled>Select category</option>
                                    @foreach ($categories as $item)
                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('common.close') }}</button>
                            <button type="button" class="btn btn-primary" onclick="saveData()">{{ trans('common.save_changes') }}</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal End -->

            <!-- Spinner -->
            <div class="spinner" id="loading">
                <div class="spinner-bg"></div>
                <div class="spinner-position">
                    <div class="lds-ring">
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        tinymce.init({
            selector: 'textarea',
            plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount checklist mediaembed casechange export formatpainter pageembed linkchecker a11ychecker tinymcespellchecker permanentpen powerpaste advtable advcode editimage tinycomments tableofcontents footnotes mergetags autocorrect typography inlinecss',
            toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table mergetags | addcomment showcomments | spellcheckdialog a11ycheck typography | align lineheight | checklist numlist bullist indent outdent | emoticons charmap | removeformat',
            tinycomments_mode: 'embedded',
            tinycomments_author: 'Author name',
            mergetags_list: [
                { value: 'First.Name', title: 'First Name' },
                { value: 'Email', title: 'Email' },
            ]
        });
    </script>
    <script>
        $("input[type='search']").wrap("<form>");
        $("input[type='search']").closest("form").attr("autocomplete", "off");

        var loading = $("#loading")
        var articleData
        var isEdit = false

        function reload() {
            setTimeout(function() {
                location.reload()
            }, 1000)
        }

        function openAddModal() {
            isEdit = false
            resetForm()
            $("#editModalLabel").html("Add new Article")
            $("#editModal").modal('show')
        }

        function openEditModal(data) {
            isEdit = true

            $("#title").val(data.title)
            var desc = tinymce.get("desc").setContent(data.desc);
            $("#meta_desc").val(data.meta_desc)
            $("#category").val(data.category_id)
            $("#oldPicture").val(data.picture ? true : false)

            $("#editModalLabel").html("Edit new Article")
            $("#editModal").modal('show')
        }

        function getStaffData() {

            $(".x-table").DataTable({
                "processing": true,
                "serverSide": true,
                "language": {
                    processing: `<div class="spinner">
                            <div class="spinner-bg"></div>
                            <div class="spinner-position">
                                <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
                            </div>
                        </div>`
                },
                "ajax": "{{ route('api.v1.article.index') }}",
                "columns": [{
                        "title": "id",
                        "data": "id"
                    },
                    {
                        "title": "Title",
                        "data": "title"
                    },
                    {
                        "title": "Category",
                        "data": "name",
                        "name" : "categories.name"
                    },
                    {
                        "title": "{{ trans('common.actions') }}",
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        render: function(data, type, row) {
                            var view = '<button class="btn btn-sm btn-primary btn-edit-item" onclick="viewData(' + row.id + ')">Edit</button>';
                            view += '&nbsp;<button class="btn btn-sm btn-warning btn-delete-item" data-id="' + row.id + '" onclick="deleteData(' + row.id + ')">Remove</button>'
                            return view
                        }
                    }
                ],
            })



            loading.hide()
        }

        function saveData() {
            var url = ''
            var formData = new FormData();
            if (this.isEdit) {
                url = url = "/api/article/update"
                formData.append("article_id", articleData.id)
            } else {
                url = "/api/article/store"
            }

            let headers = {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
            this.isLoading = true

            var totalfiles = document.getElementById('picture').files.length;
            for (var index = 0; index < totalfiles; index++) {
                formData.append("picture[]", document.getElementById('picture').files[index]);
            }
            formData.append('meta_desc', $("#meta_desc").val());
            // get Data tinyMce
            var desc = tinymce.get("desc").getContent();
            formData.append('desc', desc );
            formData.append('title', $("#title").val());
            formData.append('category_id', $("#category").val());
            formData.append('oldPicture', $("#oldPicture").val());

            $.ajax({
                type: "POST",
                url: url,
                data: formData,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(response) {

                    if (response.code === 0) {
                        swal('{{ trans('common.success') }}', response.message, 'success')
                        reload()
                    } else {
                        swal('{{ trans('common.warning') }}', response.message, 'warning')
                        reload()
                    }
                },
                error: function(request, status, error) {
                    console.log(request)
                    swal('{{ trans('common.warning') }}', request.responseJSON.message, 'warning')
                },
                processData: false,
                contentType: false,
            });

        }

        function deleteData(id) {
            swal({
                    title: "{{ trans('common.are_you_sure') }}?",
                    text: "{{ trans('common.sure_to_delete') }}",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        var url = "/api/article/delete/" + id
                        $.ajax({
                            type: "DELETE",
                            url: url,
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            success: function(response) {

                                if (response.code === 0) {
                                    swal('{{ trans('common.success') }}', response.message, 'success')
                                    reload()
                                } else {
                                    swal('{{ trans('common.warning') }}', response.message, 'warning')
                                    reload()
                                }
                            },
                            error: function(request, status, error) {
                                console.log(request)
                                swal('{{ trans('common.warning') }}', request.responseJSON.message, 'warning')
                            },
                            processData: false,
                            contentType: false,
                        });
                    }
                });
        }

        function viewData(id) {
            var url = "/api/article/" + id
            isEdit = true
            $.ajax({
                type: "GET",
                url: url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(response) {

                    if (response.code === 0) {
                        articleData = response.data
                        console.log(articleData)
                        openEditModal(response.data)
                    } else {
                        console.log("get data error ")
                    }
                },
                error: function(request, status, error) {
                    console.log(request)
                    swal('{{ trans('common.warning') }}', request.responseJSON.message, 'warning')
                },
                processData: false,
                contentType: false,
            });


        }

        function resetForm(){
            $("#price").val("")
            $("#description").val("")
            $("#category").val("")
            $("#category").val("")
            $("#oldPicture").val("")
        }



        getStaffData();
    </script>
@endsection
