@extends('layouts.admin')
@section('css')
<link href="{{ asset('css/spinner.css') }}" rel="stylesheet" type="text/css">
<style>

.img-responsive{
    height: 100px;
}
.preview{
    margin-top:10px;
}
</style>
@endsection
@section('content')
<div class="row" id="product">
<div class="col-12">
            <button class="btn btn-sm btn-primary btn-add-item" data-toggle="modal" onclick="openAddModal()" style="float: right;margin: 8px;">{{ trans('common.add') }}</button>
    </div>
    <div class="col-md-12">
                <!-- ============================================================== -->
        <!-- top perfomimg  -->
        <!-- ============================================================== -->
        <div class="card">
            <h5 class="card-header">User management</h5>
            <div class="card-body p-4">
                <div class="table-responsive">
                    <table class="table no-wrap p-table x-table">
                    </table>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end top perfomimg  -->
        <!-- ============================================================== -->
               <!-- Modal Start -->
               <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true" >
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="editModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input id="name" class="form-control" type="text" placeholder="Name" required>
                </div>
                  <div class="form-group">
                      <label for="name">Email</label>
                      <input id="email" class="form-control" type="text" placeholder="Email" required>
                  </div>
                    <div class="form-group">
                        <label for="link">Phone</label>
                        <input id="phone" class="form-control" type="text" placeholder="Phone" required>
                    </div>
                      <div class="form-group">
                          <label for="link">Password</label>
                          <input id="password" class="form-control" type="password">
                      </div>
                    <div class="form-group">
                        <label for="my-select">Active</label>
                        <select id="isActive" class="form-control" >
                            <option value="1">true</option>
                            <option value="0">False</option>
                        </select>
                    </div>
                  <div class="form-group">
                      <label for="role">Role</label>
                      <select id="role" class="form-control" placeholder="hello">
                          <option value="" selected disabled>Select role</option>
                        @foreach ($role as $roles)
                          <option value="{{$roles->id}}" >{{$roles->name}}</option>
                         @endforeach
                      </select>
                  </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('common.close') }}</button>
                <button type="button" class="btn btn-primary" onclick="saveData()">{{ trans('common.save_changes') }}</button>
              </div>
            </div>
          </div>
        </div>
        <!-- Modal End -->

        <!-- Spinner -->
        <div class="spinner" id="loading">
            <div class="spinner-bg"></div>
            <div class="spinner-position">
                <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
    $("input[type='search']").wrap("<form>");
    $("input[type='search']").closest("form").attr("autocomplete","off");

    var loading = $("#loading")
    var staffData
    var isEdit= false

    function reload() {
        setTimeout(function(){
            location.reload()
        },1000)
    }

    function openAddModal() {
        isEdit = false
        $("#editModalLabel").html("Add new user")
        $("#editModal").modal('show')
    }
    function openEditModal(data) {
        isEdit = true

        $("#name").val(data.name)
        $("#email").val(data.email)
        $("#phone").val(data.phone)
        $("#role").val(data.role_id)
        $("#isActive").val(data.is_active)

        $("#editModalLabel").html("Edit new user")
        $("#editModal").modal('show')
    }
    function getStaffData() {

        $(".x-table").DataTable({
            "processing": true,
            "serverSide": true,
            "language": {
                processing: `<div class="spinner">
                            <div class="spinner-bg"></div>
                            <div class="spinner-position">
                                <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
                            </div>
                        </div>`
            },
            "ajax": "{{ route('api.v1.user.index') }}",
            "columns":[
                {
                    "title": "id",
                    "data": "id"
                },
                {
                    "title": "Name",
                    "data": "name"
                },
                {
                    "title" : "Email",
                    "data": "email"
                },
                {
                    "title" : "Phone",
                    "data": "phone"
                },
                {
                    "title" : "Role",
                    "data": "role",
                    'name': "roles.name"
                },
                {
                    "title": "{{ trans('common.actions') }}",
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    render: function(data, type, row) {
                        var view = '<button class="btn btn-sm btn-primary btn-edit-item" onclick="viewData('+row.id+')">Edit</button>';
                        view += '&nbsp;<button class="btn btn-sm btn-warning btn-delete-item" data-id="'+row.id+'" onclick="deleteData('+row.id+')">Remove</button>'
                        return view
                    }
                }
            ],
        })



        loading.hide()
    }
    function saveData() {
        var url = ''
        var formData = new FormData();
        if(this.isEdit){
            url =  url ="/api/user/update"
            formData.append("user_id",staffData.id)
        }else{
            url ="/api/user/store"
        }

        let headers =    {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
        this.isLoading = true
        formData.append('name', $("#name").val());
        formData.append('email',  $("#email").val());
        formData.append('phone',  $("#phone").val());

        if ($("#password").val() !== "" ) {
            formData.append('password',  $("#password").val());
        }


        formData.append('role', $("#role").val());
        formData.append('is_active', $("#isActive").val());

        $.ajax({
            type: "POST",
            url: url,
            data : formData,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {

                if (response.code === 0) {
                    swal('{{ trans('common.success') }}', response.message, 'success')
                    reload()
                } else {
                    swal('{{ trans('common.warning') }}', response.message, 'warning')
                    reload()
                }
            },
            error: function (request, status, error) {
                console.log(request)
                swal('{{ trans('common.warning') }}', request.responseJSON.message, 'warning')
            },
            processData: false,
            contentType: false,
        });

    }
    function deleteData(id) {
        swal({
            title: "{{ trans('common.are_you_sure') }}?",
            text: "{{ trans('common.sure_to_delete') }}",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    var url ="/api/user/delete/" + id
                    $.ajax({
                        type: "DELETE",
                        url: url,
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function (response) {

                            if (response.code === 0) {
                                swal('{{ trans('common.success') }}', response.message, 'success')
                                reload()
                            } else {
                                swal('{{ trans('common.warning') }}', response.message, 'warning')
                                reload()
                            }
                        },
                        error: function (request, status, error) {
                            console.log(request)
                            swal('{{ trans('common.warning') }}', request.responseJSON.message, 'warning')
                        },
                        processData: false,
                        contentType: false,
                    });
                }
            });
    }
    function viewData(id) {
        var url ="/api/user/" + id
        isEdit=true
        $.ajax({
            type: "GET",
            url: url,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {

                if (response.code === 0) {
                    staffData = response.data
                    console.log(staffData)
                    openEditModal(response.data)
                } else {
                    console.log("get data error ")
                }
            },
            error: function (request, status, error) {
                console.log(request)
                swal('{{ trans('common.warning') }}', request.responseJSON.message, 'warning')
            },
            processData: false,
            contentType: false,
        });


    }



    getStaffData();

</script>
@endsection
