@extends('layouts.admin')
@section('css')
<link href="{{ asset('css/spinner.css') }}" rel="stylesheet" type="text/css">
<style>
[v-cloak] {
    display: none !important;
}

.img-responsive{
    height: 100px;
}
.preview{
    margin-top:10px;
}
</style>
@endsection
@section('content')
<div class="row" id="slider" v-cloak>
<div class="col-12">
            <button class="btn btn-sm btn-primary btn-add-item" @click="isEdit=false" data-toggle="modal" data-target="#editModal" style="float: right;margin: 8px;">{{ trans('common.add') }}</button>
    </div>
    <div class="col-md-12">
                <!-- ============================================================== -->
        <!-- top perfomimg  -->
        <!-- ============================================================== -->
        <div class="card">
            <h5 class="card-header">Slider Pages</h5>
            <div class="card-body p-4">
                <div class="table-responsive">
                    <table class="table no-wrap p-table banner-table">
                    </table>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end top perfomimg  -->
        <!-- ============================================================== -->
               <!-- Modal Start -->
               <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true" >
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="editModalLabel">@{{ isEdit ? "@lang('common.edit_product_banner')" : "@lang('common.add_product_banner')" }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                    <div class="form-group">
                        <label for="caption">{{ trans('common.caption') }}</label>
                        <input v-model="itemBanner.caption" class="form-control" type="text" placeholder="{{ trans('common.caption') }}">
                    </div>          
                    <div class="form-group">
                        <label for="image">{{ trans('common.image') }}</label>
                        <input type="file" class="form-control-file" @change="fileChanged">
                        <div class="preview" v-if="image_src">
                            <img :src="image_src" class="img-responsive">
                        </div>
                    </div>
{{--                <div class="form-group">
                        <label>{{ trans('common.sort_order') }}</label>
                        <input v-model="itemBanner.sort" class="form-control" type="number" placeholder="{{ trans('common.sort_order') }}">
                    </div>--}}
                    <div class="form-group">
                        <label for="my-select">Active</label>
                        <select id="my-select" class="form-control" v-model="itemBanner.is_active">
                        <option v-for="(activ,index) in actives" v-bind:value="activ.key">@{{ activ.value }}</option>
                        </select>
                    </div>
{{--                    <div class="form-group">
                        <label for="my-select">{{ trans('common.select_product') }}</label>
                        <select id="my-select" class="form-control" v-model="itemBanner.product_id" v-if="products.length > 0">
                            <option value="">{{ trans('common.select_product') }}</option>
                            <option v-for="(product,index) in products" v-bind:value="product.id">@{{ product.title }}</option>
                        </select>
                    </div>--}}
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('common.close') }}</button>
                <button type="button" class="btn btn-primary" @click="saveDeal">{{ trans('common.save_changes') }}</button>
              </div>
            </div>
          </div>
        </div>
        <!-- Modal End -->

        <!-- Spinner -->
        <div class="spinner" v-if="isLoading">
            <div class="spinner-bg"></div>
            <div class="spinner-position">
                <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
    const slider = new Vue({
        el: '#slider',
        data:{
            banners: null,
            isLoading: false,
            isEdit:false,
            image_src:'',
            itemBanner: {},
            actives:[{key:true,value:'True'},{key:false,value:'False'}],
        },
        methods:{
            fileChanged: function(e) {
                const file = e.target.files[0];
                this.itemBanner.image = file;
                this.image_src = URL.createObjectURL(file);
            },
            getBanners: function (){
                this.banners = $(".banner-table").DataTable({
                    "processing": true,
                    "serverSide": true,
                    "language": {
                        processing: `<div class="spinner">
                            <div class="spinner-bg"></div>
                            <div class="spinner-position">
                                <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
                            </div>
                        </div>`
                    },
                    "ajax": "{{ route('slider.list') }}",
                    "columns":[
                        {
                            "title": "{{ trans('common.id') }}",
                            "data": "id"
                        },
                        {   
                            "title" : "{{ trans('common.image') }}",
                            "data": null,
                            "searchable": false,
                            "orderable": false,
                            render: function(data, type, row) {
                                var view = '<img src="'+row.image+'" class="img-responsive" />';            
                                return view
                            }
                        },
                        {
                            "title": "{{ trans('common.caption') }}",
                            "data": "caption"
                        },
                        {   
                            "title" : "{{ trans('common.active') }}",
                            "data" : "is_active"
                        },
                        {
                            "title": "{{ trans('common.actions') }}",
                            "data": null,
                            "searchable": false,
                            "orderable": false,
                            render: function(data, type, row) {
                                var view = '<button class="btn btn-sm btn-primary btn-edit-item" data-toggle="modal" data-target="#editModal">Edit</button>';
                                view += '&nbsp;<button class="btn btn-sm btn-warning btn-delete-item" data-id="'+row.id+'">Remove</button>'
                                return view
                            }
                        }                                       
                    ],
                });
                let self = this
                $(".banner-table").on("click", ".btn-delete-item", function() {
                    var id = $(this).attr('data-id')
                    self.removeItem(id)
                })
                $(".banner-table").on("click", ".btn-edit-item", function() {
                    var $tr = $(this).closest('tr');
                    var data = self.banners.row($tr).data();
                    self.itemBanner = data
                    if (self.itemBanner.image == '') {
                        self.image_src = ''
                    } else {
                        self.image_src = self.itemBanner.image
                    }
                    self.isEdit = true
                })
            },
            saveDeal: function(){
                var url = ''
                var formData = new FormData();
                if(this.isEdit){
                    url = '/api/v1/banner/update/' + this.itemBanner.id
                    formData.append('_method', 'PUT');
                }else{
                    url = '{{ route('api.v1.banner.store') }}'
                }
                this.itemBanner.caption = this.itemBanner.caption != undefined ? this.itemBanner.caption : ''
                this.isLoading = true                
                formData.append('caption', this.itemBanner.caption);
                formData.append('image', this.itemBanner.image);
                // formData.append('sort', this.itemBanner.sort);
                // formData.append('type', this.itemBanner.type);
                formData.append('is_active', this.itemBanner.is_active);
                // formData.append('product_id', this.itemBanner.product_id);
                this.$http.post(url,formData).then(response => {
                    this.reset()
                    if (response.data.code == 0) {                       
                        swal('{{ trans('common.success') }}', response.data.message, 'success')
                        this.banners.ajax.reload()
                    } else {
                        swal('{{ trans('common.warning') }}', response.data.message, 'warning')
                    }
                }, response => {
                    this.reset()
                    swal('{{ trans('common.warning') }}', response.data.message, 'warning')
                });
            },
            reset: function(){
                this.isLoading = false
                this.isEdit = false
                $("#editModal").modal("hide")
                this.itemBanner = {}
                this.img_src = ''
            },
            removeItem: function(id){
                swal({
                    title: "{{ trans('common.are_you_sure') }}?",
                    text: "{{ trans('common.sure_to_delete') }}",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        var url = '/api/v1/banner/destroy/' + id
                        var params = {
                            // _token: this.edit._token,
                        }

                        this.isLoading = true
                        this.$http.delete(url, params).then(response => {
                            this.isLoading = false
                            if (response.data.code == 0) {
                                swal('{{ trans('common.success') }}', response.data.message, 'success')
                                this.banners.ajax.reload()
                            } else {
                                swal('{{ trans('common.warning') }}', response.data.message, 'warning')
                            }
                        }, response => {
                            this.isLoading = false
                            swal('{{ trans('common.warning') }}', response.data.message, 'warning')
                        });
                    }
                });
            },
        },
        mounted() {
            this.getBanners()
        }
    });
</script>
@endsection