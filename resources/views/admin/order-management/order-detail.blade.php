@extends('layouts.admin')
@section('css')
<link href="{{ asset('css/spinner.css') }}" rel="stylesheet" type="text/css">
<style>

.img-responsive{
    height: 100px;
}
.preview{
    margin-top:10px;
}
</style>
@endsection
@section('content')
<div class="row" id="product">
    <div class="col-md-12">
                <!-- ============================================================== -->
        <!-- top perfomimg  -->
        <!-- ============================================================== -->
        <div class="card">
            <h5 class="card-header">Order Detail</h5>
            <div class="card-body p-4">
                <table>

                    <tr>
                        <td>Patient Name</td>
                        <td>{{$order->patient_name}} </td>
                    </tr>
                    <tr>
                        <td>Patient Floor</td>
                        <td>{{$order->patient_floor}}</td>
                    </tr>
                    <tr>
                        <td>Patient Bed</td>
                        <td>{{$order->patient_bed}}</td>
                    </tr>
                    <tr>
                        <td>Patient Phone</td>
                        <td>{{$order->patient_phone}}</td>
                    </tr>
                    <tr>
                        <td>Officer Name | ID</td>
                        <td>{{$order->user_name}} | {{$order->user_id}}</td>
                    </tr>

                </table>
                <div class="table-responsive">
                    <table class="table no-wrap p-table x-table">
                    </table>
                </div>
                <h2> Total Order Price : {{$order->total_price_order}}</h2>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end top perfomimg  -->
        <!-- ============================================================== -->


        <!-- Spinner -->
        <div class="spinner" id="loading">
            <div class="spinner-bg"></div>
            <div class="spinner-position">
                <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
    $("input[type='search']").wrap("<form>");
    $("input[type='search']").closest("form").attr("autocomplete","off");

    var loading = $("#loading")
    var staffData
    var isEdit= false

    function reload() {
        setTimeout(function(){
            location.reload()
        },1000)
    }
    function getOrderData() {

        $(".x-table").DataTable({
            "processing": true,
            "serverSide": true,
            "language": {
                processing: `<div class="spinner">
                            <div class="spinner-bg"></div>
                            <div class="spinner-position">
                                <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
                            </div>
                        </div>`
            },
            "ajax": "{{ route('api.v1.order.detail', $order->id) }}",
            "columns":[
                {data: 'DT_RowIndex', searchable: false, orderable: false},
                {
                    "title": "Menu Desc",
                    "data": "menu_desc",
                    "name":"menus.description"
                },
                {
                    "title" : "Quantity",
                    "data": "qty"
                },
                {
                    "title" : "Price",
                    "data": "price"
                },
                {
                    "title" : "Subtotal",
                    "data": "total_price"
                }
            ],
        })

        loading.hide()
    }



    getOrderData();

</script>
@endsection
