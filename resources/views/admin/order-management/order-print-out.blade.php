@extends('layouts.admin')
@section('css')
<link href="{{ asset('css/spinner.css') }}" rel="stylesheet" type="text/css">
<style>

.img-responsive{
    height: 100px;
}
.preview{
    margin-top:10px;
}

#section-to-print { visibility: hidden;}
@media print {
    body * {
        visibility: hidden;
    }
    #section-to-print, #section-to-print * {
        visibility: visible;
    }

    #section-to-print {
        align-content: center;
        width: 100% !important;
    }
    #section-to-print {
        position: absolute;
        left: 0;
        right: 0;
        top: 0;
        z-index: 100;
    }
}
</style>
@endsection
@section('content')
<div class="row" id="product">
    <div class="col-md-12">
                <!-- ============================================================== -->
        <!-- top perfomimg  -->
        <!-- ============================================================== -->
        <div class="card">
            <h5 class="card-header">Order management</h5>
            <div class="card-body p-4">
                <div class="table-responsive">
                    <table class="table no-wrap p-table x-table">
                        <thead>
                        <tr>
                            <th>Patient Name</th>
                            <th>Order time</th>
                            <th>Menu</th>
                            <th>Qty</th>
                            <th>Total Price</th>
                            <th>Patient Floor</th>
                            <th>Patient Bed</th>
                            <th>Order Status</th>
                        </tr>
                        </thead>
                        @foreach ($datas as $data)
                        <tr>
                            <td>{{$data->patient_name}}</td>
                            <td>{{$data->created_at}}</td>
                            <td>{{$data->description}}</td>
                            <td>{{$data->qty}}</td>
                            <td>{{$data->total_price}}</td>
                            <td>{{$data->patient_floor}}</td>
                            <td>{{$data->patient_bed}}</td>
                            <td>{{$data->status}}</td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end top perfomimg  -->
        <!-- ============================================================== -->


        <!-- Spinner -->
        <div class="spinner" id="loading">
            <div class="spinner-bg"></div>
            <div class="spinner-position">
                <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
            </div>
        </div>
    </div>
</div>
<div class="row" id="section-to-print">
    <div class="col-md-12">
        <table class="table no-wrap p-table x-table">
            <thead>
            <tr>
                <th>Patient Name</th>
                <th>Order time</th>
                <th>Menu</th>
                <th>Qty</th>
                <th>Total Price</th>
                <th>Patient Floor</th>
                <th>Patient Bed</th>
                <th>Order Status</th>
            </tr>
            </thead>
            @foreach ($datas as $data)
            <tr>
                <td>{{$data->patient_name}}</td>
                <td>{{$data->created_at}}</td>
                <td>{{$data->description}}</td>
                <td>{{$data->qty}}</td>
                <td>{{$data->total_price}}</td>
                <td>{{$data->patient_floor}}</td>
                <td>{{$data->patient_bed}}</td>
                <td>{{$data->status}}</td>
            </tr>
            @endforeach
        </table>
    </div>
</div>
@endsection
@section('js')
<script>
    $("input[type='search']").wrap("<form>");
    $("input[type='search']").closest("form").attr("autocomplete","off");

    var loading = $("#loading")
    var staffData
    var isEdit= false

    function reload() {
        setTimeout(function(){
            location.reload()
        },1000)
    }
    function getOrderData() {

        loading.hide()
    }
    function saveData() {
        var url = ''
        var formData = new FormData();
        if(this.isEdit){
            url =  url ="/api/user/update"
            formData.append("user_id",staffData.id)
        }else{
            url ="/api/user/store"
        }

        let headers =    {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
        this.isLoading = true
        formData.append('name', $("#name").val());
        formData.append('email',  $("#email").val());
        formData.append('phone',  $("#phone").val());

        if ($("#password").val() !== "" ) {
            formData.append('password',  $("#password").val());
        }


        formData.append('role', $("#role").val());
        formData.append('is_active', $("#isActive").val());

        $.ajax({
            type: "POST",
            url: url,
            data : formData,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {

                if (response.code === 0) {
                    swal('{{ trans('common.success') }}', response.message, 'success')
                    reload()
                } else {
                    swal('{{ trans('common.warning') }}', response.message, 'warning')
                    reload()
                }
            },
            error: function (request, status, error) {
                console.log(request)
                swal('{{ trans('common.warning') }}', request.responseJSON.message, 'warning')
            },
            processData: false,
            contentType: false,
        });

    }
    function deleteData(id) {
        swal({
            title: "{{ trans('common.are_you_sure') }}?",
            text: "{{ trans('common.sure_to_delete') }}",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    var url ="/api/user/delete/" + id
                    $.ajax({
                        type: "DELETE",
                        url: url,
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function (response) {

                            if (response.code === 0) {
                                swal('{{ trans('common.success') }}', response.message, 'success')
                                reload()
                            } else {
                                swal('{{ trans('common.warning') }}', response.message, 'warning')
                                reload()
                            }
                        },
                        error: function (request, status, error) {
                            console.log(request)
                            swal('{{ trans('common.warning') }}', request.responseJSON.message, 'warning')
                        },
                        processData: false,
                        contentType: false,
                    });
                }
            });
    }
    function viewData(id) {
        var url ="/dashboard/order/"+id
        location.href=url


    }



    getOrderData();

</script>
@endsection
