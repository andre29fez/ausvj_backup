@extends('layouts.admin')
@section('css')
<link href="{{ asset('css/spinner.css') }}" rel="stylesheet" type="text/css">
<style>

.img-responsive{
    height: 100px;
}
.preview{
    margin-top:10px;
}
</style>
@endsection
@section('content')
<div class="row" id="product">
    <div class="col-md-12">
                <!-- ============================================================== -->
        <!-- top perfomimg  -->
        <!-- ============================================================== -->
        <div class="card">
            <h5 class="card-header">Order management</h5>
            <div class="card-body p-4">
                <div class="table-responsive">
                    <table class="table no-wrap p-table x-table">
                    </table>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end top perfomimg  -->
        <!-- ============================================================== -->


        <!-- Spinner -->
        <div class="spinner" id="loading">
            <div class="spinner-bg"></div>
            <div class="spinner-position">
                <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
    $("input[type='search']").wrap("<form>");
    $("input[type='search']").closest("form").attr("autocomplete","off");

    var loading = $("#loading")
    var staffData
    var isEdit= false

    function reload() {
        setTimeout(function(){
            location.reload()
        },1000)
    }
    function getOrderData() {

        $(".x-table").DataTable({
            "processing": true,
            "serverSide": true,
            "language": {
                processing: `<div class="spinner">
                            <div class="spinner-bg"></div>
                            <div class="spinner-position">
                                <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
                            </div>
                        </div>`
            },
            "ajax": "{{ route('api.v1.order.index') }}",
            "columns":[
                {
                    "title": "",
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    render: function(data, type, row) {
                        var view = '<input type="checkbox" name="checkBox[]" value="'+row.id+'" />';
                        return view
                    }
                },
                {
                    "title": "Order Id ",
                    "data": "id"
                },
                {
                    "title": "Cust first name",
                    "data": "first_name",
                },
                {
                    "title": "Cust last name",
                    "data": "last_name",
                },
                {
                    "title": "Cust email",
                    "data": "email",
                },
                {
                    "title": "Cust Phone",
                    "data": "phone_number",
                },
                {
                    "title": "Cust address",
                    "data": "address",
                },
                {
                    "title": "Cust address",
                    "data": "address",
                },
                {
                    "title": "Service",
                    "data": "product_name",
                    "name": "products.name",
                },
                {
                    "title": "price",
                    "data": "price",
                },
                {
                    "title": "QTY",
                    "data": "qty",
                },
                {
                    "title": "Total price",
                    "data": "total_price",
                },
                {
                    "title": "Total discount",
                    "data": "total_discount",
                },
                {
                    "title": "Payment status",
                    "data": "payment_status",
                }
            ],
        })

        loading.hide()
    }
    function saveData() {
        var url = ''
        var formData = new FormData();
        if(this.isEdit){
            url =  url ="/api/user/update"
            formData.append("user_id",staffData.id)
        }else{
            url ="/api/user/store"
        }

        let headers =    {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
        this.isLoading = true
        formData.append('name', $("#name").val());
        formData.append('email',  $("#email").val());
        formData.append('phone',  $("#phone").val());

        if ($("#password").val() !== "" ) {
            formData.append('password',  $("#password").val());
        }


        formData.append('role', $("#role").val());
        formData.append('is_active', $("#isActive").val());

        $.ajax({
            type: "POST",
            url: url,
            data : formData,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {

                if (response.code === 0) {
                    swal('{{ trans('common.success') }}', response.message, 'success')
                    reload()
                } else {
                    swal('{{ trans('common.warning') }}', response.message, 'warning')
                    reload()
                }
            },
            error: function (request, status, error) {
                console.log(request)
                swal('{{ trans('common.warning') }}', request.responseJSON.message, 'warning')
            },
            processData: false,
            contentType: false,
        });

    }
    function deleteData(id) {
        swal({
            title: "{{ trans('common.are_you_sure') }}?",
            text: "{{ trans('common.sure_to_delete') }}",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    var url ="/api/user/delete/" + id
                    $.ajax({
                        type: "DELETE",
                        url: url,
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function (response) {

                            if (response.code === 0) {
                                swal('{{ trans('common.success') }}', response.message, 'success')
                                reload()
                            } else {
                                swal('{{ trans('common.warning') }}', response.message, 'warning')
                                reload()
                            }
                        },
                        error: function (request, status, error) {
                            console.log(request)
                            swal('{{ trans('common.warning') }}', request.responseJSON.message, 'warning')
                        },
                        processData: false,
                        contentType: false,
                    });
                }
            });
    }
    function viewData(id) {
        var url ="/dashboard/order/"+id
        location.href=url


    }



    getOrderData();

</script>
@endsection
