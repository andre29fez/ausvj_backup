@extends('layouts.admin')
@section('css')
<link href="{{ asset('css/spinner.css') }}" rel="stylesheet" type="text/css">
<style>
[v-cloak] {
    display: none !important;
}

.img-responsive{
    height: 100px;
}
.preview{
    margin-top:10px;
}
</style>
@endsection
@section('content')
<div class="row" id="product" v-cloak>
<div class="col-12">
            <button class="btn btn-sm btn-primary btn-add-item" @click="isEdit=false" data-toggle="modal" data-target="#editModal" style="float: right;margin: 8px;">{{ trans('common.add') }}</button>
    </div>
    <div class="col-md-12">
                <!-- ============================================================== -->
        <!-- top perfomimg  -->
        <!-- ============================================================== -->
        <div class="card">
            <h5 class="card-header">Product Pages</h5>
            <div class="card-body p-4">
                <div class="table-responsive">
                    <table class="table no-wrap p-table x-table">
                    </table>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end top perfomimg  -->
        <!-- ============================================================== -->
               <!-- Modal Start -->
               <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true" >
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="editModalLabel">@{{ isEdit ? "@lang('common.edit_product')" : "@lang('common.add_product')" }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                    <div class="form-group">
                        <label for="name">{{ trans('common.name') }}</label>
                        <input v-model="itemProduct.name" class="form-control" type="text" placeholder="{{ trans('common.name') }}">
                    </div>          
                    <div class="form-group">
                        <label for="logo">{{ trans('common.logo') }}</label>
                        <input type="file" class="form-control-file" @change="fileChanged($event,'logo')">
                        <div class="preview" v-if="logo_src">
                            <img :src="logo_src" class="img-responsive">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="background">{{ trans('common.background') }}</label>
                        <input type="file" class="form-control-file" @change="fileChanged($event,'background')">
                        <div class="preview" v-if="background_src">
                            <img :src="background_src" class="img-responsive">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="link">{{ trans('common.link_desktop') }}</label>
                        <input v-model="itemProduct.link_desktop" class="form-control" type="text" placeholder="{{ trans('common.link_example') }}">
                    </div>
                    <div class="form-group">
                        <label for="link">{{ trans('common.link_mac') }}</label>
                        <input v-model="itemProduct.link_mac" class="form-control" type="text" placeholder="{{ trans('common.link_example') }}">
                    </div>   
                    <div class="form-group">
                        <label for="link">{{ trans('common.link_android') }}</label>
                        <input v-model="itemProduct.link_android" class="form-control" type="text" placeholder="{{ trans('common.link_example') }}">
                    </div>   
{{--                <div class="form-group">
                        <label>{{ trans('common.sort_order') }}</label>
                        <input v-model="itemProduct.sort" class="form-control" type="number" placeholder="{{ trans('common.sort_order') }}">
                    </div>--}}
                    <div class="form-group">
                        <label for="my-select">Active</label>
                        <select id="my-select" class="form-control" v-model="itemProduct.is_active">
                        <option v-for="(activ,index) in actives" v-bind:value="activ.key">@{{ activ.value }}</option>
                        </select>
                    </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('common.close') }}</button>
                <button type="button" class="btn btn-primary" @click="saveDeal">{{ trans('common.save_changes') }}</button>
              </div>
            </div>
          </div>
        </div>
        <!-- Modal End -->

        <!-- Spinner -->
        <div class="spinner" v-if="isLoading">
            <div class="spinner-bg"></div>
            <div class="spinner-position">
                <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
    const product = new Vue({
        el: '#product',
        data:{
            products: null,
            isLoading: false,
            isEdit:false,
            logo_src:'',
            background_src:'',
            itemProduct: {},
            actives:[{key:true,value:'True'},{key:false,value:'False'}],
        },
        methods:{
            fileChanged: function(e,type) {
                const file = e.target.files[0];
                if(type == 'logo'){                    
                    this.itemProduct.logo = file;
                    this.logo_src = URL.createObjectURL(file);
                }else if(type == 'background'){
                    this.itemProduct.background = file;
                    this.background_src = URL.createObjectURL(file);   
                }else{
                   console.log('nothing to action')
                }
            },
            getProducts: function (){
                this.products = $(".x-table").DataTable({
                    "processing": true,
                    "serverSide": true,
                    "language": {
                        processing: `<div class="spinner">
                            <div class="spinner-bg"></div>
                            <div class="spinner-position">
                                <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
                            </div>
                        </div>`
                    },
                    "ajax": "{{ route('product.list') }}",
                    "columns":[
                        {
                            "title": "{{ trans('common.id') }}",
                            "data": "id"
                        },
                        {
                            "title": "{{ trans('common.name') }}",
                            "data": "name"
                        },
                        {   
                            "title" : "{{ trans('common.logo') }}",
                            "data": null,
                            "searchable": false,
                            "orderable": false,
                            render: function(data, type, row) {
                                var view = '<img src="'+row.logo+'" class="img-responsive" />';            
                                return view
                            }
                        },
                        {   
                            "title" : "{{ trans('common.background') }}",
                            "data": null,
                            "searchable": false,
                            "orderable": false,
                            render: function(data, type, row) {
                                var view = '<img src="'+row.background+'" class="img-responsive" />';            
                                return view
                            }
                        },
                        {   
                            "title" : "{{ trans('common.active') }}",
                            "data" : "is_active"
                        },
                        {
                            "title": "{{ trans('common.actions') }}",
                            "data": null,
                            "searchable": false,
                            "orderable": false,
                            render: function(data, type, row) {
                                var view = '<button class="btn btn-sm btn-primary btn-edit-item" data-toggle="modal" data-target="#editModal">Edit</button>';
                                view += '&nbsp;<button class="btn btn-sm btn-warning btn-delete-item" data-id="'+row.id+'">Remove</button>'
                                return view
                            }
                        }                                       
                    ],
                });
                let self = this
                $(".x-table").on("click", ".btn-delete-item", function() {
                    var id = $(this).attr('data-id')
                    self.removeItem(id)
                })
                $(".x-table").on("click", ".btn-edit-item", function() {
                    var $tr = $(this).closest('tr');
                    var data = self.products.row($tr).data();
                    self.itemProduct = data 
                    self.logo_src = self.itemProduct.logo != '' ? self.itemProduct.logo : ''
                    self.background_src = self.itemProduct.background != '' ? self.itemProduct.background : ''                  
                    self.isEdit = true
                })
            },
            saveDeal: function(){
                var url = ''
                var formData = new FormData();
                if(this.isEdit){
                    url = '/api/v1/product/update/' + this.itemProduct.id
                    formData.append('_method', 'PUT');
                }else{
                    url = '{{ route('api.v1.product.store') }}'
                }
                this.itemProduct.name = this.itemProduct.name != undefined ? this.itemProduct.name : ''
                this.itemProduct.link_desktop = this.itemProduct.link_desktop != undefined ? this.itemProduct.link_desktop : ''
                this.itemProduct.link_mac = this.itemProduct.link_mac != undefined ? this.itemProduct.link_mac : ''
                this.itemProduct.link_android = this.itemProduct.link_android != undefined ? this.itemProduct.link_android : ''
                this.isLoading = true                
                formData.append('name', this.itemProduct.name);
                formData.append('logo', this.itemProduct.logo);
                formData.append('background', this.itemProduct.background);
                formData.append('link_desktop', this.itemProduct.link_desktop);
                formData.append('link_mac', this.itemProduct.link_mac);
                formData.append('link_android', this.itemProduct.link_android);
                // formData.append('sort', this.itemProduct.sort);
                // formData.append('type', this.itemProduct.type);
                formData.append('is_active', this.itemProduct.is_active);

                this.$http.post(url,formData).then(response => {
                    this.reset()
                    if (response.data.code == 0) {                       
                        swal('{{ trans('common.success') }}', response.data.message, 'success')
                        this.products.ajax.reload()
                    } else {
                        swal('{{ trans('common.warning') }}', response.data.message, 'warning')
                    }
                }, response => {
                    this.reset()
                    swal('{{ trans('common.warning') }}', response.data.message, 'warning')
                });
            },
            reset: function(){
                this.isLoading = false
                this.isEdit = false
                $("#editModal").modal("hide")
                this.itemProduct = {}
                this.logo_src = ''
                this.background_src = ''
            },
            removeItem: function(id){
                swal({
                    title: "{{ trans('common.are_you_sure') }}?",
                    text: "{{ trans('common.sure_to_delete') }}",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        var url = '/api/v1/product/destroy/' + id
                        var params = {
                            // _token: this.edit._token,
                        }

                        this.isLoading = true
                        this.$http.delete(url, params).then(response => {
                            this.isLoading = false
                            if (response.data.code == 0) {
                                swal('{{ trans('common.success') }}', response.data.message, 'success')
                                this.products.ajax.reload()
                            } else {
                                swal('{{ trans('common.warning') }}', response.data.message, 'warning')
                            }
                        }, response => {
                            this.isLoading = false
                            swal('{{ trans('common.warning') }}', response.data.message, 'warning')
                        });
                    }
                });
            },
        },
        mounted() {
            this.getProducts()
        }
    });
</script>
@endsection