@extends('layouts.admin')
@section('css')
    <link href="{{ asset('css/spinner.css') }}" rel="stylesheet" type="text/css">
    <style>
        .img-responsive {
            height: 100px;
        }

        .preview {
            margin-top: 10px;
        }

        .tox-tinymce-aux {
            display: none !important;
        }
    </style>
@endsection
@section('content')
    <div class="row" id="product">
        <div class="col-12">
            <button class="btn btn-sm btn-primary btn-add-item" data-toggle="modal" onclick="openAddModal()" style="float: right;margin: 8px;">{{ trans('common.add') }}</button>
        </div>
        <div class="col-md-12">
            <!-- ============================================================== -->
            <!-- top perfomimg  -->
            <!-- ============================================================== -->
            <div class="card">
                <h5 class="card-header">Product Discount management</h5>
                <div class="card-body p-4">
                    <div class="table-responsive">
                        <table class="table no-wrap p-table x-table">
                        </table>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end top perfomimg  -->
            <!-- ============================================================== -->
            <!-- Modal Start -->
            <div class="modal fade " id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-xl">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="editModalLabel"></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="product_id">Product</label>
                                <select id="product_id" name="product_id" class="form-control" placeholder="hello">
                                    <option value="" selected disabled>Select product</option>
                                    @foreach ($products as $item)
                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label> Quantity </label>
                                <input id="qty_discount" name="qty_discount" class="form-control" type="number" min="0" required>
                            </div>
                            <div class="form-group">
                                <label> Discount Percentage % (from price)</label>
                                <input id="percentage" name="percentage" class="form-control" type="number" min="0" required>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('common.close') }}</button>
                            <button type="button" class="btn btn-primary" onclick="saveData()">{{ trans('common.save_changes') }}</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal End -->

            <!-- Spinner -->
            <div class="spinner" id="loading">
                <div class="spinner-bg"></div>
                <div class="spinner-position">
                    <div class="lds-ring">
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        tinymce.init({
            selector: 'textarea',
            plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount checklist mediaembed casechange export formatpainter pageembed linkchecker a11ychecker tinymcespellchecker permanentpen powerpaste advtable advcode editimage tinycomments tableofcontents footnotes mergetags autocorrect typography inlinecss',
            toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table mergetags | addcomment showcomments | spellcheckdialog a11ycheck typography | align lineheight | checklist numlist bullist indent outdent | emoticons charmap | removeformat',
            tinycomments_mode: 'embedded',
            tinycomments_author: 'Author name',
            mergetags_list: [
                { value: 'First.Name', title: 'First Name' },
                { value: 'Email', title: 'Email' },
            ]
        });
    </script>
    <script>
        $("input[type='search']").wrap("<form>");
        $("input[type='search']").closest("form").attr("autocomplete", "off");

        var loading = $("#loading")
        var data
        var isEdit = false

        function reload() {
            setTimeout(function() {
                location.reload()
            }, 1000)
        }

        function openAddModal() {
            isEdit = false
            resetForm()
            $("#editModalLabel").html("Add new Product")
            $("#editModal").modal('show')
        }

        function openEditModal(data) {
            isEdit = true

            $("#product_id").val(data.product_id)
            $("#qty_discount").val(data.qty_discount)
            $("#percentage").val(data.percentage)

            $("#editModalLabel").html("Edit Product")
            $("#editModal").modal('show')
        }

        function getStaffData() {

            $(".x-table").DataTable({
                "processing": true,
                "serverSide": true,
                "language": {
                    processing: `<div class="spinner">
                            <div class="spinner-bg"></div>
                            <div class="spinner-position">
                                <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
                            </div>
                        </div>`
                },
                "ajax": "{{ route('api.v1.product-discount.index') }}",
                "columns": [{
                        "title": "id",
                        "data": "id"
                    },
                    {
                        "title": "Product Name",
                        "data": "name",
                        "name": "products.name"
                    },
                    {
                        "title": "Product Price",
                        "data": "price",
                        "name": "products.price"
                    },
                    {
                        "title": "Discount Quantity",
                        "data": "qty_discount",
                    },
                    {
                        "title": "Discount Percentage",
                        "data": "percentage",
                        "searchable": false,
                        "orderable": false,
                        render: function(data, type, row) {
                           return data + " %"
                        }
                    },
                    {
                        "title": "{{ trans('common.actions') }}",
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        render: function(data, type, row) {
                            var view = '<button class="btn btn-sm btn-primary btn-edit-item" onclick="viewData(' + row.id + ')">Edit</button>';
                            view += '&nbsp;<button class="btn btn-sm btn-warning btn-delete-item" data-id="' + row.id + '" onclick="deleteData(' + row.id + ')">Remove</button>'
                            return view
                        }
                    }
                ],
            })



            loading.hide()
        }

        function saveData() {
            var url = ''
            var formData = new FormData();
            if (this.isEdit) {
                url = "/api/product-discount/update"
                formData.append("product_discount_id", data.id)
            } else {
                url = "/api/product-discount/store"
            }

            let headers = {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
            this.isLoading = true

            formData.append('product_id', $("#product_id").val())
            formData.append('qty_discount', $("#qty_discount").val());
            formData.append('percentage', $("#percentage").val() );


            $.ajax({
                type: "POST",
                url: url,
                data: formData,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(response) {

                    if (response.code === 0) {
                        swal('{{ trans('common.success') }}', response.message, 'success')
                        reload()
                    } else {
                        swal('{{ trans('common.warning') }}', response.message, 'warning')
                        reload()
                    }
                },
                error: function(request, status, error) {
                    console.log(request)
                    swal('{{ trans('common.warning') }}', request.responseJSON.message, 'warning')
                },
                processData: false,
                contentType: false,
            });

        }

        function deleteData(id) {
            swal({
                    title: "{{ trans('common.are_you_sure') }}?",
                    text: "{{ trans('common.sure_to_delete') }}",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        var url = "/api/product-discount/delete/" + id
                        $.ajax({
                            type: "DELETE",
                            url: url,
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            success: function(response) {

                                if (response.code === 0) {
                                    swal('{{ trans('common.success') }}', response.message, 'success')
                                    reload()
                                } else {
                                    swal('{{ trans('common.warning') }}', response.message, 'warning')
                                    reload()
                                }
                            },
                            error: function(request, status, error) {
                                console.log(request)
                                swal('{{ trans('common.warning') }}', request.responseJSON.message, 'warning')
                            },
                            processData: false,
                            contentType: false,
                        });
                    }
                });
        }

        function viewData(id) {
            var url = "/api/product-discount/" + id
            isEdit = true
            $.ajax({
                type: "GET",
                url: url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(response) {

                    if (response.code === 0) {
                        data = response.data
                        console.log(data)
                        openEditModal(response.data)
                    } else {
                        console.log("get data error ")
                    }
                },
                error: function(request, status, error) {
                    console.log(request)
                    swal('{{ trans('common.warning') }}', request.responseJSON.message, 'warning')
                },
                processData: false,
                contentType: false,
            });


        }

        function resetForm(){
            $("#price").val("")
            $("#description").val("")
            $("#category").val("")
            $("#category").val("")
            $("#oldPicture").val("")
        }



        getStaffData();
    </script>
@endsection
