@extends('layouts.admin')
@section('css')
<link href="{{ asset('css/spinner.css') }}" rel="stylesheet" type="text/css">
<style>
[v-cloak] {
    display: none !important;
}

.img-responsive{
    height: 100px;
}
</style>
@endsection
@section('content')
<div class="row" id="layout-setting" v-cloak>
    <div class="col-md-12">
                <!-- ============================================================== -->
        <!-- top perfomimg  -->
        <!-- ============================================================== -->
        <div class="card">
            <h5 class="card-header">Setting Pages</h5>
            <div class="card-body p-4">
                <!-- start setting -->
                <div class="row">
                <div class="col-12" v-for="(settingmain,index) in settings">
                        <div class="card rounded m-2 p-4">
                                <div class="d-flex justify-content-start border-bottom mb-4">
                                <div><strong>@{{settingmain.name}}</strong> &nbsp;<small><i>@{{settingmain.description}}</i></small></div>
                                </div>
                                <div class="row" v-for="(child,x) in settingmain.setting">
                                <div class="col-2">
                                        <label :for="child.key">@{{child.label}}</label>
                                </div>
                                <div class="col-10 mt-2" v-if="child.type == 'text'">
                                        <input type="text" class="form-control" @focusout="saveSetting(child)" :placeholder="child.description" :id="child.key" v-model="child.value">
                                        <span><small>@{{child.description}}</small></span>
                                </div>
                                <div class="col-10 mt-2" v-if="child.type == 'textarea'">
                                        <textarea class="form-control" @focusout="saveSetting(child)" :placeholder="child.description" :id="child.key" v-model="child.value" rows="2"></textarea>                                        
                                        <span><small>@{{child.description}}</small></span>
                                </div>

                                </div>
                        </div>
                </div>
                </div>
                <!-- end setting -->
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end top perfomimg  -->
        <!-- ============================================================== -->        
        <!-- Spinner -->
        <div class="spinner" v-if="isLoading">
            <div class="spinner-bg"></div>
            <div class="spinner-position">
                <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
    var app = new Vue({
        el: '#layout-setting',
        data: {
            isLoading: false,
            settings:[],
        },
        methods: {
            getSettings: function(){
                this.isLoading = true
                var params = {
                    all: 1
                }
                let headers = {
                    'Content-Language': '{{app()->getLocale()}}'
                    }
                var url = '{{ route('api.v1.setting.index') }}'
                this.$http.get(url+ '?' + jQuery.param( params ),{headers}).then(response => {
                    this.isLoading = false
                    if (response.data.code == 0) {
                        this.settings = response.data.data
                    }
                }, response => {
                    this.isLoading = false
                    swal('{{ trans('common.warning') }}', response.data.message, 'error')
                });
            },
            saveSetting: function(setting){
                this.isLoading = true
                let url = '/api/v1/setting/update/' + setting.id
                var params = {
                    '_method': 'PUT',
                    'value' : setting.value
                }
                let headers = {
                    'Content-Language': '{{app()->getLocale()}}'
                    }
                this.$http.post(url,params,{headers}).then(response => {
                    this.isLoading = false
                    if (response.data.code == 0) {
                    }
                }, response => {
                    this.isLoading = false
                    swal('{{ trans('common.warning') }}', response.data.message, 'error')
                });

            }
        },
        mounted() {
            this.getSettings()
        }
    })
</script>
@endsection