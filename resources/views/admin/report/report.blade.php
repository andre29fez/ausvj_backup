@extends('layouts.admin')
@section('css')
    <link href="{{ asset('css/spinner.css') }}" rel="stylesheet" type="text/css">
    <style>
        .img-responsive {
            height: 100px;
        }

        .preview {
            margin-top: 10px;
        }
    </style>
@endsection
@section('content')
    <div class="row" id="product">
        <div class="col-md-12">
            <!-- ============================================================== -->
            <!-- top perfomimg  -->
            <!-- ============================================================== -->
            <div class="card">
                <h5 class="card-header">Report</h5>
                <div class="card-body p-4">
                    <form action="{{ route('api.v1.report.print') }}" method="post">
                    @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="fromDate">From</label>
                                    <input id="fromDate" name="fromDate" class="form-control" type="date" value="{{ date('Y-m-d') }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="toDate">To</label>
                                    <input id="toDate" name="toDate" class="form-control" type="date" value="{{ date('Y-m-d') }}">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="fromDate">Status</label>
                                    <select class="form-control" id="status" name="status">
                                      <option value="0" selected>All</option>
                                      <option value="IN_PROCCESS">In Process</option>
                                      <option value="READY">Ready</option>
                                      <option value="SENT">Sent</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12">
                                <button class="btn btn-sm btn-primary btn-add-item" type="submit">Print</button>
                                {{-- <a class="btn btn-warning" href="{{ route('api.v1.report.print') }}">Print</a> --}}
                                {{-- <button class="btn btn-sm btn-primary btn-add-item" data-toggle="modal" onclick="openAddModal()" style="float: right;margin: 8px;">Print</button> --}}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end top perfomimg  -->
        </div>
    </div>
@endsection

