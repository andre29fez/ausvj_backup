$('.responsive').slick({
    dots: true,
    infinite: true,
    speed: 300,
    arrow:true,
    slidesToShow: 1,
    dots: false,
    centerMode: false,
    slidesToScroll: 1,
     autoplay: true,
    autoplaySpeed: 3000,
    
  });

  var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5e19412a7e39ea1242a4146e/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();