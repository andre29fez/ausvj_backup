<?php


namespace App\Service\Product;

// all logic business is here

use App\Http\Requests\Product\ProductRequest;
use App\Model\Categorytegory;
use App\Http\Requests\Menu\ArticleRequest;
use App\Model\Articlecle;
use App\Model\Product;
use App\Model\ProductDiscount;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Contracts\DataTable;
use Yajra\DataTables\DataTables;

class ProductService
{
    public function getProductList(Request $request) {
        $query = $this->baseQuery($request);
        $perPage = 10;

        // check limit
        if ($request->has('limit')) {
            $perPage = $request->limit;
        }

       // process pagination
        return $this->pagination($request,$query,$perPage);
    }


    public function pagination($request,$query,$perPage) {
        if ($request->has('all')) {
            $results = $query->get();
            $data = new \Illuminate\Pagination\LengthAwarePaginator($results, $results->count(), -1);
        }else{
            $data = $query->paginate($perPage);
        }

        return $data;
    }

    public function getSingle($id) {
        $query= $this->baseQuery()->where("products.id", "=", $id);
        return $query;
    }

    public function baseQuery($request = null)
    {
        $query = DB::table('products');
        $query = $query->select('products.*');

        return $query;
    }

    public function dataTables(Request $request) {
        return DataTables::of($this->baseQuery($request))->make(true);
    }

    public function storeNew(ProductRequest  $request) {
        $product = new Product();

        $product->name = $request->name;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->save();

        return $product;
    }

    public function update(ProductRequest $request) {

        $product = Product::findOrFail($request->product_id);

        $product->name = $request->name;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->save();

        return $product;
    }

    public function delete(int $id) {
        $product = Product::findOrFail($id);
        $product->delete();
        $productDiscount = ProductDiscount::where("product_id","=",$id)->delete();


    }

}
