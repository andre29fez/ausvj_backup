<?php


namespace App\Service\Product;

// all logic business is here

use App\Http\Requests\Product\ProductDiscountRequest;
use App\Http\Requests\Product\ProductRequest;
use App\Model\Categorytegory;
use App\Http\Requests\Menu\ArticleRequest;
use App\Model\Articlecle;
use App\Model\Product;
use App\Model\ProductDiscount;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Contracts\DataTable;
use Yajra\DataTables\DataTables;

class ProductDiscountService
{
    public function getProductDiscountList(Request $request) {
        $query = $this->baseQuery($request);
        $perPage = 10;

        // check limit
        if ($request->has('limit')) {
            $perPage = $request->limit;
        }

       // process pagination
        return $this->pagination($request,$query,$perPage);
    }


    public function pagination($request,$query,$perPage) {
        if ($request->has('all')) {
            $results = $query->get();
            $data = new \Illuminate\Pagination\LengthAwarePaginator($results, $results->count(), -1);
        }else{
            $data = $query->paginate($perPage);
        }

        return $data;
    }

    public function getSingle($id) {
        $query= $this->baseQuery()->where("products_discount.id", "=", $id);
        return $query;
    }

    public function baseQuery($request = null)
    {
        $query = DB::table('products_discount');
        $query = $query->join('products', 'products_discount.product_id', '=', 'products.id'); 
        $query = $query->select('products.name','products.price', 'products_discount.*');

        return $query;
    }

    public function dataTables(Request $request) {
        return DataTables::of($this->baseQuery($request))->make(true);
    }

    public function storeNew(ProductDiscountRequest $request) {
        $productDiscount = new ProductDiscount();

        $productDiscount->product_id = $request->product_id;
        $productDiscount->qty_discount = $request->qty_discount;
        $productDiscount->percentage = $request->percentage;
        $productDiscount->save();

        return $productDiscount;
    }

    public function update(ProductDiscountRequest $request) {

        $productDiscount = ProductDiscount::findOrFail($request->product_discount_id);

        $productDiscount->product_id = $request->product_id;
        $productDiscount->qty_discount = $request->qty_discount;
        $productDiscount->percentage = $request->percentage;
        $productDiscount->save();

        return $productDiscount;
    }

    public function delete(int $id) {
        $productDiscount = ProductDiscount::findOrFail($id);
        $productDiscount->delete();

    }

}
