<?php


namespace App\Service\User;

// all logic business is here
use App\Http\Requests\User\UserRequest;
use App\Model\User;
use App\Model\UserRoleRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Contracts\DataTable;
use Yajra\DataTables\DataTables;

class UserService
{
    public function getUserList(Request $request) {
        $query = $this->baseQuery($request);
        $perPage = 10;

        // check limit
        if ($request->has('limit')) {
            $perPage = $request->limit;
        }

        if ($request->has("role")) {
            $query = $query->where('role_id','=',$request->role);
        }

       // process pagination
        return $this->pagination($request,$query,$perPage);
    }


    public function pagination($request,$query,$perPage) {
        if ($request->has('all')) {
            $results = $query->get();
            $data = new \Illuminate\Pagination\LengthAwarePaginator($results, $results->count(), -1);
        }else{
            $data = $query->paginate($perPage);
        }

        return $data;
    }

    public function getSingle($id) {
        $query= $this->baseQuery()->where("users.id", "=", $id);
        return $query;
    }

    public function baseQuery($request = null)
    {
        $query = DB::table('users');
        $query = $query->join("users_roles",'users.id','=','users_roles.user_id');
        $query = $query->join("roles", "roles.id","=","users_roles.role_id");
        $query = $query->select('users.name', 'users.id', 'users.email','users.phone', 'users.is_active',
                    'roles.name as role', 'roles.id as role_id');
        if($request != null) {
            $query= $query->where("users.id", '!=', $request->user()->id);
        }

        return $query;
    }

    public function dataTables(Request $request) {
        return DataTables::of($this->baseQuery($request))->make(true);
    }

    public function storeNewUser(UserRequest  $request) {
        $user = new User();
        $userRole = new UserRole();

        $user->name = $request->name;
        $user->phone = $request->phone;
        $user->email = $request->email ;
        $user->is_active = $request->is_active;
        $user->password= Hash::make($request->password);
        $user->save();

        DB::table('users_roles')->insert([
            'user_id' => $user->id,
            'role_id' => $request->role
        ]);

        return $user;
    }

    public function updateUser(UserRequest  $request) {
        $user = User::findOrFail($request->user_id);
        $user->name = $request->name;
        $user->phone = $request->phone;
        $user->email = $request->email ;
        $user->is_active = $request->is_active;

        if ($request->password != null || $request->password != '') {
            $user->password=Hash::make($request->password());
        }
        $user->save();

        $userRole = UserRole::where('user_id', '=', $request->user_id)->update([
            'role_id' => $request->role
        ]);


        return $user;
    }

    public function deleteUser(int $id) {
        $user = User::findOrFail($id);
        $user->delete();

        $userRole = UserRole::where('user_id','=',$id);
        $userRole->delete();

    }
}
