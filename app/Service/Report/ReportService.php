<?php


namespace App\Service\Menu;

// all logic business is here

use App\Model\Categorytegory;
use App\Http\Requests\Menu\ArticleRequest;
use App\Model\Articlecle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Contracts\DataTable;
use Yajra\DataTables\DataTables;

class ReportService
{
    public function getMenuList(Request $request) {
        $query = $this->baseQuery($request);
        $perPage = 10;

        // check limit
        if ($request->has('limit')) {
            $perPage = $request->limit;
        }

        if ($request->has("role")) {
            $query = $query->where('role_id','=',$request->role);
        }

       // process pagination
        return $this->pagination($request,$query,$perPage);
    }


    public function pagination($request,$query,$perPage) {
        if ($request->has('all')) {
            $results = $query->get();
            $data = new \Illuminate\Pagination\LengthAwarePaginator($results, $results->count(), -1);
        }else{
            $data = $query->paginate($perPage);
        }

        return $data;
    }

    public function getSingle($id) {
        $query= $this->baseQuery()->where("menus.id", "=", $id);
        return $query;
    }

    public function baseQuery($request = null)
    {
        $query = DB::table('menus');
        $query = $query->join("categories",'menus.category_id','=','categories.id');
        $query = $query->select('menus.*', 'categories.name');

        // dd($query->toSql());
        return $query;
    }

    public function dataTables(Request $request) {
        return DataTables::of($this->baseQuery($request))->make(true);
    }

    public function storeNewMenu(ArticleRequest  $request) {
        $menu = new Article();

        if($request->hasfile('picture')){

            foreach($request->file('picture') as $key=>$file)
            {
                $name = time().$key.'.'.$file->extension();
                $file->move(public_path().'/images/food/', $name);
                $data[] = $name;
            }

            $menu->picture = json_encode($data);
        }

        $menu->price = $request->price;
        $menu->description = $request->description;
        $menu->category_id = $request->category_id ;
        $menu->save();

        return $menu;
    }

    public function updateMenu(ArticleRequest  $request) {

        $menu = Article::findOrFail($request->menu_id);

        if($request->hasfile('picture')){

            foreach($request->file('picture') as $key=>$file)
            {
                $name = time().$key.'.'.$file->extension();
                $file->move(public_path().'/images/food/', $name);
                $data[] = $name;
            }

            $menu->picture = json_encode($data);
        }
        $menu->price = $request->price;
        $menu->description = $request->description;
        $menu->category_id = $request->category_id ;
        $menu->save();

        return $menu;
    }

    public function deleteMenu(int $id) {
        $menu = Article::findOrFail($id);
        $menu->delete();

    }

    public function itemPerCategory(int $id) {
        $menu = Article::where('category_id', '=', $id)->get();
        return $menu;
        // $menu->delete();
    }

    public function mostPopular() {
        $menu = DB::table('menus')
        ->leftJoin('categories', 'menus.category_id', 'categories.id')
        ->select('menus.*', 'categories.name', 'categories.url')
        ->where('picture', '<>', null)
        ->inRandomOrder()
        ->limit(8)
        ->get();
        return $menu;
        // $menu->delete();

    }

    public function categories() {
        $categories = Category::all();
        return $categories;
    }

    public function category(int $id) {
        $category = Category::findOrFail($id);
        return $category;
    }
}
