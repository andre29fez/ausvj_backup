<?php


namespace App\Service\Order;

use App\Http\Requests\Order\OrderRequest;
use App\Model\Order;
use App\Model\OrderDetailsDetails;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Yajra\DataTables\DataTables;
use Stripe; 

class OrderService
{
    public function doOrder(OrderRequest $request) {
        DB::beginTransaction();
        try {
            
            Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
            $charge = Stripe\Charge::create ([
                "amount" =>  $request->total_price * 100,
                "currency" => "myr",
                "source" => $request->stripe_token,
                "description" => "User Order : ". $request->total_price 
            ]);

            
            $orderId =  DB::table('orders')
                ->insertGetId([
                    'first_name' => $request->first_name,
                    'last_name' => $request->last_name,
                    'email' => $request->email,
                    'phone_number' => $request->phone_number,
                    'address' => $request->address,
                    'product_id' => $request->product_id,
                    'qty' => $request->qty,
                    'price' => $request->price,
                    'total_price' => $request->total_price,
                    'total_discount' => $request->total_discount,
                    'payment_status' => "SUCCESS",
                    
                ]);

            DB::commit();
        }
        catch (\Exception $e) {
            DB::rollBack();
            Log::error("[doOrder] error msg : " . $e->getMessage() . " stack trace : " . $e->getTraceAsString());
            throw $e; 
        }
    }

    public function baseQuery($request = null)
    {
        $query = DB::table('orders');
        $query = $query->join('products', 'orders.product_id', '=','products.id');
        $query = $query->select('*', "products.name as product_name");

        return $query;
    }

    public function dataTables(Request $request) {
        return DataTables::of($this->baseQuery($request))->make(true);
    }

    public function baseQueryOrderDetail($id) {
        $query = DB::table('order_details');
        $query = $query->where("order_id","=",$id);
        $query = $query->join("menus",'order_details.menu_id','=','menus.id');
        $query = $query->select("order_details.price", "order_details.total_price", "menus.description as menu_desc",
        "order_details.qty");

        return $query;
    }

    public function dataTablesOrderDetail($id) {
        return DataTables::of($this->baseQueryOrderDetail($id))->addIndexColumn()->make(true);
    }



    public function getOrder($id) {
        return DB::table("orders")->where("orders.id","=",$id)
            ->leftJoin("users","orders.user_id","=","users.id")
            ->select("orders.*","users.name as user_name","users.id as user_id")
            ->first();
    }

    public function getOrderAndOrderDetails() {
        return $this->baseQuery()
            ->join("order_details", 'orders.id', "=", "order_details.order_id")
            ->join("menus", "order_details.menu_id", "=", "menus.id")
            ->select("orders.patient_name", "orders.patient_floor", "orders.patient_bed",
                        "order_details.qty", "order_details.total_price","menus.description", "orders.created_at","orders.status");
    }

    public function getMultipleOrderAndDetailsById(array $id) {
        return $this->getOrderAndOrderDetails()->whereIn("orders.id", $id)
            ->get();
    }

    public function getMultipleOrderAndDetailsByDate(string $startDate, string $endDate) {
        return $this->getOrderAndOrderDetails()->where("orders.created_at", ">", $startDate)
                        ->where("orders.created_at", "<", $endDate)->get();
    }

}
