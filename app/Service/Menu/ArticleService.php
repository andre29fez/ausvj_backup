<?php


namespace App\Service\Menu;

// all logic business is here

use App\Model\Category;
use App\Http\Requests\Menu\ArticleRequest;
use App\Model\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Contracts\DataTable;
use Yajra\DataTables\DataTables;

class ArticleService
{
    public function getArticleList(Request $request) {
        $query = $this->baseQuery($request);
        $perPage = 10;

        // check limit
        if ($request->has('limit')) {
            $perPage = $request->limit;
        }

        if ($request->has("role")) {
            $query = $query->where('role_id','=',$request->role);
        }

       // process pagination
        return $this->pagination($request,$query,$perPage);
    }


    public function pagination($request,$query,$perPage) {
        if ($request->has('all')) {
            $results = $query->get();
            $data = new \Illuminate\Pagination\LengthAwarePaginator($results, $results->count(), -1);
        }else{
            $data = $query->paginate($perPage);
        }

        return $data;
    }

    public function getSingle($id) {
        $query= $this->baseQuery()->where("articles.id", "=", $id);
        return $query;
    }

    public function baseQuery($request = null)
    {
        $query = DB::table('articles');
        $query = $query->join("categories",'articles.category_id','=','categories.id');
        $query = $query->select('articles.*', 'categories.name');

        // dd($query->toSql());
        return $query;
    }

    public function dataTables(Request $request) {
        return DataTables::of($this->baseQuery($request))->make(true);
    }

    public function storeNewArticle(ArticleRequest  $request) {
        $article = new Article();

        if($request->hasfile('picture')){

            foreach($request->file('picture') as $key=>$file)
            {
                $name = time().$key.'.'.$file->extension();
                $file->move(public_path().'/images/food/', $name);
                $data[] = $name;
            }

            $article->picture = json_encode($data);
        }

        $article->title = $request->title;
        $article->desc = $request->desc;
        $article->meta_desc = $request->meta_desc;
        $article->category_id = $request->category_id ;
        $article->save();

        return $article;
    }

    public function updateArticle(ArticleRequest  $request) {

        $article = Article::findOrFail($request->article_id);

        if($request->hasfile('picture')){

            foreach($request->file('picture') as $key=>$file)
            {
                $name = time().$key.'.'.$file->extension();
                $file->move(public_path().'/images/food/', $name);
                $data[] = $name;
            }

            $article->picture = json_encode($data);
        }
        $article->title = $request->title;
        $article->desc = $request->desc;
        $article->meta_desc = $request->meta_desc;
        $article->category_id = $request->category_id ;
        $article->save();

        return $article;
    }

    public function deleteMenu(int $id) {
        $menu = Article::findOrFail($id);
        $menu->delete();

    }

    public function itemPerCategory(int $id) {
        $menu = Article::where('category_id', '=', $id)->get();
        return $menu;
        // $menu->delete();
    }

    public function mostPopular() {
        $menu = DB::table('articles')
        ->leftJoin('categories', 'articles.category_id', 'categories.id')
        ->select('articles.*', 'categories.name', 'categories.url')
        ->where('picture', '<>', null)
        ->inRandomOrder()
        ->limit(8)
        ->get();
        return $menu;

    }

    public function categories() {
        $categories = Category::all();
        return $categories;
    }

    public function category(int $id) {
        $category = Category::findOrFail($id);
        return $category;
    }
}
