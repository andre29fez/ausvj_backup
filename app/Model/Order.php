<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //
    public static $STATUS_IN_PROCESS = "IN_PROCESS";
    public static $STATUS_READY = "READY";
    public static $STATUS_SENT = "SENT";

    protected $table="orders";
    protected $fillable = ['first_name',
        'last_name',
        'email',
        'phone_number',
        'address',
        'product_id',
        'qty', 
        'price', 
        'total_price', 
        'total_discount', 
        'payment_status', 
        'zip' ];

}
