<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    //
    protected $fillable = [
     'category_id','picture','desc','meta_desc','title'
    ];
}
