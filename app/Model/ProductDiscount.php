<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductDiscount extends Model
{
    //

    protected $table= "products_discount";
    protected $fillable = [
        'product_id','qty_discount','percentage'
    ];
}
