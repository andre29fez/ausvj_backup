<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    //
    protected $table = 'users_roles';
    protected $fillable = ['user_is', 'role_id'];
    public $timestamps = false;
    protected $primaryKey = null;
    public $incrementing = false;

}
