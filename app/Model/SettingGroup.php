<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SettingGroup extends Model
{
    protected $guarded = [];
    public function setting()
    {
        return $this->hasMany(Setting::class);
    }
}
