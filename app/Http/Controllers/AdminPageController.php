<?php

namespace App\Http\Controllers;

use App\Model\Category;
use App\Model\Role;
use App\Service\Order\OrderService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Slider;
use App\Model\Product;
use Yajra\DataTables\Facades\DataTables;

class AdminPageController extends Controller
{

    protected $orderService;

    public function __construct()
    {
        $this->middleware('auth');
        $this->orderService=new OrderService();
    }
    public function index()
    {
        return view('home');
    }
    public function slider()
    {
        return view('admin.slider');
    }


    public function userList() {
        $role = Role::all();
        return view('admin.user-management.user',compact('role'));
    }

    public function articleList() {
        $categories = Category::all();
        return view('admin.article-management.article',compact('categories'));
    }

    public function orderList(){
        return view('admin.order-management.order');
    }

    public function orderDetail($id) {
        $order = $this->orderService->getOrder($id);
        return view('admin.order-management.order-detail',compact('order'));
    }


    public function report()
    {
        return view('admin.report.report');
    }

    public function setting()
    {
        return view('admin.setting');
    }

    public function orderPrintOut(Request $request) {

        $startDateString = $request->startDate." 00:00:00";
        $endDateString = $request->endDate." 23:59:59";

        $datas = $this->orderService->getMultipleOrderAndDetailsByDate($startDateString, $endDateString);

        return view('admin.order-management.order-print-out', compact("datas"));
    }

    public function productList() {
        return view('admin.product-management.product');
    }

    public function productDiscountList() {
        // TODO 
        // should be optimize if already have more than 100 products; 

        $products = Product::all(); 
        return view('admin.product-management.productsDiscount', compact('products'));
    }
}
