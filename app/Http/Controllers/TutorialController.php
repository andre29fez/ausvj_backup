<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TutorialController extends Controller
{
    //

    public function tutorialPussy() {
        return $this->tutorialPage("pussy");
    }

    public function tutorialLpe88() {
        return $this->tutorialPage("lpe88");
    }

    public function tutorialEvo888() {
        return $this->tutorialPage("evo888");
    }

    public function tutorial918Kaya() {
        return $this->tutorialPage("918kaya");
    }


    public function tutorialPage(string $web) {

        $webUrlApk="";
        $webUrlIos="";
        $webUrlGame= "";
        $webLogo = "";

        switch ($web) {
            case "pussy" :
                $webUrlApk="http://dm21.pussy888.com/";
                $webUrlIos="http://dm21.pussy888.com/";
                $webUrlGame= "http://dm21.pussy888.com/";
                $webLogo="https://king888.me/images/product/1616927556__2113814803.png";
                break;
            case "lpe88" :
                $webUrlApk="http://m.ld176988.com/download.html";
                $webUrlIos="http://m.ld176988.com/download.html";
                $webUrlGame= "http://m.ld176988.com/download.html";
                $webLogo="https://king888.me/images/product/1616927608__1667517395.png";

                break;
            case "evo888" :
                $webUrlApk="http://d1.evo288.com/";
                $webUrlIos="http://d1.evo288.com/";
                $webUrlGame= "http://d1.evo288.com/";
                $webLogo="https://king888.me/images/product/1616924248__643444950.png";
                break;
            case "918kaya":
                $webUrlApk="http://m.918kisses.download/";
                $webUrlIos="http://m.918kisses.download/";
                $webUrlGame= "http://m.918kisses.download/";
                $webLogo="https://king888.me/images/product/1635865587__1834638526.png";
                break;
        }
        return view('tutorial', compact("webLogo", "webUrlApk", "webUrlGame", "webUrlIos", "web"));
    }
}
