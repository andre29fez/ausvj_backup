<?php

namespace App\Http\Controllers;

use App\Model\Product;
use App\Model\ProductDiscount;
use App\Model\Setting;
use App\Service\Menu\ArticleService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Service\Menu\ProductService;

class LandingController extends Controller
{
    protected $articleService;


    public function __construct()
    {
        $this->articleService = new ArticleService();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('landingg');
    }

    public function services()
    {
        // $products = Product::all();
        // return view('services', compact('products'));
        return view('servicess');
    }

    public function article()
    {
        $article = $this->articleService->baseQuery()->simplePaginate(10);
        return view('articlee',compact('article'));
    }


   public function articleDetail($id) {
        $article = $this->articleService->getSingle($id)->first();
        return view('article-detaill',compact('article'));
   }

   public function checkout($productId) {
        $product = Product::find($productId);
        $productDiscount = ProductDiscount::where('product_id',$productId)->get();

        return view('checkout', compact('product', 'productDiscount'));
   }



}
