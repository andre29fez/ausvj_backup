<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\V1\BaseApiController;
use App\Http\Requests\Banner\AddBannerRequest;
use Illuminate\Http\Request;
use App\Slider;
use App\Traits\UploadFileHelper;

class BannerController extends BaseApiController
{
    use UploadFileHelper;
    public function index(Request $request)
    {
        $query = $this->baseQuery($request);
        $perPage = 10;
        if ($request->has('limit')) {
            $perPage = $request->limit;
        }
        if ($request->has('all')) {
            $results = $query->get();
            $data = new \Illuminate\Pagination\LengthAwarePaginator($results, $results->count(), -1);
        } else {
            $data = $query->paginate($perPage);
        }
        //$data = $query->paginate($request->limit ?? 10);
        return $this->successResponse($data);
    }

    public function baseQuery(Request $request)
    {
        $query = Slider::query(); //->with(['product']);
        // $query->whereHas('product', function ($childQuery) {
        //     $childQuery->with('brand');
        // });
        // if ($request->has('type')) {
        //     $query->where('type', $request->type);
        // }
        if ($request->has('active')) {
            $query->where('is_active', true);
        }

        return $query;
    }
    public function store(AddBannerRequest $request)
    {
        $data = new Slider();
        // $data->product_id = $request->product_id;
        // $data->type = $request->type;
        $data->caption = $request->caption;
        // $data->setTranslations('text', $this->addTranslations(
        //     $request->get('text_en'),
        //     $request->get('text_zh'),
        //     $request->get('text_ms'))
        // );
        if ($request->hasFile('image')) {
            $image = $this->uploadImg(Slider::UPLOAD_FOLDER, 'image', $request, 1200, 609);
            $data->image = $image['name'];
        }
        if ($request->has('is_active')) {
            $isActive = $request->is_active == 'true' ? true : false;
            $data->is_active = $isActive;
        }
        $data->saveOrFail();
        $data->refresh();
        // $data->load('product');

        return $this->successResponse($data);
    }
    public function update(Request $request, $id)
    {
        $data = Slider::find($id);
        //$data->product_id = $request->product_id;
        //$data->type = $request->type;
        //$data->sort = $request->sort;
        if ($request->has('caption')) {
            $data->caption = $request->caption;
            // $data->setTranslations('text', $this->addTranslations(
            //     $request->get('text_en'),
            //     $request->get('text_zh'),
            //     $request->get('text_ms'))
            // );

        }

        if ($request->hasFile('image')) {
            $image = $this->uploadImg(Slider::UPLOAD_FOLDER, 'image', $request, 1068, 609);
            $data->image = $image['name'];
        }
        if ($request->has('is_active')) {
            $isActive = $request->is_active == 'true' ? true : false;
            $data->is_active = $isActive;
        }
        $data->saveOrFail();
        $data->refresh();
        // $data->load('product');

        return $this->successResponse($data);
    }
    public function destroy($id)
    {
        $data = Slider::findOrFail($id);
        $data->delete();

        return $this->successResponse($data);
    }
}
