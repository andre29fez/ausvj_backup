<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\V1\BaseApiController;
use App\Http\Requests\Order\OrderRequest;
use App\Http\Requests\User\UserRequest;
use App\Service\Order\OrderService;
use App\Service\User\UserService;
use Illuminate\Http\Request;
use App\Model\Product;
use App\Traits\UploadFileHelper;

class OrderController extends BaseApiController
{
    protected $orderService;


    public function __construct()
    {
        $this->orderService = new OrderService();
    }

    public function doProcessOrder(OrderRequest $request)
    {
        // dd($data->all());
        $data = $this->orderService->doOrder($request);
        $data = str_pad($data, 5, '0', STR_PAD_LEFT);
        if ($data) {
            // if ($data == "ok") {
            return $this->successResponse([
                "msg"=>"Order Success",
                "data"=>$data
            ]);
        }

        return $this->failedResponse($data);
    }



}
