<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Api\V1\BaseApiController;
use App\Http\Requests\Menu\ArticleRequest;
use App\Http\Requests\Product\ProductRequest;
use App\Service\Product\ProductService;
use Illuminate\Http\Request;
use App\Model\Product;
use App\Traits\UploadFileHelper;

class ProductController extends BaseApiController
{
    use UploadFileHelper;
    protected $productService;


    public function __construct()
    {
        $this->productService = new ProductService();
    }

    public function index(Request $request)
    {
        return $this->productService->dataTables($request);
    }
    public function view($id)
    {
        $data = $this->productService->getSingle($id)->first();
        return $this->successResponse($data);
    }
    public function store(ProductRequest $request)
    {
        $data = $this->productService->storeNew($request);
        return $this->successResponse($data);
    }
    public function update(ProductRequest $request)
    {
        $data = $this->productService->update($request);
        return $this->successResponse($data);
    }
    public function delete($id)
    {
        $this->productService->delete($id);
        return $this->successResponse("ok");
    }
}
