<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Exports\OrdersExport;
use App\Http\Controllers\Api\V1\BaseApiController;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ReportController extends BaseApiController
{
    public function print(Request $request)
    {
        // dd($request->all());
        return Excel::download(new OrdersExport($request), 'orders'.date('YmdHis').'.xlsx');
    }
}
