<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Api\V1\BaseApiController;
use App\Http\Requests\User\UserRequest;
use App\Service\User\UserService;
use Illuminate\Http\Request;
use App\Model\Product;
use App\Traits\UploadFileHelper;

class UserController extends BaseApiController
{
    use UploadFileHelper;
    protected $userService;


    public function __construct()
    {
        $this->userService = new UserService();
    }

    public function index(Request $request)
    {
        return $this->userService->dataTables($request);
    }
    public function view($id)
    {
        $data = $this->userService->getSingle($id)->first();
        return $this->successResponse($data);
    }
    public function store(UserRequest $request)
    {
        $data = $this->userService->storeNewUser($request);
        return $this->successResponse($data);
    }
    public function update(UserRequest $request)
    {
        $data = $this->userService->updateUser($request);
        return $this->successResponse($data);
    }
    public function delete($id)
    {
        $this->userService->deleteUser($id);
        return $this->successResponse("ok");
    }
}
