<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Api\V1\BaseApiController;
use App\Http\Requests\Menu\ArticleRequest;
use App\Service\Menu\ArticleService;
use App\Service\Menu\ProductService;
use Illuminate\Http\Request;
use App\Model\Product;
use App\Traits\UploadFileHelper;

class ArticleController extends BaseApiController
{
    use UploadFileHelper;
    protected $articleService;


    public function __construct()
    {
        $this->articleService = new ArticleService();
    }

    public function index(Request $request)
    {
        return $this->articleService->dataTables($request);
    }
    public function view($id)
    {
        $data = $this->articleService->getSingle($id)->first();
        return $this->successResponse($data);
    }
    public function store(ArticleRequest $request)
    {
        $data = $this->articleService->storeNew($request);
        return $this->successResponse($data);
    }
    public function update(ArticleRequest $request)
    {
        $data = $this->articleService->updateArticle($request);
        return $this->successResponse($data);
    }
    public function delete($id)
    {
        $this->articleService->deleteMenu($id);
        return $this->successResponse("ok");
    }
}
