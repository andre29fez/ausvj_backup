<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Api\V1\BaseApiController;
use App\Http\Requests\Order\OrderRequest;
use App\Http\Requests\User\UserRequest;
use App\Service\Order\OrderService;
use App\Service\User\UserService;
use Illuminate\Http\Request;
use App\Model\Product;
use App\Traits\UploadFileHelper;

class OrderController extends BaseApiController
{
    protected $orderService;


    public function __construct()
    {
        $this->orderService = new OrderService();
    }

    public function index(Request $request)
    {
        return $this->orderService->dataTables($request);
    }

    public function orderDetail($id, Request $request) {
        return $this->orderService->dataTablesOrderDetail($id);
    }



}
