<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Api\V1\BaseApiController;
use App\Http\Requests\Menu\ArticleRequest;
use App\Http\Requests\Product\ProductDiscountRequest;
use App\Http\Requests\Product\ProductRequest;
use App\Service\Product\ProductDiscountService;
use Illuminate\Http\Request;
use App\Model\Product;
use App\Traits\UploadFileHelper;

class ProductsDiscountController extends BaseApiController
{
    use UploadFileHelper;
    protected $productsDiscountService;


    public function __construct()
    {
        $this->productsDiscountService = new ProductDiscountService();
    }

    public function index(Request $request)
    {
        return $this->productsDiscountService->dataTables($request);
    }
    public function view($id)
    {
        $data = $this->productsDiscountService->getSingle($id)->first();
        return $this->successResponse($data);
    }
    public function store(ProductDiscountRequest $request)
    {
        $data = $this->productsDiscountService->storeNew($request);
        return $this->successResponse($data);
    }
    public function update(ProductDiscountRequest $request)
    {
        $data = $this->productsDiscountService->update($request);
        return $this->successResponse($data);
    }
    public function delete($id)
    {
        $this->productsDiscountService->delete($id);
        return $this->successResponse("ok");
    }
}
