<?php

namespace App\Http\Requests\Menu;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class ArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        $rules['article_id'] = ['sometimes','numeric'];
        $rules['meta_desc'] = ['required', 'string'];
        $rules['desc'] = ['required','string'];
        $rules['title']=['required','string'];
        $rules['category_id'] = ['required','numeric'];

        // if (!Request::input('oldPicture')) {
        //     # code...
        //     $rules['picture'] = ['required'];
        // }

        return $rules;
    }
}
