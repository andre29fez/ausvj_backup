<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => ['sometimes','numeric'],
            'name'=>['required', 'string'],
            'email'=>['required','email'],
            'phone'=>['required','numeric'],
            'password'=>['sometimes', 'string'],
            'is_active'=>['required','numeric'],
            'role'=>['required','numeric']
        ];
    }
}
