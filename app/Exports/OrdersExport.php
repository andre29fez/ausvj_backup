<?php

namespace App\Exports;

use App\Model\Order;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class OrdersExport implements FromCollection, WithHeadings, WithEvents
{
    protected $request;

    function __construct($request)
    {
        $this->request = $request;
    }
    public function startCell(): string
    {
        return 'A2';
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $status = $this->request->status;
        $fromDate = $this->request->fromDate;
        $toDate = $this->request->toDate;


        $data = DB::table('orders')
            ->select('orders.patient_name', 'orders.created_at', 'menus.description', 'orders.patient_bed', 'orders.total_price_order', 'orders.status')
            ->leftJoin('order_details', 'orders.id', 'order_details.order_id')
            ->leftJoin('menus', 'order_details.menu_id', 'menus.id')
            ->whereDate('orders.created_at', '>=', $fromDate)
            ->whereDate('orders.created_at', '<=', $toDate)
            ->when($status != '0', function ($query) use ($status) {
                // dd($status);
                $query->where('orders.status', $status);
            })
            ->get();
        // ->toSql();
        // dd($data);
        return $data;
    }



    public function headings(): array
    {
        return ["Patient name", "Date", "Food",  "Bed number", "Price", "Status"];
    }


    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function (AfterSheet $event) {

                $event->sheet->getDelegate()->getColumnDimension('A')->setWidth(30);
                $event->sheet->getDelegate()->getColumnDimension('B')->setWidth(20);
                $event->sheet->getDelegate()->getColumnDimension('C')->setWidth(40);
                $event->sheet->getDelegate()->getColumnDimension('D')->setWidth(15);
                $event->sheet->getDelegate()->getColumnDimension('E')->setWidth(15);
                $event->sheet->getDelegate()->getColumnDimension('F')->setWidth(15);
            },
        ];
    }
}
