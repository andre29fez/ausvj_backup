<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('first_name'); 
            $table->string('last_name');
            $table->string('email')->unique();
            $table->string('phone_number')->unique();;
            $table->string('address');
            $table->unsignedBigInteger('product_id');
            $table->decimal('price'); 
            $table->integer('qty');
            $table->decimal('total_price');
            $table->decimal('total_discount'); 
            $table->string('payment_status'); 
             
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
