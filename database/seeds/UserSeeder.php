<?php

use App\Model\Role;
use App\Model\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $roleSuperAdmin = new Role();
        $roleSuperAdmin->name="super";
        $roleSuperAdmin->slug="super";
        $roleSuperAdmin->created_at = \Carbon\Carbon::now();
        $roleSuperAdmin->save();

        $superAdmin = new User();
        $superAdmin->name="super admin";
        $superAdmin->email="superadmin@mail.com";
        $superAdmin->password= Hash::make("passwordAdmin123");
        $superAdmin->created_at = \Carbon\Carbon::now();
        $superAdmin->save();


        $roleAdmin = new Role();
        $roleAdmin->name="admin";
        $roleAdmin->slug="admin";
        $roleAdmin->created_at = \Carbon\Carbon::now();
        $roleAdmin->save();

        $userAdmin = new User();
        $userAdmin->name="admin";
        $userAdmin->email="admin@mail.com";
        $userAdmin->password= Hash::make("passwordAdmin123");
        $userAdmin->created_at = \Carbon\Carbon::now();
        $userAdmin->save();


        $roleNurse = new Role();
        $roleNurse->name="nurse";
        $roleNurse->slug="nurse";
        $roleNurse->created_at = \Carbon\Carbon::now();
        $roleNurse->save();

        $userNurse = new User();
        $userNurse->name="nurse";
        $userNurse->email="nurse@mail.com";
        $userNurse->password= Hash::make("password123");
        $userNurse->created_at = \Carbon\Carbon::now();
        $userNurse->save();

        DB::table('users_roles')->insert([
            'user_id' => $userAdmin->id,
            'role_id' => $roleAdmin->id
        ]);

        DB::table('users_roles')->insert([
            'user_id' => $superAdmin->id,
            'role_id' => $roleSuperAdmin->id
        ]);

        DB::table('users_roles')->insert([
            'user_id' => $userNurse->id,
            'role_id' => $roleNurse->id
        ]);
    }
}
