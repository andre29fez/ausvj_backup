<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $data = ["Breakfast", "Mix Tab", "Instant Mee", "Classic", "Local Favourite", "Snacks Team", "Western Playoff", "Layer / Pulp Calve", "Bottle / Juice"];
        $url = ["breakfast", "mixtab", "instantmee", "classic", "localfavourite", "snackteam", "westernplayoff", "layer", "bottle"];

        for ($i=0; $i < count($data); $i++) { 
            # code...
            DB::table('categories')->insert([
                'name' => $data[$i],
                'url' => $url[$i],
            ]);
        }
    }
}
