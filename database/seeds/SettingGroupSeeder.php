<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class SettingGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now()->toDateTimeString();

        DB::table('setting_groups')->insert([
             ['id' => 1, 'name' => 'General', 'description' => 'General settings', 'created_at' => $now, 'updated_at' => $now],
             ['id' => 2, 'name' => 'Contact', 'description' => 'Contact settings', 'created_at' => $now, 'updated_at' => $now],
             ['id' => 3, 'name' => 'Google Meta', 'description' => 'Google Meta settings', 'created_at' => $now, 'updated_at' => $now]
        ]);
    }
}
