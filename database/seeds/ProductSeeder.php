<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now()->toDateTimeString();
        DB::table('products')->insert([
            [
                'name' => 'Product 1',
                'logo' => '918_kiss.png',
                'background' => 'bg_kiss.png',
                'link_desktop' => 'http://linkexample.me/',
                'link_mac' => 'http://linkexample.me/',
                'link_android' => 'http://linkexample.me/',
                'created_at' => $now, 
                'updated_at' => $now
            ],
            [
                'name' => 'Product 2',
                'logo' => '918_kiss.png',
                'background' => 'bg_kiss.png',
                'link_desktop' => 'http://linkexample.me/',
                'link_mac' => 'http://linkexample.me/',
                'link_android' => 'http://linkexample.me/',
                'created_at' => $now, 
                'updated_at' => $now
            ],
            [
                'name' => 'Product 3',
                'logo' => '918_kiss.png',
                'background' => 'bg_kiss.png',
                'link_desktop' => 'http://linkexample.me/',
                'link_mac' => 'http://linkexample.me/',
                'link_android' => 'http://linkexample.me/',
                'created_at' => $now, 
                'updated_at' => $now
            ]
        ]);
    }
}
