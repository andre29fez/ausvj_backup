<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(SliderSeeder::class);
        //$this->call(ProductSeeder::class);
        //$this->call(SettingGroupSeeder::class);
        //$this->call(SettingSeeder::class);
       // $this->call(CategorySeeder::class);
        $this->call(UserSeeder::class);
       // $this->call(FoodSeeder::class);

    }
}
