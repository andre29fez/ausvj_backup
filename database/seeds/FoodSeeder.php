<?php

use App\Model\Articlecle;
use Illuminate\Database\Seeder;

class FoodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $data=[
            [
                "id"=>"1",
                "category"=>"9",
                "name"=>"CANNED MILK",
                "price"=>"2.80",
                "img"=>[]
            ],
            [
                "id"=>"2",
                "category"=>"9",
                "name"=>"KURMA MILK",
                "price"=>"4.00",
                "img"=>[]
            ],
            [
                "id"=>"3",
                "category"=>"9",
                "name"=>"EVIAN 500ML",
                "price"=>"6.00",
                "img"=>[]
            ],
            [
                "id"=>"4",
                "category"=>"9",
                "name"=>"MINERAL WATER 500ML",
                "price"=>"1.80",
                "img"=>[]
            ],
            [
                "id"=>"5",
                "category"=>"9",
                "name"=>"MINERAL WATER 1.5L",
                "price"=>"3.30",
                "img"=>[]
            ],

            [
                "id"=>"1",
                "category"=>"1",
                "name"=>"SET [COFFEE C/ TEH C/ SOFT BOIL/ TOAST]",
                "price"=>"8.50",
                "img"=>[]
            ],
            [
                "id"=>"2",
                "category"=>"1",
                "name"=>"SOFT BOIL ALA CARTE",
                "price"=>"3.30",
                "img"=>[]
            ],
            [
                "id"=>"3",
                "category"=>"1",
                "name"=>"KAYA BUTTER TOAST",
                "price"=>"3.80",
                "img"=>[]
            ],
            [
                "id"=>"4",
                "category"=>"1",
                "name"=>"ROTI DINO",
                "price"=>"4.80",
                "img"=>[]
            ],
            [
                "id"=>"5",
                "category"=>"1",
                "name"=>"PEANUT BUTTER",
                "price"=>"4.30",
                "img"=>[]
            ],
            [
                "id"=>"6",
                "category"=>"1",
                "name"=>"PEANUT CHEESE",
                "price"=>"4.80",
                "img"=>[]
            ],
            [
                "id"=>"7",
                "category"=>"1",
                "name"=>"EGG SANDWICH",
                "price"=>"3.30",
                "img"=>[]
            ],
            [
                "id"=>"8",
                "category"=>"1",
                "name"=>"AMERICAN 1",
                "price"=>"9.90",
                "img"=>[
                    "AmericanBreakfast.jpg",
                ]
            ],

            [
                "id"=>"1",
                "category"=>"4",
                "name"=>"MEE KOLOK",
                "price"=>"4.50",
                "img"=>[
                    "KolokMee.jpg",
                ]
            ],
            [
                "id"=>"2",
                "category"=>"4",
                "name"=>"MEE KOLOK SPECIAL",
                "price"=>"6.00",
                "img"=>[
                    "KolokMeeSpecial.jpg",
                ]
            ],
            [
                "id"=>"3",
                "category"=>"4",
                "name"=>"MEE KOLOK MERAH",
                "price"=>"5.00",
                "img"=>[]
            ],
            [
                "id"=>"4",
                "category"=>"4",
                "name"=>"MEE KOLOK KICAP PEDAS",
                "price"=>"5.00",
                "img"=>[]
            ],
            [
                "id"=>"5",
                "category"=>"4",
                "name"=>"BEE HOON KOLOK",
                "price"=>"4.50",
                "img"=>[]
            ],
            [
                "id"=>"6",
                "category"=>"4",
                "name"=>"BEE HOON SPECIAL",
                "price"=>"6.00",
                "img"=>[]
            ],
            [
                "id"=>"7",
                "category"=>"4",
                "name"=>"BEE HOON MERAH",
                "price"=>"5.00",
                "img"=>[]
            ],
            [
                "id"=>"8",
                "category"=>"4",
                "name"=>"BEE HOON KICAP PEDAS",
                "price"=>"5.00",
                "img"=>[

                ]
            ],
            [
                "id"=>"9",
                "category"=>"4",
                "name"=>"KUEH TEOW KOLOK",
                "price"=>"4.50",
                "img"=>[

                ]
            ],
            [
                "id"=>"10",
                "category"=>"4",
                "name"=>"KUEH TEOW SPECIAL",
                "price"=>"6.00",
                "img"=>[

                ]
            ],
            [
                "id"=>"11",
                "category"=>"4",
                "name"=>"KUEH TEOW MERAH",
                "price"=>"5.00",
                "img"=>[

                ]
            ],
            [
                "id"=>"12",
                "category"=>"4",
                "name"=>"KUEH TEOW KICAP PEDAS",
                "price"=>"5.00",
                "img"=>[

                ]
            ],
            [
                "id"=>"13",
                "category"=>"4",
                "name"=>"WANTAN",
                "price"=>"4.50",
                "img"=>[

                ]
            ],
            [
                "id"=>"14",
                "category"=>"4",
                "name"=>"WANTAN KOLOK",
                "price"=>"6.50",
                "img"=>[

                ]
            ],
            [
                "id"=>"15",
                "category"=>"4",
                "name"=>"WANTAN SPECIAL",
                "price"=>"6.00",
                "img"=>[

                ]
            ],
            [
                "id"=>"16",
                "category"=>"4",
                "name"=>"WANTAN MERAH",
                "price"=>"5.00",
                "img"=>[

                ]
            ],
            [
                "id"=>"17",
                "category"=>"4",
                "name"=>"WANTAN KICAP PEDAS",
                "price"=>"5.00",
                "img"=>[

                ]
            ],

            [
                "id"=>"1",
                "category"=>"3",
                "name"=>"MAGGI GORENG",
                "price"=>"5.50",
                "img"=>[]
            ],
            [
                "id"=>"2",
                "category"=>"3",
                "name"=>"MAGGI SOUP",
                "price"=>"4.50",
                "img"=>[]
            ],
            [
                "id"=>"3",
                "category"=>"3",
                "name"=>"MEE SEDAP GORENG",
                "price"=>"5.50",
                "img"=>[]
            ],

            [
                "id"=>"1",
                "category"=>"8",
                "name"=>"3 LAYER TEA APONG",
                "price"=>"5.50",
                "img"=>[
                    "Drink3LayerBlackSugar.jpg"
                ]
            ],
            [
                "id"=>"2",
                "category"=>"8",
                "name"=>"3 LAYER TEA PANDAN",
                "price"=>"5.50",
                "img"=>[
                    "Drink3LayerPandan.jpg"
                ]
            ],
            [
                "id"=>"3",
                "category"=>"8",
                "name"=>"3 LAYER COFFEE APONG",
                "price"=>"5.50",
                "img"=>[]
            ],
            [
                "id"=>"4",
                "category"=>"8",
                "name"=>"3 LAYER COFFEE PANDAN",
                "price"=>"5.50",
                "img"=>[]
            ],
            [
                "id"=>"5",
                "category"=>"8",
                "name"=>"SIRAP BANDUNG",
                "price"=>"3.80",
                "img"=>[]
            ],
            [
                "id"=>"6",
                "category"=>"8",
                "name"=>"BANDUNG CINCAU",
                "price"=>"4.80",
                "img"=>[
                    "DrinkChinChawBantung.jpg"
                ]
            ],
            [
                "id"=>"7",
                "category"=>"8",
                "name"=>"BANDUNG LONGAN",
                "price"=>"4.80",
                "img"=>[]
            ],
            [
                "id"=>"8",
                "category"=>"8",
                "name"=>"LONGAN MILK",
                "price"=>"4.80",
                "img"=>[

                ]
            ],

            [
                "id"=>"1",
                "category"=>"5",
                "name"=>"HOKKIEN MEE",
                "price"=>"9.80",
                "img"=>[
                    "HokkienMee.jpg",
                ]
            ],
            [
                "id"=>"2",
                "category"=>"5",
                "name"=>"SAMBAL FRIED RICE / NOODLE / KUEW TEOW / RICE NOODLE",
                "price"=>"7.80",
                "img"=>[]
            ],
            [
                "id"=>"3",
                "category"=>"5",
                "name"=>"FRIED WET NOODLE",
                "price"=>"6.80",
                "img"=>[]
            ],
            [
                "id"=>"4",
                "category"=>"5",
                "name"=>"SARAWAK FRIED MEE HOON",
                "price"=>"9.80",
                "img"=>[
                    "FriedBeeHoon.jpg"
                ]
            ],
            [
                "id"=>"5",
                "category"=>"5",
                "name"=>"FISH FILLET ON RICE",
                "price"=>"11.80",
                "img"=>[
                    "ButterFishChicken.jpg"
                ]
            ],
            [
                "id"=>"6",
                "category"=>"5",
                "name"=>"SARAWAK ASAM SOUP NOODLE",
                "price"=>"11.80",
                "img"=>[
                    "AsamSoupNoodle.jpg"
                ]
            ],
            [
                "id"=>"7",
                "category"=>"5",
                "name"=>"CHICKEN BUTTER MILK + RICE",
                "price"=>"9.80",
                "img"=>[
                    "ButterChickenRice.jpg"
                ]
            ],
            [
                "id"=>"8",
                "category"=>"5",
                "name"=>"SWEET & SOUR CHICKEN + RICE",
                "price"=>"9.80",
                "img"=>[

                ]
            ],
            [
                "id"=>"9",
                "category"=>"5",
                "name"=>"LEMON CHICKEN + RICE",
                "price"=>"9.80",
                "img"=>[

                ]
            ],
            [
                "id"=>"10",
                "category"=>"5",
                "name"=>"[ADD ON FRIED EGG]",
                "price"=>"1.80",
                "img"=>[

                ]
            ],
            [
                "id"=>"11",
                "category"=>"5",
                "name"=>"FRIED RICE",
                "price"=>"5.80",
                "img"=>[
                    "FriedRice.jpg"
                ]
            ],
            [
                "id"=>"12",
                "category"=>"5",
                "name"=>"FRIED KUEH TEOW",
                "price"=>"5.80",
                "img"=>[
                    "FriedKuewTeow.jpg"
                ]
            ],
            [
                "id"=>"13",
                "category"=>"5",
                "name"=>"FRIED MEE",
                "price"=>"5.80",
                "img"=>[

                ]
            ],
            [
                "id"=>"14",
                "category"=>"5",
                "name"=>"MEE JAWA",
                "price"=>"6.80",
                "img"=>[
                    "JawaSatay.jpg"
                ]
            ],
            [
                "id"=>"15",
                "category"=>"5",
                "name"=>"MEE JAWA SPECIAL",
                "price"=>"9.80",
                "img"=>[

                ]
            ],
            [
                "id"=>"16",
                "category"=>"5",
                "name"=>"LAKSA SARAWAK",
                "price"=>"7.80",
                "img"=>[
                    "Laksa.jpg"
                ]
            ],
            [
                "id"=>"17",
                "category"=>"5",
                "name"=>"LAKSA SARAWAK SPECIAL",
                "price"=>"10.80",
                "img"=>[

                ]
            ],
            [
                "id"=>"17",
                "category"=>"5",
                "name"=>"FRESCO NASI LEMAK WITH FRIED CHICKEN",
                "price"=>"10.80",
                "img"=>[
                    "NasiLemak.jpg"
                ]
            ],

            [
                "id"=>"1",
                "category"=>"2",
                "name"=>"WANTAN KOLOK KICAP PEDAS",
                "price"=>"7.80",
                "img"=>[]
            ],
            [
                "id"=>"2",
                "category"=>"2",
                "name"=>"WANTAN KOLOK MERAH",
                "price"=>"7.00",
                "img"=>[]
            ],
            [
                "id"=>"3",
                "category"=>"2",
                "name"=>"WANTAN KOLOK SPECIAL",
                "price"=>"8.00",
                "img"=>[]
            ],
            [
                "id"=>"4",
                "category"=>"2",
                "name"=>"FISH PORRIDGE",
                "price"=>"5.50",
                "img"=>[]
            ],
            [
                "id"=>"5",
                "category"=>"2",
                "name"=>"CHICKEN PORRIDGE",
                "price"=>"5.50",
                "img"=>[]
            ],
            [
                "id"=>"1",
                "category"=>"6",
                "name"=>"BASKET FRIES",
                "price"=>"6.50",
                "img"=>[]
            ],
            [
                "id"=>"2",
                "category"=>"6",
                "name"=>"FISHBALL [3 PIECES]",
                "price"=>"3.00",
                "img"=>[]
            ],
            [
                "id"=>"3",
                "category"=>"6",
                "name"=>"CHICKEN NUGGET [3 PIECES]",
                "price"=>"3.00",
                "img"=>[]
            ],
            [
                "id"=>"4",
                "category"=>"6",
                "name"=>"CIRSPY CHICKEN WING [1 PIECE]",
                "price"=>"3.80",
                "img"=>[]
            ],

            [
                "id"=>"1",
                "category"=>"7",
                "name"=>"BUTTERMILK PRAWN PASTA",
                "price"=>"19.90",
                "img"=>[
                    "PrawnPasta.jpg"
                ]
            ],
            [
                "id"=>"2",
                "category"=>"7",
                "name"=>"CORDEN BLUE CHICKEN",
                "price"=>"19.90",
                "img"=>[]
            ],
            [
                "id"=>"3",
                "category"=>"7",
                "name"=>"CHICKEN CHOP [Black Pepper Sauce / Mushroom Sauce]",
                "price"=>"19.90",
                "img"=>[
                    "ChickenChop.jpg"
                ]
            ],
            [
                "id"=>"4",
                "category"=>"7",
                "name"=>"BOLOGNESE PASTA WITH CHICKEN CHOP",
                "price"=>"19.90",
                "img"=>[]
            ],
            [
                "id"=>"5",
                "category"=>"7",
                "name"=>"BOLOGNESE PASTA CHICKEN",
                "price"=>"11.90",
                "img"=>[]
            ],
            [
                "id"=>"6",
                "category"=>"7",
                "name"=>"BOLOGNESES PASTA BEEF",
                "price"=>"14.90",
                "img"=>[]
            ],
            [
                "id"=>"7",
                "category"=>"7",
                "name"=>"FISH & CHIPS",
                "price"=>"14.9",
                "img"=>[
                    "FishandChip.jpg"
                ]
            ]
                ];
                Article::truncate();
                for ($i=0; $i < count($data); $i++) {
                    # code...
                    // dd($data[$i]['img'][0]);
                    $menu = new Article();
                    $menu->price = $data[$i]['price'];
                    $menu->description = $data[$i]['name'];
                    $menu->category_id = $data[$i]['category'] ;
                    if ($data[$i]['img']) {
                        # code...
                        $menu->picture = json_encode([$data[$i]['img'][0]]);
                    }

                    $menu->save();
                }

    }
}
