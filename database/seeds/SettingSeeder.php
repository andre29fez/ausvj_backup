<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //general settings 1
        //contact setting 2
        //google meta 3
        $now = Carbon::now()->toDateTimeString();
        DB::table('settings')->insert([            
            ['label' => 'Video Url', 'key' => 'video_url', 'value' => 'i', 'description' => 'Insert Google drive video eg. https://drive.google.com/uc?export=download&id=0B0JMGMGgxp9WMEdWb1hyQUhlOWs', 'setting_group_id' => 1, 'order' => 5, 'created_at' => $now, 'updated_at' => $now]
        ]);
        //['label' => 'Main Header Title', 'key' => 'header_title', 'value' => 'Best Trusted Agency', 'description' => 'Insert your main header title', 'setting_group_id' => 1, 'order' => 1, 'created_at' => $now, 'updated_at' => $now],    
            //['label' => 'Main Header Description', 'key' => 'header_desc', 'value' => 'We have variety of game product, among the popular brand : 918Kiss and Mega888, do look out for our agent for any top up enquiry and exclusive top up bonus.', 'description' => 'Insert your main header description', 'setting_group_id' => 1, 'order' => 2, 'created_at' => $now, 'updated_at' => $now],    
            //['label' => 'Product Header Title', 'key' => 'product_title', 'value' => 'Our Product Range', 'description' => 'Insert your product title', 'setting_group_id' => 1, 'order' => 3, 'created_at' => $now, 'updated_at' => $now],    
            //['label' => 'Product Header Description', 'key' => 'product_desc', 'value' => 'You can find our product download details on each brand.', 'setting_group_id' => 1, 'order' => 4, 'created_at' => $now, 'updated_at' => $now],                
            //['label' => 'LiveChat Url', 'key' => 'livechat_url', 'value' => 'http://example.com', 'description' => 'Insert your LiveChat url', 'setting_group_id' => 2, 'order' => 1, 'created_at' => $now, 'updated_at' => $now],
            //['label' => 'Facebook Url', 'key' => 'facebook_url', 'value' => 'http://example.com', 'description' => 'Insert your Facebook url', 'setting_group_id' => 2, 'order' => 2,'created_at' => $now, 'updated_at' => $now],
            //['label' => 'Whatsapp Url', 'key' => 'whatsapp_url', 'value' => 'http://example.com', 'description' => 'Insert your Twitter url', 'setting_group_id' => 2, 'order' => 3,'created_at' => $now, 'updated_at' => $now],
            //['label' => 'Instagram Url', 'key' => 'instagram_url', 'value' => 'http://example.com', 'description' => 'Insert your Instagram url', 'setting_group_id' => 2, 'order' => 4,'created_at' => $now, 'updated_at' => $now],
            //['label' => 'Telegram Url', 'key' => 'telegram_url', 'value' => 'http://example.com', 'description' => 'Insert your Telegram url', 'setting_group_id' => 2, 'order' => 5,'created_at' => $now, 'updated_at' => $now],            
            //['label' => 'Google analytics code', 'key' => 'google_analytic', 'value' => 'input code here', 'description' => 'Insert Google analytics code here', 'setting_group_id' => 3, 'order' => 1, 'created_at' => $now, 'updated_at' => $now],
            //['label' => 'Google ads code', 'key' => 'google_ads', 'value' => 'input code here', 'description' => 'Insert Google ads code', 'setting_group_id' => 3, 'order' => 2, 'created_at' => $now, 'updated_at' => $now]
    }
}
