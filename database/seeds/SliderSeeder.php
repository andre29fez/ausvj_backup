<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class SliderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now()->toDateTimeString();
        DB::table('sliders')->insert([
            ['image' => 'PIC_1.jpg', 'caption' => 'CAPTION 1','created_at' => $now, 'updated_at' => $now],
            ['image' => 'PIC_2.jpg', 'caption' => 'CAPTION 2','created_at' => $now, 'updated_at' => $now],
            ['image' => 'PIC_3.jpg', 'caption' => 'CAPTION 3','created_at' => $now, 'updated_at' => $now],
            ['image' => 'PIC_4.jpg', 'caption' => 'CAPTION 4','created_at' => $now, 'updated_at' => $now]
        ]);
    }
}
