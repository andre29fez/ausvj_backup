<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();


Route::get('/', 'LandingController@index')->name('landing');
Route::get('/service', 'LandingController@services')->name('service');
Route::get('/article', 'LandingController@article')->name('article');
Route::get('/article/{id}', 'LandingController@articleDetail')->name('article.detail');
Route::get('/checkout/{productId}', 'LandingController@checkout')->name('checkout');

// ---- ROUTE FOR ADMIN ---
Route::group(['middleware' => ['role:super|admin']], function() {
    Route::get('/dashboard', 'AdminPageController@index')->name('home');
    Route::group(['prefix' => 'dashboard'], function() {
        Route::get('/slider', 'AdminPageController@slider')->name('slider.index');
        Route::get('/product', 'AdminPageController@product')->name('product.index');
        Route::get('/user', 'AdminPageController@userList')->name('user.index');
        Route::get('/article', 'AdminPageController@articleList')->name('menu.index');
        Route::get('/report', 'AdminPageController@report')->name('report.index');
        Route::get('/order', 'AdminPageController@orderList')->name('order.index');
        Route::get('/order/print', 'AdminPageController@orderPrintOut')->name('order.print');
        Route::get('/order/{id}', 'AdminPageController@orderDetail')->name('order.details');
        Route::get('/product', 'AdminPageController@productList')->name('product.index');
        Route::get('/product-discount', 'AdminPageController@productDiscountList')->name('product-discount.index');
    });

    // api for admin
    Route::group(['prefix' => 'api'], function() {
        Route::group(['prefix' => 'user','middleware' => ['role:super|admin']], function () {
            Route::get('index','Api\V1\Admin\UserController@index')->name('api.v1.user.index');
            Route::post('store','Api\V1\Admin\UserController@store')->name('api.v1.user.store');
            Route::get('{id}','Api\V1\Admin\UserController@view')->name('api.v1.user.view');
            Route::post('update','Api\V1\Admin\UserController@update')->name('api.v1.user.update');
            Route::delete('delete/{id}','Api\V1\Admin\UserController@delete')->name('api.v1.user.delete');
        });

        Route::group(['prefix' => 'article','middleware' => ['role:super|admin']], function () {
            Route::get('index','Api\V1\Admin\ArticleController@index')->name('api.v1.article.index');
            Route::post('store','Api\V1\Admin\ArticleController@store')->name('api.v1.article.store');
            Route::get('{id}','Api\V1\Admin\ArticleController@view')->name('api.v1.article.view');
            Route::post('update','Api\V1\Admin\ArticleController@update')->name('api.v1.article.update');
            Route::delete('delete/{id}','Api\V1\Admin\ArticleController@delete')->name('api.v1.article.delete');
        });

        Route::group(['prefix' => 'order','middleware' => ['role:super|admin']], function () {
            Route::get('index','Api\V1\Admin\OrderController@index')->name('api.v1.order.index');
            Route::get('{id}','Api\V1\Admin\OrderController@orderDetail')->name('api.v1.order.detail');
        });

        Route::group(['prefix' => 'report','middleware' => ['role:super|admin']], function () {
            Route::get('index','Api\V1\Admin\ReportController@index')->name('api.v1.report.index');
            Route::post('print','Api\V1\Admin\ReportController@print')->name('api.v1.report.print');
        });


        Route::group(['prefix' => 'product','middleware' => ['role:super|admin']], function () {
            Route::get('index','Api\V1\Admin\ProductController@index')->name('api.v1.product.index');
            Route::post('store','Api\V1\Admin\ProductController@store')->name('api.v1.product.store');
            Route::post('update','Api\V1\Admin\ProductController@update')->name('api.v1.product.update');;
            Route::get('{id}','Api\V1\Admin\ProductController@view')->name('api.v1.product.view');
            Route::delete('delete/{id}','Api\V1\Admin\ProductController@delete')->name('api.v1.article.delete');
        });

        Route::group(['prefix' => 'product-discount','middleware' => ['role:super|admin']], function () {
            Route::get('index','Api\V1\Admin\ProductsDiscountController@index')->name('api.v1.product-discount.index');
            Route::post('store','Api\V1\Admin\ProductsDiscountController@store')->name('api.v1.product-discount.store');
            Route::post('update','Api\V1\Admin\ProductsDiscountController@update')->name('api.v1.product-discount.update');;
            Route::get('{id}','Api\V1\Admin\ProductsDiscountController@view')->name('api.v1.product-discount.view');
            Route::delete('delete/{id}','Api\V1\Admin\ProductsDiscountController@delete')->name('api.v1.product-discount.delete');
        });

    });

 });


//  ---- FRONT END API ROUTE ----
Route::post('/api/order/create', 'Api\V1\OrderController@doProcessOrder')->name('api.v1.order.process');
