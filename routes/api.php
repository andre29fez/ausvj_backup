<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::group(['prefix' => 'v1', 'namespace' => 'Api\V1'], function () {
    Route::group(['prefix' => 'banner'], function () {
        Route::get('index','BannerController@index')->name('api.v1.banner.index');
        Route::post('store','BannerController@store')->name('api.v1.banner.store');
        Route::put('update/{id}','BannerController@update')->name('api.v1.banner.update');
        Route::delete('destroy/{id}','BannerController@destroy')->name('api.v1.banner.destroy');
    });
    Route::group(['prefix' => 'product'], function () {
        Route::get('index','ProductController@index')->name('api.v1.product.index');
        Route::post('store','ProductController@store')->name('api.v1.product.store');
        Route::put('update/{id}','ProductController@update')->name('api.v1.product.update');
        Route::delete('destroy/{id}','ProductController@destroy')->name('api.v1.product.destroy');
    });
    Route::group(['prefix' => 'setting'], function () {
        Route::get('index','SettingController@index')->name('api.v1.setting.index');
        Route::get('data','SettingController@getSettings')->name('api.v1.setting.get');
        Route::put('update/{setting}','SettingController@update')->name('api.v1.setting.update');
       // Route::delete('destroy/{id}','SettingController@destroy')->name('api.v1.setting.destroy');
    });

});
